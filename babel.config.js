const presets = [
    ["@babel/preset-env", {
        debug: true
    }]
];
const plugins = [];

module.exports = {presets, plugins};
