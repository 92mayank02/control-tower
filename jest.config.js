// module.exports = {
//     bail: true,
//     collectCoverage: true,
//     coverageDirectory: '<rootDir>/coverage',
//     coveragePathIgnorePatterns: [
//       '/node_modules/',
//       '/public',
//       '/reduxLib',
//     ],
//     coverageReporters: ['html', 'text', 'text-summary'],
//     coverageThreshold: {
//       global: {
//         branches: 20,
//         functions: 15,
//         lines: 20,
//         statements: 30,
//       },
//     },
//     moduleFileExtensions: ['jsx', 'js', 'json', 'node'],
//     rootDir: './',
//     setupFiles: ['jest-localstorage-mock', 'jest-canvas-mock'],
//     setupFilesAfterEnv: ['./jest.extensions.js'],
//     testEnvironment: 'jsdom',
//     testMatch: ['<rootDir>/src/**/?(*.)test.{jsx,js}'],
//     testPathIgnorePatterns: ['reduxLib', 'node_modules'],
//     testURL: 'http://localhost:3000',
//     transform: {
//       '^.+\\.[t|j]sx?$': 'babel-jest',
//     },
//     verbose: true,
//   };
  