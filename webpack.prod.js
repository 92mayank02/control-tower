/**
 * @author Raja Polavarapu <raja.polavarapu@kcc.com>
 * @version 1.0.0
 **/

const HtmlWebPackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const webpack = require('webpack');
const path = require('path');
const Dotenv = require('dotenv-webpack');

// Environment Variables
const { EnvironmentPlugin, ProgressPlugin } = webpack;
//const environment = process.env.NODE_ENV || 'development';
//const isProduction = environment === 'production';

let envVariables = {
    //NODE_ENV: environment,
    //isDevelopment: !isProduction,
    REACT_APP_GLOSSARY_URL:process.env.REACT_APP_GLOSSARY_URL
};

// Plugins
const plugins = [
    new ProgressPlugin(),
    new EnvironmentPlugin(envVariables),
    new webpack.NoEmitOnErrorsPlugin(),
    new HotModuleReplacementPlugin(),
    new HtmlWebPackPlugin({
        inject: true,
        template: path.resolve(__dirname, 'public', 'index.html'),
        filename: "./index.html",    
    }),
    new MiniCssExtractPlugin({
        filename: 'main.[hash].bundle.css',
        
    }),
    new Dotenv({
        path: './.env.production',
    }),
  ];
  

module.exports = (env) => {
    return {
        mode: 'development',
        optimization: {
            splitChunks: {
                chunks: 'all'
            }
        },
        module: {
            rules: [
                {
                    test: /\.css$/,
                    include: /node_modules/,
                    loaders: ['style-loader', 'css-loader'],
                },
                {
                    test: /\.scss$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        'css-loader',
                        'sass-loader'
                    ]
                },
                {
                    test: /\.(js|jsx)$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader"
                    }
                },
                {
                    test: /\.html$/,
                    use: [
                        {
                            loader: "html-loader"
                        }
                    ]
                },
                {
                    test: /\.(png|svg|jpg|jpeg|gif|ico)$/,
                    exclude: /node_modules/,
                    use: ['file-loader?name=static/images/[name].[ext]']
                },
                {
                    test: /\.(woff(2)?|eot|ttf|otf)$/,
                    use: ['file-loader?name=static/fonts/[name].[ext]']
                }
            ]
        },
        entry: './index.js',
        output: {
            path: path.resolve('dist'),
            filename: '[name].[hash].bundle.js',
            chunkFilename: '[name].[hash].bundle.js',
            publicPath: '/static',
        },
        stats: {
            children: false,
        },
        resolve: {
            modules: [path.resolve(__dirname, './src'), 'node_modules'],
            extensions: ['*', '.js', '.jsx','woff','woff2'],
            alias: {
                "@material-ui/styles": path.resolve(__dirname, "node_modules", "@material-ui/styles"),
            }
        },
        optimization: {
            minimize: true
        },
        plugins
    };
}
