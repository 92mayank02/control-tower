import FIlterChips from 'components/common/FIlterChips';
import { mockStoreValue } from 'components/dummydata/mock';
import { getTestElement, renderWithMockStore, fireEvent, userEvent, screen } from '../testUtils';
import moment from "moment-timezone";
describe('<Filter Chips Component>', () => {

    afterAll(() => {
        jest.restoreAllMocks();
    });

    const props = {
        tableName: "network",
        type: "network",
        subtype: 'network',
        checkFilterChipsLength: jest.fn()
    }

    const customMock1 = {
        ...mockStoreValue, options: {
            ...mockStoreValue.options, filters: {
                ...mockStoreValue.options.filters, network: {
                    ...mockStoreValue.options.filters.network, "tariffServiceCode": {
                        "type": "text",
                        "name": "Carrier Service Code",
                        "data": ["PKUP,HKMD", "VHGB"]
                    },
                    destName: {
                        stringToArray: true,
                        "type": "text",
                        "name": "Customer Name",
                        data: "test"
                    },
                    textCheckList: {
                        stringToArray: true,
                        "type": "textcheck",
                        "name": "Text Check",
                        data: "test"
                    },
                    deliveryBlockCodeList: {
                        type: "textcheck",
                        name: "Delivery/Credit Block",
                        placeholder: 'Enter delivery block code',
                        shortName: "Delivery Block Code",
                        addon: "creditOnHold",
                        "data": ["XX", "AA", "VB"]
                    },
                }
            }
        }
    }

    const customMock2 = {
        ...mockStoreValue, options: {
            ...mockStoreValue.options, filters: {
                ...mockStoreValue.options.filters, network: {
                    ...mockStoreValue.options.filters.network, "tariffServiceCode": {
                        "type": "text",
                        "name": "Carrier Service Code",
                        stringToArray: true,
                        "data": ["PKUP,HKMD"]
                    },
                    deliveryBlockCodeList: {
                        type: "textcheck",
                        name: "Delivery/Credit Block",
                        placeholder: 'Enter delivery block code',
                        shortName: "Delivery Block Code",
                        addon: "creditOnHold",
                        "data": ["XX"]
                    },
                }
            }
        }
    }

    const customMock3 = {
        ...mockStoreValue, options: {
            ...mockStoreValue.options, filters: {
                ...mockStoreValue.options.filters, network: {
                    ...mockStoreValue.options.filters.network,
                    "tariffServiceCode": {
                        "type": "text",
                        "name": "Carrier Service Code",
                        "data": []
                    },
                    deliveryBlockCodeList: {
                        type: "textcheck",
                        name: "Delivery/Credit Block",
                        placeholder: 'Enter delivery block code',
                        shortName: "Delivery Block Code",
                        addon: "creditOnHold",
                        "data": []
                    },
                    liveLoad: {
                        type: "radio",
                        name: "Live Load",
                        data: "Y"
                    },
                    inboundOrderExecutionHealth: {
                        type: "checkbox",
                        name: "Health",
                        data: [
                            { name: "Unhealthy", value: "RED", checked: false },
                            { name: "Needs Attention", value: "YELLOW", checked: false },
                            { name: "Healthy", value: "GREEN", checked: false }

                        ]
                    },
                }
            }
        }
    }

    const customMock4 = {
        ...mockStoreValue, options: {
            ...mockStoreValue.options, filters: {
                ...mockStoreValue.options.filters, network: {
                    ...mockStoreValue.options.filters.network,
                    orderStatusBucket: {
                        type: "ordercheckbox",
                        name: "Order Status",
                        data: [
                            { name: "100% Confirmed Cube", value: "SO_COMPLETELY_CONFIRMED_CUBE", checked: true, parent: "Order Block Free", },
                            { name: "Less than 100% Confirmed Cube", value: "SO_NOT_COMPLETELY_CONFIRMED_CUBE", checked: false, parent: "Order Block Free", },
                            { name: "Back Orders Block Free", value: "SO_BACK_ORDER_BLOCK_FREE", checked: false, parent: "Order Block Free", },
                            { name: "Visible in TMS", value: "SO_BLOCKED_TMS_PLANNED_VISIBLE", checked: false, parent: "Orders Blocked" },
                            { name: "Not visible in TMS", value: "SO_BLOCKED_TMS_PLANNED_NOT_VISIBLE", checked: false, parent: "Orders Blocked" },
                            { name: "Pickup not scheduled", value: "SO_BLOCKED_NON_TMS_PLANNED_PICKUP_NOT_SCHEDULED", checked: false, parent: "Orders Blocked" },
                            { name: "Pickup/Package Multi Block", value: "SO_BLOCKED_NON_TMS_PLANNED_PICKUP_MULTI_BLOCK", checked: false, parent: "Orders Blocked" },
                            { name: "Immediate Action", value: "SO_BLOCKED_NON_TMS_PLANNED_IMMEDIATE_ACTION", checked: false, parent: "Orders Blocked" },
                            { name: "Back Orders Blocked", value: "SO_BACK_ORDER_BLOCKED", checked: false, parent: "Orders Blocked" },
                        ]
                    },
                    deliveryBlockCodeList: {
                        type: "textcheck",
                        name: "Delivery/Credit Block",
                        placeholder: 'Enter delivery block code',
                        shortName: "Delivery Block Code",
                        addon: "creditOnHold",
                        "data": []
                    },
                    matAvailDate: {
                        type: "date",
                        name: "Material Availability Date",
                        shortName: 'Mat.Avail Date',
                        data: {
                            startTime: moment().format("YYYY-MM-DD"),
                            endTime: moment().add(2, "days").format("YYYY-MM-DD")
                        }
                    },
                    confirmedEquivCubes: {
                        type: "checkbox",
                        name: "Confirmed Cube",
                        shortName: 'Confirmed Cube',
                        data: [
                            { name: "< 1000", value: { lte: 999.9999999 }, checked: true },
                            { name: "1000 - 2000", value: { gte: 1000, lte: 1999.9999999 }, checked: false },
                            { name: "2000 - 3000", value: { gte: 2000, lte: 2999.9999999 }, checked: false },
                            { name: "> 3000", value: { gte: 3000 }, checked: false },
                        ]
                    },
                    liveLoad: {
                        type: "radio",
                        name: "Live Load",
                        data: "N"
                    }
                }
            }
        }
    }

    it('Filter Chips should render', async () => {
        await renderWithMockStore(FIlterChips, props);
        const component = getTestElement('filterChips');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
    });

    it('Filter Chips should render', async () => {
        const { getAllByTestId } = await renderWithMockStore(FIlterChips, props, customMock1);
        const component = getTestElement('filterChips');
        expect(component).toBeInTheDocument();
        expect(getAllByTestId("chip")[0]).toBeTruthy();
    });

    it('Filter Chips should render', async () => {
        const { getAllByTestId } = await renderWithMockStore(FIlterChips, props, customMock2);
        const component = getTestElement('filterChips');
        expect(component).toBeInTheDocument();
        expect(getAllByTestId("chip")[0]).toBeTruthy();
    });

    it('Filter Chips should render', async () => {
        const { getAllByTestId } = await renderWithMockStore(FIlterChips, props, customMock3);
        const component = getTestElement('filterChips');
        expect(component).toBeInTheDocument();
        expect(getAllByTestId("chip")[0]).toBeTruthy();
    });

    it('Filter Chips should render', async () => {
        const { getAllByTestId } = await renderWithMockStore(FIlterChips, props, customMock2);
        const component = getTestElement('filterChips');
        expect(component).toBeInTheDocument();
        expect(getAllByTestId("chip")[0]).toBeTruthy();
        fireEvent.click(getAllByTestId('deleteIconChip')[0]);
    });

    it('Filter Chips should render', async () => {
        const { getAllByTestId } = await renderWithMockStore(FIlterChips, props, customMock4);
        const component = getTestElement('filterChips');
        expect(component).toBeInTheDocument();
        expect(getAllByTestId("chip")[0]).toBeTruthy();
        fireEvent.click(getAllByTestId('deleteIconChip')[0]);
    });
})


