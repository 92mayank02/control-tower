import SalesGroupSearchLisiting from '../components/header/SalesGroupSearchLisiting';
import { getTestElement, renderWithMockStore, fireEvent } from '../testUtils';

describe('<Sales Office Search Listing Component>', () => {
  afterAll(() => {
    jest.restoreAllMocks();
  })

  const props = {
    handleSalesOfficeLink:jest.fn(),
    salesGroups:{
    "open": true,
    "list": [
        "Z01",
        "Z02",
        "Z03",
        "Z04",
        "Z05",
        "Z06",
        "Z07",
        "Z08",
        "Z09",
        "Z10",
        "Z11",
        "Z12",
        "Z13",
        "Z14",
        "Z15",
        "Z16",
        "Z17",
        "Z18",
        "Z19",
        "Z20"
    ],
    "reducerObject": {
        "salesOffice": "DY01",
        "selectionType": "SALES_OFFICE",
        "salesOrg": "2821",
        "distributionChannel": "80"
    },
    "fromState": {
        "salesOffice": "DY01",
        "selectionType": "SALES_OFFICE",
        "salesOrg": "2821",
        "distributionChannel": "80"
    },
    "fromFavState": {
      "salesOffice": "DY01",
      "selectionType": "SALES_OFFICE",
      "salesOrg": "2821",
      "distributionChannel": "80"
    }
}}

const props2 = {
    handleSalesOfficeLink:jest.fn(),
    salesGroups:{
    "open": true,
    "list": [
        "Z01",
        "Z02",
        "Z03",
        "Z04",
   
    ],
    "reducerObject": {
        "salesOffice": "DY01",
        "selectionType": "SALES_OFFICE",
        "salesOrg": "2821",
        "distributionChannel": "80"
    },
    "fromState": {
        "salesOffice": "DY01",
        "selectionType": "SALES_OFFICE",
        "salesOrg": "2821",
        "distributionChannel": "80",
        "salesGroups":[
            "Z01",
            "Z02",
            "Z03",
            "Z04",
       
        ]
    },
    "fromFavState": {
      "salesOffice": "DY01",
      "selectionType": "SALES_OFFICE",
      "salesOrg": "2821",
      "distributionChannel": "80",
      "salesGroups":["Z01", "Z02","Z03", "Z04"]
    }
}}
 
  it('should render with label', async () => {
    await renderWithMockStore(SalesGroupSearchLisiting, props );
    const component = getTestElement('SGListing');
    expect(component).toBeInTheDocument();
    expect(component).toBeTruthy();
  });

  it('should render with label', async () => {
    await renderWithMockStore(SalesGroupSearchLisiting, props2 );
    const component = getTestElement('SGListing');
    expect(component).toBeInTheDocument();
    expect(component).toBeTruthy();
    const chip = getTestElement('favIcon');
    expect(chip).toBeInTheDocument();
  });

  it('Test Click', async () => {
    const { getByTestId } =  await renderWithMockStore(SalesGroupSearchLisiting, props ) 
    expect(getByTestId("ctLink")).toBeInTheDocument();
    fireEvent.click(getByTestId("ctLink"));
  });

  it('SG fav click', async () => {
    const { getAllByTestId } = await renderWithMockStore(SalesGroupSearchLisiting, props2 );
    const component = getTestElement('favIcon');
    expect(component).toBeInTheDocument();
    fireEvent.click(getAllByTestId("tickIcon")[0]);
    fireEvent.click(getAllByTestId("favIcon")[0]);
  });
});