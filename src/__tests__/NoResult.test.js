import NoResult from '../components/header/NoResult';
import { getTestElement, renderWithProvider } from '../testUtils';


describe('<No Result Component>', () => {
    afterAll(() => {
      jest.restoreAllMocks();
    });
  
    const props = {label:'TestDetails'}
    it('should render with label', async () => {
      await renderWithProvider(NoResult,props);
      const component = getTestElement('noresult');
      expect(component).toBeInTheDocument();
      expect(component).toBeTruthy();
    });

});