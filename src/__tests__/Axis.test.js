import Axis from '../components/D3Charts/Axis';
import { getTestElement, renderWithProvider } from '../testUtils';
import { scaleLinear, scaleBand } from 'd3'

describe('<Axis>', () => {
    afterAll(() => {
        jest.restoreAllMocks();
    });

    const height = 160;
    const margins = {
        top: 10,
        right: 10,
        bottom: 40,
        left: 100,
        margin: 10
    };

    const scale = scaleLinear()

    const AxisProps = {
        orient: 'Bottom',
        scale,
        translate: `translate(0, ${height - margins.bottom})`,
        tickSize: height - margins.top - margins.bottom,
        svgDimensions: {
            width: 284,
            height: 224
        },
        wrapit: false,
        horizontal: false
    }

    it('Axis Should Render With G Tag', async () => {
        await renderWithProvider(Axis, AxisProps);
        const component = getTestElement('d3axis');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
    });

    it('Axis Should Call Wrapper', async () => {
        await renderWithProvider(Axis, { ...AxisProps, wrapit: true, horizontal: false, scale: scaleBand() });
        const component = getTestElement('d3axis');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
    });

});