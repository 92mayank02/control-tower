import configureMockStore from 'redux-mock-store'
import {
    showhideFavourites,
    saveFilters,
    resetFilters,
    saveColumnState,
    resetColumnState,
    setViewBy
} from "../reduxLib/services/optionsActions"
import thunk from 'redux-thunk'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)
const store = mockStore({})
describe('Options Actions', () => {
  it('Show Hide ', () => {
    store.dispatch(showhideFavourites(
      { dummyPayload:
          {    
            "id":1,
            "title":"Example",
            "project":"Testing",
            "createdAt":"2017-03-02T23:04:38.003Z",
            "modifiedAt":"2017-03-22T16:44:29.034Z"
        }
      }
    ));
    expect(store.getActions()).toMatchSnapshot();
  });

  it('Save Filter', () => {
    store.dispatch(saveFilters(
      { dummyPayload:
          {    
            "id":1,
            "title":"Example",
            "project":"Testing",
            "createdAt":"2017-03-02T23:04:38.003Z",
            "modifiedAt":"2017-03-22T16:44:29.034Z"
        }
      }
    ));
    expect(store.getActions()).toMatchSnapshot();
  });

  
  it('Reset Filter', () => {
    store.dispatch(resetFilters(
      { dummyPayload:
          {    
            "id":1,
            "title":"Example",
            "project":"Testing",
            "createdAt":"2017-03-02T23:04:38.003Z",
            "modifiedAt":"2017-03-22T16:44:29.034Z"
        }
      }
    ));
    expect(store.getActions()).toMatchSnapshot();
  });

  const payload = {subtype:"network"}
  const stateForColumns = {"showfavs":true,"theme":"dark","filters":{"network":{"sites":{"type":"sites","name":"Origin","data":null},"searchStringList":{"type":"text","name":"Search","data":null},"materialNumberList":{"type":"text","name":"Material ID","data":null},"destName":{"type":"text","name":"Customer","data":null},"destState":{"type":"text","name":"Destination State","data":null},"destCity":{"type":"text","name":"Destination City","data":null},"orderStatusBucket":{"type":"checkbox","name":"Order Status","data":[{"name":"Order Block Free","value":"ORDER_BLOCK_FREE","checked":false},{"name":"Order Blocked","value":"ORDER_BLOCKED","checked":false}]},"orderTypes":{"type":"checkbox","name":"Order Type","data":[{"name":"Cust. Order","value":"CUST","checked":false},{"name":"STO","value":"STO","checked":false}]},"tariffServiceCodeList":{"type":"text","name":"Carrier Service Code","data":null},"orderExecutionBucket":{"type":"checkbox","name":"Shipment Status","data":[{"name":"Transportation to be planned","value":"TRANS_PLAN_UNASSIGNED","checked":false},{"name":"Transportation planning","value":"TRANS_PLAN_OPEN","checked":false},{"name":"Transportation processing","value":"TRANS_PLAN_PROCESSING","checked":false},{"name":"Transportation Carrier committed","value":"TRANS_PLAN_TENDER_ACCEPTED","checked":false},{"name":"Shipment Created","value":"SHIPMENT_CREATED","checked":false},{"name":"Checked in by DC","value":"CHECK_IN","checked":false},{"name":"Loading Started","value":"LOADING_STARTED","checked":false},{"name":"Ready for Pickup","value":"TRANS_EXEC_READY_PICK_UP","checked":false},{"name":"In Transit","value":"TRANS_EXEC_IN_TRANSIT","checked":false},{"name":"Delivery confirmed","value":"TRANS_EXEC_DELIVERY_CONFIRMED","checked":false}]},"loadReadyDateTimeOriginTZ":{"type":"date","dummytime":true,"timeformat":"T","startTime":"[ 00:00:00.000]","endTime":"[ 23:59:59.999]","name":"Carrier Ready Date","shortName":"Carr. Ready Date","data":null}}},"columns":{"network":[{"title":"Dest City","field":"destinationCity","tableData":{"columnOrder":0,"groupSort":"asc","width":"calc((100% - (0px)) / 12)","id":0},"hidden":false},{"title":"Order Status","field":"orderStatus","cellStyle":{"minWidth":160},"tableData":{"columnOrder":1,"groupSort":"asc","width":"calc((100% - (0px)) / 12)","id":1},"hidden":false},{"title":"Order Type","field":"orderType","tableData":{"columnOrder":2,"groupSort":"asc","width":"calc((100% - (0px)) / 12)","id":2},"hidden":false},{"title":"Shipment Status","field":"orderExecutionBucketDesc","cellStyle":{"minWidth":160},"tableData":{"columnOrder":3,"groupSort":"asc","width":"calc((100% - (0px)) / 12)","id":3},"hidden":false},{"title":"Shipment #","field":"shipmentNum","cellStyle":{"minWidth":160},"tableData":{"columnOrder":4,"groupSort":"asc","width":"calc((100% - (0px)) / 12)","id":4},"hidden":false},{"title":"Order #","field":"orderNum","cellStyle":{"minWidth":160},"tableData":{"columnOrder":5,"groupSort":"asc","width":"calc((100% - (0px)) / 12)","id":5},"hidden":false},{"title":"Customer","field":"customer","tableData":{"columnOrder":6,"groupSort":"asc","width":"calc((100% - (0px)) / 12)","id":6},"hidden":false},{"title":"Origin","field":"origin","cellStyle":{"minWidth":160},"tableData":{"columnOrder":7,"groupSort":"asc","width":"calc((100% - (0px)) / 12)","id":7},"hidden":false},{"title":"Dest State","field":"destinationState","tableData":{"columnOrder":8,"groupSort":"asc","width":"calc((100% - (0px)) / 12)","id":8},"hidden":false},{"title":"Carrier Ready Date & Time","field":"loadReadyDateTime","cellStyle":{"minWidth":160},"tableData":{"columnOrder":9,"groupSort":"asc","width":"calc((100% - (0px)) / 12)","id":9},"hidden":false},{"title":"Carrier","field":"tariffServiceCode","tableData":{"columnOrder":10,"groupSort":"asc","width":"calc((100% - (0px)) / 12)","id":10},"hidden":false},{"title":"Ship Condition","field":"shippingCondition","tableData":{"columnOrder":11,"groupSort":"asc","width":"calc((100% - (0px)) / 12)","id":11},"hidden":false}]}}

  it('Reset Columns', () => {
    store.dispatch(resetColumnState(payload));
    expect(store.getActions()).toMatchSnapshot();
  });

  it('Save Columns', () => {
    store.dispatch(saveColumnState({columns:stateForColumns.columns.network, subtype:'network'}));
    expect(store.getActions()).toMatchSnapshot();
  });

  it('Set View By Action', () => {
    store.dispatch(setViewBy("customer"));
    expect(store.getActions()).toMatchSnapshot();
  });
  
});