import selectionsReducer from "../reduxLib/reducers/selectionsReducer";
import { checklistConstants } from '../reduxLib/constants';
describe("Get Fav Unfav Reducer Functions",() => {

    afterEach(() => {
        jest.clearAllMocks();
    });


    const successPayload1 = [
        {"siteNum":"2047", isFav: true, "region":"NA","type":"DC","plantType":"RDC","alias":"LADC","shortName":"Los Angeles DC","siteName":"S_2047_917","siteNameDesc":"Los Angeles DC ExtOps/DC","location":{"id":"2047","name":"Los Angeles DC","city":"ONTARIO","state":"CA","country":"USA","street":"4815 S HELLMAN AVE","geoLocation":{"longitude":-117.602477,"latitude":34.040893},"timezone":"US/Pacific","postalCode":"91762"}},
        {"siteNum":"2031","region":"NA","type":"MILL","plantType":"KCNA Mill","alias":"NMC","shortName":"New Milford Mill","siteName":"S_2031_067","siteNameDesc":"New Milford Global Sales Mill","location":{"id":"2031","name":"New Milford Mill","city":"NEW MILFORD","state":"CT","country":"USA","street":"58 PICKETT DIST ROAD","geoLocation":{"longitude":-73.4111248,"latitude":41.5579034},"timezone":"US/Eastern","postalCode":"06776-4413"}}
    ]

    const successPayload2 = [
        {"siteNum":"2047","region":"NA","type":"DC","plantType":"RDC","alias":"LADC","shortName":"Los Angeles DC","siteName":"S_2047_917","siteNameDesc":"Los Angeles DC ExtOps/DC","location":{"id":"2047","name":"Los Angeles DC","city":"ONTARIO","state":"CA","country":"USA","street":"4815 S HELLMAN AVE","geoLocation":{"longitude":-117.602477,"latitude":34.040893},"timezone":"US/Pacific","postalCode":"91762"}},
    ]

    const successFavPayload2 = [
        {"siteNum":"2047","region":"NA","type":"DC","plantType":"RDC","alias":"LADC","shortName":"Los Angeles DC","siteName":"S_2047_917","siteNameDesc":"Los Angeles DC ExtOps/DC","location":{"id":"2047","name":"Los Angeles DC","city":"ONTARIO","state":"CA","country":"USA","street":"4815 S HELLMAN AVE","geoLocation":{"longitude":-117.602477,"latitude":34.040893},"timezone":"US/Pacific","postalCode":"91762"},"isFav":true},
    ]
    const successPayload3 = [
        {"siteNum":"2031","region":"NA","type":"MILL","plantType":"KCNA Mill","alias":"NMC","shortName":"New Milford Mill","siteName":"S_2031_067","siteNameDesc":"New Milford Global Sales Mill","location":{"id":"2031","name":"New Milford Mill","city":"NEW MILFORD","state":"CT","country":"USA","street":"58 PICKETT DIST ROAD","geoLocation":{"longitude":-73.4111248,"latitude":41.5579034},"timezone":"US/Eastern","postalCode":"06776-4413"}}
    ]

    const succesPayloadCuso = {
        "CUST": [
            {
                "customerName": "AMAZON CA INC",
                "selectionType": "CUST",
                "distributionChannel": "80",
                "salesOrg": "2821"
            },
            {
                "customerName": "AMAZON CA INC",
                "selectionType": "CUST",
                "distributionChannel": "80",
                "salesOrg": "2820"
            },
            {
                "customerName": "AMAZON VENDOR FLEX 2.0",
                "selectionType": "CUST",
                "distributionChannel": "80",
                "salesOrg": "2810"
            },
            {
                "customerName": "AMAZON.COM EDI",
                "selectionType": "CUST",
                "distributionChannel": "80",
                "salesOrg": "2811"
            },
            {
                "customerName": "AMAZON.COM INC",
                "selectionType": "CUST",
                "distributionChannel": "80",
                "salesOrg": "2811"
            },
            {
                "customerName": "AMAZON.COM INC",
                "selectionType": "CUST",
                "distributionChannel": "80",
                "salesOrg": "2810"
            }
        ],
        "SALES_OFFICE": [
            {
                "salesOffice": "DY01",
                "selectionType": "SALES_OFFICE",
                "salesOrg": "2821",
                "distributionChannel": "80"
            },
            {
                "salesOffice": "KP02",
                "selectionType": "SALES_OFFICE",
                "salesOrg": "2821",
                "distributionChannel": "80"
            },
            {
                "salesOffice": "KH10",
                "selectionType": "SALES_OFFICE",
                "salesOrg": "2821",
                "distributionChannel": "80"
            },
            {
                "salesOffice": "KP06",
                "selectionType": "SALES_OFFICE",
                "salesOrg": "2821",
                "distributionChannel": "80"
            }
        ]
    }

    const successRemovedCUSO ={
        "CUST": [
            {
                "customerName": "AMAZON CA INC",
                "selectionType": "CUST",
                "distributionChannel": "80",
                "salesOrg": "2821"
            },
            {
                "customerName": "AMAZON CA INC",
                "selectionType": "CUST",
                "distributionChannel": "80",
                "salesOrg": "2820"
            },
            {
                "customerName": "AMAZON VENDOR FLEX 2.0",
                "selectionType": "CUST",
                "distributionChannel": "80",
                "salesOrg": "2810"
            },
            {
                "customerName": "AMAZON.COM EDI",
                "selectionType": "CUST",
                "distributionChannel": "80",
                "salesOrg": "2811"
            },
            {
                "customerName": "AMAZON.COM INC",
                "selectionType": "CUST",
                "distributionChannel": "80",
                "salesOrg": "2811"
            },
            {
                "customerName": "AMAZON.COM INC",
                "selectionType": "CUST",
                "distributionChannel": "80",
                "salesOrg": "2810"
            }
        ],
        "SALES_OFFICE": [

            {
                "salesOffice": "KP02",
                "selectionType": "SALES_OFFICE",
                "salesOrg": "2821",
                "distributionChannel": "80"
            },
            {
                "salesOffice": "KH10",
                "selectionType": "SALES_OFFICE",
                "salesOrg": "2821",
                "distributionChannel": "80"
            },
            {
                "salesOffice": "KP06",
                "selectionType": "SALES_OFFICE",
                "salesOrg": "2821",
                "distributionChannel": "80"
            }
        ]
    }


    const successRemovedCUSO2 ={
        "CUST": [
            {
                "customerName": "AMAZON CA INC",
                "selectionType": "CUST",
                "distributionChannel": "80",
                "salesOrg": "2820"
            },
            {
                "customerName": "AMAZON VENDOR FLEX 2.0",
                "selectionType": "CUST",
                "distributionChannel": "80",
                "salesOrg": "2810"
            },
            {
                "customerName": "AMAZON.COM EDI",
                "selectionType": "CUST",
                "distributionChannel": "80",
                "salesOrg": "2811"
            },
            {
                "customerName": "AMAZON.COM INC",
                "selectionType": "CUST",
                "distributionChannel": "80",
                "salesOrg": "2811"
            },
            {
                "customerName": "AMAZON.COM INC",
                "selectionType": "CUST",
                "distributionChannel": "80",
                "salesOrg": "2810"
            }
        ],
        "SALES_OFFICE": [

            {
                "salesOffice": "KP02",
                "selectionType": "SALES_OFFICE",
                "salesOrg": "2821",
                "distributionChannel": "80"
            },
            {
                "salesOffice": "KH10",
                "selectionType": "SALES_OFFICE",
                "salesOrg": "2821",
                "distributionChannel": "80"
            },
            {
                "salesOffice": "KP06",
                "selectionType": "SALES_OFFICE",
                "salesOrg": "2821",
                "distributionChannel": "80"
            }
        ]
    }

    const addCusoSuccess = {
        "CUST": [],
        "SALES_OFFICE": [{
            "salesOffice": "KP02",
            "selectionType": "SALES_OFFICE",
            "salesOrg": "2821",
            "distributionChannel": "90"
        }]
    }

    const newItem = {"siteNum":"2508","region":"NA","type":"DC","plantType":"RDC","alias":"NEDC","shortName":"Northeast DC","siteName":"S_2508_195","siteNameDesc":"Northeast Distribution Center (NEDC)","location":{"id":"2508","name":"Northeast DC","city":"SHOEMAKERSVILLE","state":"PA","country":"USA","street":"211 LOGISTICS DRIVE","geoLocation":{"longitude":-75.966777,"latitude":40.522329},"timezone":"US/Eastern","postalCode":"19555"},"isFav":false}

    const newSO = {
        "customerName": "AMAZON DVS",
        "selectionType": "CUST",
        "distributionChannel": "80",
        "salesOrg": [
            "2810"
        ]
    }
    const removeSO = {
        "salesOffice": "DY01",
        "selectionType": "SALES_OFFICE",
        "salesOrg": "2821",
        "distributionChannel": "80"
    }
    
    const defaultState = { items: [], cuso:{CUST:[],SALES_OFFICE:[]}, stopfavload: true, "blockOnTabChange": false }

    const defaultPopulatedState = {...defaultState, items:successPayload1, cuso:succesPayloadCuso}
    const defaultPopulatedState2 = {...defaultState, cuso:addCusoSuccess}

    it("Default Switch Return", async () => {
        expect(selectionsReducer(undefined,{type:"Action not listed", payload: defaultState})).toStrictEqual(defaultState);        
    });
    it("Add item Success Reducer", async () => {
        expect(selectionsReducer(undefined,{
            type: checklistConstants.ADD_ITEM_SUCCESS,
            payload: newItem,
        })).toStrictEqual({ 
            ...defaultState, items: [newItem]
        });
        expect(selectionsReducer(defaultPopulatedState,{
            type: checklistConstants.ADD_ITEM_SUCCESS,
            payload: newItem,
        })).toStrictEqual({ 
            ...defaultPopulatedState, items: [...successPayload1,newItem]
        });            
    });

    it("Add Cu and SO Success Reducer", async () => {
        expect(selectionsReducer(undefined,{
            type: checklistConstants.ADD_CUSO_SUCCESS,
            payload: newSO,
        })).toStrictEqual({ 
            ...defaultState, cuso: {...defaultState.cuso, CUST:[{
                "customerName": "AMAZON DVS",
                "selectionType": "CUST",
                "distributionChannel": "80",
                "salesOrg":"2810"
            }]}
        });           
    });

    it("Add Cu and SO Success Reducer - no selectiontype", async () => {
        expect(selectionsReducer(defaultState,{
            type: checklistConstants.ADD_CUSO_SUCCESS,
            payload: {
                "salesOffice": "DVS",
                "selectionType": "",
                "distributionChannel": "80",
                "salesOrg":"2810"
            },
        })).toStrictEqual({ 
            ...defaultState, cuso: {CUST: [], SALES_OFFICE: []}
        });           
    });

    it("Add Cu and SO Success Reducer part2", async () => {
        expect(selectionsReducer(undefined,{
            type: checklistConstants.ADD_CUSO_SUCCESS,
            payload: {
                "salesOffice": "DVS",
                "selectionType": "SALES_OFFICE",
                "distributionChannel": "80",
                "salesOrg":"2810"
            },
        })).toStrictEqual({ 
            ...defaultState, cuso: {...defaultState.cuso, SALES_OFFICE:[{
                "salesOffice": "DVS",
                "selectionType": "SALES_OFFICE",
                "distributionChannel": "80",
                "salesOrg":"2810"
            }]}
        });           
    });

    it("Add Cu and SO Success Reducer part3", async () => {
        expect(selectionsReducer(defaultPopulatedState2,{
            type: checklistConstants.ADD_CUSO_SUCCESS,
            payload: {
                "salesOffice": "KP02",
                "selectionType": "SALES_OFFICE",
                "distributionChannel": "80",
                "salesOrg":"2821"
            },
        })).toStrictEqual({ 
            ...defaultPopulatedState2, cuso: {...defaultPopulatedState2.cuso, SALES_OFFICE:[{
                "salesOffice": "KP02",
                "selectionType": "SALES_OFFICE",
                "distributionChannel": "80",
                "salesOrg":"2821"
            }]}
        });           
    });

    it("Add multiple Cu and SO Success Reducer", async () => {
        expect(selectionsReducer(undefined,{
            type: checklistConstants.ADD_CUSO_MULTIPLE_SUCCESS,
            payload: {CUST: [newSO], SALES_OFFICE: []},
        })).toStrictEqual({ 
            ...defaultState, cuso: {...defaultState.cuso, CUST:[{
                "customerName": "AMAZON DVS",
                "selectionType": "CUST",
                "distributionChannel": "80",
                "salesOrg":["2810"]
            }], SALES_OFFICE: []}
        });           
    });

    it("Add Multiple items Success Reducer", async () => {
        expect(selectionsReducer(undefined,{
            type: checklistConstants.ADD_ITEM_MULTIPLE_SUCCESS,
            payload: [...successPayload1,newItem],
        })).toStrictEqual({ 
            ...defaultState, items: [...successPayload1,newItem]
        });
        expect(selectionsReducer(defaultPopulatedState,{
            type: checklistConstants.ADD_ITEM_MULTIPLE_SUCCESS,
            payload: [...successPayload2,newItem],
        })).toStrictEqual({ 
            ...defaultPopulatedState, items: [...successPayload1,newItem]
        });            
    });


    it("Set Items on load selections", async () => {

        expect(selectionsReducer(defaultPopulatedState,{
            type: checklistConstants.SET_ONLOAD_SELECTIONS,
            payload: {locations:[...successPayload2,newItem], cuso:{CUST: [newSO], SALES_OFFICE: []}},
        })).toStrictEqual({ 
            ...defaultPopulatedState, items: [...successPayload1,newItem], cuso:{CUST: [newSO], SALES_OFFICE: []}, stopfavload: false
        });  
    })

    it("Remove item Success Reducer", async () => {
        expect(selectionsReducer(defaultPopulatedState,{
            type: checklistConstants.REMOVE_ITEM_SUCCESS,
            payload: successPayload2[0],
        })).toStrictEqual({ 
            ...defaultPopulatedState, items: [...successPayload3]
        });            
    });

    
    it("Remove CUSO Success Reducer", async () => {
        expect(selectionsReducer(defaultPopulatedState,{
            type: checklistConstants.REMOVE_CUSO_SUCCESS,
            payload: removeSO,
        })).toStrictEqual({ 
            ...defaultPopulatedState, cuso: successRemovedCUSO

        });            
    });

    it("Remove CUSO Success Reducer part2", async () => {
        expect(selectionsReducer(defaultState,{
            type: checklistConstants.REMOVE_CUSO_SUCCESS,
            payload: {
                "salesOffice": "DY01",
                "selectionType": "",
                "salesOrg": "2821",
                "distributionChannel": "80"
        
            },
        })).toStrictEqual({ 
            ...defaultState, cuso:{CUST:[],SALES_OFFICE:[]}

        });            
    });

    it("Remove CUSO Success Reducer || ", async () => {
        expect(selectionsReducer({ ...defaultPopulatedState, cuso: successRemovedCUSO },{
            type: checklistConstants.REMOVE_CUSO_SUCCESS,
            payload:{
                "customerName": "AMAZON CA INC",
                "selectionType": "CUST",
                "distributionChannel": "80",
                "salesOrg": "2821"
            },
        })).toStrictEqual({ ...defaultPopulatedState, cuso: successRemovedCUSO2 });            
    });

    it("Remove Fav item Success Reducer", async () => {
        expect(selectionsReducer(defaultPopulatedState,{
            type: checklistConstants.REMOVE_FAV_ITEMS,
            payload: successFavPayload2[0],
        })).toStrictEqual({ 
            ...defaultPopulatedState, items: [successPayload1[1]]
        });            
    });
    it("Clear all Reducer", async () => {
        expect(selectionsReducer(defaultPopulatedState,{
            type: checklistConstants.CLEAR_ALL,
            payload: null,
        })).toStrictEqual({ 
            ...defaultPopulatedState, items:[]
        });            
    });

    it("Clear all  CUSO Reducer", async () => {
        expect(selectionsReducer(defaultPopulatedState,{
            type: checklistConstants.CLEAR_CUSO,
            payload: null,
        })).toStrictEqual({ 
            ...defaultPopulatedState,  cuso:{CUST:[],SALES_OFFICE:[]}
        });            
    });

    it("Add SG SG Success Reducer", async () => {

        const prevState = {...defaultState, cuso:{
            "CUST": [
                {
                    "customerName": "AMAZON DVS",
                    "selectionType": "CUST",
                    "distributionChannel": "80",
                    "salesOrg": "2810"
                },
                {
                    "customerName": "AMAZON CA INC",
                    "selectionType": "CUST",
                    "distributionChannel": "80",
                    "salesOrg": "2821"
                },
                {
                    "customerName": "AMAZON CA INC",
                    "selectionType": "CUST",
                    "distributionChannel": "80",
                    "salesOrg": "2820"
                },
                {
                    "customerName": "AMAZON VENDOR FLEX 2.0",
                    "selectionType": "CUST",
                    "distributionChannel": "80",
                    "salesOrg": "2810"
                },
                {
                    "customerName": "AMAZON.COM EDI",
                    "selectionType": "CUST",
                    "distributionChannel": "80",
                    "salesOrg": "2811"
                },
                {
                    "customerName": "AMAZON.COM INC",
                    "selectionType": "CUST",
                    "distributionChannel": "80",
                    "salesOrg": "2811"
                },
                {
                    "customerName": "AMAZON.COM INC",
                    "selectionType": "CUST",
                    "distributionChannel": "80",
                    "salesOrg": "2810"
                }
            ],
            "SALES_OFFICE": [
                {
                    "salesOffice": "DY01",
                    "selectionType": "SALES_OFFICE",
                    "salesOrg": "2821",
                    "distributionChannel": "80",
                    "salesGroup": [
                        "Z01"
                    ]
                },
                {
                    "salesOffice": "KP02",
                    "selectionType": "SALES_OFFICE",
                    "salesOrg": "2821",
                    "distributionChannel": "80"
                },
                {
                    "salesOffice": "KH10",
                    "selectionType": "SALES_OFFICE",
                    "salesOrg": "2821",
                    "distributionChannel": "80"
                },
                {
                    "salesOffice": "KP06",
                    "selectionType": "SALES_OFFICE",
                    "salesOrg": "2821",
                    "distributionChannel": "80"
                }
            ]
        }}
        expect(selectionsReducer(prevState,{
            type: checklistConstants.ADD_SG_SUCCESS,
            payload: {
                "shortName": "Z04",
                "reducerObject": {
                    "salesOffice": "DY01",
                    "selectionType": "SALES_OFFICE",
                    "salesOrg": "2821",
                    "distributionChannel": "80"
                }
            },
        })).toStrictEqual({ 
            ...defaultState, cuso: {
                "CUST": [
                    {
                        "customerName": "AMAZON DVS",
                        "selectionType": "CUST",
                        "distributionChannel": "80",
                        "salesOrg": "2810"
                    },
                    {
                        "customerName": "AMAZON CA INC",
                        "selectionType": "CUST",
                        "distributionChannel": "80",
                        "salesOrg": "2821"
                    },
                    {
                        "customerName": "AMAZON CA INC",
                        "selectionType": "CUST",
                        "distributionChannel": "80",
                        "salesOrg": "2820"
                    },
                    {
                        "customerName": "AMAZON VENDOR FLEX 2.0",
                        "selectionType": "CUST",
                        "distributionChannel": "80",
                        "salesOrg": "2810"
                    },
                    {
                        "customerName": "AMAZON.COM EDI",
                        "selectionType": "CUST",
                        "distributionChannel": "80",
                        "salesOrg": "2811"
                    },
                    {
                        "customerName": "AMAZON.COM INC",
                        "selectionType": "CUST",
                        "distributionChannel": "80",
                        "salesOrg": "2811"
                    },
                    {
                        "customerName": "AMAZON.COM INC",
                        "selectionType": "CUST",
                        "distributionChannel": "80",
                        "salesOrg": "2810"
                    }
                ],
                "SALES_OFFICE": [
                    {
                        "salesOffice": "DY01",
                        "selectionType": "SALES_OFFICE",
                        "salesOrg": "2821",
                        "distributionChannel": "80",
                        "salesGroup": [
                            "Z01",
                            "Z04"
                        ]
                    },
                    {
                        "salesOffice": "KP02",
                        "selectionType": "SALES_OFFICE",
                        "salesOrg": "2821",
                        "distributionChannel": "80"
                    },
                    {
                        "salesOffice": "KH10",
                        "selectionType": "SALES_OFFICE",
                        "salesOrg": "2821",
                        "distributionChannel": "80"
                    },
                    {
                        "salesOffice": "KP06",
                        "selectionType": "SALES_OFFICE",
                        "salesOrg": "2821",
                        "distributionChannel": "80"
                    }
                ]
            }})
    })

    it("Add SG SG Success Reducer part2", async () => {

        const prevState = {...defaultState, cuso:{
            "CUST": [
                {
                    "customerName": "AMAZON DVS",
                    "selectionType": "CUST",
                    "distributionChannel": "80",
                    "salesOrg": "2810"
                }
            ],
            "SALES_OFFICE": [
                {
                    "salesOffice": "DY01",
                    "selectionType": "SALES_OFFICE",
                    "salesOrg": "2821",
                    "distributionChannel": "80",
                    "salesGroup": [
                        "Z01"
                    ]
                }
            ]
        }}
        expect(selectionsReducer(prevState,{
            type: checklistConstants.ADD_SG_SUCCESS,
            payload: {
                "shortName": "Z04",
                "reducerObject": {
                    "salesOffice": "DY03",
                    "selectionType": "SALES_OFFICE",
                    "salesOrg": "2821",
                    "distributionChannel": "90"
                }
            },
        })).toStrictEqual({ 
            ...defaultState, cuso: {
                "CUST": [
                    {
                        "customerName": "AMAZON DVS",
                        "selectionType": "CUST",
                        "distributionChannel": "80",
                        "salesOrg": "2810"
                    }
                ],
                "SALES_OFFICE": [
                    {
                        "salesOffice": "DY01",
                        "selectionType": "SALES_OFFICE",
                        "salesOrg": "2821",
                        "distributionChannel": "80",
                        "salesGroup": ["Z01"]
                    },
                    {
                        "salesOffice": "DY03",
                        "selectionType": "SALES_OFFICE",
                        "salesOrg": "2821",
                        "distributionChannel": "90",
                        "salesGroup": ["Z04"]
                    }
                ]
            }})
    })

    it("rEMOVE SG SG Success Reducer p1", async () => {

        const prevState = {...defaultState, cuso:{
            "CUST": [],
            "SALES_OFFICE": [
                {
                    "salesOffice": "DY01",
                    "selectionType": "SALES_OFFICE",
                    "salesOrg": "2821",
                    "distributionChannel": "80",
                    "salesGroup": [
                        "Z01",
                        "Z04"
                    ]
                }
            ]
        }}
        expect(selectionsReducer(prevState,{
            type: checklistConstants.REMOVE_SG_SUCCESS,
            payload: {
                "shortName": "Z04",
                "reducerObject": {
                    "salesOffice": "DY01",
                    "selectionType": "SALES_OFFICE",
                    "salesOrg": "2821",
                    "distributionChannel": "80"
                }
            }
        })).toStrictEqual({ 
            ...defaultState, cuso: {
                "CUST": [],
                "SALES_OFFICE": [
                    {
                        "salesOffice": "DY01",
                        "selectionType": "SALES_OFFICE",
                        "salesOrg": "2821",
                        "distributionChannel": "80",
                        "salesGroup": [
                            "Z01",
                        ]
                    }
                ]
            }})
    })

    it("rEMOVE SG SG Success Reducer p2", async () => {

        const prevState = {...defaultState, cuso:{
            "CUST": [],
            "SALES_OFFICE": [
                {
                    "salesOffice": "DY01",
                    "selectionType": "SALES_OFFICE",
                    "salesOrg": "2821",
                    "distributionChannel": "80",
                    "salesGroup": [
                        "Z04",
                    ]
                }
            ]
        }}
        expect(selectionsReducer(prevState,{
            type: checklistConstants.REMOVE_SG_SUCCESS,
            payload: {
                "shortName": "Z04",
                "reducerObject": {
                    "salesOffice": "DY01",
                    "selectionType": "SALES_OFFICE",
                    "salesOrg": "2821",
                    "distributionChannel": "80"
                }
            }
        })).toStrictEqual({ 
            ...defaultState, cuso: {
                "CUST": [],
                "SALES_OFFICE": [
                    {
                        "salesOffice": "DY01",
                        "selectionType": "SALES_OFFICE",
                        "salesOrg": "2821",
                        "distributionChannel": "80",
                    }
                ]
            }})
    })
    
    it("rEMOVE SG SG Success Reducer1", async () => {

        const prevState = {...defaultState, cuso:{
            "CUST": [],
            "SALES_OFFICE": [
                {
                    "salesOffice": "DY01",
                    "selectionType": "SALES_OFFICE",
                    "salesOrg": "2821",
                    "distributionChannel": "80",
                    "salesGroup": [
                        "Z01",
                        "Z04"
                    ]
                },
                {
                    "salesOffice": "DY02",
                    "selectionType": "SALES_OFFICE",
                    "salesOrg": "2821",
                    "distributionChannel": "80",
                }
            ]
        }}
        expect(selectionsReducer(prevState,{
            type: checklistConstants.REMOVE_SG_SUCCESS,
            payload: {
                "shortName": "Z04",
                "reducerObject": {
                    "salesOffice": "DY01",
                    "selectionType": "SALES_OFFICE",
                    "salesOrg": "2821",
                    "distributionChannel": "80"
                },
            }
        })).toStrictEqual({ 
            ...defaultState, cuso: {
                "CUST": [],
                "SALES_OFFICE": [
                    {
                        "salesOffice": "DY01",
                        "selectionType": "SALES_OFFICE",
                        "salesOrg": "2821",
                        "distributionChannel": "80",
                        "salesGroup": [
                            "Z01",
                        ]
                    },
                    {
                        "salesOffice": "DY02",
                        "selectionType": "SALES_OFFICE",
                        "salesOrg": "2821",
                        "distributionChannel": "80",
                    }
                ]
            }})
    })

    it("Change Tab Reducer", async () => {
        expect(selectionsReducer(defaultPopulatedState,{
            type: checklistConstants.CHANGE_TAB,
            payload: {
                "siteNum": "2028",
                "region": "NA",
                "type": "MILL",
                "plantType": "KCNA Mill",
                "alias": "BIL",
                "shortName": "Beech Island Mill",
                "siteName": "S_2028_298",
                "siteNameDesc": "Beech Island Global Sales Mill",
                "businessUnits": [
                    "CONSUMER",
                    "PROFESSIONAL"
                ],
                "location": {
                    "id": "2028",
                    "name": "Beech Island Mill",
                    "city": "BEECH ISLAND",
                    "state": "SC",
                    "country": "USA",
                    "street": "246 OLD JACKSON HIGHWAY",
                    "geoLocation": {
                        "longitude": -81.8986332,
                        "latitude": 33.4163503
                    },
                    "timezone": "US/Eastern",
                    "postalCode": "29842"
                }
            },
        })).toStrictEqual({ 
            ...defaultPopulatedState,
            blockOnTabChange: true, 
            items:[...successPayload1,{
                "siteNum": "2028",
                "region": "NA",
                "type": "MILL",
                "plantType": "KCNA Mill",
                "alias": "BIL",
                "shortName": "Beech Island Mill",
                "siteName": "S_2028_298",
                "siteNameDesc": "Beech Island Global Sales Mill",
                "businessUnits": [
                    "CONSUMER",
                    "PROFESSIONAL"
                ],
                "location": {
                    "id": "2028",
                    "name": "Beech Island Mill",
                    "city": "BEECH ISLAND",
                    "state": "SC",
                    "country": "USA",
                    "street": "246 OLD JACKSON HIGHWAY",
                    "geoLocation": {
                        "longitude": -81.8986332,
                        "latitude": 33.4163503
                    },
                    "timezone": "US/Eastern",
                    "postalCode": "29842"
                }
            }]
        });            
    });

    it("Change Tab Reducer", async () => {
        expect(selectionsReducer(defaultPopulatedState,{
            type: checklistConstants.SET_BLOCKONTABCHANGE,
        })).toStrictEqual({ 
            ...defaultPopulatedState,
            blockOnTabChange: false });
    })

    it("Clear Selectd Reducer", async () => {
        expect(selectionsReducer(defaultPopulatedState,{
            type: checklistConstants.CLEAR_SELECTED,
            payload: successPayload1,
        })).toStrictEqual({ 
            ...defaultPopulatedState, items: []
        });            
    });

    it("Remove Fav CUso", async () => {
        expect(selectionsReducer(defaultPopulatedState,{
            type: checklistConstants.REMOVE_FAV_CUSO,
            payload: [],
        })).toStrictEqual({ 
            ...defaultPopulatedState
        });            
    });

    it("Join Fav CUso", async () => {
        expect(selectionsReducer(defaultPopulatedState,{
            type: checklistConstants.JOIN_FAVS_AND_SELECTIONS_CUSO,
            payload: [],
        })).toStrictEqual({ 
            ...defaultPopulatedState
        });            
    });

})