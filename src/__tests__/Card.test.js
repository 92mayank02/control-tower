import KCCard from '../components/common/Card';
import { getTestElement, renderWithProvider } from '../testUtils';


describe('<KCCard>', () => {
    afterAll(() => {
      jest.restoreAllMocks();
    });
  
    it('Without Actions', async () => {
        var CardProps = {children:'TestDetails',headertext:"Test Basic Card"}
        await renderWithProvider(KCCard,CardProps);
        const component = getTestElement('cardbasic');
        expect(component.className).toMatch(/(makeStyles-addPadding-2)/i);
    });
    it('With Actions', async () => {
        var CardProps = {children:'TestDetails',headertext:"Test Basic Card", actions:jest.fn()}
        await renderWithProvider(KCCard,CardProps);
        const component = getTestElement('cardbasic');
        expect(component.className).not.toMatch(/(makeStyles-addPadding-2)/i);
    });

});