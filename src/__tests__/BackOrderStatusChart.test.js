import { BackOrderDetailsChartBuilder } from 'components/common/BackOrderDetailsChartBuilder';
import { useEvent } from 'react-use';
import { getTestElement, renderWithProvider, userEvent, fireEvent, waitFor } from '../testUtils';


describe('<BackOrderChart Component>', () => {

    afterAll(() => {
        jest.restoreAllMocks();
    });

    const props = {
        chartsData: [
                    {
                        "state": "SO_BACK_ORDER_BLOCKED",
                        "stateDesc": "Orders With Block",
                        "totalCount": 200,
                        "greenCount": 400
                    },
                    {
                        "state": "SO_BACK_ORDER_BLOCK_FREE",
                        "stateDesc": "Orders Block Free",
                        "totalCount": 200,
                        "greenCount": 600
                    }, 
                ],
        setDetails: jest.fn()
    }

    it('Back Order Chart Should Render should render', async () => {
        await renderWithProvider(BackOrderDetailsChartBuilder, props);
        const component = getTestElement('backderorderchart');
        expect(component).toBeTruthy();
    });

    it('Hyperlink for Orders with block', async () => {
        const { getByText } = await renderWithProvider(BackOrderDetailsChartBuilder, props);
        fireEvent.mouseOver(getByText('400'));
        await waitFor(() => getByText('Orders With Block : 400'))
        expect(getByText('Orders With Block : 400')).toBeInTheDocument()
        userEvent.click(getByText('Orders With Block : 400'))
    });

    it('Hyperlink for Block free orders', async () => {
        const { getByText } = await renderWithProvider(BackOrderDetailsChartBuilder, props);
        fireEvent.mouseOver(getByText('600'));
        await waitFor(() => getByText('Orders Block Free : 600'))
        expect(getByText('Orders Block Free : 600')).toBeInTheDocument()
        userEvent.click(getByText('Orders Block Free : 600'))
    });

    it('Back Order Chart Should Render should render - ( Undefined Chart Data )', async () => {
        const emptyChartsData = {...props, chartsData :["totalEntities"]};
        await renderWithProvider(BackOrderDetailsChartBuilder, emptyChartsData);
        const component = getTestElement('backderorderchart');
        expect(component).toBeTruthy();
    });

    it('Hyperlink for Orders with block - SetDetails Not a function', async () => {
        const emptyChartsData = { ...props, setDetails:[]};
        const { getByText } = await renderWithProvider(BackOrderDetailsChartBuilder, emptyChartsData);
        fireEvent.mouseOver(getByText('400'));
        await waitFor(() => getByText('Orders With Block : 400'))
        expect(getByText('Orders With Block : 400')).toBeInTheDocument()
        userEvent.click(getByText('Orders With Block : 400'))
    });

    it('Hyperlink for Block free orders - SetDetails Not a function', async () => {
        const emptyChartsData = { ...props, setDetails:[] };
        const { getByText } = await renderWithProvider(BackOrderDetailsChartBuilder, emptyChartsData);
        fireEvent.mouseOver(getByText('600'));
        await waitFor(() => getByText('Orders Block Free : 600'))
        expect(getByText('Orders Block Free : 600')).toBeInTheDocument()
        userEvent.click(getByText('Orders Block Free : 600'))
    });

});