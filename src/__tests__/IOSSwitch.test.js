import IOSSwitch from "components/common/IOSSwitch";

import { getTestElement, renderWithProvider } from '../testUtils';

describe('<IOSSwitch>', () => {
    afterAll(() => {
        jest.restoreAllMocks();
    });

    const props = {
        checked: false
    };

    it('IOSSwitch Should Render', async () => {
        await renderWithProvider(IOSSwitch, props);
        const component = getTestElement('iosswitch');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
    });

});