import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { setFavService, setUnfavService, resetMarkings, setBusinessService, setBusinessLocalAction, setFavCUSOService } from "../reduxLib/services/getFavUnfavService"
const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)
import fetch from "../reduxLib/services/serviceHelpers";

jest.mock("../reduxLib/services/serviceHelpers")

describe('async actions', () => {
  let store;

  beforeEach(() => {
    store = mockStore({});
  });


  it('Set/Unset FavUnfav Services Data Success', () => {

    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return resolve()
      })
    })
    const dataVariables = { variables: [{ "siteNum": "2028", "region": "NA", "type": "MILL", "plantType": "KCNA Mill", "alias": "BIL", "shortName": "Beech Island Mill", "siteName": "S_2028_298", "siteNameDesc": "Beech Island Global Sales Mill", "location": { "id": "2028", "name": "Beech Island Mill", "city": "BEECH ISLAND", "state": "SC", "country": "USA", "street": "246 OLD JACKSON HIGHWAY", "geoLocation": { "longitude": -81.8986332, "latitude": 33.4163503 }, "timezone": "US/Eastern", "postalCode": "29842" }, "isFav": false }] }

    store.dispatch(setFavService(dataVariables))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })

    store.dispatch(setUnfavService(dataVariables))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })

  it('Set/Unset FavUnfav Services Data Failed', () => {

    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return reject()
      })
    })
    const dataVariables = { variables: [{ "siteNum": "2028", "region": "NA", "type": "MILL", "plantType": "KCNA Mill", "alias": "BIL", "shortName": "Beech Island Mill", "siteName": "S_2028_298", "siteNameDesc": "Beech Island Global Sales Mill", "location": { "id": "2028", "name": "Beech Island Mill", "city": "BEECH ISLAND", "state": "SC", "country": "USA", "street": "246 OLD JACKSON HIGHWAY", "geoLocation": { "longitude": -81.8986332, "latitude": 33.4163503 }, "timezone": "US/Eastern", "postalCode": "29842" }, "isFav": false }] }

    store.dispatch(setFavService(dataVariables))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })

    store.dispatch(setUnfavService(dataVariables))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })


  it('Reset Markers', () => {
    store.dispatch(resetMarkings(
      {
        dummyPayload:
        {
          "id": 1,
          "title": "Example",
          "project": "Testing",
          "createdAt": "2017-03-02T23:04:38.003Z",
          "modifiedAt": "2017-03-22T16:44:29.034Z"
        }
      }
    ));
    expect(store.getActions()).toMatchSnapshot();
  });


  it('setBusinessService ', () => {

    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return resolve()
      })
    })

    store.dispatch(setBusinessService({ value: { value: "ALL" }, type: "fav", bunits: [] }))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })

    store.dispatch(setBusinessService({ value: { value: "ALL" }, type: "unfav", bunits: ["PROFESSIONAL", "CONSUMER"] }))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })

    store.dispatch(setBusinessService({ value: { value: "PROFESSIONAL" }, type: "unfav", bunits: ["CONSUMER"] }))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })

    store.dispatch(setBusinessService({ value: { value: "PROFESSIONAL" }, type: "fav", bunits: ["CONSUMER"] }))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })

      store.dispatch(setBusinessService({ value: { value: "PROFESSIONAL" }, type: "fav", bunits: [] }))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })

  });


  it('setBusinessService Failed', () => {

    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return reject({
          status: 500,
          json: () => {
            return { data: {} }
          }
        })
      })
    })



    store.dispatch(setBusinessService({}))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  });


  it('setBusinessLocalActions', () => {
    store.dispatch(setBusinessLocalAction(["PROFESSIONAL"]));
    expect(store.getActions()).toMatchSnapshot();
  });

  it('Set Fav CUST Services - Success', () => {

    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return resolve()
      })
    })
    const variables = { variables: { "shortName": "N CAROLINA MUTUAL WHLSE DRUG", "reducerObject": { "customerName": "N CAROLINA MUTUAL WHLSE DRUG", "selectionType": "CUST", "distributionChannel": "80", "salesOrg": ["2810"]},"state": [{"selectionType": "CUST", "salesOrg": "2810", "distributionChannel": "80", "customerName": "R & R HEALTHCARE INC"}]}}

    store.dispatch(setFavCUSOService(variables))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })

  it('Set Unfav CUST Services - Success', () => {

    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return resolve()
      })
    })
    const variables = { variables: { "shortName": "N CAROLINA MUTUAL WHLSE DRUG", "reducerObject": { "customerName": "N CAROLINA MUTUAL WHLSE DRUG", "selectionType": "CUST", "distributionChannel": "80", "salesOrg": ["2810"]},"state": [{"selectionType": "CUST", "salesOrg": "2810", "distributionChannel": "80", "customerName": "N CAROLINA MUTUAL WHLSE DRUG"}]}}

    store.dispatch(setFavCUSOService(variables))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })

  it('Set Fav SO Services - Success', () => {

    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return resolve()
      })
    })
    const variables = { variables: { "shortName": "CS06", "reducerObject": { "salesOffice": "CS06", "selectionType": "SALES_OFFICE", "distributionChannel": "80", "salesOrg": "2810"},"state": [{"selectionType": "SALES_OFFICE", "salesOrg": "2810", "distributionChannel": "80", "salesOffice": "CS01"}]}}

    store.dispatch(setFavCUSOService(variables))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })

  it('Set Fav SO Services other DC - Success', () => {

    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return resolve()
      })
    })
    const variables = { variables: { "shortName": "CS06", "reducerObject": { "salesOffice": "CS06", "selectionType": "SALES_OFFICE", "distributionChannel": "80", "salesOrg": "2810"},"state": [{"selectionType": "SALES_OFFICE", "salesOrg": "2810", "distributionChannel": "90", "salesOffice": "CS06"}]}}

    store.dispatch(setFavCUSOService(variables))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })

  it('Set Unfav SO Services - Success', () => {

    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return resolve()
      })
    })
    const variables = { variables: { "shortName": "CS01", "reducerObject": { "salesOffice": "CS01", "selectionType": "SALES_OFFICE", "distributionChannel": "80", "salesOrg": "2810"},"state": [{"selectionType": "SALES_OFFICE", "salesOrg": "2810", "distributionChannel": "80", "salesOffice": "CS01"}]}}

    store.dispatch(setFavCUSOService(variables))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })

  it('Set Fav SG Services - Success', () => {

    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return resolve()
      })
    })
    const variables = { variables: { "shortName": "Z01", "type":"SALES_GROUP", "reducerObject": { "salesOffice": "CS01", "selectionType": "SALES_OFFICE", "distributionChannel": "80", "salesOrg": "2810"},"state": [{"selectionType": "SALES_OFFICE", "salesOrg": "2810", "distributionChannel": "80", "salesOffice": "CS01", "salesGroup": ["Z01"]}]}}

    store.dispatch(setFavCUSOService(variables))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })

  it('Set Fav SG Services - Success 2', () => {

    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return resolve({
          status:200,
          json: () => {
              return {
                state : [{"selectionType": "SALES_OFFICE", "salesOrg": "2810", "distributionChannel": "80", "salesOffice": "CS01", "salesGroup": ["Z01"]}]
              }
        }
      })
      })
    })
    const variables = { variables: { "shortName": "Z02", "type":"SALES_GROUP", "reducerObject": { "salesOffice": "CS01", "selectionType": "SALES_OFFICE", "distributionChannel": "80", "salesOrg": "2810"},"state": [{"selectionType": "SALES_OFFICE", "salesOrg": "2810", "distributionChannel": "80", "salesOffice": "CS01", "salesGroup": ["Z01", "Z02"]}]}}

    store.dispatch(setFavCUSOService(variables))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })

  it('Set Unfav SG Services - Success', () => {

    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return resolve()
      })
    })
    const variables = { variables: { "shortName": "Z01", "type":"SALES_GROUP", "reducerObject": { "salesOffice": "CS01", "selectionType": "SALES_OFFICE", "distributionChannel": "80", "salesOrg": "2810"},"state": [{"selectionType": "SALES_OFFICE", "salesOrg": "2810", "distributionChannel": "80", "salesOffice": "CS01"}]}}

    store.dispatch(setFavCUSOService(variables))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })

  it('Set/Unset FavUnfav CUST Services - Failed', () => {

    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return reject()
      })
    })
    const variables = { variables: { "shortName": "N CAROLINA MUTUAL WHLSE DRUG", "reducerObject": { "customerName": "N CAROLINA MUTUAL WHLSE DRUG", "selectionType": "CUST", "distributionChannel": "80", "salesOrg": ["2810"]},"state": [{"selectionType": "CUST", "salesOrg": "2810", "distributionChannel": "80", "customerName": "R & R HEALTHCARE INC"}]}}

    store.dispatch(setFavCUSOService(variables))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })

});