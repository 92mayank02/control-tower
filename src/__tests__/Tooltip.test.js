import BootstrapTooltip from '../components/D3Charts/D3Tooltip';
import { getTestElement, renderWithProvider } from '../testUtils';


describe('<BootstrapTooltip>', () => {
    afterAll(() => {
      jest.restoreAllMocks();
    });
  
    const BootstrapTooltipProps = {color:"red", placement:"top", title:"testTooltipbody", ref:null, children:null}
    xit('should render with label', async () => {
      await renderWithProvider(BootstrapTooltip,BootstrapTooltipProps);
      const component = getTestElement('bootstrapTooltip');
      expect(component).toBeInTheDocument();
    });

});