import Timer from '../components/common/Helpers/Timer';
import { getTestElement, renderWithProvider } from '../testUtils';


describe('<Timer Element>', () => {
   

    afterAll(() => {
        jest.restoreAllMocks();
    });

    const props = {
        date: new Date() 
    }

    it('Timer should render', async () => {
        await renderWithProvider(Timer, props);
        const component = getTestElement('timer');
        expect(component).toBeInTheDocument();
    });

});