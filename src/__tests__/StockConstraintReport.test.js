import React from 'react'
import StockConstraintReport from '../components/common/StockConstraintReport';
import { getTestElement, renderWithProvider, renderWithMockStore, screen, fireEvent } from '../testUtils';
import { waitFor } from '@testing-library/react';
import { mockStoreValue } from 'components/dummydata/mock';

describe('<Stock Constraint Report>', () => {
    afterAll(() => {
        jest.restoreAllMocks();
    });

    const props = { setDetails: jest.fn(), type: "2028", pageDetails:{filterParams:{navHistory: [{id: 'DIST', label: 'Distribution', type: '2028'}]}} }

    it('should render component container', async () => {
        await renderWithMockStore(StockConstraintReport, props, mockStoreValue);
        const component = getTestElement('stockConstraintReport');
        expect(component).toBeInTheDocument();
    });
});