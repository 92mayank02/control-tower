import getFavUnfavReducer from "../reduxLib/reducers/getFavUnfavReducer";
import { getFavUnfavConstants } from '../reduxLib/constants';

describe("Get Fav Unfav Reducer Functions", () => {

    afterEach(() => {
        jest.clearAllMocks();
    });

    const successPayload1 = {
        "dc": [{ "siteNum": "2047", "region": "NA", "type": "DC", "plantType": "RDC", "alias": "LADC", "shortName": "Los Angeles DC", "siteName": "S_2047_917", "siteNameDesc": "Los Angeles DC ExtOps/DC", "location": { "id": "2047", "name": "Los Angeles DC", "city": "ONTARIO", "state": "CA", "country": "USA", "street": "4815 S HELLMAN AVE", "geoLocation": { "longitude": -117.602477, "latitude": 34.040893 }, "timezone": "US/Pacific", "postalCode": "91762" } }],
        "mill": [{ "siteNum": "2031", "region": "NA", "type": "MILL", "plantType": "KCNA Mill", "alias": "NMC", "shortName": "New Milford Mill", "siteName": "S_2031_067", "siteNameDesc": "New Milford Global Sales Mill", "location": { "id": "2031", "name": "New Milford Mill", "city": "NEW MILFORD", "state": "CT", "country": "USA", "street": "58 PICKETT DIST ROAD", "geoLocation": { "longitude": -73.4111248, "latitude": 41.5579034 }, "timezone": "US/Eastern", "postalCode": "06776-4413" } }]
    }

    const successPayload2 = {
        "mill": [{ "siteNum": "2031", "region": "NA", "type": "MILL", "plantType": "KCNA Mill", "alias": "NMC", "shortName": "New Milford Mill", "siteName": "S_2031_067", "siteNameDesc": "New Milford Global Sales Mill", "location": { "id": "2031", "name": "New Milford Mill", "city": "NEW MILFORD", "state": "CT", "country": "USA", "street": "58 PICKETT DIST ROAD", "geoLocation": { "longitude": -73.4111248, "latitude": 41.5579034 }, "timezone": "US/Eastern", "postalCode": "06776-4413" } }]
    }

    const successPayload3 = {
        "dc": [{ "siteNum": "2047", "region": "NA", "type": "DC", "plantType": "RDC", "alias": "LADC", "shortName": "Los Angeles DC", "siteName": "S_2047_917", "siteNameDesc": "Los Angeles DC ExtOps/DC", "location": { "id": "2047", "name": "Los Angeles DC", "city": "ONTARIO", "state": "CA", "country": "USA", "street": "4815 S HELLMAN AVE", "geoLocation": { "longitude": -117.602477, "latitude": 34.040893 }, "timezone": "US/Pacific", "postalCode": "91762" } }],
    }

    const defaultState = { bu_updated: true, favloading: false, mark_loading: false, favorites: [], tempBusinessUnit: [], fav_cuso_loading: false, favCuso: { FAV_CUST: [], FAV_SALES_OFFICE: [] } }

    const markNewFav = { "siteNum": "2508", "region": "NA", "type": "DC", "plantType": "RDC", "alias": "NEDC", "shortName": "Northeast DC", "siteName": "S_2508_195", "siteNameDesc": "Northeast Distribution Center (NEDC)", "location": { "id": "2508", "name": "Northeast DC", "city": "SHOEMAKERSVILLE", "state": "PA", "country": "USA", "street": "211 LOGISTICS DRIVE", "geoLocation": { "longitude": -75.966777, "latitude": 40.522329 }, "timezone": "US/Eastern", "postalCode": "19555" }, "isFav": false }

    const defaultPopulatedState = { ...defaultState, favorites: [{ ...successPayload1.dc[0], isFav: true }, { ...successPayload1.mill[0], isFav: true }] }

    const favSOState = [{"selectionType": "SALES_OFFICE", "salesOrg": "2810", "distributionChannel": "80", "salesOffice": "CS01"}]
    
    const favCustState = [{"selectionType": "CUST", "salesOrg": "2810", "distributionChannel": "80", "customerName": "AMAZON"}]

    it("Default Switch Return", async () => {
        expect(getFavUnfavReducer(undefined, { type: "Action not listed", payload: successPayload1 })).toStrictEqual(defaultState);
    });
    it("Fav Loading Reducer", async () => {
        expect(getFavUnfavReducer(undefined, {
            type: getFavUnfavConstants.LOADING,
            payload: successPayload1,
        })).toStrictEqual({
            ...defaultState, favloading: successPayload1
        });
    });
    it("Get Favs Success", async () => {
        expect(getFavUnfavReducer(undefined, {
            type: getFavUnfavConstants.GET_FAVS_SUCCESS,
            payload: [{type: "DC"}],
        })).toStrictEqual({
            ...defaultState, favorites: [{type: "DC", isFav: true}]
        });

        expect(getFavUnfavReducer(undefined, {
            type: getFavUnfavConstants.GET_FAVS_SUCCESS,
            payload: [],
        })).toStrictEqual({
            ...defaultState, favorites: []
        });
    });
    it("Fav Failed Reducer", async () => {
        expect(getFavUnfavReducer(undefined, {
            type: getFavUnfavConstants.GET_FAVS_FAILED,
            payload: false,
        })).toStrictEqual({
            ...defaultState, favorites: []
        });
    });

    it("Fav Cuso Failed Reducer", async () => {
        expect(getFavUnfavReducer(undefined, {
            type: getFavUnfavConstants.FAV_CUSO_FAILED,
            payload: false,
        })).toStrictEqual({
            ...defaultState, favCuso: { FAV_CUST: [], FAV_SALES_OFFICE: [] }
        });
    });

    it("Mark Fav Success Reducer", async () => {
        expect(getFavUnfavReducer(defaultPopulatedState, {
            type: getFavUnfavConstants.MARK_FAV_SUCCESS,
            payload: { data: markNewFav, status: 'done' },
        })).toStrictEqual({
            ...defaultState, markfav: 'done', favorites: [...defaultPopulatedState.favorites, { ...markNewFav, isFav: true }]
        });
    });

    it("UnMark Fav Success Reducer", async () => {
        expect(getFavUnfavReducer(defaultPopulatedState, {
            type: getFavUnfavConstants.MARK_UNFAV_SUCCESS,
            payload: { data: successPayload2.mill[0], status: 'done' },
        })).toStrictEqual({
            ...defaultState, markunfav: 'done', favorites: [{ ...successPayload3.dc[0], isFav: true }]
        });
    });

    it("Mark Fav Failed Reducer", async () => {
        expect(getFavUnfavReducer(defaultPopulatedState, {
            type: getFavUnfavConstants.MARK_FAV_FAILED,
            payload: { data: null, status: 'failed' },
        })).toStrictEqual({
            ...defaultPopulatedState, markfav: 'failed'
        });
    })

    it("UnMark Fav Failed Reducer", async () => {
        expect(getFavUnfavReducer(defaultPopulatedState, {
            type: getFavUnfavConstants.MARK_UNFAV_FAILED,
            payload: { data: null, status: 'failed' },
        })).toStrictEqual({
            ...defaultPopulatedState, markunfav: 'failed'
        });
    })

    it("Mark Loading Reducer", async () => {
        expect(getFavUnfavReducer(defaultPopulatedState, {
            type: getFavUnfavConstants.MARK_LOADING,
            payload: false,
        })).toStrictEqual({
            ...defaultPopulatedState, mark_loading: false
        });
    });

    it("BU Loading Reducer", async () => {
        expect(getFavUnfavReducer(defaultState, {
            type: getFavUnfavConstants.ADD_BUSINESS_UNIT_LOADING,
            payload: false,
        })).toStrictEqual({
            ...defaultState, add_bu_loading: false
        });
    });

    it("Add Business Unit Success/Failure  Reducer", async () => {
        expect(getFavUnfavReducer(defaultState, {
            type: getFavUnfavConstants.ADD_BUSINESS_UNIT_SUCCESS,
            payload: ["PROFESSIONAL"],
        })).toStrictEqual({
            ...defaultState,
            bu_updated: false,
            businessUnits: ["PROFESSIONAL"],
            tempBusinessUnit: ["PROFESSIONAL"]
        });
    })

    it("Temp Business Unit Reducer", async () => {
        expect(getFavUnfavReducer(defaultState, {
            type: getFavUnfavConstants.SET_BUSINESS_UNIT_LOCAL,
            payload: ["PROFESSIONAL"],
        })).toStrictEqual({
            ...defaultState,
            tempBusinessUnit: ["PROFESSIONAL"]
        });
    })

    it("CUSO Loading Reducer", async () => {
        expect(getFavUnfavReducer(defaultState, {
            type: getFavUnfavConstants.FAV_CUSO_LOADING,
            payload: false,
        })).toStrictEqual({
            ...defaultState, fav_cuso_loading: false
        });
    });

    it("Fav/Unfav SO Success Reducer", async () => {
        expect(getFavUnfavReducer(defaultState, {
            type: getFavUnfavConstants.FAV_CUSO_SUCCESS,
            payload: { selectionType: "SALES_OFFICE", updatedState: favSOState },
        })).toStrictEqual({
            ...defaultState, favCuso:{ ...defaultState.favCuso, FAV_SALES_OFFICE: favSOState}
        });
    });

    it("Fav/Unfav Cust Success Reducer", async () => {
        expect(getFavUnfavReducer(defaultState, {
            type: getFavUnfavConstants.FAV_CUSO_SUCCESS,
            payload: { selectionType: "CUST", updatedState: favCustState },
        })).toStrictEqual({
            ...defaultState, favCuso:{ ...defaultState.favCuso, FAV_CUST: favCustState}
        });
    });

    it("Fav/Unfav default Success Reducer", async () => {
        expect(getFavUnfavReducer(defaultState, {
            type: getFavUnfavConstants.FAV_CUSO_SUCCESS,
            payload: { selectionType: "", updatedState: favCustState },
        })).toStrictEqual({
            ...defaultState, favCuso: { FAV_CUST: [], FAV_SALES_OFFICE: [] }
        });
    });

    it("Get Fav CUSO Success Reducer", async () => {
        expect(getFavUnfavReducer(undefined, {
            type: getFavUnfavConstants.GET_FAV_CUSO,
            payload: [],
        })).toStrictEqual({
            ...defaultState, favCuso: { FAV_CUST: [], FAV_SALES_OFFICE: [] }
        });
    });
})