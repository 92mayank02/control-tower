import PerformanceMapTable from '../components/common/PerformanceMapTable';
import { getTestElement, renderWithMockStore, fireEvent } from '../testUtils';
import { performanceView } from 'components/dummydata/mock';
import { getStatusData } from "helpers";
import { sortBy, isArray } from "lodash";

describe('<PerformanceMapTable>', () => {
    afterAll(() => {
      jest.restoreAllMocks();
    });

    const formatData = (siteData) => {

        
        let perfData = isArray(siteData) ? siteData : [];
    
        perfData = perfData.map(d => {
            const data = getStatusData({
                ...d,
                actualHours: d.aheadOrBehind == "BEHIND" ? -(d.hours): d.hours
            }, "actualHours");
            return {
                ...d,
                ...data
            };
        });
    
        return  perfData = sortBy(perfData, ['actualHours']);
    
        }

    const PerformanceTableProps = {data : formatData(performanceView), setDetails: jest.fn(), type: "network", perfTableRedirection: jest.fn()};
    const PerformanceTableProps2 = {data : [], setDetails: jest.fn(), type: "network", perfTableRedirection: jest.fn() };




    it('should render with values', async () => {
        
        await renderWithMockStore(PerformanceMapTable, PerformanceTableProps);
        const component = getTestElement('performancetable');
        expect(component).toBeInTheDocument();
    });
  
    it('should render no data', async () => {
        await renderWithMockStore(PerformanceMapTable, PerformanceTableProps2);
        const component = getTestElement('performancetable');
        expect(component).toBeInTheDocument();
    });

    it('should route to distribution page', async () => {
        const { getAllByTestId } = await renderWithMockStore(PerformanceMapTable, PerformanceTableProps);
        fireEvent.click(getAllByTestId("distributionlink")[0]);
    });

    it('should route to distribution page (when setDetails is not a function)', async () => {
        const { getAllByTestId } = await renderWithMockStore(PerformanceMapTable, {...PerformanceTableProps, setDetails:{}, toggleMainTab:jest.fn()});
        fireEvent.click(getAllByTestId("distributionlink")[0]);
    });
});