import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import getShipmentDetailsService from "../reduxLib/services/getShipmentDetailsService";
const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
import { mockShipmentDetailsStopsDetails } from "../components/dummydata/mock";
import fetch from "../reduxLib/services/serviceHelpers";
import { detailsConstants } from '../reduxLib/constants';


jest.mock("../reduxLib/services/serviceHelpers")

describe('async actions', () => {
    let store;

    beforeEach(() => {
        store = mockStore({});
    });

    it('Get Shipment Details Services Data Success', () => {

        fetch.mockImplementation(() => {
            return new Promise((resolve, reject) => {
                return resolve({
                    status: 200,
                    json: () => {
                        return mockShipmentDetailsStopsDetails
                    }
                })
            })
        })

        return store.dispatch(getShipmentDetailsService({
            "region": "NA",
            "shipmentNum": "shipment123",
            "searchType": detailsConstants.STOPS_DETAILS
        }))
            .then(() => { // return of async actions
                expect(store.getActions()).toMatchSnapshot();
            })
    })

    it('Get Shipment Tracking Details Services Data Success', () => {

        fetch.mockImplementation(() => {
            return new Promise((resolve, reject) => {
                return resolve({
                    status: 200,
                    json: () => {
                        return mockShipmentDetailsStopsDetails
                    }
                })
            })
        })

        return store.dispatch(getShipmentDetailsService({
            "region": "NA",
            "shipmentNum": "shipment123",
            "searchType": detailsConstants.TRACKING_DETAILS
        }))
            .then(() => { // return of async actions
                expect(store.getActions()).toMatchSnapshot();
            })
    })

    it('Get Shipment Details Services from Server Side', () => {

        fetch.mockImplementation(() => {
            return new Promise((resolve, reject) => {
                return reject({
                    status: 500,
                    json: () => {
                        return { data: {} }
                    }
                })
            })
        })

        return store.dispatch(getShipmentDetailsService({
            "region": "NA",
            "shipmentNum": "shipment123",
            "searchType": detailsConstants.STOPS_DETAILS
        }))
            .then(() => { // return of async actions
                expect(store.getActions()).toMatchSnapshot();
            })
    })

    it('Get Shipment Details Services Data Failed from Client Side', () => {
        fetch.mockImplementation(() => {
            return new Promise((resolve, reject) => {
                return resolve({
                    status: 100
                })
            })
        })

        return store.dispatch(getShipmentDetailsService({
            "region": "NA",
            "shipmentNum": "shipment123",
            "searchType": detailsConstants.STOPS_DETAILS
        }))
            .then(() => { // return of async actions
                expect(store.getActions()).toMatchSnapshot();
            })
    })

    it('Get Shipment Tracking Details Services Data Failed from Client Side', () => {
        fetch.mockImplementation(() => {
            return new Promise((resolve, reject) => {
                return resolve({
                    status: 100
                })
            })
        })

        return store.dispatch(getShipmentDetailsService({
            "region": "NA",
            "shipmentNum": "shipment123",
            "searchType": detailsConstants.TRACKING_DETAILS
        }))
            .then(() => { // return of async actions
                expect(store.getActions()).toMatchSnapshot();
            })
    })

});