import TransportView, { conditionalFilterReplacement, tableStyleConfig } from 'components/pages/TransportView';
import { getTestElement, initiateGlobalAuthService, renderWithMockStore } from '../testUtils';
import { defaultTransportColumns } from "../reduxLib/constdata/transportColumns"
import { fireEvent, waitFor } from '@testing-library/react';
import fetch from "../reduxLib/services/serviceHelpers";
import Hidden from "@material-ui/core/Hidden"
jest.mock("../reduxLib/services/serviceHelpers")
jest.mock("@material-ui/core/Hidden")
describe('<TransportView Component>', () => {

    afterAll(() => {
        jest.restoreAllMocks();
    });

    beforeAll(() => {
        initiateGlobalAuthService();
    });

    const props = {
        setDetails: jest.fn(),
        pageDetails: { filterParams: null },
        setRefresh: jest.fn()
    }

    it('TransportView (Filter Condition Function)', async () => {
        let transportFilters = {
            orderExecutionBucket: ["TRANS_PLAN_PROCESSING"]
        }

        await conditionalFilterReplacement(transportFilters)
        expect(transportFilters).toEqual(
            {
                "orderExecutionBucket": ["TRANS_PLAN_PLANNED", "TRANS_PLAN_TENDERED", "TRANS_PLAN_TENDER_REJECTED"],
            }
        );
    });

    it('TransportView (Table Options Function)', async () => {
        const tableoptions = {
            rowStyle: {
                color: "#FFF"
            }
        }
        expect(tableStyleConfig.rowStyle({orderExecutionHealth:"RED"})).toEqual(
            {
                "@media (min-width:600px)": {
                    "fontWeight": "500",
                },
                "color": "#FFF",
                "borderLeft":"5px solid #FF4081",
                "flexGrow": 1,
                "fontFamily": "'Gilroy', sans_serif",
                "fontSize": "14px",
                "fontWeight": 400,
                "lineHeight": 1.5,
            }
        );

        expect(tableStyleConfig.rowStyle({orderExecutionHealth:"YELLOW"})).toEqual(
            {
                "@media (min-width:600px)": {
                    "fontWeight": "500",
                },
                "color": "#FFF",
                "borderLeft":"5px solid #F5B023",
                "flexGrow": 1,
                "fontFamily": "'Gilroy', sans_serif",
                "fontSize": "14px",
                "fontWeight": 400,
                "lineHeight": 1.5,
            }
        );

        expect(tableStyleConfig.rowStyle({orderHealth:"YELLOW"})).toEqual(
            {
                "@media (min-width:600px)": {
                    "fontWeight": "500",
                },
                "color": "#FFF",
                "borderLeft":"",
                "flexGrow": 1,
                "fontFamily": "'Gilroy', sans_serif",
                "fontSize": "14px",
                "fontWeight": 400,
                "lineHeight": 1.5,
            }
        );
    });


    it('TransportView (Column Configuration for "-" )', async () => {
        const data = {}
        expect(defaultTransportColumns.columnConfiguration(data)).toEqual(
            {
                originId: "-",
                origin: "-",
                liveLoadInd: "-",
                appointmentRequired: "-",
                orderOnHoldInd: "-",
                deliverApptDateTime: "-",
                expectedDeliveryDateTime: "-",
                loadReadyDateTime: "-",
                loadingCompletedOnTime: "-",
                loadingCompletionDateTime: "-",
                customer: "-",
                actualDeliveredDateTime: "-",
                appointmentStatusDesc: "-",
                appointmentType: "-",
                destinationCity: "-",
                destinationState: "-",
                shipmentSuspendedStatus: "-",
                tmsRushIndicator: "-",
                reasonCodeString: "-"
            }
        );
    });



    it('TransportView should render- No Data', async () => {

        fetch.mockImplementation(()=>{
            return new Promise((resolve,reject) => {
                return resolve({
                    status:200,
                    json: () => {
                        return {"onPage":1,"pageSize":5,"totalPages":1,"totalRecords":0,"orders":[]}
                    }
                })
              })
        });

        Hidden.mockImplementation((props)=>props.children)
        const { getByText, getByTestId } =  await renderWithMockStore(TransportView, props);
        const component = getTestElement('transportview');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
        fireEvent.click(getByText("View More"));
        waitFor(()=> getByText("View Less"));
        expect(getByText("View Less")).toBeInTheDocument()
    });


});