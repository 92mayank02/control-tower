import React from 'react';
import matchMediaPolyfill from 'mq-polyfill'
import ChartCarouselComponent from '../components/common/ChartCarouselComponent';
import { getTestElement, renderWithProvider } from '../testUtils';


describe('<ChartCarouselComponent>', () => {
  beforeAll(() => {
    matchMediaPolyfill(window)
    window.resizeTo = function resizeTo(width, height) {
      Object.assign(this, {
        innerWidth: width,
        innerHeight: height,
        outerWidth: width,
        outerHeight: height,
      }).dispatchEvent(new this.Event('resize'))
    }
    window.resizeTo(400, 400);
  })

  it('should render no data', async () => {
    await renderWithProvider(ChartCarouselComponent, { chartsArray: [null, null, null, null] });
    const component = getTestElement('slickCarousel');
    Array.from(document.querySelectorAll(".BrainhubCarousel__dot")).forEach(button => button.click())
    expect(component).toBeInTheDocument();
  });

})