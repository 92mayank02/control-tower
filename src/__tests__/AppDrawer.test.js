import AppDrawer from '../components/common/AppDrawer';
import { fireEvent, getTestElement, renderWithProvider } from '../testUtils';
import useMediaQuery from "@material-ui/core/useMediaQuery"
jest.mock('@material-ui/core/useMediaQuery')

jest.mock('@azure/app-configuration', () => {
  return {
    AppConfigurationClient: function (app_config_string) {
      return {
        getConfigurationSetting: ({key}) => {
           if (key.indexOf("showAnalyticsLink") > 0 ) {
              return {
                "value": "{\"id\":\"showAnalyticsLink\",\"description\":\"true\",\"enabled\":true,\"conditions\":{\"client_filters\":[]}}",
                "syncToken": "zAJw6V16=MDozIzU3MDM5NjA=;sn=5703960",
                "lastModified": "2021-07-12T05:20:42.000Z",
                "content-type": "application/vnd.microsoft.appconfig.kv+json; charset=utf-8",
                "date": "Mon, 12 Jul 2021 07:29:20 GMT",
                "x-ms-request-id": "fde20d09-b679-4a25-a60f-96a340426946",
                "key": ".appconfig.featureflag/showAnalyticsLink",
                "label": null,
                "contentType": "application/vnd.microsoft.appconfig.ff+json;charset=utf-8",
                "tags": {},
                "etag": "D1w3acDjCmzwvZgALVtwTP45L6p",
                "isReadOnly": false,
                "statusCode": 200
            }
           }
           else if( key.indexOf("analyticsReports") > 0){
              return {
                "value": "{\"id\":\"analyticsReports\",\"description\":\"\\\"[{\\\\\\\"name\\\\\\\":\\\\\\\"Carrier Selection Optimization\\\\\\\",\\\\\\\"open\\\\\\\":false,\\\\\\\"reports\\\\\\\":[{\\\\\\\"reportName\\\\\\\":\\\\\\\"Quality of Service\\\\\\\",\\\\\\\"reportUrl\\\\\\\":\\\\\\\"https://app.powerbi.com/reportEmbed?reportId=b861ca9e-412d-44dc-b505-808692d5c1a3&autoAuth=true&ctid=fee2180b-69b6-4afe-9f14-ccd70bd4c737&config=eyJjbHVzdGVyVXJsIjoiaHR0cHM6Ly93YWJpLXVzLW5vcnRoLWNlbnRyYWwtZS1wcmltYXJ5LXJlZGlyZWN0LmFuYWx5c2lzLndpbmRvd3MubmV0LyJ9\\\\\\\"},{\\\\\\\"reportName\\\\\\\":\\\\\\\"Recommendation Analysis\\\\\\\",\\\\\\\"reportUrl\\\\\\\":\\\\\\\"https://app.powerbi.com/reportEmbed?reportId=b0ee5159-29a7-4a52-97b5-c6dec7de1c76&autoAuth=true&ctid=fee2180b-69b6-4afe-9f14-ccd70bd4c737&config=eyJjbHVzdGVyVXJsIjoiaHR0cHM6Ly93YWJpLXVzLW5vcnRoLWNlbnRyYWwtZS1wcmltYXJ5LXJlZGlyZWN0LmFuYWx5c2lzLndpbmRvd3MubmV0LyJ9\\\\\\\"}]}]\\\"\",\"enabled\":true,\"conditions\":{\"client_filters\":[]}}",
                "syncToken": "zAJw6V16=MDozIzU3MDMzOTc=;sn=5703397",
                "lastModified": "2021-07-09T02:26:08.000Z",
                "content-type": "application/vnd.microsoft.appconfig.kv+json; charset=utf-8",
                "date": "Mon, 12 Jul 2021 05:16:50 GMT",
                "x-ms-request-id": "c02c4daa-41ff-4d0c-a82d-95cbb9781190",
                "key": ".appconfig.featureflag/analyticsReports",
                "label": null,
                "contentType": "application/vnd.microsoft.appconfig.ff+json;charset=utf-8",
                "tags": {},
                "etag": "xd8kHM6sAN4xsOMbe5W9J98E6Ip",
                "isReadOnly": false,
                "statusCode": 200
            }
           }
           else 
            return {}
        }
      };
    }
  };
});

jest.mock('../theme/layouts/MainLayout', () => ({
  chartDataIdentifierForAPI: {
    network: 'NETWORK',
    order: 'ORDER',
    transPlan: 'TRANS_PLAN',
    transExec: 'TRANS_EXEC',
    dist: 'DIST',
    shipmentdetails: 'SHIPMENTDETAILS',
    analytics: 'ANALYTICS'
  }
}));

describe('<AppDrawer>', () => {
  afterAll(() => {
    jest.restoreAllMocks();
  });

  const AppDrawerProps = {
    setDetails: jest.fn(),
    pageDetails: { NETWORK: true, type: 'network' }
  };
  const AppDrawerProps2 = {
    setDetails: jest.fn(),
    pageDetails: { type: 'network' }
  };

  it('should open hamburger menu - desktop', async () => {
    const { getByTestId, getAllByTestId } = await renderWithProvider(
      AppDrawer,
      AppDrawerProps
    );
    const component = getTestElement('menu');
    expect(component).toBeInTheDocument();
    fireEvent.click(getByTestId('menu'));
    expect(getTestElement('app-drawer')).toBeInTheDocument();
    fireEvent.click(getByTestId('nested-list-subheader'));
    fireEvent.click(getAllByTestId('ctLink')[0], 'NETWORK');
    fireEvent.click(getAllByTestId('ctLink')[1], 'ORDER');
    fireEvent.click(getByTestId('analytics'));
    fireEvent.click(getAllByTestId('analyticsFolder')[0], 0);
    fireEvent.click(getAllByTestId('analyticsReport')[0], 'Carrier Report');
  });

  it('should open hamburger menu - mobile', async () => {
    useMediaQuery.mockImplementation(() => {
      return {
          __esModule: true,
          default: () => {
            return true;
          },
        };
    })
    const { getByTestId, getAllByTestId } = await renderWithProvider(
      AppDrawer,
      AppDrawerProps
    );
    const component = getTestElement('menu');
    expect(component).toBeInTheDocument();
    fireEvent.click(getByTestId('menu'));
    expect(getTestElement('app-drawer')).toBeInTheDocument();
    fireEvent.click(getByTestId('nested-list-subheader'));
    fireEvent.click(getAllByTestId('ctLink')[0], 'NETWORK');
    fireEvent.click(getAllByTestId('ctLink')[1], 'ORDER');
  });

  it('check active menu is in sync', async () => {
    const { getByTestId } = await renderWithProvider(
      AppDrawer,
      AppDrawerProps2
    );
    const component = getTestElement('menu');
    expect(component).toBeInTheDocument();
    fireEvent.click(getByTestId('menu'));
  });
});
