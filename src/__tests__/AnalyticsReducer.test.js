import analyticsReducer from "../reduxLib/reducers/analyticsReducer";
import { analyticsConstants } from '../reduxLib/constants';

describe("Analytics Reducer Functions", () => {

  afterEach(() => {
      jest.clearAllMocks();
  });

  const defaultState = { cstUpdated: null, loading: false }
  const data = {
    userName: "abc@kcc.com",
    lane: "Lane 123",
    CSTRankingList: ["HJBI", "HJBN"],
    CTRankingList: ["HJBN", "HJBI"]
  }

  it("Update Ranking Loading Reducer", async () => {
    expect(analyticsReducer(defaultState, {
        type: analyticsConstants.UPDATE_LOADING,
        payload: true,
    })).toStrictEqual({
        ...defaultState, loading: true
    });
  });

  it("Update Ranking Success Reducer", async () => {
    expect(analyticsReducer(defaultState, {
        type: analyticsConstants.UPDATE_CST_RANK,
        payload: data,
    })).toStrictEqual({
        ...defaultState, cstUpdated: true
    });
  });

  it("Update Ranking Failed Reducer", async () => {
    expect(analyticsReducer(defaultState, {
        type: analyticsConstants.UPDATE_FAILED,
        payload: false,
    })).toStrictEqual({
        ...defaultState, loading: false, cstUpdated: false
    });
  });

  it("Analytics Default Reducer", async () => {
    expect(analyticsReducer(defaultState, {
        type: "",
        payload: null,
    })).toStrictEqual({
        ...defaultState, cstUpdated: null, loading: false
    });
  });

})