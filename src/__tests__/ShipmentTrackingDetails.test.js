import React from 'react'
import ShipmentTrackingDetails from '../components/common/ShipmentTrackingDetails';
import { getTestElement, renderWithProvider, fireEvent } from '../testUtils';
import { mockShipmentDetailsStopsDetails } from 'components/dummydata/mock';
jest.mock("../theme/layouts/GeoMap", () => {
    return {
      __esModule: true,
      default: () => {
        return <div data-testid="geochartmock" ></div>;
      },
    };
  });

describe('<ShipmentTrackingDetails>', () => {
    afterAll(() => {
      jest.restoreAllMocks();
    });

    const ShipmentTrackingDetailsProps = {shipmentTrackingData : mockShipmentDetailsStopsDetails};
    const ShipmentTrackingDetailsProps2 = {shipmentTrackingData : {stops :[], locationHistory:[]}};

    it('should render with values', async () => {
        await renderWithProvider(ShipmentTrackingDetails, ShipmentTrackingDetailsProps);
        const component = getTestElement('trackingdetails');
        expect(component).toBeInTheDocument();
    });

    it('should render no data', async () => {
        await renderWithProvider(ShipmentTrackingDetails, ShipmentTrackingDetailsProps2);
        const component = getTestElement('trackingdetails');
        expect(component).toBeInTheDocument();
    });
});