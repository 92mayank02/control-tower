import TransportDetails, { columns } from 'components/common/Table/TransportDetails';
import { generateDefaultData } from "../helpers";
import { getTestElement, renderWithMockStore } from '../testUtils';


describe('<TransportDetails Component>', () => {

    afterAll(() => {
        jest.restoreAllMocks();
    });


    const props = {
        data: {
            customerNotifiedDateTime: [new Date(), null],
            deliveryApptConfirmedDateTimeList: [new Date(), null],
            tmsOrderCreateDateTime: new Date(),
            estimatedLoadTimeMS: 20,
            remainingLoadTimeMS: 10
        },
        data2: {        },
        classes: {}
    }

    it('TransportDetails should render', async () => {
        await renderWithMockStore(TransportDetails, props);
        const component = getTestElement('transportdetails');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
    });

    it("Transport Columns Render Test - Case 1", () => {
        const mockResult = (cols) => {            
            return cols.columnConfiguration(props.data);
        }
        expect(mockResult(columns)["estimatedLoadTimeMS"]).toBe(50);

    });
    it("Transport Columns Render Test - Case 2", () => {
        const mockResult = (cols) => {            
            return cols.columnConfiguration(props.data2);
        }
        expect(mockResult(columns)["estimatedLoadTimeMS"]).toBe("-");

    });

});