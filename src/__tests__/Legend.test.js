import Legend from '../components/common/Legend';
import { getTestElement, renderWithProvider } from '../testUtils';


describe('<Legend>', () => {
    afterAll(() => {
      jest.restoreAllMocks();
    });
  
    const props = {}
    it('should render with label', async () => {
      await renderWithProvider(Legend,props);
      const component = getTestElement('legend');
      expect(component).toBeInTheDocument();
    });

});