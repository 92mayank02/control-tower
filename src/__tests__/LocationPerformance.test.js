import React from 'react'
import LocationPerformance from '../components/common/LocationPerformance';
import { getTestElement, renderWithMockStore } from '../testUtils';
import { performanceView, mockStoreValue } from '../components/dummydata/mock';

jest.mock("helpers", () => {
  const getStatusData = () => {};
  return {
    getStatusData
  }
})

describe('<LocationPerformance>', () => {

  const props = {
    setDetails:jest.fn(),
    type: "2028"
  }

  const props2 = {
    setDetails:jest.fn(),
    type: "2024"
  }

  it('should render with values - AHEAD', async () => {
    await renderWithMockStore(LocationPerformance, props, {...mockStoreValue, sites: { ...mockStoreValue.sites, performance:performanceView}});
    const component = getTestElement('performancetable');
    expect(component).toBeInTheDocument();
  });

  it('should render with values - BEHIND', async () => {
    await renderWithMockStore(LocationPerformance, props2, {...mockStoreValue, sites: { ...mockStoreValue.sites, performance:performanceView}});
    const component = getTestElement('performancetable');
    expect(component).toBeInTheDocument();
  });


  it('should render no data', async () => {
    await renderWithMockStore(LocationPerformance, props, {...mockStoreValue, sites: { ...mockStoreValue.sites, performance:null}});
    const component = getTestElement('performancetable');
    expect(component).toBeInTheDocument();
  });
});