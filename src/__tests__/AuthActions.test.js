import configureMockStore from 'redux-mock-store'
import {addUser, removeUser} from "../reduxLib/services/authActions"
import thunk from 'redux-thunk'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)
const store = mockStore({})
describe('Auth Actions', () => {
  it('Auth Actions Add User Details', () => {
    store.dispatch(addUser(
      { dummyPayload:
          {    
            "id":1,
            "title":"Example",
            "project":"Testing",
            "createdAt":"2017-03-02T23:04:38.003Z",
            "modifiedAt":"2017-03-22T16:44:29.034Z"
        }
      }
    ));
    expect(store.getActions()).toMatchSnapshot();
  });

  it('Auth Actions Remove User Details', () => {
    store.dispatch(removeUser(
      { dummyPayload:
          {    
            "id":1,
            "title":"Example",
            "project":"Testing",
            "createdAt":"2017-03-02T23:04:38.003Z",
            "modifiedAt":"2017-03-22T16:44:29.034Z"
        }
      }
    ));
    expect(store.getActions()).toMatchSnapshot();
  });
});