import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { getAllGuardrails, saveGuardrails } from 'reduxLib/services/getAllGuardRailsService'
const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)
import fetch from "../reduxLib/services/serviceHelpers";

jest.mock("../reduxLib/services/serviceHelpers")

describe('async actions', () => {
  let store;

  const mockdata = [
    {
      "siteNum": "2023",
      "unit": "LOADS", // Possible values "Loads" or "Hours"
      "weekdays": [
        {
          "day": "Sunday",
          "orderBusinessUnit": [
            {
              "businessUnit": "CONSUMER",
              "guardrailFL": 2023,
              "guardrailLTL": 18
            },
            {
              "businessUnit": "PROFESSIONAL",
              "guardrailFL": 36,
              "guardrailLTL": 36
            },
            {
              "businessUnit": "ALL",
              "guardrailFL": 36,
              "guardrailLTL": 36
            }
          ]
        },
        {
          "day": "Monday",
          "orderBusinessUnit": [
            {
              "businessUnit": "CONSUMER",
              "guardrailFL": 19,
              "guardrailLTL": 19
            },
            {
              "businessUnit": "PROFESSIONAL",
              "guardrailFL": 33,
              "guardrailLTL": 23
            },
            {
              "businessUnit": "ALL",
              "guardrailFL": 23,
              "guardrailLTL": 23
            }
          ]
        }
      ]
    },
    {
        "siteNum": "2027",
        "unit": "LOADS", // Possible values "Loads" or "Hours"
        "weekdays": [
          {
            "day": "Sunday",
            "orderBusinessUnit": [
              {
                "businessUnit": "CONSUMER",
                "guardrailFL": 2027,
                "guardrailLTL": 12
              },
              {
                "businessUnit": "PROFESSIONAL",
                "guardrailFL": 45,
                "guardrailLTL": 45
              },
              {
                "businessUnit": "ALL",
                "guardrailFL": 67,
                "guardrailLTL": 67
              }
            ]
          },
          {
            "day": "Monday",
            "orderBusinessUnit": [
              {
                "businessUnit": "CONSUMER",
                "guardrailFL": 13,
                "guardrailLTL": 13
              },
              {
                "businessUnit": "PROFESSIONAL",
                "guardrailFL": 88,
                "guardrailLTL": 88
              },
              {
                "businessUnit": "ALL",
                "guardrailFL": 99,
                "guardrailLTL": 99
              }
            ]
          }
        ]
      },
      {
        "siteNum": "2022",
        "unit": "LOADS", // Possible values "Loads" or "Hours"
        "weekdays": [
          {
            "day": "Sunday",
            "orderBusinessUnit": [
              {
                "businessUnit": "CONSUMER",
                "guardrailFL": 2022,
                "guardrailLTL": 56
              },
              {
                "businessUnit": "PROFESSIONAL",
                "guardrailFL": 66,
                "guardrailLTL": 66
              },
              {
                "businessUnit": "ALL",
                "guardrailFL": 66,
                "guardrailLTL": 66
              }
            ]
          },
          {
            "day": "Monday",
            "orderBusinessUnit": [
              {
                "businessUnit": "CONSUMER",
                "guardrailFL": 88,
                "guardrailLTL": 88
              },
              {
                "businessUnit": "PROFESSIONAL",
                "guardrailFL": 88,
                "guardrailLTL": 88
              },
              {
                "businessUnit": "ALL",
                "guardrailFL": 99,
                "guardrailLTL": 99
              }
            ]
          }
        ]
      }
  ]

  beforeEach(() => {
    store = mockStore({});
  });

  it('Get Guardrails Data Success', () => {

    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return resolve({
          status: 200,
          json: () => {
            return mockdata
          }
        })
      })
    })

    return store.dispatch(getAllGuardrails())
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })

  it('Get Guardrails Data Falied', () => {

    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return resolve({
          status: 500,
          json: () => {
            return []
          }
        })
      })
    })

    return store.dispatch(getAllGuardrails())
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })

  it('Update Guardrails Data Success', () => {

    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return resolve({
          status: 200,
          json: () => {
            return mockdata
          }
        })
      })
    })

    return store.dispatch(saveGuardrails(mockdata))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })

  it('Update Guardrails Data Falied', () => {

    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return resolve({
          status: 401,
          json: () => {
            return {error: 401, message: "Unauthorised Access"}
          }
        })
      })
    })

    return store.dispatch(saveGuardrails(mockdata))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })




})