import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { getTableData, resetTable } from "../reduxLib/services"
const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)
import fetch from "../reduxLib/services/serviceHelpers";

jest.mock("../reduxLib/services/serviceHelpers")

describe('async actions', () => {
  let store;

  beforeEach(() => {
    store = mockStore({});
  });

  it('Table Data  Post Success', () => {

    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return resolve({
          status: 200,
          json: () => {
            return {}
          }
        })
      })
    })

    return store.dispatch(getTableData({
      fetchEndPoint: "/search",
      tablebody: {},
      pageSize: 10,
      page: 1
    }))
      .then(() => {
        expect(store.getActions()).toMatchSnapshot();
      })
  })

  it('Table Data  Post Failure', () => {

    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return reject()
      })
    })

    return store.dispatch(getTableData({
      fetchEndPoint: "/search",
      tablebody: {},
      pageSize: 10,
      page: 1
    }))
      .then(() => {
        expect(store.getActions()).toMatchSnapshot();
      })
  })


})