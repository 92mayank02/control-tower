import optionsReducer from "../reduxLib/reducers/optionsReducer";
import { optionsConstants } from '../reduxLib/constants';
import { networkfilters } from "../reduxLib/constdata/filters";



describe("Test Option Reducer Functions", () => {

    afterEach(() => {
        jest.clearAllMocks();
    });

    const stateForColumns = {
        "showfavs": true, "theme": "dark", "filters": { "network": { "sites": { "type": "sites", "name": "Origin", "data": null }, "searchStringList": { "type": "text", "name": "Search", "data": null }, "materialNumberList": { "type": "text", "name": "Material ID", "data": null }, "destName": { "type": "text", "name": "Customer", "data": null }, "destState": { "type": "text", "name": "Destination State", "data": null }, "destCity": { "type": "text", "name": "Destination City", "data": null }, "orderStatusBucket": { "type": "checkbox", "name": "Order Status", "data": [{ "name": "Order Block Free", "value": "ORDER_BLOCK_FREE", "checked": false }, { "name": "Order Blocked", "value": "ORDER_BLOCKED", "checked": false }] }, "orderTypes": { "type": "checkbox", "name": "Order Type", "data": [{ "name": "Cust. Order", "value": "CUST", "checked": false }, { "name": "STO", "value": "STO", "checked": false }] }, "tariffServiceCodeList": { "type": "text", "name": "Carrier Service Code", "data": null }, "orderExecutionBucket": { "type": "checkbox", "name": "Shipment Status", "data": [{ "name": "Transportation to be planned", "value": "TRANS_PLAN_UNASSIGNED", "checked": false }, { "name": "Transportation planning", "value": "TRANS_PLAN_OPEN", "checked": false }, { "name": "Transportation processing", "value": "TRANS_PLAN_PROCESSING", "checked": false }, { "name": "Transportation Carrier committed", "value": "TRANS_PLAN_TENDER_ACCEPTED", "checked": false }, { "name": "Shipment Created", "value": "SHIPMENT_CREATED", "checked": false }, { "name": "Checked in by DC", "value": "CHECK_IN", "checked": false }, { "name": "Loading Started", "value": "LOADING_STARTED", "checked": false }, { "name": "Ready for Pickup", "value": "TRANS_EXEC_READY_PICK_UP", "checked": false }, { "name": "In Transit", "value": "TRANS_EXEC_IN_TRANSIT", "checked": false }, { "name": "Delivery confirmed", "value": "TRANS_EXEC_DELIVERY_CONFIRMED", "checked": false }] }, "loadReadyDateTimeOriginTZ": { "type": "date", "dummytime": true, "timeformat": "T", "startTime": "[ 00:00:00.000]", "endTime": "[ 23:59:59.999]", "name": "Carrier Ready Date", "shortName": "Carr. Ready Date", "data": null } } }, "columns": { "network": [{ "title": "Dest City", "field": "destinationCity", "tableData": { "columnOrder": 0, "groupSort": "asc", "width": "calc((100% - (0px)) / 12)", "id": 0 }, "hidden": false }, { "title": "Order Status", "field": "orderStatus", "cellStyle": { "minWidth": 160 }, "tableData": { "columnOrder": 1, "groupSort": "asc", "width": "calc((100% - (0px)) / 12)", "id": 1 }, "hidden": false }, { "title": "Order Type", "field": "orderType", "tableData": { "columnOrder": 2, "groupSort": "asc", "width": "calc((100% - (0px)) / 12)", "id": 2 }, "hidden": false }, { "title": "Shipment Status", "field": "orderExecutionBucketDesc", "cellStyle": { "minWidth": 160 }, "tableData": { "columnOrder": 3, "groupSort": "asc", "width": "calc((100% - (0px)) / 12)", "id": 3 }, "hidden": false }, { "title": "Shipment #", "field": "shipmentNum", "cellStyle": { "minWidth": 160 }, "tableData": { "columnOrder": 4, "groupSort": "asc", "width": "calc((100% - (0px)) / 12)", "id": 4 }, "hidden": false }, { "title": "Order #", "field": "orderNum", "cellStyle": { "minWidth": 160 }, "tableData": { "columnOrder": 5, "groupSort": "asc", "width": "calc((100% - (0px)) / 12)", "id": 5 }, "hidden": false }, { "title": "Customer", "field": "customer", "tableData": { "columnOrder": 6, "groupSort": "asc", "width": "calc((100% - (0px)) / 12)", "id": 6 }, "hidden": false }, { "title": "Origin", "field": "origin", "cellStyle": { "minWidth": 160 }, "tableData": { "columnOrder": 7, "groupSort": "asc", "width": "calc((100% - (0px)) / 12)", "id": 7 }, "hidden": false }, { "title": "Dest State", "field": "destinationState", "tableData": { "columnOrder": 8, "groupSort": "asc", "width": "calc((100% - (0px)) / 12)", "id": 8 }, "hidden": false }, { "title": "Carrier Ready Date & Time", "field": "loadReadyDateTime", "cellStyle": { "minWidth": 160 }, "tableData": { "columnOrder": 9, "groupSort": "asc", "width": "calc((100% - (0px)) / 12)", "id": 9 }, "hidden": false }, { "title": "Carrier", "field": "tariffServiceCode", "tableData": { "columnOrder": 10, "groupSort": "asc", "width": "calc((100% - (0px)) / 12)", "id": 10 }, "hidden": false }, { "title": "Ship Condition", "field": "shippingCondition", "tableData": { "columnOrder": 11, "groupSort": "asc", "width": "calc((100% - (0px)) / 12)", "id": 11 }, "hidden": false }] }
    }

    const initialState = {
        showfavs: true,
        theme: "dark",
        filters: {},
    }
    it("Default State Reducer", async () => {
        expect(optionsReducer(undefined, { type: "UndefinedAction", payload: true })).toStrictEqual(initialState);
    });
    it("Show Hide Fav Reducer", async () => {

        expect(optionsReducer(undefined, { type: optionsConstants.SHOW_HIDE_FAVOURITES, payload: true })).toStrictEqual(
            {
                ...initialState,
                showfavs: true
            }
        )

    });

    it("Set Filters Reducer", async () => {

        const payload = { "filter": { "sites": { "type": "sites", "name": "Origin", "data": ["Test Site1", "Test Site 2"] } }, "type": "network", "subtype": "network" }

        expect(optionsReducer(undefined, { type: optionsConstants.SET_FILTERS, payload })).toStrictEqual(
            {
                ...initialState,
                filters: {
                    'network': {
                        ...networkfilters,
                        ...payload.filter
                    }
                }
            }
        )

    });

    it("Set Filters Reducer II", async () => {

        const payload = { "filter": { "sites": { "type": "sites", "name": "Origin", "data": ["Test Site1", "Test Site 2"] } }, "type": "network", "subtype": "network" }

        expect(optionsReducer({ ...stateForColumns, filters: {} }, { type: optionsConstants.SET_FILTERS, payload })).toStrictEqual(
            {
                ...stateForColumns,
                filters: {
                    'network': {
                        ...networkfilters,
                        ...payload.filter
                    }
                }
            }
        )

    });

    it("Reset Filters Reducer", async () => {

        const payload = { filter: {}, type: "network", subtype: "network" }

        expect(optionsReducer(undefined, { type: optionsConstants.RESET_FILTERS, payload })).toStrictEqual(
            {
                ...initialState,
                filters: {
                    ...initialState.filters,
                    'network': {
                        ...networkfilters
                    }
                }
            }
        )

    })

    it("Save Columns Reducer", async () => {

        const payload = { columns: stateForColumns.columns.network, subtype: "transport" }

        expect(optionsReducer(stateForColumns, { type: optionsConstants.SAVE_COLUMN_STATE, payload })).toStrictEqual(
            {
                ...stateForColumns,
                columns: {
                    ...stateForColumns.columns,
                    'transport': [
                        ...stateForColumns.columns.network
                    ]
                }
            }
        )

    })

    it("Save Columns Reducer II", async () => {

        const payload = { subtype: "transport" }

        expect(optionsReducer(stateForColumns, { type: optionsConstants.SAVE_COLUMN_STATE, payload })).toStrictEqual(
            {
                ...stateForColumns,
                columns: {
                    ...stateForColumns.columns,
                    'transport': []
                }
            }
        )

    })


    it("Reset Columns Reducer", async () => {

        const payload = { subtype: "network" }

        expect(optionsReducer(stateForColumns, { type: optionsConstants.RESET_COLUMN_STATE, payload })).toStrictEqual(
            {
                ...stateForColumns,
                columns: {}
            }
        )

    })

    it("Show Tabs By Reducer", async () => {

        const payload = "locations"

        expect(optionsReducer(stateForColumns, { type: optionsConstants.SET_VIEW_BY, payload })).toStrictEqual(
            {
                ...stateForColumns,
                showTabsBy: "locations"
            }
        )

    })

    it("Set Table Body", async () => {

        const payload = { body: { test: "value" }, tableName: "network" }

        expect(optionsReducer(stateForColumns, { type: optionsConstants.SET_TABLE_BODY, payload })).toStrictEqual(
            {
                ...stateForColumns,
                tablebody: { network: payload.body }
            }
        )

    })

})