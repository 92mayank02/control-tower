import React from 'react'
import NetworkView, { conditionalFilterReplacement, tableStyleConfig } from 'components/pages/NetworkView';
import { getTestElement, initiateGlobalAuthService, renderWithMockStore } from '../testUtils';
import { defaultNetworkColumns } from "../reduxLib/constdata/networkColumns"
import fetch from "../reduxLib/services/serviceHelpers";
import PerformanceMap from '../components/common/PerformanceMap';

jest.mock("../reduxLib/services/serviceHelpers")
jest.mock('../components/common/PerformanceMap', () => {
    return {
        __esModule: true,
        A: true,
        default: () => {
          return <div></div>;
        },
      };
})

describe('<NetworkView Component>', () => {

    afterAll(() => {
        jest.restoreAllMocks();
    });

    beforeAll(() => {
        initiateGlobalAuthService();
    });

    const props = {
        perfTableRedirection: jest.fn(),
        setDetails: jest.fn(),
        pageDetails: {
            filterParams: {
                state: null
            }
        },
        setRefresh: jest.fn(),
        type: "network"
    }

    const props2 = {
        setDetails: jest.fn(),
        pageDetails: {
            filterParams: {
                state: null
            }
        },
        setRefresh: jest.fn(),
        type: "mylocation"
    }

    const props3 = {
        setDetails: jest.fn(),
        pageDetails: {
            filterParams: {
                state: null
            }
        },
        setRefresh: jest.fn(),
        type: "2028"
    }


    it('NetworkView (Filter Condition Function)', async () => {
        let orderFilters = {
            //orderStatusFilters: ["ORDER_BLOCKED"],
            orderExecutionBucket: ["TRANS_PLAN_PROCESSING"]
        }

        await conditionalFilterReplacement(orderFilters)
        expect(orderFilters).toEqual(
            {
                "orderExecutionBucket": ["TRANS_PLAN_PLANNED", "TRANS_PLAN_TENDERED", "TRANS_PLAN_TENDER_REJECTED"],
            }
        );

        orderFilters = {
            orderStatusBucket: ["ORDER_BLOCKED"]
        }

        await conditionalFilterReplacement(orderFilters)
        expect(orderFilters).toEqual(
            {
                "orderStatusBucket": ["SO_BLOCKED_NON_TMS_PLANNED_IMMEDIATE_ACTION",
                "SO_BLOCKED_TMS_PLANNED_VISIBLE",
                "SO_BLOCKED_TMS_PLANNED_NOT_VISIBLE",
                "SO_BLOCKED_NON_TMS_PLANNED_PICKUP_NOT_SCHEDULED",
                "SO_BLOCKED_NON_TMS_PLANNED_PICKUP_MULTI_BLOCK",
                "SO_BACK_ORDER_BLOCKED"
              ],
            }
        );

        orderFilters = {
            orderStatusBucket: ["ORDER_BLOCK_FREE"]
        }

        await conditionalFilterReplacement(orderFilters)
        expect(orderFilters).toEqual(
            {
                "orderStatusBucket": ["SO_COMPLETELY_CONFIRMED_CUBE", "SO_NOT_COMPLETELY_CONFIRMED_CUBE", "SO_BACK_ORDER_BLOCK_FREE"],
            }
        );

        orderFilters = {
            orderStatusFilters: ["ORDER_BLOCKED"],
        }

        expect(conditionalFilterReplacement(orderFilters)).toEqual(
            {
                "orderStatusFilters": ["ORDER_BLOCKED"],
            }
        );

        orderFilters = {
            orderStatusFilters: ["ORDER_BLOCKED"],
        }

        expect(conditionalFilterReplacement(orderFilters)).toEqual(
            {
                "orderStatusFilters": ["ORDER_BLOCKED"],
            }
        );
    });

    it('NetworkView (Table Options Function)', async () => {
        const tableoptions = {
            rowStyle: {
                color: "#FFF"
            }
        }
        expect(tableStyleConfig.rowStyle({ orderHealthAndExecutionHealth: "RED" })).toEqual(
            {
                "@media (min-width:600px)": {
                    "fontWeight": "500",
                },
                "color": "#FFF",
                "borderLeft": "5px solid #FF4081",
                "flexGrow": 1,
                "fontFamily": "'Gilroy', sans_serif",
                "fontSize": "14px",
                "fontWeight": 400,
                "lineHeight": 1.5,
            }
        );
    });


    it('NetworkView (Column Configuration for "-" )', async () => {
        const data = {}
        expect(defaultNetworkColumns.columnConfiguration(data)).toEqual(
            {
                liveLoadInd: "-",
                appointmentRequired: "-",
                orderOnHoldInd: "-",
                deliverApptDateTime: "-",
                actualDeliveredDateTime: "-",
                loadReadyDateTime: "-",
                customer: "-",
                origin: "-",
                originId: "-",
                destinationCity: "-",
                destinationState: "-",
                orderStatusBucket: "-",
                shippingCondition: "-",
                reasonCodeString: "-",
                expectedDeliveryDateTime: "-",
                salesGroup: "-",
                salesOffice: "-",
                salesOrg: "-",
                distributionChannel: "-"

            }
        );
    });

    it('Network should render- No Data', async () => {

        fetch.mockImplementation(() => {
            return new Promise((resolve, reject) => {
                return resolve({
                    status: 200,
                    json: () => {
                        return { "onPage": 1, "pageSize": 5, "totalPages": 1, "totalRecords": 0, "orders": [] }
                    }
                })
            })
        })
        const { getByText, getByTestId } = await renderWithMockStore(NetworkView, props);
        const component = getTestElement('networkview');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
    });

    it('Network should render- No Data My Selections', async () => {

        fetch.mockImplementation(() => {
            return new Promise((resolve, reject) => {
                return resolve({
                    status: 200,
                    json: () => {
                        return { "onPage": 1, "pageSize": 5, "totalPages": 1, "totalRecords": 0, "orders": [] }
                    }
                })
            })
        })
        const { getByText, getByTestId } = await renderWithMockStore(NetworkView, props2);
        const component = getTestElement('networkview');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
    });

    it('Network should render- No Data Single Location', async () => {

        fetch.mockImplementation(() => {
            return new Promise((resolve, reject) => {
                return resolve({
                    status: 200,
                    json: () => {
                        return { "onPage": 1, "pageSize": 5, "totalPages": 1, "totalRecords": 0, "orders": [] }
                    }
                })
            })
        })
        const { getByText, getByTestId } = await renderWithMockStore(NetworkView, props3);
        const component = getTestElement('networkview');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
    });

});