import DetailsView from '../components/pages/DistributionView';
import NetworkView from '../components/pages/NetworkView';
import OrdersView from '../components/pages/OrdersView';
import TransportView from '../components/pages/TransportView';
import { getTestElement, renderWithMockStore, renderWithProvider, shallowSetup } from '../testUtils';


describe('<Parent View Component>', () => {
    afterAll(() => {
      jest.restoreAllMocks();
    });

    const detailsViewProps = {
        setDetails:jest.fn(),
        setRefresh:jest.fn(),
        pageDetails:{        
            DIST:true,
            filterParams:{}
        },
        refresh: { refresh: false, time: new Date(), siteNums:[] }
    }
    it('should render with label', async () => {
        const wrapper = shallowSetup(DetailsView,detailsViewProps);
        //await renderWithMockStore(DetailsView,detailsViewProps);
        //const component = getTestElement('distributionContainer');
        //expect(component).toBeInTheDocument();
        expect(wrapper.exists()).toBeTruthy();
    });

    const transportViewProps = {
        setDetails:jest.fn(),
        setRefresh:jest.fn(),
        pageDetails:{        
            TRANS_PLAN:true,
            filterParams:{}
        },
        refresh: { refresh: false, time: new Date(), siteNums:[] }
    }
    it('should render with label', async () => {
        const wrapper = shallowSetup(TransportView,transportViewProps);
        //await renderWithMockStore(DetailsView,detailsViewProps);
        //const component = getTestElement('distributionContainer');
        //expect(component).toBeInTheDocument();
        expect(wrapper.exists()).toBeTruthy();
    });


    const ordersViewProps = {
        setDetails:jest.fn(),
        setRefresh:jest.fn(),
        pageDetails:{        
            ORDER:true,
            filterParams:{}
        },
        refresh: { refresh: false, time: new Date(), siteNums:[] }
    }
    it('should render with label', async () => {
        const wrapper = shallowSetup(OrdersView,ordersViewProps);
        //await renderWithMockStore(DetailsView,detailsViewProps);
        //const component = getTestElement('distributionContainer');
        //expect(component).toBeInTheDocument();
        expect(wrapper.exists()).toBeTruthy();
    });

    const networkViewProps = {
        setDetails:jest.fn(),
        setRefresh:jest.fn(),
        pageDetails:{        
            NETWORK:true,
            filterParams:{}
        },
        refresh: { refresh: false, time: new Date(), siteNums:[] }
    }
    it('should render with label', async () => {
        const wrapper = shallowSetup(NetworkView,networkViewProps);
        //await renderWithMockStore(DetailsView,detailsViewProps);
        //const component = getTestElement('distributionContainer');
        //expect(component).toBeInTheDocument();
        expect(wrapper.exists()).toBeTruthy();
    });
});