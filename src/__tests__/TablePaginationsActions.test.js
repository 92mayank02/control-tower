import TablePaginationActions from 'components/common/Table/TablePaginationActions';
import { getTestElement, renderWithMockStore, fireEvent } from '../testUtils';


describe('<Table Paginations Component>', () => {

    afterAll(() => {
        jest.restoreAllMocks();
    });

    const props = {
        count: 10,
        page: 1,
        rowsPerPage: 10,
        onChangePage: jest.fn()
    }

    it('Table Paginations should render', async () => {
        await renderWithMockStore(TablePaginationActions, props);
        const component = getTestElement('tablepagination');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
    });


    it('Table Paginations Click', async () => {
        const { getByTestId } = await renderWithMockStore(TablePaginationActions, props);

        const component = getTestElement('tablepagination');
        fireEvent.click(getByTestId('first page'));
        fireEvent.click(getByTestId('previous page'));
        fireEvent.click(getByTestId('next page'));
        fireEvent.click(getByTestId('last page'));

        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();

    });

    

    it('Table Last/Next Paginations Click on Active Buttons', async () => {
        const { getByTestId } = await renderWithMockStore(TablePaginationActions, {
            ...props, 
            count: 100,
            page: 8,
            rowsPerPage: 10 
        });

        const component = getTestElement('tablepagination');
        fireEvent.click(getByTestId('last page'));
        fireEvent.click(getByTestId('next page'));

        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();

    });

});