import configureMockStore from 'redux-mock-store'
import { downloadService, resetDownload } from "../reduxLib/services/downloadService"
import thunk from 'redux-thunk'
import { transportSearch } from "../components/dummydata/mock"
import { shortTitleCellStyle } from '../helpers/tableStyleOverride';
import fetch from "../reduxLib/services/serviceHelpers";
jest.mock("../reduxLib/services/serviceHelpers")


const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)
const store = mockStore({})

const columns = {
  columnOrder: [
    { title: 'Load #', field: "shipmentNum", cellStyle: shortTitleCellStyle },
    { title: 'Order #', field: "orderNum", cellStyle: shortTitleCellStyle },
    { title: 'Carrier', field: "tariffServiceCode" },
    { title: 'Shipment Status', field: "orderExecutionBucketDesc", cellStyle: shortTitleCellStyle },
    { title: 'TM Status', field: "tmsShipmentStatus", cellStyle: shortTitleCellStyle },
    { title: 'Carrier Ready Date & Time', field: "loadReadyDateTime", cellStyle: shortTitleCellStyle },
    { title: 'Origin', field: "origin", cellStyle: shortTitleCellStyle },
    { title: 'Customer', field: "customer", cellStyle: shortTitleCellStyle },
    { title: 'Live Load', field: 'liveLoadInd' },
    { title: 'Order On Hold', field: "orderOnHoldInd" },
    { title: 'Cust Confirmed Delivery Date & Time', field: "deliverApptDateTime", cellStyle: shortTitleCellStyle },
    { title: 'Dest City', field: "destinationCity" },
    { title: 'Dest St', field: "destinationState" },
    { title: 'Ship Condition', field: "shippingCondition" },
    { title: 'Appt Type', field: "appointmentType" },
    { title: 'Appt Req\'d', field: "appointmentRequired" },
    { title: 'Appt Status', field: "appointmentStatusDesc" },
    { title: 'Suspended Status', field: "shipmentSuspendedStatus", cellStyle: shortTitleCellStyle }

  ],
  columnConfiguration: (d) => {
    return {
      origin: d?.orderOrigin?.name ? `${get(d, "orderOrigin.name", "-")} (${get(d, 'orderOrigin.id', '-')})` : `${get(d, 'orderOrigin.id', '-')}`,
      liveLoadInd: d.liveLoadInd ? d.liveLoadInd === "Y" ? "Yes" : "No" : "-",
      appointmentRequired: d.appointmentRequired ? d.appointmentRequired === "Y" ? "Yes" : "No" : "-",
      orderOnHoldInd: d.orderOnHoldInd ? d.orderOnHoldInd === "Y" ? "Yes" : "No" : "-",
      deliverApptDateTime: (d?.deliverApptDateTime && d?.destinationTimeZone) ? moment(d.deliverApptDateTime)?.tz(d.destinationTimeZone)?.format(`MMM DD YYYY HH:mm z`) : "-",
      loadReadyDateTime: (d?.loadReadyDateTime && d?.originTimeZone) ? moment(d?.loadReadyDateTime)?.tz(d?.originTimeZone)?.format(`MMM DD YYYY HH:mm z`) : "-",
      customer: formatToSentenceCase(get(d, "orderDestination.name", "-")),
      appointmentStatusDesc: formatToSentenceCase(get(d, "appointmentStatusDesc", "-")),
      appointmentType: formatToSentenceCase(get(d, "appointmentType", "-")),
      destinationCity: formatToSentenceCase(get(d, "orderDestination.city", "-")),
      destinationState: get(d, "orderDestination.state", "-"),
      shipmentSuspendedStatus: get(d, "shipmentSuspendedStatus", "-"),
    }
  }
}

describe('Download Service ', () => {

  it('Download Service Main Test', () => {
    store.dispatch(downloadService({
      config: {
        totalRecords: 0,
        body: {},
        url: "/",
        columns: []
      },
      type: "network",
      subtype: "network",
      itemInfoRequired: false,
      detailsProcess: jest.fn(),
      detailsColumns: [],
      process: jest.fn()
    }));
    expect(store.getActions()).toMatchSnapshot();
  });


  it('Download Table Component (Test API Response Handling)', async () => {

    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return resolve({
          status: 200,
          ok: true,
          json: () => {
            return transportSearch
          }
        })
      })
    })
    store.dispatch(downloadService({
      config: {
        totalRecords: 1,
        body: {},
        url: "/",
        columns: columns.columnOrder
      },
      type: "network",
      subtype: "transport",
      detailsProcess: jest.fn(),
      detailsColumns: [],
      process: jest.fn()
    }));
    expect(store.getActions()).toMatchSnapshot();
  });


  it('Download Table Component (Test API Response Handling)', async () => {

    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return resolve({
          status: 200,
          ok: false,
          json: () => {
            return {}
          }
        })
      })
    })
    store.dispatch(downloadService({
      config: {
        totalRecords: 1,
        body: {},
        url: "/",
        columns: columns.columnOrder
      },
      type: "network",
      subtype: "transport",
      process: jest.fn(),
      detailsProcess: jest.fn(),
      detailsColumns: [],
    }));
    expect(store.getActions()).toMatchSnapshot();
  });

  it('Download Table Component (Test API Payload Error Response Handling)', async () => {

    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return resolve({
          status: 200,
          data: () => {
            return {}
          }
        })
      })
    })
    store.dispatch(downloadService({
      config: {
        totalRecords: -1,
        body: {},
        url: "/",
        columns: columns.columnOrder
      },
      type: "network",
      subtype: "transport",
      process: jest.fn(),
      detailsProcess: jest.fn(),
      detailsColumns: []
    }));
    expect(store.getActions()).toMatchSnapshot();
  });

  it('Download Reset', () => {
    store.dispatch(resetDownload({
      type: "network",
      subtype: "network",
    }));
    expect(store.getActions()).toMatchSnapshot();
  });

});