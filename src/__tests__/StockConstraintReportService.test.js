import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { fetchShipmentList, fetchShipmentMaterial, searchShipment, fetchRecomendation } from "../reduxLib/services"
const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)
import fetch from "../reduxLib/services/serviceHelpers";
import { mockStockConstraintReport } from 'components/dummydata/mock'

jest.mock("../reduxLib/services/serviceHelpers")

describe('<StockConstraintReportService>', () => {
  let store;

  beforeEach(() => {
    store = mockStore({});
  });

  it('Fetch Shipment List - Success', () => {
    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return resolve({
          status: 200,
          json: () => {
            return mockStockConstraintReport.shipmentFullListResponse 
          }
        })
      })
    })

    return store.dispatch(fetchShipmentList({ 
      "region":"NA",
      "siteNum":"2323",
      "reportWindowInDays":4
     }))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })

  it('Fetch Shipment List - Failed', () => {
    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return resolve({
          status: 500,
          json: () => {
            return {}
          }
        })
      })
    })

    return store.dispatch(fetchShipmentList({ 
      "region":"NA",
      "siteNum":"2323",
      "reportWindowInDays":4
     }))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })

  it('Fetch Shipment Materials - Success', () => {
    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return resolve({
          status: 200,
          json: () => {
            return mockStockConstraintReport.shipmentMaterialsResponse 
          }
        })
      })
    })

    return store.dispatch(fetchShipmentMaterial({ 
      "region":"NA",
      "siteNum":"2323",
      "shipmentNum":"543132345"
     }))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })

  it('Fetch Shipment Materials - Failed', () => {
    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return resolve({
          status: 500,
          json: () => {
            return {}
          }
        })
      })
    })

    return store.dispatch(fetchShipmentMaterial({ 
      "region":"NA",
      "siteNum":"2323",
      "shipmentNum":"543132345"
     }))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })

  it('Fetch Shipment Search - Success', () => {
    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return resolve({
          status: 200,
          json: () => {
            return mockStockConstraintReport.shipmentSearchResponse
          }
        })
      })
    })

    return store.dispatch(searchShipment({ 
      "region":"NA",
      "siteNum":"2323",
      "shipmentNum":"543132345"
     }))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })

  it('Fetch Shipment Search - Failed', () => {
    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return resolve({
          status: 500,
          json: () => {
            return {}
          }
        })
      })
    })

    return store.dispatch(searchShipment({ 
      "region":"NA",
      "siteNum":"2323",
      "shipmentNum":"543132345"
     }))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })

  it('Fetch Shipment Recommendation - Success', () => {
    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return resolve({
          status: 200,
          json: () => {
            return mockStockConstraintReport.shipmentRecommendationResponse
          }
        })
      })
    })

    return store.dispatch(fetchRecomendation({ 
      "region":"NA",
      "siteNum":"2323",
      "shipmentNum":"543132345",
      "materialNum": "109137193",
      "plannedLoadingStartDatetime": "2022-01-05T23:00:00.000Z"
     }))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })

  it('Fetch Shipment Recommendation - Failed', () => {
    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return resolve({
          status: 500,
          json: () => {
            return {}
          }
        })
      })
    })

    return store.dispatch(fetchRecomendation({ 
      "region":"NA",
      "siteNum":"2323",
      "shipmentNum":"543132345",
      "materialNum": "109137193",
      "plannedLoadingStartDatetime": "2022-01-05T23:00:00.000Z"
     }))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })
})