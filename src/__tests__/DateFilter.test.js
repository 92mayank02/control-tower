import DateFilter from 'components/common/DateFilter';
import { getTestElement, renderWithMockStore, fireEvent } from '../testUtils';
import { saveFilters } from "../reduxLib/services";
jest.mock("../reduxLib/services")

describe('<DateFilter>', () => {
    afterAll(() => {
        jest.restoreAllMocks();
    });

    const filterprops = {
        "type": "network",
        "subtype": "network",
        "filterKey": "loadReadyDateTimeOriginTZ",
        "saveFilters": jest.fn()
    };

    it('DateFilter should render', async () => {
        await renderWithMockStore(DateFilter, filterprops);
        const component = getTestElement('dateFilterElement');
        expect(component).toBeInTheDocument();
    });

    
    it(' Select and set Dates ', async () => {
        const {getByLabelText } = await renderWithMockStore(DateFilter, filterprops);
        const component = getTestElement('dateFilterElement');
        expect(component).toBeInTheDocument();
        fireEvent.change(getByLabelText("startdate"), {target: {value: '2021-09-24'}})
        fireEvent.change(getByLabelText("startdate"), {target: {value: '2021-09-26'}})
        //userEvent.type(screen.getByLabelText("search"), '{enter}')
        saveFilters.mockImplementation(() => {
            return () => ({
                type:"ADD_FILTER_SUCCESS",
                payload: "true"
            })
        })
        //expect(saveFilters).toBeCalledTimes(1)
    });

});