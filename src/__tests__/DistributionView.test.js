import DistributionView from 'components/pages/DistributionView';
import { getTestElement, initiateGlobalAuthService, renderWithMockStore } from '../testUtils';
import useMediaQuery from "@material-ui/core/useMediaQuery"
import { mockStoreValue } from '../components/dummydata/mock';
import { fireEvent, waitFor } from '@testing-library/react';

jest.mock("@material-ui/core/useMediaQuery")
jest.mock("../reduxLib/services/getAllGuardRailsService",() =>({
  ...jest.requireActual("../reduxLib/services/getAllGuardRailsService"),
  getAllGuardrails : () => {
      return {
          type:"ADD_FILTER_SUCCESS",
          payload: "true"
      }
  }
}))

describe('<Distribution View Component>', () => {

    afterAll(() => {
        jest.restoreAllMocks();
    });

    beforeAll(() => {
        initiateGlobalAuthService();
    });

    const mockdata = [
        {
          "siteNum": "2023",
          "unit": "LOADS", // Possible values "Loads" or "Hours"
          "weekdays": [
            {
              "day": "Sunday",
              "orderBusinessUnit": [
                {
                  "businessUnit": "CONSUMER",
                  "guardrailFL": 2023,
                  "guardrailLTL": 18
                },
                {
                  "businessUnit": "PROFESSIONAL",
                  "guardrailFL": 36,
                  "guardrailLTL": 36
                },
                {
                  "businessUnit": "ALL",
                  "guardrailFL": 36,
                  "guardrailLTL": 36
                }
              ]
            },
            {
              "day": "Monday",
              "orderBusinessUnit": [
                {
                  "businessUnit": "CONSUMER",
                  "guardrailFL": 19,
                  "guardrailLTL": 19
                },
                {
                  "businessUnit": "PROFESSIONAL",
                  "guardrailFL": 33,
                  "guardrailLTL": 23
                },
                {
                  "businessUnit": "ALL",
                  "guardrailFL": 23,
                  "guardrailLTL": 23
                }
              ]
            }
          ]
        },
        {
            "siteNum": "2027",
            "unit": "LOADS", // Possible values "Loads" or "Hours"
            "weekdays": [
              {
                "day": "Sunday",
                "orderBusinessUnit": [
                  {
                    "businessUnit": "CONSUMER",
                    "guardrailFL": 2027,
                    "guardrailLTL": 12
                  },
                  {
                    "businessUnit": "PROFESSIONAL",
                    "guardrailFL": 45,
                    "guardrailLTL": 45
                  },
                  {
                    "businessUnit": "ALL",
                    "guardrailFL": 67,
                    "guardrailLTL": 67
                  }
                ]
              },
              {
                "day": "Monday",
                "orderBusinessUnit": [
                  {
                    "businessUnit": "CONSUMER",
                    "guardrailFL": 13,
                    "guardrailLTL": 13
                  },
                  {
                    "businessUnit": "PROFESSIONAL",
                    "guardrailFL": 88,
                    "guardrailLTL": 88
                  },
                  {
                    "businessUnit": "ALL",
                    "guardrailFL": 99,
                    "guardrailLTL": 99
                  }
                ]
              }
            ]
          },
          {
            "siteNum": "2022",
            "unit": "LOADS", // Possible values "Loads" or "Hours"
            "weekdays": [
              {
                "day": "Sunday",
                "orderBusinessUnit": [
                  {
                    "businessUnit": "CONSUMER",
                    "guardrailFL": 2022,
                    "guardrailLTL": 56
                  },
                  {
                    "businessUnit": "PROFESSIONAL",
                    "guardrailFL": 66,
                    "guardrailLTL": 66
                  },
                  {
                    "businessUnit": "ALL",
                    "guardrailFL": 66,
                    "guardrailLTL": 66
                  }
                ]
              },
              {
                "day": "Monday",
                "orderBusinessUnit": [
                  {
                    "businessUnit": "CONSUMER",
                    "guardrailFL": 88,
                    "guardrailLTL": 88
                  },
                  {
                    "businessUnit": "PROFESSIONAL",
                    "guardrailFL": 88,
                    "guardrailLTL": 88
                  },
                  {
                    "businessUnit": "ALL",
                    "guardrailFL": 99,
                    "guardrailLTL": 99
                  }
                ]
              }
            ]
          }
      ]

    const props = {
        setDetails: jest.fn(),
        pageDetails: { filterParams: {
            state: null
        } },
        setRefresh: jest.fn(),
        type: "network"
    }

    const props2 = {
        setDetails: jest.fn(),
        pageDetails: { filterParams: {
            state: null
        } },
        setRefresh: jest.fn(),
        type: 2034
    }

    const props3 = {
      setDetails: jest.fn(),
      pageDetails: { filterParams: {
          state: null,
          navHistory: [{id: 'DIST', label: 'Distribution', type: '2028'}]
      } },
      goToStockConstraint: jest.fn(),
      type: 2028,
      // subtype: 'stockConstraintReport',
      saveFilters: jest.fn(),

  }

    it('Distribution View should render', async () => {
        await renderWithMockStore(DistributionView, props, {...mockStoreValue,guardrails:{...mockStoreValue.guardrails, allSiteGuardrails:mockdata}});
        const component = getTestElement('distributionContainer');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
    });

    it('Distribution View should render -- Click Edit Button', async () => {
      const { getByText, getByTestId } = await renderWithMockStore(DistributionView, props2, {...mockStoreValue,guardrails:{...mockStoreValue.guardrails, allSiteGuardrails: {data:mockdata, allowedToEdit:true}, disableButton:false}});
      const component = getTestElement('distributionContainer');
      expect(component).toBeInTheDocument();
      expect(component).toBeTruthy();
      // await fireEvent.click(getByTestId("button_fetch"))
      // waitFor(()=>getByTestId("closeicon"))
      // expect(getByTestId("closeicon")).toBeInTheDocument()
    });

    it('Distribution View should render -- Click Edit Button - No Snack', async () => {
      const { getByText, getByTestId } = await renderWithMockStore(DistributionView, props2, {...mockStoreValue,guardrails:{...mockStoreValue.guardrails, allSiteGuardrails: {data:mockdata, allowedToEdit:false}, disableButton:false}});
      const component = getTestElement('distributionContainer');
      expect(component).toBeInTheDocument();
      expect(component).toBeTruthy();
      // await fireEvent.click(getByTestId("button_fetch"))
      // waitFor(()=>getByTestId("closeicon"))
      // expect(getByTestId("closeicon")).toBeInTheDocument()
    });

    it('Distribution View should render -- Click Close button Button', async () => {
      const { getByText, getByTestId } = await renderWithMockStore(DistributionView, props2, {...mockStoreValue,guardrails:{...mockStoreValue.guardrails, allSiteGuardrails: {data:mockdata, allowedToEdit:true}, disableButton:false}});
      const component = getTestElement('distributionContainer');
      expect(component).toBeInTheDocument();
      expect(component).toBeTruthy();
      // await fireEvent.click(getByTestId("button_fetch"))
      // waitFor(()=>getByTestId("closeicon"))
      // expect(getByTestId("closeicon")).toBeInTheDocument()
      // fireEvent.click(getByTestId("closeicon"))
      // expect(component).toBeInTheDocument();
    });
    it('Distribution View should render - Mobile Resolution', async () => {
        
        useMediaQuery.mockImplementation(() => () => true)
        await renderWithMockStore(DistributionView, props, {...mockStoreValue,guardrails:{...mockStoreValue.guardrails, allSiteGuardrails: {data:mockdata, allowedToEdit:true}}});
        const component = getTestElement('distributionContainer');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
    });

    xit('Distribution View should render - Single Location', async () => {
        await renderWithMockStore(DistributionView, props2, {...mockStoreValue,guardrails:{...mockStoreValue.guardrails, allSiteGuardrails: {data:mockdata, allowedToEdit:true}}});
        const component = getTestElement('guardrailTitle');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
    });

  //   it('Distribution View should render - Single Location - Stock Constraint', async () => {
  //     const { getByTestId } = await renderWithMockStore(DistributionView, props3, {...mockStoreValue, guardrails:{...mockStoreValue.guardrails, allSiteGuardrails: {data:mockdata, allowedToEdit:true}}});
  //     const component = getTestElement('scReport');
  //     expect(component).toBeInTheDocument();
  //     expect(component).toBeTruthy();
  //     fireEvent.click(getByTestId("scReport"))
  //     waitFor(()=>getByTestId("stockConstraintReport"))
  //     // expect(getTestElement("stockConstraintReportMock")).toBeInTheDocument()

  // });

});