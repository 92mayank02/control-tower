import getLocationsReducer from "../reduxLib/reducers/getLocationsReducer";
import { getLocationsConstants } from '../reduxLib/constants';

describe("Get Locations Reducer Functions", () => {

    afterEach(() => {
        jest.clearAllMocks();
    });

    const defaultState = { loading: false, locations: [], performance_loading: false, salesOffice: [], customer:{ noData:false, data:[]}  }
    const successPayload = {
        performance_loading: false,
        "dc": [{ "siteNum": "2047", "region": "NA", "type": "DC", "plantType": "RDC", "alias": "LADC", "shortName": "Los Angeles DC", "siteName": "S_2047_917", "siteNameDesc": "Los Angeles DC ExtOps/DC", "location": { "id": "2047", "name": "Los Angeles DC", "city": "ONTARIO", "state": "CA", "country": "USA", "street": "4815 S HELLMAN AVE", "geoLocation": { "longitude": -117.602477, "latitude": 34.040893 }, "timezone": "US/Pacific", "postalCode": "91762" } }],
        "mill": [{ "siteNum": "2031", "region": "NA", "type": "MILL", "plantType": "KCNA Mill", "alias": "NMC", "shortName": "New Milford Mill", "siteName": "S_2031_067", "siteNameDesc": "New Milford Global Sales Mill", "location": { "id": "2031", "name": "New Milford Mill", "city": "NEW MILFORD", "state": "CT", "country": "USA", "street": "58 PICKETT DIST ROAD", "geoLocation": { "longitude": -73.4111248, "latitude": 41.5579034 }, "timezone": "US/Eastern", "postalCode": "06776-4413" } }]
    }

    const customerSearchSucessfulPayload = [
        {
            customerName: "WalmartCanada",
            salesOrgList: [

                {
                    salesOrg: "2810",
                    distributionChannel: "80"
                },
                {
                    salesOrg: "2810",
                    distributionChannel: "90",
                },

                {
                    salesOrg: "2820",
                    distributionChannel: "80",
                }

            ]

        }
    ]

    const salesOfficeSearchSucessfulPayload = [

        {

            "salesOffice": 'CS01',

            "salesOfficeName": "pharma",

            "salesGroupList": ["Z01", "Z02", "Z03"],
        
        "salesOrgList": [

                {
                    "salesOrg" :  "2810",
        
                    "distributionChannel" : "80"
        
        },

                {

                    "salesOrg": "2810",

                    "distributionChannel": "90",

                },

                {

                    "salesOrg": "2820",

                    "distributionChannel": "80",

                }

            ]

        },

        {

            salesOffice: 'CS02',

            salesOfficeName: "Retail",

            "salesGroupList": ["Z01", "Z02", "Z03"],
        
        "salesOrgList": [

                {
                    "salesOrg" : "2810",
        
        "salesOrg" : "2810",

                    "distributionChannel": "90",

                },

                {

                    "salesOrg": "2820",

                    "distributionChannel": "80",

                }

            ]

        }

    ]
    it("Default Switch Return", async () => {
        expect(getLocationsReducer(undefined, { type: "Action not listed", payload: successPayload })).toStrictEqual({
            ...defaultState
        });
    });
    it("Locations Success Reducer", async () => {
        expect(getLocationsReducer(undefined, {
            type: getLocationsConstants.LOCATION_FETCH_SUCCESS,
            payload: successPayload,
        })).toStrictEqual({
            ...defaultState, locations: [...successPayload.dc, ...successPayload.mill]
        });
    });
    it("Locations Loading Reducer", async () => {
        expect(getLocationsReducer(undefined, {
            type: getLocationsConstants.LOADING,
            payload: successPayload,
        })).toStrictEqual({
            ...defaultState, loading: successPayload
        });
    });
    it("Locations Failed Reducer", async () => {
        expect(getLocationsReducer(undefined, {
            type: getLocationsConstants.LOCATION_FETCH_FAILED,
            payload: false,
        })).toStrictEqual({
            ...defaultState, locations: []
        });
    });

    it("Performance SUccess?Failure Reducer", async () => {
        expect(getLocationsReducer(undefined, {
            type: getLocationsConstants.PERFORMANCE_FETCH_SUCCESS,
            payload: [],
        })).toStrictEqual({
            ...defaultState, performance: []
        });
    });

    it("Performance Loading Reducer", async () => {
        expect(getLocationsReducer(undefined, {
            type: getLocationsConstants.PEFRORMANCE_LOADING,
            payload: true,
        })).toStrictEqual({
            ...defaultState, performance_loading: true
        });
    });

    it("Customer SUccess Reducer", async () => {
        expect(getLocationsReducer(undefined, {
            type: getLocationsConstants.CUSTOMER_FETCH_SUCCESS,
            payload: customerSearchSucessfulPayload,
        })).toStrictEqual({
            ...defaultState, customer:{ noData:false, data:customerSearchSucessfulPayload}
             
        });

        expect(getLocationsReducer(undefined, {
            type: getLocationsConstants.CUSTOMER_FETCH_SUCCESS,
            payload: [],
        })).toStrictEqual({
            ...defaultState, customer:{ noData:true, data:[]}
             
        });
    });

    it("Customer Failed Reducer", async () => {
        expect(getLocationsReducer(undefined, {
            type: getLocationsConstants.CUSTOMER_FETCH_FAILED,
            payload: false,
        })).toStrictEqual({
            ...defaultState, customer:{ noData:true, data:[]}
        });

    });

    it("Sales Office SUccess Reducer", async () => {
        expect(getLocationsReducer(undefined, {
            type: getLocationsConstants.SALESOFC_FETCH_SUCCESS,
            payload: salesOfficeSearchSucessfulPayload,
        })).toStrictEqual({
            ...defaultState, salesOffice: salesOfficeSearchSucessfulPayload
        });
    });

    it("Sales Office Failed Reducer", async () => {
        expect(getLocationsReducer(undefined, {
            type: getLocationsConstants.SALESOFC_FETCH_FAILED,
            payload: false,
        })).toStrictEqual({
            ...defaultState, salesOffice: []
        });
    });



})