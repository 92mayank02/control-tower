import ErrorBox from '../components/common/ErrorBox';
import { getTestElement, renderWithProvider } from '../testUtils';


describe('<Error Box>', () => {
    afterAll(() => {
      jest.restoreAllMocks();
    });
  
    const props = {dimensions:{height:0,width:0}}
    it('should render with label', async () => {
      await renderWithProvider(ErrorBox,props);
      const component = getTestElement('errorBox');
      expect(component).toBeInTheDocument();
    });

});