import configureMockStore from 'redux-mock-store'
import {
    addItem,
    addItemsLocations,
    removeItem,
    clearAll,
    removeFavs,
    clearSelected,
    changeTab,
    addItemCuso,
    addItemsCuso,
    removeItemCuso,
    clearAllCuso,
    addItemsCustomer,
    removeFavsCuso,
    joinFavstoCuso,
    setBlockOnTabChange
} from "../reduxLib/services/selectionsActions"
import thunk from 'redux-thunk'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)
const store = mockStore({})
describe('Selection Actions', () => {
    it('Add Item ', () => {
        store.dispatch(addItem(
            {
                dummyPayload:
                {
                    "id": 1,
                    "title": "Example",
                    "project": "Testing",
                    "createdAt": "2017-03-02T23:04:38.003Z",
                    "modifiedAt": "2017-03-22T16:44:29.034Z"
                }
            }
        ));
        expect(store.getActions()).toMatchSnapshot();
    });

    it('Add Customers and SO ', () => {
        store.dispatch(addItemCuso(
            {
                dummyPayload:
                {
                    "id": 1,
                    "title": "Example",
                    "project": "Testing",
                    "createdAt": "2017-03-02T23:04:38.003Z",
                    "modifiedAt": "2017-03-22T16:44:29.034Z"
                }
            }
        ));
        expect(store.getActions()).toMatchSnapshot();
    });

    it('Add Multiple Customers and SO ', () => {
        store.dispatch(addItemsCuso(
            {
                dummyPayload:
                {
                    "id": 1,
                    "title": "Example",
                    "project": "Testing",
                    "createdAt": "2017-03-02T23:04:38.003Z",
                    "modifiedAt": "2017-03-22T16:44:29.034Z"
                }
            }
        ));
        expect(store.getActions()).toMatchSnapshot();
    });

    it('Add Items', () => {
        store.dispatch(addItemsLocations(
            {
                dummyPayload:
                {
                    "id": 1,
                    "title": "Example",
                    "project": "Testing",
                    "createdAt": "2017-03-02T23:04:38.003Z",
                    "modifiedAt": "2017-03-22T16:44:29.034Z"
                }
            }
        ));
        expect(store.getActions()).toMatchSnapshot();
    });

    it('Add Items Customer', () => {
        store.dispatch(addItemsCustomer(
            {
                dummyPayload:
                {
                    "id": 1,
                    "title": "Example",
                    "project": "Testing",
                    "createdAt": "2017-03-02T23:04:38.003Z",
                    "modifiedAt": "2017-03-22T16:44:29.034Z"
                }
            }
        ));
        expect(store.getActions()).toMatchSnapshot();
    });

    it('Remove Favs Cuso', () => {
        store.dispatch(removeFavsCuso(
            {
                dummyPayload:
                {
                    "id": 1,
                    "title": "Example",
                    "project": "Testing",
                    "createdAt": "2017-03-02T23:04:38.003Z",
                    "modifiedAt": "2017-03-22T16:44:29.034Z"
                }
            }
        ));
        expect(store.getActions()).toMatchSnapshot();
    });

    it('Join Favs Cuso', () => {
        store.dispatch(joinFavstoCuso(
            {
                dummyPayload:
                {
                    "id": 1,
                    "title": "Example",
                    "project": "Testing",
                    "createdAt": "2017-03-02T23:04:38.003Z",
                    "modifiedAt": "2017-03-22T16:44:29.034Z"
                }
            }
        ));
        expect(store.getActions()).toMatchSnapshot();
    });

    it('Remove Items', () => {
        store.dispatch(removeItem(
            {
                dummyPayload:
                {
                    "id": 1,
                    "title": "Example",
                    "project": "Testing",
                    "createdAt": "2017-03-02T23:04:38.003Z",
                    "modifiedAt": "2017-03-22T16:44:29.034Z"
                }
            }
        ));
        expect(store.getActions()).toMatchSnapshot();
    });

    it('Remove Cust and SO', () => {
        store.dispatch(removeItemCuso(
            {
                dummyPayload:
                {
                    "id": 1,
                    "title": "Example",
                    "project": "Testing",
                    "createdAt": "2017-03-02T23:04:38.003Z",
                    "modifiedAt": "2017-03-22T16:44:29.034Z"
                }
            }
        ));
        expect(store.getActions()).toMatchSnapshot();
    });

    it('Clear All', () => {
        store.dispatch(clearAll(
            {
                dummyPayload: {
                    data: {
                        "id": 1,
                        "title": "Example",
                        "project": "Testing",
                        "createdAt": "2017-03-02T23:04:38.003Z",
                        "modifiedAt": "2017-03-22T16:44:29.034Z"
                    }
                }
            }));
        expect(store.getActions()).toMatchSnapshot();
    });

    it('Clear All CU and SO', () => {
        store.dispatch(clearAllCuso(
            {
                dummyPayload: {
                    data: {
                        "id": 1,
                        "title": "Example",
                        "project": "Testing",
                        "createdAt": "2017-03-02T23:04:38.003Z",
                        "modifiedAt": "2017-03-22T16:44:29.034Z"
                    }
                }
            }));
        expect(store.getActions()).toMatchSnapshot();
    });

    it('Remove Favourites', () => {
        store.dispatch(removeFavs(
            {
                dummyPayload:
                {
                    "id": 1,
                    "title": "Example",
                    "project": "Testing",
                    "createdAt": "2017-03-02T23:04:38.003Z",
                    "modifiedAt": "2017-03-22T16:44:29.034Z"
                }
            }
        ));
        expect(store.getActions()).toMatchSnapshot();
    });
    it('Change Tab', () => {
        store.dispatch(changeTab(
            {
                dummyPayload:
                {
                    "id": 1,
                    "title": "Example",
                    "project": "Testing",
                    "createdAt": "2017-03-02T23:04:38.003Z",
                    "modifiedAt": "2017-03-22T16:44:29.034Z"
                }
            }
        ));
        expect(store.getActions()).toMatchSnapshot();
    });

    
    it('Change Tab', () => {
        store.dispatch(setBlockOnTabChange());
        expect(store.getActions()).toMatchSnapshot();
    });

    it('Clear Selected', () => {
        store.dispatch(clearSelected(
            {
                dummyPayload:
                {
                    "id": 1,
                    "title": "Example",
                    "project": "Testing",
                    "createdAt": "2017-03-02T23:04:38.003Z",
                    "modifiedAt": "2017-03-22T16:44:29.034Z"
                }
            }
        ));
        expect(store.getActions()).toMatchSnapshot();
    });
});