import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { getChartsService, getGuardrailsService } from "../reduxLib/services/getChartsService"
const middlewares = [ thunk ]
const mockStore = configureMockStore(middlewares)
import mockGuardrailsDetails from 'components/dummydata/mock'
import fetch from "../reduxLib/services/serviceHelpers";

jest.mock("../reduxLib/services/serviceHelpers")
// ,()=>{
  // const fetch = (url,{method, body}) => {
  //   return new Promise((resolve,reject) => {
  //       if(body.status===200){
  //         return resolve({
  //           status:200,
  //           json: () => {
  //             return {data:{"ORDER":[{"state":"SO_BLOCK_FREE","stateDesc":"Customer Orders Block free","totalCount":35802,"redCount":51,"yellowCount":35655},{"state":"SO_BLOCKED","stateDesc":"Customer Orders Blocked","totalCount":163,"redCount":163,"yellowCount":0},{"state":"STO","stateDesc":"Stock Transfer Orders","totalCount":14,"redCount":9,"yellowCount":0}]}}
  //           }
  //         })
  //       }
  //       else if(body.status===500){
  //         return resolve({
  //           status:500,
  //           json: () => {
  //             return {data:{}}
  //           }
  //         })
  //       }
  //       else{
  //         return reject("Failed")
  //       }
  //   })
  // }
  // return fetch
//})

describe('async actions', () => {
  let store;

  beforeEach(() => {
    store = mockStore({});
  });

  it('Get Chart Services Data Success', () => {
    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return resolve({
          status:200,
          json: () => {
            return {
              data:{"ORDER":[{"state":"SO_BLOCK_FREE","stateDesc":"Customer Orders Block free","totalCount":35802,"redCount":51,"yellowCount":35655},{"state":"SO_BLOCKED","stateDesc":"Customer Orders Blocked","totalCount":163,"redCount":163,"yellowCount":0},{"state":"STO","stateDesc":"Stock Transfer Orders","totalCount":14,"redCount":9,"yellowCount":0}]}
            }
          }
        })
      })
    })

    return store.dispatch(getChartsService({
        body: {
            "status":200,
            "region": "NA",
            "subtype": "ORDERS",
        }, type: "network", subtype: "ORDERS",
    }))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })

  it('Get Chart Services Data Failed from Server Side', () => {
    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return reject({
          status: 500,
          json: () => {
            return { data: {} }
          }
        })
      })
    })

    return store.dispatch(getChartsService({
      body: {
        "status":500,
        "region": "NA",
        "subtype": "ORDERS",
    }, type: "network", subtype: "ORDERS",
    }))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })

  it('Get Chart Services Data Failed from Client Side', () => {
    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return resolve({
          status: 400
        })
      })
    })

    return store.dispatch(getChartsService({
        body: {
            "status":400,
            "region": "NA",
            "subtype": "ORDERS",
        }, type: "network", subtype: "ORDERS",
    }))
      .then(() => {
        expect(store.getActions()).toMatchSnapshot();
      })
  })

  // Guardrails Chart UT
  it('Get Guardrails Chart Services Data Success', () => {
    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return resolve({
          status:200,
          json: () => {
            return {
              state : mockGuardrailsDetails
            }
          }
        })
      })
    })

    return store.dispatch(getGuardrailsService({
        body: {
          "siteNum": 2034
        }
    }))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })

  it('Get Guardrails Chart Services Data Failed - Server Side', () => {
    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return reject({
          status: 500,
          json: () => {
            return { data: {} }
          }
        })
      })
    })

    return store.dispatch(getGuardrailsService({
      body:{
        siteNum: 2034
      }
    }))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })

  it('Get Guardrails Chart Services Data Failed - Client Side', () => {
    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return resolve({
          status: 400
        })
      })
    })
    return store.dispatch(getGuardrailsService({
        body: {
            "siteNum": 2034
        }
    }))
      .then(() => {
        expect(store.getActions()).toMatchSnapshot();
      })
  })

});