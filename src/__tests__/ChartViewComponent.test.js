
import { NetworkOrderChartBuilder } from '../components/common/NetworkOrderChartBuilder';
import { NetworkChartBuilder } from '../components/common/NetworkChartBuilder';
import { AppointmentDetailsChartBuilder } from '../components/common/AppointmentDetailsChartBuilder';
import { DeliveryDetailsChartBuilder } from '../components/common/DeliveryDetailsChartBuilder';
import { PlanningDetailsChartBuilder } from '../components/common/PlanningDetailsChartBuilder';
import { SuspendedDetailsChartBuilder } from '../components/common/SuspendedDetailsChartBuilder';
import { DistributionInboundChartBuilder } from '../components/common/DistributionInboundChartBuilder';
import { DistributionOutboundSCChartBuilder } from '../components/common/DistributionOutboundSCChartBuilder';
import getChartsService from "../reduxLib/services/getChartsService";
import {ChartComponent} from "../components/common/ChartComponent";
import { getTestElement, renderWithProvider, fireEvent } from '../testUtils';
import { BlockedTmsPlannedChartBuilder } from 'components/common/BlockedTmsPlannedChartBuilder';
import { BlockedNonTmsPlannedChartBuilder } from 'components/common/BlockedNonTmsPlannedChartBuilder';


jest.mock("../reduxLib/services/getChartsService", () => {
    const getChartsService = () => true;
    return getChartsService
})

const rest = { refresh: { refresh: false, time: new Date(), siteNums: [] }, setRefresh: (newRefreshObj) => rest.refresh = newRefreshObj }


describe('<All Charts Rendering Component>', () => {
    afterAll(() => {
        jest.restoreAllMocks();
    });

    const chartProps = {
        ...rest,
        getChartsService,
        setHeaderTotalCount: jest.fn(),
        charts: [{ "state": "TRANS_PLAN_TO_BE_PLANNED", "stateDesc": "To be planned", "totalCount": 297, "redCount": 297 }, { "state": "TRANS_PLAN_PLANNING", "stateDesc": "Planning", "totalCount": 4, "redCount": 3 }, { "state": "TRANS_PLAN_PROCESSING", "stateDesc": "Processing", "totalCount": 4, "redCount": 4 }, { "state": "TRANS_PLAN_CONFIRMED", "stateDesc": "Confirmed", "totalCount": 2, "redCount": 2 }]
    }

    const transportPlanningNoData = [{"state":"TRANS_PLAN_UNASSIGNED","stateDesc":"Unassigned","totalCount":0,"redCount":0,"greenCount":0},{"state":"TRANS_PLAN_OPEN","stateDesc":"Open","totalCount":0,"redCount":0,"greenCount":0},{"state":"TRANS_PLAN_PLANNED","stateDesc":"Planned","totalCount":0,"redCount":0,"greenCount":0},{"state":"TRANS_PLAN_TENDERED","stateDesc":"Tendered","totalCount":0,"redCount":0,"greenCount":0},{"state":"TRANS_PLAN_TENDER_REJECTED","stateDesc":"Tender Rejected","totalCount":0,"redCount":0,"greenCount":0},{"state":"TRANS_PLAN_TENDER_ACCEPTED","stateDesc":"Tender Accepted","totalCount":0,"redCount":0,"greenCount":0}]

    const transportPlanningEmptyResponse = []

    const donutProps = {
        chartsData: [{ "state": "SO_BLOCK_FREE", "stateDesc": "Customer Orders Block free", "totalCount": 35800, "redCount": 1, "yellowCount": 35755 }, { "state": "SO_BLOCKED", "stateDesc": "Customer Orders Blocked", "totalCount": 163, "redCount": 163, "yellowCount": 0 }, { "state": "STO", "stateDesc": "Stock Transfer Orders", "totalCount": 14, "redCount": 0, "yellowCount": 0 }],
        setHeaderTotalCount: jest.fn()
    }


    const builderProps = {
        keys:["blueCount", "redCount"],
        chartsData:chartProps.charts,
        setHeaderTotalCount: jest.fn()
    }

    const blockedOrderBuilderProps = {
        keys:["blueCount", "redCount"],
        chartsData : [{"state":"SO_BLOCKED_TMS_PLANNED_VISIBLE","stateDesc":"Visible in TMS","totalCount":16,"redCount":16,"yellowCount":0},{"state":"SO_BLOCKED_TMS_PLANNED_NOT_VISIBLE","stateDesc":"Not Visible in TMS","totalCount":58,"redCount":55,"yellowCount":0},{"state":"SO_BLOCKED_NON_TMS_PLANNED_PICKUP_NOT_SCHEDULED","stateDesc":"Pickup Not Scheduled","totalCount":74,"redCount":74,"yellowCount":0},{"state":"SO_BLOCKED_NON_TMS_PLANNED_IMMEDIATE_ACTION","stateDesc":"Immediate Action","totalCount":365,"redCount":365,"yellowCount":0},{"state":"SO_BLOCKED_NON_TMS_PLANNED_PICKUP_MULTI_BLOCK","stateDesc":"Pickup/Package Multi Block","totalCount":209,"redCount":208,"yellowCount":0}],
        setHeaderTotalCount: jest.fn()
    }

    it('Render Donut component', async () => {
        await renderWithProvider(NetworkOrderChartBuilder, donutProps);
        const component = getTestElement('networkorderchartbuilder');
        expect(component).toBeInTheDocument();
    });
    it('Render Chart Component', async () => {
        const {getByText} = await renderWithProvider(ChartComponent, { ...chartProps, showLegends:true, showViewDetails:true, BuilderComponent:NetworkChartBuilder });
        const component = getTestElement('chartElement')
        expect(component).toBeInTheDocument();
        fireEvent.click(getByText("View Details"))
        expect(getByText("View Details")).toBeInTheDocument()


    });

    it('Render Chart Component with SiteNum', async () => {
        await renderWithProvider(ChartComponent, { ...chartProps, globalFilterSelection:{siteNums:["1122","2280"]}, showLegends:true, showViewDetails:true, BuilderComponent:NetworkChartBuilder });
        const component = getTestElement('chartElement')
        expect(component).toBeInTheDocument();
    });

    it('Render Chart Component with CUSO', async () => {
        await renderWithProvider(ChartComponent, { ...chartProps, globalFilterSelection:{customerOrSalesOffice:[    {
            "selectionType": "SALES_OFFICE",
            "salesOrg": 2010,
            "distributionChannel": 90,
            "salesOffice": "CS01"
          },
          {
            "selectionType": "SALES_OFFICE",
            "salesOrg": 2010,
            "distributionChannel": 90,
            "salesOffice": "CS02"
          },
          {
            "selectionType": "SALES_OFFICE",
            "salesOrg": 2020,
            "distributionChannel": 90,
            "salesOffice": "CS03",
            "salesGroup": [
              "Z01",
              "Z02",
              "Z03"
            ]
          }]}, showLegends:true, showViewDetails:true, BuilderComponent:NetworkChartBuilder });
        const component = getTestElement('chartElement')
        expect(component).toBeInTheDocument();
    });

    it('Render Transport Planning Details Status component', async () => {
        await renderWithProvider(PlanningDetailsChartBuilder, { ...builderProps, chartsData:transportPlanningNoData });
        const component = getTestElement('transportPlanningStatus')
        expect(component).toBeInTheDocument();
    });

    it('Render Transport Planning Details Status component', async () => {
        await renderWithProvider(PlanningDetailsChartBuilder, { ...builderProps, chartsData:transportPlanningEmptyResponse });
        const component = getTestElement('transportPlanningStatus')
        expect(component).toBeInTheDocument();
    });

    it('Render Transport Planning Details Status component', async () => {
        await renderWithProvider(PlanningDetailsChartBuilder, { ...builderProps });
        const component = getTestElement('transportPlanningStatus')
        expect(component).toBeInTheDocument();
    });
    it('Render Transport Appointment Status component', async () => {
        await renderWithProvider(AppointmentDetailsChartBuilder, { ...builderProps });
        const component = getTestElement('transportAppointmentStatus')
        expect(component).toBeInTheDocument();
    });
    it('Render Transport Delviery Status component', async () => {
        await renderWithProvider(DeliveryDetailsChartBuilder, { ...builderProps });
        const component = getTestElement('transportDeliveryStatus')
        expect(component).toBeInTheDocument();
    });
    it('Render Transport Suspended Status component', async () => {
        await renderWithProvider(SuspendedDetailsChartBuilder, { ...builderProps });
        const component = getTestElement('transportSuspendedStatus')
        expect(component).toBeInTheDocument();
    });

    it('Render BlockedNonTmsPlannedChartBuilder with Blocks component', async () => {
        await renderWithProvider(BlockedNonTmsPlannedChartBuilder, {...blockedOrderBuilderProps});
        const component = getTestElement('blockedNonTmsPlanned')
        expect(component).toBeInTheDocument();
    });

    it('Render BlockedTmsPlannedChartBuilder with Blocks component', async () => {
        await renderWithProvider(BlockedTmsPlannedChartBuilder, {...blockedOrderBuilderProps});
        const component = getTestElement('blockedTmsPlanned')
        expect(component).toBeInTheDocument();
    });

    
    it('Render BlockedNonTmsPlannedChartBuilder with Blocks component -2', async () => {
        await renderWithProvider(BlockedNonTmsPlannedChartBuilder, {...blockedOrderBuilderProps, chartsData:undefined});
        const component = getTestElement('blockedNonTmsPlanned')
        expect(component).toBeInTheDocument();
    });

    it('Render BlockedTmsPlannedChartBuilder with Blocks component -2 ', async () => {
        await renderWithProvider(BlockedTmsPlannedChartBuilder, {...blockedOrderBuilderProps, chartsData:undefined});
        const component = getTestElement('blockedTmsPlanned')
        expect(component).toBeInTheDocument();
    });

    it('Render Distribution Outbound component', async () => {
        await renderWithProvider(DistributionOutboundSCChartBuilder, {...builderProps});
        const component = getTestElement('distributionOutbound')
        expect(component).toBeInTheDocument();
    });
    it('Render Distribution Inbound', async () => {
        await renderWithProvider(DistributionInboundChartBuilder, {...builderProps});
        const component = getTestElement('distributionInbound')
        expect(component).toBeInTheDocument();
    });
});