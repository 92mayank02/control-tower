import { BlockedFreeOrderTreeMapBuilder } from 'components/common/BlockedFreeOrderTreeMapBuilder';
import { getTestElement, renderWithProvider, waitFor, fireEvent, userEvent } from '../testUtils';


describe('<OrdersBoxDesign Component>', () => {

    afterAll(() => {
        jest.restoreAllMocks();
    });

    const props = {
        chartsData: [
            { "state": "SO_COMPLETELY_CONFIRMED_CUBE", "stateDesc": "100% Confirmed Cube", "totalCount": 100 },
            { "state": "SO_NOT_COMPLETELY_CONFIRMED_CUBE", "stateDesc": "Less than 100% Confirmed Cube", "totalCount": 100, "yellowCount": 20, "redCount": 40, "greenCount":40 }
        ],
        setDetails:jest.fn(),
        type:'network',
        subtype:"SO_BLOCKED"
    }

    const props2 = {
        chartsData: [],
        setDetails:jest.fn(),
        type:'network',
        subtype:"SO_BLOCKED"
    }

    it('OrdersBoxDesign should render - click Healthy Tooltip', async () => {
        const { getByTestId, getByText } = await renderWithProvider(BlockedFreeOrderTreeMapBuilder, props);
        const component = getTestElement('ordersboxdesign');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
        fireEvent.mouseOver(getByTestId("blockfreehealthy"));
        await waitFor(() => getByText('Healthy : 40'))
        expect(getByText('Healthy : 40')).toBeInTheDocument()
        userEvent.click(getByText('Healthy : 40'))
        
    });

    it('OrdersBoxDesign should render - click Healthy Tooltip -2', async () => {
        const { getByTestId, getByText } = await renderWithProvider(BlockedFreeOrderTreeMapBuilder, props2);
        const component = getTestElement('ordersboxdesign');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
    });

    it('OrdersBoxDesign should render - click unHealthy Tooltip', async () => {
        const { getByTestId, getByText } = await renderWithProvider(BlockedFreeOrderTreeMapBuilder, props);
        const component = getTestElement('ordersboxdesign');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
        fireEvent.mouseOver(getByTestId("blockfreeunhealthy"));
        await waitFor(() => getByText('Critical Risk : 40'))
        expect(getByText('Critical Risk : 40')).toBeInTheDocument()
        userEvent.click(getByText('Critical Risk : 40'))
    });

    it('OrdersBoxDesign should render --  click Risk Tooltip', async () => {
        const { getByTestId, getByText } = await renderWithProvider(BlockedFreeOrderTreeMapBuilder, props);
        const component = getTestElement('ordersboxdesign');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
        fireEvent.mouseOver(getByTestId("blockfreerisk"));
        await waitFor(() => getByText('Potential Risk : 20'))
        expect(getByText('Potential Risk : 20')).toBeInTheDocument()
        userEvent.click(getByText('Potential Risk : 20'))
    });
});