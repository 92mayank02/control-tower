import { TabPanel, GetViews, MainLayout } from 'theme/layouts/MainLayout';
import { getTestElement, renderWithMockStore, renderWithProvider, shallowSetup } from '../testUtils';
import { mockStoreValue } from '../components/dummydata/mock'

describe('< Main Layout >', () => {
    afterAll(() => {
        jest.restoreAllMocks();
    });

    xit('should render with Tab Panel', async () => {
        const props = {
            children: GetViews,
            value: 1,
            index: 1
        }
        await renderWithProvider(TabPanel, props);
        const component = getTestElement('tabpanel');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
    });

    it('should render with MainLayout', async () => {
        const props = {
            items: [2022]
        }
        const wrapper = shallowSetup(MainLayout, props);
        expect(wrapper.exists()).toBeTruthy();
    });

    it('should render with Get Views', async () => {
        const props = {
            type: "network",
            siteNums: ["2033", "2092"],
            globalRefresh: null,
            setDetails: jest.fn(),
            pageDetails: {
                "NETWORK": true,
                filterParams: {}
            }
        }

        const wrapper = shallowSetup(GetViews, props);
        expect(wrapper.exists()).toBeTruthy();
    });
});