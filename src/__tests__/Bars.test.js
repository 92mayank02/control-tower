import Bars from '../components/D3Charts/Bars';
import { getTestElement, renderWithProvider } from '../testUtils';
import { scaleOrdinal } from 'd3'
import { waitFor } from '@testing-library/react';


document.createRange = () => ({
  setStart: () => { },
  setEnd: () => { },
  commonAncestorContainer: {
    nodeName: "BODY",
    ownerDocument: document,
  },
})


describe('<Bars>', () => {
  afterAll(() => {
    jest.restoreAllMocks();
  });

  class xScale {
    constructor(x) {
      this.x = x;
      return 1000
    }
  }

  var _old = xScale;
  xScale = function (...args) {
    return new _old(...args)
  };

  xScale.bandwidth = function () {
    return 2000;
  }

  const BarsProps = {
    xAccessor: jest.fn(),
    margins: {
      top: 10,
      right: 10,
      bottom: 40,
      left: 100,
      margin: 10
    },
    height: 160,
    colors: jest.fn(),
    setDetails: jest.fn(),
    horizontal: false,
    keys: ["blueCount", "yellowCount", "redCount"],
    xKey: "statedesc",
    data: [
      {
        "state": "TRANS_EXEC_READY_PICK_UP",
        "stateDesc": "Ready for pickup",
        "totalCount": 10,
        "redCount": 20,
        "yellowCount": 10,
        "blueCount": 30,
        "total": 60
      },
      {
        "state": "TRANS_EXEC_IN_TRANSIT",
        "stateDesc": "In Transit",
        "totalCount": 10,
        "redCount": 20,
        "blueCount": 30,
        "yellowCount": 10,
        "total": 60
      },
      {
        "state": "TRANS_EXEC_DELIVERY_CONFIRMED",
        "stateDesc": "Delivery Confirmed",
        "totalCount": 10,
        "redCount": 10,
        "yellowCount": 10,
        "blueCount": 10,
        "total": 30
      }
    ],
    type: "network",
    subtype: "TRANS_PLAN",
    scales: {
      xScale: xScale,
      yScale: jest.fn()
    }
  }

  // it('should render with label - xScale', async () => {
  //   await renderWithProvider(Bars, BarsProps);
  //   const component = getTestElement('bars');
  //   waitFor(()=>component)
  //   expect(component).toBeInTheDocument();
  //   expect(component).toBeTruthy();
  // });

  const yProps = {
    ...BarsProps,
    horizontal: true,
    scales: {
      xScale: xScale,
      yScale: xScale
    }
  }


  xit('should render with label - yScale', async () => {
    await renderWithProvider(Bars, yProps);
    const component = getTestElement('bars');
    waitFor(()=>component)
    expect(component).toBeInTheDocument();
    expect(component).toBeTruthy();
  });


  // it('Mouseover Test', async () => {
  //   const wrapper = await renderWithProvider(Bars, BarsProps);
  //   const rect = await document.querySelector("rect")

  //   const test = rect.dispatchEvent(new MouseEvent("mouseover", { bubbles: true }))

  //   expect(test).toBeTruthy();
  // });

  // it('Click Test Tooltip Test Red Click', async () => {
  //   const wrapper = await renderWithProvider(Bars, BarsProps);
  //   const rect = await document.querySelector("rect")
  //   await rect.dispatchEvent(new MouseEvent("mouseover", { bubbles: true }))
  //   const click = await wrapper.findByTitle("clicktextred");
  //   const test = await click.dispatchEvent(new MouseEvent('click', { bubbles: true }));
  //   expect(test).toBeTruthy();
  // });

  // it('Click Test Tooltip Test Blue Click', async () => {
  //   const wrapper = await renderWithProvider(Bars, BarsProps);
  //   const rect = await document.querySelector("rect")
  //   await rect.dispatchEvent(new MouseEvent("mouseover", { bubbles: true }))
  //   const click = await wrapper.findByTitle("clicktextblue");
  //   const test = await click.dispatchEvent(new MouseEvent('click', { bubbles: true }));
  //   expect(test).toBeTruthy();
  // });

});