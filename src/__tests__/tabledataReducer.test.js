import tabledataReducer from "../reduxLib/reducers/tabledataReducer";
import { tabledataConstants } from '../reduxLib/constants';

describe("Feedback Reducer Functions", () => {

    afterEach(() => {
        jest.clearAllMocks();
    });

    const defaultState = { loading: true, data: {} }
    const successPayload = {}

    it("Default Switch Return", async () => {
        expect(tabledataReducer(undefined, { type: "Action not listed", payload: successPayload })).toStrictEqual({
            ...defaultState
        });
    });



    it("Table Loading Reducer", async () => {
        expect(tabledataReducer(undefined, {
            type: tabledataConstants.LOADING,
            payload: false,
        })).toStrictEqual({
            ...defaultState,
            loading: false,
            data: {}
        });
        expect(tabledataReducer(undefined, {
            type: tabledataConstants.LOADING,
            payload: true,
        })).toStrictEqual({
            ...defaultState,
            loading: true,
            data: {}
        });
    });

    it("Table Success/Failure", async () => {
        expect(tabledataReducer(undefined, {
            type: tabledataConstants.TABLE_DATA_FETCH_SUCCESS,
            payload: [{ name: "test" }],
            variables: { test: "value1" }
        })).toStrictEqual({
            ...defaultState,
            data: [{ name: "test" }],
            test: "value1"
        });

        expect(tabledataReducer(undefined, {
            type: tabledataConstants.TABLE_DATA_FETCH_FAILURE,
            payload: {},
            variables: { test: "value1" }
        })).toStrictEqual({
            ...defaultState,
            data: {},
            test: "value1"
        });
    });




})