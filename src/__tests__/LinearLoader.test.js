import LinearLoader from '../components/common/LinearLoader';
import { getTestElement, renderWithProvider } from '../testUtils';


describe('<Linear Loader>', () => {
    afterAll(() => {
      jest.restoreAllMocks();
    });
  
    const props = {}
    it('should render with label', async () => {
      await renderWithProvider(LinearLoader,props);
      const component = getTestElement('linearLoader');
      expect(component).toBeInTheDocument();
    });

});