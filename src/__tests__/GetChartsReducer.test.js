import getChartsReducer from "../reduxLib/reducers/getChartsReducer";
import { optionsConstants, chartsConstants } from '../reduxLib/constants';
import { mockGuardrailsDetails } from "components/dummydata/mock";

describe("Get Charts Reducer Functions",() => {

    afterEach(() => {
        jest.clearAllMocks();
    });

    const defaultState = {loaders: {}, charts: []}
    const variables = {"body":{"region":"NA","subtype":"TRANS_EXEC","sites":null},"type":"network","subtype":"TRANS_EXEC"};
    const successPayload = {"TRANS_EXEC":[{"state":"TRANS_EXEC_READY_PICK_UP","stateDesc":"Ready for pickup","totalCount":1,"redCount":1},{"state":"TRANS_EXEC_IN_TRANSIT","stateDesc":"In Transit","totalCount":1,"redCount":1},{"state":"TRANS_EXEC_DELIVERY_CONFIRMED","stateDesc":"Delivery Confirmed","totalCount":1,"redCount":0}]}

    it("Default State Reducer", async () => {
        expect(getChartsReducer(undefined,{type:"Action Paload Without Variables", payload: true})).toStrictEqual(defaultState);        
    });

    it("Default Switch Return", async () => {
        expect(getChartsReducer(undefined,{type:"Action Paload Without Variables", payload: true, variables:true})).toStrictEqual(defaultState);        
    });
    it("Chart Success Reducer", async () => {
        expect(getChartsReducer(undefined,{
            type: chartsConstants.CHART_DATA_FETCH_SUCCESS,
            payload: successPayload,
            variables
        })).toStrictEqual({ 
            ...defaultState, charts: {...defaultState.charts, "network": {"TRANS_EXEC": successPayload.TRANS_EXEC} }
        });        
    });
    it("Chart Loading Reducer", async () => {
        expect(getChartsReducer(undefined,{
            type: chartsConstants.LOADING,
            payload: false,
            variables
        })).toStrictEqual({         
            ...defaultState, loaders: { "network": { "TRANS_EXEC": {loading: false}}}
        });      
    });
    it("Chart Failed Reducer", async () => {
        expect(getChartsReducer(undefined,{
            type: chartsConstants.CHART_DATA_FETCH_FAILED,
            payload: [],
            variables
        })).toStrictEqual({         
            ...defaultState, charts: {...defaultState.charts, "network": {"TRANS_EXEC": null} }
        });  
    });


    // Guardrail Reducers

    it("Guardrail Chart - Success", async () => {
        expect(getChartsReducer(undefined,{
            type: chartsConstants.GUARDRAIL_DATA_FETCH_SUCCESS,
            payload: mockGuardrailsDetails,
            variables: {body: {siteNum: 2034}}
        })).toStrictEqual({ 
            ...defaultState, charts: {...defaultState.charts, "guardrails": mockGuardrailsDetails }
        });        
    });

    it("Guardrail Chart - Loading", async () => {
        expect(getChartsReducer(undefined,{
            type: chartsConstants.GUARDRAIL_LOADING,
            payload: false,
            variables: {body: {siteNum: 2034}}
        })).toStrictEqual({         
            ...defaultState, loaders: { guardrails: {loading: false}}
        });      
    });

    it("Guardrail Chart - Failed", async () => {
        expect(getChartsReducer(undefined,{
            type: chartsConstants.GUARDRAIL_DATA_FETCH_FAILED,
            payload: [],
            variables: {body: {siteNum: 2034}}
        })).toStrictEqual({         
            ...defaultState, charts: {...defaultState.charts, guardrails: null}
        });      
    });

})