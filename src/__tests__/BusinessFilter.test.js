import { mockStoreValue } from 'components/dummydata/mock';
import BusinessFilter from 'components/header/BusinessFilter';
import { getTestElement, renderWithMockStore, fireEvent, waitFor } from '../testUtils';
import useMediaQuery from "@material-ui/core/useMediaQuery"
jest.mock('@material-ui/core/useMediaQuery')

describe('<BusinessFilter Component>', () => {

    afterAll(() => {
        jest.restoreAllMocks();
    });

    let props = {
        handleTabHideShow:jest.fn(),
    }

    it('BusinessFilter Should Render', async () => {
        
        await renderWithMockStore(BusinessFilter, props);
        const component = getTestElement('businessfilter');
        expect(component).toBeTruthy();
    });

    it('BusinessFilter Should Render - Mobile ', async () => {

        await renderWithMockStore(BusinessFilter, props);
        const component = getTestElement('businessfilter');
        Object.defineProperty(window, 'innerWidth', {
            writable: true,
            configurable: true,
            value: 375,
          });
    
        window.dispatchEvent(new Event('resize'));
        await renderWithMockStore(BusinessFilter, props);
        expect(component).toBeTruthy();
    });

    it('BusinessFilter Uncheck Click', async () => {
        await renderWithMockStore(BusinessFilter, props, { ...props, favorites: { tempBusinessUnit: ["CONSUMER", "PROFESSIONAL"] } });
        getTestElement('businessfilter');
        const item = document.querySelector("[name='uncheck']");
        const test = await item.dispatchEvent(new MouseEvent('click', { bubbles: true }));
        expect(test).toBeTruthy();
    });

    it('BusinessFilter Fav Click', async () => {
        await renderWithMockStore(BusinessFilter, props);
        getTestElement('businessfilter');
        const item = document.querySelector("[name='fav']");
        const test = await item.dispatchEvent(new MouseEvent('click', { bubbles: true }));
        expect(test).toBeTruthy();
    });

    it('BusinessFilter unfav Click', async () => {
        await renderWithMockStore(BusinessFilter, props);
        getTestElement('businessfilter');
        const item = document.querySelector("[name='unfav']");
        const test = await item.dispatchEvent(new MouseEvent('click', { bubbles: true }));
        expect(test).toBeTruthy();
    });

    it('Tick icon', async () => {
        await renderWithMockStore(BusinessFilter,{handleTabHideShow:jest.fn()}, {...mockStoreValue, favorites:{...mockStoreValue.favorites,"businessUnits": [],"tempBusinessUnit": ["CONSUMER"],}})
        const component = getTestElement('businessfilter');
        const { getAllByTestId } = await renderWithMockStore(BusinessFilter,{handleTabHideShow:jest.fn()}, mockStoreValue);
        fireEvent.click(getAllByTestId('checkicon')[0]);
        expect(component).toBeInTheDocument();
    });

    it('Tick icon', async () => {
        await renderWithMockStore(BusinessFilter,{handleTabHideShow:jest.fn()}, mockStoreValue)
        const component = getTestElement('businessfilter');
        const { getAllByTestId, getByTestId } = await renderWithMockStore(BusinessFilter,{handleTabHideShow:jest.fn()}, mockStoreValue);
        fireEvent.click(getAllByTestId('uncheckicon')[0]);
        waitFor(() => getByTestId("snackbar"));


    });

    it('Tick icon - Mock Mobile', async () => {
        useMediaQuery.mockImplementation(() => {
            return {
                __esModule: true,
                default: () => {
                  return true;
                },
              };
          })
        await renderWithMockStore(BusinessFilter,{handleTabHideShow:jest.fn()}, mockStoreValue)
        const { getAllByTestId, getByTestId } = await renderWithMockStore(BusinessFilter,{handleTabHideShow:jest.fn()}, mockStoreValue);
        fireEvent.click(getAllByTestId('bumobiletab')[0]);
        waitFor(() => getByTestId("snackbar"));


    });

});