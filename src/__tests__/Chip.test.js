//import '@testing-library/jest-dom/extend-expect'
import KCChip from '../components/common/Chip';
import { getTestElement, renderWithProvider } from '../testUtils';


describe('<Chip>', () => {
    afterAll(() => {
      jest.restoreAllMocks();
    });
  
    const props = {}
    it('should render with label', async () => {
      await renderWithProvider(KCChip,props);
      const breadcrumbComponent = getTestElement('chip');
      expect(breadcrumbComponent).toBeInTheDocument();
    });

});