import CustomizedTooltips from '../components/common/CustomizedTooltip';
import { getTestElement, renderWithProvider} from '../testUtils';
import React from 'react';

describe('<Tooltip Body>', () => {
    afterAll(() => {
      jest.restoreAllMocks();
    });
  
    const props = {title:'testcomponent'}
    it('should render with label', async () => {
      await renderWithProvider(CustomizedTooltips,props);
      const component = getTestElement('tooltipBody');
      expect(component).toBeInTheDocument();
    });

});