import stockConstraintReportReducer from "../reduxLib/reducers/stockConstraintReportReducer";
import { stockConstraintReportConstants } from '../reduxLib/constants';
import { mockStockConstraintReport } from "components/dummydata/mock";

describe("<stockConstraintReportReducer>",() => {

  afterEach(() => {
      jest.clearAllMocks();
  });

  const defaultState = { 
    shipment_list_loading: false, 
    shipmentList: [],
    shipment_material_loading: false,
    shipmentMaterials: {},
    shipment_data_loading: false,
    shipment_data: {},
    recomendation_data_loading: false,
    recomendation_data: []
  }

  it("Default Switch Return", async () => {
    expect(stockConstraintReportReducer(undefined, { type: "Action not listed", payload: [] })).toStrictEqual({
        ...defaultState
    });
  });

  it("Fetch Shipment List - Success Reducer", async () => {
    expect(stockConstraintReportReducer(undefined,{
        type: stockConstraintReportConstants.FETCH_SHIPMENT_LIST_SUCCESS,
        payload: mockStockConstraintReport.shipmentFullListResponse,
        variables: { "region":"NA", "siteNum":"2323", "reportWindowInDays":4 }
    })).toStrictEqual({         
        ...defaultState, shipmentList: mockStockConstraintReport.shipmentFullListResponse
    });      
  });

  it("Fetch Shipment List - Loading Reducer", async () => {
    expect(stockConstraintReportReducer(undefined,{
        type: stockConstraintReportConstants.SHIPMENT_LIST_LOADING,
        payload: false,
        variables: { "region":"NA", "siteNum":"2323", "reportWindowInDays":4 }
    })).toStrictEqual({         
        ...defaultState, shipment_list_loading: false
    });      
  });

  it("Fetch Shipment List - Failed Reducer", async () => {
    expect(stockConstraintReportReducer(undefined,{
        type: stockConstraintReportConstants.SHIPMENT_LIST_FAILED,
        payload: [],
        variables: { "region":"NA", "siteNum":"2323", "reportWindowInDays":4 }
    })).toStrictEqual({         
        ...defaultState, shipmentList: []
    });      
  });


  it("Fetch Shipment Materials - Success Reducer", async () => {
    expect(stockConstraintReportReducer(undefined,{
        type: stockConstraintReportConstants.FETCH_SHIPMENT_MATERIALS_SUCCESS,
        payload: mockStockConstraintReport.shipmentMaterialsResponse,
        variables: { "region":"NA", "siteNum":"2323", "shipmentNum":"543132345" }
    })).toStrictEqual({         
        ...defaultState, shipmentMaterials: mockStockConstraintReport.shipmentMaterialsResponse
    });      
  });

  it("Fetch Shipment Materials - Loading Reducer", async () => {
    expect(stockConstraintReportReducer(undefined,{
        type: stockConstraintReportConstants.SHIPMENT_MATERIALS_LOADING,
        payload: false,
        variables: { "region":"NA", "siteNum":"2323", "shipmentNum":"543132345" }
    })).toStrictEqual({         
        ...defaultState, shipment_material_loading: false
    });      
  });

  it("Fetch Shipment Materials - Failed Reducer", async () => {
    expect(stockConstraintReportReducer(undefined,{
        type: stockConstraintReportConstants.SHIPMENT_MATERIALS_FAILED,
        payload: {},
        variables: { "region":"NA", "siteNum":"2323", "shipmentNum":"543132345" }
    })).toStrictEqual({         
        ...defaultState, shipmentMaterials: {}
    });      
  });


  it("Fetch Shipment Search - Success Reducer", async () => {
    expect(stockConstraintReportReducer(undefined,{
        type: stockConstraintReportConstants.SEARCH_SHIPMENT_SUCCESS,
        payload: mockStockConstraintReport.shipmentSearchResponse,
        variables: { "region":"NA", "siteNum":"2323", "shipmentNum":"543132345" }
    })).toStrictEqual({         
        ...defaultState, shipment_data: mockStockConstraintReport.shipmentSearchResponse
    });      
  });

  it("Fetch Shipment Search - Loading Reducer", async () => {
    expect(stockConstraintReportReducer(undefined,{
        type: stockConstraintReportConstants.SEARCH_SHIPMENT_LOADING,
        payload: false,
        variables: { "region":"NA", "siteNum":"2323", "shipmentNum":"543132345" }
    })).toStrictEqual({         
        ...defaultState, shipment_data_loading: false
    });      
  });

  it("Fetch Shipment Search - Failed Reducer", async () => {
    expect(stockConstraintReportReducer(undefined,{
        type: stockConstraintReportConstants.SEARCH_SHIPMENT_FAILED,
        payload: {},
        variables: { "region":"NA", "siteNum":"2323", "shipmentNum":"543132345" }
    })).toStrictEqual({         
        ...defaultState, shipment_data: {}
    });      
  });


  it("Fetch Shipment Search - Success Reducer", async () => {
    expect(stockConstraintReportReducer(undefined,{
        type: stockConstraintReportConstants.SEARCH_SHIPMENT_SUCCESS,
        payload: mockStockConstraintReport.shipmentSearchResponse,
        variables: { "region":"NA", "siteNum":"2323", "shipmentNum":"543132345" }
    })).toStrictEqual({         
        ...defaultState, shipment_data: mockStockConstraintReport.shipmentSearchResponse
    });      
  });

  it("Fetch Shipment Search - Loading Reducer", async () => {
    expect(stockConstraintReportReducer(undefined,{
        type: stockConstraintReportConstants.SEARCH_SHIPMENT_LOADING,
        payload: false,
        variables: { "region":"NA", "siteNum":"2323", "shipmentNum":"543132345" }
    })).toStrictEqual({         
        ...defaultState, shipment_data_loading: false
    });      
  });

  it("Fetch Shipment Search - Failed Reducer", async () => {
    expect(stockConstraintReportReducer(undefined,{
        type: stockConstraintReportConstants.SEARCH_SHIPMENT_FAILED,
        payload: {},
        variables: { "region":"NA", "siteNum":"2323", "shipmentNum":"543132345" }
    })).toStrictEqual({         
        ...defaultState, shipment_data: {}
    });      
  });


  it("Fetch Shipment Recommendation - Success Reducer", async () => {
    expect(stockConstraintReportReducer(undefined,{
        type: stockConstraintReportConstants.FETCH_RECOMENDATIONS_SUCCESS,
        payload: mockStockConstraintReport.shipmentRecommendationResponse,
        variables: { "region":"NA", "siteNum":"2323", "shipmentNum":"543132345", "materialNum": "109137193", "plannedLoadingStartDatetime": "2022-01-05T23:00:00.000Z" }
    })).toStrictEqual({         
        ...defaultState, recomendation_data: mockStockConstraintReport.shipmentRecommendationResponse
    });      
  });

  it("Fetch Shipment Recommendation - Loading Reducer", async () => {
    expect(stockConstraintReportReducer(undefined,{
        type: stockConstraintReportConstants.RECOMENDATIONS_LOADING,
        payload: false,
        variables: { "region":"NA", "siteNum":"2323", "shipmentNum":"543132345", "materialNum": "109137193", "plannedLoadingStartDatetime": "2022-01-05T23:00:00.000Z" }
    })).toStrictEqual({         
        ...defaultState, recomendation_data_loading: false
    });      
  });

  it("Fetch Shipment Recommendation - Failed Reducer", async () => {
    expect(stockConstraintReportReducer(undefined,{
        type: stockConstraintReportConstants.RECOMENDATIONS_FAILED,
        payload: [],
        variables: { "region":"NA", "siteNum":"2323", "shipmentNum":"543132345", "materialNum": "109137193", "plannedLoadingStartDatetime": "2022-01-05T23:00:00.000Z" }
    })).toStrictEqual({         
        ...defaultState, recomendation_data: []
    });      
  });

})