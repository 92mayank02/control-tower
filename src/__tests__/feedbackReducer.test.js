import feedbackReducer from "../reduxLib/reducers/feedbackReducer";
import { feedbackConstants } from '../reduxLib/constants';

describe("Feedback Reducer Functions",() => {

    afterEach(() => {
        jest.clearAllMocks();
    });

    const defaultState = {feedback_status: null, feedback_submit_loading: false}
    const successPayload = {}

    it("Default Switch Return", async () => {
        expect(feedbackReducer(undefined,{type:"Action not listed", payload: successPayload})).toStrictEqual({
            ...defaultState
        });        
    });

    

    it("Feedback Loading Reducer", async () => {
        expect(feedbackReducer(undefined,{
            type: feedbackConstants.SUBMIT_FEEDBACK_LOADING,
            payload: true,
        })).toStrictEqual({         
            ...defaultState, 
            feedback_submit_loading: true
        });      
    });

    it("Feedback Success", async () => {
        expect(feedbackReducer(undefined,{
            type: feedbackConstants.SUBMIT_FEEDBACK_STATUS,
            payload: "success",
        })).toStrictEqual({         
            ...defaultState, 
            feedback_status: "success"
        });      
    });


    it("Feedback Reset", async () => {
        expect(feedbackReducer(undefined,{
            type: feedbackConstants.RESET_SUBMIT_FEEDBACK_STATUS,
            payload: "success",
        })).toStrictEqual({         
            ...defaultState, 
            feedback_status: null
        });      
    });

})