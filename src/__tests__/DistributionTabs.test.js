import
DistributionTabs, {
    inboundConditionalFilterReplacement, tableStyleConfigInbound, inboundColumns,
    outboundConditionalFilterReplacement, tableStyleConfigOutbound, tabTitleEnum
} from '../components/common/DistributionTabs';
import { getTestElement, renderWithMockStore, fireEvent, waitFor } from '../testUtils';
import { defaultInboundColumns } from "../reduxLib/constdata/distinboundColumns";
import { defaultOutboundColumns } from "../reduxLib/constdata/distoutboundColumns";
import fetch from "../reduxLib/services/serviceHelpers";

jest.mock("../reduxLib/services/serviceHelpers")
describe('<DistributionTabs>', () => {
    afterAll(() => {
        jest.restoreAllMocks();
    });

    const props = {
        pageDetails: {
            filterParams: {}
        },
        prevPageDetails: { secondaryActiveTab: tabTitleEnum.inbound }
    }

    it('DistributionTabs (Filter Condition Function)', async () => {

        const newfiltersInbound = {
            inboundOrderExecutionBucket: []
        }
        await inboundConditionalFilterReplacement(newfiltersInbound)
        expect(newfiltersInbound).toEqual(
            {
                "inboundOrderExecutionBucket": ["INBOUND_IN_TRANSIT", "INBOUND_IN_YARD", "INBOUND_UNLOADING", "INBOUND_UNLOADING_COMPLETED"],
            }
        );

        const newfilters = {
            orderExecutionBucket: []
        }
        await outboundConditionalFilterReplacement(newfilters)
        expect(newfilters).toEqual(
            {
                "orderExecutionBucket": ["SHIPMENT_CREATED", "CHECK_IN", "LOADING_STARTED", "TRANS_EXEC_READY_PICK_UP"],
            }
        );
    });

    it('DistributionTabs (Table Options Function)', async () => {
        const tableoptions = {
            rowStyle: {
                color: "#FFF"
            }
        }
        expect(tableStyleConfigOutbound.rowStyle({ orderExecutionHealth: "RED" })).toEqual(
            {
                "@media (min-width:600px)": {
                    "fontWeight": "500",
                },
                "color": "#FFF",
                "borderLeft": "5px solid #FF4081",
                "flexGrow": 1,
                "fontFamily": "'Gilroy', sans_serif",
                "fontSize": "14px",
                "fontWeight": 400,
                "lineHeight": 1.5,
            }
        );

        expect(tableStyleConfigInbound.rowStyle({ inboundOrderExecutionHealth: "RED" })).toEqual(
            {
                "@media (min-width:600px)": {
                    "fontWeight": "500",
                },
                "color": "#FFF",
                "borderLeft": "5px solid #FF4081",
                "flexGrow": 1,
                "fontFamily": "'Gilroy', sans_serif",
                "fontSize": "14px",
                "fontWeight": 400,
                "lineHeight": 1.5,
            }
        );
    });


    it('DistributionTabs (Column Configuration for "-" )', async () => {
        const data = {}
        expect(defaultInboundColumns.columnConfiguration(data)).toEqual(
            {
                originId: "-",
                origin: "-",
                liveLoadInd: "-",
                appointmentRequired: "-",
                orderOnHoldInd: "-",
                deliverApptDateTime: "-",
                loadReadyDateTime: "-",
                yardArrivalDateTime: "-",
                customer: "-",
                destinationCity: "-",
                destinationState: "-",
                durationInYardMS: "0.00 Hours",
                shippingCondition: "-",
                reasonCodeString: "-",
                expectedDeliveryDateTime: "-",
            }
        );
        expect(defaultOutboundColumns.columnConfiguration(data)).toEqual(
            {
                originId: "-",
                origin: "-",
                liveLoadInd: "-",
                appointmentRequired: "-",
                orderOnHoldInd: "-",
                loadingStartDateTime: "-",
                loadReadyDateTime: "-",
                customer: "-",
                destinationCity: "-",
                destinationState: "-",
                remainingLoadTimeMS: "0.00 Hours",
                estimatedLoadTimeMS: "0.00 Hours",
                shippingCondition: "-",
                reasonCodeString: "-"
            }
        );
    });
    it('DistributionTabs should render with label', async () => {
        await renderWithMockStore(DistributionTabs, props);
        const component = getTestElement('distributiontabs');
        expect(component).toBeInTheDocument();
    });


    it('Distinbound should render- No Data', async () => {

        fetch.mockImplementation(() => {
            return new Promise((resolve, reject) => {
                return resolve({
                    status: 200,
                    json: () => {
                        return { "onPage": 1, "pageSize": 5, "totalPages": 1, "totalRecords": 0, "orders": [] }
                    }
                })
            })
        })
        const { getByText, getByTestId } = await renderWithMockStore(DistributionTabs, props);
        const component = getTestElement('distributiontabs');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
    });


});