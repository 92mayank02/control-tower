import PerformanceFilters , { performanceConstants }from '../components/common/PerformanceFilters';
import { getTestElement, renderWithMockStore, fireEvent } from '../testUtils';
import { performanceView } from 'components/dummydata/mock';
import { getStatusData } from "helpers";
import { sortBy, isArray } from "lodash";

describe('<PerformanceMapTable>', () => {
    afterAll(() => {
        jest.restoreAllMocks();
    });

    const formatData = (siteData) => {


        let perfData = isArray(siteData) ? siteData : [];

        perfData = perfData.map(d => {
            const data = getStatusData({
                ...d,
                actualHours: d.aheadOrBehind == "BEHIND" ? -(d.hours) : d.hours
            }, "actualHours");
            return {
                ...d,
                ...data
            };
        });

        return perfData = sortBy(perfData, ['actualHours']);

    }

    const filters = [{
        name: 'Healthy',
        checked: true,
        value: "Healthy",
        legend: '(Ahead >3 hours)',
    },
    {
        name: 'Monitor',
        checked: false,
        value: "Monitor",
        legend: '(Ahead 0.1-3 hours)'
    }];

    const filters2 = [
        {
            name: 'Healthy',
            checked: true,
            value: performanceConstants.HEALTHY,
            legend: '(Ahead >3 hours)',
        },
        {
            name: 'Monitor',
            checked: true,
            value: performanceConstants.MONITOR,
            legend: '(Ahead 0.1-3 hours)',
        },
        {
            name: 'Behind',
            checked: true,
            value: performanceConstants.BEHIND,
            legend: '(Behind >0 to 6 hours)',
        },
        {
            name: 'Critical',
            checked: true,
            value: performanceConstants.CRITICAL,
            legend: '(Behind >6 hours)',
        },
        {
            name: 'No Data',
            checked: false,
            value: performanceConstants.NODATA,
            legend: '(No Data 0 hour)',
        }
    ]

    const PerformanceTableProps = {
        data: formatData(performanceView),
        perfData: formatData(performanceView),
        setData: jest.fn(),
        filters,
        updateFilters: jest.fn(),
        open: true,
        handleClick: jest.fn(),
        handleClickAway: jest.fn(),
        search: "2023",
        searchKey: (search, d) => {
            return d.siteNum === search
        }
    };

    it('should render with values', async () => {

        await renderWithMockStore(PerformanceFilters, PerformanceTableProps);
        const component = getTestElement('filtericon');
        expect(component).toBeInTheDocument();
    });

    it('should filter (check) the performance data', async () => {
        const { getByTestId } = await renderWithMockStore(PerformanceFilters, PerformanceTableProps);
        fireEvent.click(getByTestId("filtercheckbox_0"), { target: { value: "Healthy", checked: false } });
        fireEvent.click(getByTestId("filtercheckbox_0"), { target: { value: "Healthy", checked: true } });
    });

    it('With Search ', async () => {
        const { getByTestId } = await renderWithMockStore(PerformanceFilters, PerformanceTableProps);
        fireEvent.click(getByTestId("filtercheckbox_0"), { target: { value: "Healthy", checked: false } });
        fireEvent.click(getByTestId("filtercheckbox_0"), { target: { value: "Healthy", checked: true } });
    });

    it('Without Search ', async () => {
        const { getByTestId } = await renderWithMockStore(PerformanceFilters, { ...PerformanceTableProps, search: null });
        fireEvent.click(getByTestId("filtercheckbox_0"), { target: { value: "Healthy", checked: false } });
        fireEvent.click(getByTestId("filtercheckbox_0"), { target: { value: "Healthy", checked: false } });
    });

    it('Without Search healthy False ', async () => {
        const { getByTestId } = await renderWithMockStore(PerformanceFilters, { ...PerformanceTableProps, filters: filters2, search: null });
        fireEvent.click(getByTestId("filtercheckbox_0"), { target: { value: "Healthy", checked: true } });
        fireEvent.click(getByTestId("filtercheckbox_0"), { target: { value: "Healthy", checked: false } });
    });

    it('With Search healthy False ', async () => {
        const { getByTestId } = await renderWithMockStore(PerformanceFilters, { ...PerformanceTableProps, filters: filters2, search: "2023" });
        fireEvent.click(getByTestId("filtercheckbox_0"), { target: { value: "Healthy", checked: true } });
        fireEvent.click(getByTestId("filtercheckbox_0"), { target: { value: "Healthy", checked: false } });
    });

});