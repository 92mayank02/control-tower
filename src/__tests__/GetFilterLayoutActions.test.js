import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { addNewFilterLayout, setCurrentView, getFilterLayouts, setFilterLayouts, setAsFavorite, setFirstLoad } from "../reduxLib/services/getFilterLayoutsService"
const middlewares = [ thunk ]
const mockStore = configureMockStore(middlewares)
import fetch from "../reduxLib/services/serviceHelpers";

jest.mock("../reduxLib/services/serviceHelpers")

describe('async actions', () => {
  let store;

  beforeEach(() => {
    store = mockStore({});
  });

  it('Get Filter Views Success', () => {

    fetch.mockImplementation(()=>{
        return new Promise((resolve,reject) => {
            return resolve({
                status:200,
                json: () => {
                    return {favoriteSites:[],layouts:{},favouriteCustomerSalesOffice:[]}
                }
            })
          })
    })

    return store.dispatch(getFilterLayouts())
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })

  it('Get Filter Views Success - CUSO', () => {

    fetch.mockImplementation(()=>{
        return new Promise((resolve,reject) => {
            return resolve({
                status:200,
                json: () => {
                    return {favoriteSites:[],layouts:{},favouriteCustomerSalesOffice:[{"selectionType": "CUST", "salesOrg": "2810", "distributionChannel": "80", "customerName": "Amazon"}]}
                }
            })
          })
    })

    return store.dispatch(getFilterLayouts())
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })

  it('SetNew/SetFav Services Data Success', () => {

    fetch.mockImplementation(()=>{
        return new Promise((resolve,reject) => {
            return resolve()
          })
    })
    const variables = {
        actionObject:{
            "NETWORK":{
                tableName:"network",
                views:[]
            }
        },
        viewObject:{}
    }
    
    store.dispatch(setFilterLayouts(variables))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })

    store.dispatch(setAsFavorite(variables))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })

  it('SetNew/SetFav Services Data Failed', () => {

    fetch.mockImplementation(()=>{
        return new Promise((resolve,reject) => {
            return reject()
          })
    })
    const variables = {
        actionObject:{
            "NETWORK":{
                tableName:"network",
                views:[]
            }
        },
        viewObject:{}
    }
    
    store.dispatch(setFilterLayouts(variables))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })

    store.dispatch(setAsFavorite(variables))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })



  it('Get Filter Layout Failed', () => {

    fetch.mockImplementation(()=>{
        return new Promise((resolve,reject) => {
            return reject({
                status:500,
            })
          })
    })

    return store.dispatch(getFilterLayouts())
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })

  it('set Current View', () => {
    store.dispatch(setCurrentView(
      { viewObject:
          {    
            "id":1,
            "name":"My FilterView 1",
            "default":false,
            "fitlers":{}
  
        }
      }
    ));
    expect(store.getActions()).toMatchSnapshot();
  });

  it('set First Load', () => {
    store.dispatch(setFirstLoad(false));
    expect(store.getActions()).toMatchSnapshot();
  });

  it('Add New Filter Layout', () => {
    store.dispatch(addNewFilterLayout(
      { viewObject:
          {    
            "id":1,
            "name":"My FilterView 1",
            "default":false,
            "fitlers":{}
  
        },
        tableName:"Transport"
      }
    ));
    expect(store.getActions()).toMatchSnapshot();
  });

});