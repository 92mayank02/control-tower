//import '@testing-library/jest-dom/extend-expect'
import {CTLink} from '../components/common/CTLink';
import { getTestElement, renderWithProvider } from '../testUtils';


describe('<CT Link>', () => {
    afterAll(() => {
      jest.restoreAllMocks();
    });
  
    const props = {}
    it('should render with label', async () => {
      await renderWithProvider(CTLink,props);
      const breadcrumbComponent = getTestElement('ctLink');
      expect(breadcrumbComponent).toBeInTheDocument();
    });

});