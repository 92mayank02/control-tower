import PieChart from '../components/D3Charts/PieChart';
import { getTestElement, renderWithProvider, userEvent, fireEvent, waitFor } from '../testUtils';

describe('<PieChart>', () => {
  afterAll(() => {
    jest.restoreAllMocks();
  });

  const PieChartProps = {
    "labels": true,
    "center": "Total",
    "footer": "Stock Transfer Orders",
    "data": [
      {
        "state": "STO",
        "stateDesc": "Stock Transfer Orders",
        "totalCount": 24,
        "redCount": 0,
        "yellowCount": 0,
        "value": 24,
        "color": "red",
        "name": "Stock Transfer Orders"
      },
      {
        "state": "STO",
        "stateDesc": "Stock Transfer Orders",
        "totalCount": 34,
        "redCount": 0,
        "yellowCount": 0,
        "name": "Stock Transfer Orders",
        "color": "blue",
        "value": 14
      }
    ],
    "colors": [
      "red",
      "green",
      "yellow"
    ],
    "xKey": "color",
    "margins": {
      "top": 20,
      "right": 10,
      "bottom": 40,
      "left": 50,
      "margin": 10
    },
    "height": 140,
    "parentWidth": 152.5833282470703,
    "legend": true,
    "setDetails": jest.fn()
  }
  it('should render with label', async () => {
    await renderWithProvider(PieChart, PieChartProps);
    const component = getTestElement('pieChart');
    expect(component).toBeInTheDocument();
    expect(component).toBeTruthy();
  });


  it('should render with label with data length > 2', async () => {
    const data = [
      {
        "state": "STO",
        "stateDesc": "Stock Transfer Orders",
        "totalCount": 24,
        "redCount": 5,
        "yellowCount": 0,
        "value": 0,
        "color": "red",
        "name": "Stock Transfer Orders"
      },
      {
        "state": "STO",
        "stateDesc": "Stock Transfer Orders",
        "totalCount": 14,
        "redCount": 7,
        "yellowCount": 0,
        "name": "Stock Transfer Orders",
        "color": "blue",
        "value": 14
      },
      {
        "state": "STO",
        "stateDesc": "Stock Transfer Orders",
        "totalCount": 34,
        "redCount": 7,
        "yellowCount": 0,
        "name": "Stock Transfer Orders",
        "color": "blue",
        "value": 14
      }
    ]
    await renderWithProvider(PieChart, { ...PieChartProps, data });
    const component = getTestElement('pieChart');
    expect(component).toBeInTheDocument();
    expect(component).toBeTruthy();
  });

  it('should render with label with no data ', async () => {
    const data = []
    await renderWithProvider(PieChart, { ...PieChartProps, data });
    const component = getTestElement('pieChart');
    expect(component).toBeInTheDocument();
    expect(component).toBeTruthy();
  });

  
  it('Tooltip Click', async () => {
    const data = [{
      "state": "STO",
      "stateDesc": "Stock Transfer Orders",
      "totalCount": 24,
      "redCount": 0,
      "yellowCount": 0,
      "value": 24,
      "color": "red",
      "name": "Stock Transfer Orders"
    },
    {
      "state": "STO",
      "stateDesc": "Stock Transfer Orders",
      "totalCount": 14,
      "redCount": 7,
      "yellowCount": 0,
      "name": "Stock Transfer Orders",
      "color": "blue",
      "value": 14
    },
    {
      "state": "STO",
      "stateDesc": "Stock Transfer Orders",
      "totalCount": 14,
      "redCount": 7,
      "yellowCount": 0,
      "name": "Stock Transfer Orders",
      "color": "yellow",
      "value": 14
    }
  ];
    const { getByTitle, getByText } = await renderWithProvider(PieChart, {...PieChartProps, data});
    const arc = getByTitle('piearc-24')
    fireEvent.mouseOver(arc);
    await waitFor(() => getByText('Stock Transfer Orders : 24'))
    expect(getByText('Stock Transfer Orders : 24')).toBeInTheDocument()
    userEvent.click(getByText('Stock Transfer Orders : 24'));
  });

  it('Tooltip Click Blue', async () => {
    const data = [{
      "state": "STO",
      "stateDesc": "Stock Transfer Orders",
      "totalCount": 24,
      "redCount": 0,
      "yellowCount": 0,
      "value": 24,
      "color": "blue",
      "name": "Stock Transfer Orders"
    }
  ];
    const { getByTitle, getByText } = await renderWithProvider(PieChart, {...PieChartProps, data});
    const arc = getByTitle('piearc-24')
    fireEvent.mouseOver(arc);
    await waitFor(() => getByText('Stock Transfer Orders : 24'))
    expect(getByText('Stock Transfer Orders : 24')).toBeInTheDocument()
    userEvent.click(getByText('Stock Transfer Orders : 24'));
  });

  it('Tooltip Click No Color', async () => {
    const data = [{
      "state": "STO",
      "stateDesc": "Stock Transfer Orders",
      "totalCount": 24,
      "redCount": 0,
      "yellowCount": 0,
      "value": 24,
      "color": "",
      "name": "Stock Transfer Orders"
    }
  ];
    const { getByTitle, getByText } = await renderWithProvider(PieChart, {...PieChartProps, data});
    const arc = getByTitle('piearc-24')
    fireEvent.mouseOver(arc);
    await waitFor(() => getByText('Stock Transfer Orders : 24'))
    expect(getByText('Stock Transfer Orders : 24')).toBeInTheDocument()
    userEvent.click(getByText('Stock Transfer Orders : 24'));
  });

  it('Tooltip Click with SetDetails Null', async () => {
    const data = [{
      "state": "STO",
      "stateDesc": "Stock Transfer Orders",
      "totalCount": 24,
      "redCount": 0,
      "yellowCount": 0,
      "value": 24,
      "color": "red",
      "name": "Stock Transfer Orders"
    }];
    const { getByTitle, getByText } = await renderWithProvider(PieChart, {...PieChartProps, data, setDetails: null});
    const arc = getByTitle('piearc-24')
    fireEvent.mouseOver(arc);
    await waitFor(() => getByText('Stock Transfer Orders : 24'))
    expect(getByText('Stock Transfer Orders : 24')).toBeInTheDocument()
    userEvent.click(getByText('Stock Transfer Orders : 24'))
  });

});