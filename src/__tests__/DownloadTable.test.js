import DownloadTable from 'components/common/Table/DownloadTable';
import { getTestElement, renderWithMockStore } from '../testUtils';
import useMediaQuery from "@material-ui/core/useMediaQuery"
jest.mock('@material-ui/core/useMediaQuery')
import { mockStoreValue } from "../components/dummydata/mock"
import { fireEvent, screen, waitFor } from '@testing-library/react';

describe('<DownloadTable Component>', () => {

    afterAll(() => {
        jest.restoreAllMocks();
    });

    const props = {
        downloadConfig: {
            type: "network",
            subtype: "network"
        },
        downloads: {
            network: {
                network: {
                    downloading: true
                }
            }
        }
    }

    it('DownloadTable should render', async () => {
        const { getByLabelText, getByTestId } = await renderWithMockStore(DownloadTable, props, {
            ...mockStoreValue, downloads: {
                status: "Success",
                data: [{ "orderNum": 1 }]
            }
        });
        const component = getTestElement('downloadtable');
        expect(component).toBeInTheDocument();
        fireEvent.click(getByLabelText("ExportTable"))
        fireEvent.click(getByLabelText("exportWith"))
        expect(getByLabelText("exportWith")).toBeInTheDocument();
    });

    it('DownloadTable should render and click to cancel snackbar', async () => {
        const { getByLabelText, getByTestId } = await renderWithMockStore(DownloadTable, props, {
            ...mockStoreValue, downloads: {
                status: "Success",
                data: [{ "orderNum": 1 }],
                network: {
                    network: {
                        downloading: true
                    }
                }
            }
        });
        const component = getTestElement('downloadtable');
        expect(component).toBeInTheDocument();
        fireEvent.click(getByLabelText("ExportTable"))
        fireEvent.click(getByLabelText("exportWithOut"))
        expect(getByLabelText("exportWithOut")).toBeInTheDocument();
    });

    it('DownloadTable should render and click to cancel snackbar - downlaodign false', async () => {
        const { getByLabelText, getByTestId } = await renderWithMockStore(DownloadTable, props, {
            ...mockStoreValue, downloads: {
                status: "Success",
                data: [{ "orderNum": 1 }],
                network: {
                    network: {
                        downloading: false
                    }
                }
            }
        });
        const component = getTestElement('downloadtable');
        expect(component).toBeInTheDocument();
        fireEvent.click(getByLabelText("ExportTable"))
        fireEvent.click(getByLabelText("exportWithOut"))
        expect(getByLabelText("exportWithOut")).toBeInTheDocument();
    });


    it('DownloadTable should render with failure', async () => {
        await renderWithMockStore(DownloadTable, props, {
            ...mockStoreValue, downloads: {
                status: "Failed",
                data: null
            }
        });
        const component = getTestElement('downloadtable');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
    });

});