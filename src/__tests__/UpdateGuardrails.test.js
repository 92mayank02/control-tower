import { getTestElement, renderWithMockStore, fireEvent } from '../testUtils';
import UpdateGuardrails,{GuardrailDataDisplayGrid} from "components/UpdateGuardrails"
import { mockStoreValue } from '../components/dummydata/mock';

jest.mock("../reduxLib/services/optionsActions",() =>({
    ...jest.requireActual("../reduxLib/services/getAllGuardRailsService"),
    saveGuardrails : () => {
        return {
            type:"GUARDRAILS_UPDATE_SUCCESS",
            payload: "true"
        }
    }
}))
describe("<Update Guardrails Component>",()=>{
    afterAll(() => {
        jest.restoreAllMocks();
      });
    
      const mockdata = [
        {
          "siteNum": "2028",
          "unit": "LOADS", // Possible values "Loads" or "Hours"
          "weekdays": [
            {
              "day": "Sunday",
              "orderBusinessUnit": [
                {
                  "businessUnit": "CONSUMER",
                  "guardrailFL": 2023,
                  "guardrailLTL": 18
                },
                {
                  "businessUnit": "PROFESSIONAL",
                  "guardrailFL": 36,
                  "guardrailLTL": 36
                },
                {
                  "businessUnit": "ALL",
                }
              ]
            },
            {
              "day": "Monday",
              "orderBusinessUnit": [
                {
                  "businessUnit": "CONSUMER",
                  "guardrailFL": 19,
                  "guardrailLTL": 19
                },
                {
                  "businessUnit": "PROFESSIONAL",
                  "guardrailFL": 33,
                  "guardrailLTL": 23
                },
                {
                  "businessUnit": "ALL",
                  "guardrailFL": 23,
                  "guardrailLTL": 23
                }
              ]
            }
          ]
        },
        {
            "siteNum": "2027",
            "unit": "LOADS", // Possible values "Loads" or "Hours"
            "weekdays": [
              {
                "day": "Sunday",
                "orderBusinessUnit": [
                  {
                    "businessUnit": "CONSUMER",
                    "guardrailFL": 2027,
                    "guardrailLTL": 12
                  },
                  {
                    "businessUnit": "PROFESSIONAL",
                    "guardrailFL": 45,
                    "guardrailLTL": 45
                  },
                  {
                    "businessUnit": "ALL",
                    "guardrailFL": 67,
                    "guardrailLTL": 67
                  }
                ]
              },
              {
                "day": "Monday",
                "orderBusinessUnit": [
                  {
                    "businessUnit": "CONSUMER",
                    "guardrailFL": 13,
                    "guardrailLTL": 13
                  },
                  {
                    "businessUnit": "PROFESSIONAL",
                    "guardrailFL": 88,
                    "guardrailLTL": 88
                  },
                  {
                    "businessUnit": "ALL",
                    "guardrailFL": 99,
                    "guardrailLTL": 99
                  }
                ]
              }
            ]
          }
      ]
  
      const mockdata2 = [
        {
          "siteNum": "2022",
          "unit": "LOADS", // Possible values "Loads" or "Hours"
          "weekdays": [
            {
              "day": "Sunday",
              "orderBusinessUnit": [
                {
                  "businessUnit": "CONSUMER",
                  "guardrailFL": 2022,
                  "guardrailLTL": 56
                },
                {
                  "businessUnit": "PROFESSIONAL",
                  "guardrailFL": 66,
                  "guardrailLTL": 66
                },
                {
                  "businessUnit": "ALL",
                  "guardrailFL": 66,
                  "guardrailLTL": 66
                }
              ]
            },
            {
              "day": "Monday",
              "orderBusinessUnit": [
                {
                  "businessUnit": "CONSUMER",
                  "guardrailFL": 88,
                  "guardrailLTL": 88
                },
                {
                  "businessUnit": "PROFESSIONAL",
                  "guardrailFL": 88,
                  "guardrailLTL": 88
                },
                {
                  "businessUnit": "ALL",
                  "guardrailFL": 99,
                  "guardrailLTL": 99
                }
              ]
            }
          ]
        }
      ]
  
      it('should render with values - guardrails response has the selected location', async () => {
          
          await renderWithMockStore(UpdateGuardrails,{},{...mockStoreValue,guardrails:{...mockStoreValue.guardrails, allSiteGuardrails: {data:mockdata, allowedToEdit:true}}});
          const component = getTestElement('updateguardrails');
          expect(component).toBeInTheDocument();
      });

      it('should render with values - guardrails response has the selected location - but not allowed to edit', async () => {
          
        await renderWithMockStore(UpdateGuardrails,{},{...mockStoreValue,guardrails:{...mockStoreValue.guardrails, allSiteGuardrails: {data:mockdata, allowedToEdit:false}}});
        const component = getTestElement('updateguardrails');
        expect(component).toBeInTheDocument();
    });


      it('should render with values - guardrails response doesn0t have the selected location', async () => {
          
        await renderWithMockStore(UpdateGuardrails,{},{...mockStoreValue,guardrails:{...mockStoreValue.guardrails, allSiteGuardrails: {data:mockdata2, allowedToEdit:true}}});
        const component = getTestElement('updateguardrails');
        expect(component).toBeInTheDocument();
    });
  
      it('Click Save Changes Button', async () => {
        const { getByText } = await renderWithMockStore(UpdateGuardrails,{},{...mockStoreValue,guardrails:{...mockStoreValue.guardrails, allSiteGuardrails: {data:mockdata, allowedToEdit:true}}});
        const component = getTestElement('updateguardrails');
        fireEvent.click(getByText("Update Changes"));
        expect(component).toBeInTheDocument();
      });
      
      it('Change Values and Save', async () => {
        const { getByText,getAllByTestId } = await renderWithMockStore(UpdateGuardrails,{},{...mockStoreValue,guardrails:{...mockStoreValue.guardrails, allSiteGuardrails: {data:mockdata, allowedToEdit:true}}});
        const component = getTestElement('updateguardrails');
        fireEvent.change(getAllByTestId("textfilter_CONSUMER_guardrailFL")[0], { target: { value:22  } });
        fireEvent.click(getByText("Update Changes"));
        expect(component).toBeInTheDocument();
      });
})