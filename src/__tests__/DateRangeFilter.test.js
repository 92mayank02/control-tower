import DateRangeFilter from 'components/common/DateRangeFilter';
import { getTestElement, renderWithMockStore, fireEvent } from '../testUtils';
import { saveFilters } from "../reduxLib/services/optionsActions";

    jest.mock("../reduxLib/services/optionsActions",() =>({
        ...jest.requireActual("../reduxLib/services/optionsActions"),
        saveFilters : () => {
            return {
                type:"ADD_FILTER_SUCCESS",
                payload: "true"
            }
        }
    }))

describe('<DateRangeFilter>', () => {
    afterAll(() => {
        jest.restoreAllMocks();
    });

    const filterprops = {
        "type": "network",
        "subtype": "network",
        "preSetFilter": true,
        "title": "Carrier Ready Date",
        "filterKey": "loadReadyDateTimeOriginTZ",
        saveFilters: jest.fn()
    };

    const filterprops2 = {
        "type": "network",
        "subtype": "network",
        "preSetFilter": true,
        "title": "Carrier Ready Date",
        "filterKey": "loadStartDateTimeOriginTZ",
        saveFilters: jest.fn()
    };
    it('DateRangeFilter should render', async () => {
        await renderWithMockStore(DateRangeFilter, filterprops);
        const component = getTestElement('daterangefilter');
        expect(component).toBeInTheDocument();
    });

    it('DateRangeFilter should render', async () => {
        const { getByTestId, getAllByText, getByText } = await renderWithMockStore(DateRangeFilter, filterprops);
        const component = getTestElement('daterangefilter');
        expect(component).toBeInTheDocument();
        fireEvent.click(getByTestId("daterangefilter"))
        fireEvent.click(getAllByText("15")[0])
        fireEvent.click(getAllByText("16")[1])
        fireEvent.click(getByText("Apply"))
    });

    it('DateRangeFilter should render', async () => {
        const { getByTestId, getAllByText, getByText } = await renderWithMockStore(DateRangeFilter, filterprops2);
        const component = getTestElement('daterangefilter');
        expect(component).toBeInTheDocument();
        fireEvent.click(getByTestId("daterangefilter"))
        fireEvent.click(getAllByText("15")[0])
        fireEvent.click(getAllByText("16")[1])
        fireEvent.click(getByText("Apply"))
    });

});