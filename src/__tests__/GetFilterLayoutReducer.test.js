import getFilterLayoutsReducer from "../reduxLib/reducers/getFilterLayoutsReducer";
import { profileConstants } from '../reduxLib/constants';
import { transportfilters, distoutboundfilters, distinboundfilters, ordersfilters, networkfilters } from "../reduxLib/constdata/filters";

describe("Get Filter Layout Reducer",() => {

    afterEach(() => {
        jest.clearAllMocks();
    });

    const defaultdata = {
      transport: transportfilters,
      distoutbound: distoutboundfilters,
      distinbound: distinboundfilters,
      orders: ordersfilters,
      network: networkfilters
  }

    const successPayload1 = {
      "layout": {
          "filterLayouts": {
              "NETWORK": {
                  "tableName": "network",
                  "views": [
                      {
                          "id": "583d9e86-b1ae-4b25-8752-c0732889d8db",
                          "name": "Mathson",
                          "default": true,
                          "filters": {
                              "sites": {
                                  "type": "sites",
                                  "name": "Origin",
                                  "data": null
                              },
                              "searchStringList": {
                                  "type": "text",
                                  "name": "Search",
                                  "stringToArray": true,
                                  "data": null
                              },
                              "destName": {
                                  "type": "text",
                                  "name": "Customer",
                                  "data": null
                              },
                              "destState": {
                                  "type": "text",
                                  "name": "Destination State",
                                  "data": null
                              },
                              "destCity": {
                                  "type": "text",
                                  "name": "Destination City",
                                  "data": null
                              },
                              "orderStatusBucket": {
                                  "type": "checkbox",
                                  "name": "Order Status",
                                  "data": [
                                      {
                                          "name": "Order Block Free",
                                          "value": "ORDER_BLOCK_FREE",
                                          "checked": false
                                      },
                                      {
                                          "name": "Order Blocked",
                                          "value": "ORDER_BLOCKED",
                                          "checked": true
                                      }
                                  ]
                              },
                              "orderTypes": {
                                  "type": "checkbox",
                                  "name": "Order Type",
                                  "data": [
                                      {
                                          "name": "Cust. Order",
                                          "value": "CUST",
                                          "checked": false
                                      },
                                      {
                                          "name": "STO",
                                          "value": "STO",
                                          "checked": false
                                      }
                                  ]
                              },
                              "tariffServiceCodeList": {
                                  "type": "text",
                                  "stringToArray": true,
                                  "name": "Carrier Service Code",
                                  "data": null
                              },
                              "orderExecutionBucket": {
                                  "type": "checkbox",
                                  "name": "Shipment Status",
                                  "data": [
                                      {
                                          "name": "Transportation to be planned",
                                          "value": "TRANS_PLAN_UNASSIGNED",
                                          "checked": false
                                      },
                                      {
                                          "name": "Transportation planning",
                                          "value": "TRANS_PLAN_OPEN",
                                          "checked": false
                                      },
                                      {
                                          "name": "Transportation processing",
                                          "value": "TRANS_PLAN_PROCESSING",
                                          "checked": false
                                      },
                                      {
                                          "name": "Transportation Carrier committed",
                                          "value": "TRANS_PLAN_TENDER_ACCEPTED",
                                          "checked": false
                                      },
                                      {
                                          "name": "Shipment Created",
                                          "value": "SHIPMENT_CREATED",
                                          "checked": false
                                      },
                                      {
                                          "name": "Checked in by DC",
                                          "value": "CHECK_IN",
                                          "checked": false
                                      },
                                      {
                                          "name": "Loading Started",
                                          "value": "LOADING_STARTED",
                                          "checked": false
                                      },
                                      {
                                          "name": "Ready for Pickup",
                                          "value": "TRANS_EXEC_READY_PICK_UP",
                                          "checked": false
                                      },
                                      {
                                          "name": "In Transit",
                                          "value": "TRANS_EXEC_IN_TRANSIT",
                                          "checked": false
                                      },
                                      {
                                          "name": "Delivery confirmed",
                                          "value": "TRANS_EXEC_DELIVERY_CONFIRMED",
                                          "checked": false
                                      }
                                  ]
                              },
                              "loadReadyDateTimeOriginTZ": {
                                  "type": "date",
                                  "dummytime": true,
                                  "timeformat": "T",
                                  "startTime": "[ 00:00:00.000]",
                                  "endTime": "[ 23:59:59.999]",
                                  "name": "Carrier Ready Date",
                                  "shortName": "Carr. Ready Date",
                                  "data": null
                              },
                              "searchString": {
                                  "type": "text",
                                  "name": "Search String",
                                  "data": null
                              },
                              "tariffServiceCode": {
                                  "type": "text",
                                  "name": "Carrier Service Code",
                                  "data": null
                              },
                              "materialNumberList": {
                                "type": "text",
                                "name": "Material ID",
                                "stringToArray": true,
                                "data": null
                            }
                          },
                          "columns": [
                              {
                                  "title": "Shipment #",
                                  "field": "shipmentNum",
                                  "cellStyle": {
                                      "minWidth": 160
                                  }
                              },
                              {
                                  "title": "Delivery #",
                                  "field": "deliveryNum",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 1,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 15)",
                                      "id": 1
                                  }
                              },
                              {
                                  "title": "Shipment Status",
                                  "field": "orderExecutionBucketDesc",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 2,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 15)",
                                      "id": 2
                                  }
                              },
                              {
                                  "title": "Order #",
                                  "field": "orderNum",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 3,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 15)",
                                      "id": 3
                                  }
                              },
                              {
                                  "title": "Order Status",
                                  "field": "orderStatus",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 4,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 15)",
                                      "id": 4
                                  }
                              },
                              {
                                  "title": "Order Type",
                                  "field": "orderType",
                                  "tableData": {
                                      "columnOrder": 5,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 15)",
                                      "id": 5
                                  }
                              },
                              {
                                  "title": "Customer",
                                  "field": "customer",
                                  "tableData": {
                                      "columnOrder": 6,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 15)",
                                      "id": 6
                                  }
                              },
                              {
                                  "title": "Origin ID",
                                  "field": "originId",
                                  "tableData": {
                                      "columnOrder": 7,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 15)",
                                      "id": 7
                                  }
                              },
                              {
                                  "title": "Origin",
                                  "field": "origin",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 8,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 15)",
                                      "id": 8
                                  }
                              },
                              {
                                  "title": "Dest City",
                                  "field": "destinationCity",
                                  "tableData": {
                                      "columnOrder": 9,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 15)",
                                      "id": 9
                                  }
                              },
                              {
                                  "title": "Dest State",
                                  "field": "destinationState",
                                  "tableData": {
                                      "columnOrder": 10,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 15)",
                                      "id": 10
                                  }
                              },
                              {
                                  "title": "Carrier Ready Date & Time",
                                  "field": "loadReadyDateTime",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 11,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 15)",
                                      "id": 11
                                  }
                              },
                              {
                                  "title": "Carrier",
                                  "field": "tariffServiceCode",
                                  "tableData": {
                                      "columnOrder": 12,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 16)",
                                      "id": 12
                                  }
                              },
                              {
                                  "title": "Ship Condition",
                                  "field": "shippingCondition",
                                  "tableData": {
                                      "columnOrder": 13,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 15)",
                                      "id": 12
                                  }
                              },
                              {
                                  "title": "Reason for Alert",
                                  "field": "reasonCodeString",
                                  "cellStyle": {
                                      "minWidth": 320
                                  },
                                  "tableData": {
                                      "columnOrder": 14,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 15)",
                                      "id": 13
                                  }
                              },
                              {
                                  "title": "Fourkites ETA",
                                  "field": "expectedDeliveryDateTime",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "disableClick": true
                              }
                          ]
                      },
                      {
                          "id": "222cd2b0-7f11-415d-b985-c26e57180e0a",
                          "name": "Blocked Ready for Pickup",
                          "default": false,
                          "filters": {
                              "sites": {
                                  "type": "sites",
                                  "name": "Origin",
                                  "data": null
                              },
                              "searchStringList": {
                                  "type": "text",
                                  "name": "Search",
                                  "stringToArray": true,
                                  "data": null
                              },
                              "destName": {
                                  "type": "text",
                                  "name": "Customer",
                                  "data": null
                              },
                              "destState": {
                                  "type": "text",
                                  "name": "Destination State",
                                  "data": null
                              },
                              "destCity": {
                                  "type": "text",
                                  "name": "Destination City",
                                  "data": null
                              },
                              "orderStatusBucket": {
                                  "type": "checkbox",
                                  "name": "Order Status",
                                  "data": [
                                      {
                                          "name": "Order Block Free",
                                          "value": "ORDER_BLOCK_FREE",
                                          "checked": false
                                      },
                                      {
                                          "name": "Order Blocked",
                                          "value": "ORDER_BLOCKED",
                                          "checked": true
                                      }
                                  ]
                              },
                              "orderTypes": {
                                  "type": "checkbox",
                                  "name": "Order Type",
                                  "data": [
                                      {
                                          "name": "Cust. Order",
                                          "value": "CUST",
                                          "checked": false
                                      },
                                      {
                                          "name": "STO",
                                          "value": "STO",
                                          "checked": false
                                      }
                                  ]
                              },
                              "tariffServiceCodeList": {
                                  "type": "text",
                                  "stringToArray": true,
                                  "name": "Carrier Service Code",
                                  "data": null
                              },
                              "orderExecutionBucket": {
                                  "type": "checkbox",
                                  "name": "Shipment Status",
                                  "data": [
                                      {
                                          "name": "Transportation to be planned",
                                          "value": "TRANS_PLAN_UNASSIGNED",
                                          "checked": false
                                      },
                                      {
                                          "name": "Transportation planning",
                                          "value": "TRANS_PLAN_OPEN",
                                          "checked": false
                                      },
                                      {
                                          "name": "Transportation processing",
                                          "value": "TRANS_PLAN_PROCESSING",
                                          "checked": false
                                      },
                                      {
                                          "name": "Transportation Carrier committed",
                                          "value": "TRANS_PLAN_TENDER_ACCEPTED",
                                          "checked": false
                                      },
                                      {
                                          "name": "Shipment Created",
                                          "value": "SHIPMENT_CREATED",
                                          "checked": false
                                      },
                                      {
                                          "name": "Checked in by DC",
                                          "value": "CHECK_IN",
                                          "checked": false
                                      },
                                      {
                                          "name": "Loading Started",
                                          "value": "LOADING_STARTED",
                                          "checked": false
                                      },
                                      {
                                          "name": "Ready for Pickup",
                                          "value": "TRANS_EXEC_READY_PICK_UP",
                                          "checked": true
                                      },
                                      {
                                          "name": "In Transit",
                                          "value": "TRANS_EXEC_IN_TRANSIT",
                                          "checked": false
                                      },
                                      {
                                          "name": "Delivery confirmed",
                                          "value": "TRANS_EXEC_DELIVERY_CONFIRMED",
                                          "checked": false
                                      }
                                  ]
                              },
                              "loadReadyDateTimeOriginTZ": {
                                  "type": "date",
                                  "dummytime": true,
                                  "timeformat": "T",
                                  "startTime": "[ 00:00:00.000]",
                                  "endTime": "[ 23:59:59.999]",
                                  "name": "Carrier Ready Date",
                                  "shortName": "Carr. Ready Date",
                                  "data": null
                              },
                              "searchString": {
                                  "type": "text",
                                  "name": "Search String",
                                  "data": null
                              },
                              "tariffServiceCode": {
                                  "type": "text",
                                  "name": "Carrier Service Code",
                                  "data": null
                              },
                              "materialNumberList": {
                                "type": "text",
                                "name": "Material ID",
                                "stringToArray": true,
                                "data": null
                            }
                          },
                          "columns": [
                              {
                                  "title": "Shipment #",
                                  "field": "shipmentNum",
                                  "cellStyle": {
                                      "minWidth": 160
                                  }
                              },
                              {
                                  "title": "Delivery #",
                                  "field": "deliveryNum",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 1,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 14)",
                                      "id": 1
                                  }
                              },
                              {
                                  "title": "Shipment Status",
                                  "field": "orderExecutionBucketDesc",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 2,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 14)",
                                      "id": 2
                                  }
                              },
                              {
                                  "title": "Order #",
                                  "field": "orderNum",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 3,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 14)",
                                      "id": 3
                                  }
                              },
                              {
                                  "title": "Order Status",
                                  "field": "orderStatus",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 4,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 16)",
                                      "id": 4
                                  }
                              },
                              {
                                  "title": "Order Type",
                                  "field": "orderType",
                                  "tableData": {
                                      "columnOrder": 5,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 16)",
                                      "id": 5
                                  }
                              },
                              {
                                  "title": "Customer",
                                  "field": "customer",
                                  "tableData": {
                                      "columnOrder": 6,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 14)",
                                      "id": 4
                                  }
                              },
                              {
                                  "title": "Origin ID",
                                  "field": "originId",
                                  "tableData": {
                                      "columnOrder": 7,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 14)",
                                      "id": 5
                                  }
                              },
                              {
                                  "title": "Origin",
                                  "field": "origin",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 8,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 14)",
                                      "id": 6
                                  }
                              },
                              {
                                  "title": "Dest City",
                                  "field": "destinationCity",
                                  "tableData": {
                                      "columnOrder": 9,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 14)",
                                      "id": 7
                                  }
                              },
                              {
                                  "title": "Dest State",
                                  "field": "destinationState",
                                  "tableData": {
                                      "columnOrder": 10,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 14)",
                                      "id": 8
                                  }
                              },
                              {
                                  "title": "Carrier Ready Date & Time",
                                  "field": "loadReadyDateTime",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 11,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 14)",
                                      "id": 9
                                  }
                              },
                              {
                                  "title": "Carrier",
                                  "field": "tariffServiceCode",
                                  "tableData": {
                                      "columnOrder": 12,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 14)",
                                      "id": 10
                                  }
                              },
                              {
                                  "title": "Ship Condition",
                                  "field": "shippingCondition",
                                  "tableData": {
                                      "columnOrder": 13,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 14)",
                                      "id": 11
                                  }
                              },
                              {
                                  "title": "Reason for Alert",
                                  "field": "reasonCodeString",
                                  "cellStyle": {
                                      "minWidth": 320
                                  },
                                  "tableData": {
                                      "columnOrder": 14,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 14)",
                                      "id": 12
                                  }
                              },
                              {
                                  "title": "Fourkites ETA",
                                  "field": "expectedDeliveryDateTime",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "disableClick": true
                              }
                          ]
                      }
                  ]
              },
              "ORDERS": {
                  "tableName": "orders",
                  "views": [
                      {
                          "id": "c6de3780-13f8-4ebb-9ffb-038c8f2711a0",
                          "name": "RiverPort STO View",
                          "default": true,
                          "filters": {
                              "searchStringList": {
                                  "type": "text",
                                  "name": "Search",
                                  "stringToArray": true,
                                  "data": null
                              },
                              "destName": {
                                  "type": "text",
                                  "name": "Customer",
                                  "data": null
                              },
                              "sites": {
                                  "type": "sites",
                                  "name": "Origin",
                                  "data": null
                              },
                              "shippingConditionList": {
                                  "type": "text",
                                  "stringToArray": true,
                                  "name": "Shipping Condition",
                                  "data": null
                              },
                              "requestedDeliveryDate": {
                                  "type": "date",
                                  "hidetime": true,
                                  "name": "Requested Delivery Date",
                                  "shortName": "Req. Delivery Date",
                                  "data": null
                              },
                              "deliveryBlockCodeList": {
                                  "type": "textcheck",
                                  "stringToArray": true,
                                  "name": "Delivery/Credit Block",
                                  "placeholder": "Enter delivery block code",
                                  "shortName": "Delivery Block Code",
                                  "addon": "creditOnHold",
                                  "data": null
                              },
                              "creditOnHold": {
                                  "hidden": true,
                                  "type": "text",
                                  "name": "Show Only Credit Hold Orders",
                                  "data": null
                              },
                              "confirmedEquivCubes": {
                                  "type": "checkbox",
                                  "name": "Confirmed Cube",
                                  "shortName": "Confirmed Cube",
                                  "data": [
                                      {
                                          "name": "< 1000",
                                          "value": {
                                              "lte": 999.9999999
                                          },
                                          "checked": false
                                      },
                                      {
                                          "name": "1000 - 2000",
                                          "value": {
                                              "gte": 1000,
                                              "lte": 1999.9999999
                                          },
                                          "checked": false
                                      },
                                      {
                                          "name": "2000 - 3000",
                                          "value": {
                                              "gte": 2000,
                                              "lte": 2999.9999999
                                          },
                                          "checked": false
                                      },
                                      {
                                          "name": "> 3000",
                                          "value": {
                                              "gte": 3000
                                          },
                                          "checked": false
                                      }
                                  ]
                              },
                              "destState": {
                                  "type": "text",
                                  "name": "Destination State",
                                  "data": null
                              },
                              "destCity": {
                                  "type": "text",
                                  "name": "Destination City",
                                  "data": null
                              },
                              "matAvailDate": {
                                  "type": "date",
                                  "name": "Material Availability Date",
                                  "shortName": "Mat.Avail Date",
                                  "data": null
                              },
                              "orderStatusBucket": {
                                  "type": "ordercheckbox",
                                  "name": "Order Status",
                                  "data": [
                                      {
                                          "name": "100% Confirmed Cube",
                                          "value": "SO_COMPLETELY_CONFIRMED_CUBE",
                                          "checked": false,
                                          "parent": "Order Block Free"
                                      },
                                      {
                                          "name": "Less than 100% Confirmed Cube",
                                          "value": "SO_NOT_COMPLETELY_CONFIRMED_CUBE",
                                          "checked": false,
                                          "parent": "Order Block Free"
                                      },
                                      {
                                          "name": "Back Orders Block Free",
                                          "value": "SO_BACK_ORDER_BLOCK_FREE",
                                          "checked": false,
                                          "parent": "Order Block Free"
                                      },
                                      {
                                          "name": "Visible in TMS",
                                          "value": "SO_BLOCKED_TMS_PLANNED_VISIBLE",
                                          "checked": false,
                                          "parent": "Orders Blocked"
                                      },
                                      {
                                          "name": "Not visible in TMS",
                                          "value": "SO_BLOCKED_TMS_PLANNED_NOT_VISIBLE",
                                          "checked": false,
                                          "parent": "Orders Blocked"
                                      },
                                      {
                                          "name": "Pickup not scheduled",
                                          "value": "SO_BLOCKED_NON_TMS_PLANNED_PICKUP_NOT_SCHEDULED",
                                          "checked": false,
                                          "parent": "Orders Blocked"
                                      },
                                      {
                                          "name": "Pickup/Package Multi Block",
                                          "value": "SO_BLOCKED_NON_TMS_PLANNED_PICKUP_MULTI_BLOCK",
                                          "checked": false,
                                          "parent": "Orders Blocked"
                                      },
                                      {
                                          "name": "Immediate Action",
                                          "value": "SO_BLOCKED_NON_TMS_PLANNED_IMMEDIATE_ACTION",
                                          "checked": false,
                                          "parent": "Orders Blocked"
                                      },
                                      {
                                          "name": "Back Orders Blocked",
                                          "value": "SO_BACK_ORDER_BLOCKED",
                                          "checked": false,
                                          "parent": "Orders Blocked"
                                      }
                                  ]
                              },
                              "orderExecutionBucket": {
                                  "type": "checkbox",
                                  "name": "Shipment Status",
                                  "data": [
                                      {
                                          "name": "Transportation to be planned",
                                          "value": "TRANS_PLAN_UNASSIGNED",
                                          "checked": false
                                      },
                                      {
                                          "name": "Transportation planning",
                                          "value": "TRANS_PLAN_OPEN",
                                          "checked": false
                                      },
                                      {
                                          "name": "Transportation processing",
                                          "value": "TRANS_PLAN_PROCESSING",
                                          "checked": false
                                      },
                                      {
                                          "name": "Transportation Carrier committed",
                                          "value": "TRANS_PLAN_TENDER_ACCEPTED",
                                          "checked": false
                                      },
                                      {
                                          "name": "Shipment Created",
                                          "value": "SHIPMENT_CREATED",
                                          "checked": false
                                      },
                                      {
                                          "name": "Checked in by DC",
                                          "value": "CHECK_IN",
                                          "checked": false
                                      },
                                      {
                                          "name": "Loading Started",
                                          "value": "LOADING_STARTED",
                                          "checked": false
                                      },
                                      {
                                          "name": "Ready for Pickup",
                                          "value": "TRANS_EXEC_READY_PICK_UP",
                                          "checked": false
                                      },
                                      {
                                          "name": "In Transit",
                                          "value": "TRANS_EXEC_IN_TRANSIT",
                                          "checked": false
                                      },
                                      {
                                          "name": "Delivery confirmed",
                                          "value": "TRANS_EXEC_DELIVERY_CONFIRMED",
                                          "checked": false
                                      }
                                  ]
                              },
                              "orderTypes": {
                                  "type": "checkbox",
                                  "name": "Order Type",
                                  "data": [
                                      {
                                          "name": "All Customer Orders",
                                          "value": "CUST",
                                          "checked": false
                                      },
                                      {
                                          "name": "STOs",
                                          "value": "STO",
                                          "checked": true
                                      }
                                  ]
                              },
                              "documentTypes": {
                                  "type": "checkbox",
                                  "name": "Document type",
                                  "data": [
                                      {
                                          "name": "Standard Orders (ZSTD)",
                                          "value": "ZSTD",
                                          "checked": false
                                      },
                                      {
                                          "name": "Multi Plant Orders (ZMPF)",
                                          "value": "ZMPF",
                                          "checked": false
                                      },
                                      {
                                          "name": "VMI Orders (ZVMI)",
                                          "value": "ZVMI",
                                          "checked": false
                                      },
                                      {
                                          "name": "Merged Orders (ZMER)",
                                          "value": "ZMER",
                                          "checked": false
                                      }
                                  ]
                              },
                              "orderHealth": {
                                  "type": "checkbox",
                                  "name": "Health",
                                  "data": [
                                      {
                                          "name": "Unhealthy",
                                          "value": "RED",
                                          "checked": false
                                      },
                                      {
                                          "name": "Needs Attention",
                                          "value": "YELLOW",
                                          "checked": false
                                      },
                                      {
                                          "name": "Healthy",
                                          "value": "GREEN",
                                          "checked": false
                                      }
                                  ]
                              },
                              "materialNumberList": {
                                "type": "text",
                                "name": "Material ID",
                                "stringToArray": true,
                                "data": null
                            }
                          },
                          "columns": [
                              {
                                  "title": "Order #",
                                  "field": "orderNum",
                                  "cellStyle": {
                                      "minWidth": 160
                                  }
                              },
                              {
                                  "title": "Customer",
                                  "field": "customer",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 1,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 30)",
                                      "id": 1
                                  }
                              },
                              {
                                  "title": "Customer PO #",
                                  "field": "customerPoNum",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 2,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 30)",
                                      "id": 2
                                  }
                              },
                              {
                                  "title": "LEQ",
                                  "field": "loadEquivCube",
                                  "tableData": {
                                      "columnOrder": 3,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 30)",
                                      "id": 3
                                  }
                              },
                              {
                                  "title": "Confirmed LEQ",
                                  "field": "confLoadEquivCube",
                                  "tableData": {
                                      "columnOrder": 4,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 30)",
                                      "id": 4
                                  }
                              },
                              {
                                  "title": "Allocated LEQ",
                                  "field": "allocEquivCube",
                                  "tableData": {
                                      "columnOrder": 5,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 30)",
                                      "id": 5
                                  }
                              },
                              {
                                  "title": "Origin ID",
                                  "field": "originId",
                                  "tableData": {
                                      "columnOrder": 6,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 30)",
                                      "id": 6
                                  }
                              },
                              {
                                  "title": "Origin",
                                  "field": "origin",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 7,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 30)",
                                      "id": 7
                                  }
                              },
                              {
                                  "title": "MAD",
                                  "field": "matAvailDate",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 8,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 30)",
                                      "id": 8
                                  }
                              },
                              {
                                  "title": "Original RDD",
                                  "field": "originalRequestedDeliveryDatetime",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 9,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 30)",
                                      "id": 9
                                  }
                              },
                              {
                                  "title": "RDD",
                                  "field": "requestedDeliveryDate",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 10,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 30)",
                                      "id": 10
                                  }
                              },
                              {
                                  "title": "Ship Condition",
                                  "field": "shippingCondition",
                                  "tableData": {
                                      "columnOrder": 11,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 30)",
                                      "id": 11
                                  }
                              },
                              {
                                  "title": "Delivery Block",
                                  "field": "deliveryBlocksString",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 12,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 30)",
                                      "id": 12
                                  }
                              },
                              {
                                  "title": "Shipment #",
                                  "field": "shipmentNum",
                                  "tableData": {
                                      "columnOrder": 13,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 30)",
                                      "id": 13
                                  }
                              },
                              {
                                  "title": "Delivery #",
                                  "field": "deliveryNum",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 14,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 30)",
                                      "id": 14
                                  }
                              },
                              {
                                  "title": "Carrier Ready Date & Time",
                                  "field": "loadReadyDateTime",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 15,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 30)",
                                      "id": 15
                                  }
                              },
                              {
                                  "title": "Dest City",
                                  "field": "destinationCity",
                                  "tableData": {
                                      "columnOrder": 16,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 30)",
                                      "id": 16
                                  }
                              },
                              {
                                  "title": "Dest State",
                                  "field": "destinationState",
                                  "tableData": {
                                      "columnOrder": 17,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 30)",
                                      "id": 17
                                  }
                              },
                              {
                                  "title": "Trailer #",
                                  "field": "trailerNum",
                                  "tableData": {
                                      "columnOrder": 18,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 30)",
                                      "id": 18
                                  }
                              },
                              {
                                  "title": "Shipment Status",
                                  "field": "orderExecutionBucketDesc",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 19,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 30)",
                                      "id": 19
                                  }
                              },
                              {
                                  "title": "Order Status",
                                  "field": "orderStatus",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 20,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 30)",
                                      "id": 20
                                  }
                              },
                              {
                                  "title": "Order Sub Status",
                                  "field": "orderStatusBucketDesc",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 21,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 30)",
                                      "id": 21
                                  }
                              },
                              {
                                  "title": "Order Type",
                                  "field": "orderType",
                                  "tableData": {
                                      "columnOrder": 22,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 30)",
                                      "id": 22
                                  }
                              },
                              {
                                  "title": "Business",
                                  "field": "orderBusinessUnit",
                                  "tableData": {
                                      "columnOrder": 23,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 30)",
                                      "id": 23
                                  }
                              },
                              {
                                  "title": "Reason for Alert",
                                  "field": "reasonCodeString",
                                  "cellStyle": {
                                      "minWidth": 320
                                  },
                                  "tableData": {
                                      "columnOrder": 24,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 30)",
                                      "id": 24
                                  }
                              },
                              {
                                  "title": "Fourkites ETA",
                                  "field": "expectedDeliveryDateTime",
                                  "cellStyle": {
                                      "minWidth": 160
                                  }
                              },
                              {
                                  "title": "Sales Org",
                                  "field": "salesOrg",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 26,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 30)",
                                      "id": 26
                                  }
                              },
                              {
                                  "title": "Distribution Channel",
                                  "field": "distributionChannel",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 27,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 30)",
                                      "id": 27
                                  }
                              },
                              {
                                  "title": "Sales Office",
                                  "field": "salesOffice",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 28,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 30)",
                                      "id": 28
                                  }
                              },
                              {
                                  "title": "Sales Group",
                                  "field": "salesGroup",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 29,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 30)",
                                      "id": 29
                                  }
                              }
                          ]
                      }
                  ]
              },
              "TRANSPORT": {
                  "tableName": "transport",
                  "views": [
                      {
                          "id": "37908e4d-7d87-44da-9051-a5680bd20b93",
                          "name": "In Transit - Transport",
                          "default": true,
                          "filters": {
                              "searchStringList": {
                                  "type": "text",
                                  "name": "Search",
                                  "stringToArray": true,
                                  "data": null
                              },
                              "sites": {
                                  "type": "sites",
                                  "name": "Origin",
                                  "data": null
                              },
                              "destName": {
                                  "type": "text",
                                  "name": "Customer",
                                  "data": null
                              },
                              "orderExecutionBucket": {
                                  "type": "checkbox",
                                  "name": "Shipment Status",
                                  "data": [
                                      {
                                          "name": "Transportation to be planned",
                                          "value": "TRANS_PLAN_UNASSIGNED",
                                          "checked": false
                                      },
                                      {
                                          "name": "Transportation planning",
                                          "value": "TRANS_PLAN_OPEN",
                                          "checked": false
                                      },
                                      {
                                          "name": "Transportation processing",
                                          "value": "TRANS_PLAN_PROCESSING",
                                          "checked": false
                                      },
                                      {
                                          "name": "Transportation Carrier committed",
                                          "value": "TRANS_PLAN_TENDER_ACCEPTED",
                                          "checked": false
                                      },
                                      {
                                          "name": "Ready for Pickup",
                                          "value": "TRANS_EXEC_READY_PICK_UP",
                                          "checked": false
                                      },
                                      {
                                          "name": "In Transit",
                                          "value": "TRANS_EXEC_IN_TRANSIT",
                                          "checked": true
                                      },
                                      {
                                          "name": "Delivery confirmed",
                                          "value": "TRANS_EXEC_DELIVERY_CONFIRMED",
                                          "checked": false
                                      }
                                  ]
                              },
                              "tmsShipmentStatus": {
                                  "type": "checkbox",
                                  "name": "TM Operational Status",
                                  "data": [
                                      {
                                          "name": "Unassigned",
                                          "value": "Unassigned",
                                          "checked": false
                                      },
                                      {
                                          "name": "Open",
                                          "value": "Open",
                                          "checked": false
                                      },
                                      {
                                          "name": "Planned",
                                          "value": "Planned",
                                          "checked": false
                                      },
                                      {
                                          "name": "Tendered",
                                          "value": "Tendered",
                                          "checked": false
                                      },
                                      {
                                          "name": "Tender Rejected",
                                          "value": "Tender Rejected",
                                          "checked": false
                                      },
                                      {
                                          "name": "Tender Accepted",
                                          "value": "Tender Accepted",
                                          "checked": false
                                      },
                                      {
                                          "name": "Confirming",
                                          "value": "Confirming",
                                          "checked": false
                                      },
                                      {
                                          "name": "In Transit",
                                          "value": "In Transit",
                                          "checked": false
                                      },
                                      {
                                          "name": "Completed",
                                          "value": "Completed",
                                          "checked": false
                                      }
                                  ]
                              },
                              "appointmentType": {
                                  "type": "checkbox",
                                  "name": "Appointment Type",
                                  "data": [
                                      {
                                          "name": "Manual",
                                          "value": "MANUAL",
                                          "checked": false
                                      },
                                      {
                                          "name": "Automated",
                                          "value": "AUTOMATED",
                                          "checked": false
                                      }
                                  ]
                              },
                              "destState": {
                                  "type": "text",
                                  "name": "Destination State",
                                  "data": null
                              },
                              "destCity": {
                                  "type": "text",
                                  "name": "Destination City",
                                  "data": null
                              },
                              "appointmentRequired": {
                                  "type": "radio",
                                  "name": "Appointment Required",
                                  "data": null
                              },
                              "appointmentStatus": {
                                  "type": "checkbox",
                                  "name": "Appointment Status",
                                  "data": [
                                      {
                                          "name": "Notified",
                                          "value": "NOTIFIED",
                                          "checked": false
                                      },
                                      {
                                          "name": "Planned",
                                          "value": "PLANNED",
                                          "checked": false
                                      },
                                      {
                                          "name": "Suggested",
                                          "value": "SUGGESTED",
                                          "checked": false
                                      },
                                      {
                                          "name": "Confirmed",
                                          "value": "CONFIRMED",
                                          "checked": false
                                      },
                                      {
                                          "name": "Not yet contacted",
                                          "value": "NOT_YET_CONTACTED",
                                          "checked": false
                                      },
                                      {
                                          "name": "Appt not made by Transport",
                                          "value": "NOT_MADE_BY_TRANSPORT",
                                          "checked": false
                                      }
                                  ]
                              },
                              "liveLoad": {
                                  "type": "radio",
                                  "name": "Live Load",
                                  "data": null
                              },
                              "equipmentTypeList": {
                                  "type": "checkbox",
                                  "name": "Equipment Type",
                                  "data": [
                                      {
                                          "name": "Full Truck Load",
                                          "value": "FULL_TRUCK_LOAD",
                                          "checked": false
                                      },
                                      {
                                          "name": "Less Than Truck Load",
                                          "value": "LESS_THAN_TRUCK_LOAD",
                                          "checked": false
                                      }
                                  ]
                              },
                              "shippingConditionList": {
                                  "type": "checkbox",
                                  "name": "Ship Condition",
                                  "data": [
                                      {
                                          "name": "Truck Load (TL)",
                                          "value": "TL",
                                          "checked": false
                                      },
                                      {
                                          "name": "Intermodal (TF)",
                                          "value": "TF",
                                          "checked": false
                                      },
                                      {
                                          "name": "Open (OP) ",
                                          "value": "OP",
                                          "checked": false
                                      },
                                      {
                                          "name": "Package (PG)",
                                          "value": "PG",
                                          "checked": false
                                      },
                                      {
                                          "name": "Customer Pickup (PU)",
                                          "value": "PU",
                                          "checked": false
                                      }
                                  ]
                              },
                              "onHold": {
                                  "type": "radio",
                                  "name": "Order on Hold",
                                  "data": null
                              },
                              "deliveryApptDateTimeDestTZ": {
                                  "type": "date",
                                  "name": "Delivery Date",
                                  "dummytime": true,
                                  "timeformat": "T",
                                  "startTime": "[00:00:00.000]",
                                  "endTime": "[23:59:59.999]",
                                  "shortName": "Del. Date",
                                  "data": null
                              },
                              "loadReadyDateTimeOriginTZ": {
                                  "type": "date",
                                  "name": "Carrier Ready Date",
                                  "shortName": "Car.Ready Date",
                                  "dummytime": true,
                                  "timeformat": "T",
                                  "startTime": "[00:00:00.000]",
                                  "endTime": "[23:59:59.999]",
                                  "data": null
                              },
                              "tariffServiceCodeList": {
                                  "type": "text",
                                  "name": "Carrier Service Code",
                                  "stringToArray": true,
                                  "data": null
                              },
                              "orderExecutionHealth": {
                                  "type": "checkbox",
                                  "name": "Health",
                                  "data": [
                                      {
                                          "name": "Unhealthy",
                                          "value": "RED",
                                          "checked": false
                                      },
                                      {
                                          "name": "Needs Attention",
                                          "value": "YELLOW",
                                          "checked": false
                                      },
                                      {
                                          "name": "Healthy",
                                          "value": "GREEN",
                                          "checked": true
                                      }
                                  ]
                              }
                          },
                          "columns": [
                              {
                                  "title": "Load #",
                                  "field": "shipmentNum",
                                  "cellStyle": {
                                      "minWidth": 160
                                  }
                              },
                              {
                                  "title": "Delivery #",
                                  "field": "deliveryNum",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 1,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 1
                                  }
                              },
                              {
                                  "title": "Order #",
                                  "field": "orderNum",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 2,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 2
                                  }
                              },
                              {
                                  "title": "Carrier",
                                  "field": "tariffServiceCode",
                                  "tableData": {
                                      "columnOrder": 3,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 3
                                  }
                              },
                              {
                                  "title": "Shipment Status",
                                  "field": "orderExecutionBucketDesc",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 4,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 4
                                  }
                              },
                              {
                                  "title": "TM Status",
                                  "field": "tmsShipmentStatus",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 5,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 5
                                  }
                              },
                              {
                                  "title": "Carrier Ready Date & Time",
                                  "field": "loadReadyDateTime",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 6,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 6
                                  }
                              },
                              {
                                  "title": "Origin ID",
                                  "field": "originId",
                                  "tableData": {
                                      "columnOrder": 7,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 7
                                  }
                              },
                              {
                                  "title": "Origin",
                                  "field": "origin",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 8,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 8
                                  }
                              },
                              {
                                  "title": "Customer",
                                  "field": "customer",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 9,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 9
                                  }
                              },
                              {
                                  "title": "Live Load",
                                  "field": "liveLoadInd",
                                  "tableData": {
                                      "columnOrder": 10,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 10
                                  }
                              },
                              {
                                  "title": "Order On Hold",
                                  "field": "orderOnHoldInd",
                                  "tableData": {
                                      "columnOrder": 11,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 11
                                  }
                              },
                              {
                                  "title": "Cust Confirmed Delivery Date & Time",
                                  "field": "deliverApptDateTime",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 12,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 12
                                  }
                              },
                              {
                                  "title": "Dest City",
                                  "field": "destinationCity",
                                  "tableData": {
                                      "columnOrder": 13,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 13
                                  }
                              },
                              {
                                  "title": "Dest St",
                                  "field": "destinationState",
                                  "tableData": {
                                      "columnOrder": 14,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 14
                                  }
                              },
                              {
                                  "title": "Ship Condition",
                                  "field": "shippingCondition",
                                  "tableData": {
                                      "columnOrder": 15,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 15
                                  }
                              },
                              {
                                  "title": "Appt Type",
                                  "field": "appointmentType",
                                  "tableData": {
                                      "columnOrder": 16,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 16
                                  }
                              },
                              {
                                  "title": "Appt Req'd",
                                  "field": "appointmentRequired",
                                  "tableData": {
                                      "columnOrder": 17,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 17
                                  }
                              },
                              {
                                  "title": "Appt Status",
                                  "field": "appointmentStatusDesc",
                                  "tableData": {
                                      "columnOrder": 18,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 18
                                  }
                              },
                              {
                                  "title": "Suspended Status",
                                  "field": "shipmentSuspendedStatus",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 19,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 19
                                  }
                              },
                              {
                                  "title": "Short Lead",
                                  "field": "tmsRushIndicator",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 20,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 20
                                  }
                              },
                              {
                                  "title": "Reason for Alert",
                                  "field": "reasonCodeString",
                                  "cellStyle": {
                                      "minWidth": 320
                                  },
                                  "tableData": {
                                      "columnOrder": 21,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 21
                                  }
                              },
                              {
                                  "title": "Fourkites ETA",
                                  "field": "expectedDeliveryDateTime",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "disableClick": true
                              }
                          ]
                      },
                      {
                          "id": "16b7db57-9dff-4faa-8013-efb06e3e6486",
                          "name": "CTT Ready for Pickup",
                          "default": true,
                          "filters": {
                              "searchStringList": {
                                  "type": "text",
                                  "name": "Search",
                                  "stringToArray": true,
                                  "data": null
                              },
                              "sites": {
                                  "type": "sites",
                                  "name": "Origin",
                                  "data": null
                              },
                              "destName": {
                                  "type": "text",
                                  "name": "Customer",
                                  "data": null
                              },
                              "orderExecutionBucket": {
                                  "type": "checkbox",
                                  "name": "Shipment Status",
                                  "data": [
                                      {
                                          "name": "Transportation to be planned",
                                          "value": "TRANS_PLAN_UNASSIGNED",
                                          "checked": false
                                      },
                                      {
                                          "name": "Transportation planning",
                                          "value": "TRANS_PLAN_OPEN",
                                          "checked": false
                                      },
                                      {
                                          "name": "Transportation processing",
                                          "value": "TRANS_PLAN_PROCESSING",
                                          "checked": false
                                      },
                                      {
                                          "name": "Transportation Carrier committed",
                                          "value": "TRANS_PLAN_TENDER_ACCEPTED",
                                          "checked": false
                                      },
                                      {
                                          "name": "Ready for Pickup",
                                          "value": "TRANS_EXEC_READY_PICK_UP",
                                          "checked": true
                                      },
                                      {
                                          "name": "In Transit",
                                          "value": "TRANS_EXEC_IN_TRANSIT",
                                          "checked": false
                                      },
                                      {
                                          "name": "Delivery confirmed",
                                          "value": "TRANS_EXEC_DELIVERY_CONFIRMED",
                                          "checked": false
                                      }
                                  ]
                              },
                              "tmsShipmentStatus": {
                                  "type": "checkbox",
                                  "name": "TM Operational Status",
                                  "data": [
                                      {
                                          "name": "Unassigned",
                                          "value": "Unassigned",
                                          "checked": false
                                      },
                                      {
                                          "name": "Open",
                                          "value": "Open",
                                          "checked": false
                                      },
                                      {
                                          "name": "Planned",
                                          "value": "Planned",
                                          "checked": false
                                      },
                                      {
                                          "name": "Tendered",
                                          "value": "Tendered",
                                          "checked": false
                                      },
                                      {
                                          "name": "Tender Rejected",
                                          "value": "Tender Rejected",
                                          "checked": false
                                      },
                                      {
                                          "name": "Tender Accepted",
                                          "value": "Tender Accepted",
                                          "checked": false
                                      },
                                      {
                                          "name": "Confirming",
                                          "value": "Confirming",
                                          "checked": true
                                      },
                                      {
                                          "name": "In Transit",
                                          "value": "In Transit",
                                          "checked": false
                                      },
                                      {
                                          "name": "Completed",
                                          "value": "Completed",
                                          "checked": false
                                      }
                                  ]
                              },
                              "appointmentType": {
                                  "type": "checkbox",
                                  "name": "Appointment Type",
                                  "data": [
                                      {
                                          "name": "Manual",
                                          "value": "MANUAL",
                                          "checked": false
                                      },
                                      {
                                          "name": "Automated",
                                          "value": "AUTOMATED",
                                          "checked": false
                                      }
                                  ]
                              },
                              "destState": {
                                  "type": "text",
                                  "name": "Destination State",
                                  "data": null
                              },
                              "destCity": {
                                  "type": "text",
                                  "name": "Destination City",
                                  "data": null
                              },
                              "appointmentRequired": {
                                  "type": "radio",
                                  "name": "Appointment Required",
                                  "data": null
                              },
                              "appointmentStatus": {
                                  "type": "checkbox",
                                  "name": "Appointment Status",
                                  "data": [
                                      {
                                          "name": "Notified",
                                          "value": "NOTIFIED",
                                          "checked": false
                                      },
                                      {
                                          "name": "Planned",
                                          "value": "PLANNED",
                                          "checked": false
                                      },
                                      {
                                          "name": "Suggested",
                                          "value": "SUGGESTED",
                                          "checked": false
                                      },
                                      {
                                          "name": "Confirmed",
                                          "value": "CONFIRMED",
                                          "checked": true
                                      },
                                      {
                                          "name": "Not yet contacted",
                                          "value": "NOT_YET_CONTACTED",
                                          "checked": false
                                      },
                                      {
                                          "name": "Appt not made by Transport",
                                          "value": "NOT_MADE_BY_TRANSPORT",
                                          "checked": true
                                      }
                                  ]
                              },
                              "liveLoad": {
                                  "type": "radio",
                                  "name": "Live Load",
                                  "data": null
                              },
                              "equipmentTypeList": {
                                  "type": "checkbox",
                                  "name": "Equipment Type",
                                  "data": [
                                      {
                                          "name": "Full Truck Load",
                                          "value": "FULL_TRUCK_LOAD",
                                          "checked": true
                                      },
                                      {
                                          "name": "Less Than Truck Load",
                                          "value": "LESS_THAN_TRUCK_LOAD",
                                          "checked": false
                                      }
                                  ]
                              },
                              "shippingConditionList": {
                                  "type": "checkbox",
                                  "name": "Ship Condition",
                                  "data": [
                                      {
                                          "name": "Truck Load (TL)",
                                          "value": "TL",
                                          "checked": true
                                      },
                                      {
                                          "name": "Intermodal (TF)",
                                          "value": "TF",
                                          "checked": true
                                      },
                                      {
                                          "name": "Open (OP) ",
                                          "value": "OP",
                                          "checked": false
                                      },
                                      {
                                          "name": "Package (PG)",
                                          "value": "PG",
                                          "checked": false
                                      },
                                      {
                                          "name": "Customer Pickup (PU)",
                                          "value": "PU",
                                          "checked": false
                                      }
                                  ]
                              },
                              "onHold": {
                                  "type": "radio",
                                  "name": "Order on Hold",
                                  "data": "N"
                              },
                              "deliveryApptDateTimeDestTZ": {
                                  "type": "date",
                                  "name": "Delivery Date",
                                  "dummytime": true,
                                  "timeformat": "T",
                                  "startTime": "[00:00:00.000]",
                                  "endTime": "[23:59:59.999]",
                                  "shortName": "Del. Date",
                                  "data": null
                              },
                              "loadReadyDateTimeOriginTZ": {
                                  "type": "date",
                                  "name": "Carrier Ready Date",
                                  "shortName": "Car.Ready Date",
                                  "dummytime": true,
                                  "timeformat": "T",
                                  "startTime": "[00:00:00.000]",
                                  "endTime": "[23:59:59.999]",
                                  "data": null
                              },
                              "tariffServiceCodeList": {
                                  "type": "text",
                                  "name": "Carrier Service Code",
                                  "stringToArray": true,
                                  "data": null
                              },
                              "orderExecutionHealth": {
                                  "type": "checkbox",
                                  "name": "Health",
                                  "data": [
                                      {
                                          "name": "Unhealthy",
                                          "value": "RED",
                                          "checked": true
                                      },
                                      {
                                          "name": "Needs Attention",
                                          "value": "YELLOW",
                                          "checked": true
                                      },
                                      {
                                          "name": "Healthy",
                                          "value": "GREEN",
                                          "checked": false
                                      }
                                  ]
                              }
                          },
                          "columns": [
                              {
                                  "title": "Load #",
                                  "field": "shipmentNum",
                                  "cellStyle": {
                                      "minWidth": 160
                                  }
                              },
                              {
                                  "title": "Delivery #",
                                  "field": "deliveryNum",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 1,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 1
                                  }
                              },
                              {
                                  "title": "Order #",
                                  "field": "orderNum",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 2,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 2
                                  }
                              },
                              {
                                  "title": "Carrier",
                                  "field": "tariffServiceCode",
                                  "tableData": {
                                      "columnOrder": 3,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 3
                                  }
                              },
                              {
                                  "title": "Shipment Status",
                                  "field": "orderExecutionBucketDesc",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 4,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 4
                                  }
                              },
                              {
                                  "title": "TM Status",
                                  "field": "tmsShipmentStatus",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 5,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 5
                                  }
                              },
                              {
                                  "title": "Carrier Ready Date & Time",
                                  "field": "loadReadyDateTime",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 6,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 6
                                  }
                              },
                              {
                                  "title": "Origin ID",
                                  "field": "originId",
                                  "tableData": {
                                      "columnOrder": 7,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 7
                                  }
                              },
                              {
                                  "title": "Origin",
                                  "field": "origin",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 8,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 8
                                  }
                              },
                              {
                                  "title": "Customer",
                                  "field": "customer",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 9,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 9
                                  }
                              },
                              {
                                  "title": "Live Load",
                                  "field": "liveLoadInd",
                                  "tableData": {
                                      "columnOrder": 10,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 10
                                  }
                              },
                              {
                                  "title": "Order On Hold",
                                  "field": "orderOnHoldInd",
                                  "tableData": {
                                      "columnOrder": 11,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 11
                                  }
                              },
                              {
                                  "title": "Cust Confirmed Delivery Date & Time",
                                  "field": "deliverApptDateTime",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 12,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 12
                                  }
                              },
                              {
                                  "title": "Dest City",
                                  "field": "destinationCity",
                                  "tableData": {
                                      "columnOrder": 13,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 13
                                  }
                              },
                              {
                                  "title": "Dest St",
                                  "field": "destinationState",
                                  "tableData": {
                                      "columnOrder": 14,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 14
                                  }
                              },
                              {
                                  "title": "Ship Condition",
                                  "field": "shippingCondition",
                                  "tableData": {
                                      "columnOrder": 15,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 15
                                  }
                              },
                              {
                                  "title": "Appt Type",
                                  "field": "appointmentType",
                                  "tableData": {
                                      "columnOrder": 16,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 16
                                  }
                              },
                              {
                                  "title": "Appt Req'd",
                                  "field": "appointmentRequired",
                                  "tableData": {
                                      "columnOrder": 17,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 17
                                  }
                              },
                              {
                                  "title": "Appt Status",
                                  "field": "appointmentStatusDesc",
                                  "tableData": {
                                      "columnOrder": 18,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 18
                                  }
                              },
                              {
                                  "title": "Suspended Status",
                                  "field": "shipmentSuspendedStatus",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 19,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 19
                                  }
                              },
                              {
                                  "title": "Short Lead",
                                  "field": "tmsRushIndicator",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 20,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 20
                                  }
                              },
                              {
                                  "title": "Reason for Alert",
                                  "field": "reasonCodeString",
                                  "cellStyle": {
                                      "minWidth": 320
                                  },
                                  "tableData": {
                                      "columnOrder": 21,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 23)",
                                      "id": 21
                                  }
                              },
                              {
                                  "title": "Fourkites ETA",
                                  "field": "expectedDeliveryDateTime",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "disableClick": true
                              }
                          ]
                      }
                  ]
              },
              "DISTOUTBOUND": {
                  "tableName": "distoutbound",
                  "views": [
                      {
                          "id": "7a607eb0-c727-4728-8b50-38ed18744ffe",
                          "name": "Dist-Checkin-Healthy",
                          "default": true,
                          "filters": {
                              "searchStringList": {
                                  "type": "text",
                                  "name": "Search",
                                  "stringToArray": true,
                                  "data": null
                              },
                              "orderExecutionBucket": {
                                  "type": "checkbox",
                                  "name": "Shipment Status",
                                  "data": [
                                      {
                                          "name": "Shipment created",
                                          "value": "SHIPMENT_CREATED",
                                          "checked": false
                                      },
                                      {
                                          "name": "Checked in by DC",
                                          "value": "CHECK_IN",
                                          "checked": true
                                      },
                                      {
                                          "name": "Loading started",
                                          "value": "LOADING_STARTED",
                                          "checked": false
                                      },
                                      {
                                          "name": "Ready for Pickup",
                                          "value": "TRANS_EXEC_READY_PICK_UP",
                                          "checked": false
                                      }
                                  ]
                              },
                              "orderTypes": {
                                  "type": "checkbox",
                                  "name": "Order Type",
                                  "data": [
                                      {
                                          "name": "Cust. Order",
                                          "value": "CUST",
                                          "checked": false
                                      },
                                      {
                                          "name": "STO",
                                          "value": "STO",
                                          "checked": false
                                      }
                                  ]
                              },
                              "sites": {
                                  "type": "sites",
                                  "name": "Origin",
                                  "data": null
                              },
                              "destName": {
                                  "type": "text",
                                  "name": "Customer",
                                  "data": null
                              },
                              "destState": {
                                  "type": "text",
                                  "name": "Destination State",
                                  "data": null
                              },
                              "destCity": {
                                  "type": "text",
                                  "name": "Destination City",
                                  "data": null
                              },
                              "loadReadyDateTimeOriginTZ": {
                                  "type": "date",
                                  "dummytime": true,
                                  "timeformat": "T",
                                  "startTime": "[00:00:00.000]",
                                  "endTime": "[23:59:59.999]",
                                  "name": "Carrier Ready Date",
                                  "shortName": "Carr. Ready Date",
                                  "data": null
                              },
                              "loadStartDateTimeOriginTZ": {
                                  "type": "date",
                                  "dummytime": true,
                                  "timeformat": "T",
                                  "startTime": "[00:00:00.000]",
                                  "endTime": "[23:59:59.999]",
                                  "name": "Load Start Date",
                                  "shortName": "Load Start Date",
                                  "data": null
                              },
                              "tariffServiceCodeList": {
                                  "type": "text",
                                  "stringToArray": true,
                                  "name": "Carrier Service Code",
                                  "data": null
                              },
                              "orderExecutionHealth": {
                                  "type": "checkbox",
                                  "name": "Health",
                                  "data": [
                                      {
                                          "name": "Unhealthy",
                                          "value": "RED",
                                          "checked": false
                                      },
                                      {
                                          "name": "Needs Attention",
                                          "value": "YELLOW",
                                          "checked": false
                                      },
                                      {
                                          "name": "Healthy",
                                          "value": "GREEN",
                                          "checked": true
                                      }
                                  ]
                              },
                              "liveLoad": {
                                  "type": "radio",
                                  "name": "Live Load",
                                  "data": null
                              },
                              "materialNumberList": {
                                "type": "text",
                                "name": "Material ID",
                                "stringToArray": true,
                                "data": null
                            }
                          },
                          "columns": [
                              {
                                  "title": "Shipment #",
                                  "field": "shipmentNum",
                                  "cellStyle": {
                                      "minWidth": 160
                                  }
                              },
                              {
                                  "title": "Delivery #",
                                  "field": "deliveryNum",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 1,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 20)",
                                      "id": 1
                                  }
                              },
                              {
                                  "title": "Shipment Status",
                                  "field": "orderExecutionBucketDesc",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 2,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 20)",
                                      "id": 2
                                  }
                              },
                              {
                                  "title": "Order #",
                                  "field": "orderNum",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 3,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 20)",
                                      "id": 3
                                  }
                              },
                              {
                                  "title": "Carrier Ready Date & Time",
                                  "field": "loadReadyDateTime",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 4,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 20)",
                                      "id": 4
                                  }
                              },
                              {
                                  "title": "Load Start Date & Time",
                                  "field": "loadingStartDateTime",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 5,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 20)",
                                      "id": 5
                                  }
                              },
                              {
                                  "title": "Estd Load Hrs Rem",
                                  "field": "remainingLoadTimeMS",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 6,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 20)",
                                      "id": 6
                                  }
                              },
                              {
                                  "title": "Total Load Hrs",
                                  "field": "estimatedLoadTimeMS",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 7,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 20)",
                                      "id": 7
                                  }
                              },
                              {
                                  "title": "Customer",
                                  "field": "customer",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 8,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 20)",
                                      "id": 8
                                  }
                              },
                              {
                                  "title": "Origin ID",
                                  "field": "originId",
                                  "tableData": {
                                      "columnOrder": 9,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 20)",
                                      "id": 9
                                  }
                              },
                              {
                                  "title": "Origin",
                                  "field": "origin",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 10,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 20)",
                                      "id": 10
                                  }
                              },
                              {
                                  "title": "Order Status",
                                  "field": "orderStatus",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 11,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 20)",
                                      "id": 11
                                  }
                              },
                              {
                                  "title": "Order Type",
                                  "field": "orderType",
                                  "tableData": {
                                      "columnOrder": 12,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 20)",
                                      "id": 12
                                  }
                              },
                              {
                                  "title": "Shipping Condition",
                                  "field": "shippingCondition",
                                  "cellStyle": {
                                      "minWidth": 160
                                  },
                                  "tableData": {
                                      "columnOrder": 13,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 20)",
                                      "id": 13
                                  }
                              },
                              {
                                  "title": "Live Load",
                                  "field": "liveLoadInd",
                                  "tableData": {
                                      "columnOrder": 14,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 20)",
                                      "id": 14
                                  }
                              },
                              {
                                  "title": "Carrier",
                                  "field": "tariffServiceCode",
                                  "tableData": {
                                      "columnOrder": 15,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 20)",
                                      "id": 15
                                  }
                              },
                              {
                                  "title": "Dest City",
                                  "field": "destinationCity",
                                  "tableData": {
                                      "columnOrder": 16,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 20)",
                                      "id": 16
                                  }
                              },
                              {
                                  "title": "Dest State",
                                  "field": "destinationState",
                                  "tableData": {
                                      "columnOrder": 17,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 20)",
                                      "id": 17
                                  }
                              },
                              {
                                  "title": "Business",
                                  "field": "orderBusinessUnit",
                                  "tableData": {
                                      "columnOrder": 18,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 20)",
                                      "id": 18
                                  }
                              },
                              {
                                  "title": "Reason for Alert",
                                  "field": "reasonCodeString",
                                  "cellStyle": {
                                      "minWidth": 320
                                  },
                                  "tableData": {
                                      "columnOrder": 19,
                                      "groupSort": "asc",
                                      "width": "calc((100% - (0px)) / 20)",
                                      "id": 19
                                  }
                              }
                          ]
                      }
                  ]
              },
              "DISTINBOUND": {
                "tableName": "distinbound",
                "views": [
                    {
                        "id": "7a607eb0-c727-4728-8b50-38ed18744ffe",
                        "name": "Dist-Checkin-Healthy",
                        "default": true,
                        "filters": {
                            "searchStringList": {
                                "type": "text",
                                "name": "Search",
                                "stringToArray": true,
                                "data": null
                            },
                            "orderExecutionBucket": {
                                "type": "checkbox",
                                "name": "Shipment Status",
                                "data": [
                                    {
                                        "name": "Shipment created",
                                        "value": "SHIPMENT_CREATED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Checked in by DC",
                                        "value": "CHECK_IN",
                                        "checked": true
                                    },
                                    {
                                        "name": "Loading started",
                                        "value": "LOADING_STARTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Ready for Pickup",
                                        "value": "TRANS_EXEC_READY_PICK_UP",
                                        "checked": false
                                    }
                                ]
                            },
                            "orderTypes": {
                                "type": "checkbox",
                                "name": "Order Type",
                                "data": [
                                    {
                                        "name": "Cust. Order",
                                        "value": "CUST",
                                        "checked": false
                                    },
                                    {
                                        "name": "STO",
                                        "value": "STO",
                                        "checked": false
                                    }
                                ]
                            },
                            "sites": {
                                "type": "sites",
                                "name": "Origin",
                                "data": null
                            },
                            "destName": {
                                "type": "text",
                                "name": "Customer",
                                "data": null
                            },
                            "destState": {
                                "type": "text",
                                "name": "Destination State",
                                "data": null
                            },
                            "destCity": {
                                "type": "text",
                                "name": "Destination City",
                                "data": null
                            },
                            "loadReadyDateTimeOriginTZ": {
                                "type": "date",
                                "dummytime": true,
                                "timeformat": "T",
                                "startTime": "[00:00:00.000]",
                                "endTime": "[23:59:59.999]",
                                "name": "Carrier Ready Date",
                                "shortName": "Carr. Ready Date",
                                "data": null
                            },
                            "loadStartDateTimeOriginTZ": {
                                "type": "date",
                                "dummytime": true,
                                "timeformat": "T",
                                "startTime": "[00:00:00.000]",
                                "endTime": "[23:59:59.999]",
                                "name": "Load Start Date",
                                "shortName": "Load Start Date",
                                "data": null
                            },
                            "tariffServiceCodeList": {
                                "type": "text",
                                "stringToArray": true,
                                "name": "Carrier Service Code",
                                "data": null
                            },
                            "orderExecutionHealth": {
                                "type": "checkbox",
                                "name": "Health",
                                "data": [
                                    {
                                        "name": "Unhealthy",
                                        "value": "RED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Needs Attention",
                                        "value": "YELLOW",
                                        "checked": false
                                    },
                                    {
                                        "name": "Healthy",
                                        "value": "GREEN",
                                        "checked": true
                                    }
                                ]
                            },
                            "liveLoad": {
                                "type": "radio",
                                "name": "Live Load",
                                "data": null
                            },
                            "materialNumberList": {
                              "type": "text",
                              "name": "Material ID",
                              "stringToArray": true,
                              "data": null
                          }
                        },
                        "columns": [
                            {
                                "title": "Shipment #",
                                "field": "shipmentNum",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Delivery #",
                                "field": "deliveryNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 1,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 20)",
                                    "id": 1
                                }
                            },
                            {
                                "title": "Shipment Status",
                                "field": "orderExecutionBucketDesc",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 2,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 20)",
                                    "id": 2
                                }
                            },
                            {
                                "title": "Order #",
                                "field": "orderNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 3,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 20)",
                                    "id": 3
                                }
                            },
                            {
                                "title": "Carrier Ready Date & Time",
                                "field": "loadReadyDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 4,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 20)",
                                    "id": 4
                                }
                            },
                            {
                                "title": "Load Start Date & Time",
                                "field": "loadingStartDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 5,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 20)",
                                    "id": 5
                                }
                            },
                            {
                                "title": "Estd Load Hrs Rem",
                                "field": "remainingLoadTimeMS",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 6,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 20)",
                                    "id": 6
                                }
                            },
                            {
                                "title": "Total Load Hrs",
                                "field": "estimatedLoadTimeMS",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 7,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 20)",
                                    "id": 7
                                }
                            },
                            {
                                "title": "Customer",
                                "field": "customer",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 8,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 20)",
                                    "id": 8
                                }
                            },
                            {
                                "title": "Origin ID",
                                "field": "originId",
                                "tableData": {
                                    "columnOrder": 9,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 20)",
                                    "id": 9
                                }
                            },
                            {
                                "title": "Origin",
                                "field": "origin",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 10,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 20)",
                                    "id": 10
                                }
                            },
                            {
                                "title": "Order Status",
                                "field": "orderStatus",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 11,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 20)",
                                    "id": 11
                                }
                            },
                            {
                                "title": "Order Type",
                                "field": "orderType",
                                "tableData": {
                                    "columnOrder": 12,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 20)",
                                    "id": 12
                                }
                            },
                            {
                                "title": "Shipping Condition",
                                "field": "shippingCondition",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 13,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 20)",
                                    "id": 13
                                }
                            },
                            {
                                "title": "Live Load",
                                "field": "liveLoadInd",
                                "tableData": {
                                    "columnOrder": 14,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 20)",
                                    "id": 14
                                }
                            },
                            {
                                "title": "Carrier",
                                "field": "tariffServiceCode",
                                "tableData": {
                                    "columnOrder": 15,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 20)",
                                    "id": 15
                                }
                            },
                            {
                                "title": "Dest City",
                                "field": "destinationCity",
                                "tableData": {
                                    "columnOrder": 16,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 20)",
                                    "id": 16
                                }
                            },
                            {
                                "title": "Dest State",
                                "field": "destinationState",
                                "tableData": {
                                    "columnOrder": 17,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 20)",
                                    "id": 17
                                }
                            },
                            {
                                "title": "Business",
                                "field": "orderBusinessUnit",
                                "tableData": {
                                    "columnOrder": 18,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 20)",
                                    "id": 18
                                }
                            },
                            {
                                "title": "Reason for Alert",
                                "field": "reasonCodeString",
                                "cellStyle": {
                                    "minWidth": 320
                                },
                                "tableData": {
                                    "columnOrder": 19,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 20)",
                                    "id": 19
                                }
                            }
                        ]
                    }
                ]
            }
          }
      },
      "favouriteBusinessUnits": [
          "PROFESSIONAL"
      ],
      "favouriteCustomerSalesOffice": [
          {
              "selectionType": "SALES_OFFICE",
              "salesOrg": "2811",
              "distributionChannel": "80",
              "salesOffice": "CS05"
          },
          {
              "selectionType": "SALES_OFFICE",
              "salesOrg": "2811",
              "distributionChannel": "80",
              "salesOffice": "EX01"
          },
          {
              "selectionType": "SALES_OFFICE",
              "salesOrg": "2821",
              "distributionChannel": "80",
              "salesOffice": "KP02",
              "salesGroup": [
                  "Z07",
                  "Z02",
                  "Z08"
              ]
          }
      ]
  }
        
    const defaultState = {loading: false, filterLayouts: {}, initialLayouts:{}, currentView:{}}

    it("Default Switch Return", async () => {
        expect(getFilterLayoutsReducer(undefined,{type:"Action not listed", payload: successPayload1})).toStrictEqual(defaultState);        
    });
    it("Get FilterLayout Success ", async () => {

        expect(getFilterLayoutsReducer(defaultState,{
            type: profileConstants.PROFILE_FETCH_SUCCESS,
            payload: successPayload1,
        })).toStrictEqual(
          {
            ...defaultState,
            filterLayouts: successPayload1.layout.filterLayouts, 
            initialLayouts: successPayload1.layout?.filterLayouts,
            firstLoad:true,
            currentView: {
              "network":{...successPayload1.layout?.filterLayouts?.["NETWORK"]?.views?.filter(view => view.default === true)[0] || {}},
              "transport":{...successPayload1.layout?.filterLayouts?.["TRANSPORT"]?.views?.filter(view => view.default === true)[0] || {}},
              "orders":{...successPayload1.layout?.filterLayouts?.["ORDERS"]?.views?.filter(view => view.default === true)[0] || {}},
              "distoutbound":{...successPayload1.layout?.filterLayouts?.["DISTOUTBOUND"]?.views?.filter(view => view.default === true)[0] || {}},
              "distinbound":{...successPayload1.layout?.filterLayouts?.["DISTINBOUND"]?.views?.filter(view => view.default === true)[0] || {}}
            },
            defaultViewsFilters:{
                "network":{...successPayload1.layout?.filterLayouts?.["NETWORK"]?.views?.filter(view => view.default === true)[0]?.filters || defaultdata['network']},
                "transport":{...successPayload1.layout?.filterLayouts?.["TRANSPORT"]?.views?.filter(view => view.default === true)[0]?.filters || defaultdata['transport']},
                "orders":{...successPayload1.layout?.filterLayouts?.["ORDERS"]?.views?.filter(view => view.default === true)[0]?.filters || defaultdata['orders']},
                "distoutbound":{...successPayload1.layout?.filterLayouts?.["DISTOUTBOUND"]?.views?.filter(view => view.default === true)[0]?.filters || defaultdata['distoutbound']},
                "distinbound":{...successPayload1.layout?.filterLayouts?.["DISTINBOUND"]?.views?.filter(view => view.default === true)[0]?.filters || defaultdata['distinbound']}
            },
        });
        
        expect(getFilterLayoutsReducer(undefined,{
            type: profileConstants.PROFILE_FETCH_SUCCESS,
            payload: {},
        })).toStrictEqual({ 
            ...defaultState,
            filterLayouts: {}, 
            initialLayouts: {},
            currentView:{
              "network":{},
              "transport":{},
              "orders":{},
              "distoutbound":{},
              "distinbound":{}
          },
          defaultViewsFilters:{
            "network": defaultdata['network'],
            "transport":defaultdata['transport'],
            "orders":defaultdata['orders'],
            "distoutbound":defaultdata['distoutbound'],
            "distinbound":defaultdata['distinbound']
          },
            firstLoad:true
        });
        expect(getFilterLayoutsReducer(undefined,{
            type: profileConstants.PROFILE_FETCH_SUCCESS,
            payload: null,
        })).toStrictEqual({ 
            ...defaultState,
            filterLayouts: {}, 
            initialLayouts: {},
            currentView:{
              "network":{},
              "transport":{},
              "orders":{},
              "distoutbound":{},
              "distinbound":{}
          },
          defaultViewsFilters:{
            "network": defaultdata['network'],
            "transport":defaultdata['transport'],
            "orders":defaultdata['orders'],
            "distoutbound":defaultdata['distoutbound'],
            "distinbound":defaultdata['distinbound']
          },
            firstLoad:true
        });                      
    });

    it("Fetch Loading Reducer", async () => {
        expect(getFilterLayoutsReducer(undefined,{
            type: profileConstants.LOADING,
            payload: successPayload1,
        })).toStrictEqual({         
            ...defaultState, loading: successPayload1
        });      
    });
    it("Fetch Failed Reducer", async () => {
        expect(getFilterLayoutsReducer(undefined,{
            type: profileConstants.PROFILE_FETCH_FAILED,
            payload: false,
        })).toStrictEqual({         
            ...defaultState, filterLayouts: {}, currentView:{} 
        });  
    });

    it("Profile Save Success Reducer", async () => {
        expect(getFilterLayoutsReducer(undefined,{
            type: profileConstants.PROFILE_SAVE_SUCCESS,
            payload:{   
                data: {
                    filterLayouts:successPayload1,           
                },
                currentView:{
                    id:3,
                    name:"My Filter View3",
                    filter:{}
                },
                tableName:"network"
            },
        })).toStrictEqual({ 
            ...defaultState,            
            filterLayouts: successPayload1, 
            initialLayouts: successPayload1,
            currentView: {...defaultState.currentView, "network":{
                id:3,
                name:"My Filter View3",
                filter:{}
            }}
        });            
    });

    it("Profile mark fav Save Success Reducer", async () => {
        expect(getFilterLayoutsReducer(defaultState,{
            type: profileConstants.PROFILE_MARKFAV_SUCCESS,
            payload:{   
                data: {
                    filterLayouts:{},           
                }
            },
        })).toStrictEqual({ 
            ...defaultState,            
            filterLayouts: {}, 
            initialLayouts: {},
            defaultViewsFilters:{
              "network": defaultdata['network'],
              "transport":defaultdata['transport'],
              "orders":defaultdata['orders'],
              "distoutbound":defaultdata['distoutbound'],
              "distinbound":defaultdata['distinbound']
            },
        });            
    });

    it("Profile mark fav Save Success Reducer", async () => {
      expect(getFilterLayoutsReducer(defaultState,{
          type: profileConstants.PROFILE_MARKFAV_SUCCESS,
          payload:{   
              data: {
                filterLayouts:successPayload1.layout.filterLayouts,           
              }
          },
      })).toStrictEqual({ 
          ...defaultState,            
          filterLayouts: successPayload1.layout.filterLayouts, 
          initialLayouts: successPayload1.layout.filterLayouts,
          defaultViewsFilters:{
            "network":successPayload1.layout.filterLayouts?.["NETWORK"]?.views?.filter(view => view.default === true)[0]?.filters,
            "transport":successPayload1.layout.filterLayouts?.["TRANSPORT"]?.views?.filter(view => view.default === true)[0]?.filters,
            "orders":successPayload1.layout.filterLayouts?.["ORDERS"]?.views?.filter(view => view.default === true)[0]?.filters,
            "distoutbound":successPayload1.layout.filterLayouts?.["DISTOUTBOUND"]?.views?.filter(view => view.default === true)[0]?.filters,
            "distinbound":successPayload1.layout.filterLayouts?.["DISTINBOUND"]?.views?.filter(view => view.default === true)[0]?.filters
        },
      });            
  });

    it("Profile Save Failed", async () => {
        expect(getFilterLayoutsReducer(undefined,{
            type: profileConstants.PROFILE_SAVE_FAILED,
            payload:null,
        })).toStrictEqual({ 
            ...defaultState
        });   
        
        expect(getFilterLayoutsReducer(undefined,{
            type: profileConstants.PROFILE_MARKFAV_FAILED,
            payload:null,
        })).toStrictEqual({ 
            ...defaultState
        }); 
    })


    it("Set Current View", async () => {
        expect(getFilterLayoutsReducer(undefined,{
            type: profileConstants.PROFILE_SET_CURRENT_VIEW,
            payload:{variables:{
                id:3,
                name:"My Filter View3",
                filter:{}
            }, tableName:'network'},
        })).toStrictEqual({ 
            ...defaultState,currentView: {
                ...{
                  "network":{},
                  "transport":{},
                  "orders":{},
                  "distoutbound":{},
                  "distinbound":{}
              },
              network:{
                id:3,
                name:"My Filter View3",
                filter:{}
            }}
        });            
    })


    it("Set Current View", async () => {

      const state ={...defaultState,
        filterLayouts:successPayload1.layout.filterLayouts,
      }
        expect(getFilterLayoutsReducer(state,
            {
            type: profileConstants.PROFILE_SET_CURRENT_VIEW,
            payload:{variables:{
                id:3,
                name:"My Filter View3",
                filter:{}
            }, tableName:'network'},
        })).toStrictEqual({ 
            ...state,
            currentView: {
              ...{
                "network":{...successPayload1.layout?.filterLayouts?.["NETWORK"]?.views?.filter(view => view.default === true)[0]},
                "transport":{...successPayload1.layout?.filterLayouts?.["TRANSPORT"]?.views?.filter(view => view.default === true)[0]},
                "orders":{...successPayload1.layout?.filterLayouts?.["ORDERS"]?.views?.filter(view => view.default === true)[0]},
                "distoutbound":{...successPayload1.layout?.filterLayouts?.["DISTOUTBOUND"]?.views?.filter(view => view.default === true)[0]},
                "distinbound":{...successPayload1.layout?.filterLayouts?.["DISTINBOUND"]?.views?.filter(view => view.default === true)[0]}
              },
              network:{
                id:3,
                name:"My Filter View3",
                filter:{}
            }}
        });            
    })

    it("Set First Load", async () => {
      expect(getFilterLayoutsReducer(undefined,{
          type: profileConstants.SET_FIRST_LOAD,
          payload:false
      })).toStrictEqual({ 
          ...defaultState, firstLoad:false
      });            
  })

})