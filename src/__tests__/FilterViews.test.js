import { waitFor } from '@testing-library/react';
import FilterView, { saveView } from 'components/common/FilterView';
import FilterViewsMenu from 'components/common/FilterViewsMenu';
import { mockStoreValue } from 'components/dummydata/mock';
import { getTestElement, renderWithMockStore, fireEvent, reRenderWithMockStore } from '../testUtils';
import networkfilters from "../reduxLib/constdata/networkFilters";

describe('<Filter View Component>', () => {

    afterAll(() => {
        jest.restoreAllMocks();
    });

    const props = {
        tableName: "network", 
        type: "network", 
        pageDetails:{
            filterParams:{}
        },
        filterBody:networkfilters
    }

    const props3 = {
        tableName: "network", 
        type: "network", 
        pageDetails:{
            filterParams:{}
        },
        prevPageDetails:{activeTopNavTab:'mylocation'},
        filterBody:networkfilters
    }

    const props2 = {
        tableName: "network",
        filterBody:networkfilters, 
        type: "network", 
        pageDetails:{
            filterParams:{
                "args": {
                    "orderStatusBucket": [
                        "SO_BLOCKED_NON_TMS_PLANNED_IMMEDIATE_ACTION",
                        "SO_BLOCKED_TMS_PLANNED_VISIBLE",
                        "SO_BLOCKED_TMS_PLANNED_NOT_VISIBLE",
                        "SO_BLOCKED_NON_TMS_PLANNED_PICKUP_NOT_SCHEDULED",
                        "SO_BLOCKED_NON_TMS_PLANNED_PICKUP_MULTI_BLOCK"
                    ]
                },
                "health": "RED",
                "state": "ORDER"
            }
        }
    }

    const customMock1 = {...mockStoreValue, user:{...mockStoreValue.user,currentView:{}, firstLoad:true}, options:{ ...mockStoreValue.options, filters:{...mockStoreValue.options.filters, network: {...mockStoreValue.options.filters.network,"tariffServiceCode": {
        "type": "text",
        "name": "Carrier Service Code",
        "data": ["PKUP,HKMD","VHGB"]
        },
        deliveryBlockCodeList: {
            type: "textcheck",
            name: "Delivery/Credit Block",
            placeholder: 'Enter delivery block code',
            shortName: "Delivery Block Code",
            addon: "creditOnHold",
            "data": ["XX","AA","VB"]
        },
    }}}}

    const customMock2 = {...mockStoreValue, options:{ ...mockStoreValue.options, filters:{...mockStoreValue.options.filters, network: {...mockStoreValue.options.filters.network,"tariffServiceCode": {
        "type": "text",
        "name": "Carrier Service Code",
        "data": ["PKUP,HKMD"]
        },
        deliveryBlockCodeList: {
            type: "textcheck",
            name: "Delivery/Credit Block",
            placeholder: 'Enter delivery block code',
            shortName: "Delivery Block Code",
            addon: "creditOnHold",
            "data": ["XX"]
        },
    }}}}

    const customMock3 = {...mockStoreValue, options:{ ...mockStoreValue.options, filters:{...mockStoreValue.options.filters, network: {...mockStoreValue.options.filters.network,"tariffServiceCode": {
        "type": "text",
        "name": "Carrier Service Code",
        "data": []
        },
        deliveryBlockCodeList: {
            type: "textcheck",
            name: "Delivery/Credit Block",
            placeholder: 'Enter delivery block code',
            shortName: "Delivery Block Code",
            addon: "creditOnHold",
            "data": []
        },
    }}}}
    it('Filter View should render', async () => {
        const { rerender } = await renderWithMockStore(FilterView, props);
        const component = getTestElement('filterview');
        rerender(await reRenderWithMockStore(FilterView, props2));
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
    });

    it('Filter View should render', async () => {
        const { rerender } = await renderWithMockStore(FilterView, props,customMock1);
        const component = getTestElement('filterview');
        rerender(await reRenderWithMockStore(FilterView, props, customMock1));
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
    });

    it('Filter View should render', async () => {
        await renderWithMockStore(FilterView, props,customMock2);
        const component = getTestElement('filterview');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
    });

    it('Filter View should render', async () => {
        await renderWithMockStore(FilterView, props3, customMock2);
        const component = getTestElement('filterview');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
    });

    it('Filter View should render', async () => {
        await renderWithMockStore(FilterView, props, customMock3);
        const component = getTestElement('filterview');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
    });

    

    it('Test New View Toggle', async () => {        
        const { getByTestId } = await renderWithMockStore(FilterView, props);
        fireEvent.click(getByTestId("togglenewviewicon"))
        waitFor(()=> getByTestId("savenewfromcurrent"))
        expect(getByTestId("savenewfromcurrent")).toBeInTheDocument()
        fireEvent.click(getByTestId("savenewfromcurrent"))
    });

    it('Test Save/Delete View', async () => {        
        const { getByTestId } = await renderWithMockStore(FilterView, props);
        fireEvent.click(getByTestId("savecurrentview"));
        fireEvent.click(getByTestId("deletecurrentview"));
    });

    xit('Test Save Form  Submission', async () => {        
        const { getByTestId } = await renderWithMockStore(FilterView, props);
        fireEvent.click(getByTestId("togglenewviewicon"))
        fireEvent.change(getByTestId("savenewviewforminput"), { target: { value: "abc" } });
        fireEvent.keyPress(getByTestId("savenewviewforminput"), { key: "Enter", code: 13, charCode: 13 }); 
        fireEvent.submit(getByTestId("savenewviewforminput"));
    
    });

    it('Test Clear', async () => {        
        const { getByTestId } = await renderWithMockStore(FilterView, props);
        waitFor(()=>fireEvent.click(getByTestId("clearfilters")))    
        waitFor(()=>fireEvent.click(getByTestId("resetfilters")))    
        expect(getByTestId("chip")).toBeInTheDocument()
    });

    // it('Test Reset and Clear', async () => {        
    //     const { getByTestId } = await renderWithMockStore(FilterView, props);
    //     fireEvent.click(getByTestId("togglenewviewicon"))    
    // });


    it('Filter Views Menu should render', async () => {
        await renderWithMockStore(FilterViewsMenu, props);
        const component = getTestElement('filterviewmenu');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
    });

    it('Test New View Creation (Filter Views Menu)', async () => {   
        const { getAllByTestId, getAllByText } = await renderWithMockStore(FilterViewsMenu, props);
        fireEvent.click(getAllByTestId("filterviewmenubutton")[0]);
        fireEvent.click(getAllByText(/Add View/i)[0]);
        fireEvent.change(getAllByTestId("savenewviewforminput")[0], { target: { value: "abc" } });
        fireEvent.keyPress(getAllByTestId("savenewviewforminput")[0], { key: "Enter", code: 13, charCode: 13 }); 
        fireEvent.submit(getAllByTestId("savenewviewforminput")[0]);
    });
    it('Mark Fav/Unfav and Delete', async () => {        
        const { getAllByTestId } = await renderWithMockStore(FilterViewsMenu, props);
        fireEvent.click(getAllByTestId("filterviewmenubutton")[0]);
        fireEvent.click(getAllByTestId("markfavourite")[0]);
        fireEvent.click(getAllByTestId("deletecurrentview")[0]);
    });



    xit(" Save View Return the Payload", async () => {
         expect(saveView(
{           createNewView:false,
            tableName:"network",
            viewName: "Untitled 1",
            filterBody: {},
            filters: {},
            currentView: {},
            initialLayouts: [],
            setFilterLayouts: jest.fn(),}
         ))
    });

});