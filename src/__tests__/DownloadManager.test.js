import DownloadMGR, { DownloadManager } from 'components/common/Table/DownloadManager';
import { getTestElement, renderWithProvider, renderWithMockStore } from '../testUtils';

jest.mock('react-json-csv', () => {
    return {
        useJsonToCsv: () => {
            return {
                saveAsCsv: jest.fn()
            }
        }
    };
});


describe('<DownloadManager Component>', () => {

    afterAll(() => {
        jest.restoreAllMocks();
    });


    let props = {
        resetDownload: jest.fn(),
        downloads: {
            network: {
                network: {
                    columns: [{ field: "orderNum", title: "orderNum" }],
                    status: "Success",
                    data: [{ orderNum: "1213" }],
                    process: jest.fn(),
                    detailsColumns: [{ field: "materialNum", title: "materialNum" }],
                    detailsProcess: jest.fn(),
                    itemInfoRequired: false
                }
            }
        },
    }


    it('DownloadManager should render -  Success Condition', async () => {
        await renderWithProvider(DownloadManager, props);
        const component = getTestElement('downloadmanager');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
    });

    it('DownloadManager should render without Provide - Failed Condition', async () => {
        props = {
            resetDownload: jest.fn(),
            downloads: {
                network: {
                    network: {
                        columns: [{ field: "orderNum", title: "orderNum" }],
                        status: "Failed",
                        data: [{ orderNum: "1213" }],
                        process: jest.fn()
                    }
                }
            },
        }
        await renderWithProvider(DownloadManager, props);
        const component = getTestElement('downloadmanager');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
    });

    it('DownloadManager should render with Store', async () => {
        await renderWithMockStore(DownloadMGR, props);
        const component = getTestElement('downloadmanager');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
    });

    it('Close Button Click Test', async () => {
        const wrapper = await renderWithProvider(DownloadManager, props);
        const submitButton = await wrapper.findByTitle("Close");
        const test = submitButton.dispatchEvent(new MouseEvent('click', { bubbles: true }));
        expect(test).toBeTruthy();
    });

});