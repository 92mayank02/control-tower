import { getTestElement, renderWithMockStore, fireEvent } from '../testUtils';
import FavTickElement from "components/common/FavUnfavElement";

describe('<FavTickElement>', () => {
    afterAll(() => {
        jest.restoreAllMocks();
    });

    const storeprops = {
        items: {},
        key: 2047,
        favClick: jest.fn(),
        unfavClick: jest.fn(),
        setUnfavorite: jest.fn(),
        data: { "siteNum": "2047", "region": "NA", "type": "DC", "plantType": "RDC", "alias": "LADC", "shortName": "Los Angeles DC", "siteName": "S_2047_917", "siteNameDesc": "Los Angeles DC ExtOps/DC", "location": { "id": "2047", "name": "Los Angeles DC", "city": "ONTARIO", "state": "CA", "country": "USA", "street": "4815 S HELLMAN AVE", "geoLocation": { "longitude": -117.602477, "latitude": 34.040893 }, "timezone": "PST", "postalCode": "91762" }, "isFav": true }
    }

    const storeprops2 = {
        data:{
            shortName:`DVS`,
            salesGroup:'2811',
            fromState:{},
            reducerObject:{
                salesOffice:`DVS`,
                selectionType:"SALES_OFFICE",
                salesOrg:2811,
                distributionChannel:80,
            }
        },
        favClick: () => { return { type:"REMOVE_CUSO_SUCCESS", payload:{} } },
        unfavClick: () => { return { type:"REMOVE_CUSO_SUCCESS", payload:{} } },
        setUnfavorite: jest.fn(),
        checkClick:() => { return { type:"REMOVE_CUSO_SUCCESS", payload:{} } },
        unCheckClick:() => { return { type:"REMOVE_CUSO_SUCCESS", payload:{} } },
        handleClick:jest.fn(),
        clickHandle: () => { return { type:"REMOVE_CUSO_SUCCESS", payload:{} } }
    }

    it('FavTickElement Should Render ', async () => {
        await renderWithMockStore(FavTickElement, storeprops);
        const component = getTestElement('favtickelement');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
    });

    it('Tooltip Click', async () => {
        const { getByTestId } =  await renderWithMockStore(FavTickElement, storeprops2) 
        expect(getByTestId("ctLink")).toBeInTheDocument();
        fireEvent.click(getByTestId("ctLink"));
    });

    it('Click Fav/Tick Element', async () => {
        const { getAllByTestId } = await renderWithMockStore(FavTickElement, storeprops2);
        const component = getTestElement('favtickelement');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
        fireEvent.click(getAllByTestId('tickIcon')[0]);
        fireEvent.click(getAllByTestId('favIcon')[0]);
    });

    it('Click Fav/Tick Element', async () => {
        const { getAllByTestId } = await renderWithMockStore(FavTickElement, {...storeprops2, fav: true, tick: true});
        const component = getTestElement('favtickelement');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
        await fireEvent.click(getAllByTestId('tickIcon')[0]);
        await fireEvent.click(getAllByTestId('favIcon')[0]);
    });

});