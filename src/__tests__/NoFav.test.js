import NoResult from '../components/header/NoFav';
import { getTestElement, renderWithProvider } from '../testUtils';


describe('<No Fav Component>', () => {
    afterAll(() => {
      jest.restoreAllMocks();
    });
  
    const props = {label:'TestDetails'}
    it('should render with label', async () => {
      await renderWithProvider(NoResult,props);
      const component = getTestElement('nofav');
      expect(component).toBeInTheDocument();
      expect(component).toBeTruthy();
    });

});