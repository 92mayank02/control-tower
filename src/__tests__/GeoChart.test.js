import {GeoCharts} from '../theme/layouts/components/GeoChart';
import { getTestElement, shallowSetup, renderWithProvider } from '../testUtils';


describe('<Geo Chart>', () => {
    afterAll(() => {
      jest.restoreAllMocks();
    });
  
    const props = {
      sites:[]
    }
    it('should render with label', async () => {      
      const wrapper = shallowSetup(GeoCharts,props);
      expect(wrapper.exists()).toBeTruthy();  
    });

});