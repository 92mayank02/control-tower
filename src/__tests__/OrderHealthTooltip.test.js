import {OrderHealthTooltip} from '../components/common/OrderHealthTooltip';
import { getTestElement, renderWithProvider } from '../testUtils';

describe('<Order Health Tooltip>', () => {
    afterAll(() => {
      jest.restoreAllMocks();
    });
  
    const props = {
        data:{"orderNum":"a114","orderExecutionBucketDesc":"Transportation Planning","orderDestination":{"name":"WALGREENS PERRYSBURG","city":"PERRYSBURG","state":"OH"},"originSiteNum":"2292","orderOrigin":{"id":"2292","name":"North Central DC","city":"ROMEOVILLE","state":"IL","country":"USA","street":"740 PROLOGIS PARKWAY","geoLocation":{"longitude":-88.1021821,"latitude":41.6561017},"timezone":"US/Central","postalCode":"60446-4502"},"customerNotifiedDateTime":[],"deliveryApptConfirmedDateTimeList":[],"orderExecutionBucket":"TRANS_PLAN_OPEN","orderExecutionHealth":"RED","originTimeZone":"US/Central","orderType":"STO","shippingCondition":"TL","durationInYardMS":0,"orderHealth":"RED","orderHealthAndExecutionHealth":"RED","orderStatusBucket":"STO","orderStatusBucketDesc":"Stock Transfer Orders","orderExecutionReasonCodes":["UNASSIGNED_SHORT_LEAD","UNASSIGNED_PAST_DUE_PLANNING","TRANS_OPEN_SHORT_LEAD","TRANS_OPEN_NOT_ON_TRACK","TRANS_PROCESS_SHIP_PAST","TRANS_PROCESS_SHIP_SOON"],"orderStatusReasonCodes":["NOT_CONFIRM_LESS_X_CUBE_SOON_MAD","NOT_CONFIRM_LESS_X_CUBE_PAST_MAD","NOT_CONFIRM_0_CUBE_SOON_MAD","NOT_CONFIRM_0_CUBE_PAST_MAD","NOT_CONFIRM_CUBE_SOON_MAD","STO_SHORT_LEAD","STO_LESS_PRODUCT","STO_HEAVY","STO_LARGE"],"inboundOrderExecutionReasonCodes":[],"orderStatusAndExecutionStatusReasonCodes":["UNASSIGNED_SHORT_LEAD","UNASSIGNED_PAST_DUE_PLANNING","TRANS_OPEN_SHORT_LEAD","TRANS_OPEN_NOT_ON_TRACK","TRANS_PROCESS_SHIP_PAST","TRANS_PROCESS_SHIP_SOON","NOT_CONFIRM_LESS_X_CUBE_SOON_MAD","NOT_CONFIRM_LESS_X_CUBE_PAST_MAD","NOT_CONFIRM_0_CUBE_SOON_MAD","NOT_CONFIRM_0_CUBE_PAST_MAD","NOT_CONFIRM_CUBE_SOON_MAD","STO_SHORT_LEAD","STO_LESS_PRODUCT","STO_HEAVY","STO_LARGE"]},
        tableName:'network'
    }

    const yellowReasonProps = {
        data:{"orderNum":"a114","orderExecutionBucketDesc":"Transportation Planning","orderDestination":{"name":"WALGREENS PERRYSBURG","city":"PERRYSBURG","state":"OH"},"originSiteNum":"2292","orderOrigin":{"id":"2292","name":"North Central DC","city":"ROMEOVILLE","state":"IL","country":"USA","street":"740 PROLOGIS PARKWAY","geoLocation":{"longitude":-88.1021821,"latitude":41.6561017},"timezone":"US/Central","postalCode":"60446-4502"},"customerNotifiedDateTime":[],"deliveryApptConfirmedDateTimeList":[],"orderExecutionBucket":"TRANS_PLAN_OPEN","orderExecutionHealth":"YELLOW","originTimeZone":"US/Central","orderType":"STO","shippingCondition":"TL","durationInYardMS":0,"orderHealth":"YELLOW","orderHealthAndExecutionHealth":"YELLOW","orderStatusBucket":"STO","orderStatusBucketDesc":"Stock Transfer Orders","orderExecutionReasonCodes":["UNASSIGNED_SHORT_LEAD","UNASSIGNED_PAST_DUE_PLANNING","TRANS_OPEN_SHORT_LEAD","TRANS_OPEN_NOT_ON_TRACK","TRANS_PROCESS_SHIP_PAST","TRANS_PROCESS_SHIP_SOON"],"orderStatusReasonCodes":["NOT_CONFIRM_LESS_X_CUBE_SOON_MAD","NOT_CONFIRM_LESS_X_CUBE_PAST_MAD","NOT_CONFIRM_0_CUBE_SOON_MAD","NOT_CONFIRM_0_CUBE_PAST_MAD","NOT_CONFIRM_CUBE_SOON_MAD","STO_SHORT_LEAD","STO_LESS_PRODUCT","STO_HEAVY","STO_LARGE"],"inboundOrderExecutionReasonCodes":[],"orderStatusAndExecutionStatusReasonCodes":["UNASSIGNED_SHORT_LEAD","UNASSIGNED_PAST_DUE_PLANNING","TRANS_OPEN_SHORT_LEAD","TRANS_OPEN_NOT_ON_TRACK","TRANS_PROCESS_SHIP_PAST","TRANS_PROCESS_SHIP_SOON","NOT_CONFIRM_LESS_X_CUBE_SOON_MAD","NOT_CONFIRM_LESS_X_CUBE_PAST_MAD","NOT_CONFIRM_0_CUBE_SOON_MAD","NOT_CONFIRM_0_CUBE_PAST_MAD","NOT_CONFIRM_CUBE_SOON_MAD","STO_SHORT_LEAD","STO_LESS_PRODUCT","STO_HEAVY","STO_LARGE"]},
        tableName:'network'
    }

    const greenReasonProps = {
        data:{"orderNum":"a114","orderExecutionBucketDesc":"Transportation Planning","orderDestination":{"name":"WALGREENS PERRYSBURG","city":"PERRYSBURG","state":"OH"},"originSiteNum":"2292","orderOrigin":{"id":"2292","name":"North Central DC","city":"ROMEOVILLE","state":"IL","country":"USA","street":"740 PROLOGIS PARKWAY","geoLocation":{"longitude":-88.1021821,"latitude":41.6561017},"timezone":"US/Central","postalCode":"60446-4502"},"customerNotifiedDateTime":[],"deliveryApptConfirmedDateTimeList":[],"orderExecutionBucket":"TRANS_PLAN_OPEN","orderExecutionHealth":"GREEN","originTimeZone":"US/Central","orderType":"STO","shippingCondition":"TL","durationInYardMS":0,"orderHealth":"GREEN","orderHealthAndExecutionHealth":"GREEN","orderStatusBucket":"STO","orderStatusBucketDesc":"Stock Transfer Orders","orderExecutionReasonCodes":["UNASSIGNED_SHORT_LEAD","UNASSIGNED_PAST_DUE_PLANNING","TRANS_OPEN_SHORT_LEAD","TRANS_OPEN_NOT_ON_TRACK","TRANS_PROCESS_SHIP_PAST","TRANS_PROCESS_SHIP_SOON"],"orderStatusReasonCodes":["NOT_CONFIRM_LESS_X_CUBE_SOON_MAD","NOT_CONFIRM_LESS_X_CUBE_PAST_MAD","NOT_CONFIRM_0_CUBE_SOON_MAD","NOT_CONFIRM_0_CUBE_PAST_MAD","NOT_CONFIRM_CUBE_SOON_MAD","STO_SHORT_LEAD","STO_LESS_PRODUCT","STO_HEAVY","STO_LARGE"],"inboundOrderExecutionReasonCodes":[],"orderStatusAndExecutionStatusReasonCodes":["UNASSIGNED_SHORT_LEAD","UNASSIGNED_PAST_DUE_PLANNING","TRANS_OPEN_SHORT_LEAD","TRANS_OPEN_NOT_ON_TRACK","TRANS_PROCESS_SHIP_PAST","TRANS_PROCESS_SHIP_SOON","NOT_CONFIRM_LESS_X_CUBE_SOON_MAD","NOT_CONFIRM_LESS_X_CUBE_PAST_MAD","NOT_CONFIRM_0_CUBE_SOON_MAD","NOT_CONFIRM_0_CUBE_PAST_MAD","NOT_CONFIRM_CUBE_SOON_MAD","STO_SHORT_LEAD","STO_LESS_PRODUCT","STO_HEAVY","STO_LARGE"]},
        tableName:'network'
    }
    it('Show read reason tooltip', async () => {
      await renderWithProvider(OrderHealthTooltip,props);
      const component = getTestElement('orderhealthtooltip');
      expect(component).toBeInTheDocument();
    });

    it('Show read reason tooltip', async () => {
        await renderWithProvider(OrderHealthTooltip,yellowReasonProps);
        const component = getTestElement('orderhealthtooltip');
        expect(component).toBeInTheDocument();
    });

    it('Dont Show read reason tooltip', async () => {
        await renderWithProvider(OrderHealthTooltip,greenReasonProps);
        const component = getTestElement('orderhealthtooltip');
        expect(component).toBe(undefined);
    });

});