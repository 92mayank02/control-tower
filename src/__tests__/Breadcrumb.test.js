import { Breadcrumb } from '../components/common/Breadcrumb';
import { getTestElement, renderWithProvider, screen, fireEvent, reRenderWithMockStore } from '../testUtils';
import { render } from '@testing-library/react';

describe('<Breadcrumb>', () => {
  afterAll(() => {
    jest.restoreAllMocks();
  });

  const setDetails = (type, subtype, value, filterParams = {}) => {
    return true
  }

  const BreadcrumbProps = { setDetails, type: "network", label: 'TestDetails', navHistory: [{ id: "TRANS_PLAN", label: "Transportation" }] }
  const BreadcrumbProps2 = { setDetails, type: "network", }
  const BreadcrumbProps3 = { type: "network", }


  it('should render with label', async () => {
    await render(await reRenderWithMockStore(Breadcrumb, BreadcrumbProps));
    const component = getTestElement('breadcrumb');
    expect(component).toBeInTheDocument();
    expect(component).toHaveTextContent('TestDetails');
    expect(component).toBeTruthy();
  });

  it('should render with label', async () => {
    await renderWithProvider(Breadcrumb, BreadcrumbProps);
    const component = getTestElement('breadcrumb');
    expect(component).toBeInTheDocument();
    expect(component).toHaveTextContent('TestDetails');
    expect(component).toBeTruthy();
  });

  it('should render with label- click', async () => {
    const { getByTestId } = await renderWithProvider(Breadcrumb, BreadcrumbProps);
    const component = getTestElement('breadcrumb');
    expect(component).toBeInTheDocument();
    expect(component).toHaveTextContent('TestDetails');
    fireEvent.click(getByTestId("breadcrumb_home"))
  });
  it('should render with label - click', async () => {
    const { getByTestId } = await renderWithProvider(Breadcrumb, BreadcrumbProps);
    const component = getTestElement('breadcrumb');
    expect(component).toBeInTheDocument();
    expect(component).toHaveTextContent('TestDetails');
    fireEvent.click(getByTestId("intermediatebreadcrumb_TRANS_PLAN"))
  });


  it('should render with label', async () => {
    await renderWithProvider(Breadcrumb, BreadcrumbProps2);
    const component = getTestElement('breadcrumb');
    expect(component).not.toHaveTextContent('TestDetails');
  });

  it('Test Set Details', async () => {
    await renderWithProvider(Breadcrumb, BreadcrumbProps);
    fireEvent.click(screen.queryByText(/home/i));
    expect(screen.queryByText(/home/i)).toBeVisible();
  });


  it('Without Set Details', async () => {
    await renderWithProvider(Breadcrumb, BreadcrumbProps3);
    fireEvent.click(screen.queryByText(/home/i));
    expect(screen.queryByText(/home/i)).toBeVisible();
  })

});