import AnalyticsReport from '../components/common/AnalyticsReport';
import { getTestElement, renderWithProvider } from '../testUtils';

describe('<AnalyticsReport>', () => {
  afterAll(() => {
    jest.restoreAllMocks();
  });

  const AnalyticsProps = { pageDetails: { filterParams : { reportURL: "https://app.powerbi.com/reportEmbed?reportId=b861ca9e-412d-44dc-b505-808692d5c1a3&autoAuth=true&ctid=fee2180b-69b6-4afe-9f14-ccd70bd4c737&config=eyJjbHVzdGVyVXJsIjoiaHR0cHM6Ly93YWJpLXVzLW5vcnRoLWNlbnRyYWwtZS1wcmltYXJ5LXJlZGlyZWN0LmFuYWx5c2lzLndpbmRvd3MubmV0LyJ9"}}};

  it('should render Power Bi report', async () => {
    await renderWithProvider(AnalyticsReport, AnalyticsProps);
    const component = getTestElement('powerBiReport');
    expect(component).toBeInTheDocument();
  });

})