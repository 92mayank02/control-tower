import {
    formatToSentenceCase,
    generateTablebody,
    msToHMS,
    tableoptions,
    refreshTable,
    calcPercentage,
    generateDefaultData,
    getStatusData,
    capitalizeWord,
    sortOrder,
    getId,
    getTabsData,
    generateHeader,
    defaultShowTabsBy,
    generateFilterLayoutTemplate,
    mergeSalesGroups,
    stringToArray,
    getUpdatedColumns,
    createHyperLinkFilterObject,
    searchKey,
    processGuardRailData
} from "../helpers";

import { appTheme } from "../theme"
import { v4 as uuid } from 'uuid';
import { mockGuardrailsDetails } from "components/dummydata/mock";

jest.mock('uuid');

jest.mock('react-cookies', () => {
    return {
        save: jest.fn(),
        load: (key) => true
    }
});

import networkfilters from "reduxLib/constdata/networkFilters";
import ordersfilters from "reduxLib/constdata/ordersFilters";
import transportfilters from "reduxLib/constdata/transportFilters";
import distinboundfilters from "reduxLib/constdata/distinboundFilters";
import distoutboundfilters from "reduxLib/constdata/distoutboundFilters";


describe("Test Helper Functions", () => {

    afterEach(() => {
        jest.clearAllMocks();
    });

    it("formatToSentenceCase", () => {

        const withDC = "WALMART DC #7015-HVDC"
        const withoutDC = "DOLLAR TREE STORES INC DC9"

        expect(formatToSentenceCase(withoutDC)).toStrictEqual('Dollar Tree Stores Inc Dc9');
        expect(formatToSentenceCase(withDC)).toStrictEqual('Walmart DC #7015-Hvdc');

    });

    it("milisecondsToHours", () => {
        const number = "7200000"
        const nonNumber = "String"
        expect(msToHMS(number)).toStrictEqual("2.00 Hours");
        expect(msToHMS(nonNumber)).toStrictEqual("-");
        expect(msToHMS(15000)).toStrictEqual("0.00 Hours");

    });

    it("Calculate Percentage", () => {
        expect(calcPercentage(10, 5)).toBe(50);
        expect(calcPercentage(5, 5)).toBe(0)
        expect(calcPercentage(0, 5)).toBe(0)
        expect(calcPercentage(-2, 40)).toBe(2100)
        expect(calcPercentage(2.2222, 40.223)).toBe(-1710)
    })

    it("Generate Default Data", () => {
        const columns = [
            { title: 'Shipment #', field: "shipmentNum" },
            { title: 'Shipment Status', field: "orderExecutionBucketDesc" },
        ];
        expect(generateDefaultData(columns)["shipmentNum"]).toBe("-");
        expect(generateDefaultData(columns)["orderExecutionBucketDesc"]).toBe("-");
    });

    it("Generate Table Body", () => {
        const filters = {
            "sites": {
                "type": "sites",
                "name": "Origin",
                "data": null
            },
            "searchStringList": {
                "type": "text",
                "name": "Search",
                "data": "Test String"
            },
            "orderStatusBucket": {
                "type": "checkbox",
                "name": "Order Status",
                "data": [
                    {
                        "name": "Order Block Free",
                        "value": "ORDER_BLOCK_FREE",
                        "checked": true
                    },
                    {
                        "name": "Order Blocked",
                        "value": "ORDER_BLOCKED",
                        "checked": false
                    }
                ]
            },
            "orderTypes": {
                "type": "checkboxradio",
                "name": "Order Type",
                "data": [
                    {
                        "name": "Cust. Order",
                        "value": "CUST",
                        "checked": true
                    },
                    {
                        "name": "STO",
                        "value": "STO",
                        "checked": false
                    }
                ]
            },
        }

        const filtersObjectWithError = {
            "sites": {
                "type": "sites",
                "name": "Origin",
                "data": null
            },
            "searchStringList": {
                "type": "text",
                "name": "Search",
            },
            "orderStatusBucket": {
                "type": "checkbox",
                "name": "Order Status",
            },
        }

        const tableBody = generateTablebody(filters);

        expect(generateTablebody(filters, ["2050"])['sites']).toStrictEqual(["2050"]);
        expect(generateTablebody(filtersObjectWithError)).toStrictEqual({});
        expect(generateTablebody({})).toStrictEqual({});

        expect(tableBody['sites']).toBe(null)
        expect(tableBody.searchStringList).toBe("Test String");
        expect(tableBody.orderStatusBucket).toStrictEqual(["ORDER_BLOCK_FREE"]);
        expect(tableBody.orderTypes).toStrictEqual("CUST");
    });

    it('Should Calculate Percentage with 0', () => {
        expect(calcPercentage(10, 10)).toEqual(0);
    });

    it('Should Calculate Percentage with 50', () => {
        expect(calcPercentage(20, 10)).toEqual(50);
    });

    it('Should Calculate Percentage with 50', () => {
        expect(calcPercentage(0, 0)).toEqual(0);
    });

    it('formatToSentenceCase', () => {
        expect(formatToSentenceCase("TestAA")).toEqual("Testaa");
    });

    it('formatToSentenceCase DC Test', () => {
        expect(formatToSentenceCase("dc")).toEqual("DC");
    });


    it('generateTablebody', () => {
        expect(generateTablebody(networkfilters)).toEqual({
            "actualDeliveredDateTime": null,
            "deliveryApptDateTimeDestTZ": null,
            "destCity": null,
            "destName": null,
            "destState": null,
            "inboundOrderExecutionBucket": [],
            "loadReadyDateTimeOriginTZ": null,
            "matAvailDate": null,
            "orderExecutionBucket": [],
            "orderHealthAndExecutionHealth": [],
            "orderStatusBucket": [],
            "orderTypes": [],
            "requestedDeliveryDate": null,
            "originalRequestedDeliveryDate": null,
            "searchStringList": null,
            "sites": null,
            "tariffServiceCodeList": null,
            "materialNumberList": null
        });
    });

    const filters = {
        orderExecutionBucket: {
            type: "checkbox",
            name: "Shipment Status",
            data: [
                { name: "Transportation to be planned", value: "TRANS_PLAN_UNASSIGNED", checked: true },
                { name: "Transportation planning", value: "TRANS_PLAN_OPEN", checked: false },
                { name: "Transportation processing", value: "TRANS_PLAN_PROCESSING", checked: false },
                { name: "Transportation Carrier committed", value: "TRANS_PLAN_TENDER_ACCEPTED", checked: false },
                { name: "Shipment Created", value: "SHIPMENT_CREATED", checked: false },
                { name: "Checked in by DC", value: "CHECK_IN", checked: false },
                { name: "Loading Started", value: "LOADING_STARTED", checked: false },
                { name: "Ready for Pickup", value: "TRANS_EXEC_READY_PICK_UP", checked: false },
                { name: "In Transit", value: "TRANS_EXEC_IN_TRANSIT", checked: false },
                { name: "Delivery confirmed", value: "TRANS_EXEC_DELIVERY_CONFIRMED", checked: false }
            ]
        },
    }

    it('generateTablebody', () => {
        expect(generateTablebody(filters)).toEqual({
            orderExecutionBucket: ["TRANS_PLAN_UNASSIGNED"]
        });
    });

    it('generateTablebody Catch Condition', async () => {
        expect(generateTablebody(jest.fn(), null)).toEqual({});
    });

    const checkboxradio = {
        durationInYard: {
            type: "checkboxradio",
            name: "Duration in Yard",
            data: [
                { name: "< 24 Hours", value: { startTimeMins: null, endTimeMins: 1439 }, checked: true },
                { name: "24 - 48 Hours", value: { startTimeMins: 1440, endTimeMins: 2880 }, checked: false },
                { name: "> 72 Hours", value: { startTimeMins: 4321, endTimeMins: null }, checked: false },
            ]
        },
    }

    it('generateTablebody checkboxradio checked conditions', () => {
        expect(generateTablebody(checkboxradio)).toEqual({
            "durationInYard": {
                "endTimeMins": 1439,
                "startTimeMins": null,
            }
        });
    });


    it('generateDefaultData', () => {
        expect(generateDefaultData([{ field: "name" }])).toEqual({ name: "-" });
    });

    it('refreshTable', () => {

        const ref = {
            tableRef: {
                current: {
                    onQueryChange: jest.fn()
                }
            }
        }
        const prom = new Promise((res, rej) => {
            try {
                refreshTable(ref);
                res(true)
            } catch {
                rej(false)
            }
        }).catch()

        expect(prom).resolves.toEqual(true);
    });


    it('msToHMS', () => {
        expect(msToHMS(10000)).toEqual("0.00 Hours");
        expect(msToHMS(10000000)).toEqual("2.78 Hours");
    });

    it('capitalizeWord', () => {
        expect(capitalizeWord("hello")).toEqual("Hello");
    });

    it('msToHMS NaN Condition', () => {
        expect(msToHMS("Test")).toEqual("-");
    });

    it('geStatusData', () => {
        expect(getStatusData({ hours: 0 })).toEqual({ hours: 0, status: "No Data", "color": appTheme.palette.legendColors.greyLight });
        expect(getStatusData({ hours: 4 })).toEqual({ hours: 4, status: "Healthy", "color": appTheme.palette.legendColors.healthyGreen });
        expect(getStatusData({ hours: 3 })).toEqual({ hours: 3, status: "Monitor", "color": appTheme.palette.legendColors.risk });
        expect(getStatusData({ hours: -1 })).toEqual({ hours: -1, status: "Behind", "color": appTheme.palette.legendColors.pink });
        expect(getStatusData({ hours: -6 })).toEqual({ hours: -6, status: "Critical", "color": appTheme.palette.legendColors.criticalRed });
        expect(getStatusData({})).toEqual({ status: "No Data", "color": appTheme.palette.legendColors.greyLight });
    });

    it('sortOrder', () => {

        const data = [
            {
                state: "test2",
                value: 100
            },
            {
                state: "test",
                value: 100
            },
        ]

        expect(sortOrder({ order: ["test", "test2"], key: "test", data })).toEqual([{ state: "test2", value: 100 }, { state: "test", value: 100 }]);
    });


    it('getId', () => {
        expect(getId({ type: "DC", siteNum: "100" }, "type")).toEqual("100");
        expect(getId({ type: "SALES_OFFICE", selectionType: "Test" }, "type")).toEqual("Test_undefined_undefined_undefined_");
        expect(getId({ type: "CUST", selectionType: "Test" }, "type")).toEqual("Test_undefined_undefined_undefined");
    });


    it('getTabsData', () => {
        expect(getTabsData([{ type: "DC", siteNum: "100" }], { name: "test", type: "DC" }))
            .toEqual([{ "custom": { "id": undefined, "idParent": undefined, "name": "undefined" }, "siteNum": "100", "type": "DC" }]);


        expect(getTabsData([{ type: "DC", name: "hello", siteNum: "100", salesGroup: [100] }], { name: "name", type: "DC" }))
            .toEqual([{ "custom": { "id": undefined, "idParent": undefined, salesGroup: [100], "name": "helloundefined100" }, name: "hello", "siteNum": "100", "type": "DC", salesGroup: [100] }]);

        expect(getTabsData([{ type: "DC", name: "hello", siteNum: "100", customerName: "test", salesOrg: "testorg" }], { name: "name", type: "DC" }))
            .toEqual([{ "custom": { "id": undefined, "idParent": undefined, "name": "test(testorg)" }, customerName: "test", salesOrg: "testorg", name: "hello", "siteNum": "100", "type": "DC" }]);

        expect(getTabsData([{ type: "DC", name: "hello", siteNum: "100", customerName: null, salesOrg: "testorg" }], { name: "name", type: "DC" }))
            .toEqual([{ "custom": { "id": undefined, "idParent": undefined, "name": "hello(testorg)" }, customerName: null, salesOrg: "testorg", name: "hello", "siteNum": "100", "type": "DC" }]);
    });

    it('geenrateHeader', () => {
        const selections = {
            locations: [{ shortName: "hello", siteNum: 100 }],
            customer: [{
                "selectionType": "CUST",
                "salesOrg": 2010,
                "distributionChannel": 80,
                "customerName": "Walmrt US"
            }]
        }
        expect(generateHeader({ selections, type: "network" })).toEqual({});
        expect(generateHeader({ selections, type: "mylocation" })).toEqual({ siteNums: [100], customerOrSalesOffice: selections.customer });
        expect(generateHeader({ selections, type: "mylocation", showTabsBy: "customer" })).toEqual({ siteNums: [100], customerOrSalesOffice: selections.customer });
        expect(generateHeader({ selections, type: "tab", showTabsBy: "customer", item: selections.customer[0] })).toEqual({ siteNums: [100], customerOrSalesOffice: selections.customer });
        expect(generateHeader({ selections, type: "tab", showTabsBy: "other", item: selections.locations[0] })).toEqual({ siteNums: [100], customerOrSalesOffice: selections.customer });
    });

    it('defaultShowTabsBy', () => {
        const state1 = {
            items: {
                items: [1, 2, 3],
                cuso: {
                    CUST: [1, 2, 3],
                    SALES_OFFICE: [1, 2, 3]
                }
            }
        }
        const state2 = {
            items: {
                items: [],
                cuso: {
                    CUST: [1, 2, 3],
                    SALES_OFFICE: [1, 2, 3]
                }
            }
        }

        const state3 = {
            items: {
                items: [1, 2, 3],
                cuso: {
                    CUST: [],
                    SALES_OFFICE: []
                }
            }
        }

        const state4 = {
            items: {
                cuso: {
                    CUST: [1, 2, 3],
                    SALES_OFFICE: [1, 2, 3]
                }
            }
        }

        const state5 = {
            items: {
                items: [1, 2, 3],
                cuso: {
                    CUST: [],
                    SALES_OFFICE: []
                }
            },
            options: {
                showTabsBy: "customer"
            }
        }

        expect(defaultShowTabsBy({})).toEqual("locations");
        expect(defaultShowTabsBy(state1)).toEqual("customer");
        expect(defaultShowTabsBy(state2)).toEqual("customer");
        expect(defaultShowTabsBy(state3)).toEqual("locations");
        expect(defaultShowTabsBy(state4)).toEqual("customer");
        expect(defaultShowTabsBy(state5)).toEqual("locations");
    });

    it('generateFilterLayoutTemplate', () => {
        uuid.mockImplementation(() => 'testid');

        expect(generateFilterLayoutTemplate({ viewName: "test" })).toEqual(
            { "columns": [], "default": false, "filters": {}, "id": "testid", "name": "test" }
        );
        expect(generateFilterLayoutTemplate({ viewName: null })).toEqual(
            { "columns": [], "default": false, "filters": {}, "id": "testid", "name": "Untitled View" }
        );
    });

    it('mergeSalesGroups', () => {
        expect(mergeSalesGroups([{ customUniqObjectId: "1", name: "Hello" }])).toEqual([{ name: "Hello" }]);
        expect(mergeSalesGroups([
            { customUniqObjectId: "1", name: "Hello", salesGroup: [1] },
            { customUniqObjectId: "1", name: "Hello", salesGroup: [2] }
        ])).toEqual([{ name: "Hello", salesGroup: [1, 2] }]);
    });


    it('getUpdatedColumns', () => {
        const case0 = { viewCols: [], defaultCols: [{ field: 'a' }] }
        const case1 = { viewCols: [{ field: 'a' }], defaultCols: [{ field: 'a' }] }
        const case2 = { viewCols: [{ field: 'a' }], defaultCols: [{ field: 'a' }, { field: 'b' }] }
        const case3 = { viewCols: [{ field: 'a' }], defaultCols: [{ field: 'b' }] }

        expect(getUpdatedColumns()).toEqual([]);
        expect(getUpdatedColumns(case0.viewCols, case0.defaultCols)).toEqual([{ field: 'a', hidden: true }]);
        expect(getUpdatedColumns(case1.viewCols, case1.defaultCols)).toEqual([{ field: 'a' }]);
        expect(getUpdatedColumns(case2.viewCols, case2.defaultCols)).toEqual([{ field: 'a' }, { field: 'b', hidden: true }]);
        expect(getUpdatedColumns(case3.viewCols, case3.defaultCols)).toEqual([{ field: 'b', hidden: true }]);

    });

    it('String to Array', () => {
        expect(stringToArray("dc")).toEqual(["dc"]);
        expect(stringToArray(null)).toEqual(null);
    });


    it("create Hyperlink Object - Orders", () => {
        const payload = {
            pageDetails: {
                "ORDER": true,
                "filterParams": {
                    "args": {
                        "orderStatusBucket": [
                            "SO_BLOCKED_NON_TMS_PLANNED_IMMEDIATE_ACTION",
                            "SO_BLOCKED_TMS_PLANNED_VISIBLE",
                            "SO_BLOCKED_TMS_PLANNED_NOT_VISIBLE",
                            "SO_BLOCKED_NON_TMS_PLANNED_PICKUP_NOT_SCHEDULED",
                            "SO_BLOCKED_NON_TMS_PLANNED_PICKUP_MULTI_BLOCK"
                        ]
                    },
                    "health": "RED",
                    "state": "ORDER"
                }
            },
            tableName: 'orders',
            filterBody: ordersfilters
        }

        expect(createHyperLinkFilterObject(payload)).toStrictEqual({
            "fullObject": {
                "orderStatusBucket": {
                    "type": "ordercheckbox",
                    "name": "Order Status",
                    "data": [
                        {
                            "name": "100% Confirmed Cube",
                            "value": "SO_COMPLETELY_CONFIRMED_CUBE",
                            "checked": false,
                            "parent": "Order Block Free"
                        },
                        {
                            "name": "Less than 100% Confirmed Cube",
                            "value": "SO_NOT_COMPLETELY_CONFIRMED_CUBE",
                            "checked": false,
                            "parent": "Order Block Free"
                        },
                        {
                            "name": "Back Orders Block Free",
                            "value": "SO_BACK_ORDER_BLOCK_FREE",
                            "checked": false,
                            "parent": "Order Block Free"
                        },
                        {
                            "name": "Visible in TMS",
                            "value": "SO_BLOCKED_TMS_PLANNED_VISIBLE",
                            "checked": true,
                            "parent": "Orders Blocked"
                        },
                        {
                            "name": "Not visible in TMS",
                            "value": "SO_BLOCKED_TMS_PLANNED_NOT_VISIBLE",
                            "checked": true,
                            "parent": "Orders Blocked"
                        },
                        {
                            "name": "Pickup not scheduled",
                            "value": "SO_BLOCKED_NON_TMS_PLANNED_PICKUP_NOT_SCHEDULED",
                            "checked": true,
                            "parent": "Orders Blocked"
                        },
                        {
                            "name": "Pickup/Package Multi Block",
                            "value": "SO_BLOCKED_NON_TMS_PLANNED_PICKUP_MULTI_BLOCK",
                            "checked": true,
                            "parent": "Orders Blocked"
                        },
                        {
                            "name": "Immediate Action",
                            "value": "SO_BLOCKED_NON_TMS_PLANNED_IMMEDIATE_ACTION",
                            "checked": true,
                            "parent": "Orders Blocked"
                        },
                        {
                            "name": "Back Orders Blocked",
                            "value": "SO_BACK_ORDER_BLOCKED",
                            "checked": false,
                            "parent": "Orders Blocked"
                        }
                    ]
                },
                "orderHealth": {
                    "type": "checkbox",
                    "name": "Health",
                    "data": [
                        {
                            "name": "Unhealthy",
                            "value": "RED",
                            "checked": true
                        },
                        {
                            "name": "Needs Attention",
                            "value": "YELLOW",
                            "checked": false
                        },
                        {
                            "name": "Healthy",
                            "value": "GREEN",
                            "checked": false
                        }
                    ]
                },
                "matAvailDate": {
                    "type": "date",
                    "name": "Material Availability Date",
                    "shortName": "Mat.Avail Date",
                    "data": null
                }
            },
            "preSetStatusFilter": {
                "orderStatusBucket": {
                    "type": "ordercheckbox",
                    "name": "Order Status",
                    "data": [
                        {
                            "name": "100% Confirmed Cube",
                            "value": "SO_COMPLETELY_CONFIRMED_CUBE",
                            "checked": false,
                            "parent": "Order Block Free"
                        },
                        {
                            "name": "Less than 100% Confirmed Cube",
                            "value": "SO_NOT_COMPLETELY_CONFIRMED_CUBE",
                            "checked": false,
                            "parent": "Order Block Free"
                        },
                        {
                            "name": "Back Orders Block Free",
                            "value": "SO_BACK_ORDER_BLOCK_FREE",
                            "checked": false,
                            "parent": "Order Block Free"
                        },
                        {
                            "name": "Visible in TMS",
                            "value": "SO_BLOCKED_TMS_PLANNED_VISIBLE",
                            "checked": true,
                            "parent": "Orders Blocked"
                        },
                        {
                            "name": "Not visible in TMS",
                            "value": "SO_BLOCKED_TMS_PLANNED_NOT_VISIBLE",
                            "checked": true,
                            "parent": "Orders Blocked"
                        },
                        {
                            "name": "Pickup not scheduled",
                            "value": "SO_BLOCKED_NON_TMS_PLANNED_PICKUP_NOT_SCHEDULED",
                            "checked": true,
                            "parent": "Orders Blocked"
                        },
                        {
                            "name": "Pickup/Package Multi Block",
                            "value": "SO_BLOCKED_NON_TMS_PLANNED_PICKUP_MULTI_BLOCK",
                            "checked": true,
                            "parent": "Orders Blocked"
                        },
                        {
                            "name": "Immediate Action",
                            "value": "SO_BLOCKED_NON_TMS_PLANNED_IMMEDIATE_ACTION",
                            "checked": true,
                            "parent": "Orders Blocked"
                        },
                        {
                            "name": "Back Orders Blocked",
                            "value": "SO_BACK_ORDER_BLOCKED",
                            "checked": false,
                            "parent": "Orders Blocked"
                        }
                    ]
                }
            }
        })

    })

    it("create Hyperlink Object - transport", () => {
        const payload = {
            pageDetails: {
                "TRANS_PLAN": true,
                "filterParams": {
                    "args": {
                        "orderExecutionBucket": [
                            "TRANS_PLAN_UNASSIGNED"
                        ]
                    },
                    "health": "RED",
                    "state": "TRANS_PLAN"
                }
            },
            tableName: 'transport',
            filterBody: transportfilters
        }

        expect(createHyperLinkFilterObject(payload)).toStrictEqual({
            "fullObject": {
                "orderExecutionBucket": {
                    "type": "checkbox",
                    "name": "Shipment Status",
                    "data": [
                        {
                            "name": "Transportation to be planned",
                            "value": "TRANS_PLAN_UNASSIGNED",
                            "checked": true
                        },
                        {
                            "name": "Transportation planning",
                            "value": "TRANS_PLAN_OPEN",
                            "checked": false
                        },
                        {
                            "name": "Transportation processing",
                            "value": "TRANS_PLAN_PROCESSING",
                            "checked": false
                        },
                        {
                            "name": "Transportation Carrier committed",
                            "value": "TRANS_PLAN_TENDER_ACCEPTED",
                            "checked": false
                        },
                        {
                            "name": "Ready for Pickup",
                            "value": "TRANS_EXEC_READY_PICK_UP",
                            "checked": false
                        },
                        {
                            "name": "In Transit",
                            "value": "TRANS_EXEC_IN_TRANSIT",
                            "checked": false
                        },
                        {
                            "name": "Delivery confirmed",
                            "value": "TRANS_EXEC_DELIVERY_CONFIRMED",
                            "checked": false
                        }
                    ]
                },
                "orderExecutionHealth": {
                    "type": "checkbox",
                    "name": "Health",
                    "data": [
                        {
                            "name": "Unhealthy",
                            "value": "RED",
                            "checked": true
                        },
                        {
                            "name": "Needs Attention",
                            "value": "YELLOW",
                            "checked": false
                        },
                        {
                            "name": "Healthy",
                            "value": "GREEN",
                            "checked": false
                        }
                    ]
                },
                "loadReadyDateTimeOriginTZ": {
                    "type": "date",
                    "name": "Carrier Ready Date",
                    "shortName": "Car.Ready Date",
                    "dummytime": true,
                    "timeformat": "T",
                    "startTime": "[00:00:00.000]",
                    "endTime": "[23:59:59.999]",
                    "data": null
                }
            },
            "preSetStatusFilter": {
                "orderExecutionBucket": {
                    "type": "checkbox",
                    "name": "Shipment Status",
                    "data": [
                        {
                            "name": "Transportation to be planned",
                            "value": "TRANS_PLAN_UNASSIGNED",
                            "checked": true
                        },
                        {
                            "name": "Transportation planning",
                            "value": "TRANS_PLAN_OPEN",
                            "checked": false
                        },
                        {
                            "name": "Transportation processing",
                            "value": "TRANS_PLAN_PROCESSING",
                            "checked": false
                        },
                        {
                            "name": "Transportation Carrier committed",
                            "value": "TRANS_PLAN_TENDER_ACCEPTED",
                            "checked": false
                        },
                        {
                            "name": "Ready for Pickup",
                            "value": "TRANS_EXEC_READY_PICK_UP",
                            "checked": false
                        },
                        {
                            "name": "In Transit",
                            "value": "TRANS_EXEC_IN_TRANSIT",
                            "checked": false
                        },
                        {
                            "name": "Delivery confirmed",
                            "value": "TRANS_EXEC_DELIVERY_CONFIRMED",
                            "checked": false
                        }
                    ]
                }
            }
        })

    })

    it("create Hyperlink Object - outbound", () => {
        const payload = {
            pageDetails: {
                "DIST": true,
                "filterParams": {
                    "args": {
                        "orderExecutionBucket": [
                            "CHECK_IN"
                        ]
                    },
                    "health": "RED",
                    "state": "DIST"
                }
            },
            tableName: 'distoutbound',
            filterBody: distoutboundfilters
        }

        expect(createHyperLinkFilterObject(payload)).toStrictEqual({
            "fullObject": {
                "orderExecutionBucket": {
                    "type": "checkbox",
                    "name": "Shipment Status",
                    "data": [
                        {
                            "name": "Shipment created",
                            "value": "SHIPMENT_CREATED",
                            "checked": false
                        },
                        {
                            "name": "Checked in by DC",
                            "value": "CHECK_IN",
                            "checked": true
                        },
                        {
                            "name": "Loading started",
                            "value": "LOADING_STARTED",
                            "checked": false
                        },
                        {
                            "name": "Ready for Pickup",
                            "value": "TRANS_EXEC_READY_PICK_UP",
                            "checked": false
                        }
                    ]
                },
                "orderExecutionHealth": {
                    "type": "checkbox",
                    "name": "Health",
                    "data": [
                        {
                            "name": "Unhealthy",
                            "value": "RED",
                            "checked": true
                        },
                        {
                            "name": "Needs Attention",
                            "value": "YELLOW",
                            "checked": false
                        },
                        {
                            "name": "Healthy",
                            "value": "GREEN",
                            "checked": false
                        }
                    ]
                }
            },
            "preSetStatusFilter": {
                "orderExecutionBucket": {
                    "type": "checkbox",
                    "name": "Shipment Status",
                    "data": [
                        {
                            "name": "Shipment created",
                            "value": "SHIPMENT_CREATED",
                            "checked": false
                        },
                        {
                            "name": "Checked in by DC",
                            "value": "CHECK_IN",
                            "checked": true
                        },
                        {
                            "name": "Loading started",
                            "value": "LOADING_STARTED",
                            "checked": false
                        },
                        {
                            "name": "Ready for Pickup",
                            "value": "TRANS_EXEC_READY_PICK_UP",
                            "checked": false
                        }
                    ]
                }
            }
        })

    })

    it("create Hyperlink Object - inbound", () => {
        const payload = {
            pageDetails: {
                "DIST": true,
                "filterParams": {
                    "args": {
                        "inboundOrderExecutionBucket": [
                            "INBOUND_IN_TRANSIT"
                        ]
                    },
                    "health": "GREEN",
                    "state": "DIST_INBOUND_SHIPMENTS"
                }
            },
            tableName: 'distinbound',
            filterBody: distinboundfilters
        }

        expect(createHyperLinkFilterObject(payload)).toStrictEqual({
            "fullObject": {
                "inboundOrderExecutionBucket": {
                    "type": "checkbox",
                    "name": "Shipment Status",
                    "data": [
                        {
                            "name": "In Transit",
                            "value": "INBOUND_IN_TRANSIT",
                            "checked": true
                        },
                        {
                            "name": "In Yard",
                            "value": "INBOUND_IN_YARD",
                            "checked": false
                        },
                        {
                            "name": "Unloading",
                            "value": "INBOUND_UNLOADING",
                            "checked": false
                        },
                        {
                            "name": "Unloading Completed",
                            "value": "INBOUND_UNLOADING_COMPLETED",
                            "checked": false
                        }
                    ]
                },
                "inboundOrderExecutionHealth": {
                    "type": "checkbox",
                    "name": "Health",
                    "data": [
                        {
                            "name": "Unhealthy",
                            "value": "RED",
                            "checked": false
                        },
                        {
                            "name": "Needs Attention",
                            "value": "YELLOW",
                            "checked": false
                        },
                        {
                            "name": "Healthy",
                            "value": "GREEN",
                            "checked": true
                        }
                    ]
                }
            },
            "preSetStatusFilter": {
                "inboundOrderExecutionBucket": {
                    "type": "checkbox",
                    "name": "Shipment Status",
                    "data": [
                        {
                            "name": "In Transit",
                            "value": "INBOUND_IN_TRANSIT",
                            "checked": true
                        },
                        {
                            "name": "In Yard",
                            "value": "INBOUND_IN_YARD",
                            "checked": false
                        },
                        {
                            "name": "Unloading",
                            "value": "INBOUND_UNLOADING",
                            "checked": false
                        },
                        {
                            "name": "Unloading Completed",
                            "value": "INBOUND_UNLOADING_COMPLETED",
                            "checked": false
                        }
                    ]
                }
            }
        })

    })

    it('searchKey', () => {
        const data = {
            shortName: "Test",
            siteNum: "100",
            alias: "test2"
        }
        expect(searchKey("test", data)).toEqual(true);
        expect(searchKey("100", data)).toEqual(true);
        expect(searchKey("test2", data)).toEqual(true);
        expect(searchKey("-", data)).toEqual(false);
        expect(searchKey(null, data)).toEqual(false);
        expect(searchKey()).toEqual(false);
    });
    it('processGuardRailData', () => {
        expect(processGuardRailData(mockGuardrailsDetails.weekdays)[0].PROFESSIONAL_countLTL).toEqual(32.001);
        expect(processGuardRailData(mockGuardrailsDetails.weekdays)[0].name).toEqual("11/21 Sun");
        expect(processGuardRailData(mockGuardrailsDetails.weekdays)[1].name).toEqual("11/22 Mon Today");
        expect(processGuardRailData(mockGuardrailsDetails.weekdays)[2].PROFESSIONAL_countLTL).toEqual(0.001);

    });
})