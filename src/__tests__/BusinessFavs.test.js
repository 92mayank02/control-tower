import BusinessFavs from '../components/header/BusinessFavs';
import { getTestElement, renderWithMockStore } from '../testUtils';


describe('<Business Favs Component>', () => {
    afterAll(() => {
        jest.restoreAllMocks();
    });

    const props = {
        businessUnits: [],
        setBusinessService: jest.fn()
    }

    it('should render with label', async () => {
        await renderWithMockStore(BusinessFavs, props);
        const component = getTestElement('businessfavs');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
    });

});