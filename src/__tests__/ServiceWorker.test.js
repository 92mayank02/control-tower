import { workbox } from 'workbox-sw';
const makeServiceWorkerEnv = require('service-worker-mock');
import { registerRoute } from 'workbox-routing';
import { StaleWhileRevalidate } from 'workbox-strategies';
import { clientsClaim } from 'workbox-core';

jest.mock("workbox-core", () => {
  return {
    clientsClaim: jest.fn(),
  }
})

jest.mock("workbox-strategies", () => {
  return {
    StaleWhileRevalidate: function({ cacheName, plugins = [] }) {

     return {
        cacheName : cacheName,
        plugins : plugins
      }
    }
  }
})

jest.mock("workbox-expiration", () => {
  return {
    ExpirationPlugin:function({ maxEntries }) {
      return { maxEntries: maxEntries }
    }
  }
})

jest.mock("workbox-precaching", () => {
  return {
    precacheAndRoute: jest.fn(),
    createHandlerBoundToURL: (url) => ({ newUrl: url })
  }
})

jest.mock("workbox-routing", () => {
  return {
    registerRoute: (bool, urlObject) => ({ bool: true, url: urlObject })
  }
})


describe('Service Worker Suite', function () {
  beforeEach(() => {
    Object.assign(global, makeServiceWorkerEnv(), workbox);
    jest.resetModules();
  });
  it('should register a service worker and cache file on install', async () => {
    require("../service-worker.js")
    // Create old cache
    await self.caches.open('OLD_CACHE');
    expect(self.snapshot().caches.OLD_CACHE).toStrictEqual({});

    // Activate and verify old cache is removed
    await self.trigger('activate');
    expect(self.snapshot().caches.OLD_CACHE).toStrictEqual({});
  });

  it('should register a service worker and cache file on install', async () => {
      require("../service-worker.js")

  });
});