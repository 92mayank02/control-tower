import useUniqtab from 'components/common/Helpers/UniqTab';
import React from "react";
import { renderWithMockStore } from '../testUtils';

describe('<useUniqtab Custom hook>', () => {

    afterAll(() => {
        jest.restoreAllMocks();
    });


    it('useUniqtab', async () => {
        const CustomComponent = () => <h1>{useUniqtab()}</h1>
        await renderWithMockStore(CustomComponent, {});
        const sessionId = sessionStorage.getItem("Tab-Id");
        expect(sessionId).toBeTruthy()
    });

    it('useUniqtab', async () => {
        window.Storage = undefined;
        const CustomComponent = () => <h1>{useUniqtab()}</h1>
        await renderWithMockStore(CustomComponent, {});
        const sessionId = sessionStorage.getItem("Tab-Id");
        expect(sessionId).toBeTruthy()
    });

});