import FilterLocations, { InsideFilter } from '../components/header/Locations';
import { getTestElement, renderWithMockStore, renderWithProvider, fireEvent, screen } from '../testUtils';
import { mockStoreValue, mockLocationsProps } from "../components/dummydata/mock"
import useMediaQuery from '@material-ui/core/useMediaQuery';

jest.mock("@material-ui/core/useMediaQuery")
describe('<Locations Component>', () => {
  afterAll(() => {
    jest.restoreAllMocks();
  })

  const props = {
    ...mockLocationsProps,
    search: { search: "", type: "MILL", businessUnit: "ALL" },
    setSearch: jest.fn(),
    handleTabHideShow:jest.fn()
  }

  const props2 = {
    ...mockLocationsProps,
    search: { search:"Jenks", type: "DC", businessUnit: "ALL" },
    setSearch: jest.fn()
  }

  const props3 = {
    ...mockLocationsProps,
    search: { search:"jenks", type: "ALL", businessUnit: "ALL" },
    setSearch: jest.fn()
  }
  
 
  it('should render with label -search ', async () => {
    await renderWithMockStore(FilterLocations, props2,  {...mockStoreValue, favorites: {...mockStoreValue.favorites,"tempBusinessUnit": ["PROFESSIONAL", "CONSUMER"]}} );
    const component = getTestElement('filterLocations');
    expect(component).toBeInTheDocument();
    expect(component).toBeTruthy();
  });

  it('should render with label - search 2', async () => {
    await renderWithMockStore(FilterLocations, props3,  {...mockStoreValue, favorites: {...mockStoreValue.favorites,"tempBusinessUnit": ["PROFESSIONAL", "CONSUMER"]}} );
    const component = getTestElement('filterLocations');
    expect(component).toBeInTheDocument();
    expect(component).toBeTruthy();
  });

  it('should render with label', async () => {
    await renderWithMockStore(FilterLocations, props, mockStoreValue );
    const component = getTestElement('filterLocations');
    expect(component).toBeInTheDocument();
    expect(component).toBeTruthy();
  });

  it('should render with (Fav Loading)', async () => {
    await renderWithMockStore(FilterLocations, props, { ...mockStoreValue, favorites: { loading: true } });
    const component = getTestElement('filterLocations');
    expect(component).toBeInTheDocument();
    expect(component).toBeTruthy();
  });


  it('Test Clear Selections', async () => {
    const { getByText } =  await renderWithMockStore(FilterLocations, props, {...mockStoreValue, favorites: {...mockStoreValue.favorites,"tempBusinessUnit": ["PROFESSIONAL", "CONSUMER"]}}) 
    expect(getByText(/clear selections/i).closest('button')).not.toBeDisabled();
    fireEvent.click(screen.queryByText(/clear selections/i));
  });


  const insideFilterProps = { onSearch: "102220" }
  it('should render with label', async () => {
    await renderWithProvider(InsideFilter, insideFilterProps);
    const component = getTestElement('insideFilter');
    expect(component).toBeInTheDocument();
    expect(component).toBeTruthy();
  });

  it('should render with label - Mock Mobile ', async () => {
    useMediaQuery.mockImplementation(()=>{
      return {
        __esModule: true,
        default: () => {
          return true;
        },
      }
    })

    const { getByTestId } =  await renderWithMockStore(FilterLocations, props, mockStoreValue );
    const component = getTestElement('filterLocations');
    expect(component).toBeInTheDocument();
    expect(component).toBeTruthy();
    fireEvent.click(getByTestId('mobilebackbutton'));
  
  });

});