import React from 'react'
import PerformanceMap from '../components/common/PerformanceMap';
import { getTestElement, fireEvent, renderWithMockStore, waitFor, userEvent, screen } from '../testUtils';
import { performanceView, mockStoreValue } from '../components/dummydata/mock';

jest.mock("../reduxLib/services/getLocationsService", () => {
  return {
    getPerformanceMapService: () => (
      {
        type: "PERFORMANCE_FETCH_SUCCESS",
        payload: {}
      }
    )
  }
})

jest.mock("../theme/layouts/GeoChart", () => {
  return {
    __esModule: true,
    default: () => {
      return <div data-testid="geochartmock" ></div>;
    },
  };
});

jest.mock("helpers", () => {
  const getStatusData = () => { };
  return {
    getStatusData,
    searchKey: jest.fn()
  }
})

describe('<PerformanceMap>', () => {

  const props = {
    setDetails: jest.fn(),
    type: "network",
    siteNums: [2028, 2822]
  }

  it('should render with values', async () => {
    await renderWithMockStore(PerformanceMap, props, { ...mockStoreValue, sites: { ...mockStoreValue.sites, performance: performanceView } });
    const component = getTestElement('performanceview');
    expect(component).toBeInTheDocument();
  });

  it('should render no data', async () => {
    await renderWithMockStore(PerformanceMap, props, { ...mockStoreValue, sites: { ...mockStoreValue.sites, performance: [] } });
    const component = getTestElement('performanceview');
    expect(component).toBeInTheDocument();
  });

  it('should render Non array response', async () => {
    await renderWithMockStore(PerformanceMap, props, { ...mockStoreValue, sites: { ...mockStoreValue.sites, performance: {} } });
    const component = getTestElement('performanceview');
    expect(component).toBeInTheDocument();
  });


  it('show Check toggle view', async () => {
    const { getByTestId } = await renderWithMockStore(PerformanceMap,
      props, { ...mockStoreValue, sites: { ...mockStoreValue.sites, performance: performanceView } });
    fireEvent.click(getByTestId("tableicon"));
    waitFor(() => getByTestId('performancetable'))
    expect(getTestElement('performancetable')).toBeInTheDocument();

    fireEvent.click(getByTestId("mapicon"));
    waitFor(() => getByTestId('geochartmock'))
    expect(getTestElement('geochartmock')).toBeInTheDocument();
  });

  it('Trigger refresh without error', async () => {
    const { getByTestId } = await renderWithMockStore(PerformanceMap,
      props, { ...mockStoreValue, sites: { ...mockStoreValue.sites, performance: performanceView } });
    expect(getTestElement('geochartmock')).toBeInTheDocument();
    fireEvent.click(getByTestId("refreshicon"));
    expect(getTestElement('geochartmock')).toBeInTheDocument();

  });

  it('Map Search Text Change Event', async () => {
    const { getByTestId } = await renderWithMockStore(PerformanceMap, props, { ...mockStoreValue, sites: { ...mockStoreValue.sites, performance: performanceView } });
    const component = getTestElement('performanceview');
    expect(component).toBeInTheDocument();
    fireEvent.change(getByTestId("searchbox"), { target: { value: '2028' } })
  });

  it('Map Search Text Change Event', async () => {
    const { getByTestId } = await renderWithMockStore(PerformanceMap, props, { ...mockStoreValue, sites: { ...mockStoreValue.sites, performance: performanceView } });
    const component = getTestElement('performanceview');
    expect(component).toBeInTheDocument();
    fireEvent.change(getByTestId("searchbox"), { target: { value: '2028' } })
    fireEvent.keyPress(getByTestId("searchbox"), { key: "Enter", code: 13, charCode: 13 });
    fireEvent.submit(getByTestId("searchbox"));

  });

  it('Empty String Event Test', async () => {
    const { getByTestId } = await renderWithMockStore(PerformanceMap, {
      ...props,
      siteNums: [2028, 2822],
      type: "mylocation"
    }, { ...mockStoreValue, sites: { ...mockStoreValue.sites, performance: performanceView } });
    const component = getTestElement('performanceview');
    expect(component).toBeInTheDocument();
    fireEvent.change(getByTestId("searchbox"), { target: { value: "" } })
    fireEvent.submit(getByTestId("searchbox"));
  });

});