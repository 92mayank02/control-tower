import TabsMenu from 'components/header/TabsMenu';
import { renderWithProvider } from '../testUtils';
import { fireEvent } from '@testing-library/react';

describe('<TabsMenu>', () => {
    afterAll(() => {
        jest.restoreAllMocks();
    });

    const tabsprops = {
        setSecondaryTab: jest.fn(),
        secondaryTab: 1,
        primaryTab: 0,
        setPrimaryTab: jest.fn(),
        handleTabHideShow:jest.fn()
    };

    it('List Item Click', async () => {
        await renderWithProvider(TabsMenu, tabsprops);
        const { container } = await renderWithProvider(TabsMenu, tabsprops);
        fireEvent.click(container.querySelector("[name='listitem']"))
        expect(container).toBeInTheDocument();
    });

    it('Favs Click', async () => {
        const wrapper = await renderWithProvider(TabsMenu, { ...tabsprops, primaryTab: 1 });
        const click = await wrapper.findByTitle("Favourites");
        const test = await click.dispatchEvent(new MouseEvent('click', { bubbles: true }));
        expect(test).toBeTruthy();
    });

    it('Switch to Menu', async () => {
        const wrapper = await renderWithProvider(TabsMenu, tabsprops);
        const click = await wrapper.findByTitle("Menu");
        const test = await click.dispatchEvent(new MouseEvent('click', { bubbles: true }));
        expect(test).toBeTruthy();
    });

});