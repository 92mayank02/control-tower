import TextFilterElement from '../components/common/TextFilterElement';
import { getTestElement, renderWithMockStore, fireEvent, userEvent, screen } from '../testUtils';
import { mockStoreValue } from "../components/dummydata/mock"


describe('<Text Filter Element>', () => {
    afterAll(() => {
      jest.restoreAllMocks();
    });
  
    let props = { 
      title:'Test Placeholer',
      resetOthers:'false',
      placeholder:"Enter placholder"
    }
    it('should render with label', async () => {
      await renderWithMockStore(TextFilterElement,props);
      const component = getTestElement('textFilterElement');
      expect(component).toBeInTheDocument();
    });

    it('should render with label- Search Input and Submit', async () => {
      const {getByTestId } = await renderWithMockStore(TextFilterElement, props, mockStoreValue );
      const element = screen.getByRole('textbox');
      fireEvent.change(element, {target: {value: '1234'}})
      userEvent.type(element, '{enter}')
    });

    it('On Change Event', async () => {
      props = {
        ...props,
        filter: {
          stringToArray: true
        }
      }
      const wrapper = await renderWithMockStore(TextFilterElement, props, mockStoreValue);
      const { getAllByTestId, debug } = wrapper;
      fireEvent.change(getAllByTestId("textfilter")[0], { target: { value: "abc" } });
      fireEvent.keyPress(getAllByTestId("textfilter")[0], { key: "Enter", code: 13, charCode: 13 }); 
      fireEvent.submit(getAllByTestId("textfilter")[0]);
      expect(true).toBe(true);
    });

    it('On Change Event with String to array false', async () => {
      props = {
        ...props,
        filter: {},
        placeholder: null
      }
      const wrapper = await renderWithMockStore(TextFilterElement, props, mockStoreValue);
      const { getAllByTestId, debug } = wrapper;
      fireEvent.change(getAllByTestId("textfilter")[0], { target: { value: null } });
      fireEvent.keyPress(getAllByTestId("textfilter")[0], { key: "Enter", code: 13, charCode: 13 }); 
      fireEvent.submit(getAllByTestId("textfilter")[0]);
      expect(true).toBe(true);
    });
});