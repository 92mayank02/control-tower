import downloadReducer from "../reduxLib/reducers/downloadReducer";
import { downloadConstants } from '../reduxLib/constants';



describe("Test Option Reducer Functions", () => {

    afterEach(() => {
        jest.clearAllMocks();
    });

    const initialState = {
    }

    it("Default State Reducer", async () => {
        expect(downloadReducer(undefined, { type: "UndefinedAction", payload: true })).toStrictEqual(initialState);
    });

    it("Default Switch Return", async () => {
        expect(downloadReducer(undefined, { type: "Action Paload Without Variables", payload: true, variables: true })).toStrictEqual(initialState);
    });

    const optionalData = {
        columns: [],
        process: jest.fn(),
        detailsProcess: jest.fn(),
        detailsColumns: []
    }

    it("Download Loading Reducer", async () => {
        expect(downloadReducer(undefined, {
            type: downloadConstants.LOADING,
            payload: false,
            page: "network",
            subtype: "network",
            ...optionalData
        })).toStrictEqual({
            ...initialState, network: {
                network: {
                    columns: [],
                    downloading: false,
                    process: optionalData.process,
                    status: null,
                    detailsProcess: optionalData.detailsProcess,
                    detailsColumns: [],
                }
            }
        });
    });

    it("Download Success Reducer", async () => {
        expect(downloadReducer(undefined, {
            type: downloadConstants.DOWNLOAD_SUCCESS,
            payload: [],
            page: "network",
            subtype: "network",
            ...optionalData
        })).toStrictEqual({
            ...initialState, network: {
                network: {
                    data: [],
                    downloading: false,
                    status: "Success",
                    itemInfoRequired: false
                }
            }
        });
    });

    it("Download Failure Reducer", async () => {
        expect(downloadReducer(undefined, {
            type: downloadConstants.DOWNLOAD_FAILURE,
            payload: [],
            page: "network",
            subtype: "network",
            ...optionalData
        })).toStrictEqual({
            ...initialState, network: {
                network: {
                    data: null,
                    downloading: false,
                    status: "Failed",
                    itemInfoRequired: false
                }
            }
        });
    });

    it("Download Reset Reducer", async () => {
        expect(downloadReducer(undefined, {
            type: downloadConstants.DOWNLOAD_RESET,
            payload: [],
            page: "network",
            subtype: "network",
            ...optionalData
        })).toStrictEqual({
            ...initialState, network: {
                network: {}
            }
        });
    });

})