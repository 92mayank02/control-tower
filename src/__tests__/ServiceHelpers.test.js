import NetworkView from 'components/pages/NetworkView';
import { getTestElement, initiateGlobalAuthService, renderWithMockStore } from '../testUtils';
import { get, uniq } from "lodash";

describe('<NetworkView Component>', () => {

    afterAll(() => {
        jest.restoreAllMocks();
    });

    beforeAll(() => {    });
    const props = {
        setDetails: jest.fn(),
        pageDetails: {
            filterParams: {
                state: null
            }
        },
        setRefresh: jest.fn()
    }

    it('NetworkView should render', async () => {
        global.authService = {
            
            redirect:jest.fn(),
            isAuthenticated : false,
            token:"",
            getIdToken: () => {
              return token
            },
            getAuthState: () => {
              return {
                isAuthenticated
              }
            },
            getTokenManager : () => {
                return {
                    renew : (type) => {
                        if(type==="idToken"){
                            token = "AAA";
                            isAuthenticated = true
                        }
                    }
                }
            }
        };
        await renderWithMockStore(NetworkView, props);
        const component = getTestElement('networkview');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
    });

    it('NetworkView should render', async () => {
        global.authService = {
            
            redirect:jest.fn(),
            isAuthenticated : false,
            token:"",
            getIdToken: () => {
              return undefined
            },
            getAuthState: () => {
              return {
                isAuthenticated
              }
            },
            getTokenManager : () => {
                return {
                    renew : (type) => {
                        if(type==="idToken"){
                            token = "AAA";
                            isAuthenticated = true
                        }
                    }
                }
            }
        };
    
        await renderWithMockStore(NetworkView, props);
        const component = getTestElement('networkview');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
    });

});