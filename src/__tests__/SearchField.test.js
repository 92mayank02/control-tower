import SearchField from '../components/common/Filters/SearchFilter';
import { getTestElement, renderWithMockStore, fireEvent, userEvent, screen } from '../testUtils';
import { mockStoreValue } from "../components/dummydata/mock"

describe('<SearchField>', () => {
    afterAll(() => {
        jest.restoreAllMocks();
    });

    const props = {
        saveFilters: jest.fn(),
        subtype: "network",
        filterKey: "searchStringList",
        filters: {
            searchStringList: {
                data: null,
                name: "Search",
                type: "text"
            }
        },
        type: "network"
    };

    it('SearchField Should Render', async () => {
        await renderWithMockStore(SearchField, props);
        const component = getTestElement('searchfield');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
    });

    it('should render with label- Search Input and Submit', async () => {
        const {getByLabelText } = await renderWithMockStore(SearchField, props, mockStoreValue );
        const component = getTestElement('searchfield');
        expect(component).toBeInTheDocument();
        fireEvent.change(getByLabelText("search"), {target: {value: '2626335'}})
        userEvent.type(screen.getByLabelText("search"), '{enter}')
      });

});