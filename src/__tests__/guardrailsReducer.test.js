import guardrailsReducer from "../reduxLib/reducers/guardrailsReducer";
import { guardrailsConstants } from '../reduxLib/constants';

describe("Guardrails Reducer Functions", () => {

    afterEach(() => {
        jest.clearAllMocks();
    });

    const defaultState = { getGaurdrailsLoading: false, updateGaurdrailsLoading: false, allSiteGuardrails: {data:[], allowedToEdit:false},  notification:{}, disableButton: true}
    const mockdata = [
        {
          "siteNum": "2023",
          "unit": "LOADS", // Possible values "Loads" or "Hours"
          "weekdays": [
            {
              "day": "Sunday",
              "orderBusinessUnit": [
                {
                  "businessUnit": "CONSUMER",
                  "guardrailFL": 2023,
                  "guardrailLTL": 18
                },
                {
                  "businessUnit": "PROFESSIONAL",
                  "guardrailFL": 36,
                  "guardrailLTL": 36
                },
                {
                  "businessUnit": "ALL",
                  "guardrailFL": 36,
                  "guardrailLTL": 36
                }
              ]
            },
            {
              "day": "Monday",
              "orderBusinessUnit": [
                {
                  "businessUnit": "CONSUMER",
                  "guardrailFL": 19,
                  "guardrailLTL": 19
                },
                {
                  "businessUnit": "PROFESSIONAL",
                  "guardrailFL": 33,
                  "guardrailLTL": 23
                },
                {
                  "businessUnit": "ALL",
                  "guardrailFL": 23,
                  "guardrailLTL": 23
                }
              ]
            }
          ]
        },
        {
            "siteNum": "2027",
            "unit": "LOADS", // Possible values "Loads" or "Hours"
            "weekdays": [
              {
                "day": "Sunday",
                "orderBusinessUnit": [
                  {
                    "businessUnit": "CONSUMER",
                    "guardrailFL": 2027,
                    "guardrailLTL": 12
                  },
                  {
                    "businessUnit": "PROFESSIONAL",
                    "guardrailFL": 45,
                    "guardrailLTL": 45
                  },
                  {
                    "businessUnit": "ALL",
                    "guardrailFL": 67,
                    "guardrailLTL": 67
                  }
                ]
              },
              {
                "day": "Monday",
                "orderBusinessUnit": [
                  {
                    "businessUnit": "CONSUMER",
                    "guardrailFL": 13,
                    "guardrailLTL": 13
                  },
                  {
                    "businessUnit": "PROFESSIONAL",
                    "guardrailFL": 88,
                    "guardrailLTL": 88
                  },
                  {
                    "businessUnit": "ALL",
                    "guardrailFL": 99,
                    "guardrailLTL": 99
                  }
                ]
              }
            ]
          },
          {
            "siteNum": "2022",
            "unit": "LOADS", // Possible values "Loads" or "Hours"
            "weekdays": [
              {
                "day": "Sunday",
                "orderBusinessUnit": [
                  {
                    "businessUnit": "CONSUMER",
                    "guardrailFL": 2022,
                    "guardrailLTL": 56
                  },
                  {
                    "businessUnit": "PROFESSIONAL",
                    "guardrailFL": 66,
                    "guardrailLTL": 66
                  },
                  {
                    "businessUnit": "ALL",
                    "guardrailFL": 66,
                    "guardrailLTL": 66
                  }
                ]
              },
              {
                "day": "Monday",
                "orderBusinessUnit": [
                  {
                    "businessUnit": "CONSUMER",
                    "guardrailFL": 88,
                    "guardrailLTL": 88
                  },
                  {
                    "businessUnit": "PROFESSIONAL",
                    "guardrailFL": 88,
                    "guardrailLTL": 88
                  },
                  {
                    "businessUnit": "ALL",
                    "guardrailFL": 99,
                    "guardrailLTL": 99
                  }
                ]
              }
            ]
          }
      ]

    const updateGr = {
      "siteNum": "2030",
      "unit": "LOADS", // Possible values "Loads" or "Hours"
      "weekdays": [
        {
          "day": "Sunday",
          "orderBusinessUnit": [
            {
              "businessUnit": "CONSUMER",
              "guardrailFL": 2023,
              "guardrailLTL": 18
            },
            {
              "businessUnit": "PROFESSIONAL",
              "guardrailFL": 36,
              "guardrailLTL": 36
            },
            {
              "businessUnit": "ALL",
              "guardrailFL": 36,
              "guardrailLTL": 36
            }
          ]
        }]}
      
    it("Default Switch Return", async () => {
        expect(guardrailsReducer(undefined, { type: "Action not listed", payload: mockdata })).toStrictEqual({
            ...defaultState
        });
    });
    it("guardRails Fetch Success Reducer", async () => {
        expect(guardrailsReducer(undefined, {
            type: guardrailsConstants.GUARDRAILS_FETCH_SUCCESS,
            payload: {data:mockdata, allowedToEdit:true},
        })).toStrictEqual({
            ...defaultState, allSiteGuardrails: {data:mockdata, allowedToEdit:true}, notification:{}, disableButton: false
        });
    });
    it("GuardRails Loading Reducer", async () => {
        expect(guardrailsReducer(undefined, {
            type: guardrailsConstants.GUARDRAILS_FETCH_LOADING,
            payload: true,
        })).toStrictEqual({
            ...defaultState, getGaurdrailsLoading: true, notification:{}
        });
    });
    it("GuardRails Failed Reducer", async () => {
        expect(guardrailsReducer(undefined, {
            type: guardrailsConstants.GUARDRAILS_FETCH_FAILED,
            payload: [],
        })).toStrictEqual({
            ...defaultState, allSiteGuardrails: {data:[], allowedToEdit:false} 
        });
    });

    
    it("guardRails update Success Reducer", async () => {
        expect(guardrailsReducer({
          ...defaultState, notification:{message:"Changes updated successfully", severity:"info"}, allSiteGuardrails: {allowedToEdit: false, data:mockdata}}, {
            type: guardrailsConstants.GUARDRAILS_UPDATE_SUCCESS,
            payload: updateGr,
        })).toStrictEqual({
            ...defaultState, notification:{message:"Changes updated successfully", severity:"info"}, allSiteGuardrails: {allowedToEdit: false, data:[...mockdata, updateGr]}
        });
    });
    it("GuardRails update Loading Reducer", async () => {
        expect(guardrailsReducer(undefined, {
            type: guardrailsConstants.GUARDRAILS_UPDATE_LOADING,
            payload: true,
        })).toStrictEqual({
            ...defaultState, updateGaurdrailsLoading: true, notification:{}
        });
    });
    it("GuardRails update Failed Reducer", async () => {
        expect(guardrailsReducer(undefined, {
            type: guardrailsConstants.GUARDRAILS_UPDATE_FAILED,
            payload:mockdata,
        })).toStrictEqual({
            ...defaultState, notification:{message:" You are not authorized to make changes", severity:"error"}
        });
    });





})