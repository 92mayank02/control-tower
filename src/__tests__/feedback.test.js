import { mockStoreValue } from 'components/dummydata/mock';
import FeedBack from '../components/common/FeedBack';
import { getTestElement, renderWithMockStore, screen, fireEvent, userEvent } from '../testUtils';

describe('<CT Link>', () => {
  afterAll(() => {
    jest.restoreAllMocks();
  });

  const props = {
    openFeedback: true,
    openAction: jest.fn(),
    resetFeedback: jest.fn()
  };

  const mockStore = {...mockStoreValue,feedbackform: { feedback_status: "success", feedback_submit_loading: false } };

  it('should render with label', async () => {
    await renderWithMockStore(FeedBack, props, mockStore);
    const breadcrumbComponent = getTestElement('feedback');
    expect(breadcrumbComponent).toBeInTheDocument();
  });

  it('Click Happy', async () => {
    await renderWithMockStore(FeedBack, props, mockStore);
    const elment = getTestElement('feedback');
    const happy =  screen.getByTestId("happyicon")
    fireEvent.click(happy);
    expect(elment).toBeTruthy();
  });

  it('Click UnHappy', async () => {
    await renderWithMockStore(FeedBack, props, mockStore);
    const elment = getTestElement('feedback');
    const unhappy =  screen.getByTestId("unhappyicon")
    fireEvent.click(unhappy);
    expect(elment).toBeTruthy();
  });


  it('Click Close', async () => {
    await renderWithMockStore(FeedBack, props, mockStore);
    const elment = getTestElement('feedback');
    const closebutton =  screen.getByTestId("closeicon")
    fireEvent.click(closebutton);
    expect(elment).toBeTruthy();
  });

  it('On Change', async () => {
    await renderWithMockStore(FeedBack, props, mockStore);
    let ele = screen.getByRole('textbox');

    const happy =  screen.getByTestId("happyicon")
    fireEvent.click(happy);

    fireEvent.change(ele, { target: { value: "HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH" } })
    const submit = screen.getByTestId("submitfeedback")
    fireEvent.click(submit);
  });

  it('Submit with Test Only / No Happy Selection', async () => {
    const wrapper = await renderWithMockStore(FeedBack, props, mockStore);
    let ele = await wrapper.container.querySelector('[id="feedbackinput"]');

    fireEvent.change(ele, { target: { value: "HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH" } })

    const submit = await document.querySelector("[id='button']")
    const test = await submit.dispatchEvent(new MouseEvent('click', { bubbles: true }));

    expect(test).toBeTruthy();

  });

});