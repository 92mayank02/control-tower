import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { updateCSTRank } from "../reduxLib/services/analyticsService"
const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)
import fetch from "../reduxLib/services/serviceHelpers";

jest.mock("../reduxLib/services/serviceHelpers")

describe('<AnalyticsService>', () => {
  let store;

  beforeEach(() => {
    store = mockStore({});
  });

  const data = {
    // userName: "abc@kcc.com",
    lane: "Lane 123",
    // CSTRankingList: ["HJBI", "HJBN"],
    // CTRankingList: ["HJBN", "HJBI"]
  }

  it('Update ranking Services Data Failed', () => {
    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return reject({
          status: 500,
          json: () => {
            return null
          }
        })
      })
    })
  
    store.dispatch(updateCSTRank(data))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })

  it('Update ranking Services Success', () => {
    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return resolve({
          status: 200,
          json: () => {
            return {}
          }
        })
      })
    })
  
    store.dispatch(updateCSTRank(data))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })

  it('Update ranking Services - Server Failed', () => {
    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return resolve({
          status: 400,
          json: () => {
            return {}
          }
        })
      })
    })
  
    store.dispatch(updateCSTRank(data))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })

})