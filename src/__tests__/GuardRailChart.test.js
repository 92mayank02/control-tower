import GuardRailBarChart from '../components/D3Charts/GuardrailChart';
import { getTestElement, renderWithProvider } from '../testUtils';


describe('<GuardRaliBarChart>', () => {
    afterAll(() => {
      jest.restoreAllMocks();
    });
  
    const BarChartProps = {
        margins: {
          top: 10,
          right: 10,
          bottom: 40,
          left: 100,
          margin: 10
        },
        height: 160,
        colors: [
          "red",
          "green"
        ],
        dimensions: {
          width: 284,
          height: 224
        },
        parentWidth: 368.22222900390625,
        setDetails:jest.fn(),
        wrapit:true,
        horizontal:true,
        allkeys: [],
        allkeys:["blueCount", "yellowCount", "redCount"],
        keys:["blueCount", "yellowCount", "redCount"],
        xKey:"statedesc",
        data:[
            {
              "state": "TRANS_EXEC_READY_PICK_UP",
              "stateDesc": "Ready for pickup",
              "totalCount": 0,
              "redCount": 0,
              "blueCount": 0,
              "total": 0
            },
            {
              "state": "TRANS_EXEC_IN_TRANSIT",
              "stateDesc": "In Transit",
              "totalCount": 0,
              "redCount": 0,
              "blueCount": 0,
              "total": 0
            },
            {
              "state": "TRANS_EXEC_DELIVERY_CONFIRMED",
              "stateDesc": "Delivery Confirmed",
              "totalCount": 0,
              "redCount": 0,
              "blueCount": 0,
              "total": 0
            }
        ],
        total:0,
        type:"network",
        subtype:"TRANS_PLAN"
      }
    it('should render with label', async () => {
      await renderWithProvider(GuardRailBarChart,BarChartProps);
      const component = getTestElement('gbarChart');
      expect(component).toBeInTheDocument();
      expect(component).toBeTruthy();
    });

});