import authReducer  from "../reduxLib/reducers/authReducer";
import { authConstants } from '../reduxLib/constants';

describe("Auth Reducer Functions",() => {

    afterEach(() => {
        jest.clearAllMocks();
    });

    const defaultState = { auth: { user: {name: "john doe", email: "john@doe.com"} } }

    const payload = {userViews:{ tableName: "networkTable", views : [{filters:[], columns:[], sort:{}}]}}

    it("Default State Reducer", async () => {
        expect(authReducer(defaultState,{type:"Action not in the reducer switch", payload})).toStrictEqual(defaultState);        
    });

    it("Undefined State Reducer", async () => {
        expect(authReducer(undefined,{type:"Action not in the reducer switch", payload}).auth.user).not.toBeTruthy();        
    });

    it("Undefined Action Type Reducer", async () => {
        expect(authReducer(defaultState,{payload}).auth.user.name).toBe(defaultState.auth.user.name);        
    });

    it("Remove User Reducer", async () => {
        expect(authReducer(defaultState,{
            type: authConstants.REMOVE_USER_DETAILS,
            payload
        })).toStrictEqual({         
            ...defaultState, user: payload
        });  
    });

})