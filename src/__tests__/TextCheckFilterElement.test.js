import TextCheckFilterElement from '../components/common/TextCheckFilterElement';
import { getTestElement, renderWithMockStore, shallowSetup } from '../testUtils';
import { mount, spyOn } from 'enzyme';
import { act, fireEvent, render, screen, waitFor } from '@testing-library/react';
import KCCheckBox from "components/common/Elements/KCCheckbox";
import { mockStoreValue } from "../components/dummydata/mock"



describe('<Text Filter Element>', () => {
    afterAll(() => {
        jest.restoreAllMocks();
    });

    let props = {
        title: 'Test Placeholer',
        resetOthers: 'false',
        saveFilters: jest.fn()
    }
    it('should render with label', async () => {
        await renderWithMockStore(TextCheckFilterElement, props);
        const component = getTestElement('textCheckFilterElement');
        expect(component).toBeInTheDocument();
    });

    it('On Change', async () => {
        const wrapper = await renderWithMockStore(TextCheckFilterElement, props);
        let ele = await wrapper.container.querySelector('input');
        fireEvent.change(ele, { target: { value: "Hello" } })
        expect(ele.value).toBe("Hello");
    });

    it('On Change Checkbox', async () => {
        const wrapper = await renderWithMockStore(TextCheckFilterElement, props);
        let ele = await wrapper.container.querySelector('input[type="checkbox"][name="checkboxtest"]');
        fireEvent.click(ele, { target: { checked: true } });
        expect(ele.checked).toBe(false);
    });

    it('On Change Checkbox', async () => {
        const { getAllByTestId } = await renderWithMockStore(TextCheckFilterElement, props);
        fireEvent.change(getAllByTestId("textcheckfilter")[0], { target: { value: "abc" } });
        fireEvent.keyPress(getAllByTestId("textcheckfilter")[0], { key: "Enter", code: 13, charCode: 13 }); 
        fireEvent.submit(getAllByTestId("textcheckfilter")[0]);
        expect(true).toBe(true);
    });


    it('On Change Event with filter params', async () => {
        props = {
          ...props,
          filter: {
            stringToArray: true
          }
        }
        const wrapper = await renderWithMockStore(TextCheckFilterElement, props, mockStoreValue);
        const { getAllByTestId, debug } = wrapper;
        fireEvent.change(getAllByTestId("textcheckfilter")[0], { target: { value: "abc" } });
        fireEvent.keyPress(getAllByTestId("textcheckfilter")[0], { key: "Enter", code: 13, charCode: 13 }); 
        fireEvent.submit(getAllByTestId("textcheckfilter")[0]);
        expect(true).toBe(true);
      });
  
      it('On Change Event with String to array false', async () => {
        props = {
          ...props,
          filter: {},
          placeholder: null
        }
        const wrapper = await renderWithMockStore(TextCheckFilterElement, props, mockStoreValue);
        const { getAllByTestId, debug } = wrapper;
        fireEvent.change(getAllByTestId("textcheckfilter")[0], { target: { value: null } });
        fireEvent.keyPress(getAllByTestId("textcheckfilter")[0], { key: "Enter", code: 13, charCode: 13 }); 
        fireEvent.submit(getAllByTestId("textcheckfilter")[0]);
        expect(true).toBe(true);
      });

});  