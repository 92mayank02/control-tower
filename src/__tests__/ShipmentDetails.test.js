import React from 'react'
import ShipmentDetails from '../components/common/ShipmentDetails';
import { getTestElement, renderWithMockStore, screen, fireEvent } from '../testUtils';
import { mockStoreValue, mockShipmentDetailsStopsDetails } from "../components/dummydata/mock"
import { waitFor } from '@testing-library/react';

jest.mock("../theme/layouts/GeoMap", () => {
    return {
      __esModule: true,
      default: () => {
        return <div data-testid="geochartmock" ></div>;
      },
    };
  });
describe('<Shipment Details>', () => {
    afterAll(() => {
        jest.restoreAllMocks();
    });

    const props = { setDetails: jest.fn(), type: "network", pageDetails:{filterParams:{shipmentNum:"1234567"}} }

    it('should render with label', async () => {

        const stops = mockShipmentDetailsStopsDetails.stops
        const shipmentDetails = mockShipmentDetailsStopsDetails;

        await renderWithMockStore(ShipmentDetails, props, { ...mockStoreValue, shipment: { details: shipmentDetails, stops, locationHistory: [] } });
        const component = getTestElement('shipmentdetails');
        expect(component).toBeInTheDocument();
    });

    it('should render tracking details and stop details', async () => {

        const stops = mockShipmentDetailsStopsDetails.stops;
        const locationHistory = mockShipmentDetailsStopsDetails.locationHistory;
        const shipmentDetails = mockShipmentDetailsStopsDetails;

        const { getByText } = await renderWithMockStore(ShipmentDetails, props, { ...mockStoreValue, shipment: { details: shipmentDetails, stops, locationHistory } });
        fireEvent.click(getByText("Tracking Details"))
        const component = getTestElement('trackingdetails');
        expect(component).toBeInTheDocument();

        fireEvent.click(getByText("Stops"))
        // waitFor(() => getByText("S"))
        expect(getByText("STOPS")).toBeInTheDocument();
    });

    it('should render no data in tracking details', async () => {
        const { getByText } = await renderWithMockStore(ShipmentDetails, props, { ...mockStoreValue, shipment: { details: [], stops: [], locationHistory: [] } });
        fireEvent.click(getByText("Tracking Details"))
        waitFor(() => getByText("No Data Found"))
        expect(getByText("No Data Found")).toBeInTheDocument();
    });
});