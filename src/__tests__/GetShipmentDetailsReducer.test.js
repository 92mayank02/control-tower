import getShipmentDetailsReducer from "../reduxLib/reducers/getShipmentDetailsReducer";
import { detailsConstants } from '../reduxLib/constants';
import { mockStoreValue, mockShipmentDetailsStopsDetails } from "../components/dummydata/mock"


describe("Get Shipment Details Reducer Functions",() => {

    afterEach(() => {
        jest.clearAllMocks();
    });

    const defaultState = {loading: false, details: {}, stops: [], locationHistory:[]}
    const variables = {"body":{    
        region : "NA",               
        shipmentNum: "1234",
        searchType: "STOP_DETAILS",  
    }};

    const successPayload = mockShipmentDetailsStopsDetails

    it("Default State Reducer", async () => {
        expect(getShipmentDetailsReducer(undefined,{type:"Action Paload Without Variables", payload: true})).toStrictEqual(defaultState);        
    });

    it("Default Switch Return", async () => {
        expect(getShipmentDetailsReducer(undefined,{type:"Action Paload Without Variables", payload: true })).toStrictEqual(defaultState);        
    });

    it("Ship Details Success Reducer", async () => {
        expect(getShipmentDetailsReducer(undefined,{
            type: detailsConstants.SHIPMENT_STOPS_DATA_FETCH_SUCCESS,
            payload: successPayload,
        })).toStrictEqual({ 
            ...defaultState, details: successPayload, stops:successPayload.stops, locationHistory:[]
        });        
    });

    it("Ship Details Loading Reducer", async () => {
        expect(getShipmentDetailsReducer(undefined,{
            type: detailsConstants.SHIPMENT_LOADING,
            payload: false,
        })).toStrictEqual({         
            ...defaultState, loading:false
        });      
    });

    it("Ship Details Failed Reducer", async () => {
        expect(getShipmentDetailsReducer(undefined,{
            type: detailsConstants.SHIPMENT_STOPS_DATA_FETCH_FAILED,
            payload: {},
        })).toStrictEqual({         
            ...defaultState,  details: {}, stops:[], locationHistory:[], error: {}
        });  
    });

    it("Shipment Tracking Details Success Reducer", async () => {
        const locationHistory = successPayload.locationHistory.map(carrierPing => ({ ...carrierPing, latitude: carrierPing.geoLocation.latitude, longitude: carrierPing.geoLocation.longitude }))

        expect(getShipmentDetailsReducer(undefined,{
            type: detailsConstants.SHIPMENT_TRACKING_DATA_FETCH_SUCCESS,
            payload: successPayload,
        })).toStrictEqual({ 
            ...defaultState, locationHistory
        });        
    });

    it("Shipment Tracking Details Failed Reducer", async () => {
        expect(getShipmentDetailsReducer(undefined,{
            type: detailsConstants.SHIPMENT_TRACKING_DATA_FETCH_FAILED,
            payload: {},
        })).toStrictEqual({         
            ...defaultState, locationHistory:[], error: {}
        });  
    });

})