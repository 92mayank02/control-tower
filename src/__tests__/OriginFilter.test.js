import OriginFilter, { OriginFilterElement } from '../components/common/Filters/OriginFilter';
import { getTestElement, renderWithMockStore } from '../testUtils';


describe('<Origin Filter Element>', () => {
    afterAll(() => {
      jest.restoreAllMocks();
    });
  
    const props = {
      locations:[
        {
          "siteNum": "2496",
          "region": "NA",
          "type": "DC",
          "plantType": "RDC",
          "alias": "SDDC",
          "shortName": "South Dallas DC",
          "siteName": "S_2496_752",
          "siteNameDesc": "EXT OPS/DC - SOUTH DALLAS DC -",
          "location": {
            "id": "2496",
            "name": "South Dallas DC",
            "city": "DALLAS",
            "state": "TX",
            "country": "USA",
            "street": "4808 MOUNTAIN CREEK PARKWAY",
            "geoLocation": {
              "longitude": -96.9650634,
              "latitude": 32.6761483
            },
            "timezone": "US/Central",
            "postalCode": "75236-4602"
          }
        },
        {
          "siteNum": "2299",
          "region": "NA",
          "type": "DC",
          "plantType": "RDC",
          "alias": "SRDC",
          "shortName": "Southern Regional DC",
          "siteName": "S_2299_302",
          "siteNameDesc": "South Regional DC -ExtOps/DC",
          "location": {
            "id": "2299",
            "name": "Southern Regional DC",
            "city": "MCDONOUGH",
            "state": "GA",
            "country": "USA",
            "street": "340 WESTRIDGE PARKWAY",
            "geoLocation": {
              "longitude": -84.1929612,
              "latitude": 33.4042901
            },
            "timezone": "US/Eastern",
            "postalCode": "30253-3006"
          }
        }
      ],
      addOrigin:jest.fn(), 
      defaultValues: ["2299"],
      type:'network'
    }

    const propsWithoutAlias = {
      locations:[
        {
          "siteNum": "2496",
          "region": "NA",
          "type": "DC",
          "plantType": "RDC",
          "shortName": "South Dallas DC",
          "siteName": "S_2496_752",
          "siteNameDesc": "EXT OPS/DC - SOUTH DALLAS DC -",
          "location": {
            "id": "2496",
            "name": "South Dallas DC",
            "city": "DALLAS",
            "state": "TX",
            "country": "USA",
            "street": "4808 MOUNTAIN CREEK PARKWAY",
            "geoLocation": {
              "longitude": -96.9650634,
              "latitude": 32.6761483
            },
            "timezone": "US/Central",
            "postalCode": "75236-4602"
          }
        },
        {
          "siteNum": "2299",
          "region": "NA",
          "type": "DC",
          "plantType": "RDC",
          "shortName": "Southern Regional DC",
          "siteName": "S_2299_302",
          "siteNameDesc": "South Regional DC -ExtOps/DC",
          "location": {
            "id": "2299",
            "name": "Southern Regional DC",
            "city": "MCDONOUGH",
            "state": "GA",
            "country": "USA",
            "street": "340 WESTRIDGE PARKWAY",
            "geoLocation": {
              "longitude": -84.1929612,
              "latitude": 33.4042901
            },
            "timezone": "US/Eastern",
            "postalCode": "30253-3006"
          }
        }
      ],
      addOrigin:jest.fn(), 
      defaultValues:null,
      type:'network'
    }

    it('should render with label', async () => {
      await renderWithMockStore(OriginFilter,props);
      const component = getTestElement('originFilterElementContainer');
      expect(component).toBeInTheDocument();
    });

    it('should render with label - without alias', async () => {
      await renderWithMockStore(OriginFilter,propsWithoutAlias);
      const component = getTestElement('originFilterElementContainer');
      expect(component).toBeInTheDocument();
    });

    it('should render with label - without alias', async () => {
        await renderWithMockStore(OriginFilterElement,props);
        const component = getTestElement('originFilterElement');
        expect(component).toBeInTheDocument();
      });

});