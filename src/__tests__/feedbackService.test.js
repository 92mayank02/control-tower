import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { saveFeedback, resetFeedback } from "../reduxLib/services"
const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)
import fetch from "../reduxLib/services/serviceHelpers";

jest.mock("../reduxLib/services/serviceHelpers")

describe('async actions', () => {
  let store;

  beforeEach(() => {
    store = mockStore({});
  });

  it('Feedback Post Success', () => {

    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return resolve({
          status: 200,
          json: () => {
            return {}
          }
        })
      })
    })

    return store.dispatch(saveFeedback({ isHappy: true, feedbackMessage: "Hello world", receivedDatetime: new Date() }))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })

  it('Get Locations Data Failed', () => {

    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return reject({
          status: 500,
          json: () => {
            return null
          }
        })
      })
    })

    return store.dispatch(saveFeedback({ isHappy: true, feedbackMessage: "Hello world" }))
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })

  it('Reset Feedback', () => {

    store.dispatch(resetFeedback());
    expect(store.getActions()).toMatchSnapshot();

  })

})