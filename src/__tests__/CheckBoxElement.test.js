//import '@testing-library/jest-dom/extend-expect'
import CheckBoxElement from '../components/common/CheckBoxElement';
import { getTestElement, renderWithMockStore, shallowSetup } from '../testUtils';
import { mockStoreValue } from 'components/dummydata/mock';
import { fireEvent } from '@testing-library/react';

describe('<CheckBoxElement>', () => {
    afterAll(() => {
      jest.restoreAllMocks();
    });
  
    const props = {
      filterKey: "orderTypes",
      type: "network",
      subtype: "network",
      openAll:true
    }

    it('should render with label - with Provider', async () => {
      await renderWithMockStore(CheckBoxElement, props, { ...mockStoreValue, filters:{network:mockStoreValue.user.filterLayouts.NETWORK.views[0].filters}});
      const element = getTestElement('checkBoxElement');
      expect(element).toBeInTheDocument();
    });

    it('Trigger Save Filter', async () => {
      const {getByText, container } = await renderWithMockStore(CheckBoxElement, props, { ...mockStoreValue, filters:{network:mockStoreValue.user.filterLayouts.NETWORK.views[0].filters}});
      fireEvent.click(container.querySelector("input[name='STO']"))
    });
});