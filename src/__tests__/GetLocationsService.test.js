import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { getCustomersService, getLocationsService, getPerformanceMapService, getSalesOfficeService } from "../reduxLib/services"
const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)
import fetch from "../reduxLib/services/serviceHelpers";

jest.mock("../reduxLib/services/serviceHelpers")

describe('async actions', () => {
  let store;

  const customerSearchSucessfulPayload = [
    {
      customerName: "WalmartCanada",
      salesOrgList: [
        {
          salesOrg: "2810",
          distributionChannel: "80",
        },
        {
          salesOrg: "2810",
          distributionChannel: "90",
        },
  
        {
          salesOrg: "2820",
          distributionChannel: "80",
        },
      ],
    },
  ];
  
  const salesOfficeSearchSucessfulPayload = [
    {
      salesOffice: "CS01",
  
      salesOfficeName: "pharma",
  
      salesGroupList: ["Z01", "Z02", "Z03"],
  
      salesOrgList: [
        {
          salesOrg: "2810",
  
          distributionChannel: "80",
        },
  
        {
          salesOrg: "2810",
  
          distributionChannel: "90",
        },
  
        {
          salesOrg: "2820",
  
          distributionChannel: "80",
        },
      ],
    },
  
    {
      salesOffice: "CS02",
  
      salesOfficeName: "Retail",
  
      salesGroupList: ["Z01", "Z02", "Z03"],
  
      salesOrgList: [
        {
          salesOrg: "2810",
  
          salesOrg: "2810",
  
          distributionChannel: "90",
        },
  
        {
          salesOrg: "2820",
  
          distributionChannel: "80",
        },
      ],
    },
  ];
  

  beforeEach(() => {
    store = mockStore({});
  });

  it('Get Locations Data Success', () => {

    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return resolve({
          status: 200,
          json: () => {
            return { data: { "dc": [], "mill": [] } }
          }
        })
      })
    })

    return store.dispatch(getLocationsService())
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })

  it('Get Locations Data Failed', () => {

    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return reject({
          status: 500,
          json: () => {
            return { data: { "dc": [], "mill": [] } }
          }
        })
      })
    })

    return store.dispatch(getLocationsService())
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })


  it('Get Performance Data Failed', () => {

    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return reject({
          status: 500,
          json: () => {
            return []
          }
        })
      })
    })

    return store.dispatch(getPerformanceMapService())
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  });

  it('Get Performance Data Success', () => {

    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return resolve({
          status: 200,
          json: () => {
            return []
          }
        })
      })
    })

    return store.dispatch(getPerformanceMapService())
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })

  it('Get Customer Data Failed', () => {

    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return reject({
          status: 500,
          json: () => {
            return []
          }
        })
      })
    })

    return store.dispatch(getCustomersService())
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  });

  it('Get Customer Data Success', () => {

    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return resolve({
          status: 200,
          json: () => {
            return customerSearchSucessfulPayload
          }
        })
      })
    })

    return store.dispatch(getCustomersService())
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })

  it('Get Sales Ofc Data Failed', () => {

    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return reject({
          status: 500,
          json: () => {
            return []
          }
        })
      })
    })

    return store.dispatch(getSalesOfficeService())
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  });

  it('Get Customer Data Success', () => {

    fetch.mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return resolve({
          status: 200,
          json: () => {
            return salesOfficeSearchSucessfulPayload
          }
        })
      })
    })

    return store.dispatch(getSalesOfficeService())
      .then(() => { // return of async actions
        expect(store.getActions()).toMatchSnapshot();
      })
  })

})