import TableComponent from "../components/common/TableComponent"
import { getTestElement, renderWithMockStore, renderWithProvider, fireEvent } from '../testUtils';
import { defaultTransportFilterArgs, defaultInboundFilterArgs, defaultOutboundFilterArgs, defaultOrderFilterArgs } from "../reduxLib/constdata/filters"
import transportFilters from '../reduxLib/constdata/transportFilters';
import ordersFilters from '../reduxLib/constdata/ordersFilters';
import distoutboundFilters from '../reduxLib/constdata/distoutboundFilters';
import distinboundFilters from '../reduxLib/constdata/distinboundFilters';
import { shortTitleCellStyle } from '../helpers/tableStyleOverride';
import { formatToSentenceCase, endpoints } from "helpers";
import { get, uniq } from "lodash";
import moment from "moment-timezone";
import TransportDetails from "../components/common/Table/TransportDetails";
import ItemDetailsTableInBound from "../components/common/ItemDetailsTableInBound";
import ItemDetailsTableOutBound from "../components/common/ItemDetailsTableOutBound";
import { transportSearch, mockStoreValue } from "../components/dummydata/mock";
import tablemock from "../components/dummydata/tablemock";

jest.mock("../reduxLib/services/tabledataService", () => {
  const getTableData = () => {
    return {
      type: "TABLE_DATA_FETCH_SUCCESS",
      payload: [],
      variables: {}
    }
  };

  return { getTableData }
})

describe('<Table Component>', () => {
  afterAll(() => {
    jest.restoreAllMocks();
  });


  const columns = {
    columnOrder: [
      { title: 'Load #', field: "test", cellStyle: shortTitleCellStyle },
    ],
    columnConfiguration: (d) => {
      return {
        test: get(d, "test", "-"),
      }
    }
  }

  const conditionalFilterReplacement = (newfilters) => {
    if (get(newfilters, "orderExecutionBucket", []).length === 0) {
      newfilters.orderExecutionBucket = defaultTransportFilterArgs.orderExecutionBucket;
    }

    if (newfilters?.orderExecutionBucket?.includes("TRANS_PLAN_PROCESSING")) {
      const transporcessing = ["TRANS_PLAN_PLANNED", "TRANS_PLAN_TENDERED", "TRANS_PLAN_TENDER_REJECTED"]
      newfilters.orderExecutionBucket = uniq([...newfilters.orderExecutionBucket, ...transporcessing]).filter(d => d !== "TRANS_PLAN_PROCESSING");
    }
  }

  const tableprops = {
    siteNums: [],
    pageDetails: { "TRANS_PLAN": true, "filterParams": { "args": { "orderExecutionBucket": ["TRANS_PLAN_UNASSIGNED"] }, tariffServiceCodeList: ["PKUP"], "health": "RED", "state": "TRANS_PLAN" } },
    tableName: "transport",
    title: "transport",
    filters: transportFilters,
    type: "network",
    ItemDetailSection: TransportDetails,
    defaultFilterArgs: defaultTransportFilterArgs,
    filterBody: transportFilters,
    filterType: "transportfilters",
    itemDetailsColumns: [],
    endpoints: endpoints.table.transport,
    excludeArray: ["searchStringList", "loadReadyDateTimeOriginTZ"],
    searchTextPlaceholder: "Enter Shipment #, Order#, Trailer id",
    tableRef: {},
    tablebody: {
      test: "value"
    },
    columns: columns,
    conditionalFilterReplacement: conditionalFilterReplacement,
    topDateFilter: {
      title: "Carrier Ready Date",
      key: "loadReadyDateTimeOriginTZ"
    },
    views: []
  }

  it('Table Component should Transport ', async () => {
    await renderWithMockStore(TableComponent, tableprops);
    const component = getTestElement('tablecomponent');
    expect(component).toBeTruthy();
  });

  it('Table loading Condition ', async () => {
    await renderWithMockStore(TableComponent, { ...tableprops }, {
      ...mockStoreValue, options: {
        ...mockStoreValue.options,
        tablebody: { test: 'test' }
      }, tabledata: { loading: false }
    });
    const component = getTestElement('tablecomponent');
    expect(component).toBeTruthy();
  });

  it('Table loading Condition = BLocked Call ', async () => {
    await renderWithMockStore(TableComponent, { ...tableprops }, { ...mockStoreValue, tabledata: { loading: false }, items: { ...mockStoreValue.items, blockOnTabChange: true }, });
    const component = getTestElement('tablecomponent');
    expect(component).toBeTruthy();
  });

  it('Table Filters Change ', async () => {
    const { getByTestId } = await renderWithMockStore(TableComponent, {
      ...tableprops,
      filters: null
    }, {
      ...mockStoreValue,
      tabledata: {
        data: tablemock,
        page: 1,
        pageSize: 10,
        loading: false
      }
    });
    const component = getTestElement('tablecomponent');
    fireEvent.click(getByTestId('next page'));
    fireEvent.click(getByTestId('last page'));
    fireEvent.click(getByTestId('first page'));
    fireEvent.click(getByTestId('previous page'));

    expect(component).toBeTruthy();
  });


  it('Table Filters Change -+', async () => {
    const { getByTestId } = await renderWithMockStore(TableComponent, {
      ...tableprops,
      filters: null
    }, {
      ...mockStoreValue,
      items: { ...mockStoreValue.items, blockOnTabChange: true },
      tabledata: {
        data: tablemock,
        page: 1,
        pageSize: 10,
        loading: false
      }
    });
    const component = getTestElement('tablecomponent');
    fireEvent.click(getByTestId('next page'));
    fireEvent.click(getByTestId('last page'));
    fireEvent.click(getByTestId('first page'));
    fireEvent.click(getByTestId('previous page'));

    expect(component).toBeTruthy();
  });


});
