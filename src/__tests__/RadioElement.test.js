import { fireEvent } from '@testing-library/react';
import RadioElement from '../components/common/RadioElement';
import { getTestElement, renderWithMockStore } from '../testUtils';
import { Component } from 'react';
import { mockStoreValue } from 'components/dummydata/mock';


describe('<Radio Element>', () => {
    afterAll(() => {
      jest.restoreAllMocks();
    });
  
    const props = {
      "filterKey": "liveLoad",
      "type": "network",
      "subtype": "distoutbound"
    }
    const customStore = {...mockStoreValue, options:{...mockStoreValue.options, filters:{ ...mockStoreValue.options.filters, distoutbound:{
      "liveLoad": {
      "type": "radio",
      "name": "Live Load",
      "data": "Y"
      }
    }}}}
    it('should render with label', async () => {
      await renderWithMockStore(RadioElement,props, customStore );
      const component = getTestElement('radioElement');
      expect(component).toBeInTheDocument();
    });

    it('Click Radio Button - Subit Change', async () => {
      const { getByText } = await renderWithMockStore(RadioElement,props, customStore);
      const component = getTestElement('radioElement');
      expect(component).toBeInTheDocument();
      fireEvent.click(getByText("No"))
    });

    it('Click Radio Button - Reset Value', async () => {
      const { getByText } = await renderWithMockStore(RadioElement,props, customStore);
      const component = getTestElement('radioElement');
      expect(component).toBeInTheDocument();
      fireEvent.click(getByText("Yes"))
    });

});