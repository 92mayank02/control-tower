import SalesOfficeSearchListing from '../components/header/SalesOfficeSearchListing';
import { getTestElement, renderWithMockStore, fireEvent, screen } from '../testUtils';
import { mockStoreValue } from "../components/dummydata/mock"
import userEvent from '@testing-library/user-event';


describe('<Sales Office Search Listing Component>', () => {
  afterAll(() => {
    jest.restoreAllMocks();
  })

  const props = {
    listingFilters:{
    "selectedDC": "80",
    "filterViewType": "SALES_OFFICE",
    "saleOrgsCheckboxArray": [
        {
            "value": "2821",
            "checked": true,
            "name": "NA Professional"
        },
        {
            "value": "2811",
            "checked": true,
            "name": "NA Professional"
        },
        {
            "value": "2820",
            "checked": true,
            "name": "NA Consumer"
        },
        {
            "value": "2810",
            "checked": true,
            "name": "NA Consumer"
        }
    ]
}}

const customerListingProps2 = {
  listingFilters: {
    "selectedDC": "80",
    "filterViewType": "CUST",
    "saleOrgsCheckboxArray": [
        {
            "value": "2821",
            "checked": false,
            "name": "US PROFESSIONAL"
        },
        {
            "value": "2811",
            "checked": false,
            "name": "US PROFESSIONAL"
        },
        {
            "value": "2820",
            "checked": false,
            "name": "Canada CONSUMER"
        },
        {
            "value": "2810",
            "checked": false,
            "name": "Canada CONSUMER"
        }
    ]
}}
 
  it('should render with label', async () => {
    const { getByTestId } = await renderWithMockStore(SalesOfficeSearchListing, props, mockStoreValue );
    const component = getTestElement('SOListing');
    expect(component).toBeInTheDocument();
   // fireEvent.click(getByTestId("tab_2811"))
  });

  it('should render with label', async () => {
    const { getByTestId } = await renderWithMockStore(SalesOfficeSearchListing, props, mockStoreValue );
    const component = getTestElement('SOListing');
    fireEvent.click(getByTestId("tab_2811"))
    expect(screen.getByText("List of Sales offices for Sales Org 2811")).toBeInTheDocument();
    
  });

  it('should render with label - No checkbox selected', async () => {
    await renderWithMockStore(SalesOfficeSearchListing, customerListingProps2, mockStoreValue );
    const component = getTestElement('SOListing');
    expect(component).toBeInTheDocument();
    expect(component).toBeTruthy();
  });

  it('should render with label- Search Input and Submit', async () => {
    const {getByLabelText } = await renderWithMockStore(SalesOfficeSearchListing, props, mockStoreValue );
    const component = getTestElement('SOListing');
    expect(component).toBeInTheDocument();
    fireEvent.change(getByLabelText("search"), {target: {value: 'CSO1'}})
    userEvent.type(screen.getByLabelText("search"), '{enter}')
  });

  it('should render with label - Click Sales Office ', async () => {
    const { getAllByTestId } = await renderWithMockStore(SalesOfficeSearchListing, props, mockStoreValue );
    const component = getTestElement('SOListing');
    expect(component).toBeInTheDocument();
    fireEvent.click(getAllByTestId("ctLink")[0]);
  });
  it('Test Clear Selections', async () => {
    const { getByText } =  await renderWithMockStore(SalesOfficeSearchListing, props, mockStoreValue ) 
    expect(getByText(/clear selections/i).closest('button')).not.toBeDisabled();
    fireEvent.click(screen.queryByText(/clear selections/i));
  });

});