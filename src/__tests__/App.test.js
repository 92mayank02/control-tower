import App from '../../src/App';
import { shallowSetup } from '../testUtils';

describe('<App>', () => {
    afterAll(() => {
      jest.restoreAllMocks();
    });
  
    const appProps = {}
    it('Should render the App', async () => {
      const wrapper = shallowSetup(App);
      expect(wrapper.exists()).toBeTruthy();  
    });

});