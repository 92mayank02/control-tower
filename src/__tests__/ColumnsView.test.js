import { ColumnsView } from 'components/common/Table/TableElements/ColumnsView';
import { getTestElement, renderWithProvider } from '../testUtils';
import { mockStoreValue, mockLocationsProps } from "../components/dummydata/mock"
import { fireEvent } from '@testing-library/react';

describe('<ColumnsView Component>', () => {

    afterAll(() => {
        jest.restoreAllMocks();
    });

    let props = {
        openManager: true,
        columnsAction: jest.fn(),
        columns: mockStoreValue.user.filterLayouts.NETWORK.views[0].columns,
    }

    it('ColumnsView should Not render', async () => {
        await renderWithProvider(ColumnsView, {...props, openManager:false});
        const component = getTestElement('columnsview');
        expect(component).toBe(undefined);
    });

    it('ColumnsView should render', async () => {
        await renderWithProvider(ColumnsView, props);
        const component = getTestElement('columnsview');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
    });

    it('ColumnsView - Test Show Hide Columns', async () => {
        const { getByTestId, getByText } = await renderWithProvider(ColumnsView, props);
        fireEvent.click(getByTestId("hideallbutton"));
        expect(getByText(/show all/i).closest('button')).not.toBeDisabled();

        fireEvent.click(getByTestId("showallbutton"));
        expect(getByText(/hide all/i).closest('button')).not.toBeDisabled();
    });

    it('ColumnsView - Test Show Hide Columns = II', async () => {
        const { getByTestId, getByText } = await renderWithProvider(ColumnsView, { ...props, columns: mockStoreValue.user.filterLayouts.DISTINBOUND.views[1].columns});
        fireEvent.click(getByTestId("hideallbutton"));
        expect(getByText(/show all/i).closest('button')).not.toBeDisabled();
        expect(getByText(/hide all/i).closest('button')).toBeDisabled();

        fireEvent.click(getByTestId("showallbutton"));
        expect(getByText(/show all/i).closest('button')).toBeDisabled();
        expect(getByText(/hide all/i).closest('button')).not.toBeDisabled();
    });

    it('ColumnsView - Shuffle Items Up and Down', async () => {
        const { getAllByTestId } = await renderWithProvider(ColumnsView, { ...props, columns: mockStoreValue.user.filterLayouts.DISTINBOUND.views[1].columns});
        fireEvent.click(getAllByTestId("shuffleuparrow")[0]);
        fireEvent.click(getAllByTestId("shuffleuparrow")[5]);
        fireEvent.click(getAllByTestId("shuffledownarrow")[3]);
    });

    xit('ColumnsView - Toggle Visibility On/OFf', async () => {
        const { getAllByTestId } = await renderWithProvider(ColumnsView, props);
        fireEvent.click(getAllByTestId("togglevisibilityon")[1]);

    });

    xit('ColumnsView - Toggle Visibility On/OFf', async () => {
        const { getAllByTestId } = await renderWithProvider(ColumnsView, { ...props, columns: mockStoreValue.user.filterLayouts.NETWORK.views[1].columns});
        fireEvent.click(getAllByTestId("togglevisibilityoff")[2]);
    });
});