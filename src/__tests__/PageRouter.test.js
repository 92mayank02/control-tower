import { renderWithMockStore,initiateGlobalAuthService, reRenderWithMockStore, shallowSetup } from '../testUtils';
import { mockStoreValue, mockShipmentDetailsStopsDetails, performanceView } from "../components/dummydata/mock"
import { PageRouter } from 'components/pages/PageRouter';

jest.mock('../theme/layouts/MainLayout', () => ({
    chartDataIdentifierForAPI: {
      network: 'NETWORK',
      order: 'ORDER',
      transPlan: 'TRANS_PLAN',
      transExec: 'TRANS_EXEC',
      dist: 'DIST',
      shipmentdetails: 'SHIPMENTDETAILS',
      analytics: 'ANALYTICS'
    }
  }));

jest.mock("../theme/layouts/GeoChart", () => {
    return {
      __esModule: true,
      default: () => {
        return <div data-testid="geochartmock" ></div>;
      },
    };
});
jest.mock("../reduxLib/services/getLocationsService", () => {
    return {
      getPerformanceMapService: () => (
        {
          type: "PERFORMANCE_FETCH_SUCCESS",
          payload: {}
        }
      )
    }
  })
jest.mock("../theme/layouts/AppContext", ()=>{
    return {
        useAppContext : () => ({ setRef:jest.fn(), getRef:jest.fn()})
    } 
})

describe("Page Router", ()=> {

    afterAll(() => {
        jest.restoreAllMocks();
    });

    beforeAll(() => {
        initiateGlobalAuthService();
    });

    it("Render Network", async() =>{

        const networkProps = {
            perfTableRedirection:jest.fn(),
            "type": "2028",
            "prevPageDetails": {
                "activeTopNavTab": "orders",
                "secondaryActiveTab": null
            },
            "pageDetails": {
                "NETWORK": true,
                "filterParams": {
                    "reportURL": false
                },
                "type": "2028"
            },
            "siteNums": null,
            "showTabsBy": "customer",
            "globalFilterSelection": {}
        }
        const { getByTestId } = await renderWithMockStore(PageRouter, networkProps, {...mockStoreValue, sites: { ...mockStoreValue.sites, performance:performanceView}})
        expect(getByTestId("getviews")).toBeInTheDocument()
    })

    it('should render with label', async () => {  
        const networkProps = {
            perfTableRedirection:jest.fn(),
            "type": "2028",
            "prevPageDetails": {
                "activeTopNavTab": "orders",
                "secondaryActiveTab": null
            },
            "pageDetails": {
                "NETWORK": true,
                "filterParams": {
                    "reportURL": false
                },
                "type": "2028"
            },
            "siteNums": null,
            "showTabsBy": "customer",
            "globalFilterSelection": {}
        }    
        const wrapper = shallowSetup(PageRouter,networkProps);
        expect(wrapper.exists()).toBeTruthy();  
    });
    it("Render Orders", async() =>{
        const orderProps = {
            "type": "mylocation",
            "prevPageDetails": {
                "activeTopNavTab": "mylocation",
                "secondaryActiveTab": null
            },
            "pageDetails": {
                "ORDER": true,
                "filterParams": {},
                "type": "mylocation"
            },
            "siteNums": [
                "CS01",
                "CS07"
            ],
            "showTabsBy": "customer",
            "globalFilterSelection": {
                "siteNums": [],
                "customerOrSalesOffice": [
                    {
                        "selectionType": "SALES_OFFICE",
                        "salesOrg": "2810",
                        "distributionChannel": "80",
                        "salesOffice": "CS01"
                    },
                    {
                        "selectionType": "SALES_OFFICE",
                        "salesOrg": "2820",
                        "distributionChannel": "80",
                        "salesOffice": "CS07"
                    }
                ]
            }
        }
        const { getByTestId, rerender } = await renderWithMockStore(PageRouter, orderProps, null, "/dashboard/ORDER")
        expect(getByTestId("getviews")).toBeInTheDocument()
        await rerender(await reRenderWithMockStore(PageRouter, {...orderProps, "globalFilterSelection": {
            "siteNums": ["2042"],
            "customerOrSalesOffice": [
                {
                    "selectionType": "SALES_OFFICE",
                    "salesOrg": "2820",
                    "distributionChannel": "80",
                    "salesOffice": "CS07"
                }
            ]
        }}));
        expect(getByTestId("getviews")).toBeInTheDocument()
    })

    it("Render Transport", async() =>{
        const transportProps = {
            "type": "mylocation",
            "prevPageDetails": {
                "activeTopNavTab": "mylocation",
                "secondaryActiveTab": null
            },
            "pageDetails": {
                "TRANS_PLAN": true,
                "filterParams": {
                    "reportURL": false
                },
                "type": "mylocation"
            },
            "siteNums": [
                "CS01",
                "CS07"
            ],
            "showTabsBy": "customer",
            "globalFilterSelection": {
                "siteNums": [],
                "customerOrSalesOffice": [
                    {
                        "selectionType": "SALES_OFFICE",
                        "salesOrg": "2810",
                        "distributionChannel": "80",
                        "salesOffice": "CS01"
                    },
                    {
                        "selectionType": "SALES_OFFICE",
                        "salesOrg": "2820",
                        "distributionChannel": "80",
                        "salesOffice": "CS07"
                    }
                ]
            }
        }
        const { getByTestId } = await renderWithMockStore(PageRouter, transportProps, null, "/dashboard/TRANS_PLAN")
        expect(getByTestId("getviews")).toBeInTheDocument()
    })
    it("Render Transport Exec", async() =>{
        const transportProps = {
            "type": "mylocation",
            "prevPageDetails": {
                "activeTopNavTab": "mylocation",
                "secondaryActiveTab": null
            },
            "pageDetails": {
                "TRANS_PLAN": true,
                "filterParams": {
                    "reportURL": false
                },
                "type": "mylocation"
            },
            "siteNums": [
                "CS01",
                "CS07"
            ],
            "showTabsBy": "customer",
            "globalFilterSelection": {
                "siteNums": [],
                "customerOrSalesOffice": [
                    {
                        "selectionType": "SALES_OFFICE",
                        "salesOrg": "2810",
                        "distributionChannel": "80",
                        "salesOffice": "CS01"
                    },
                    {
                        "selectionType": "SALES_OFFICE",
                        "salesOrg": "2820",
                        "distributionChannel": "80",
                        "salesOffice": "CS07"
                    }
                ]
            }
        }
        const { getByTestId } = await renderWithMockStore(PageRouter, transportProps, null, "/dashboard/TRANS_EXEC")
        expect(getByTestId("getviews")).toBeInTheDocument()
    })
    it("Render Dist", async() =>{
        const distProps = {
            "type": "mylocation",
            "prevPageDetails": {
                "activeTopNavTab": "mylocation",
                "secondaryActiveTab": null
            },
            "pageDetails": {
                "DIST": true,
                "filterParams": {},
                "type": "mylocation"
            },
            "siteNums": [
                "CS01",
                "CS07"
            ],
            "showTabsBy": "customer",
            "globalFilterSelection": {
                "siteNums": [],
                "customerOrSalesOffice": [
                    {
                        "selectionType": "SALES_OFFICE",
                        "salesOrg": "2810",
                        "distributionChannel": "80",
                        "salesOffice": "CS01"
                    },
                    {
                        "selectionType": "SALES_OFFICE",
                        "salesOrg": "2820",
                        "distributionChannel": "80",
                        "salesOffice": "CS07"
                    }
                ]
            }
        }
        const { getByTestId } = await renderWithMockStore(PageRouter, distProps, null, "/dashboard/DIST")
        expect(getByTestId("distributionContainer")).toBeInTheDocument()
    })

    it("Render ShipmentDetails", async() => {
        const stops = mockShipmentDetailsStopsDetails.stops
        const shipmentDetails = mockShipmentDetailsStopsDetails;
        const sdProps = {
            "type": "mylocation",
            "prevPageDetails": {
                "activeTopNavTab": "mylocation",
                "secondaryActiveTab": null
            },
            "pageDetails": {
                "SHIPMENTDETAILS": true,
                "filterParams":{shipmentNum:"1234567"},
                "type": "mylocation"
            },
            "siteNums": [
                "CS01",
                "CS07"
            ],
            "showTabsBy": "customer",
            "globalFilterSelection": {
                "siteNums": [],
                "customerOrSalesOffice": [
                    {
                        "selectionType": "SALES_OFFICE",
                        "salesOrg": "2810",
                        "distributionChannel": "80",
                        "salesOffice": "CS01"
                    },
                    {
                        "selectionType": "SALES_OFFICE",
                        "salesOrg": "2820",
                        "distributionChannel": "80",
                        "salesOffice": "CS07"
                    }
                ]
            }
        }
        const { getByTestId } = await renderWithMockStore(PageRouter, sdProps, { ...mockStoreValue, shipment: { details: shipmentDetails, stops, locationHistory: [] } }, "/dashboard/SHIPMENTDETAILS")
        expect(getByTestId("getviews")).toBeInTheDocument()
    })

    it("Render Analytics", async() =>{
        const anaProps = {
            "type": "mylocation",
            "prevPageDetails": {
                "activeTopNavTab": "mylocation",
                "secondaryActiveTab": null
            },
            "pageDetails": {
                "ANALYTICS": true,
                "type": "network",
                "filterParams":{ reportURL: "https://app.powerbi.com/reportEmbed?reportId=b861ca9e-412d-44dc-b505-808692d5c1a3&autoAuth=true&ctid=fee2180b-69b6-4afe-9f14-ccd70bd4c737&config=eyJjbHVzdGVyVXJsIjoiaHR0cHM6Ly93YWJpLXVzLW5vcnRoLWNlbnRyYWwtZS1wcmltYXJ5LXJlZGlyZWN0LmFuYWx5c2lzLndpbmRvd3MubmV0LyJ9"}
            },
            "siteNums": [
                "CS01",
                "CS07"
            ],
            "showTabsBy": "customer",
            "globalFilterSelection": {
                "siteNums": [],
                "customerOrSalesOffice": [
                    {
                        "selectionType": "SALES_OFFICE",
                        "salesOrg": "2810",
                        "distributionChannel": "80",
                        "salesOffice": "CS01"
                    },
                    {
                        "selectionType": "SALES_OFFICE",
                        "salesOrg": "2820",
                        "distributionChannel": "80",
                        "salesOffice": "CS07"
                    }
                ]
            }
        }
        const { getByTestId } = await renderWithMockStore(PageRouter, anaProps,null, "/dashboard/ANALYTICS")
        expect(getByTestId("getviews")).toBeInTheDocument()
    })

})