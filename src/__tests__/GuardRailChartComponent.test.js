
import { getGuardrailsService } from "../reduxLib/services/getChartsService";
import { GuardrailChartComponent } from "../components/common/GuardrailChartComponent";
import { getTestElement, renderWithProvider } from '../testUtils';
import { mockGuardrailsDetails } from "components/dummydata/mock";
import { DistributionShipmentsChartBuilder } from 'components/common/DistributionShipmentsChartBuilder';

jest.mock("../reduxLib/services/getChartsService", () => {
    const getGuardrailsService = () => true;
    return { getGuardrailsService }
})

const rest = { refresh: { refresh: false, time: new Date(), siteNums: [] }, setRefresh: (newRefreshObj) => rest.refresh = newRefreshObj }


describe('<All Charts Rendering Component>', () => {
    afterAll(() => {
        jest.restoreAllMocks();
    });

    const chartProps = {
        ...rest,
        getGuardrailsService,
        setHeaderTotalCount: jest.fn(),
        guardrailData: mockGuardrailsDetails
    }

    const transportPlanningNoData = [{ "state": "TRANS_PLAN_UNASSIGNED", "stateDesc": "Unassigned", "totalCount": 0, "redCount": 0, "greenCount": 0 }, { "state": "TRANS_PLAN_OPEN", "stateDesc": "Open", "totalCount": 0, "redCount": 0, "greenCount": 0 }, { "state": "TRANS_PLAN_PLANNED", "stateDesc": "Planned", "totalCount": 0, "redCount": 0, "greenCount": 0 }, { "state": "TRANS_PLAN_TENDERED", "stateDesc": "Tendered", "totalCount": 0, "redCount": 0, "greenCount": 0 }, { "state": "TRANS_PLAN_TENDER_REJECTED", "stateDesc": "Tender Rejected", "totalCount": 0, "redCount": 0, "greenCount": 0 }, { "state": "TRANS_PLAN_TENDER_ACCEPTED", "stateDesc": "Tender Accepted", "totalCount": 0, "redCount": 0, "greenCount": 0 }]

    const transportPlanningEmptyResponse = []

    const donutProps = {
        chartsData: [{ "state": "SO_BLOCK_FREE", "stateDesc": "Customer Orders Block free", "totalCount": 35800, "redCount": 1, "yellowCount": 35755 }, { "state": "SO_BLOCKED", "stateDesc": "Customer Orders Blocked", "totalCount": 163, "redCount": 163, "yellowCount": 0 }, { "state": "STO", "stateDesc": "Stock Transfer Orders", "totalCount": 14, "redCount": 0, "yellowCount": 0 }],
        setHeaderTotalCount: jest.fn()
    }

    it('Render Chart Component', async () => {
        await renderWithProvider(GuardrailChartComponent, { ...chartProps, showLegends: true, showViewDetails: true, BuilderComponent: DistributionShipmentsChartBuilder });
        const component = getTestElement('guardrailTitle')
        expect(component).toBeInTheDocument();
    });

});