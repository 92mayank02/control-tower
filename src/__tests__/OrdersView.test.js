import OrdersView, { conditionalFilterReplacement, tableStyleConfig } from 'components/pages/OrdersView';
import { getTestElement, initiateGlobalAuthService, renderWithMockStore, fireEvent, waitFor } from '../testUtils';
import { defaultOrderColumns } from "../reduxLib/constdata/orderColumns"
import { mockStoreValue, } from "../components/dummydata/mock"
import fetch from "../reduxLib/services/serviceHelpers";


jest.mock("../../src/reduxLib/constdata/ordersFilters", () => {
    const ordersfilters = {
        searchStringList: {
            type: "text",
            name: "Search",
            data: null
        },
        destName: {
            type: "text",
            name: "Customer",
            data: null
        },
        sites: {
            type: "sites",
            name: "Origin",
            data: null
        },
        shippingCondition: {
            type: "text",
            name: "Shipping Condition",
            data: null
        },
        requestedDeliveryDate: {
            type: "date",
            hidetime: true,
            name: "Requested Delivery Date",
            shortName: 'Req. Delivery Date',
            data: null
        },
        deliveryBlockCodeList: {
            type: "text",
            name: "Delivery Block",
            data: null
        },
        confirmedEquivCubes: {
            type: "checkbox",
            name: "Confirmed Cube",
            shortName: 'Confirmed Cube',
            data: [
                { name: "< 1000", value: { lte: 999.9999999 }, checked: false },
                { name: "1000 - 2000", value: { gte: 1000, lte: 1999.9999999 }, checked: false },
                { name: "2000 - 3000", value: { gte: 2000, lte: 2999.9999999 }, checked: false },
                { name: "> 3000", value: { gte: 3000 }, checked: false },
            ]
        },
        destState: {
            type: "text",
            name: "Destination State",
            data: null
        },
        destCity: {
            type: "text",
            name: "Destination City",
            data: null
        },
        matAvailDate: {
            type: "date",
            name: "Material Availability Date",
            shortName: 'Mat.Avail Date',
            data: null
        },
        orderExecutionBucket: {
            type: "checkbox",
            name: "Shipment Status",
            data: [
                { name: "Transportation to be planned", value: "TRANS_PLAN_UNASSIGNED", checked: false },
                { name: "Transportation planning", value: "TRANS_PLAN_OPEN", checked: false },
                { name: "Transportation processing", value: "TRANS_PLAN_PROCESSING", checked: false },
                { name: "Transportation Carrier committed", value: "TRANS_PLAN_TENDER_ACCEPTED", checked: false },
                { name: "Shipment Created", value: "SHIPMENT_CREATED", checked: false },
                { name: "Checked in by DC", value: "CHECK_IN", checked: false },
                { name: "Loading Started", value: "LOADING_STARTED", checked: false },
                { name: "Ready for Pickup", value: "TRANS_EXEC_READY_PICK_UP", checked: false },
                { name: "In Transit", value: "TRANS_EXEC_IN_TRANSIT", checked: false },
                { name: "Delivery confirmed", value: "TRANS_EXEC_DELIVERY_CONFIRMED", checked: false }
            ]
        },
        orderTypes: {
            type: "checkbox",
            name: "Order Type",
            data: [
                { name: "Cust. Order", value: "CUST", checked: false },
                { name: "STO", value: "STO", checked: false }
            ]
        },
        orderHealth: {
            type: "checkbox",
            name: "Health",
            data: [
                { name: "Unhealthy", value: "RED", checked: false },
                { name: "Needs Attention", value: "YELLOW", checked: false },
                { name: "Healthy", value: "GREEN", checked: false }

            ]
        },
        materialNumberList: {
            type: "text",
            name: "Material ID",
            stringToArray: true,
            data: null
        }
    }

    return ordersfilters
}
)
jest.mock("../reduxLib/services/serviceHelpers")
describe('< OrdersView Component 1>', () => {

    afterEach(() => {
        jest.restoreAllMocks();
    });

    beforeAll(() => {
        initiateGlobalAuthService();
    });

    const props = {
        setDetails: jest.fn(),
        pageDetails: { filterParams: null },
        setRefresh: jest.fn()
    }

    it('OrdersView (Filter Condition Function)', async () => {
        let orderFilters = {
            //orderStatusFilters: ["ORDER_BLOCKED"],
            orderExecutionBucket: ["TRANS_PLAN_PROCESSING"]
        }

        await conditionalFilterReplacement(orderFilters)
        expect(orderFilters).toEqual(
            {
                "orderExecutionBucket": ["TRANS_PLAN_PLANNED", "TRANS_PLAN_TENDERED", "TRANS_PLAN_TENDER_REJECTED"],
            }
        );
    });

    it('OrdersView (Table Options Function)', async () => {
        const tableoptions = {
            rowStyle: {
                color: "#FFF"
            }
        }
        expect(tableStyleConfig.rowStyle({ orderHealth: "RED" })).toEqual(
            {
                "@media (min-width:600px)": {
                    "fontWeight": "500",
                },
                "color": "#FFF",
                "borderLeft": "5px solid #FF4081",
                "flexGrow": 1,
                "fontFamily": "'Gilroy', sans_serif",
                "fontSize": "14px",
                "fontWeight": 400,
                "lineHeight": 1.5,
            }
        );
        expect(tableStyleConfig.rowStyle({ orderHealth: "YELLOW" })).toEqual(
            {
                "@media (min-width:600px)": {
                    "fontWeight": "500",
                },
                "color": "#FFF",
                "borderLeft": "5px solid #F5B023",
                "flexGrow": 1,
                "fontFamily": "'Gilroy', sans_serif",
                "fontSize": "14px",
                "fontWeight": 400,
                "lineHeight": 1.5,
            }
        );
        expect(tableStyleConfig.rowStyle({ orderExecutionHealth: "RED" })).toEqual(
            {
                "@media (min-width:600px)": {
                    "fontWeight": "500",
                },
                "color": "#FFF",
                "borderLeft": "",
                "flexGrow": 1,
                "fontFamily": "'Gilroy', sans_serif",
                "fontSize": "14px",
                "fontWeight": 400,
                "lineHeight": 1.5,
            }
        );
    });


    it('OrdersView (Column Configuration for "-" )', async () => {
        const data = {}
        expect(defaultOrderColumns.columnConfiguration(data)).toEqual(
            {
                liveLoadInd: "-",
                appointmentRequired: "-",
                orderOnHoldInd: "-",
                deliverApptDateTime: "-",
                deliveryBlocksString: "-",
                expectedDeliveryDateTime: "-",
                actualDeliveredDateTime: "-",
                loadReadyDateTime: "-",
                customer: "-",
                origin: "-",
                originId: "-",
                destinationCity: "-",
                destinationState: "-",
                orderStatusBucket: "-",
                orderType: "-",
                reasonCodeString: "-",
                salesGroup: "-",
                salesOffice: "-",
                salesOrg: "-",
                distributionChannel: "-"
            }
        );
    });

    it('OrdersView should render', async () => {

        fetch.mockImplementation(() => {
            return new Promise((resolve, reject) => {
                return resolve({
                    status: 200,
                    json: () => {
                        return { "onPage": 1, "pageSize": 5, "totalPages": 1, "totalRecords": 0, "orders": [] }
                    }
                })
            })
        })
        await renderWithMockStore(OrdersView, props, { ...mockStoreValue, favorites: { ...mockStoreValue.favorites, tempBusinessUnit: ["PROFESSOINAL"] } });
        const component = getTestElement('ordersview');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
    });


});