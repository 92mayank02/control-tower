import {LocationsFilterLayout} from '../components/header/LocationsFilterLayout';
import { getTestElement, renderWithProvider, fireEvent, renderWithMockStore } from '../testUtils';
import React from "react";
import { mockStoreValue } from 'components/dummydata/mock';
import useMediaQuery from "@material-ui/core/useMediaQuery"
jest.mock('../components/header/BusinessFilter', () => {
    return {
        __esModule: true,
        A: true,
        default: () => {
          return <div></div>;
        },
      };
})

jest.mock('@material-ui/core/useMediaQuery')


describe('<LocationsFilterLayout>', () => {
    afterAll(() => {
      jest.restoreAllMocks();
    });
  
    const props = {
        handleClick: jest.fn(),
        open: true
    };

    it('should render with label', async () => {
      await renderWithMockStore(LocationsFilterLayout,props);
      const component = getTestElement('locationfilterlayout');
      expect(component).toBeInTheDocument();
    });

    it('should render with label - Null Check ', async () => {
      await renderWithMockStore(LocationsFilterLayout,{...props, open:false});
      const component = getTestElement('locationfilterlayout');
      expect(component).toBe(undefined);
    });

    it('Click Away', async () => {
        await renderWithMockStore(LocationsFilterLayout,props)
        const component = getTestElement('locationfilterlayout');
        window.document.body.dispatchEvent(new Event('click'));
        expect(component).toBeInTheDocument();
      });

      it('Close icon', async () => {
        await renderWithMockStore(LocationsFilterLayout,props)
        const component = getTestElement('locationfilterlayout');
        const { getAllByTestId } = await renderWithMockStore(LocationsFilterLayout,props);
        fireEvent.click(getAllByTestId('closeicon')[0]);
        expect(component).toBeInTheDocument();
      });

      it('Tab Click', async () => {
        useMediaQuery.mockImplementation(() => {
          return {
              __esModule: true,
              default: () => {
                return true;
              },
            };
        })
        await renderWithMockStore(LocationsFilterLayout,props)
        const component = getTestElement('locationfilterlayout');
        const { getAllByTestId } = await renderWithMockStore(LocationsFilterLayout,props, mockStoreValue);
        fireEvent.click(getAllByTestId('Location_tab')[0]);
        expect(component).toBeInTheDocument();
      });

});