import Select from '../components/common/Select';
import { getTestElement, renderWithProvider } from '../testUtils';

describe('<Select Element>', () => {
    afterAll(() => {
      jest.restoreAllMocks();
    });
  
    let props = {
      options: [
          {name: "All DC and Mills", value: "ALL"}
      ],
      label: "Test",
      onChange: jest.fn()
  }

    it('should render with label', async () => {
      await renderWithProvider(Select,props);
      const component = getTestElement('selectElement');
      expect(component).toBeInTheDocument();
    });

    it('Button Click Test', async () => {
        await renderWithProvider(Select, props);
        const button = await document.querySelector("button")
        const test = await button.dispatchEvent(new MouseEvent('click', { bubbles: true }));
        expect(test).toBeTruthy();
    });

    it('Button & Menu Item Click Test', async () => {
      await renderWithProvider(Select, {...props, label: null});
      const button = await document.querySelector("button")
      await button.dispatchEvent(new MouseEvent('click', { bubbles: true }));

      const item = document.querySelector("[name='menuitem']");
      const test = await item.dispatchEvent(new MouseEvent('click', { bubbles: true }));

      expect(test).toBeTruthy();
  });

});