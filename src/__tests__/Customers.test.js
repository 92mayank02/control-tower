import Customers, { TooltipTitle } from '../components/header/Customers';
import { getTestElement, renderWithMockStore, renderWithProvider, fireEvent, screen, userEvent } from '../testUtils';
import { mockStoreValue } from "../components/dummydata/mock"
import CustomerSearchListing from 'components/header/CustomerSearchListing';
import useMediaQuery from "@material-ui/core/useMediaQuery"
jest.mock('@material-ui/core/useMediaQuery')

describe('<Customers Component>', () => {
  afterAll(() => {
    jest.restoreAllMocks();
  })

  const props = {}

  const customerListingProps = {
    listingFilters: {
      "selectedDC": "80",
      "filterViewType": "CUST",
      "saleOrgsCheckboxArray": [
          {
              "value": "2821",
              "checked": true,
              "name": "US PROFESSIONAL"
          },
          {
              "value": "2811",
              "checked": true,
              "name": "US PROFESSIONAL"
          },
          {
              "value": "2820",
              "checked": true,
              "name": "Canada CONSUMER"
          },
          {
              "value": "2810",
              "checked": true,
              "name": "Canada CONSUMER"
          }
      ]
  }}

  const customerListingProps2 = {
    listingFilters: {
      "selectedDC": "80",
      "filterViewType": "CUST",
      "saleOrgsCheckboxArray": [
          {
              "value": "2821",
              "checked": false,
              "name": "US PROFESSIONAL"
          },
          {
              "value": "2811",
              "checked": false,
              "name": "US PROFESSIONAL"
          },
          {
              "value": "2820",
              "checked": false,
              "name": "Canada CONSUMER"
          },
          {
              "value": "2810",
              "checked": false,
              "name": "Canada CONSUMER"
          }
      ]
  }}
 
  it('should render with label', async () => {
    await renderWithMockStore(Customers, props, mockStoreValue );
    const component = getTestElement('customers');
    expect(component).toBeInTheDocument();
    expect(component).toBeTruthy();
  });

  it('should render with label -Mobile', async () => {
    useMediaQuery.mockImplementation(() => {
      return {
          __esModule: true,
          default: () => {
            return true;
          },
        };
    })
    await renderWithMockStore(Customers, props, mockStoreValue );
    const component = getTestElement('customers');
    expect(component).toBeInTheDocument();
    expect(component).toBeTruthy();
  });


  it('should render with label', async () => {
    await renderWithProvider(TooltipTitle, {dc:"80"} );
    const component = getTestElement('customertooltip');
    expect(component).toBeInTheDocument();
    expect(component).toBeTruthy();
  });

  it('should render with label', async () => {
    await renderWithProvider(TooltipTitle, {dc:"80"} );
    const component = getTestElement('customertooltip');
    expect(component).toBeInTheDocument();
    expect(component).toBeTruthy();
  });


  it('should render with label -- No selections', async () => {
    await renderWithMockStore(CustomerSearchListing, customerListingProps, {...mockStoreValue, items: {...mockStoreValue.items, cuso:{SALES_OFFICE:[], CUST:[]}}});
    const component = getTestElement('CUlisting');
    expect(component).toBeInTheDocument();
    expect(component).toBeTruthy();
  });

  it('should render with label', async () => {
    await renderWithMockStore(CustomerSearchListing, customerListingProps2, mockStoreValue);
    const component = getTestElement('CUlisting');  
    expect(component).toBeInTheDocument();
    expect(component).toBeTruthy();
  });
  it('Test Clear Selections', async () => {
    const { getByText } =  await renderWithMockStore(CustomerSearchListing, customerListingProps, mockStoreValue ) 
    expect(getByText(/clear selections/i).closest('button')).not.toBeDisabled();
    fireEvent.click(screen.queryByText(/clear selections/i));
  });

  it('should render with label -Mobile', async () => {
    useMediaQuery.mockImplementation(() => {
      return {
          __esModule: true,
          default: () => {
            return true;
          },
        };
    })
    const { getByText } = await renderWithMockStore(Customers, props, mockStoreValue );    
    expect(getByText("FilterBy")).toBeInTheDocument();
    fireEvent.click(screen.queryByText(/FilterBy/i));

  });

  it('should render with label- Search Input and Submit', async () => {
    const {getByLabelText } = await renderWithMockStore(Customers, props, mockStoreValue );
    fireEvent.change(getByLabelText("search"), {target: {value: 'ama*'}})
    userEvent.type(screen.getByLabelText("search"), '{enter}')
  });
  
  it('should render with label- Search Input and Submit', async () => {
    const {getByLabelText } = await renderWithMockStore(CustomerSearchListing, customerListingProps, mockStoreValue );
    const component = getTestElement('CUlisting');
    expect(component).toBeInTheDocument();
    fireEvent.change(getByLabelText("search"), {target: {value: 'wal*'}})
    userEvent.type(screen.getByLabelText("search"), '{enter}')
  });

  it('should render with label- Checkbox', async () => {
    const {getByLabelText } = await renderWithMockStore(Customers, props, mockStoreValue );
    fireEvent.click(screen.getByText("2821 - Canada professional"))
    //userEvent.type(screen.getByLabelText("2821 - Canada professional"), '{enter}')
  });

  it('should render with label-2', async () => {
    await renderWithMockStore(Customers, props, {...mockStoreValue, favorites:{...mockStoreValue.favorites, tempBusinessUnit:[]}} );
    const component = getTestElement('customers');
    expect(component).toBeInTheDocument();
    expect(component).toBeTruthy();
  });

});