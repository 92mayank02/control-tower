import { ColumnManager } from 'components/common/Table/ColumnManager';
import { getTestElement, renderWithProvider } from '../testUtils';
import { fireEvent } from '@testing-library/react';

describe('<ColumnManager Component>', () => {

    afterAll(() => {
        jest.restoreAllMocks();
    });

    let props = {
        openManager: true,
        columnsAction: jest.fn(),
        columns: [{ title: 'Shipment #', field: "shipmentNum", cellStyle: jest.fn() }],
    }

    it('ColumnManager should render', async () => {
        await renderWithProvider(ColumnManager, props);
        const component = getTestElement('columnmanager');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
    });


    it('ColumnManager Button Click Test', async () => {
        await renderWithProvider(ColumnManager, props);
        const submitButton = await getTestElement('button');
        const test = submitButton.dispatchEvent(new MouseEvent('click', { bubbles: true }));
        expect(test).toBeTruthy();
    });

    it('Click Away Test', async () => {
        await renderWithProvider(ColumnManager, props);
        const body = await document.querySelector("body");
        const test = body.dispatchEvent(new MouseEvent('click', { bubbles: true }));
        expect(test).toBeTruthy();
    });

});