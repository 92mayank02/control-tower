import TableActions from 'components/common/Table/TableActions';
import { getTestElement, renderWithMockStore } from '../testUtils';


describe('<TableActions Component>', () => {

    afterAll(() => {
        jest.restoreAllMocks();
    });

    const props = {
        refreshTable: jest.fn(),
        handleClose: jest.fn(),
        handleClick: jest.fn(),
        filters: null,
        downloads: {
            type: "network",
            subtype: "network"
        },
        columns: [{ name: "test" }],
        subtype:"NETWORK"  
    }

    it('TableActions should render', async () => {
        await renderWithMockStore(TableActions, props);
        const component = getTestElement('tableactions');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
    });

});