import Filters from '../components/common/Filters';
import { getTestElement, renderWithMockStore } from '../testUtils';


describe('<Filters Main Container Element>', () => {
    afterAll(() => {
      jest.restoreAllMocks();
    });
  
    const props = {
      filtertype:"networkfilters",
      open:"true",
      type:"network",
      subtype:"network",
      exclude:["searchStringList"],
      searchTextPlaceholder:"Enter Shipment#, Order#"
    }
    it('should render with label', async () => {
      await renderWithMockStore(Filters,props);
      const component = getTestElement('filterMainContaner');
      expect(component).toBeInTheDocument();
    });

});