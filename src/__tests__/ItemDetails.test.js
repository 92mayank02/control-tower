import ItemDetailsTableInBound, { columns as inColumns } from 'components/common/ItemDetailsTableInBound';
import ItemDetailsTableOutBound, { columns as outColumns } from 'components/common/ItemDetailsTableOutBound';
import { getTestElement, renderWithProvider, initiateGlobalAuthService } from '../testUtils';
import fetch from "../reduxLib/services/serviceHelpers";
jest.mock("../reduxLib/services/serviceHelpers")

describe('<Item Details Component>', () => {

    afterAll(() => {
        jest.restoreAllMocks();
    });

    beforeAll(() => {
        initiateGlobalAuthService();
    });



    const propsInbound = { data: { "orderNum": "76782499", "customerNotifiedDateTime": [], "deliveryApptConfirmedDateTimeList": [], "originRegion": "NA", "orderItems": [{ "materialNum": "104684303", "custMaterialNum": "54000468438", "materialGroupNum": "048", "itemDesc": "SCOTT,BR CAS,TWL,-,6 PK,96", "netOrderValue": 13470.03, "orderQty": 768.0, "loadedQty": 0, "differenceQty": "100", "qtyUom": "CS", "itemPosNum": "000011", "qtyDiscrepancy": "UNKNOWN", "deliveryLines": [{ "deliveryLineId": "0814794467-000010", "batchNum": " ", "deliveryNum": "814794467", "batchMgmtReq": "N" }] }] } }

    const propsOutbound = { data: { "orderNum": "2004839934", "customerNotifiedDateTime": [], "deliveryApptConfirmedDateTimeList": [], "originRegion": "NA", "orderItems": [{ "materialNum": "107244420", "custMaterialNum": "2461", "materialGroupNum": "220", "itemDesc": "COTT FRSH FMW RFL 42", "netOrderValue": 22246.96, "orderedQty": 798.0, "confirmedQty": 798.0, "qtyUom": "CS", "itemPosNum": "000011", "qtyDiscrepancy": "OVERAGE", differenceQty: 25 }, { "materialNum": "101670100", "custMaterialNum": "", "materialGroupNum": "048", "itemDesc": "HUG SS RFL 3PK 200 PLT", "netOrderValue": 14161.72, "orderedQty": 18.0, "confirmedQty": 18.0, "qtyUom": "PAL", "itemPosNum": "000021", "qtyDiscrepancy": "UNKNOWN" }] } }

    it('Inbound item details  should render', async () => {

        fetch.mockImplementation(() => {
            return new Promise((resolve, reject) => {
                return resolve({
                    status: 200,
                    json: () => {
                        return propsInbound.data
                    }
                })
            })
        })
        await renderWithProvider(ItemDetailsTableInBound, propsInbound);
        const component = getTestElement('itemdetailsinbound');
        expect(component).toBeInTheDocument();
    });

    it('Inbound item details (Empty Result)', async () => {

        fetch.mockImplementation(() => {
            return new Promise((resolve, reject) => {
                return resolve({
                    status: 200,
                    json: () => {
                        return false
                    }
                })
            })
        })
        await renderWithProvider(ItemDetailsTableInBound, propsInbound);
        const component = getTestElement('itemdetailsinbound');
        expect(component).toBeInTheDocument();
    });

    it('Inbound item details ( Null/Undefined/EmptyObject )', async () => {

        const orderLine = {
            orderExecutionHealth: "RED",
            "materialNum": undefined, "custMaterialNum": "", "materialGroupNum": null, "itemDesc": "SCOTT,BR CAS,TWL,-,6 PK,96", "netOrderValue": 13470.03, "orderQty": 0, "qtyUom": "CS", "itemPosNum": "000011", "qtyDiscrepancy": "", loadedQty: "", differenceQty: "qw",
            "deliveryLines": [{ "deliveryLineId": "0814794467-000010", "batchNum": " ", "deliveryNum": "814794467", "batchMgmtReq": "N" }]
        }
        fetch.mockImplementation(() => {
            return new Promise((resolve, reject) => {
                return resolve({
                    status: 200,
                    json: () => {
                        return { ...propsInbound.data, orderItems: [...propsInbound.data.orderItems, orderLine] }
                    }
                })
            })
        })
        await renderWithProvider(ItemDetailsTableInBound, propsInbound);
        const component = getTestElement('itemdetailsinbound');
        expect(component).toBeInTheDocument();
    });

    it('Outbound item details  should render', async () => {

        fetch.mockImplementation(() => {
            return new Promise((resolve, reject) => {
                return resolve({
                    status: 200,
                    json: () => {
                        return propsOutbound.data
                    }
                })
            })
        })

        await renderWithProvider(ItemDetailsTableOutBound, propsOutbound);
        const component = getTestElement('itemdetailsoutbound');
        expect(component).toBeInTheDocument();
    });

    it('Outbound item details (Empty Result)', async () => {

        fetch.mockImplementation(() => {
            return new Promise((resolve, reject) => {
                return resolve({
                    status: 200,
                    json: () => {
                        return false
                    }
                })
            })
        })

        await renderWithProvider(ItemDetailsTableOutBound, propsOutbound);
        const component = getTestElement('itemdetailsoutbound');
        expect(component).toBeInTheDocument();
    });

    it('Outbound item details ( Null/Undefined/EmptyObject )', async () => {

        const orderLine = { orderExecutionHealth: "RED", "materialNum": undefined, "custMaterialNum": null, "materialGroupNum": "", "itemDesc": "COTT FRSH FMW RFL 42", "netOrderValue": 22246.96, "orderedQty": -10, "confirmedQty": -10, "qtyUom": "CS", "itemPosNum": "000011", "qtyDiscrepancy": "", differenceQty: "qa", loadedQty: 100 }


        fetch.mockImplementation(() => {
            return new Promise((resolve, reject) => {
                return resolve({
                    status: 200,
                    json: () => {
                        return { ...propsOutbound.data, orderItems: [...propsOutbound.data.orderItems, orderLine] }
                    }
                })
            })
        })

        await renderWithProvider(ItemDetailsTableOutBound, propsOutbound);
        const component = getTestElement('itemdetailsoutbound');
        expect(component).toBeInTheDocument();
    });


    it("Outbound Columns Render Test - Case 1", () => {
        const data = {
            materialNum: 111,
            qtyUom: "cs",
            orderedQty: 100,
            confirmedQty: 200,
            loadedQty: 100,
            differenceQty: 100,
            deliveryBlockCode: "Test",
            matAvailDate: "11-11-11",
        };

        const mockResult = (cols, data) => {
            return cols.columnConfiguration(data);
        }
        expect(mockResult(outColumns, data)["deliveryBlockCode"]).toBe("Test");
        expect(mockResult(outColumns, {})["deliveryBlockCode"]).toBe("-");

    });
    it("In Columns Render Test - Case 1", () => {
        const data = {
            materialNum: 111,
            qtyUom: "cs",
            loadedQty: 100,
        };

        const mockResult = (cols, data) => {
            return cols.columnConfiguration(data);
        }
        expect(mockResult(inColumns, data)["loadedQty"]).toBe("100 cs");
        expect(mockResult(inColumns, {})["materialNum"]).toBe("-");

    });

});