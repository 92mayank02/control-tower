import CustomersChipComponent from '../components/header/CustomersChipComponent';
import { getTestElement, renderWithProvider } from '../testUtils';
import { mockStoreValue } from "../components/dummydata/mock"
import { renderWithMockStore } from 'testUtils';
import { fireEvent } from '@testing-library/react';


describe('<Customers Chip Component>', () => {
  afterAll(() => {
    jest.restoreAllMocks();
  })

  const props1 = {
    "chipsObjectArray": [
        {
            "customerName": "AMAZON.COM EDI",
            "selectionType": "CUST",
            "distributionChannel": "80",
            "salesOrg": "2811"
        }
    ],
    "action": () => {
        return {
            type:"REMOVE_CUSO_SUCCESS",
            payload:{}
        }
    }
}

const props2 = {
    "chipsObjectArray": [
        {
            "customerName": "AMAZON.COM EDI",
            "selectionType": "SALES_OFFICE",
            "distributionChannel": "80",
            "salesOrg": ["2811"]
        }
    ],
    "action": () => {
        return {
            type:"REMOVE_CUSO_SUCCESS",
            payload:{}
        }
    }}
 

const props3 = {
        "chipsObjectArray": [
            {
                "value": "Z01",
                "reducerObject": {
                    "salesOffice": "DY01",
                    "selectionType": "SALES_OFFICE",
                    "salesOrg": "2821",
                    "distributionChannel": "80"
                }
            },
            {
                "value": "Z04",
                "reducerObject": {
                    "salesOffice": "DY01",
                    "selectionType": "SALES_OFFICE",
                    "salesOrg": "2821",
                    "distributionChannel": "80"
                }
            },
        ],
        "action": () => {
            return {
                type:"REMOVE_CUSO_SUCCESS",
                payload:{}
            }
        }    }

  it('should render with label', async () => {
    const { container } = await renderWithMockStore(CustomersChipComponent, props1, mockStoreValue );
    const component = getTestElement('customerchips');
    expect(component).toBeInTheDocument();
    expect(component).toBeTruthy();
    const foo = container.querySelector('[data-testid="chip"] > svg')
    fireEvent.click(foo)
  });

  it('should render with label', async () => {
    const { container } = await renderWithMockStore(CustomersChipComponent, props2, mockStoreValue );
    const component = getTestElement('customerchips');
    expect(component).toBeInTheDocument();
    expect(component).toBeTruthy();
    const foo = container.querySelector('[data-testid="chip"] > svg')
    fireEvent.click(foo)
  });

  it('should render with label', async () => {
    const { container } = await renderWithMockStore(CustomersChipComponent, props3, mockStoreValue );
    const component = getTestElement('customerchips');
    expect(component).toBeInTheDocument();
    expect(component).toBeTruthy();
    const foo = container.querySelector('[data-testid="chip"] > svg')
    fireEvent.click(foo)
  });

  
  // it('Test Click', async () => {
  //   const { getByTestId } =  await renderWithMockStore(SalesGroupSearchLisiting, props ) 
  //   expect(getByTestId("ctLink")).toBeInTheDocument();
  //   fireEvent.click(getByTestId("ctLink"));
  // });

});