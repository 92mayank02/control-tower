import Button from '../components/common/Button';
import { getTestElement, renderWithProvider } from '../testUtils';


describe('<Button>', () => {
    afterAll(() => {
      jest.restoreAllMocks();
    });
  
    const ButtonProps = {children:'TestDetails'}
    it('should render with label', async () => {
      await renderWithProvider(Button,ButtonProps);
      const component = getTestElement('button');
      expect(component).toBeInTheDocument();
      expect(component).toBeTruthy();
    });

});