import TabPanel from '../components/header/TabPanel';
import { getTestElement, renderWithProvider } from '../testUtils';


describe('<TabPanel>', () => {
    afterAll(() => {
      jest.restoreAllMocks();
    });
  
    const props = { value: 0, index: 0};
    it('should render with label', async () => {
      await renderWithProvider(TabPanel,props);
      const breadcrumbComponent = getTestElement('tabpanel');
      expect(breadcrumbComponent).toBeInTheDocument();
    });

});