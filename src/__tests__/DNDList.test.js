import DNDList from 'components/common/Table/TableElements/DNDList';
import { getTestElement, renderWithMockStore, renderWithProvider } from '../testUtils';
import { fireEvent } from '@testing-library/react';

// jest.mock('react-beautiful-dnd', () => ({
//     Droppable: ({ children }) => children({
//         draggableProps: {
//             style: {},
//         },
//         innerRef: jest.fn(),
//     }, {}),
//     Draggable: ({ children }) => children({
//         draggableProps: {
//             style: {},
//         },
//         innerRef: jest.fn(),
//     }, {}),
//     DragDropContext: ({ children }) => children,
// }));

function simulateEvent(component, eventName, eventData) {
    const eventType = (() => {
      if (['mousedown', 'mousemove', 'mouseup'].includes(eventName)) { return 'MouseEvent'; }
    })();
    const event = new window[eventType](eventName, eventData);
    component.getDOMNode().dispatchEvent(event);
  }


describe('<DNDList Component>', () => {

    afterAll(() => {
        jest.restoreAllMocks();
    });

    let props = {
        setHiddenCols: jest.fn(),
        changeOrder: jest.fn(),
        items: [
            { title: 'Column 1', field: "column1", cellStyle: jest.fn(), hidden: true },
            { title: 'Column 2', field: "column2", cellStyle: jest.fn() , hidden: false }
        ],
    }

    it('DNDList should render', async () => {
        await renderWithMockStore(DNDList, props);
        const component = getTestElement('dndlist');
        expect(component).toBeInTheDocument();
        expect(component).toBeTruthy();
    });

    it('DNDList visibilityoff Click Test', async () => {
        await renderWithMockStore(DNDList, props);
        const icon = await document.querySelector('[name="visibilityoff"]');
        const test = icon.dispatchEvent(new MouseEvent('click', { bubbles: true }));
        expect(test).toBeTruthy();
    });

    it('DNDList visibilityon Click Test', async () => {
        await renderWithMockStore(DNDList, props);
        const icon = await document.querySelector('[name="visibilityon"]');
        const test = icon.dispatchEvent(new MouseEvent("click", { bubbles: true }))
        expect(test).toBeTruthy();
    });


});