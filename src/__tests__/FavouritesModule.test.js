import FavouritesModule, { FavBlock } from 'components/header/FavouritesModule';
import { getTestElement, renderWithMockStore, fireEvent } from '../testUtils';
import useMediaQuery from '@material-ui/core/useMediaQuery';
jest.mock('@material-ui/core/useMediaQuery');

jest.mock('../components/header/BusinessFilter', () => {
  return {
    __esModule: true,
    A: true,
    default: () => {
      return <div></div>;
    }
  };
});

describe('<FavModule Component>', () => {
  afterAll(() => {
    jest.restoreAllMocks();
  });

  let props = {
    items: ['PROFESSIONAL'],
    businessUnits: [],
    favCuso: { 
      FAV_CUST: [{"selectionType": "CUST", "salesOrg": ["2810"], "distributionChannel": "80", "customerName": "Amazon"}], 
      FAV_SALES_OFFICE: [{"selectionType": "SALES_OFFICE", "salesOrg": "2810", "distributionChannel": "80", "salesOffice": "CS01"}]
    },
    setBusinessService: jest.fn(),
    setFavCUSOService: jest.fn()
  };

  it('BusinessFilter Should Render - desktop', async () => {
    await renderWithMockStore(FavouritesModule, props);
    const component = getTestElement('favmodule');
    expect(component).toBeTruthy();
  });

  it('Favourites tab', async () => {
    await renderWithMockStore(FavBlock, props);
    const component = getTestElement('favblock');
    expect(component).toBeTruthy();
  });

  it('Fav UnFav check', async () => {
    const { getAllByTestId } = await renderWithMockStore(
      FavouritesModule,
      props
    );
    const component = getTestElement('favmodule');
    expect(component).toBeTruthy();
    fireEvent.click(getAllByTestId('deleteFavBU')[0]);
    fireEvent.click(getAllByTestId('deleteFavLoc')[0]);
    fireEvent.click(getAllByTestId('deleteFavCust')[0]);
    fireEvent.click(getAllByTestId('deleteFavSO')[0]);
  });

  it('No Favourites - mobile', async () => {
    useMediaQuery.mockImplementation(() => {
      return {
        __esModule: true,
        default: () => {
          return true;
        }
      };
    });
    await renderWithMockStore(FavouritesModule, props, { ...props, favorites: { businessUnits: [], favorites: [], favCuso:{ FAV_CUST: [], FAV_SALES_OFFICE: [] } }});
    const component = getTestElement('favmodule');
    expect(component).toBeTruthy();
  });
});
