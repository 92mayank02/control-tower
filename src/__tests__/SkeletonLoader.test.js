import SkeletonLoader from '../components/common/SkeletonLoader';
import { getTestElement, renderWithProvider } from '../testUtils';


describe('<Skeleton Loader>', () => {
    afterAll(() => {
      jest.restoreAllMocks();
    });
  
    const props = {dimensions:{height:0,width:0}}
    it('should render with label', async () => {
      await renderWithProvider(SkeletonLoader,props);
      const component = getTestElement('skeletonLoader');
      expect(component).toBeInTheDocument();
    });

});