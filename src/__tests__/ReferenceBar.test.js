import ReferenceBar from '../components/D3Charts/ReferenceBar';
import { getTestElement, renderWithProvider } from '../testUtils';
import { scaleOrdinal } from 'd3'
import { waitFor } from '@testing-library/react';


document.createRange = () => ({
  setStart: () => { },
  setEnd: () => { },
  commonAncestorContainer: {
    nodeName: "BODY",
    ownerDocument: document,
  },
})


describe('<ReferenceBar>', () => {
  afterAll(() => {
    jest.restoreAllMocks();
  });

  class xScale {
    constructor(x) {
      this.x = x;
      return 1000
    }
  }

  var _old = xScale;
  xScale = function (...args) {
    return new _old(...args)
  };

  xScale.bandwidth = function () {
    return 2000;
  }

  const BarsProps = {
    xAccessor: jest.fn(),
    margins: {
      top: 10,
      right: 10,
      bottom: 40,
      left: 100,
      margin: 10
    },
    height: 160,
    colors: jest.fn(),
    setDetails: jest.fn(),
    horizontal: false,
    keys: ["blueCount", "yellowCount", "redCount"],
    xKey: "statedesc",
    data: [
      {
        "state": "TRANS_EXEC_READY_PICK_UP",
        "stateDesc": "Ready for pickup",
        "totalCount": 10,
        "redCount": 20,
        "yellowCount": 10,
        "blueCount": 30,
        "total": 60, 
        "test": 100
      },
      {
        "state": "TRANS_EXEC_IN_TRANSIT",
        "stateDesc": "In Transit",
        "totalCount": 10,
        "redCount": 20,
        "blueCount": 30,
        "yellowCount": 10,
        "total": 60,
        "test": 100
      },
      {
        "state": "TRANS_EXEC_DELIVERY_CONFIRMED",
        "stateDesc": "Delivery Confirmed",
        "totalCount": 10,
        "redCount": 10,
        "yellowCount": 10,
        "blueCount": 10,
        "total": 30,
        "test": 30
      }
    ],
    type: "network",
    subtype: "TRANS_PLAN",
    scales: {
      xScale: xScale,
      yScale: jest.fn()
    }
  }

  const yProps = {
    ...BarsProps,
    horizontal: true,
    refLine: "test",
    svgDimensions: {
        height: 100,
        width: 100
    },
    scales: {
      xScale: xScale,
      yScale: xScale
    }
  }


  it('should render with label - yScale', async () => {
    await renderWithProvider(ReferenceBar, yProps);
    const component = getTestElement('referencebars');
    waitFor(()=>component)
    expect(component).toBeInTheDocument();
    expect(component).toBeTruthy();
  });

});