import { ThemeProvider } from '@material-ui/core/styles';
import { Provider } from 'react-redux'
import { testTheme } from './theme';
import { act, fireEvent, render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom'

import React from 'react';
//Donot remove this unused import 
import regeneratorRuntime from "regenerator-runtime";
import { mockStoreValue } from "./components/dummydata/mock"
import { Route, MemoryRouter, Switch } from 'react-router-dom';

import configureMockStore from 'redux-mock-store'
import { shallow } from 'enzyme';
import thunk from 'redux-thunk';


const mockStore = configureMockStore([thunk])
export const store = mockStore(mockStoreValue)

export const shallowSetup = (Element, props) => {
  const enzymeWrapper = shallow(
    <ThemeProvider theme={testTheme}>
      <Element {...props} />
    </ThemeProvider>
  );
  return enzymeWrapper;
}

export const renderWithProvider = async (Element, props = {}) => {

  jest.useFakeTimers();

  const wrapper = render(
    <ThemeProvider theme={testTheme}>
      <MemoryRouter  initialEntries={["/dashboard/NETWORK"]}  ><Switch><Route path="/dashboard" render={(routeProps) => <Element {...props} {...routeProps}/>}/></Switch></MemoryRouter>    
    </ThemeProvider>
  );

  await waitFor(() => {
    jest.runAllImmediates();
  });

  return wrapper;
};


export const renderWithMockStore = async (Element, props = {}, customStoreValue = null, initialEntries="/dashboard/NETWORK") => {

  let customStore = store;

  if (customStoreValue) {
    customStore = mockStore(customStoreValue)
  }

  jest.useFakeTimers();

  const wrapper = render(
    <Provider store={customStore}>
      <ThemeProvider theme={testTheme}>
      <MemoryRouter initialEntries={[initialEntries]}  ><Switch><Route path={"/dashboard"} render={(routeProps) => <Element {...props} {...routeProps}/>}/></Switch></MemoryRouter>    
      </ThemeProvider>
    </Provider>
  );

  await waitFor(() => {
    jest.runAllImmediates();
  });

  return wrapper;
};

export const reRenderWithMockStore = async (Element, props = {}, customStoreValue = null) => {

  let customStore = store;

  if (customStoreValue) {
    customStore = mockStore(customStoreValue)
  }

  jest.useFakeTimers();

  const wrapper =
    <Provider store={customStore}>
      <ThemeProvider theme={testTheme}>
      <MemoryRouter  initialEntries={["/dashboard/NETWORK"]}  ><Switch><Route path="/dashboard" render={(routeProps) => <Element {...props} {...routeProps}/>}/></Switch></MemoryRouter>    
      </ThemeProvider>
    </Provider>


  await waitFor(() => {
    jest.runAllImmediates();
  });

  return wrapper;
};

export const initiateGlobalAuthService = () => {
  global.authService = {
    getIdToken: () => {
      return "AAAA"
    },
    getAuthState: () => {
      return {
        isAuthenticated: true
      }
    }
  };
}

export const getTestElement = (name, index = 0) => screen.queryAllByTestId(name)[index];



// Re-export everything
export * from '@testing-library/react';

// Override render method
export { act, fireEvent, screen, waitFor, userEvent, render };
