import React from 'react';
import { Switch, Redirect } from 'react-router-dom';
import { Route, withRouter } from 'react-router-dom';
import { Security, LoginCallback, SecureRoute } from '@okta/okta-react';
import OktaConfig from "./configs/OktaConfig";
import MainLayout from './theme/layouts/MainLayout';
import UpdateCSTRanking from './theme/layouts/UpdateCSTRanking';

const PageNotFound = () => <div><h1>Page Not Found</h1></div>

const Routes = (props) => {
  return (
    <Security {...OktaConfig}>
      <Switch>
        <Redirect
          exact
          from="/"
          to="/dashboard/NETWORK"
        />
        <Redirect
          exact
          from="/dashboard"
          to="/dashboard/NETWORK"
        />
        <Route path='/callback' component={LoginCallback} />
        <SecureRoute path='/dashboard' render={(props)=> <MainLayout {...props}/>} />
        <SecureRoute path='/updateCSTRanking' component={UpdateCSTRanking} />
        <Route path='/not-found' component={PageNotFound} />
        <Redirect to="/not-found" />
      </Switch>
    </Security>
  );
}

export default withRouter(Routes);