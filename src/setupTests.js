import '@testing-library/jest-dom/extend-expect';
import * as lodash from "lodash";

// Enzyme Setup
global._ = lodash;

global.document.createRange = () => ({
    setStart: () => {},
    setEnd: () => {},
    commonAncestorContainer: {
      nodeName: 'BODY',
      ownerDocument: document,
    },
});

window.matchMedia = window.matchMedia || function() {
  return {
      matches: false,
      addListener: function() {},
      removeListener: function() {}
  };
};

const { configure } = require('enzyme');
const ReactSixteenAdapter = require('enzyme-adapter-react-16');
configure({ adapter: new ReactSixteenAdapter() });

process.env.REACT_APP_VIEWLIMIT = 20

