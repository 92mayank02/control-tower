import { tableoptions } from "./tableStyleOverride"
import { endpoints } from "../configs/endpoints"
import * as _ from "lodash";
import { v4 as uuid } from 'uuid';
import { startCase, get, uniq, flatten, sortBy, isEmpty, isArray } from "lodash";
import { appTheme } from "../theme";
import { performanceConstants } from "components/common/PerformanceFilters"
import moment from "moment-timezone";

const calcPercentage = (a, b) => {
    let percent = ((Number(a) - Number(b)) / Number(a) * 100);
    if (!isNaN(percent) && isFinite(percent)) {
        return parseFloat(percent.toFixed(0))
    } else {
        return 0;
    }
}

const generateDefaultData = (columns) => {
    let defaultData = {};
    columns.map(d => {
        defaultData[d.field] = "-";
    });
    return defaultData;
}

const msToHMS = (ms) => {
    if (!isNaN(ms)) {
        const seconds = ms / 1000;
        const hours = parseFloat(seconds / 3600).toFixed(2);
        return `${hours} Hours`;
    } else {
        return "-"
    }
}

const refreshTable = (tableRef) => {
    tableRef.current && tableRef.current.onQueryChange();
}

export const sortOrder = ({ order, key, data }) => {
    return _.sortBy(data, (obj) => {
        return _.indexOf(order, obj[key]);
    });
}

const generateTablebody = (filters, sites = null) => {
    let tablebody = {};
    try {
        Object.keys(filters).map(key => {
            const filter = filters[key];
            const checkvalues = [];
            const generic = ["text", "radio", "date", "sites", "checkboxradio", "ordercheckbox", "textcheck"];

            if (generic.includes(filter.type)) {
                tablebody[key] = filter.data;
                if (filter.type === "sites") {

                    const sitesFromFilter = filter.data || []
                    const sitesFromFavorites = sites || []
                    let commonSites = []

                    if (sitesFromFavorites.length > 0) {
                        commonSites = sitesFromFilter?.filter((site) => sitesFromFavorites.includes(site))
                    }
                    const consolidateFilters = sitesFromFavorites.length > 0 ? commonSites.length > 0 ? commonSites : sitesFromFavorites : sitesFromFilter;

                    tablebody[key] = consolidateFilters.length > 0 ? consolidateFilters : null

                }
            }
            if (filter.type === "checkbox" || filter.type === "ordercheckbox") {
                filter.data.map(d => {
                    if (d.checked) {
                        checkvalues.push(d.value);
                    }
                });
                tablebody[key] = checkvalues;
            }

            if (filter.type === "checkboxradio") {
                let crValue = {};
                filter.data.map(d => {
                    if (d.checked) {
                        crValue = d.value;
                    }
                });
                tablebody[key] = crValue;
            }
        });
    }
    catch (e) {
        tablebody = {};
    }

    return tablebody || {};
}

const formatToSentenceCase = (inputString) => {
    const regexFunction = (string) => { return string.replace(/[a-z]/i, function (letter) { return letter.toUpperCase(); }).trim() }
    const splitString = inputString.toLowerCase().split(" ").map((string) => {
        if (string === 'dc') {
            return string.toUpperCase();
        } else {
            return string.split('-').map(regexFunction).join('-')
        }
    });
    return splitString.join(" ");
}

const generateFilterLayoutTemplate = ({ viewName, filters = {}, columns = [] }) => {
    return {
        id: uuid(),
        name: viewName ? viewName : `Untitled View`,
        default: false,
        filters,
        columns
    }
}

const getStatusData = (data, key = "hours") => {

    if (data[key] > 3) {
        return {
            ...data,
            status: performanceConstants.HEALTHY,
            color: `${appTheme.palette.legendColors.healthyGreen}`
        }
    } else if (data[key] > 0 && data[key] <= 3) {
        return {
            ...data,
            status: performanceConstants.MONITOR, color: `${appTheme?.palette?.legendColors.risk}`
        }
    } else if (data[key] < 0 && data[key] > -6) {
        return {
            ...data,
            status: performanceConstants.BEHIND,
            color: `${appTheme.palette.legendColors.pink}`
        }
    } else if (data[key] <= -6) {
        return {
            ...data,
            status: performanceConstants.CRITICAL,
            color: `${appTheme.palette.legendColors.criticalRed}`
        }
    } else
        return {
            ...data,
            status: performanceConstants.NODATA,
            color: `${appTheme.palette.legendColors.greyLight}`
        }
}


const capitalizeWord = (value) => startCase(value?.toLowerCase())

const getId = (obj, type, showSG = true) => {
    const datatype = obj[type];

    if (datatype === "DC" || datatype === "MILL") {
        return obj.siteNum
    }
    if (datatype === "SALES_OFFICE") {
        return `${obj.selectionType}_${obj.salesOrg}_${obj.distributionChannel}_${obj.salesOffice}_${showSG ? obj.salesGroup || '' : ''}`
    }
    if (datatype === "CUST") {
        return `${obj.selectionType}_${obj.salesOrg}_${obj.distributionChannel}_${obj.customerName}`
    }
}

const getTabsData = (data, viewbyValues) => {
    const { name, type } = viewbyValues;
    const items = [];
    const tempData = data?.forEach(d => {
        if ("salesGroup" in d) {
            return d.salesGroup?.map(item => {
                if (item) {
                    items.push({
                        ...d,
                        customUniqObjectId: getId(d, type, false),
                        custom: {
                            name: `${d[name]}${d?.salesOrg && `(${d.salesOrg}) `}${item || ''}`,
                            id: getId({ ...d, salesGroup: item }, type),
                            salesGroup: d.salesGroup
                        },
                        salesGroup: [item]
                    })
                }
            })
        } else {
            return items.push({
                ...d,
                customUniqObjectId: getId(d, type, false),
                custom: {
                    name: !d.customerName ? `${d[name]}${d?.salesOrg ? `(${d.salesOrg})` : ''}` : `${d.customerName}${d?.salesOrg ? `(${d.salesOrg})` : ''}`,
                    id: getId(d, type, false)
                }
            })
        }
    });
    return items;
}


const generateHeader = ({ selections, type, showTabsBy, item }) => {

    let siteNums = [];
    let customerOrSalesOffice = [];

    if (type === "network") {
        return {};
    }

    if (type === "mylocation") {
        siteNums = selections?.locations?.map(d => d?.siteNum);
        customerOrSalesOffice = selections?.customer;
    } else {
        if (showTabsBy === "customer") {
            siteNums = selections?.locations?.map(d => d?.siteNum);
            customerOrSalesOffice = [item]?.map(d => {
                delete d?.custom;
                delete d?.customUniqObjectId;
                return d;
            });
        } else {
            siteNums = [item]?.map(d => d?.siteNum);
            customerOrSalesOffice = selections?.customer;
        }
    }

    return {
        siteNums,
        customerOrSalesOffice
    }
}

const defaultShowTabsBy = (state) => {
    const { locations, customer } = { locations: get(state, "items.items", []), customer: [...(get(state, "items.cuso.CUST", [])), ...(get(state, "items.cuso.SALES_OFFICE", []))] }

    let showTabsBy = locations.length > 0 && customer.length > 0 ? "customer" : locations.length > 0 && customer.length === 0 ? "locations" : customer.length > 0 ? "customer" : "locations";

    let stateShowTabsBy = get(state, "options.showTabsBy", showTabsBy);

    if (!(locations.length > 0 && customer.length > 0)) {
        if (stateShowTabsBy === "locations" && locations.length === 0) {
            stateShowTabsBy = "customer"
        }

        if (stateShowTabsBy === "customer" && customer.length === 0) {
            stateShowTabsBy = "locations"
        }
    }

    return stateShowTabsBy;
}

const mergeSalesGroups = (inputArray) => {

    const map = new Map(inputArray.map(({ customUniqObjectId, ...rest }) => [customUniqObjectId, { ...rest, salesGroup: [] }]));
    for (let { customUniqObjectId, salesGroup } of inputArray) {
        // map.get(customUniqObjectId).salesGroup.push(...[salesGroup].flat());
        map.get(customUniqObjectId).salesGroup.push(...flatten([salesGroup]));
    }

    let resultantArray = [...map.values()];

    resultantArray = resultantArray.map(d => {
        if (d?.salesGroup && d.salesGroup[0] === undefined && d.salesGroup.length === 1) {
            delete d?.salesGroup;
        } else {
            d.salesGroup = uniq(d?.salesGroup)
        }
        delete d.custom;
        delete d?.customUniqObjectId;
        return d
    });
    return resultantArray;
}

const stringToArray = (input) => {
    // https://regex101.com/  -> To validate the regex
    if (isArray(input)) return input;
    const makeArray = input?.trim().split(/,| /).filter(item => item.match(/^[a-z0-9-_()"\/]+$/i)) || []
    return isEmpty(makeArray) ? null : makeArray
}

const getUpdatedColumns = (viewColumns = [], defaultColumns = []) => {


    let viewFieldsObject = {};
    viewColumns.map(d => {
        viewFieldsObject[d.field] = d;
    });
    const viewFieldKeys = Object.keys(viewFieldsObject);
    let updatedColumns = [];
    let changedCols = [];

    defaultColumns.map(d => {
        if (viewFieldKeys.includes(d.field)) {
            updatedColumns.push(viewFieldsObject[d.field]);
        } else {
            changedCols.push({
                ...d,
                hidden: true
            });
        }
    });

    let sortedCollection = sortBy(updatedColumns, (item) => viewFieldKeys.indexOf(item.field));

    return [...sortedCollection, ...changedCols]
}

const createHyperLinkFilterObject = ({ pageDetails, tableName, filterBody }) => {

    let preSetHealthFilter = {}

    const preSetStatusFilter = pageDetails?.filterParams?.args ? Object.keys(pageDetails.filterParams.args).reduce((res, key) => {
        return {
            ...res, [key]: {
                ...filterBody[key],
                data: isArray(filterBody[key]?.data) ? filterBody[key]?.data.map(filterOption => {
                    return pageDetails.filterParams.args[key].includes(filterOption.value) ? { ...filterOption, checked: true } : filterOption
                }) : pageDetails.filterParams.args[key]
            }
        }
    }, {}) : {};

    if (tableName === 'orders') {

        preSetHealthFilter = pageDetails?.filterParams?.health ? {
            orderHealth: {
                ...filterBody.orderHealth,
                data: filterBody.orderHealth.data.map(status => { return status.value === pageDetails?.filterParams?.health ? { ...status, checked: true } : status })
            }
        } : {}

        var skipDefaultDateFilter = pageDetails?.filterParams?.args ? { matAvailDate: { ...filterBody.matAvailDate, data: null } } : {}

        var skipDefaultOrderTypeFilter = pageDetails?.filterParams?.args?.orderTypes ? {
            orderTypes: {
                ...filterBody.orderTypes,
                data: filterBody.orderTypes.data.map(type1 => {
                    return pageDetails.filterParams.args.orderTypes.includes(type1.value) ? { ...type1, checked: true } : { ...type1, checked: false }
                })
            }
        } : {}

    }

    if (tableName === 'transport') {

        preSetHealthFilter = {
            orderExecutionHealth: {
                ...filterBody.orderExecutionHealth,
                data: filterBody.orderExecutionHealth.data.map(status => { return status.value === pageDetails?.filterParams?.health ? { ...status, checked: true } : status })
            }
        }
        skipDefaultDateFilter = pageDetails?.filterParams?.args ? { loadReadyDateTimeOriginTZ: { ...filterBody.loadReadyDateTimeOriginTZ, data: null } } : {}

    }

    if (tableName === 'distoutbound') {

        preSetHealthFilter = {
            orderExecutionHealth: {
                ...filterBody.orderExecutionHealth,
                data: filterBody.orderExecutionHealth.data.map(status => { return status.value === pageDetails?.filterParams?.health ? { ...status, checked: true } : status })
            }
        }

    }

    if (tableName === 'distinbound') {
        preSetHealthFilter = pageDetails?.filterParams?.health ? {
            inboundOrderExecutionHealth: {
                ...filterBody.inboundOrderExecutionHealth,
                data: filterBody.inboundOrderExecutionHealth.data.map(status => { return status.value === pageDetails?.filterParams?.health ? { ...status, checked: true } : status })
            }
        } : {}
    }

    return { fullObject: { ...preSetStatusFilter, ...preSetHealthFilter, ...skipDefaultDateFilter, ...skipDefaultOrderTypeFilter }, preSetStatusFilter }

}

const searchKey = (key, d) => {
    key = key?.toLowerCase();
    return (d?.shortName?.toLowerCase().indexOf(key) > -1)
        || (d?.siteNum?.toLowerCase().indexOf(key) > -1)
        || (d?.alias?.toLowerCase().indexOf(key) > -1)
}

export const processGuardRailData = (data = [], optionalName=false) => {
    const processed = [];
    data.map((d, i) => {
        let barname = `${moment(d.dateTime).format("MM/DD")} ${moment(d.dateTime).format("ddd")}`;
        if (d.today == "Y") {
            barname = `${barname} Today`
        }
        let values = {
            ...d,
            name: barname
        };

        const rowDataOrder = ['CONSUMER', 'PROFESSIONAL', 'ALL']

        rowDataOrder.forEach((value)=>{
            const findBU = d?.orderBusinessUnit.filter(({businessUnit})=> businessUnit === value)
            if(isEmpty(findBU)){
                values[`${value}_countFL`] = .001
                values[`${value}_countLTL`] = .001
                values[`${value}_guardrailFL`] = .001
                values[`${value}_guardrailLTL`] = .001;
            }else{
                values[`${value}_countFL`] = findBU[0].countFL + .001
                values[`${value}_countLTL`] = findBU[0].countLTL + .001
                values[`${value}_guardrailFL`] = findBU[0].guardrailFL + .001
                values[`${value}_guardrailLTL`] = findBU[0].guardrailLTL + .001;
            }            
        })
        processed.push(values);

    });
    return processed;
}

export {
    getUpdatedColumns,
    formatToSentenceCase,
    generateTablebody,
    msToHMS,
    tableoptions,
    refreshTable,
    calcPercentage,
    generateDefaultData,
    endpoints,
    generateFilterLayoutTemplate,
    getStatusData,
    capitalizeWord,
    getId,
    getTabsData,
    generateHeader,
    defaultShowTabsBy,
    mergeSalesGroups,
    stringToArray,
    createHyperLinkFilterObject,
    searchKey
}