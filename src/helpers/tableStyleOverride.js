import { appTheme } from 'theme';

const tableoptions = {
  detailPanelColumnStyle: {
    width: '3%'
  },
  headerStyle: {
    backgroundColor: appTheme?.palette.card.base,
    color: appTheme?.palette.white,
    borderBottom: `1px solid ${appTheme?.palette.white}`,
    textTransform: "uppercase",
    padding: appTheme?.spacing(2),
    paddingRight: appTheme?.spacing(2),
    paddingLeft: appTheme?.spacing(2),
    textAlign:'left',
    ...appTheme?.typography.body2
  },
  rowStyle: {
      ...appTheme?.typography.body1,
      flexGrow: 1,
      color : appTheme?.palette.white,
  },
  draggable: false,
  sorting: false,
  search: false,
  pageSize: 10,
  showFirstLastPageButtons: true,
  showTitle: false,
  pageSizeOptions: [5, 10, 20, 50, 100],
  toolbar: false,
  maxBodyHeight: "63vh",
}

const shortTitleCellStyle = {
  minWidth: appTheme?.spacing(20),  
}

const longTitleCellStyle = {
  minWidth: appTheme?.spacing(40),  
}

const stickyColumn = {
  position:"sticky",
  left:0,
  zIndex:14,
  boxShadow:`1px 0px 4px 0px ${appTheme?.palette.divider}`
}

const stickyHeaderCell = {
  ...stickyColumn,
  zIndex:16,
  top:0
}

const stickyBodyCell = {
  ...stickyColumn,
  backgroundColor:appTheme?.palette.primary.base
}




export {
  tableoptions,
  shortTitleCellStyle,
  longTitleCellStyle,
  stickyColumn,
  stickyBodyCell,
  stickyHeaderCell
}