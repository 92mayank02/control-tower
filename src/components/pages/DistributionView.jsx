import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Typography, Grid, makeStyles, useMediaQuery, useTheme } from '@material-ui/core';
import Card from "../common/Card";
import { DistributionOutboundSCChartBuilder } from 'components/common/DistributionOutboundSCChartBuilder';
import { DistributionInboundChartBuilder } from 'components/common/DistributionInboundChartBuilder';
import DistributionTabs from "../common/DistributionTabs";
import { Breadcrumb } from '../common/Breadcrumb';
import { tabTitleEnum } from '../common/DistributionTabs'
import { viewStyles } from "theme"
import ChartComponent from "../common/ChartComponent";
import ChartCarouselComponent from "../common/ChartCarouselComponent";
import { DistributionOutboundCDChartBuilder } from 'components/common/DistributionOutboundCDChartBuilder';
import { DistributionOutboundLSChartBuilder } from 'components/common/DistributionOutboundLSChartBuilder';
import { DistributionOutboundRFPChartBuilder } from 'components/common/DistributionOutboundRFPChartBuilder';
//SCR Code <--STARTS-->
// import { chartDataIdentifierForAPI } from "../../theme/layouts/MainLayout";
// import { intersection } from "lodash";
// import {saveFilters} from "reduxLib/services";
// import stockConstraintReportFilter from "reduxLib/constdata/stockConstraintReportFilters"
//SCR Code <--ENDS-->
const useStyles = makeStyles(viewStyles);

export const formatColumnData = (chartsData, keys, type) => {
  let data = [];
  const item = chartsData?.find(d => d.state === type) || {};

  keys.map((key) => {
    data.push({
      name: key.name,
      redCount: item[key.red] || 0,
      blueCount: (item[key.green]) || 0,
      totalCount: item[key.total] || 0,
      state: `${type}_${key.name}`
    });
  })

  return data;
}

export const columnKeys = [
  { red: "redCountSO", green: "greenCountSO", total: "totalCountSO", name: "CUST" },
  { red: "redCountSTO", green: "greenCountSTO", total: "totalCountSTO", name: "STO" },
];

export const rfpColumns = [
  {
    red: "redCountCustPickUp", green: "greenCountCustPickUp", total: "totalCountCustPickUp", name: "Cust Pickup",
  },
  ...columnKeys
]


const DistributionView = ({ name, type, globalFilterSelection, setDetails, setActiveTopNavTab, ...rest }) => {
  const classes = useStyles();
  const pageTitle = "Distribution"
  const height = 160;
  const margins = { top: 30, right: 10, bottom: 40, left: 50, margin: 10 };
  const isMobile = useMediaQuery(useTheme().breakpoints.down("sm"));
  //SCR Code <--STARTS-->
  // const dispatch = useDispatch();
  // const singleLocation = /^\d+$/.test(type);
  //SCR Code <--ENDS-->

//Guardrails Code below it
  // const notification = useSelector(({guardrails}) => guardrails.notification)
  // const disableButton = useSelector(({guardrails}) => guardrails.disableButton)
  // const isAuthorized = useSelector(({guardrails}) => guardrails.allSiteGuardrails.allowedToEdit)
  // const [open, setOpen] = React.useState(false);
  // const [snack, setSnack] = React.useState({
  //   open: false,
  //   severity: "info",
  //   message: null
  // })
  // const showGuardrailsUpdateDialog = async() => {
  //   if(!isAuthorized){
  //     setSnack({
  //       open: true,
  //       severity: "error",
  //       message: "You are not authorized to edit the guardrails"    
  //     });
  //   }
  //   setOpen(true)
  // }

  // const shipmentStatusDetails = [{
  //   name: "Truckload shipments per day",
  //   componentName: DistributionShipmentsChartBuilder,
  //   showLegends: true,
  //   guardrails: true,
  //   config: {
  //     showLegends: true,
  //     guardrails: true,
  //     keys: ["CONSUMER_countFL", "PROFESSIONAL_countFL", "ALL_countFL"],
  //     xKey: "name",
  //     refLine: "totalGuardrailFL",
  //     subRefLines: ["CONSUMER_guardrailFL", "PROFESSIONAL_guardrailFL", "ALL_guardrailFL"]
  //   }
  // },
  // {
  //   name: "Less than Truckload shipments per day",
  //   componentName: DistributionShipmentsChartBuilder,
  //   showLegends: false,
  //   guardrails: true,
  //   blockApi: true,
  //   config: {
  //     showLegends: true,
  //     guardrails: true,
  //     keys: ["CONSUMER_countLTL", "PROFESSIONAL_countLTL", "ALL_countLTL"],
  //     xKey: "name",
  //     refLine: "totalGuardrailLTL",
  //     subRefLines: ["CONSUMER_guardrailLTL", "PROFESSIONAL_guardrailLTL", "ALL_guardrailLTL"]
  //   }
  // }]

  // const handleClose = (event, reason) => {
  //   if (reason === 'clickaway') {
  //     return;
  //   }
  //   setSnack({
  //     ...snack,
  //     open: false
  //   });
  // };

  // useEffect(()=>{
  //  dispatch(getAllGuardrails())
  // },[])

  // useEffect(()=>{
  //   if(!isEmpty(notification))setSnack({open:true,...notification})
  // },[notification])
  //Guardrails Code above it



  const chartComponentDetails = [{
    name: `${isMobile ? "Outbound - " : ""}Shipment Created`,
    componentName: DistributionOutboundSCChartBuilder,
    showLegends: true,
    subtype: tabTitleEnum.outbound
  },
  {
    name: `${isMobile ? "Outbound - " : ""}Checked in by DC`,
    componentName: DistributionOutboundCDChartBuilder,
    showLegends: false,
    subtype: tabTitleEnum.outbound,
    blockApi: true
  },
  {
    name: `${isMobile ? "Outbound - " : ""}Loading Started`,
    componentName: DistributionOutboundLSChartBuilder,
    showLegends: false,
    subtype: tabTitleEnum.outbound,
    blockApi: true
  },
  {
    name: `${isMobile ? "Outbound - " : ""}Ready for Pickup`,
    componentName: DistributionOutboundRFPChartBuilder,
    showLegends: false,
    subtype: tabTitleEnum.outbound,
    blockApi: true
  },
  {
    name: `${isMobile ? "Inbound - " : ""}Shipment Status`,
    componentName: DistributionInboundChartBuilder,
    showLegends: false,
    subtype: tabTitleEnum.inbound,
    sitesKey: "destSites"
  }]

  // const shipmentStatusDetails = [{
  //   name: "Truckload shipments per day",
  //   componentName: DistributionShipmentsChartBuilder,
  //   showLegends: true,
  //   guardrails: true,
  //   config: {
  //     showLegends: true,
  //     guardrails: true,
  //     keys: ["CONSUMER_countFL", "PROFESSIONAL_countFL", "ALL_countFL"],
  //     xKey: "name",
  //     refLine: "totalGuardrailFL",
  //     subRefLines: ["CONSUMER_guardrailFL", "PROFESSIONAL_guardrailFL", "ALL_guardrailFL"]
  //   }
  // },
  // {
  //   name: "Less than Truckload shipments per day",
  //   componentName: DistributionShipmentsChartBuilder,
  //   showLegends: false,
  //   guardrails: true,
  //   blockApi: true,
  //   config: {
  //     showLegends: true,
  //     guardrails: true,
  //     keys: ["CONSUMER_countLTL", "PROFESSIONAL_countLTL", "ALL_countLTL"],
  //     xKey: "name",
  //     refLine: "totalGuardrailLTL",
  //     subRefLines: ["CONSUMER_guardrailLTL", "PROFESSIONAL_guardrailLTL", "ALL_guardrailLTL"]
  //   }
  // }]

  // const handleClose = (event, reason) => {
  //   if (reason === 'clickaway') {
  //     return;
  //   }
  //   setSnack({
  //     ...snack,
  //     open: false
  //   });
  // };

  // useEffect(()=>{
  //  dispatch(getAllGuardrails())
  // },[])

  // useEffect(()=>{
  //   if(!isEmpty(notification))setSnack({open:true,...notification})
  // },[notification])

  //SCR Code <--STARTS-->
  // const goToStockConstraint = () => {
  //   dispatch(saveFilters({
  //     type,
  //     subtype: 'stockConstraintReport',
  //     filter: stockConstraintReportFilter
  //   }))
  //   setDetails(type, chartDataIdentifierForAPI.stockConstraintReport, true, {
  //     navHistory: [{ id: intersection(Object.values(chartDataIdentifierForAPI), Object.keys(rest.pageDetails))[0], label: pageTitle, type }].filter(({ id }) => id !== chartDataIdentifierForAPI.network)
  //   })
  // }
  //SCR Code <--ENDS-->
  return (
    <div className={classes.separator} data-testid='distributionContainer'>
      <Breadcrumb setDetails={setDetails} type={type} label={pageTitle} />
      {/* //SCR Code <--STARTS-->
      {singleLocation && <div data-testid="scReport" className={classes.scrLinkbutton} onClick={goToStockConstraint}>
        Go to stock constraint report
      </div>}
      //SCR Code <--ENDS--> */}
      <Card
        cardtitle={
          <>
            <Typography variant="h1" color="primary">
              {`LIVE STATUS`}
            </Typography>
            <Grid container className={classes.extendTitle} spacing={2} justify="space-between" >
              <Grid item sm={8} >
                <Typography variant="h1" color="primary">
                  {`Outbound`}
                </Typography>
              </Grid>
              <Grid item sm={4} >
                <Typography variant="h1" color="primary">
                  {`Inbound`}
                </Typography>
              </Grid>
            </Grid>
          </>
        }
      >
        <ChartCarouselComponent chartsArray={
          chartComponentDetails && chartComponentDetails.map((chart) => (
            <Grid item xs={12} md={4} lg={3} className={classes.addPadding}>
              <ChartComponent blockApi={chart.blockApi} sitesKey={chart.sitesKey} showLegends={chart.showLegends} innercard divider BuilderComponent={chart.componentName} {...rest} name={chart.name} setDetails={setDetails} globalFilterSelection={globalFilterSelection} type={type} subtype={chart.subtype} height={height} margins={margins} />
            </Grid>
          ))
        }
        />
      </Card>

      {/* GUARDRAILS CODE
      {singleLocation === true && <><Card
        cardtitle={<Grid container justify='space-between'>
          <Grid item>
              <Typography variant="h1" color="primary">
              {`SHIPMENT STATUS`}
              </Typography>
            </Grid>
            <Grid item>
              <Button disabled={disableButton} data-testid='button_fetch' onClick={showGuardrailsUpdateDialog} color='primary' variant='outlined'>View / Edit Guardrails</Button>
            </Grid>
          </Grid>
        }
      >
      <ChartCarouselComponent chartsArray={
          shipmentStatusDetails && shipmentStatusDetails.map((chart) => (
            <Grid item xs={12} md={12} lg={6} className={classes.addPadding}>
              <GuardrailChartComponent
                chartConfig={chart.config}
                blockApi={chart.blockApi} showLegends={chart.showLegends} innercard
                BuilderComponent={chart.componentName} {...rest} name={chart.name} setDetails={setDetails}
                globalFilterSelection={globalFilterSelection} type={type}
                height={height} margins={margins} guardrails={chart.guardrails} 
              />
            </Grid>
          ))
        }
        />
      </Card>
      <Modal
        className={classes.modalBody}
        open={open}
        onClose={() => setOpen(false)}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <UpdateGuardrails onClose={() => setOpen(false)} />
      </Modal>
      </>
      } */}

      <Card>
        <DistributionTabs pageTitle={pageTitle} setActiveTopNavTab={setActiveTopNavTab} {...rest} globalFilterSelection={globalFilterSelection} setDetails={setDetails} type={type} />
      </Card>

      {/*GUARDRAILS CODE 
      <Snack {...snack} handleClose={handleClose} /> */}
    </div>
  );
}

export default DistributionView;