import React from 'react';
import { Grid, Typography, makeStyles } from '@material-ui/core';
import ChartComponent from "../common/ChartComponent";
import { NetworkOrderChartBuilder } from 'components/common/NetworkOrderChartBuilder';
import Card from "../common/Card"
import TableComponent from "../common/TableComponent";
import ItemDetailsTableOutBound, { columns as outBoundDetailsColumns } from "../common/ItemDetailsTableOutBound";
import { defaultNetworkFilterArgs } from "../../reduxLib/constdata/filters"
import { tableoptions } from '../../helpers/tableStyleOverride';
import { endpoints } from "helpers";
import { uniq } from "lodash";
import { defaultNetworkColumns } from "../../reduxLib/constdata/networkColumns";
import networkfilters from "../../reduxLib/constdata/networkFilters";
import { viewStyles } from "theme"
import { NetworkChartBuilder } from "../common/NetworkChartBuilder"
import PerformanceMap from "../common/PerformanceMap";
import LocationPerformance from "../common/LocationPerformance";
import ChartCarouselComponent from "../common/ChartCarouselComponent";

const useStyles = makeStyles(viewStyles);

export const conditionalFilterReplacement = (newfilters) => {

  if (newfilters?.orderExecutionBucket?.includes("TRANS_PLAN_PROCESSING")) {
    const transporcessing = ["TRANS_PLAN_PLANNED", "TRANS_PLAN_TENDERED", "TRANS_PLAN_TENDER_REJECTED"]
    newfilters.orderExecutionBucket = uniq([...newfilters.orderExecutionBucket, ...transporcessing]).filter(d => d !== "TRANS_PLAN_PROCESSING");
  }

  if (newfilters?.orderStatusBucket?.includes("ORDER_BLOCKED")) {
    const transporcessing = ["SO_BLOCKED_NON_TMS_PLANNED_IMMEDIATE_ACTION",
      "SO_BLOCKED_TMS_PLANNED_VISIBLE",
      "SO_BLOCKED_TMS_PLANNED_NOT_VISIBLE",
      "SO_BLOCKED_NON_TMS_PLANNED_PICKUP_NOT_SCHEDULED",
      "SO_BLOCKED_NON_TMS_PLANNED_PICKUP_MULTI_BLOCK",
      "SO_BACK_ORDER_BLOCKED"
    ];
    newfilters.orderStatusBucket = uniq([...newfilters.orderStatusBucket, ...transporcessing]).filter(d => d !== "ORDER_BLOCKED");
  }

  if (newfilters?.orderStatusBucket?.includes("ORDER_BLOCK_FREE")) {
    const transporcessing = ["SO_COMPLETELY_CONFIRMED_CUBE", "SO_NOT_COMPLETELY_CONFIRMED_CUBE", "SO_BACK_ORDER_BLOCK_FREE"];
    newfilters.orderStatusBucket = uniq([...newfilters.orderStatusBucket, ...transporcessing]).filter(d => d !== "ORDER_BLOCK_FREE");
  }

  return newfilters
}

export const tableStyleConfig = {
  ...tableoptions,
  rowStyle: (d) => {
    const healthStatusBucket = d.orderHealthAndExecutionHealth ? d.orderHealthAndExecutionHealth : "";
    return {
      ...tableoptions.rowStyle,
      borderLeft: healthStatusBucket === "RED" ? '5px solid #FF4081' : healthStatusBucket === "YELLOW" ? '5px solid #F5B023' : ''
    }
  },
}


const NetworkView = ({ name, type, globalFilterSelection, setDetails, prevPageDetails, ...rest }) => {
  const height = 160;
  const margins = { top: 10, right: 10, bottom: 40, left: 30, margin: 10 };
  const tableRef = React.createRef();
  const singleLocation =  /^\d+$/.test(type);

  const classes = useStyles();

  return (
    <div className={classes.separator} data-testid="networkview">
      <Card
        cardtitle={
          <Typography variant="h1" color="primary">
            {`Live Status`}
          </Typography>
        }
      >

        <ChartCarouselComponent chartsArray={[
          <Grid item xs={12} md={6} lg={3} className={classes.addPadding}>
            <ChartComponent showLegends showViewDetails  {...rest} BuilderComponent={NetworkOrderChartBuilder} name="ORDER MANAGEMENT" height={height} margins={margins} setDetails={setDetails} globalFilterSelection={globalFilterSelection} type={type} subtype="ORDER" />
          </Grid>,
          <Grid item xs={12} md={6} lg={3} className={classes.addPadding}>
            <ChartComponent showViewDetails  {...rest} height={height} margins={margins} BuilderComponent={NetworkChartBuilder} name="TRANSPORTATION PLANNING" setDetails={setDetails} globalFilterSelection={globalFilterSelection} type={type} subtype="TRANS_PLAN" />
          </Grid>,
          <Grid item xs={12} md={6} lg={3} className={classes.addPadding}>
            <ChartComponent showViewDetails  {...rest} height={height} margins={margins} BuilderComponent={NetworkChartBuilder} name="DISTRIBUTION" globalFilterSelection={globalFilterSelection} setDetails={setDetails} type={type} subtype="DIST" />
          </Grid>,
          <Grid item xs={12} md={6} lg={3} className={classes.addPadding}>
            <ChartComponent showViewDetails  {...rest} height={height} margins={margins} BuilderComponent={NetworkChartBuilder} name="TRANSPORTATION EXECUTION" globalFilterSelection={globalFilterSelection} setDetails={setDetails} type={type} subtype="TRANS_EXEC" charttype="horizontal" />
          </Grid>]}
        />

      </Card>
      {
        (type === "network" || type === "mylocation") && 
        <PerformanceMap setDetails={setDetails} type={type} {...rest} />
      }
      {
        singleLocation === true && <LocationPerformance setDetails={setDetails} type={type} {...rest}/>
      }

      <Card>
        <TableComponent {...rest}
          pageTitle={"Network"}
          prevPageDetails={prevPageDetails}
          globalFilterSelection={globalFilterSelection}
          setDetails={setDetails}
          tableName="network"
          title="OUTBOUND ORDER DETAILS"
          excludeArray={["searchStringList"]}
          searchTextPlaceholder="Enter Shipment#, Order#, Delivery #"
          type={type} tableRef={tableRef}
          ItemDetailSection={ItemDetailsTableOutBound}
          itemDetailsColumns={outBoundDetailsColumns}
          defaultFilterArgs={defaultNetworkFilterArgs}
          columns={defaultNetworkColumns}
          conditionalFilterReplacement={conditionalFilterReplacement}
          filterType="networkfilters"
          fetchEndPoint={endpoints.table.network}
          tableStyleConfig={tableStyleConfig}
          filterBody={networkfilters}
        />
      </Card>
    </div>
  );
}

export default NetworkView;
