import React from "react";
import { Grid, Typography, makeStyles } from "@material-ui/core";
import NetworkView from '../../components/pages/NetworkView';
import TransportView from '../../components/pages/TransportView';
import DistributionView from '../../components/pages/DistributionView';
import ShipmentDetails from '../../components/common/ShipmentDetails';
//import StockConstraintReport from '../../components/common/StockConstraintReport';
import OrdersView from '../../components/pages/OrdersView';
import AnalyticsReport from 'components/common/AnalyticsReport';
import Timer from "components/common/Helpers/Timer";
import { mainLayoutStyles } from 'theme';
import { isEqual } from "lodash";
import { useAppContext } from "theme/layouts/AppContext";
import { Route } from "react-router-dom";
const useStyles = makeStyles(mainLayoutStyles);


export const chartDataIdentifierForAPI = { network: 'NETWORK', order: 'ORDER', transPlan: 'TRANS_PLAN', transExec: 'TRANS_EXEC', dist: 'DIST', shipmentdetails: 'SHIPMENTDETAILS', analytics: 'ANALYTICS' }
export const PageRouter = ({ type, prevPageDetails, globalRefresh, setDetails, setActiveTopNavTab, pageDetails, perfTableRedirection, match, ...props }) => {

    const classes = useStyles();
    const { globalFilterSelection } = props;
    const { siteNums, customerOrSalesOffice } = globalFilterSelection || {};
    const [refresh, setRefresh] = React.useState({ refresh: false, time: new Date(), siteNums, customerOrSalesOffice });
    const { setRef } = useAppContext();
  
    React.useEffect(() => {
      if ((siteNums?.length !== refresh.siteNums?.length || customerOrSalesOffice?.length !== refresh.customerOrSalesOffice?.length || !isEqual(customerOrSalesOffice, refresh.customerOrSalesOffice))) {
        setRefresh({
          refresh: !refresh.refresh,
          time: new Date(),
          siteNums,
          customerOrSalesOffice
        });
      }
    }, [siteNums, customerOrSalesOffice]);
  
    React.useEffect(() => {
      const interval = setInterval(() => {
        setRefresh({
          refresh: !refresh.refresh,
          time: new Date(),
          siteNums,
          customerOrSalesOffice
        });
      }, 300000);
      return () => clearInterval(interval);
    }, [refresh.refresh]);
  
    const customProps = {
      prevPageDetails,
      pageDetails,
      type,
      siteNums,
      setDetails,
      setRefresh,
      globalFilterSelection,
      refresh: refresh.refresh,
      setRef
    }

    console.log(match)
    return (
      <Grid className={classes.mainLayoutContainer}>
        <Typography data-testid='getviews' className={classes.timer}>
          <Timer date={refresh.time} />
        </Typography>
          <Route exact path={`${match.path}/${chartDataIdentifierForAPI.network}`} render={(props) => <NetworkView perfTableRedirection={perfTableRedirection} {...customProps} {...props} />} />
          <Route exact path={`${match.path}/${chartDataIdentifierForAPI.order}`} render={(props) => <OrdersView {...customProps} {...props} />} />
          <Route exact path={`${match.path}/${chartDataIdentifierForAPI.transPlan}`} render={(props) => <TransportView {...customProps} {...props} />} />
          <Route exact path={`${match.path}/${chartDataIdentifierForAPI.transExec}`} render={(props) => <TransportView {...customProps} {...props} />} />
          <Route exact path={`${match.path}/${chartDataIdentifierForAPI.dist}`} render={(props) => <DistributionView setActiveTopNavTab={setActiveTopNavTab} {...customProps} {...props} />} />
          <Route exact path={`${match.path}/${chartDataIdentifierForAPI.ShipmentDetails}`} render={(props) => <ShipmentDetails setActiveTopNavTab={setActiveTopNavTab} {...customProps} {...props} />} />
          {/* <Route exact path={`${match.path}/${chartDataIdentifierForAPI.stockConstraintReport}`} render={(props) => <StockConstraintReport setActiveTopNavTab={setActiveTopNavTab} {...customProps} {...props} />} /> */}
          <Route exact path={`${match.path}/${chartDataIdentifierForAPI.analytics}`} render={(props) => <AnalyticsReport pageDetails={pageDetails} {...props} />} />
      </Grid>
    )
  
  }