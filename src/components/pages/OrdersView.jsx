import React, { useEffect, useState } from 'react';
import { Typography, Grid, Hidden, makeStyles, Collapse } from '@material-ui/core';
import Card from "../common/Card";
import Expander from 'components/common/Elements/Expander';
import TableComponent from "../common/TableComponent";
import ItemDetailsTableOutBound, { columns as outBoundDetailsColumns } from "../common/ItemDetailsTableOutBound";
import { Breadcrumb } from '../common/Breadcrumb';
import { defaultOrderFilterArgs } from "../../reduxLib/constdata/filters"
import { tableoptions } from '../../helpers/tableStyleOverride';
import { endpoints } from "helpers";
import { uniq, isEmpty } from "lodash";
import ordersFilters from '../../reduxLib/constdata/ordersFilters';
import { viewStyles } from "theme"
import ChartComponent from "../common/ChartComponent";
import ChartCarouselComponent from "../common/ChartCarouselComponent";
import { BlockedFreeOrderTreeMapBuilder } from "../common/BlockedFreeOrderTreeMapBuilder";
import { BackOrderDetailsChartBuilder } from "../common/BackOrderDetailsChartBuilder";
import { BlockedNonTmsPlannedChartBuilder } from 'components/common/BlockedNonTmsPlannedChartBuilder';
import { BlockedTmsPlannedChartBuilder } from 'components/common/BlockedTmsPlannedChartBuilder';
import { defaultOrderColumns } from "../../reduxLib/constdata/orderColumns";
import { useSelector } from 'react-redux'

const useStyles = makeStyles(viewStyles);

export const conditionalFilterReplacement = (newfilters) => {
  if (newfilters?.orderExecutionBucket?.includes("TRANS_PLAN_PROCESSING")) {
    const transporcessing = ["TRANS_PLAN_PLANNED", "TRANS_PLAN_TENDERED", "TRANS_PLAN_TENDER_REJECTED"]
    newfilters.orderExecutionBucket = uniq([...newfilters.orderExecutionBucket, ...transporcessing]).filter(d => d !== "TRANS_PLAN_PROCESSING");
  }
}

export const tableStyleConfig = {
  ...tableoptions,
  rowStyle: (d) => {
    const healthStatusBucket = d.orderHealth ? d.orderHealth : "";
    return {
      ...tableoptions.rowStyle,
      borderLeft: healthStatusBucket === "RED" ? '5px solid #FF4081' : healthStatusBucket === "YELLOW" ? '5px solid #F5B023' : ''
    }
  },
}

const OrdersView = ({ name, type, globalFilterSelection, setDetails, prevPageDetails, ...rest }) => {
  const pageTitle = "Order Management"
  const classes = useStyles();
  const height = 160;
  const margins = { top: 30, right: 10, bottom: 40, left: 50, margin: 10 };
  const tableRef = React.createRef();
  
  const favBusinessUnits = useSelector(({ favorites }) => favorites?.tempBusinessUnit || [])
  const [showBackOrderStatusCard, setShowBackOrderStatusCard] = useState(isEmpty(favBusinessUnits) || favBusinessUnits.includes("PROFESSIONAL") ? true : false)
  const [expand, setExpand] = React.useState(false);
  const blockedOrderChartComponentDetails = [
    {
      name: "BLOCKED TMS PLANNED",
      componentName: BlockedTmsPlannedChartBuilder,
      subtype: "SO_BLOCKED",
      showLegends: false,
    },
    {
      name: "BLOCKED NON TMS PLANNED",
      componentName: BlockedNonTmsPlannedChartBuilder,
      subtype: "SO_BLOCKED",
      showLegends: false,
      blockApi:true
    }
  ]
  useEffect(() => {
    setShowBackOrderStatusCard(isEmpty(favBusinessUnits) || favBusinessUnits.includes("PROFESSIONAL") ? true : false)
  }, [favBusinessUnits])
  
  return (
    <div className={classes.separator} data-testid="ordersview">
      <Breadcrumb setDetails={setDetails} type={type} label={pageTitle} />
      <Card
        cardtitle={
          <Typography variant="h1" color="primary">
            {`Live Status`}
          </Typography>
        }
      >

        {/* Desktop view */}
        <Hidden smDown>
          <Grid container className={classes.grid} spacing={2}>
            <Grid className={classes.grid} item md={6} lg={6}>
              <ChartComponent showLegends innercard divider BuilderComponent={BlockedFreeOrderTreeMapBuilder} {...rest} name="ORDERS BLOCK FREE" setDetails={setDetails} globalFilterSelection={globalFilterSelection} type={type} subtype="SO_BLOCK_FREE" height={height} margins={margins} />
            </Grid>
            {blockedOrderChartComponentDetails && blockedOrderChartComponentDetails.map((component) => 
              <Grid className={classes.grid} item md={6} lg={3} >
                <ChartComponent blockApi={component.blockApi} innercard BuilderComponent={component.componentName} {...rest} name={component.name} globalFilterSelection={globalFilterSelection} setDetails={setDetails} type={type} subtype={component.subtype} height={height} margins={margins} />
              </Grid>
            )}
          </Grid>
          {showBackOrderStatusCard && <><Collapse in={expand}>
            <Grid className={classes.grid} container spacing={2}>
               <Grid className={classes.marginTop} item md={4} lg={4}>
                <ChartComponent innercard divider BuilderComponent={BackOrderDetailsChartBuilder} {...rest} name="BACK ORDERS" globalFilterSelection={globalFilterSelection} setDetails={setDetails} type={type} subtype="SO_BACK_ORDER" height={height} margins={margins} />
              </Grid>
            </Grid>
          </Collapse>
          <Expander expand={expand} change={setExpand} /></>}
        </Hidden>

        {/* Mobile view */}
        <Hidden mdUp>
          <ChartCarouselComponent chartsArray={[
            <Grid item xs={12} sm={12}>
              <ChartComponent innercard divider BuilderComponent={BlockedFreeOrderTreeMapBuilder} {...rest} name="ORDERS BLOCK FREE" setDetails={setDetails} globalFilterSelection={globalFilterSelection} type={type} subtype="SO_BLOCK_FREE" height={height} margins={margins} />
            </Grid>,
            blockedOrderChartComponentDetails && blockedOrderChartComponentDetails.map((component) => 
              <Grid item xs={12} sm={12}>
                <ChartComponent blockApi={component.blockApi} innercard BuilderComponent={component.componentName} {...rest} name={component.name} globalFilterSelection={globalFilterSelection} setDetails={setDetails} type={type} subtype={component.subtype} height={height} margins={margins} />
            </Grid>
            ),
            showBackOrderStatusCard && 
              <Grid item xs={12} sm={12} >
                <ChartComponent innercard divider BuilderComponent={BackOrderDetailsChartBuilder} {...rest} name="BACK ORDERS" globalFilterSelection={globalFilterSelection} setDetails={setDetails} type={type} subtype="SO_BACK_ORDER" height={height} margins={margins} />
              </Grid>
            ]}
          />
        </Hidden>
      </Card>
      <Card >
        <TableComponent {...rest}
          pageTitle={pageTitle}
          setDetails={setDetails}
          prevPageDetails={prevPageDetails}
          globalFilterSelection={globalFilterSelection}
          tableName="orders"
          title="Orders"
          excludeArray={["searchStringList", "matAvailDate", "creditOnHold"]}
          searchTextPlaceholder="Enter Shipment#, Order#, Customer PO#, Delivery #"
          type={type}
          tableRef={tableRef}
          ItemDetailSection={ItemDetailsTableOutBound}
          itemDetailsColumns={outBoundDetailsColumns}
          defaultFilterArgs={defaultOrderFilterArgs}
          columns={defaultOrderColumns}
          conditionalFilterReplacement={conditionalFilterReplacement}
          filterBody={ordersFilters}
          filterType="ordersfilters"
          topDateFilter={{
            title: "Material Availability Date",
            key: "matAvailDate"
          }}
          fetchEndPoint={endpoints.table.orders}
          tableStyleConfig={tableStyleConfig}
        />
      </Card>
    </div>
  );
}

export default OrdersView;
