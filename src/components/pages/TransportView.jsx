import React from 'react';
import { Typography, Grid, makeStyles, Collapse, Hidden } from '@material-ui/core';
import Card from "../common/Card";
import { PlanningDetailsChartBuilder } from 'components/common/PlanningDetailsChartBuilder';
import { AppointmentDetailsChartBuilder } from 'components/common/AppointmentDetailsChartBuilder';
import { SuspendedDetailsChartBuilder } from 'components/common/SuspendedDetailsChartBuilder';
import { DeliveryDetailsChartBuilder } from 'components/common/DeliveryDetailsChartBuilder';
import transportFilters from '../../reduxLib/constdata/transportFilters';
import { Breadcrumb } from '../common/Breadcrumb';
import { defaultTransportFilterArgs } from "../../reduxLib/constdata/filters"
import { tableoptions } from '../../helpers/tableStyleOverride';
import { endpoints } from "helpers";
import { get, uniq } from "lodash";
import TableComponent from "../common/TableComponent";
import TransportDetails, { columns as transportDetailsColumns } from "../common/Table/TransportDetails";
import { viewStyles } from "theme"
import ChartComponent from "../common/ChartComponent";
import { defaultTransportColumns } from "../../reduxLib/constdata/transportColumns"
import ChartCarouselComponent from "../common/ChartCarouselComponent";
import { ShipmentProcessingChartBuilder } from 'components/common/ShipmentProcessingChartBuilder';
import { CarrierCommittedChartBuilder } from 'components/common/CarrierCommittedChartBuilder';
import Expander from 'components/common/Elements/Expander';

const useStyles = makeStyles(viewStyles);

export const conditionalFilterReplacement = (newfilters) => {
  if (get(newfilters, "orderExecutionBucket", []).length === 0) {
    newfilters.orderExecutionBucket = defaultTransportFilterArgs.orderExecutionBucket;
  }

  if (newfilters?.orderExecutionBucket?.includes("TRANS_PLAN_PROCESSING")) {
    const transporcessing = ["TRANS_PLAN_PLANNED", "TRANS_PLAN_TENDERED", "TRANS_PLAN_TENDER_REJECTED"]
    newfilters.orderExecutionBucket = uniq([...newfilters.orderExecutionBucket, ...transporcessing]).filter(d => d !== "TRANS_PLAN_PROCESSING");
  }
}

export const tableStyleConfig = {
  ...tableoptions,
  rowStyle: (d) => {
    const healthStatusBucket = d.orderExecutionHealth ? d.orderExecutionHealth : "";
    return {
      ...tableoptions.rowStyle,
      borderLeft: healthStatusBucket === "RED" ? '5px solid #FF4081' : healthStatusBucket === "YELLOW" ? '5px solid #F5B023' : ''
    }
  },
}

const TransportView = ({ name, type, globalFilterSelection, setDetails, ...rest }) => {

  const pageTitle = "Transportation"
  const classes = useStyles();
  const height = 160;
  const margins = { top: 30, right: 10, bottom: 40, left: 50, margin: 10 };
  const [expand, setExpand] = React.useState(false);
  const tableRef = React.createRef();

  const chartComponentDetails = [
    {
      name: "PLANNING STATUS",
      componentName: PlanningDetailsChartBuilder,
      subtype: "TRANS_DETAIL_PLAN",
      showLegends: true,
    },
    {
      name: "SHIPMENT PROCESSING",
      componentName: ShipmentProcessingChartBuilder,
      subtype: "TRANS_DETAIL_PLAN",
      showLegends: false,
      blockApi: true
    },
    {
      name: "CARRIER COMMITTED",
      componentName: CarrierCommittedChartBuilder,
      subtype: "TRANS_DETAIL_PLAN",
      showLegends: false,
      blockApi: true
    },
    {
      name: "TRANSPORTATION EXECUTION STATUS",
      componentName: DeliveryDetailsChartBuilder,
      subtype: "TRANS_DETAIL_EXEC",
      showLegends: false
    },
    {
      name: "SUSPENDED STATUS",
      componentName: SuspendedDetailsChartBuilder,
      subtype: "TRANS_DETAIL_SUSPENDED_STATUS",
      showOnExpand: true,
      showLegends: false
    },
    {
      name: "APPOINTMENT STATUS",
      componentName: AppointmentDetailsChartBuilder,
      subtype: "TRANS_DETAIL_APPT_STATUS",
      showOnExpand: true,
      showLegends: false
    },
  ]

  return (
    <div className={classes.separator} data-testid="transportview">
      <Breadcrumb setDetails={setDetails} type={type} label={pageTitle} />
      <Card
        cardtitle={
          <Typography variant="h1" color="primary">
            {`Transportation Details`}
          </Typography>
        }
      >
        {/* Desktop view */}
        <Hidden smDown>
          <Grid container className={classes.grid} spacing={2}>
            {chartComponentDetails && chartComponentDetails.map((chartType) => (
              !chartType.showOnExpand && <Grid item md={6} lg={3}>
                <ChartComponent blockApi={chartType.blockApi} showLegends={chartType.showLegends} innercard divider BuilderComponent={chartType.componentName} {...rest} name={chartType.name} setDetails={setDetails} globalFilterSelection={globalFilterSelection} type={type} subtype={chartType.subtype} height={height} margins={margins} />
              </Grid>
            ))}
          </Grid>
          <Collapse in={expand}>
            <Grid className={classes.grid} container spacing={2}>
              {chartComponentDetails && chartComponentDetails.map((chartType) => (
                chartType.showOnExpand && <Grid className={classes.marginTop} item md={6} lg={4}>
                  <ChartComponent showLegends={chartType.showLegends} innercard divider BuilderComponent={chartType.componentName} {...rest} name={chartType.name} setDetails={setDetails} globalFilterSelection={globalFilterSelection} type={type} subtype={chartType.subtype} height={height} margins={margins} />
                </Grid>
              ))}
            </Grid>
          </Collapse>
          <Expander expand={expand} change={setExpand} />
        </Hidden>

        {/* Mobile view */}
        <Hidden mdUp>
          <ChartCarouselComponent chartsArray=
            {chartComponentDetails && chartComponentDetails.map((chartType) => (
              <Grid item xs={12} sm={12}>
                <ChartComponent blockApi={chartType.blockApi} showLegends={chartType.showLegends} innercard divider BuilderComponent={chartType.componentName} {...rest} name={chartType.name} setDetails={setDetails} globalFilterSelection={globalFilterSelection} type={type} subtype={chartType.subtype} height={height} margins={margins} />
              </Grid>
            ))}
          />
        </Hidden>
      </Card>
      <Card >
        <TableComponent {...rest}
          pageTitle={pageTitle}
          globalFilterSelection={globalFilterSelection}
          tableName="transport"
          title="TRANSPORT"
          excludeArray={["searchStringList", "loadReadyDateTimeOriginTZ"]}
          searchTextPlaceholder="Enter Shipment#, Order#, Trailer id, Delivery#"
          type={type}
          tableRef={tableRef}
          ItemDetailSection={TransportDetails}
          itemDetailsColumns={transportDetailsColumns}
          defaultFilterArgs={defaultTransportFilterArgs}
          columns={defaultTransportColumns}
          conditionalFilterReplacement={conditionalFilterReplacement}
          filterBody={transportFilters}
          filterType="transportfilters"
          topDateFilter={{
            title: "Carrier Ready Date",
            key: "loadReadyDateTimeOriginTZ"
          }}
          fetchEndPoint={endpoints.table.transport}
          tableStyleConfig={tableStyleConfig}
          setDetails={setDetails}
        />
      </Card>
    </div>
  );
}

export default TransportView;
