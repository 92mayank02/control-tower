export const transplan = [
    {
        "state": "TRANS_PLAN_TO_BE_PLANNED",
        "stateDesc": "Shipment Created",
        "type": 'red',
        "totalCount": 1000,
        "blueCount": 300,
        "redCount": 1304,
    },
    {
        "state": "TRANS_PLAN_TO_BE_PLANNED",
        "stateDesc": "Checked In by DC",
        "type": 'blue',
        "totalCount": 1000,
        "blueCount": 300,

        "redCount": 1281
    },
    {
        "state": "TRANS_PLAN_PLANNING",
        "stateDesc": "Loading Started",
        "type": "red",
        "totalCount": 1000,
        "blueCount": 300,

        "redCount": 2039,
    }
];

export const OutboundItemDetailsMock = { "orderNum": "67326984", "customerNotifiedDateTime": [], "deliveryApptConfirmedDateTimeList": [], "originRegion": "NA", "orderItems": [{ "materialNum": "104949800", "custMaterialNum": "104949800", "materialGroupNum": "048", "itemDesc": "HUG,LTL MOVR,DPR,GIGAJR,S5,60", "netOrderValue": 30724.2, "orderQty": 4500.0, "differenceQty": 100, "qtyUom": "CS", "itemPosNum": "00010", "qtyDiscrepancy": "UNKNOWN", "loadedQty": 4500.0, "deliveryLines": [{ "deliveryLineId": "0814112326-000010", "batchNum": " ", "deliveryNum": "814112326", "loadedQty": 4500.0, "batchMgmtReq": "N" }] }, { "materialNum": "70021962", "custMaterialNum": "70021962", "materialGroupNum": "099", "itemDesc": "PALLET, CHEP US BLOCK", "netOrderValue": 0.0, "orderQty": 30.0, "qtyUom": "EA", "itemPosNum": "00011", "qtyDiscrepancy": "UNKNOWN", "loadedQty": 30.0, "deliveryLines": [{ "deliveryLineId": "0814112326-000020", "batchNum": " ", "deliveryNum": "814112326", "loadedQty": 30.0, "batchMgmtReq": "N" }] }] }

export const mockStoreValue = {
    "sites": {
        "loading": false,
        "salesOffice": [{ "salesOffice": "CS01", "salesOfficeName": null, "salesGroupList": ["Z01", "Z02", "Z03", "Z04", "Z05", "Z06", "Z07", "Z08", "Z09", "Z10", "Z11", "Z12", "Z13", "Z14", "Z15", "Z16", "Z17", "Z18", "Z19", "Z20"], "salesOrgList": [{ "salesOrg": "2820", "distributionChannel": "80" }, { "salesOrg": "2810", "distributionChannel": "80" }] }, { "salesOffice": "CS02", "salesOfficeName": null, "salesGroupList": ["Z01", "Z02", "Z03", "Z04", "Z05", "Z06", "Z07", "Z08", "Z09", "Z10", "Z11", "Z12", "Z13", "Z14", "Z15", "Z16", "Z17", "Z18", "Z19", "Z20"], "salesOrgList": [{ "salesOrg": "2820", "distributionChannel": "80" }, { "salesOrg": "2810", "distributionChannel": "80" }, { "salesOrg": "2810", "distributionChannel": "90" }] }, { "salesOffice": "CS03", "salesOfficeName": null, "salesGroupList": ["Z01", "Z02", "Z03", "Z04", "Z05", "Z06", "Z07", "Z08", "Z09", "Z10", "Z11", "Z12", "Z15", "Z16", "Z17", "Z18", "Z19", "Z20", "Z21", "Z22", "Z23", "Z24", "Z25"], "salesOrgList": [{ "salesOrg": "2820", "distributionChannel": "80" }, { "salesOrg": "2810", "distributionChannel": "80" }, { "salesOrg": "2811", "distributionChannel": "90" }] }, { "salesOffice": "CS04", "salesOfficeName": null, "salesGroupList": ["Z01", "Z02", "Z03", "Z04", "Z05", "Z06", "Z07", "Z08", "Z09", "Z10", "Z11", "Z12", "Z13", "Z14", "Z15", "Z16", "Z17", "Z18", "Z19", "Z20"], "salesOrgList": [{ "salesOrg": "2820", "distributionChannel": "80" }, { "salesOrg": "2810", "distributionChannel": "80" }] }, { "salesOffice": "CS05", "salesOfficeName": null, "salesGroupList": ["Z01", "Z02", "Z03", "Z04", "Z05", "Z06", "Z07", "Z08", "Z09", "Z10", "Z12", "Z13", "Z14", "Z15", "Z16", "Z17", "Z18", "Z19", "Z20", "Z21", "Z22", "Z23", "Z24", "Z25"], "salesOrgList": [{ "salesOrg": "2820", "distributionChannel": "80" }, { "salesOrg": "2810", "distributionChannel": "80" }, { "salesOrg": "2811", "distributionChannel": "80" }] }, { "salesOffice": "CS06", "salesOfficeName": null, "salesGroupList": ["Z01", "Z02", "Z03", "Z04", "Z05", "Z06", "Z07", "Z08", "Z09", "Z10", "Z11", "Z12", "Z13", "Z14", "Z15"], "salesOrgList": [{ "salesOrg": "2820", "distributionChannel": "80" }, { "salesOrg": "2810", "distributionChannel": "80" }] }, { "salesOffice": "CS07", "salesOfficeName": null, "salesGroupList": ["Z01", "Z02", "Z03", "Z04", "Z05", "Z06", "Z07", "Z08", "Z09", "Z10", "Z11", "Z12", "Z13", "Z14", "Z15"], "salesOrgList": [{ "salesOrg": "2820", "distributionChannel": "80" }, { "salesOrg": "2810", "distributionChannel": "80" }] }, { "salesOffice": "CS08", "salesOfficeName": null, "salesGroupList": ["Z01", "Z02", "Z03"], "salesOrgList": [{ "salesOrg": "2820", "distributionChannel": "80" }, { "salesOrg": "2810", "distributionChannel": "80" }, { "salesOrg": "2811", "distributionChannel": "80" }] }, { "salesOffice": "CS09", "salesOfficeName": null, "salesGroupList": ["Z01", "Z02", "Z03", "Z04", "Z05", "Z06", "Z07", "Z08", "Z09", "Z10"], "salesOrgList": [{ "salesOrg": "2820", "distributionChannel": "80" }, { "salesOrg": "2810", "distributionChannel": "80" }, { "salesOrg": "2810", "distributionChannel": "90" }] }, { "salesOffice": "CS10", "salesOfficeName": null, "salesGroupList": ["Z01"], "salesOrgList": [{ "salesOrg": "2820", "distributionChannel": "80" }, { "salesOrg": "2810", "distributionChannel": "80" }] }, { "salesOffice": "CS11", "salesOfficeName": null, "salesGroupList": ["Z01", "Z02", "Z03", "Z04", "Z05", "Z06", "Z07", "Z08", "Z09", "Z10", "Z11", "Z12", "Z13", "Z14", "Z15"], "salesOrgList": [{ "salesOrg": "2820", "distributionChannel": "80" }, { "salesOrg": "2810", "distributionChannel": "80" }] }, { "salesOffice": "CS12", "salesOfficeName": null, "salesGroupList": ["Z01", "Z02", "Z03"], "salesOrgList": [{ "salesOrg": "2820", "distributionChannel": "80" }, { "salesOrg": "2810", "distributionChannel": "80" }] }, { "salesOffice": "CS13", "salesOfficeName": null, "salesGroupList": ["Z01", "Z02", "Z03", "Z04", "Z05", "Z06", "Z07", "Z08", "Z09", "Z10"], "salesOrgList": [{ "salesOrg": "2810", "distributionChannel": "80" }] }, { "salesOffice": "DIY1", "salesOfficeName": null, "salesGroupList": ["Z01", "Z02"], "salesOrgList": [{ "salesOrg": "2811", "distributionChannel": "80" }] }, { "salesOffice": "DY01", "salesOfficeName": null, "salesGroupList": ["Z01", "Z02", "Z03", "Z04", "Z05", "Z06", "Z07", "Z08", "Z09", "Z10", "Z11", "Z12", "Z13", "Z14", "Z15", "Z16", "Z17", "Z18", "Z19", "Z20"], "salesOrgList": [{ "salesOrg": "2821", "distributionChannel": "80" }, { "salesOrg": "2811", "distributionChannel": "80" }, { "salesOrg": "2811", "distributionChannel": "90" }] }, { "salesOffice": "EX01", "salesOfficeName": null, "salesGroupList": ["Z01", "Z02", "Z03", "Z04", "Z05", "Z06", "Z07", "Z08", "Z09", "Z10", "Z11", "Z12", "Z13", "Z14", "Z15", "Z16", "Z17", "Z18", "Z19", "Z20"], "salesOrgList": [{ "salesOrg": "2810", "distributionChannel": "80" }, { "salesOrg": "2810", "distributionChannel": "90" }, { "salesOrg": "2811", "distributionChannel": "80" }, { "salesOrg": "2811", "distributionChannel": "90" }] }, { "salesOffice": "KH10", "salesOfficeName": null, "salesGroupList": ["Z01", "Z02", "Z03", "Z04", "Z05", "Z06", "Z07", "Z08", "Z09", "Z10", "Z11", "Z12", "Z13", "Z14", "Z15", "Z16", "Z17", "Z18", "Z19", "Z20", "Z21", "Z22", "Z23", "Z24", "Z25", "Z26", "Z27", "Z28", "Z29", "Z30", "Z31", "Z32", "Z33", "Z34", "Z35", "Z36", "Z37", "Z38", "Z39", "Z40", "Z41", "Z42", "Z43", "Z44", "Z45", "Z46", "Z47", "Z48", "Z49", "Z50", "Z51", "Z52", "Z53", "Z54", "Z55", "Z56", "Z57", "Z58", "Z59", "Z60", "Z61", "Z62", "Z63", "Z64", "Z65", "Z66", "Z67", "Z68", "Z69", "Z70"], "salesOrgList": [{ "salesOrg": "2821", "distributionChannel": "80" }, { "salesOrg": "2811", "distributionChannel": "80" }, { "salesOrg": "2811", "distributionChannel": "90" }] }, { "salesOffice": "KP01", "salesOfficeName": null, "salesGroupList": ["Z01", "Z02", "Z03", "Z04", "Z05", "Z06", "Z07", "Z08", "Z09", "Z10", "Z11", "Z12", "Z13", "Z14", "Z15", "Z16", "Z17", "Z18", "Z19", "Z20", "Z21", "Z22", "Z23", "Z24", "Z25", "Z26", "Z27", "Z28", "Z29", "Z30", "Z31", "Z32", "Z33", "Z34", "Z35", "Z36", "Z37", "Z38", "Z39", "Z40", "Z41", "Z42", "Z43", "Z44", "Z45", "Z46", "Z47", "Z48", "Z49", "Z50", "Z51", "Z52", "Z53", "Z54", "Z55", "Z56", "Z57", "Z58", "Z59", "Z60", "Z61", "Z62", "Z63", "Z64", "Z65", "Z66", "Z67", "Z68", "Z69", "Z70"], "salesOrgList": [{ "salesOrg": "2821", "distributionChannel": "80" }, { "salesOrg": "2811", "distributionChannel": "80" }] }, { "salesOffice": "KP02", "salesOfficeName": null, "salesGroupList": ["Z01", "Z02", "Z03", "Z04", "Z05", "Z06", "Z07", "Z08", "Z09", "Z10", "Z11", "Z12", "Z13", "Z14", "Z15", "Z16", "Z17", "Z18", "Z19", "Z20", "Z21", "Z22", "Z23", "Z24", "Z25", "Z26", "Z27", "Z28", "Z29", "Z30", "Z31", "Z32", "Z33", "Z34", "Z35", "Z36", "Z37", "Z38", "Z39", "Z40", "Z41", "Z42", "Z43", "Z44", "Z45", "Z46", "Z47", "Z48", "Z49", "Z50"], "salesOrgList": [{ "salesOrg": "2821", "distributionChannel": "80" }, { "salesOrg": "2811", "distributionChannel": "80" }] }, { "salesOffice": "KP03", "salesOfficeName": null, "salesGroupList": ["Z01", "Z02", "Z03", "Z04", "Z05", "Z06", "Z07", "Z08", "Z09", "Z10", "Z11", "Z12", "Z13", "Z14", "Z15", "Z16", "Z17", "Z18", "Z19", "Z20", "Z21", "Z22", "Z23", "Z24", "Z25", "Z26", "Z27", "Z28", "Z29", "Z30", "Z31", "Z32", "Z33", "Z34", "Z35", "Z36", "Z37", "Z38", "Z39", "Z40", "Z41", "Z42", "Z43", "Z44", "Z45", "Z46", "Z47", "Z48", "Z49", "Z50"], "salesOrgList": [{ "salesOrg": "2821", "distributionChannel": "80" }, { "salesOrg": "2811", "distributionChannel": "80" }] }, { "salesOffice": "KP04", "salesOfficeName": null, "salesGroupList": ["Z01", "Z02", "Z03", "Z04", "Z05", "Z06", "Z07", "Z08", "Z09", "Z10", "Z11", "Z12", "Z13", "Z14", "Z15", "Z16", "Z17", "Z18", "Z19", "Z20", "Z21", "Z22", "Z23", "Z24", "Z25", "Z26", "Z27", "Z28", "Z29", "Z30", "Z31", "Z32", "Z33", "Z34", "Z35", "Z36", "Z37", "Z38", "Z39", "Z40", "Z41", "Z42", "Z43", "Z44", "Z45", "Z46", "Z47", "Z48", "Z49", "Z50"], "salesOrgList": [{ "salesOrg": "2821", "distributionChannel": "80" }, { "salesOrg": "2811", "distributionChannel": "80" }] }, { "salesOffice": "KP05", "salesOfficeName": null, "salesGroupList": ["Z01", "Z02", "Z03", "Z04", "Z05", "Z06", "Z07", "Z08", "Z09", "Z10", "Z11", "Z12", "Z13", "Z14", "Z15", "Z16", "Z17", "Z18", "Z19", "Z20", "Z21", "Z22", "Z23", "Z24", "Z25", "Z26", "Z27", "Z28", "Z29", "Z30", "Z31", "Z32", "Z33", "Z34", "Z35", "Z36", "Z37", "Z38", "Z39", "Z40", "Z41", "Z42", "Z43", "Z44", "Z45", "Z46", "Z47", "Z48", "Z49", "Z50"], "salesOrgList": [{ "salesOrg": "2811", "distributionChannel": "80" }] }, { "salesOffice": "KP06", "salesOfficeName": null, "salesGroupList": ["Z01", "Z02", "Z03", "Z04", "Z05"], "salesOrgList": [{ "salesOrg": "2821", "distributionChannel": "80" }, { "salesOrg": "2811", "distributionChannel": "80" }] }, { "salesOffice": "KP07", "salesOfficeName": null, "salesGroupList": ["Z01", "Z02", "Z03", "Z04", "Z06", "Z07"], "salesOrgList": [{ "salesOrg": "2811", "distributionChannel": "80" }] }, { "salesOffice": "KP08", "salesOfficeName": null, "salesGroupList": ["Z01", "Z02", "Z03", "Z04", "Z05", "Z06", "Z07", "Z08", "Z09", "Z10", "Z11", "Z12", "Z13", "Z14", "Z15", "Z16", "Z17", "Z18", "Z19", "Z20", "Z21", "Z22", "Z23", "Z24", "Z25", "Z26", "Z27", "Z28", "Z29", "Z30", "Z31", "Z32", "Z33", "Z34", "Z35", "Z36", "Z37", "Z38", "Z39", "Z40", "Z41", "Z42", "Z43", "Z44", "Z45", "Z46", "Z47", "Z48", "Z49", "Z50", "Z51", "Z52", "Z53", "Z54", "Z55", "Z56", "Z57", "Z58", "Z59", "Z60", "Z61", "Z62", "Z63", "Z64", "Z65", "Z66", "Z67", "Z68", "Z69", "Z70"], "salesOrgList": [{ "salesOrg": "2821", "distributionChannel": "80" }, { "salesOrg": "2811", "distributionChannel": "80" }] }, { "salesOffice": "KP09", "salesOfficeName": null, "salesGroupList": ["Z01"], "salesOrgList": [{ "salesOrg": "2811", "distributionChannel": "80" }] }, { "salesOffice": "KP10", "salesOfficeName": null, "salesGroupList": ["Z01"], "salesOrgList": [{ "salesOrg": "2811", "distributionChannel": "80" }] }, { "salesOffice": "KP11", "salesOfficeName": null, "salesGroupList": ["Z01", "Z02", "Z03", "Z04", "Z05", "Z06", "Z07", "Z08", "Z09", "Z10", "Z11", "Z12", "Z13", "Z14", "Z15", "Z16"], "salesOrgList": [{ "salesOrg": "2821", "distributionChannel": "80" }, { "salesOrg": "2811", "distributionChannel": "80" }] }, { "salesOffice": "KP12", "salesOfficeName": null, "salesGroupList": ["Z01", "Z02", "Z03", "Z04", "Z05", "Z06", "Z07", "Z08", "Z09", "Z10", "Z11", "Z12", "Z13", "Z14", "Z15", "Z16", "Z17", "Z18", "Z19", "Z20", "Z21", "Z22", "Z23", "Z24", "Z25"], "salesOrgList": [{ "salesOrg": "2821", "distributionChannel": "80" }, { "salesOrg": "2811", "distributionChannel": "80" }] }, { "salesOffice": "PP01", "salesOfficeName": null, "salesGroupList": ["Z01", "Z02", "Z03", "Z04", "Z05", "Z06", "Z07", "Z08", "Z09", "Z10", "Z11", "Z12", "Z13", "Z14", "Z15", "Z16", "Z17", "Z18", "Z19", "Z20"], "salesOrgList": [{ "salesOrg": "2821", "distributionChannel": "80" }, { "salesOrg": "2811", "distributionChannel": "80" }, { "salesOrg": "2811", "distributionChannel": "90" }] }, { "salesOffice": "UNKN", "salesOfficeName": null, "salesGroupList": ["Z01"], "salesOrgList": [{ "salesOrg": "2820", "distributionChannel": "80" }, { "salesOrg": "2810", "distributionChannel": "80" }, { "salesOrg": "2821", "distributionChannel": "80" }, { "salesOrg": "2810", "distributionChannel": "90" }, { "salesOrg": "2811", "distributionChannel": "80" }, { "salesOrg": "2811", "distributionChannel": "90" }] }, { "salesOffice": "ZZ01", "salesOfficeName": null, "salesGroupList": ["Z01", "Z02", "Z03"], "salesOrgList": [{ "salesOrg": "2820", "distributionChannel": "80" }, { "salesOrg": "2810", "distributionChannel": "80" }, { "salesOrg": "2821", "distributionChannel": "80" }, { "salesOrg": "2810", "distributionChannel": "90" }, { "salesOrg": "2811", "distributionChannel": "80" }, { "salesOrg": "2811", "distributionChannel": "90" }] }],
        "locations": [
            {
                "siteNum": "2028",
                "region": "NA",
                "type": "MILL",
                "plantType": "KCNA Mill",
                "shortName": "Beech Island Mill",
                "siteName": "S_2028_298",
                "siteNameDesc": "Beech Island Global Sales Mill",
                "location": {
                    "id": "2028",
                    "name": "Beech Island Mill",
                    "city": "BEECH ISLAND",
                    "state": "SC",
                    "country": "USA",
                    "street": "246 OLD JACKSON HIGHWAY",
                    "geoLocation": {
                        "longitude": -81.8986332,
                        "latitude": 33.4163503
                    },
                    "timezone": "US/Eastern",
                    "postalCode": "29842"
                }
            },
            {
                "siteNum": "2822",
                "region": "NA",
                "type": "MILL",
                "plantType": "KCNA Mill",
                "shortName": "Chester Mill",
                "siteName": "S_2822_190",
                "siteNameDesc": "Chester Global Sales Mill",
                "location": {
                    "id": "2822",
                    "name": "Chester Mill",
                    "city": "CHESTER",
                    "state": "PA",
                    "country": "USA",
                    "street": "2ND AND CROSBY STREETS",
                    "geoLocation": {
                        "longitude": -75.3556132,
                        "latitude": 39.8470955
                    },
                    "timezone": "US/Eastern",
                    "postalCode": "19013"
                }
            },
            {
                "siteNum": "2360",
                "region": "NA",
                "type": "DC",
                "plantType": "RDC",
                "alias": "LOG",
                "shortName": "Chester RDC",
                "siteName": "S_2360_080",
                "siteNameDesc": "Chester Regional Dist Cnt",
                "location": {
                    "id": "2360",
                    "name": "Chester RDC",
                    "city": "SWEDESBORO",
                    "state": "NJ",
                    "country": "USA",
                    "street": "1150 COMMERCE BOULEVARD",
                    "geoLocation": {
                        "longitude": -75.3830806,
                        "latitude": 39.7669725
                    },
                    "timezone": "US/Eastern",
                    "postalCode": "08085-1772"
                }
            },
            {
                "siteNum": "2336",
                "region": "NA",
                "type": "DC",
                "plantType": "RDC",
                "alias": "ERDC",
                "shortName": "Eastern Regional DC",
                "siteName": "S_2336_186",
                "siteNameDesc": "Eastern Regional DC - ERDC",
                "location": {
                    "id": "2336",
                    "name": "Eastern Regional DC",
                    "city": "PITTSTON",
                    "state": "PA",
                    "country": "USA",
                    "street": "325 CENTERPOINT BOULEVARD",
                    "geoLocation": {
                        "longitude": -75.7501646,
                        "latitude": 41.2942816
                    },
                    "timezone": "US/Eastern",
                    "postalCode": "18640-6139"
                }
            },
            {
                "siteNum": "2283",
                "region": "NA",
                "type": "MILL",
                "plantType": "KCNA Mill",
                "alias": "HUNT",
                "shortName": "Huntsville Mill",
                "siteName": "S_2283_P1H",
                "siteNameDesc": "Huntsville Global Sales Mill",
                "location": {
                    "id": "2283",
                    "name": "Huntsville Mill",
                    "city": "HUNTSVILLE",
                    "state": "ON",
                    "country": "CAN",
                    "street": "570 RAVENSCLIFFE RD",
                    "geoLocation": {
                        "longitude": -79.242198,
                        "latitude": 45.3461429
                    },
                    "timezone": "US/Eastern",
                    "postalCode": "P1H 2A1"
                }
            },
            {
                "siteNum": "2027",
                "region": "NA",
                "type": "MILL",
                "plantType": "KCNA Mill",
                "alias": "JKS",
                "shortName": "Jenks Mill",
                "siteName": "S_2027_740",
                "siteNameDesc": "Jenks Global Sales Mill",
                "location": {
                    "id": "2027",
                    "name": "Jenks Mill",
                    "city": "JENKS",
                    "state": "OK",
                    "country": "USA",
                    "street": "13219 SOUTH KIMBERLY-CLARK PLACE",
                    "geoLocation": {
                        "longitude": -95.929945,
                        "latitude": 35.9668902
                    },
                    "timezone": "US/Central",
                    "postalCode": "74037"
                }
            },
            {
                "siteNum": "2047",
                "region": "NA",
                "type": "DC",
                "plantType": "RDC",
                "alias": "LADC",
                "shortName": "Los Angeles DC",
                "siteName": "S_2047_917",
                "siteNameDesc": "Los Angeles DC ExtOps/DC",
                "location": {
                    "id": "2047",
                    "name": "Los Angeles DC",
                    "city": "ONTARIO",
                    "state": "CA",
                    "country": "USA",
                    "street": "4815 S HELLMAN AVE",
                    "geoLocation": {
                        "longitude": -117.602477,
                        "latitude": 34.040893
                    },
                    "timezone": "US/Pacific",
                    "postalCode": "91762"
                }
            },
            {
                "siteNum": "2833",
                "region": "NA",
                "type": "MILL",
                "plantType": "KCP Mill",
                "alias": "MAR",
                "shortName": "Marinette Mill",
                "siteName": "S_2833_541",
                "siteNameDesc": "Marinette Global Sales Mill",
                "location": {
                    "id": "2833",
                    "name": "Marinette Mill",
                    "city": "MARINETTE",
                    "state": "WI",
                    "country": "USA",
                    "street": "319 VAN CLEVE AVENUE",
                    "geoLocation": {
                        "longitude": -87.650146,
                        "latitude": 45.1018741
                    },
                    "timezone": "US/Central",
                    "postalCode": "54143-1123"
                }
            },
            {
                "siteNum": "2024",
                "region": "NA",
                "type": "MILL",
                "plantType": "KCNA Mill",
                "alias": "MAU",
                "shortName": "Maumelle Mill",
                "siteName": "S_2024_721",
                "siteNameDesc": "Maumelle Global Sales Mill",
                "location": {
                    "id": "2024",
                    "name": "Maumelle Mill",
                    "city": "MAUMELLE",
                    "state": "AR",
                    "country": "USA",
                    "street": "500 MURPHY DRIVE",
                    "geoLocation": {
                        "longitude": -92.3928768,
                        "latitude": 34.8643233
                    },
                    "timezone": "US/Central",
                    "postalCode": "72113-6189"
                }
            },
            {
                "siteNum": "2050",
                "region": "NA",
                "type": "DC",
                "plantType": "RDC",
                "alias": "MID",
                "shortName": "Mid-South DC",
                "siteName": "S_2050_720",
                "siteNameDesc": "Mid-South /DC",
                "location": {
                    "id": "2050",
                    "name": "Mid-South DC",
                    "city": "CONWAY",
                    "state": "AR",
                    "country": "USA",
                    "street": "1475 WILLIAM J CLARK DRIVE",
                    "geoLocation": {
                        "longitude": -92.4057446,
                        "latitude": 35.0579005
                    },
                    "timezone": "US/Central",
                    "postalCode": "72032-8240"
                }
            },
            {
                "siteNum": "2528",
                "region": "NA",
                "type": "DC",
                "plantType": "RDC",
                "alias": "MODC",
                "shortName": "Milton Ontario DC",
                "siteName": "S_2528_L9T",
                "siteNameDesc": "Milton Ontario ExtOPs/DC real postal L9T 6Y9",
                "location": {
                    "id": "2528",
                    "name": "Milton Ontario DC",
                    "city": "MILTON",
                    "state": "ON",
                    "country": "CAN",
                    "street": "2994 PEDDIE ROAD",
                    "geoLocation": {
                        "longitude": -79.9133878,
                        "latitude": 43.5410442
                    },
                    "timezone": "US/Eastern",
                    "postalCode": "L9T 6Y9"
                }
            },
            {
                "siteNum": "2827",
                "region": "NA",
                "type": "MILL",
                "plantType": "KCNA Mill",
                "alias": "MOB",
                "shortName": "Mobile Mill",
                "siteName": "S_2827_366",
                "siteNameDesc": "Mobile Global Sales Mill",
                "location": {
                    "id": "2827",
                    "name": "Mobile Mill",
                    "city": "MOBILE",
                    "state": "AL",
                    "country": "USA",
                    "street": "200 BAY BRIDGE ROAD",
                    "geoLocation": {
                        "longitude": -88.0501943,
                        "latitude": 30.7366615
                    },
                    "timezone": "US/Central",
                    "postalCode": "36610-3003"
                }
            },
            {
                "siteNum": "2023",
                "region": "NA",
                "type": "MILL",
                "plantType": "KCNA Mill",
                "alias": "NCSF",
                "shortName": "Neenah Cold Spring Facility",
                "siteName": "S_2023_549",
                "siteNameDesc": "Neenah Cold Spring Global Sale",
                "location": {
                    "id": "2023",
                    "name": "Neenah Cold Spring Facility",
                    "city": "NEENAH",
                    "state": "WI",
                    "country": "USA",
                    "street": "1200 JACOBSEN ROAD",
                    "geoLocation": {
                        "longitude": -88.4917463,
                        "latitude": 44.2142929
                    },
                    "timezone": "US/Central",
                    "postalCode": "54956-1314"
                }
            },
            {
                "siteNum": "2031",
                "region": "NA",
                "type": "MILL",
                "plantType": "KCNA Mill",
                "alias": "NMC",
                "shortName": "New Milford Mill",
                "siteName": "S_2031_067",
                "siteNameDesc": "New Milford Global Sales Mill",
                "location": {
                    "id": "2031",
                    "name": "New Milford Mill",
                    "city": "NEW MILFORD",
                    "state": "CT",
                    "country": "USA",
                    "street": "58 PICKETT DIST ROAD",
                    "geoLocation": {
                        "longitude": -73.4111248,
                        "latitude": 41.5579034
                    },
                    "timezone": "US/Eastern",
                    "postalCode": "06776-4413"
                }
            },
            {
                "siteNum": "2292",
                "region": "NA",
                "type": "DC",
                "plantType": "RDC",
                "alias": "NCDC",
                "shortName": "North Central DC",
                "siteName": "S_2292_604",
                "siteNameDesc": "North Central DC -Ext Ops/DC",
                "location": {
                    "id": "2292",
                    "name": "North Central DC",
                    "city": "ROMEOVILLE",
                    "state": "IL",
                    "country": "USA",
                    "street": "740 PROLOGIS PARKWAY",
                    "geoLocation": {
                        "longitude": -88.1021821,
                        "latitude": 41.6561017
                    },
                    "timezone": "US/Central",
                    "postalCode": "60446-4502"
                }
            },
            {
                "siteNum": "2358",
                "region": "NA",
                "type": "DC",
                "plantType": "RDC",
                "alias": "NCOF",
                "shortName": "North Central Overflow DC",
                "siteName": "S_2358_604",
                "siteNameDesc": "North Central Overflow Ext Ops",
                "location": {
                    "id": "2358",
                    "name": "North Central Overflow DC",
                    "city": "ROMEOVILLE",
                    "state": "IL",
                    "country": "USA",
                    "street": "775 N PROLOGIS PARKWAY",
                    "geoLocation": {
                        "longitude": -88.097522,
                        "latitude": 41.6597135
                    },
                    "timezone": "US/Central",
                    "postalCode": "60446-4501"
                }
            },
            {
                "siteNum": "2508",
                "region": "NA",
                "type": "DC",
                "plantType": "RDC",
                "alias": "NEDC",
                "shortName": "Northeast DC",
                "siteName": "S_2508_195",
                "siteNameDesc": "Northeast Distribution Center (NEDC)",
                "location": {
                    "id": "2508",
                    "name": "Northeast DC",
                    "city": "SHOEMAKERSVILLE",
                    "state": "PA",
                    "country": "USA",
                    "street": "211 LOGISTICS DRIVE",
                    "geoLocation": {
                        "longitude": -75.966777,
                        "latitude": 40.522329
                    },
                    "timezone": "US/Eastern",
                    "postalCode": "19555"
                }
            },
            {
                "siteNum": "2848",
                "region": "NA",
                "type": "DC",
                "plantType": "RDC",
                "alias": "NWDC",
                "shortName": "Northwest DC",
                "siteName": "S_2848_983",
                "siteNameDesc": "NORTHWEST DISTRIBUTION CENTER",
                "location": {
                    "id": "2848",
                    "name": "Northwest DC",
                    "city": "DUPONT",
                    "state": "WA",
                    "country": "USA",
                    "street": "1205 WHARF ROAD",
                    "geoLocation": {
                        "longitude": -122.628625,
                        "latitude": 47.115485
                    },
                    "timezone": "US/Pacific",
                    "postalCode": "98327"
                }
            },
            {
                "siteNum": "2029",
                "region": "NA",
                "type": "MILL",
                "plantType": "KCNA Mill",
                "alias": "OGD",
                "shortName": "Ogden Mill",
                "siteName": "S_2029_844",
                "siteNameDesc": "Ogden Global Sales Mill",
                "location": {
                    "id": "2029",
                    "name": "Ogden Mill",
                    "city": "OGDEN",
                    "state": "UT",
                    "country": "USA",
                    "street": "2010 RULON WHITE BLVD",
                    "geoLocation": {
                        "longitude": -112.0065929,
                        "latitude": 41.2940215
                    },
                    "timezone": "US/Mountain",
                    "postalCode": "84404"
                }
            },
            {
                "siteNum": "2032",
                "region": "NA",
                "type": "MILL",
                "plantType": "KCNA Mill",
                "alias": "PAR",
                "shortName": "Paris Mill",
                "siteName": "S_2032_754",
                "siteNameDesc": "Paris Texas Global Sales Mill",
                "location": {
                    "id": "2032",
                    "name": "Paris Mill",
                    "city": "PARIS",
                    "state": "TX",
                    "country": "USA",
                    "street": "2466 FARM ROAD 137",
                    "geoLocation": {
                        "longitude": -95.593167,
                        "latitude": 33.6408131
                    },
                    "timezone": "US/Central",
                    "postalCode": "75460-1204"
                }
            },
            {
                "siteNum": "2190",
                "region": "NA",
                "type": "DC",
                "plantType": "RDC",
                "alias": "RBW90",
                "shortName": "Richmond Bonded Warehouse",
                "siteName": "S_2190_309",
                "siteNameDesc": "Richmond Bonded Warehouse - DC",
                "location": {
                    "id": "2190",
                    "name": "Richmond Bonded Warehouse",
                    "city": "AUGUSTA",
                    "state": "GA",
                    "country": "USA",
                    "street": "1634 TOBACCO ROAD",
                    "geoLocation": {
                        "longitude": -81.9788915,
                        "latitude": 33.3690206
                    },
                    "timezone": "US/Eastern",
                    "postalCode": "30906-9267"
                }
            },
            {
                "siteNum": "2496",
                "region": "NA",
                "type": "DC",
                "plantType": "RDC",
                "alias": "SDDC",
                "shortName": "South Dallas DC",
                "siteName": "S_2496_752",
                "siteNameDesc": "EXT OPS/DC - SOUTH DALLAS DC -",
                "location": {
                    "id": "2496",
                    "name": "South Dallas DC",
                    "city": "DALLAS",
                    "state": "TX",
                    "country": "USA",
                    "street": "4808 MOUNTAIN CREEK PARKWAY",
                    "geoLocation": {
                        "longitude": -96.9650634,
                        "latitude": 32.6761483
                    },
                    "timezone": "US/Central",
                    "postalCode": "75236-4602"
                }
            },
            {
                "siteNum": "2299",
                "region": "NA",
                "type": "DC",
                "plantType": "RDC",
                "alias": "SRDC",
                "shortName": "Southern Regional DC",
                "siteName": "S_2299_302",
                "siteNameDesc": "South Regional DC -ExtOps/DC",
                "location": {
                    "id": "2299",
                    "name": "Southern Regional DC",
                    "city": "MCDONOUGH",
                    "state": "GA",
                    "country": "USA",
                    "street": "340 WESTRIDGE PARKWAY",
                    "geoLocation": {
                        "longitude": -84.1929612,
                        "latitude": 33.4042901
                    },
                    "timezone": "US/Eastern",
                    "postalCode": "30253-3006"
                }
            }
        ],
        "customer": { data: [{ "customerName": "AMAZON.COM EDI", "salesOrgList": [{ "salesOrg": "2811", "distributionChannel": "80" }] }, { "customerName": "AMAZON VENDOR FLEX 2.0", "salesOrgList": [{ "salesOrg": "2810", "distributionChannel": "80" }] }, { "customerName": "AMAZON DVS", "salesOrgList": [{ "salesOrg": "2810", "distributionChannel": "80" }] }, { "customerName": "AMAZON.COM INC", "salesOrgList": [{ "salesOrg": "2810", "distributionChannel": "80" }, { "salesOrg": "2811", "distributionChannel": "80" }] }, { "customerName": "AMAZON CA INC", "salesOrgList": [{ "salesOrg": "2820", "distributionChannel": "80" }, { "salesOrg": "2821", "distributionChannel": "80" }] }, { "customerName": "AMAZON CA INC DVS", "salesOrgList": [{ "salesOrg": "2820", "distributionChannel": "80" }] }], noData: false }
    },
    "favorites": {
        "businessUnits": ["PROFESSIONAL"],
        "tempBusinessUnit": ["PROFESSIONAL", "CONSUMER"],
        "favloading": false, "mark_loading": false, "favorites": [{ "siteNum": "2028", "region": "NA", "type": "MILL", "plantType": "KCNA Mill", "alias": "BIL", "shortName": "Beech Island Mill", "siteName": "S_2028_298", "siteNameDesc": "Beech Island Global Sales Mill", "location": { "id": "2028", "name": "Beech Island Mill", "city": "BEECH ISLAND", "state": "SC", "country": "USA", "street": "246 OLD JACKSON HIGHWAY", "geoLocation": { "longitude": -81.8986332, "latitude": 33.4163503 }, "timezone": "US/Eastern", "postalCode": "29842" }, "isFav": true }, { "siteNum": "2283", "region": "NA", "type": "MILL", "plantType": "KCNA Mill", "alias": "HUNT", "shortName": "Huntsville Mill", "siteName": "S_2283_P1H", "siteNameDesc": "Huntsville Global Sales Mill", "location": { "id": "2283", "name": "Huntsville Mill", "city": "HUNTSVILLE", "state": "ON", "country": "CAN", "street": "570 RAVENSCLIFFE RD", "geoLocation": { "longitude": -79.242198, "latitude": 45.3461429 }, "timezone": "US/Eastern", "postalCode": "P1H 2A1" }, "isFav": true }, { "siteNum": "2833", "region": "NA", "type": "MILL", "plantType": "KCP Mill", "alias": "MAR", "shortName": "Marinette Mill", "siteName": "S_2833_541", "siteNameDesc": "Marinette Global Sales Mill", "location": { "id": "2833", "name": "Marinette Mill", "city": "MARINETTE", "state": "WI", "country": "USA", "street": "319 VAN CLEVE AVENUE", "geoLocation": { "longitude": -87.650146, "latitude": 45.1018741 }, "timezone": "US/Central", "postalCode": "54143-1123" }, "isFav": true }], "markfav": "done",
        "favCuso": {
            "FAV_CUST": [{ "selectionType": "CUST", "salesOrg": "2810", "distributionChannel": "80", "customerName": "Amazon" }],
            "FAV_SALES_OFFICE": [{ "selectionType": "SALES_OFFICE", "salesOrg": "2810", "distributionChannel": "80", "salesOffice": "CS01" }]
        }
    },
    "items": {
        "items": [{ "siteNum": "2028", "region": "NA", "type": "MILL", "plantType": "KCNA Mill", "alias": "BIL", "shortName": "Beech Island Mill", "siteName": "S_2028_298", "siteNameDesc": "Beech Island Global Sales Mill", "location": { "id": "2028", "name": "Beech Island Mill", "city": "BEECH ISLAND", "state": "SC", "country": "USA", "street": "246 OLD JACKSON HIGHWAY", "geoLocation": { "longitude": -81.8986332, "latitude": 33.4163503 }, "timezone": "US/Eastern", "postalCode": "29842" }, "isFav": true }, { "siteNum": "2283", "region": "NA", "type": "MILL", "plantType": "KCNA Mill", "alias": "HUNT", "shortName": "Huntsville Mill", "siteName": "S_2283_P1H", "siteNameDesc": "Huntsville Global Sales Mill", "location": { "id": "2283", "name": "Huntsville Mill", "city": "HUNTSVILLE", "state": "ON", "country": "CAN", "street": "570 RAVENSCLIFFE RD", "geoLocation": { "longitude": -79.242198, "latitude": 45.3461429 }, "timezone": "US/Eastern", "postalCode": "P1H 2A1" }, "isFav": true }, { "siteNum": "2833", "region": "NA", "type": "MILL", "plantType": "KCP Mill", "alias": "MAR", "shortName": "Marinette Mill", "siteName": "S_2833_541", "siteNameDesc": "Marinette Global Sales Mill", "location": { "id": "2833", "name": "Marinette Mill", "city": "MARINETTE", "state": "WI", "country": "USA", "street": "319 VAN CLEVE AVENUE", "geoLocation": { "longitude": -87.650146, "latitude": 45.1018741 }, "timezone": "US/Central", "postalCode": "54143-1123" }, "isFav": true }],
        "cuso": {
            "CUST": [],
            "SALES_OFFICE": [
                {
                    "salesOffice": "DY01",
                    "selectionType": "SALES_OFFICE",
                    "salesOrg": "2821",
                    "distributionChannel": "80",
                    "salesGroup": [
                        "Z01",
                        "Z02",
                        "Z03",
                        "Z04",
                        "Z05",
                        "Z06",
                        "Z07",
                        "Z08",
                        "Z09",
                        "Z10",
                        "Z11",
                        "Z12",
                        "Z13",
                        "Z14",
                        "Z15",
                        "Z16",
                        "Z17",
                        "Z18",
                        "Z19",
                        "Z20"
                    ],
                    "isFav": false
                },
                {
                    "salesOffice": "CS01",
                    "selectionType": "SALES_OFFICE",
                    "salesOrg": "2810",
                    "distributionChannel": "80",
                    "salesGroup": [
                        "Z01",
                        "Z02",
                        "Z03",
                        "Z04",
                        "Z05",
                        "Z06",
                        "Z07",
                        "Z08",
                        "Z09",
                        "Z10",
                        "Z11",
                        "Z12",
                        "Z13",
                        "Z14",
                        "Z15",
                        "Z16",
                        "Z17",
                        "Z18",
                        "Z19",
                        "Z20"
                    ],
                    "isFav": false
                },
                {
                    "salesOffice": "CS01",
                    "selectionType": "SALES_OFFICE",
                    "salesOrg": "2820",
                    "distributionChannel": "80",
                    "salesGroup": [
                        "Z01",
                        "Z02",
                        "Z03",
                        "Z04",
                        "Z05",
                        "Z06",
                        "Z07",
                        "Z08",
                        "Z09",
                        "Z10",
                        "Z11",
                        "Z12",
                        "Z13",
                        "Z14",
                        "Z15",
                        "Z16",
                        "Z17",
                        "Z18",
                        "Z19",
                        "Z20"
                    ],
                    "isFav": false
                },
                {
                    "salesOffice": "CS05",
                    "selectionType": "SALES_OFFICE",
                    "salesOrg": "2811",
                    "distributionChannel": "80",
                    "salesGroup": [
                        "Z01",
                        "Z02",
                        "Z03",
                        "Z04",
                        "Z05",
                        "Z06",
                        "Z07",
                        "Z08",
                        "Z09",
                        "Z10",
                        "Z12",
                        "Z13",
                        "Z14",
                        "Z15",
                        "Z16",
                        "Z17",
                        "Z18",
                        "Z19",
                        "Z20",
                        "Z21",
                        "Z22",
                        "Z23",
                        "Z24",
                        "Z25"
                    ],
                    "isFav": false
                }
            ]
        },
        "tab": false,
    },
    "auth": {
        "auth": {
            "user": null
        },
        "user": {
            "sub": "00uub3tx16vpw12DQ0h7",
            "name": "Khare, Mayank",
            "locale": "IN",
            "email": "mayank.khare@kcc.com",
            "ver": 1,
            "iss": "https://kcc.oktapreview.com",
            "aud": "0oas02mzlsHqwbzWj0h7",
            "iat": 1607565028,
            "exp": 1607568628,
            "jti": "ID.E0bU4UYe-ox9RwjNl_NbPCFkONeG-egAxTLi9zSj1rI",
            "amr": [
                "pwd"
            ],
            "idp": "0oa8dx8e4f8qLf6vR0h7",
            "nonce": "hX4XeUXtQDtLHgnfKh9KCAHYrRY9chs9tR3IwWgzekDYw0KfJ4tYzrKKNdgIOJdZ",
            "preferred_username": "mayank.khare@kcc.com",
            "given_name": "Mayank",
            "family_name": "Khare",
            "zoneinfo": "America/Los_Angeles",
            "updated_at": 1601900495,
            "email_verified": true,
            "auth_time": 1607565027
        }
    },
    "options": {
        "showfavs": true,
        "theme": "dark",
        "filters": {
            "network": {
                "sites": {
                    "type": "sites",
                    "name": "Origin",
                    "data": null
                },
                "searchStringList": {
                    "type": "text",
                    "name": "Search",
                    "data": null
                },
                "destName": {
                    "type": "text",
                    "name": "Customer",
                    "data": null
                },
                "destState": {
                    "type": "text",
                    "name": "Destination State",
                    "data": null
                },
                "destCity": {
                    "type": "text",
                    "name": "Destination City",
                    "data": null
                },
                "orderStatusBucket": {
                    "type": "checkbox",
                    "name": "Order Status",
                    "data": [
                        {
                            "name": "Order Block Free",
                            "value": "ORDER_BLOCK_FREE",
                            "checked": false
                        },
                        {
                            "name": "Order Blocked",
                            "value": "ORDER_BLOCKED",
                            "checked": false
                        }
                    ]
                },
                "orderTypes": {
                    "type": "checkbox",
                    "name": "Order Type",
                    "data": [
                        {
                            "name": "Cust. Order",
                            "value": "CUST",
                            "checked": false
                        },
                        {
                            "name": "STO",
                            "value": "STO",
                            "checked": false
                        }
                    ]
                },
                "tariffServiceCode": {
                    "type": "text",
                    "name": "Carrier Service Code",
                    "data": null
                },
                "deliveryBlockCodeList": {
                    type: "textcheck",
                    name: "Delivery/Credit Block",
                    placeholder: 'Enter delivery block code',
                    shortName: "Delivery Block Code",
                    addon: "creditOnHold",
                    "data": []
                },
                "orderExecutionBucket": {
                    "type": "checkbox",
                    "name": "Shipment Status",
                    "data": [
                        {
                            "name": "Transportation to be planned",
                            "value": "TRANS_PLAN_UNASSIGNED",
                            "checked": false
                        },
                        {
                            "name": "Transportation planning",
                            "value": "TRANS_PLAN_OPEN",
                            "checked": false
                        },
                        {
                            "name": "Transportation processing",
                            "value": "TRANS_PLAN_PROCESSING",
                            "checked": false
                        },
                        {
                            "name": "Transportation Carrier committed",
                            "value": "TRANS_PLAN_TENDER_ACCEPTED",
                            "checked": false
                        },
                        {
                            "name": "Shipment Created",
                            "value": "SHIPMENT_CREATED",
                            "checked": false
                        },
                        {
                            "name": "Checked in by DC",
                            "value": "CHECK_IN",
                            "checked": false
                        },
                        {
                            "name": "Loading Started",
                            "value": "LOADING_STARTED",
                            "checked": false
                        },
                        {
                            "name": "Ready for Pickup",
                            "value": "TRANS_EXEC_READY_PICK_UP",
                            "checked": false
                        },
                        {
                            "name": "In Transit",
                            "value": "TRANS_EXEC_IN_TRANSIT",
                            "checked": false
                        },
                        {
                            "name": "Delivery confirmed",
                            "value": "TRANS_EXEC_DELIVERY_CONFIRMED",
                            "checked": false
                        }
                    ]
                },
                "loadReadyDateTimeOriginTZ": {
                    "type": "date",
                    "dummytime": true,
                    "timeformat": "T",
                    "startTime": "[ 00:00:00.000]",
                    "endTime": "[ 23:59:59.999]",
                    "name": "Carrier Ready Date",
                    "shortName": "Carr. Ready Date",
                    "data": null
                },
                "loadStartDateTimeOriginTZ": {
                    "type": "date",
                    "timeformat": "T",
                    "startTime": "[ 00:00:00.000]",
                    "endTime": "[ 23:59:59.999]",
                    "name": "Carrier Ready Date",
                    "shortName": "Carr. Ready Date",
                    "data": null
                }
            }
        }
    },
    "charts": {
        "loaders": {
            "network": {
                "ORDER": {
                    "loading": false
                },
                "TRANS_PLAN": {
                    "loading": false
                },
                "DIST": {
                    "loading": false
                },
                "TRANS_EXEC": {
                    "loading": false
                }
            }
        },
        "charts": {
            "network": {
                "DIST": [
                    {
                        "state": "SHIPMENT_CREATED",
                        "stateDesc": "Shipment created",
                        "totalCount": 1,
                        "redCount": 1
                    },
                    {
                        "state": "CHECK_IN",
                        "stateDesc": "Checked in by DC",
                        "totalCount": 0,
                        "redCount": 0
                    },
                    {
                        "state": "LOADING_STARTED",
                        "stateDesc": "Loading started",
                        "totalCount": 0,
                        "redCount": 0
                    }
                ],
                "TRANS_PLAN": [
                    {
                        "state": "TRANS_PLAN_TO_BE_PLANNED",
                        "stateDesc": "To be planned",
                        "totalCount": 296,
                        "redCount": 296
                    },
                    {
                        "state": "TRANS_PLAN_PLANNING",
                        "stateDesc": "Planning",
                        "totalCount": 3,
                        "redCount": 2
                    },
                    {
                        "state": "TRANS_PLAN_PROCESSING",
                        "stateDesc": "Processing",
                        "totalCount": 5,
                        "redCount": 2
                    },
                    {
                        "state": "TRANS_PLAN_CONFIRMED",
                        "stateDesc": "Confirmed",
                        "totalCount": 1,
                        "redCount": 1
                    }
                ],
                "TRANS_EXEC": [
                    {
                        "state": "TRANS_EXEC_READY_PICK_UP",
                        "stateDesc": "Ready for pickup",
                        "totalCount": 0,
                        "redCount": 0
                    },
                    {
                        "state": "TRANS_EXEC_IN_TRANSIT",
                        "stateDesc": "In Transit",
                        "totalCount": 0,
                        "redCount": 0
                    },
                    {
                        "state": "TRANS_EXEC_DELIVERY_CONFIRMED",
                        "stateDesc": "Delivery Confirmed",
                        "totalCount": 0,
                        "redCount": 0
                    }
                ],
                "ORDER": [
                    {
                        "state": "SO_BLOCK_FREE",
                        "stateDesc": "Customer Orders Block free",
                        "totalCount": 35801,
                        "redCount": 1,
                        "yellowCount": 35755
                    },
                    {
                        "state": "SO_BLOCKED",
                        "stateDesc": "Customer Orders Blocked",
                        "totalCount": 163,
                        "redCount": 163,
                        "yellowCount": 0
                    },
                    {
                        "state": "STO",
                        "stateDesc": "Stock Transfer Orders",
                        "totalCount": 14,
                        "redCount": 0,
                        "yellowCount": 0
                    }
                ]
            }
        }
    },
    "user": {
        "filterLayouts": {
            "NETWORK": {
                "tableName": "network",
                "views": [
                    {
                        "id": "726e2559-baa9-4e41-9bd2-b84dff86b223",
                        "name": "Wallomart Unhealthy",
                        "default": false,
                        "filters": {
                            "sites": {
                                "type": "sites",
                                "name": "Origin",
                                "data": null
                            },
                            "searchStringList": {
                                "type": "text",
                                "name": "Search",
                                "stringToArray": true,
                                "data": null
                            },
                            "destName": {
                                "type": "text",
                                "name": "Customer",
                                "data": "walmart*"
                            },
                            "destState": {
                                "type": "text",
                                "name": "Destination State",
                                "data": null
                            },
                            "destCity": {
                                "type": "text",
                                "name": "Destination City",
                                "data": null
                            },
                            "orderStatusBucket": {
                                "type": "checkbox",
                                "name": "Order Status",
                                "data": [
                                    {
                                        "name": "Order Block Free",
                                        "value": "ORDER_BLOCK_FREE",
                                        "checked": false
                                    },
                                    {
                                        "name": "Order Blocked",
                                        "value": "ORDER_BLOCKED",
                                        "checked": true
                                    }
                                ]
                            },
                            "orderTypes": {
                                "type": "checkbox",
                                "name": "Order Type",
                                "data": [
                                    {
                                        "name": "Cust. Order",
                                        "value": "CUST",
                                        "checked": false
                                    },
                                    {
                                        "name": "STO",
                                        "value": "STO",
                                        "checked": false
                                    }
                                ]
                            },
                            "tariffServiceCodeList": {
                                "type": "text",
                                "stringToArray": true,
                                "name": "Carrier Service Code",
                                "data": null
                            },
                            "orderExecutionBucket": {
                                "type": "checkbox",
                                "name": "Shipment Status",
                                "data": [
                                    {
                                        "name": "Transportation to be planned",
                                        "value": "TRANS_PLAN_UNASSIGNED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation planning",
                                        "value": "TRANS_PLAN_OPEN",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation processing",
                                        "value": "TRANS_PLAN_PROCESSING",
                                        "checked": true
                                    },
                                    {
                                        "name": "Transportation Carrier committed",
                                        "value": "TRANS_PLAN_TENDER_ACCEPTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Shipment Created",
                                        "value": "SHIPMENT_CREATED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Checked in by DC",
                                        "value": "CHECK_IN",
                                        "checked": false
                                    },
                                    {
                                        "name": "Loading Started",
                                        "value": "LOADING_STARTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Ready for Pickup",
                                        "value": "TRANS_EXEC_READY_PICK_UP",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Transit",
                                        "value": "TRANS_EXEC_IN_TRANSIT",
                                        "checked": false
                                    },
                                    {
                                        "name": "Delivery confirmed",
                                        "value": "TRANS_EXEC_DELIVERY_CONFIRMED",
                                        "checked": false
                                    }
                                ]
                            },
                            "inboundOrderExecutionBucket": {
                                "type": "checkbox",
                                "name": "Inbound Shipment Status",
                                "data": [
                                    {
                                        "name": "In Transit",
                                        "value": "INBOUND_IN_TRANSIT",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Yard",
                                        "value": "INBOUND_IN_YARD",
                                        "checked": false
                                    },
                                    {
                                        "name": "Unloading",
                                        "value": "INBOUND_UNLOADING",
                                        "checked": false
                                    },
                                    {
                                        "name": "Unloading Completed",
                                        "value": "INBOUND_UNLOADING_COMPLETED",
                                        "checked": false
                                    }
                                ]
                            },
                            "loadReadyDateTimeOriginTZ": {
                                "type": "date",
                                "dummytime": true,
                                "timeformat": "T",
                                "startTime": "[ 00:00:00.000]",
                                "endTime": "[ 23:59:59.999]",
                                "name": "Carrier Ready Date",
                                "shortName": "Carr. Ready Date",
                                "data": null
                            },
                            "searchString": {
                                "type": "text",
                                "name": "Search String",
                                "data": null
                            },
                            "tariffServiceCode": {
                                "type": "text",
                                "name": "Carrier Service Code",
                                "data": null
                            }
                        },
                        "columns": [
                            {
                                "title": "Shipment #",
                                "field": "shipmentNum",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Delivery #",
                                "field": "deliveryNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 1,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 1
                                }
                            },
                            {
                                "title": "Shipment Status",
                                "field": "orderExecutionBucketDesc",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 2,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 2
                                }
                            },
                            {
                                "title": "Inbound Shipment Status",
                                "field": "inboundOrderExecutionBucketDesc",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 3,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 3
                                }
                            },
                            {
                                "title": "Order #",
                                "field": "orderNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 4,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 4
                                }
                            },
                            {
                                "title": "Order Status",
                                "field": "orderStatus",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 5,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 5
                                }
                            },
                            {
                                "title": "Order Type",
                                "field": "orderType",
                                "tableData": {
                                    "columnOrder": 6,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 6
                                }
                            },
                            {
                                "title": "Customer",
                                "field": "customer",
                                "tableData": {
                                    "columnOrder": 7,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 7
                                }
                            },
                            {
                                "title": "Origin ID",
                                "field": "originId",
                                "tableData": {
                                    "columnOrder": 8,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 8
                                }
                            },
                            {
                                "title": "Origin",
                                "field": "origin",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 9,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 9
                                }
                            },
                            {
                                "title": "Dest City",
                                "field": "destinationCity",
                                "tableData": {
                                    "columnOrder": 10,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 10
                                }
                            },
                            {
                                "title": "Dest State",
                                "field": "destinationState",
                                "tableData": {
                                    "columnOrder": 11,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 11
                                }
                            },
                            {
                                "title": "Original RDD",
                                "field": "originalRequestedDeliveryDatetime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 12,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 12
                                }
                            },
                            {
                                "title": "Carrier Ready Date & Time",
                                "field": "loadReadyDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 13,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 13
                                }
                            },
                            {
                                "title": "Carrier",
                                "field": "tariffServiceCode",
                                "tableData": {
                                    "columnOrder": 14,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 14
                                }
                            },
                            {
                                "title": "Ship Condition",
                                "field": "shippingCondition",
                                "tableData": {
                                    "columnOrder": 15,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 15
                                }
                            },
                            {
                                "title": "Reason for Alert",
                                "field": "reasonCodeString",
                                "cellStyle": {
                                    "minWidth": 320
                                },
                                "tableData": {
                                    "columnOrder": 16,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 16
                                }
                            },
                            {
                                "title": "Fourkites ETA",
                                "field": "expectedDeliveryDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "disableClick": true
                            },
                            {
                                "title": "Sales Org",
                                "field": "salesOrg",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 18,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 18
                                }
                            },
                            {
                                "title": "Distribution Channel",
                                "field": "distributionChannel",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 19,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 19
                                }
                            },
                            {
                                "title": "Sales Office",
                                "field": "salesOffice",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 20,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 20
                                }
                            },
                            {
                                "title": "Sales Group",
                                "field": "salesGroup",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 21,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 21
                                }
                            }
                        ]
                    },
                    {
                        "id": "9c0dcab2-256d-4922-89fe-96da76ea0ef8",
                        "name": "Blank",
                        "default": true,
                        "filters": {
                            "sites": {
                                "type": "sites",
                                "name": "Origin",
                                "data": [
                                    "2042",
                                    "2425",
                                    "2822"
                                ]
                            },
                            "searchString": {
                                "type": "text",
                                "name": "Search String",
                                "data": null
                            },
                            "destName": {
                                "type": "text",
                                "name": "Customer",
                                "data": null
                            },
                            "destState": {
                                "type": "text",
                                "name": "Destination State",
                                "data": null
                            },
                            "destCity": {
                                "type": "text",
                                "name": "Destination City",
                                "data": null
                            },
                            "orderStatusBucket": {
                                "type": "checkbox",
                                "name": "Order Status",
                                "data": [
                                    {
                                        "name": "Order Block Free",
                                        "value": "ORDER_BLOCK_FREE",
                                        "checked": true
                                    },
                                    {
                                        "name": "Order Blocked",
                                        "value": "ORDER_BLOCKED",
                                        "checked": true
                                    }
                                ]
                            },
                            "orderTypes": {
                                "type": "checkbox",
                                "name": "Order Type",
                                "data": [
                                    {
                                        "name": "Cust. Order",
                                        "value": "CUST",
                                        "checked": false
                                    },
                                    {
                                        "name": "STO",
                                        "value": "STO",
                                        "checked": false
                                    }
                                ]
                            },
                            "tariffServiceCode": {
                                "type": "text",
                                "name": "Carrier Service Code",
                                "data": null
                            },
                            "orderExecutionBucket": {
                                "type": "checkbox",
                                "name": "Shipment Status",
                                "data": [
                                    {
                                        "name": "Transportation to be planned",
                                        "value": "TRANS_PLAN_UNASSIGNED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation planning",
                                        "value": "TRANS_PLAN_OPEN",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation processing",
                                        "value": "TRANS_PLAN_PROCESSING",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation Carrier committed",
                                        "value": "TRANS_PLAN_TENDER_ACCEPTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Shipment Created",
                                        "value": "SHIPMENT_CREATED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Checked in by DC",
                                        "value": "CHECK_IN",
                                        "checked": false
                                    },
                                    {
                                        "name": "Loading Started",
                                        "value": "LOADING_STARTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Ready for Pickup",
                                        "value": "TRANS_EXEC_READY_PICK_UP",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Transit",
                                        "value": "TRANS_EXEC_IN_TRANSIT",
                                        "checked": false
                                    },
                                    {
                                        "name": "Delivery confirmed",
                                        "value": "TRANS_EXEC_DELIVERY_CONFIRMED",
                                        "checked": false
                                    }
                                ]
                            },
                            "loadReadyDateTimeOriginTZ": {
                                "type": "date",
                                "dummytime": true,
                                "timeformat": "T",
                                "startTime": "[ 00:00:00.000]",
                                "endTime": "[ 23:59:59.999]",
                                "name": "Carrier Ready Date",
                                "shortName": "Carr. Ready Date",
                                "data": null
                            }
                        },
                        "columns": [
                            {
                                "title": "Shipment #",
                                "field": "shipmentNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 0,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 0
                                }
                            },
                            {
                                "title": "Delivery #",
                                "field": "deliveryNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 1,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 1
                                }
                            },
                            {
                                "title": "Shipment Status",
                                "field": "orderExecutionBucketDesc",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 2,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 2
                                }
                            },
                            {
                                "title": "Order #",
                                "field": "orderNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 3,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 3
                                }
                            },
                            {
                                "title": "Order Status",
                                "field": "orderStatus",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 4,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 4
                                }
                            },
                            {
                                "title": "Order Type",
                                "field": "orderType",
                                "tableData": {
                                    "columnOrder": 5,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 5
                                }
                            },
                            {
                                "title": "Customer",
                                "field": "customer",
                                "tableData": {
                                    "columnOrder": 6,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 6
                                }
                            },
                            {
                                "title": "Origin ID",
                                "field": "originId",
                                "tableData": {
                                    "columnOrder": 7,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 7
                                }
                            },
                            {
                                "title": "Origin",
                                "field": "origin",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 8,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 8
                                }
                            },
                            {
                                "title": "Dest City",
                                "field": "destinationCity",
                                "tableData": {
                                    "columnOrder": 9,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 9
                                }
                            },
                            {
                                "title": "Dest State",
                                "field": "destinationState",
                                "tableData": {
                                    "columnOrder": 10,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 10
                                }
                            },
                            {
                                "title": "Carrier Ready Date & Time",
                                "field": "loadReadyDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 11,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 11
                                }
                            },
                            {
                                "title": "Carrier",
                                "field": "tariffServiceCode",
                                "tableData": {
                                    "columnOrder": 12,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 12
                                }
                            },
                            {
                                "title": "Ship Condition",
                                "field": "shippingCondition",
                                "tableData": {
                                    "columnOrder": 13,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 13
                                }
                            },
                            {
                                "title": "Reason for Alert",
                                "field": "reasonCodeString",
                                "cellStyle": {
                                    "minWidth": 320
                                },
                                "tableData": {
                                    "columnOrder": 14,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 14
                                }
                            },
                            {
                                "title": "Fourkites ETA",
                                "field": "expectedDeliveryDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "disableClick": true
                            }
                        ]
                    },
                    {
                        "id": "5298a993-15c7-47e4-ac59-91f769097ed0",
                        "name": "Blank_2",
                        "default": false,
                        "filters": {
                            "sites": {
                                "type": "sites",
                                "name": "Origin",
                                "data": [
                                    "2042",
                                    "2425",
                                    "2822"
                                ]
                            },
                            "searchString": {
                                "type": "text",
                                "name": "Search String",
                                "data": null
                            },
                            "destName": {
                                "type": "text",
                                "name": "Customer",
                                "data": null
                            },
                            "destState": {
                                "type": "text",
                                "name": "Destination State",
                                "data": null
                            },
                            "destCity": {
                                "type": "text",
                                "name": "Destination City",
                                "data": null
                            },
                            "orderStatusBucket": {
                                "type": "checkbox",
                                "name": "Order Status",
                                "data": [
                                    {
                                        "name": "Order Block Free",
                                        "value": "ORDER_BLOCK_FREE",
                                        "checked": true
                                    },
                                    {
                                        "name": "Order Blocked",
                                        "value": "ORDER_BLOCKED",
                                        "checked": true
                                    }
                                ]
                            },
                            "orderTypes": {
                                "type": "checkbox",
                                "name": "Order Type",
                                "data": [
                                    {
                                        "name": "Cust. Order",
                                        "value": "CUST",
                                        "checked": true
                                    },
                                    {
                                        "name": "STO",
                                        "value": "STO",
                                        "checked": false
                                    }
                                ]
                            },
                            "tariffServiceCode": {
                                "type": "text",
                                "name": "Carrier Service Code",
                                "data": null
                            },
                            "orderExecutionBucket": {
                                "type": "checkbox",
                                "name": "Shipment Status",
                                "data": [
                                    {
                                        "name": "Transportation to be planned",
                                        "value": "TRANS_PLAN_UNASSIGNED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation planning",
                                        "value": "TRANS_PLAN_OPEN",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation processing",
                                        "value": "TRANS_PLAN_PROCESSING",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation Carrier committed",
                                        "value": "TRANS_PLAN_TENDER_ACCEPTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Shipment Created",
                                        "value": "SHIPMENT_CREATED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Checked in by DC",
                                        "value": "CHECK_IN",
                                        "checked": false
                                    },
                                    {
                                        "name": "Loading Started",
                                        "value": "LOADING_STARTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Ready for Pickup",
                                        "value": "TRANS_EXEC_READY_PICK_UP",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Transit",
                                        "value": "TRANS_EXEC_IN_TRANSIT",
                                        "checked": false
                                    },
                                    {
                                        "name": "Delivery confirmed",
                                        "value": "TRANS_EXEC_DELIVERY_CONFIRMED",
                                        "checked": false
                                    }
                                ]
                            },
                            "loadReadyDateTimeOriginTZ": {
                                "type": "date",
                                "dummytime": true,
                                "timeformat": "T",
                                "startTime": "[ 00:00:00.000]",
                                "endTime": "[ 23:59:59.999]",
                                "name": "Carrier Ready Date",
                                "shortName": "Carr. Ready Date",
                                "data": null
                            }
                        },
                        "columns": [
                            {
                                "title": "Shipment #",
                                "field": "shipmentNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 0,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 0
                                }
                            },
                            {
                                "title": "Delivery #",
                                "field": "deliveryNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 1,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 1
                                }
                            },
                            {
                                "title": "Shipment Status",
                                "field": "orderExecutionBucketDesc",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 2,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 2
                                }
                            },
                            {
                                "title": "Order #",
                                "field": "orderNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 3,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 3
                                }
                            },
                            {
                                "title": "Order Status",
                                "field": "orderStatus",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 4,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 4
                                }
                            },
                            {
                                "title": "Order Type",
                                "field": "orderType",
                                "tableData": {
                                    "columnOrder": 5,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 5
                                }
                            },
                            {
                                "title": "Customer",
                                "field": "customer",
                                "tableData": {
                                    "columnOrder": 6,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 6
                                }
                            },
                            {
                                "title": "Origin ID",
                                "field": "originId",
                                "tableData": {
                                    "columnOrder": 7,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 7
                                }
                            },
                            {
                                "title": "Origin",
                                "field": "origin",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 8,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 8
                                }
                            },
                            {
                                "title": "Dest City",
                                "field": "destinationCity",
                                "tableData": {
                                    "columnOrder": 9,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 9
                                }
                            },
                            {
                                "title": "Dest State",
                                "field": "destinationState",
                                "tableData": {
                                    "columnOrder": 10,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 10
                                }
                            },
                            {
                                "title": "Carrier Ready Date & Time",
                                "field": "loadReadyDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 11,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 11
                                }
                            },
                            {
                                "title": "Carrier",
                                "field": "tariffServiceCode",
                                "tableData": {
                                    "columnOrder": 12,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 12
                                }
                            },
                            {
                                "title": "Ship Condition",
                                "field": "shippingCondition",
                                "tableData": {
                                    "columnOrder": 13,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 13
                                }
                            },
                            {
                                "title": "Reason for Alert",
                                "field": "reasonCodeString",
                                "cellStyle": {
                                    "minWidth": 320
                                },
                                "tableData": {
                                    "columnOrder": 14,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 14
                                }
                            },
                            {
                                "title": "Fourkites ETA",
                                "field": "expectedDeliveryDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "disableClick": true
                            }
                        ]
                    },
                    {
                        "id": "15b3c9a9-fe0a-4822-9693-ee020d5f7980",
                        "name": "Wallomart Unhealthy - BlockFree",
                        "default": false,
                        "filters": {
                            "sites": {
                                "type": "sites",
                                "name": "Origin",
                                "data": null
                            },
                            "searchStringList": {
                                "type": "text",
                                "name": "Search",
                                "stringToArray": true,
                                "data": null
                            },
                            "destName": {
                                "type": "text",
                                "name": "Customer",
                                "data": null
                            },
                            "destState": {
                                "type": "text",
                                "name": "Destination State",
                                "data": null
                            },
                            "destCity": {
                                "type": "text",
                                "name": "Destination City",
                                "data": null
                            },
                            "orderStatusBucket": {
                                "type": "checkbox",
                                "name": "Order Status",
                                "data": [
                                    {
                                        "name": "Order Block Free",
                                        "value": "ORDER_BLOCK_FREE",
                                        "checked": false
                                    },
                                    {
                                        "name": "Order Blocked",
                                        "value": "ORDER_BLOCKED",
                                        "checked": true
                                    }
                                ]
                            },
                            "orderTypes": {
                                "type": "checkbox",
                                "name": "Order Type",
                                "data": [
                                    {
                                        "name": "Cust. Order",
                                        "value": "CUST",
                                        "checked": false
                                    },
                                    {
                                        "name": "STO",
                                        "value": "STO",
                                        "checked": false
                                    }
                                ]
                            },
                            "tariffServiceCodeList": {
                                "type": "text",
                                "stringToArray": true,
                                "name": "Carrier Service Code",
                                "data": null
                            },
                            "orderExecutionBucket": {
                                "type": "checkbox",
                                "name": "Shipment Status",
                                "data": [
                                    {
                                        "name": "Transportation to be planned",
                                        "value": "TRANS_PLAN_UNASSIGNED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation planning",
                                        "value": "TRANS_PLAN_OPEN",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation processing",
                                        "value": "TRANS_PLAN_PROCESSING",
                                        "checked": true
                                    },
                                    {
                                        "name": "Transportation Carrier committed",
                                        "value": "TRANS_PLAN_TENDER_ACCEPTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Shipment Created",
                                        "value": "SHIPMENT_CREATED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Checked in by DC",
                                        "value": "CHECK_IN",
                                        "checked": false
                                    },
                                    {
                                        "name": "Loading Started",
                                        "value": "LOADING_STARTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Ready for Pickup",
                                        "value": "TRANS_EXEC_READY_PICK_UP",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Transit",
                                        "value": "TRANS_EXEC_IN_TRANSIT",
                                        "checked": false
                                    },
                                    {
                                        "name": "Delivery confirmed",
                                        "value": "TRANS_EXEC_DELIVERY_CONFIRMED",
                                        "checked": false
                                    }
                                ]
                            },
                            "inboundOrderExecutionBucket": {
                                "type": "checkbox",
                                "name": "Inbound Shipment Status",
                                "data": [
                                    {
                                        "name": "In Transit",
                                        "value": "INBOUND_IN_TRANSIT",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Yard",
                                        "value": "INBOUND_IN_YARD",
                                        "checked": false
                                    },
                                    {
                                        "name": "Unloading",
                                        "value": "INBOUND_UNLOADING",
                                        "checked": false
                                    },
                                    {
                                        "name": "Unloading Completed",
                                        "value": "INBOUND_UNLOADING_COMPLETED",
                                        "checked": false
                                    }
                                ]
                            },
                            "loadReadyDateTimeOriginTZ": {
                                "type": "date",
                                "dummytime": true,
                                "timeformat": "T",
                                "startTime": "[ 00:00:00.000]",
                                "endTime": "[ 23:59:59.999]",
                                "name": "Carrier Ready Date",
                                "shortName": "Carr. Ready Date",
                                "data": null
                            },
                            "searchString": {
                                "type": "text",
                                "name": "Search String",
                                "data": null
                            },
                            "tariffServiceCode": {
                                "type": "text",
                                "name": "Carrier Service Code",
                                "data": null
                            }
                        },
                        "columns": [
                            {
                                "title": "Shipment #",
                                "field": "shipmentNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 0,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 13)",
                                    "id": 0
                                }
                            },
                            {
                                "title": "Shipment Status",
                                "field": "orderExecutionBucketDesc",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 1,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 13)",
                                    "id": 1
                                }
                            },
                            {
                                "title": "Order #",
                                "field": "orderNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 2,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 13)",
                                    "id": 2
                                }
                            },
                            {
                                "title": "Order Status",
                                "field": "orderStatus",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 3,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 13)",
                                    "id": 3
                                }
                            },
                            {
                                "title": "Order Type",
                                "field": "orderType",
                                "tableData": {
                                    "columnOrder": 4,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 13)",
                                    "id": 4
                                }
                            },
                            {
                                "title": "Customer",
                                "field": "customer",
                                "tableData": {
                                    "columnOrder": 5,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 13)",
                                    "id": 5
                                }
                            },
                            {
                                "title": "Origin",
                                "field": "origin",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 6,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 13)",
                                    "id": 6
                                }
                            },
                            {
                                "title": "Dest City",
                                "field": "destinationCity",
                                "tableData": {
                                    "columnOrder": 7,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 13)",
                                    "id": 7
                                }
                            },
                            {
                                "title": "Dest State",
                                "field": "destinationState",
                                "tableData": {
                                    "columnOrder": 8,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 13)",
                                    "id": 8
                                }
                            },
                            {
                                "title": "Carrier Ready Date & Time",
                                "field": "loadReadyDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 9,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 13)",
                                    "id": 9
                                }
                            },
                            {
                                "title": "Carrier",
                                "field": "tariffServiceCode",
                                "tableData": {
                                    "columnOrder": 10,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 13)",
                                    "id": 10
                                }
                            },
                            {
                                "title": "Ship Condition",
                                "field": "shippingCondition",
                                "tableData": {
                                    "columnOrder": 11,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 13)",
                                    "id": 11
                                }
                            },
                            {
                                "title": "Reason for Alert",
                                "field": "reasonCodeString",
                                "cellStyle": {
                                    "minWidth": 320
                                },
                                "tableData": {
                                    "columnOrder": 12,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 13)",
                                    "id": 12
                                }
                            }
                        ]
                    },
                    {
                        "id": "e01242ae-2961-4658-ab9a-5b0dcda171bb",
                        "name": "400error",
                        "default": false,
                        "filters": {
                            "sites": {
                                "type": "sites",
                                "name": "Origin",
                                "data": null
                            },
                            "searchStringList": {
                                "type": "text",
                                "name": "Search",
                                "stringToArray": true,
                                "data": null
                            },
                            "destName": {
                                "type": "text",
                                "name": "Customer",
                                "data": null
                            },
                            "destState": {
                                "type": "text",
                                "name": "Destination State",
                                "data": null
                            },
                            "destCity": {
                                "type": "text",
                                "name": "Destination City",
                                "data": null
                            },
                            "orderStatusBucket": {
                                "type": "checkbox",
                                "name": "Order Status",
                                "data": [
                                    {
                                        "name": "Order Block Free",
                                        "value": "ORDER_BLOCK_FREE",
                                        "checked": false
                                    },
                                    {
                                        "name": "Order Blocked",
                                        "value": "ORDER_BLOCKED",
                                        "checked": true
                                    }
                                ]
                            },
                            "orderTypes": {
                                "type": "checkbox",
                                "name": "Order Type",
                                "data": [
                                    {
                                        "name": "Cust. Order",
                                        "value": "CUST",
                                        "checked": false
                                    },
                                    {
                                        "name": "STO",
                                        "value": "STO",
                                        "checked": false
                                    }
                                ]
                            },
                            "tariffServiceCodeList": {
                                "type": "text",
                                "stringToArray": true,
                                "name": "Carrier Service Code",
                                "data": null
                            },
                            "orderExecutionBucket": {
                                "type": "checkbox",
                                "name": "Shipment Status",
                                "data": [
                                    {
                                        "name": "Transportation to be planned",
                                        "value": "TRANS_PLAN_UNASSIGNED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation planning",
                                        "value": "TRANS_PLAN_OPEN",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation processing",
                                        "value": "TRANS_PLAN_PROCESSING",
                                        "checked": true
                                    },
                                    {
                                        "name": "Transportation Carrier committed",
                                        "value": "TRANS_PLAN_TENDER_ACCEPTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Shipment Created",
                                        "value": "SHIPMENT_CREATED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Checked in by DC",
                                        "value": "CHECK_IN",
                                        "checked": false
                                    },
                                    {
                                        "name": "Loading Started",
                                        "value": "LOADING_STARTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Ready for Pickup",
                                        "value": "TRANS_EXEC_READY_PICK_UP",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Transit",
                                        "value": "TRANS_EXEC_IN_TRANSIT",
                                        "checked": false
                                    },
                                    {
                                        "name": "Delivery confirmed",
                                        "value": "TRANS_EXEC_DELIVERY_CONFIRMED",
                                        "checked": false
                                    }
                                ]
                            },
                            "inboundOrderExecutionBucket": {
                                "type": "checkbox",
                                "name": "Inbound Shipment Status",
                                "data": [
                                    {
                                        "name": "In Transit",
                                        "value": "INBOUND_IN_TRANSIT",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Yard",
                                        "value": "INBOUND_IN_YARD",
                                        "checked": false
                                    },
                                    {
                                        "name": "Unloading",
                                        "value": "INBOUND_UNLOADING",
                                        "checked": false
                                    },
                                    {
                                        "name": "Unloading Completed",
                                        "value": "INBOUND_UNLOADING_COMPLETED",
                                        "checked": false
                                    }
                                ]
                            },
                            "loadReadyDateTimeOriginTZ": {
                                "type": "date",
                                "dummytime": true,
                                "timeformat": "T",
                                "startTime": "[00:00:00.000]",
                                "endTime": "[23:59:59.999]",
                                "name": "Carrier Ready Date",
                                "shortName": "Carr. Ready Date",
                                "data": null
                            }
                        },
                        "columns": [
                            {
                                "title": "Shipment #",
                                "field": "shipmentNum",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Delivery #",
                                "field": "deliveryNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 1,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 1
                                }
                            },
                            {
                                "title": "Shipment Status",
                                "field": "orderExecutionBucketDesc",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 2,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 2
                                }
                            },
                            {
                                "title": "Inbound Shipment Status",
                                "field": "inboundOrderExecutionBucketDesc",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 3,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 3
                                }
                            },
                            {
                                "title": "Order #",
                                "field": "orderNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 4,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 4
                                }
                            },
                            {
                                "title": "Order Status",
                                "field": "orderStatus",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 5,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 5
                                }
                            },
                            {
                                "title": "Order Type",
                                "field": "orderType",
                                "tableData": {
                                    "columnOrder": 6,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 6
                                }
                            },
                            {
                                "title": "Customer",
                                "field": "customer",
                                "tableData": {
                                    "columnOrder": 7,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 7
                                }
                            },
                            {
                                "title": "Origin ID",
                                "field": "originId",
                                "tableData": {
                                    "columnOrder": 8,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 8
                                }
                            },
                            {
                                "title": "Origin",
                                "field": "origin",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 9,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 9
                                }
                            },
                            {
                                "title": "Dest City",
                                "field": "destinationCity",
                                "tableData": {
                                    "columnOrder": 10,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 10
                                }
                            },
                            {
                                "title": "Dest State",
                                "field": "destinationState",
                                "tableData": {
                                    "columnOrder": 11,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 11
                                }
                            },
                            {
                                "title": "Original RDD",
                                "field": "originalRequestedDeliveryDatetime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 12,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 12
                                }
                            },
                            {
                                "title": "Carrier Ready Date & Time",
                                "field": "loadReadyDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 13,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 13
                                }
                            },
                            {
                                "title": "Carrier",
                                "field": "tariffServiceCode",
                                "tableData": {
                                    "columnOrder": 14,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 14
                                }
                            },
                            {
                                "title": "Ship Condition",
                                "field": "shippingCondition",
                                "tableData": {
                                    "columnOrder": 15,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 15
                                }
                            },
                            {
                                "title": "Reason for Alert",
                                "field": "reasonCodeString",
                                "cellStyle": {
                                    "minWidth": 320
                                },
                                "tableData": {
                                    "columnOrder": 16,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 16
                                }
                            },
                            {
                                "title": "Fourkites ETA",
                                "field": "expectedDeliveryDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "disableClick": true
                            },
                            {
                                "title": "Sales Org",
                                "field": "salesOrg",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 18,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 18
                                }
                            },
                            {
                                "title": "Distribution Channel",
                                "field": "distributionChannel",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 19,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 19
                                }
                            },
                            {
                                "title": "Sales Office",
                                "field": "salesOffice",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 20,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 20
                                }
                            },
                            {
                                "title": "Sales Group",
                                "field": "salesGroup",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 21,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 21
                                }
                            }
                        ]
                    }
                ]
            },
            "ORDERS": {
                "tableName": "orders",
                "views": [
                    {
                        "id": "2fc48d20-df7f-43cf-979d-d5868ac2f373",
                        "name": "Another View",
                        "default": true,
                        "filters": {
                            "searchStringList": {
                                "type": "text",
                                "name": "Search",
                                "stringToArray": true,
                                "data": null
                            },
                            "destName": {
                                "type": "text",
                                "name": "Customer",
                                "data": null
                            },
                            "sites": {
                                "type": "sites",
                                "name": "Origin",
                                "data": null
                            },
                            "shippingConditionList": {
                                "type": "text",
                                "stringToArray": true,
                                "name": "Shipping Condition",
                                "data": null
                            },
                            "requestedDeliveryDate": {
                                "type": "date",
                                "name": "Requested Delivery Date",
                                "shortName": "Req. Delivery Date",
                                "data": null
                            },
                            "deliveryBlockCodeList": {
                                "type": "textcheck",
                                "stringToArray": true,
                                "name": "Delivery/Credit Block",
                                "placeholder": "Enter delivery block code",
                                "shortName": "Delivery Block Code",
                                "addon": "creditOnHold",
                                "data": null
                            },
                            "creditOnHold": {
                                "hidden": true,
                                "type": "text",
                                "name": "Show Only Credit Hold Orders",
                                "data": null
                            },
                            "confirmedEquivCubes": {
                                "type": "checkbox",
                                "name": "Confirmed Cube",
                                "shortName": "Confirmed Cube",
                                "data": [
                                    {
                                        "name": "< 1000",
                                        "value": {
                                            "lte": 999.9999999
                                        },
                                        "checked": false
                                    },
                                    {
                                        "name": "1000 - 2000",
                                        "value": {
                                            "gte": 1000,
                                            "lte": 1999.9999999
                                        },
                                        "checked": false
                                    },
                                    {
                                        "name": "2000 - 3000",
                                        "value": {
                                            "gte": 2000,
                                            "lte": 2999.9999999
                                        },
                                        "checked": false
                                    },
                                    {
                                        "name": "> 3000",
                                        "value": {
                                            "gte": 3000
                                        },
                                        "checked": false
                                    }
                                ]
                            },
                            "destState": {
                                "type": "text",
                                "name": "Destination State",
                                "data": "QLD"
                            },
                            "destCity": {
                                "type": "text",
                                "name": "Destination City",
                                "data": null
                            },
                            "matAvailDate": {
                                "type": "date",
                                "name": "Material Availability Date",
                                "shortName": "Mat.Avail Date",
                                "data": null
                            },
                            "orderStatusBucket": {
                                "type": "ordercheckbox",
                                "name": "Order Status",
                                "data": [
                                    {
                                        "name": "100% Confirmed Cube",
                                        "value": "SO_COMPLETELY_CONFIRMED_CUBE",
                                        "checked": false,
                                        "parent": "Order Block Free"
                                    },
                                    {
                                        "name": "Less than 100% Confirmed Cube",
                                        "value": "SO_NOT_COMPLETELY_CONFIRMED_CUBE",
                                        "checked": false,
                                        "parent": "Order Block Free"
                                    },
                                    {
                                        "name": "Back Orders Block Free",
                                        "value": "SO_BACK_ORDER_BLOCK_FREE",
                                        "checked": false,
                                        "parent": "Order Block Free"
                                    },
                                    {
                                        "name": "Visible in TMS",
                                        "value": "SO_BLOCKED_TMS_PLANNED_VISIBLE",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Not visible in TMS",
                                        "value": "SO_BLOCKED_TMS_PLANNED_NOT_VISIBLE",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Pickup not scheduled",
                                        "value": "SO_BLOCKED_NON_TMS_PLANNED_PICKUP_NOT_SCHEDULED",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Pickup/Package Multi Block",
                                        "value": "SO_BLOCKED_NON_TMS_PLANNED_PICKUP_MULTI_BLOCK",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Immediate Action",
                                        "value": "SO_BLOCKED_NON_TMS_PLANNED_IMMEDIATE_ACTION",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Back Orders Blocked",
                                        "value": "SO_BACK_ORDER_BLOCKED",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    }
                                ]
                            },
                            "orderExecutionBucket": {
                                "type": "checkbox",
                                "name": "Shipment Status",
                                "data": [
                                    {
                                        "name": "Transportation to be planned",
                                        "value": "TRANS_PLAN_UNASSIGNED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation planning",
                                        "value": "TRANS_PLAN_OPEN",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation processing",
                                        "value": "TRANS_PLAN_PROCESSING",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation Carrier committed",
                                        "value": "TRANS_PLAN_TENDER_ACCEPTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Shipment Created",
                                        "value": "SHIPMENT_CREATED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Checked in by DC",
                                        "value": "CHECK_IN",
                                        "checked": false
                                    },
                                    {
                                        "name": "Loading Started",
                                        "value": "LOADING_STARTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Ready for Pickup",
                                        "value": "TRANS_EXEC_READY_PICK_UP",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Transit",
                                        "value": "TRANS_EXEC_IN_TRANSIT",
                                        "checked": false
                                    },
                                    {
                                        "name": "Delivery confirmed",
                                        "value": "TRANS_EXEC_DELIVERY_CONFIRMED",
                                        "checked": false
                                    }
                                ]
                            },
                            "inboundOrderExecutionBucket": {
                                "type": "checkbox",
                                "name": "Inbound Shipment Status",
                                "data": [
                                    {
                                        "name": "In Transit",
                                        "value": "INBOUND_IN_TRANSIT",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Yard",
                                        "value": "INBOUND_IN_YARD",
                                        "checked": false
                                    },
                                    {
                                        "name": "Unloading",
                                        "value": "INBOUND_UNLOADING",
                                        "checked": false
                                    },
                                    {
                                        "name": "Unloading Completed",
                                        "value": "INBOUND_UNLOADING_COMPLETED",
                                        "checked": false
                                    }
                                ]
                            },
                            "orderTypes": {
                                "type": "checkbox",
                                "name": "Order Type",
                                "data": [
                                    {
                                        "name": "All Customer Orders",
                                        "value": "CUST",
                                        "checked": true
                                    },
                                    {
                                        "name": "STOs",
                                        "value": "STO",
                                        "checked": false
                                    }
                                ]
                            },
                            "documentTypes": {
                                "type": "checkbox",
                                "name": "Document type",
                                "data": [
                                    {
                                        "name": "Standard Orders (ZSTD)",
                                        "value": "ZSTD",
                                        "checked": false
                                    },
                                    {
                                        "name": "Multi Plant Orders (ZMPF)",
                                        "value": "ZMPF",
                                        "checked": false
                                    },
                                    {
                                        "name": "VMI Orders (ZVMI)",
                                        "value": "ZVMI",
                                        "checked": false
                                    },
                                    {
                                        "name": "Merged Orders (ZMER)",
                                        "value": "ZMER",
                                        "checked": false
                                    }
                                ]
                            },
                            "orderHealth": {
                                "type": "checkbox",
                                "name": "Health",
                                "data": [
                                    {
                                        "name": "Unhealthy",
                                        "value": "RED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Needs Attention",
                                        "value": "YELLOW",
                                        "checked": false
                                    },
                                    {
                                        "name": "Healthy",
                                        "value": "GREEN",
                                        "checked": false
                                    }
                                ]
                            }
                        },
                        "columns": [
                            {
                                "title": "Order #",
                                "field": "orderNum",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Customer",
                                "field": "customer",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Customer PO #",
                                "field": "customerPoNum",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "LEQ",
                                "field": "loadEquivCube"
                            },
                            {
                                "title": "Confirmed LEQ",
                                "field": "confLoadEquivCube"
                            },
                            {
                                "title": "Allocated LEQ",
                                "field": "allocEquivCube"
                            },
                            {
                                "title": "Origin ID",
                                "field": "originId"
                            },
                            {
                                "title": "Origin",
                                "field": "origin",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "MAD",
                                "field": "matAvailDate",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Original RDD",
                                "field": "originalRequestedDeliveryDatetime",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "RDD",
                                "field": "requestedDeliveryDate",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Ship Condition",
                                "field": "shippingCondition"
                            },
                            {
                                "title": "Delivery Block",
                                "field": "deliveryBlocksString",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Shipment #",
                                "field": "shipmentNum"
                            },
                            {
                                "title": "Delivery #",
                                "field": "deliveryNum",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Carrier Ready Date & Time",
                                "field": "loadReadyDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Dest City",
                                "field": "destinationCity"
                            },
                            {
                                "title": "Dest State",
                                "field": "destinationState"
                            },
                            {
                                "title": "Trailer #",
                                "field": "trailerNum"
                            },
                            {
                                "title": "Shipment Status",
                                "field": "orderExecutionBucketDesc",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Inbound Shipment Status",
                                "field": "inboundOrderExecutionBucketDesc",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Order Status",
                                "field": "orderStatus",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Order Sub Status",
                                "field": "orderStatusBucketDesc",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Order Type",
                                "field": "orderType"
                            },
                            {
                                "title": "Business",
                                "field": "orderBusinessUnit"
                            },
                            {
                                "title": "Reason for Alert",
                                "field": "reasonCodeString",
                                "cellStyle": {
                                    "minWidth": 320
                                }
                            },
                            {
                                "title": "Fourkites ETA",
                                "field": "expectedDeliveryDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Sales Org",
                                "field": "salesOrg",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Distribution Channel",
                                "field": "distributionChannel",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Sales Office",
                                "field": "salesOffice",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Sales Group",
                                "field": "salesGroup",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            }
                        ]
                    },
                    {
                        "id": "b2e66d3b-34e5-4c23-993c-204bfd5097dc",
                        "name": "Another View-2",
                        "default": false,
                        "filters": {
                            "searchStringList": {
                                "type": "text",
                                "name": "Search",
                                "stringToArray": true,
                                "data": null
                            },
                            "destName": {
                                "type": "text",
                                "name": "Customer",
                                "data": null
                            },
                            "sites": {
                                "type": "sites",
                                "name": "Origin",
                                "data": null
                            },
                            "shippingConditionList": {
                                "type": "text",
                                "stringToArray": true,
                                "name": "Shipping Condition",
                                "data": null
                            },
                            "requestedDeliveryDate": {
                                "type": "date",
                                "name": "Requested Delivery Date",
                                "shortName": "Req. Delivery Date",
                                "data": null
                            },
                            "deliveryBlockCodeList": {
                                "type": "textcheck",
                                "stringToArray": true,
                                "name": "Delivery/Credit Block",
                                "placeholder": "Enter delivery block code",
                                "shortName": "Delivery Block Code",
                                "addon": "creditOnHold",
                                "data": null
                            },
                            "creditOnHold": {
                                "hidden": true,
                                "type": "text",
                                "name": "Show Only Credit Hold Orders",
                                "data": null
                            },
                            "confirmedEquivCubes": {
                                "type": "checkbox",
                                "name": "Confirmed Cube",
                                "shortName": "Confirmed Cube",
                                "data": [
                                    {
                                        "name": "< 1000",
                                        "value": {
                                            "lte": 999.9999999
                                        },
                                        "checked": false
                                    },
                                    {
                                        "name": "1000 - 2000",
                                        "value": {
                                            "gte": 1000,
                                            "lte": 1999.9999999
                                        },
                                        "checked": false
                                    },
                                    {
                                        "name": "2000 - 3000",
                                        "value": {
                                            "gte": 2000,
                                            "lte": 2999.9999999
                                        },
                                        "checked": false
                                    },
                                    {
                                        "name": "> 3000",
                                        "value": {
                                            "gte": 3000
                                        },
                                        "checked": false
                                    }
                                ]
                            },
                            "destState": {
                                "type": "text",
                                "name": "Destination State",
                                "data": null
                            },
                            "destCity": {
                                "type": "text",
                                "name": "Destination City",
                                "data": null
                            },
                            "matAvailDate": {
                                "type": "date",
                                "name": "Material Availability Date",
                                "shortName": "Mat.Avail Date",
                                "data": null
                            },
                            "orderStatusBucket": {
                                "type": "ordercheckbox",
                                "name": "Order Status",
                                "data": [
                                    {
                                        "name": "100% Confirmed Cube",
                                        "value": "SO_COMPLETELY_CONFIRMED_CUBE",
                                        "checked": false,
                                        "parent": "Order Block Free"
                                    },
                                    {
                                        "name": "Less than 100% Confirmed Cube",
                                        "value": "SO_NOT_COMPLETELY_CONFIRMED_CUBE",
                                        "checked": false,
                                        "parent": "Order Block Free"
                                    },
                                    {
                                        "name": "Back Orders Block Free",
                                        "value": "SO_BACK_ORDER_BLOCK_FREE",
                                        "checked": false,
                                        "parent": "Order Block Free"
                                    },
                                    {
                                        "name": "Visible in TMS",
                                        "value": "SO_BLOCKED_TMS_PLANNED_VISIBLE",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Not visible in TMS",
                                        "value": "SO_BLOCKED_TMS_PLANNED_NOT_VISIBLE",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Pickup not scheduled",
                                        "value": "SO_BLOCKED_NON_TMS_PLANNED_PICKUP_NOT_SCHEDULED",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Pickup/Package Multi Block",
                                        "value": "SO_BLOCKED_NON_TMS_PLANNED_PICKUP_MULTI_BLOCK",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Immediate Action",
                                        "value": "SO_BLOCKED_NON_TMS_PLANNED_IMMEDIATE_ACTION",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Back Orders Blocked",
                                        "value": "SO_BACK_ORDER_BLOCKED",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    }
                                ]
                            },
                            "orderExecutionBucket": {
                                "type": "checkbox",
                                "name": "Shipment Status",
                                "data": [
                                    {
                                        "name": "Transportation to be planned",
                                        "value": "TRANS_PLAN_UNASSIGNED",
                                        "checked": true
                                    },
                                    {
                                        "name": "Transportation planning",
                                        "value": "TRANS_PLAN_OPEN",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation processing",
                                        "value": "TRANS_PLAN_PROCESSING",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation Carrier committed",
                                        "value": "TRANS_PLAN_TENDER_ACCEPTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Shipment Created",
                                        "value": "SHIPMENT_CREATED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Checked in by DC",
                                        "value": "CHECK_IN",
                                        "checked": false
                                    },
                                    {
                                        "name": "Loading Started",
                                        "value": "LOADING_STARTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Ready for Pickup",
                                        "value": "TRANS_EXEC_READY_PICK_UP",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Transit",
                                        "value": "TRANS_EXEC_IN_TRANSIT",
                                        "checked": false
                                    },
                                    {
                                        "name": "Delivery confirmed",
                                        "value": "TRANS_EXEC_DELIVERY_CONFIRMED",
                                        "checked": false
                                    }
                                ]
                            },
                            "inboundOrderExecutionBucket": {
                                "type": "checkbox",
                                "name": "Inbound Shipment Status",
                                "data": [
                                    {
                                        "name": "In Transit",
                                        "value": "INBOUND_IN_TRANSIT",
                                        "checked": true
                                    },
                                    {
                                        "name": "In Yard",
                                        "value": "INBOUND_IN_YARD",
                                        "checked": true
                                    },
                                    {
                                        "name": "Unloading",
                                        "value": "INBOUND_UNLOADING",
                                        "checked": false
                                    },
                                    {
                                        "name": "Unloading Completed",
                                        "value": "INBOUND_UNLOADING_COMPLETED",
                                        "checked": false
                                    }
                                ]
                            },
                            "orderTypes": {
                                "type": "checkbox",
                                "name": "Order Type",
                                "data": [
                                    {
                                        "name": "All Customer Orders",
                                        "value": "CUST",
                                        "checked": true
                                    },
                                    {
                                        "name": "STOs",
                                        "value": "STO",
                                        "checked": false
                                    }
                                ]
                            },
                            "documentTypes": {
                                "type": "checkbox",
                                "name": "Document type",
                                "data": [
                                    {
                                        "name": "Standard Orders (ZSTD)",
                                        "value": "ZSTD",
                                        "checked": false
                                    },
                                    {
                                        "name": "Multi Plant Orders (ZMPF)",
                                        "value": "ZMPF",
                                        "checked": false
                                    },
                                    {
                                        "name": "VMI Orders (ZVMI)",
                                        "value": "ZVMI",
                                        "checked": false
                                    },
                                    {
                                        "name": "Merged Orders (ZMER)",
                                        "value": "ZMER",
                                        "checked": false
                                    }
                                ]
                            },
                            "orderHealth": {
                                "type": "checkbox",
                                "name": "Health",
                                "data": [
                                    {
                                        "name": "Unhealthy",
                                        "value": "RED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Needs Attention",
                                        "value": "YELLOW",
                                        "checked": false
                                    },
                                    {
                                        "name": "Healthy",
                                        "value": "GREEN",
                                        "checked": true
                                    }
                                ]
                            }
                        },
                        "columns": [
                            {
                                "title": "Order #",
                                "field": "orderNum",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Customer",
                                "field": "customer",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 1,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 1
                                }
                            },
                            {
                                "title": "Customer PO #",
                                "field": "customerPoNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 2,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 2
                                }
                            },
                            {
                                "title": "LEQ",
                                "field": "loadEquivCube",
                                "tableData": {
                                    "columnOrder": 3,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 3
                                }
                            },
                            {
                                "title": "Confirmed LEQ",
                                "field": "confLoadEquivCube",
                                "tableData": {
                                    "columnOrder": 4,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 4
                                }
                            },
                            {
                                "title": "Allocated LEQ",
                                "field": "allocEquivCube",
                                "tableData": {
                                    "columnOrder": 5,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 5
                                }
                            },
                            {
                                "title": "Origin ID",
                                "field": "originId",
                                "tableData": {
                                    "columnOrder": 6,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 6
                                }
                            },
                            {
                                "title": "Origin",
                                "field": "origin",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 7,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 7
                                }
                            },
                            {
                                "title": "MAD",
                                "field": "matAvailDate",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 8,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 8
                                }
                            },
                            {
                                "title": "Original RDD",
                                "field": "originalRequestedDeliveryDatetime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 9,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 9
                                }
                            },
                            {
                                "title": "RDD",
                                "field": "requestedDeliveryDate",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 10,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 10
                                }
                            },
                            {
                                "title": "Ship Condition",
                                "field": "shippingCondition",
                                "tableData": {
                                    "columnOrder": 11,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 11
                                }
                            },
                            {
                                "title": "Delivery Block",
                                "field": "deliveryBlocksString",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 12,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 12
                                }
                            },
                            {
                                "title": "Shipment #",
                                "field": "shipmentNum",
                                "tableData": {
                                    "columnOrder": 13,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 13
                                }
                            },
                            {
                                "title": "Delivery #",
                                "field": "deliveryNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 14,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 14
                                }
                            },
                            {
                                "title": "Carrier Ready Date & Time",
                                "field": "loadReadyDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 15,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 15
                                }
                            },
                            {
                                "title": "Dest City",
                                "field": "destinationCity",
                                "tableData": {
                                    "columnOrder": 16,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 16
                                }
                            },
                            {
                                "title": "Dest State",
                                "field": "destinationState",
                                "tableData": {
                                    "columnOrder": 17,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 17
                                }
                            },
                            {
                                "title": "Trailer #",
                                "field": "trailerNum",
                                "tableData": {
                                    "columnOrder": 18,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 18
                                }
                            },
                            {
                                "title": "Shipment Status",
                                "field": "orderExecutionBucketDesc",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 19,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 19
                                }
                            },
                            {
                                "title": "Inbound Shipment Status",
                                "field": "inboundOrderExecutionBucketDesc",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 20,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 20
                                }
                            },
                            {
                                "title": "Order Status",
                                "field": "orderStatus",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 21,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 21
                                }
                            },
                            {
                                "title": "Order Sub Status",
                                "field": "orderStatusBucketDesc",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 22,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 22
                                }
                            },
                            {
                                "title": "Order Type",
                                "field": "orderType",
                                "tableData": {
                                    "columnOrder": 23,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 23
                                }
                            },
                            {
                                "title": "Business",
                                "field": "orderBusinessUnit",
                                "tableData": {
                                    "columnOrder": 24,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 24
                                }
                            },
                            {
                                "title": "Reason for Alert",
                                "field": "reasonCodeString",
                                "cellStyle": {
                                    "minWidth": 320
                                },
                                "tableData": {
                                    "columnOrder": 25,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 25
                                }
                            },
                            {
                                "title": "Fourkites ETA",
                                "field": "expectedDeliveryDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Sales Org",
                                "field": "salesOrg",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 27,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 27
                                }
                            },
                            {
                                "title": "Distribution Channel",
                                "field": "distributionChannel",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 28,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 28
                                }
                            },
                            {
                                "title": "Sales Office",
                                "field": "salesOffice",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 29,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 29
                                }
                            },
                            {
                                "title": "Sales Group",
                                "field": "salesGroup",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 30,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 30
                                }
                            }
                        ]
                    }
                ]
            },
            "DISTINBOUND": {
                "tableName": "distinbound",
                "views": [
                    {
                        "id": 1,
                        "name": "Inbound 1",
                        "default": true,
                        "filters": {
                            "searchString": {
                                "type": "text",
                                "name": "Search String",
                                "data": null
                            },
                            "inboundOrderExecutionBucket": {
                                "type": "checkbox",
                                "name": "Shipment Status",
                                "data": [
                                    {
                                        "name": "In Transit",
                                        "value": "INBOUND_IN_TRANSIT",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Yard",
                                        "value": "INBOUND_IN_YARD",
                                        "checked": false
                                    },
                                    {
                                        "name": "Unloading",
                                        "value": "INBOUND_UNLOADING",
                                        "checked": false
                                    },
                                    {
                                        "name": "Unloading Completed",
                                        "value": "INBOUND_UNLOADING_COMPLETED",
                                        "checked": false
                                    }
                                ]
                            },
                            "destState": {
                                "type": "text",
                                "name": "Destination State",
                                "data": null
                            },
                            "destCity": {
                                "type": "text",
                                "name": "Destination City",
                                "data": null
                            },
                            "yardArrivalDateTimeDestTZ": {
                                "type": "date",
                                "dummytime": true,
                                "timeformat": "T",
                                "startTime": "[ 00:00:00.000]",
                                "endTime": "[ 23:59:59.999]",
                                "name": "Date Arrived in Yard",
                                "shortName": "Yard Arrival",
                                "data": null
                            },
                            "durationInYard": {
                                "type": "checkboxradio",
                                "name": "Duration in Yard",
                                "data": [
                                    {
                                        "name": "< 24 Hours",
                                        "value": {
                                            "startTimeMins": null,
                                            "endTimeMins": 1439
                                        },
                                        "checked": false
                                    },
                                    {
                                        "name": "24 - 48 Hours",
                                        "value": {
                                            "startTimeMins": 1440,
                                            "endTimeMins": 2880
                                        },
                                        "checked": false
                                    },
                                    {
                                        "name": "> 72 Hours",
                                        "value": {
                                            "startTimeMins": 4321,
                                            "endTimeMins": null
                                        },
                                        "checked": true
                                    }
                                ]
                            },
                            "inboundOrderExecutionHealth": {
                                "type": "checkbox",
                                "name": "Health",
                                "data": [
                                    {
                                        "name": "Unhealthy",
                                        "value": "RED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Needs Attention",
                                        "value": "YELLOW",
                                        "checked": false
                                    },
                                    {
                                        "name": "Healthy",
                                        "value": "GREEN",
                                        "checked": false
                                    }
                                ]
                            }
                        },
                        "columns": [
                            {
                                "title": "Order #",
                                "field": "orderNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 0,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 9)",
                                    "id": 0
                                },
                                "hidden": false
                            },
                            {
                                "title": "Duration in Yard",
                                "field": "durationInYardMS",
                                "tableData": {
                                    "columnOrder": 1,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 11)",
                                    "id": 1
                                },
                                "hidden": true
                            },
                            {
                                "title": "Origin",
                                "field": "origin",
                                "tableData": {
                                    "columnOrder": 2,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 11)",
                                    "id": 1
                                },
                                "hidden": true
                            },
                            {
                                "title": "Date and Time arrived in Yard",
                                "field": "yardArrivalDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 3,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 9)",
                                    "id": 3
                                },
                                "hidden": false
                            },
                            {
                                "title": "Shipment #",
                                "field": "shipmentNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 4,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 6)",
                                    "id": 2
                                },
                                "hidden": false
                            },
                            {
                                "title": "Dest",
                                "field": "customer",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 5,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 6)",
                                    "id": 3
                                },
                                "hidden": false
                            },
                            {
                                "title": "Trailer # ",
                                "field": "trailerNum",
                                "tableData": {
                                    "columnOrder": 6,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 9)",
                                    "id": 6
                                },
                                "hidden": false
                            },
                            {
                                "title": "Dest State",
                                "field": "destinationState",
                                "tableData": {
                                    "columnOrder": 7,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 6)",
                                    "id": 4
                                },
                                "hidden": false
                            },
                            {
                                "title": "Ship Condition",
                                "field": "shippingCondition",
                                "tableData": {
                                    "columnOrder": 8,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 6)",
                                    "id": 5
                                },
                                "hidden": false
                            },
                            {
                                "title": "Dest City",
                                "field": "destinationCity",
                                "tableData": {
                                    "columnOrder": 9,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 6)",
                                    "id": 6
                                },
                                "hidden": false
                            },
                            {
                                "title": "Shipment Status",
                                "field": "inboundOrderExecutionBucketDesc",
                                "tableData": {
                                    "columnOrder": 10,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 9)",
                                    "id": 10
                                },
                                "hidden": false
                            }
                        ]
                    },
                    {
                        "id": 2,
                        "name": "Inbound 2",
                        "default": false,
                        "filters": {
                            "searchString": {
                                "type": "text",
                                "name": "Search String",
                                "data": null
                            },
                            "inboundOrderExecutionBucket": {
                                "type": "checkbox",
                                "name": "Shipment Status",
                                "data": [
                                    {
                                        "name": "In Transit",
                                        "value": "INBOUND_IN_TRANSIT",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Yard",
                                        "value": "INBOUND_IN_YARD",
                                        "checked": true
                                    },
                                    {
                                        "name": "Unloading",
                                        "value": "INBOUND_UNLOADING",
                                        "checked": true
                                    },
                                    {
                                        "name": "Unloading Completed",
                                        "value": "INBOUND_UNLOADING_COMPLETED",
                                        "checked": false
                                    }
                                ]
                            },
                            "destState": {
                                "type": "text",
                                "name": "Destination State",
                                "data": null
                            },
                            "destCity": {
                                "type": "text",
                                "name": "Destination City",
                                "data": null
                            },
                            "yardArrivalDateTimeDestTZ": {
                                "type": "date",
                                "dummytime": true,
                                "timeformat": "T",
                                "startTime": "[ 00:00:00.000]",
                                "endTime": "[ 23:59:59.999]",
                                "name": "Date Arrived in Yard",
                                "shortName": "Yard Arrival",
                                "data": null
                            },
                            "durationInYard": {
                                "type": "checkboxradio",
                                "name": "Duration in Yard",
                                "data": [
                                    {
                                        "name": "< 24 Hours",
                                        "value": {
                                            "startTimeMins": null,
                                            "endTimeMins": 1439
                                        },
                                        "checked": false
                                    },
                                    {
                                        "name": "24 - 48 Hours",
                                        "value": {
                                            "startTimeMins": 1440,
                                            "endTimeMins": 2880
                                        },
                                        "checked": false
                                    },
                                    {
                                        "name": "> 72 Hours",
                                        "value": {
                                            "startTimeMins": 4321,
                                            "endTimeMins": null
                                        },
                                        "checked": false
                                    }
                                ]
                            },
                            "inboundOrderExecutionHealth": {
                                "type": "checkbox",
                                "name": "Health",
                                "data": [
                                    {
                                        "name": "Unhealthy",
                                        "value": "RED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Needs Attention",
                                        "value": "YELLOW",
                                        "checked": false
                                    },
                                    {
                                        "name": "Healthy",
                                        "value": "GREEN",
                                        "checked": false
                                    }
                                ]
                            }
                        },
                        "columns": [
                            {
                                "title": "Shipment #",
                                "field": "shipmentNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 3,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 11)",
                                    "id": 0
                                },
                                "hidden": false
                            },
                            {
                                "title": "Order #",
                                "field": "orderNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 0,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 11)",
                                    "id": 1
                                },
                                "hidden": false
                            },
                            {
                                "title": "Origin",
                                "field": "origin",
                                "tableData": {
                                    "columnOrder": 1,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 11)",
                                    "id": 2
                                },
                                "hidden": false
                            },
                            {
                                "title": "Dest",
                                "field": "customer",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 2,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 11)",
                                    "id": 3
                                },
                                "hidden": false
                            },
                            {
                                "title": "Date and Time arrived in Yard",
                                "field": "yardArrivalDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 4,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 11)",
                                    "id": 4
                                },
                                "hidden": false
                            },
                            {
                                "title": "Duration in Yard",
                                "field": "durationInYardMS",
                                "tableData": {
                                    "columnOrder": 5,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 11)",
                                    "id": 5
                                },
                                "hidden": false
                            },
                            {
                                "title": "Trailer # ",
                                "field": "trailerNum",
                                "tableData": {
                                    "columnOrder": 6,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 11)",
                                    "id": 6
                                },
                                "hidden": false
                            },
                            {
                                "title": "Shipment Status",
                                "field": "inboundOrderExecutionBucketDesc",
                                "tableData": {
                                    "columnOrder": 7,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 11)",
                                    "id": 7
                                },
                                "hidden": false
                            },
                            {
                                "title": "Ship Condition",
                                "field": "shippingCondition",
                                "tableData": {
                                    "columnOrder": 8,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 11)",
                                    "id": 8
                                },
                                "hidden": false
                            },
                            {
                                "title": "Dest City",
                                "field": "destinationCity",
                                "tableData": {
                                    "columnOrder": 9,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 11)",
                                    "id": 9
                                },
                                "hidden": false
                            },
                            {
                                "title": "Dest State",
                                "field": "destinationState",
                                "tableData": {
                                    "columnOrder": 10,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 11)",
                                    "id": 10
                                },
                                "hidden": false
                            }
                        ]
                    },
                    {
                        "id": "2a7f7316-dab3-44cd-9b2f-376391b4480d",
                        "name": "4k-s5",
                        "default": false,
                        "filters": {
                            "searchString": {
                                "type": "text",
                                "name": "Search String",
                                "data": "4k-s5"
                            },
                            "inboundOrderExecutionBucket": {
                                "type": "checkbox",
                                "name": "Shipment Status",
                                "data": [
                                    {
                                        "name": "In Transit",
                                        "value": "INBOUND_IN_TRANSIT",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Yard",
                                        "value": "INBOUND_IN_YARD",
                                        "checked": false
                                    },
                                    {
                                        "name": "Unloading",
                                        "value": "INBOUND_UNLOADING",
                                        "checked": false
                                    },
                                    {
                                        "name": "Unloading Completed",
                                        "value": "INBOUND_UNLOADING_COMPLETED",
                                        "checked": false
                                    }
                                ]
                            },
                            "destState": {
                                "type": "text",
                                "name": "Destination State",
                                "data": null
                            },
                            "destCity": {
                                "type": "text",
                                "name": "Destination City",
                                "data": null
                            },
                            "yardArrivalDateTimeDestTZ": {
                                "type": "date",
                                "dummytime": true,
                                "timeformat": "T",
                                "startTime": "[ 00:00:00.000]",
                                "endTime": "[ 23:59:59.999]",
                                "name": "Date Arrived in Yard",
                                "shortName": "Yard Arrival",
                                "data": null
                            },
                            "durationInYard": {
                                "type": "checkboxradio",
                                "name": "Duration in Yard",
                                "data": [
                                    {
                                        "name": "< 24 Hours",
                                        "value": {
                                            "startTimeMins": null,
                                            "endTimeMins": 1439
                                        },
                                        "checked": false
                                    },
                                    {
                                        "name": "24 - 48 Hours",
                                        "value": {
                                            "startTimeMins": 1440,
                                            "endTimeMins": 2880
                                        },
                                        "checked": false
                                    },
                                    {
                                        "name": "> 72 Hours",
                                        "value": {
                                            "startTimeMins": 4321,
                                            "endTimeMins": null
                                        },
                                        "checked": false
                                    }
                                ]
                            },
                            "inboundOrderExecutionHealth": {
                                "type": "checkbox",
                                "name": "Health",
                                "data": [
                                    {
                                        "name": "Unhealthy",
                                        "value": "RED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Needs Attention",
                                        "value": "YELLOW",
                                        "checked": false
                                    },
                                    {
                                        "name": "Healthy",
                                        "value": "GREEN",
                                        "checked": false
                                    }
                                ]
                            }
                        },
                        "columns": [
                            {
                                "title": "Shipment #",
                                "field": "shipmentNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 0,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 0
                                }
                            },
                            {
                                "title": "Delivery #",
                                "field": "deliveryNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 1,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 1
                                }
                            },
                            {
                                "title": "Order #",
                                "field": "orderNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 2,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 2
                                }
                            },
                            {
                                "title": "Origin ID",
                                "field": "originId",
                                "tableData": {
                                    "columnOrder": 3,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 3
                                }
                            },
                            {
                                "title": "Origin",
                                "field": "origin",
                                "tableData": {
                                    "columnOrder": 4,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 4
                                }
                            },
                            {
                                "title": "Dest",
                                "field": "customer",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 5,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 5
                                }
                            },
                            {
                                "title": "Date and Time arrived in Yard",
                                "field": "yardArrivalDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 6,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 6
                                }
                            },
                            {
                                "title": "Duration in Yard",
                                "field": "durationInYardMS",
                                "tableData": {
                                    "columnOrder": 7,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 7
                                }
                            },
                            {
                                "title": "Trailer # ",
                                "field": "trailerNum",
                                "tableData": {
                                    "columnOrder": 8,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 8
                                }
                            },
                            {
                                "title": "Shipment Status",
                                "field": "inboundOrderExecutionBucketDesc",
                                "tableData": {
                                    "columnOrder": 9,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 9
                                }
                            },
                            {
                                "title": "Ship Condition",
                                "field": "shippingCondition",
                                "tableData": {
                                    "columnOrder": 10,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 10
                                }
                            },
                            {
                                "title": "Dest City",
                                "field": "destinationCity",
                                "tableData": {
                                    "columnOrder": 11,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 11
                                }
                            },
                            {
                                "title": "Dest State",
                                "field": "destinationState",
                                "tableData": {
                                    "columnOrder": 12,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 12
                                }
                            },
                            {
                                "title": "Business",
                                "field": "orderBusinessUnit",
                                "cellStyle": {
                                    "minWidth": 320
                                },
                                "tableData": {
                                    "columnOrder": 13,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 13
                                }
                            },
                            {
                                "title": "Reason for Alert",
                                "field": "reasonCodeString",
                                "tableData": {
                                    "columnOrder": 14,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 14
                                }
                            },
                            {
                                "title": "Fourkites ETA",
                                "field": "expectedDeliveryDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "disableClick": true
                            }
                        ]
                    }
                ]
            },
            "DISTOUTBOUND": {
                "tableName": "distoutbound",
                "views": []
            },
            "TRANSPORT": {
                "tableName": "transport",
                "views": [
                    {
                        "id": "5c7dc53c-addd-4463-a8c5-44c89c62aed0",
                        "name": "4ks-5",
                        "default": false,
                        "filters": {
                            "searchString": {
                                "type": "text",
                                "name": "Search String",
                                "data": "4k-s5"
                            },
                            "sites": {
                                "type": "sites",
                                "name": "Origin",
                                "data": null
                            },
                            "destName": {
                                "type": "text",
                                "name": "Customer",
                                "data": null
                            },
                            "orderExecutionBucket": {
                                "type": "checkbox",
                                "name": "Shipment Status",
                                "data": [
                                    {
                                        "name": "Transportation to be planned",
                                        "value": "TRANS_PLAN_UNASSIGNED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation planning",
                                        "value": "TRANS_PLAN_OPEN",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation processing",
                                        "value": "TRANS_PLAN_PROCESSING",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation Carrier committed",
                                        "value": "TRANS_PLAN_TENDER_ACCEPTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Ready for Pickup",
                                        "value": "TRANS_EXEC_READY_PICK_UP",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Transit",
                                        "value": "TRANS_EXEC_IN_TRANSIT",
                                        "checked": false
                                    },
                                    {
                                        "name": "Delivery confirmed",
                                        "value": "TRANS_EXEC_DELIVERY_CONFIRMED",
                                        "checked": false
                                    }
                                ]
                            },
                            "tmsShipmentStatus": {
                                "type": "checkbox",
                                "name": "TM Operational Status",
                                "data": [
                                    {
                                        "name": "Unassigned",
                                        "value": "Unassigned",
                                        "checked": false
                                    },
                                    {
                                        "name": "Open",
                                        "value": "Open",
                                        "checked": false
                                    },
                                    {
                                        "name": "Planned",
                                        "value": "Planned",
                                        "checked": false
                                    },
                                    {
                                        "name": "Tendered",
                                        "value": "Tendered",
                                        "checked": false
                                    },
                                    {
                                        "name": "Tender Rejected",
                                        "value": "Tender Rejected",
                                        "checked": false
                                    },
                                    {
                                        "name": "Tender Accepted",
                                        "value": "Tender Accepted",
                                        "checked": false
                                    },
                                    {
                                        "name": "Confirming",
                                        "value": "Confirming",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Transit",
                                        "value": "In Transit",
                                        "checked": false
                                    },
                                    {
                                        "name": "Completed",
                                        "value": "Completed",
                                        "checked": false
                                    }
                                ]
                            },
                            "appointmentType": {
                                "type": "checkbox",
                                "name": "Appointment Type",
                                "data": [
                                    {
                                        "name": "Manual",
                                        "value": "MANUAL",
                                        "checked": false
                                    },
                                    {
                                        "name": "Automated",
                                        "value": "AUTOMATED",
                                        "checked": false
                                    }
                                ]
                            },
                            "destState": {
                                "type": "text",
                                "name": "Destination State",
                                "data": null
                            },
                            "destCity": {
                                "type": "text",
                                "name": "Destination City",
                                "data": null
                            },
                            "appointmentRequired": {
                                "type": "radio",
                                "name": "Appointment Required",
                                "data": null
                            },
                            "appointmentStatus": {
                                "type": "checkbox",
                                "name": "Appointment Status",
                                "data": [
                                    {
                                        "name": "Notified",
                                        "value": "NOTIFIED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Planned",
                                        "value": "PLANNED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Suggested",
                                        "value": "SUGGESTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Confirmed",
                                        "value": "CONFIRMED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Not yet contacted",
                                        "value": "NOT_YET_CONTACTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Appt not made by Transport",
                                        "value": "NOT_MADE_BY_TRANSPORT",
                                        "checked": false
                                    }
                                ]
                            },
                            "liveLoad": {
                                "type": "radio",
                                "name": "Live Load",
                                "data": null
                            },
                            "equipmentTypeList": {
                                "type": "checkbox",
                                "name": "Equipment Type",
                                "data": [
                                    {
                                        "name": "Full Truck Load",
                                        "value": "FULL_TRUCK_LOAD",
                                        "checked": false
                                    },
                                    {
                                        "name": "Less Than Truck Load",
                                        "value": "LESS_THAN_TRUCK_LOAD",
                                        "checked": false
                                    }
                                ]
                            },
                            "shippingConditionList": {
                                "type": "checkbox",
                                "name": "Ship Condition",
                                "data": [
                                    {
                                        "name": "Truck Load (TL)",
                                        "value": "TL",
                                        "checked": false
                                    },
                                    {
                                        "name": "Intermodal (TF)",
                                        "value": "TF",
                                        "checked": false
                                    },
                                    {
                                        "name": "Open (OP) ",
                                        "value": "OP",
                                        "checked": false
                                    },
                                    {
                                        "name": "Package (PG)",
                                        "value": "PG",
                                        "checked": false
                                    },
                                    {
                                        "name": "Customer Pickup (PU)",
                                        "value": "PU",
                                        "checked": false
                                    }
                                ]
                            },
                            "onHold": {
                                "type": "radio",
                                "name": "Order on Hold",
                                "data": null
                            },
                            "deliveryApptDateTimeDestTZ": {
                                "type": "date",
                                "name": "Delivery Date",
                                "dummytime": true,
                                "timeformat": "T",
                                "startTime": "[ 00:00:00.000]",
                                "endTime": "[ 23:59:59.999]",
                                "shortName": "Del. Date",
                                "data": null
                            },
                            "loadReadyDateTimeOriginTZ": {
                                "type": "date",
                                "name": "Carrier Ready Date",
                                "shortName": "Car.Ready Date",
                                "data": null
                            },
                            "tariffServiceCode": {
                                "type": "text",
                                "name": "Carrier Service Code",
                                "data": null
                            },
                            "orderExecutionHealth": {
                                "type": "checkbox",
                                "name": "Health",
                                "data": [
                                    {
                                        "name": "Unhealthy",
                                        "value": "RED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Needs Attention",
                                        "value": "YELLOW",
                                        "checked": false
                                    },
                                    {
                                        "name": "Healthy",
                                        "value": "GREEN",
                                        "checked": false
                                    }
                                ]
                            }
                        },
                        "columns": [
                            {
                                "title": "Load #",
                                "field": "shipmentNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 0,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 0
                                }
                            },
                            {
                                "title": "Delivery #",
                                "field": "deliveryNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 1,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 1
                                }
                            },
                            {
                                "title": "Order #",
                                "field": "orderNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 2,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 2
                                }
                            },
                            {
                                "title": "Carrier",
                                "field": "tariffServiceCode",
                                "tableData": {
                                    "columnOrder": 3,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 3
                                }
                            },
                            {
                                "title": "Shipment Status",
                                "field": "orderExecutionBucketDesc",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 4,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 4
                                }
                            },
                            {
                                "title": "TM Status",
                                "field": "tmsShipmentStatus",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 5,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 5
                                }
                            },
                            {
                                "title": "Carrier Ready Date & Time",
                                "field": "loadReadyDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 6,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 6
                                }
                            },
                            {
                                "title": "Origin ID",
                                "field": "originId",
                                "tableData": {
                                    "columnOrder": 7,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 7
                                }
                            },
                            {
                                "title": "Origin",
                                "field": "origin",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 8,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 8
                                }
                            },
                            {
                                "title": "Customer",
                                "field": "customer",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 9,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 9
                                }
                            },
                            {
                                "title": "Live Load",
                                "field": "liveLoadInd",
                                "tableData": {
                                    "columnOrder": 10,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 10
                                }
                            },
                            {
                                "title": "Order On Hold",
                                "field": "orderOnHoldInd",
                                "tableData": {
                                    "columnOrder": 11,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 11
                                }
                            },
                            {
                                "title": "Cust Confirmed Delivery Date & Time",
                                "field": "deliverApptDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 12,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 12
                                }
                            },
                            {
                                "title": "Dest City",
                                "field": "destinationCity",
                                "tableData": {
                                    "columnOrder": 13,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 13
                                }
                            },
                            {
                                "title": "Dest St",
                                "field": "destinationState",
                                "tableData": {
                                    "columnOrder": 14,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 14
                                }
                            },
                            {
                                "title": "Ship Condition",
                                "field": "shippingCondition",
                                "tableData": {
                                    "columnOrder": 15,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 15
                                }
                            },
                            {
                                "title": "Appt Type",
                                "field": "appointmentType",
                                "tableData": {
                                    "columnOrder": 16,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 16
                                }
                            },
                            {
                                "title": "Appt Req'd",
                                "field": "appointmentRequired",
                                "tableData": {
                                    "columnOrder": 17,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 17
                                }
                            },
                            {
                                "title": "Appt Status",
                                "field": "appointmentStatusDesc",
                                "tableData": {
                                    "columnOrder": 18,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 18
                                }
                            },
                            {
                                "title": "Suspended Status",
                                "field": "shipmentSuspendedStatus",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 19,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 19
                                }
                            },
                            {
                                "title": "Short Lead",
                                "field": "tmsRushIndicator",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 20,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 20
                                }
                            },
                            {
                                "title": "Reason for Alert",
                                "field": "reasonCodeString",
                                "cellStyle": {
                                    "minWidth": 320
                                },
                                "tableData": {
                                    "columnOrder": 21,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 21
                                }
                            },
                            {
                                "title": "Fourkites ETA",
                                "field": "expectedDeliveryDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "disableClick": true
                            }
                        ]
                    },
                    {
                        "id": "4ec72e1b-0283-456d-9aeb-3bf2fdbb0a61",
                        "name": "4ks-5-p2",
                        "default": false,
                        "filters": {
                            "searchStringList": {
                                "type": "text",
                                "name": "Search",
                                "stringToArray": true,
                                "data": null
                            },
                            "sites": {
                                "type": "sites",
                                "name": "Origin",
                                "data": null
                            },
                            "destName": {
                                "type": "text",
                                "name": "Customer",
                                "data": null
                            },
                            "orderExecutionBucket": {
                                "type": "checkbox",
                                "name": "Shipment Status",
                                "data": [
                                    {
                                        "name": "Transportation to be planned",
                                        "value": "TRANS_PLAN_UNASSIGNED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation planning",
                                        "value": "TRANS_PLAN_OPEN",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation processing",
                                        "value": "TRANS_PLAN_PROCESSING",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation Carrier committed",
                                        "value": "TRANS_PLAN_TENDER_ACCEPTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Ready for Pickup",
                                        "value": "TRANS_EXEC_READY_PICK_UP",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Transit",
                                        "value": "TRANS_EXEC_IN_TRANSIT",
                                        "checked": false
                                    },
                                    {
                                        "name": "Delivery confirmed",
                                        "value": "TRANS_EXEC_DELIVERY_CONFIRMED",
                                        "checked": false
                                    }
                                ]
                            },
                            "tmsShipmentStatus": {
                                "type": "checkbox",
                                "name": "TM Operational Status",
                                "data": [
                                    {
                                        "name": "Unassigned",
                                        "value": "Unassigned",
                                        "checked": false
                                    },
                                    {
                                        "name": "Open",
                                        "value": "Open",
                                        "checked": false
                                    },
                                    {
                                        "name": "Planned",
                                        "value": "Planned",
                                        "checked": false
                                    },
                                    {
                                        "name": "Tendered",
                                        "value": "Tendered",
                                        "checked": false
                                    },
                                    {
                                        "name": "Tender Rejected",
                                        "value": "Tender Rejected",
                                        "checked": false
                                    },
                                    {
                                        "name": "Tender Accepted",
                                        "value": "Tender Accepted",
                                        "checked": false
                                    },
                                    {
                                        "name": "Confirming",
                                        "value": "Confirming",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Transit",
                                        "value": "In Transit",
                                        "checked": false
                                    },
                                    {
                                        "name": "Completed",
                                        "value": "Completed",
                                        "checked": false
                                    }
                                ]
                            },
                            "appointmentType": {
                                "type": "checkbox",
                                "name": "Appointment Type",
                                "data": [
                                    {
                                        "name": "Manual",
                                        "value": "MANUAL",
                                        "checked": false
                                    },
                                    {
                                        "name": "Automated",
                                        "value": "AUTOMATED",
                                        "checked": false
                                    }
                                ]
                            },
                            "destState": {
                                "type": "text",
                                "name": "Destination State",
                                "data": null
                            },
                            "destCity": {
                                "type": "text",
                                "name": "Destination City",
                                "data": null
                            },
                            "appointmentRequired": {
                                "type": "radio",
                                "name": "Appointment Required",
                                "data": null
                            },
                            "appointmentStatus": {
                                "type": "checkbox",
                                "name": "Appointment Status",
                                "data": [
                                    {
                                        "name": "Notified",
                                        "value": "NOTIFIED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Planned",
                                        "value": "PLANNED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Suggested",
                                        "value": "SUGGESTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Confirmed",
                                        "value": "CONFIRMED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Not yet contacted",
                                        "value": "NOT_YET_CONTACTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Appt not made by Transport",
                                        "value": "NOT_MADE_BY_TRANSPORT",
                                        "checked": false
                                    }
                                ]
                            },
                            "liveLoad": {
                                "type": "radio",
                                "name": "Live Load",
                                "data": null
                            },
                            "equipmentTypeList": {
                                "type": "checkbox",
                                "name": "Equipment Type",
                                "data": [
                                    {
                                        "name": "Full Truck Load",
                                        "value": "FULL_TRUCK_LOAD",
                                        "checked": false
                                    },
                                    {
                                        "name": "Less Than Truck Load",
                                        "value": "LESS_THAN_TRUCK_LOAD",
                                        "checked": false
                                    }
                                ]
                            },
                            "shippingConditionList": {
                                "type": "checkbox",
                                "name": "Ship Condition",
                                "data": [
                                    {
                                        "name": "Truck Load (TL)",
                                        "value": "TL",
                                        "checked": false
                                    },
                                    {
                                        "name": "Intermodal (TF)",
                                        "value": "TF",
                                        "checked": false
                                    },
                                    {
                                        "name": "Open (OP) ",
                                        "value": "OP",
                                        "checked": false
                                    },
                                    {
                                        "name": "Package (PG)",
                                        "value": "PG",
                                        "checked": false
                                    },
                                    {
                                        "name": "Customer Pickup (PU)",
                                        "value": "PU",
                                        "checked": false
                                    }
                                ]
                            },
                            "onHold": {
                                "type": "radio",
                                "name": "Order on Hold",
                                "data": null
                            },
                            "deliveryApptDateTimeDestTZ": {
                                "type": "date",
                                "name": "Delivery Date",
                                "dummytime": true,
                                "timeformat": "T",
                                "startTime": "[ 00:00:00.000]",
                                "endTime": "[ 23:59:59.999]",
                                "shortName": "Del. Date",
                                "data": null
                            },
                            "loadReadyDateTimeOriginTZ": {
                                "type": "date",
                                "name": "Carrier Ready Date",
                                "shortName": "Car.Ready Date",
                                "data": null
                            },
                            "tariffServiceCodeList": {
                                "type": "text",
                                "name": "Carrier Service Code",
                                "stringToArray": true,
                                "data": null
                            },
                            "orderExecutionHealth": {
                                "type": "checkbox",
                                "name": "Health",
                                "data": [
                                    {
                                        "name": "Unhealthy",
                                        "value": "RED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Needs Attention",
                                        "value": "YELLOW",
                                        "checked": true
                                    },
                                    {
                                        "name": "Healthy",
                                        "value": "GREEN",
                                        "checked": false
                                    }
                                ]
                            },
                            "searchString": {
                                "type": "text",
                                "name": "Search String",
                                "data": "4k-s5"
                            },
                            "shippingCondition": {
                                "type": "text",
                                "name": "Shipping Condition",
                                "data": null
                            },
                            "requestedDeliveryDate": {
                                "type": "date",
                                "hidetime": true,
                                "name": "Requested Delivery Date",
                                "shortName": "Req. Delivery Date",
                                "data": null
                            },
                            "deliveryBlockCode": {
                                "type": "textcheck",
                                "name": "Delivery/Credit Block",
                                "placeholder": "Enter delivery block code",
                                "shortName": "Delivery Block Code",
                                "addon": "creditOnHold",
                                "data": null
                            },
                            "creditOnHold": {
                                "hidden": true,
                                "type": "text",
                                "name": "Show Only Credit Hold Orders",
                                "data": null
                            },
                            "confirmedEquivCubes": {
                                "type": "checkbox",
                                "name": "Confirmed Cube",
                                "shortName": "Confirmed Cube",
                                "data": [
                                    {
                                        "name": "< 1000",
                                        "value": {
                                            "lte": 999.9999999
                                        },
                                        "checked": false
                                    },
                                    {
                                        "name": "1000 - 2000",
                                        "value": {
                                            "gte": 1000,
                                            "lte": 1999.9999999
                                        },
                                        "checked": false
                                    },
                                    {
                                        "name": "2000 - 3000",
                                        "value": {
                                            "gte": 2000,
                                            "lte": 2999.9999999
                                        },
                                        "checked": false
                                    },
                                    {
                                        "name": "> 3000",
                                        "value": {
                                            "gte": 3000
                                        },
                                        "checked": false
                                    }
                                ]
                            },
                            "matAvailDate": {
                                "type": "date",
                                "name": "Material Availability Date",
                                "shortName": "Mat.Avail Date",
                                "data": null
                            },
                            "orderStatusBucket": {
                                "type": "ordercheckbox",
                                "name": "Order Status",
                                "data": [
                                    {
                                        "name": "100% Confirmed Cube",
                                        "value": "SO_COMPLETELY_CONFIRMED_CUBE",
                                        "checked": false,
                                        "parent": "Order Block Free"
                                    },
                                    {
                                        "name": "Less than 100% Confirmed Cube",
                                        "value": "SO_NOT_COMPLETELY_CONFIRMED_CUBE",
                                        "checked": false,
                                        "parent": "Order Block Free"
                                    },
                                    {
                                        "name": "Back Orders Block Free",
                                        "value": "SO_BACK_ORDER_BLOCK_FREE",
                                        "checked": false,
                                        "parent": "Order Block Free"
                                    },
                                    {
                                        "name": "Visible in TMS",
                                        "value": "SO_BLOCKED_TMS_PLANNED_VISIBLE",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Not visible in TMS",
                                        "value": "SO_BLOCKED_TMS_PLANNED_NOT_VISIBLE",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Pickup not scheduled",
                                        "value": "SO_BLOCKED_NON_TMS_PLANNED_PICKUP_NOT_SCHEDULED",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Pickup/Package Multi Block",
                                        "value": "SO_BLOCKED_NON_TMS_PLANNED_PICKUP_MULTI_BLOCK",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Immediate Action",
                                        "value": "SO_BLOCKED_NON_TMS_PLANNED_IMMEDIATE_ACTION",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Back Orders Blocked",
                                        "value": "SO_BACK_ORDER_BLOCKED",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    }
                                ]
                            },
                            "orderTypes": {
                                "type": "checkbox",
                                "name": "Order Type",
                                "data": [
                                    {
                                        "name": "All Customer Orders",
                                        "value": "CUST",
                                        "checked": false
                                    },
                                    {
                                        "name": "STOs",
                                        "value": "STO",
                                        "checked": false
                                    }
                                ]
                            },
                            "documentTypes": {
                                "type": "checkbox",
                                "name": "Document type",
                                "data": [
                                    {
                                        "name": "Standard Orders (ZSTD)",
                                        "value": "ZSTD",
                                        "checked": false
                                    },
                                    {
                                        "name": "Multi Plant Orders (ZMPF)",
                                        "value": "ZMPF",
                                        "checked": false
                                    },
                                    {
                                        "name": "VMI Orders (ZVMI)",
                                        "value": "ZVMI",
                                        "checked": false
                                    },
                                    {
                                        "name": "Merged Orders (ZMER)",
                                        "value": "ZMER",
                                        "checked": false
                                    }
                                ]
                            },
                            "orderHealth": {
                                "type": "checkbox",
                                "name": "Health",
                                "data": [
                                    {
                                        "name": "Unhealthy",
                                        "value": "RED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Needs Attention",
                                        "value": "YELLOW",
                                        "checked": false
                                    },
                                    {
                                        "name": "Healthy",
                                        "value": "GREEN",
                                        "checked": false
                                    }
                                ]
                            },
                            "tariffServiceCode": {
                                "type": "text",
                                "name": "Carrier Service Code",
                                "data": null
                            }
                        },
                        "columns": [
                            {
                                "title": "Load #",
                                "field": "shipmentNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 0,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 0
                                }
                            },
                            {
                                "title": "Delivery #",
                                "field": "deliveryNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 1,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 1
                                }
                            },
                            {
                                "title": "Order #",
                                "field": "orderNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 2,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 2
                                }
                            },
                            {
                                "title": "Carrier",
                                "field": "tariffServiceCode",
                                "tableData": {
                                    "columnOrder": 3,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 3
                                }
                            },
                            {
                                "title": "Shipment Status",
                                "field": "orderExecutionBucketDesc",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 4,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 4
                                }
                            },
                            {
                                "title": "TM Status",
                                "field": "tmsShipmentStatus",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 5,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 5
                                }
                            },
                            {
                                "title": "Carrier Ready Date & Time",
                                "field": "loadReadyDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 6,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 6
                                }
                            },
                            {
                                "title": "Origin ID",
                                "field": "originId",
                                "tableData": {
                                    "columnOrder": 7,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 7
                                }
                            },
                            {
                                "title": "Origin",
                                "field": "origin",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 8,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 8
                                }
                            },
                            {
                                "title": "Customer",
                                "field": "customer",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 9,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 9
                                }
                            },
                            {
                                "title": "Live Load",
                                "field": "liveLoadInd",
                                "tableData": {
                                    "columnOrder": 10,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 10
                                }
                            },
                            {
                                "title": "Order On Hold",
                                "field": "orderOnHoldInd",
                                "tableData": {
                                    "columnOrder": 11,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 11
                                }
                            },
                            {
                                "title": "Cust Confirmed Delivery Date & Time",
                                "field": "deliverApptDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 12,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 12
                                }
                            },
                            {
                                "title": "Dest City",
                                "field": "destinationCity",
                                "tableData": {
                                    "columnOrder": 13,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 13
                                }
                            },
                            {
                                "title": "Dest St",
                                "field": "destinationState",
                                "tableData": {
                                    "columnOrder": 14,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 14
                                }
                            },
                            {
                                "title": "Ship Condition",
                                "field": "shippingCondition",
                                "tableData": {
                                    "columnOrder": 15,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 15
                                }
                            },
                            {
                                "title": "Appt Type",
                                "field": "appointmentType",
                                "tableData": {
                                    "columnOrder": 16,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 16
                                }
                            },
                            {
                                "title": "Appt Req'd",
                                "field": "appointmentRequired",
                                "tableData": {
                                    "columnOrder": 17,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 17
                                }
                            },
                            {
                                "title": "Appt Status",
                                "field": "appointmentStatusDesc",
                                "tableData": {
                                    "columnOrder": 18,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 18
                                }
                            },
                            {
                                "title": "Suspended Status",
                                "field": "shipmentSuspendedStatus",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 19,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 19
                                }
                            },
                            {
                                "title": "Short Lead",
                                "field": "tmsRushIndicator",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 20,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 20
                                }
                            },
                            {
                                "title": "Reason for Alert",
                                "field": "reasonCodeString",
                                "cellStyle": {
                                    "minWidth": 320
                                },
                                "tableData": {
                                    "columnOrder": 21,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 21
                                }
                            },
                            {
                                "title": "Fourkites ETA",
                                "field": "expectedDeliveryDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "disableClick": true
                            }
                        ]
                    },
                    {
                        "id": "b501228d-e2e4-4c48-8608-d3aba29e3843",
                        "name": "4ks-5-TMS",
                        "default": false,
                        "filters": {
                            "searchStringList": {
                                "type": "text",
                                "name": "Search",
                                "stringToArray": true,
                                "data": null
                            },
                            "sites": {
                                "type": "sites",
                                "name": "Origin",
                                "data": null
                            },
                            "destName": {
                                "type": "text",
                                "name": "Customer",
                                "data": null
                            },
                            "orderExecutionBucket": {
                                "type": "checkbox",
                                "name": "Shipment Status",
                                "data": [
                                    {
                                        "name": "Transportation to be planned",
                                        "value": "TRANS_PLAN_UNASSIGNED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation planning",
                                        "value": "TRANS_PLAN_OPEN",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation processing",
                                        "value": "TRANS_PLAN_PROCESSING",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation Carrier committed",
                                        "value": "TRANS_PLAN_TENDER_ACCEPTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Ready for Pickup",
                                        "value": "TRANS_EXEC_READY_PICK_UP",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Transit",
                                        "value": "TRANS_EXEC_IN_TRANSIT",
                                        "checked": false
                                    },
                                    {
                                        "name": "Delivery confirmed",
                                        "value": "TRANS_EXEC_DELIVERY_CONFIRMED",
                                        "checked": false
                                    }
                                ]
                            },
                            "tmsShipmentStatus": {
                                "type": "checkbox",
                                "name": "TM Operational Status",
                                "data": [
                                    {
                                        "name": "Unassigned",
                                        "value": "Unassigned",
                                        "checked": true
                                    },
                                    {
                                        "name": "Open",
                                        "value": "Open",
                                        "checked": false
                                    },
                                    {
                                        "name": "Planned",
                                        "value": "Planned",
                                        "checked": false
                                    },
                                    {
                                        "name": "Tendered",
                                        "value": "Tendered",
                                        "checked": false
                                    },
                                    {
                                        "name": "Tender Rejected",
                                        "value": "Tender Rejected",
                                        "checked": false
                                    },
                                    {
                                        "name": "Tender Accepted",
                                        "value": "Tender Accepted",
                                        "checked": false
                                    },
                                    {
                                        "name": "Confirming",
                                        "value": "Confirming",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Transit",
                                        "value": "In Transit",
                                        "checked": false
                                    },
                                    {
                                        "name": "Completed",
                                        "value": "Completed",
                                        "checked": false
                                    }
                                ]
                            },
                            "appointmentType": {
                                "type": "checkbox",
                                "name": "Appointment Type",
                                "data": [
                                    {
                                        "name": "Manual",
                                        "value": "MANUAL",
                                        "checked": false
                                    },
                                    {
                                        "name": "Automated",
                                        "value": "AUTOMATED",
                                        "checked": false
                                    }
                                ]
                            },
                            "destState": {
                                "type": "text",
                                "name": "Destination State",
                                "data": null
                            },
                            "destCity": {
                                "type": "text",
                                "name": "Destination City",
                                "data": null
                            },
                            "appointmentRequired": {
                                "type": "radio",
                                "name": "Appointment Required",
                                "data": null
                            },
                            "appointmentStatus": {
                                "type": "checkbox",
                                "name": "Appointment Status",
                                "data": [
                                    {
                                        "name": "Notified",
                                        "value": "NOTIFIED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Planned",
                                        "value": "PLANNED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Suggested",
                                        "value": "SUGGESTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Confirmed",
                                        "value": "CONFIRMED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Not yet contacted",
                                        "value": "NOT_YET_CONTACTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Appt not made by Transport",
                                        "value": "NOT_MADE_BY_TRANSPORT",
                                        "checked": false
                                    }
                                ]
                            },
                            "liveLoad": {
                                "type": "radio",
                                "name": "Live Load",
                                "data": null
                            },
                            "equipmentTypeList": {
                                "type": "checkbox",
                                "name": "Equipment Type",
                                "data": [
                                    {
                                        "name": "Full Truck Load",
                                        "value": "FULL_TRUCK_LOAD",
                                        "checked": false
                                    },
                                    {
                                        "name": "Less Than Truck Load",
                                        "value": "LESS_THAN_TRUCK_LOAD",
                                        "checked": false
                                    }
                                ]
                            },
                            "shippingConditionList": {
                                "type": "checkbox",
                                "name": "Ship Condition",
                                "data": [
                                    {
                                        "name": "Truck Load (TL)",
                                        "value": "TL",
                                        "checked": false
                                    },
                                    {
                                        "name": "Intermodal (TF)",
                                        "value": "TF",
                                        "checked": false
                                    },
                                    {
                                        "name": "Open (OP) ",
                                        "value": "OP",
                                        "checked": false
                                    },
                                    {
                                        "name": "Package (PG)",
                                        "value": "PG",
                                        "checked": false
                                    },
                                    {
                                        "name": "Customer Pickup (PU)",
                                        "value": "PU",
                                        "checked": false
                                    }
                                ]
                            },
                            "onHold": {
                                "type": "radio",
                                "name": "Order on Hold",
                                "data": null
                            },
                            "deliveryApptDateTimeDestTZ": {
                                "type": "date",
                                "name": "Delivery Date",
                                "dummytime": true,
                                "timeformat": "T",
                                "startTime": "[ 00:00:00.000]",
                                "endTime": "[ 23:59:59.999]",
                                "shortName": "Del. Date",
                                "data": null
                            },
                            "loadReadyDateTimeOriginTZ": {
                                "type": "date",
                                "name": "Carrier Ready Date",
                                "shortName": "Car.Ready Date",
                                "data": null
                            },
                            "tariffServiceCodeList": {
                                "type": "text",
                                "name": "Carrier Service Code",
                                "stringToArray": true,
                                "data": null
                            },
                            "orderExecutionHealth": {
                                "type": "checkbox",
                                "name": "Health",
                                "data": [
                                    {
                                        "name": "Unhealthy",
                                        "value": "RED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Needs Attention",
                                        "value": "YELLOW",
                                        "checked": false
                                    },
                                    {
                                        "name": "Healthy",
                                        "value": "GREEN",
                                        "checked": false
                                    }
                                ]
                            },
                            "searchString": {
                                "type": "text",
                                "name": "Search String",
                                "data": "4k-s5"
                            },
                            "shippingCondition": {
                                "type": "text",
                                "name": "Shipping Condition",
                                "data": null
                            },
                            "requestedDeliveryDate": {
                                "type": "date",
                                "hidetime": true,
                                "name": "Requested Delivery Date",
                                "shortName": "Req. Delivery Date",
                                "data": null
                            },
                            "deliveryBlockCode": {
                                "type": "textcheck",
                                "name": "Delivery/Credit Block",
                                "placeholder": "Enter delivery block code",
                                "shortName": "Delivery Block Code",
                                "addon": "creditOnHold",
                                "data": null
                            },
                            "creditOnHold": {
                                "hidden": true,
                                "type": "text",
                                "name": "Show Only Credit Hold Orders",
                                "data": null
                            },
                            "confirmedEquivCubes": {
                                "type": "checkbox",
                                "name": "Confirmed Cube",
                                "shortName": "Confirmed Cube",
                                "data": [
                                    {
                                        "name": "< 1000",
                                        "value": {
                                            "lte": 999.9999999
                                        },
                                        "checked": false
                                    },
                                    {
                                        "name": "1000 - 2000",
                                        "value": {
                                            "gte": 1000,
                                            "lte": 1999.9999999
                                        },
                                        "checked": false
                                    },
                                    {
                                        "name": "2000 - 3000",
                                        "value": {
                                            "gte": 2000,
                                            "lte": 2999.9999999
                                        },
                                        "checked": false
                                    },
                                    {
                                        "name": "> 3000",
                                        "value": {
                                            "gte": 3000
                                        },
                                        "checked": false
                                    }
                                ]
                            },
                            "matAvailDate": {
                                "type": "date",
                                "name": "Material Availability Date",
                                "shortName": "Mat.Avail Date",
                                "data": null
                            },
                            "orderStatusBucket": {
                                "type": "ordercheckbox",
                                "name": "Order Status",
                                "data": [
                                    {
                                        "name": "100% Confirmed Cube",
                                        "value": "SO_COMPLETELY_CONFIRMED_CUBE",
                                        "checked": false,
                                        "parent": "Order Block Free"
                                    },
                                    {
                                        "name": "Less than 100% Confirmed Cube",
                                        "value": "SO_NOT_COMPLETELY_CONFIRMED_CUBE",
                                        "checked": false,
                                        "parent": "Order Block Free"
                                    },
                                    {
                                        "name": "Back Orders Block Free",
                                        "value": "SO_BACK_ORDER_BLOCK_FREE",
                                        "checked": false,
                                        "parent": "Order Block Free"
                                    },
                                    {
                                        "name": "Visible in TMS",
                                        "value": "SO_BLOCKED_TMS_PLANNED_VISIBLE",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Not visible in TMS",
                                        "value": "SO_BLOCKED_TMS_PLANNED_NOT_VISIBLE",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Pickup not scheduled",
                                        "value": "SO_BLOCKED_NON_TMS_PLANNED_PICKUP_NOT_SCHEDULED",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Pickup/Package Multi Block",
                                        "value": "SO_BLOCKED_NON_TMS_PLANNED_PICKUP_MULTI_BLOCK",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Immediate Action",
                                        "value": "SO_BLOCKED_NON_TMS_PLANNED_IMMEDIATE_ACTION",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Back Orders Blocked",
                                        "value": "SO_BACK_ORDER_BLOCKED",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    }
                                ]
                            },
                            "orderTypes": {
                                "type": "checkbox",
                                "name": "Order Type",
                                "data": [
                                    {
                                        "name": "All Customer Orders",
                                        "value": "CUST",
                                        "checked": false
                                    },
                                    {
                                        "name": "STOs",
                                        "value": "STO",
                                        "checked": false
                                    }
                                ]
                            },
                            "documentTypes": {
                                "type": "checkbox",
                                "name": "Document type",
                                "data": [
                                    {
                                        "name": "Standard Orders (ZSTD)",
                                        "value": "ZSTD",
                                        "checked": false
                                    },
                                    {
                                        "name": "Multi Plant Orders (ZMPF)",
                                        "value": "ZMPF",
                                        "checked": false
                                    },
                                    {
                                        "name": "VMI Orders (ZVMI)",
                                        "value": "ZVMI",
                                        "checked": false
                                    },
                                    {
                                        "name": "Merged Orders (ZMER)",
                                        "value": "ZMER",
                                        "checked": false
                                    }
                                ]
                            },
                            "orderHealth": {
                                "type": "checkbox",
                                "name": "Health",
                                "data": [
                                    {
                                        "name": "Unhealthy",
                                        "value": "RED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Needs Attention",
                                        "value": "YELLOW",
                                        "checked": false
                                    },
                                    {
                                        "name": "Healthy",
                                        "value": "GREEN",
                                        "checked": false
                                    }
                                ]
                            },
                            "tariffServiceCode": {
                                "type": "text",
                                "name": "Carrier Service Code",
                                "data": null
                            }
                        },
                        "columns": [
                            {
                                "title": "Load #",
                                "field": "shipmentNum",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Delivery #",
                                "field": "deliveryNum",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Order #",
                                "field": "orderNum",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Carrier",
                                "field": "tariffServiceCode"
                            },
                            {
                                "title": "Shipment Status",
                                "field": "orderExecutionBucketDesc",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "TM Status",
                                "field": "tmsShipmentStatus",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Carrier Ready Date & Time",
                                "field": "loadReadyDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Origin ID",
                                "field": "originId"
                            },
                            {
                                "title": "Origin",
                                "field": "origin",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Customer",
                                "field": "customer",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Live Load",
                                "field": "liveLoadInd"
                            },
                            {
                                "title": "Order On Hold",
                                "field": "orderOnHoldInd"
                            },
                            {
                                "title": "Cust Confirmed Delivery Date & Time",
                                "field": "deliverApptDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Dest City",
                                "field": "destinationCity"
                            },
                            {
                                "title": "Dest St",
                                "field": "destinationState"
                            },
                            {
                                "title": "Ship Condition",
                                "field": "shippingCondition"
                            },
                            {
                                "title": "Appt Type",
                                "field": "appointmentType"
                            },
                            {
                                "title": "Appt Req'd",
                                "field": "appointmentRequired"
                            },
                            {
                                "title": "Appt Status",
                                "field": "appointmentStatusDesc"
                            },
                            {
                                "title": "Suspended Status",
                                "field": "shipmentSuspendedStatus",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Short Lead",
                                "field": "tmsRushIndicator",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Reason for Alert",
                                "field": "reasonCodeString",
                                "cellStyle": {
                                    "minWidth": 320
                                }
                            },
                            {
                                "title": "Fourkites ETA",
                                "field": "expectedDeliveryDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "disableClick": true
                            }
                        ]
                    }
                ]
            }
        },
        "currentView": {
            "network": {
                "id": "9c0dcab2-256d-4922-89fe-96da76ea0ef8",
                "name": "Blank",
                "default": true,
                "filters": {
                    "sites": {
                        "type": "sites",
                        "name": "Origin",
                        "data": [
                            "2042",
                            "2425",
                            "2822"
                        ]
                    },
                    "searchString": {
                        "type": "text",
                        "name": "Search String",
                        "data": null
                    },
                    "destName": {
                        "type": "text",
                        "name": "Customer",
                        "data": null
                    },
                    "destState": {
                        "type": "text",
                        "name": "Destination State",
                        "data": null
                    },
                    "destCity": {
                        "type": "text",
                        "name": "Destination City",
                        "data": null
                    },
                    "orderStatusBucket": {
                        "type": "checkbox",
                        "name": "Order Status",
                        "data": [
                            {
                                "name": "Order Block Free",
                                "value": "ORDER_BLOCK_FREE",
                                "checked": true
                            },
                            {
                                "name": "Order Blocked",
                                "value": "ORDER_BLOCKED",
                                "checked": true
                            }
                        ]
                    },
                    "orderTypes": {
                        "type": "checkbox",
                        "name": "Order Type",
                        "data": [
                            {
                                "name": "Cust. Order",
                                "value": "CUST",
                                "checked": false
                            },
                            {
                                "name": "STO",
                                "value": "STO",
                                "checked": false
                            }
                        ]
                    },
                    "tariffServiceCode": {
                        "type": "text",
                        "name": "Carrier Service Code",
                        "data": null
                    },
                    "orderExecutionBucket": {
                        "type": "checkbox",
                        "name": "Shipment Status",
                        "data": [
                            {
                                "name": "Transportation to be planned",
                                "value": "TRANS_PLAN_UNASSIGNED",
                                "checked": false
                            },
                            {
                                "name": "Transportation planning",
                                "value": "TRANS_PLAN_OPEN",
                                "checked": false
                            },
                            {
                                "name": "Transportation processing",
                                "value": "TRANS_PLAN_PROCESSING",
                                "checked": false
                            },
                            {
                                "name": "Transportation Carrier committed",
                                "value": "TRANS_PLAN_TENDER_ACCEPTED",
                                "checked": false
                            },
                            {
                                "name": "Shipment Created",
                                "value": "SHIPMENT_CREATED",
                                "checked": false
                            },
                            {
                                "name": "Checked in by DC",
                                "value": "CHECK_IN",
                                "checked": false
                            },
                            {
                                "name": "Loading Started",
                                "value": "LOADING_STARTED",
                                "checked": false
                            },
                            {
                                "name": "Ready for Pickup",
                                "value": "TRANS_EXEC_READY_PICK_UP",
                                "checked": false
                            },
                            {
                                "name": "In Transit",
                                "value": "TRANS_EXEC_IN_TRANSIT",
                                "checked": false
                            },
                            {
                                "name": "Delivery confirmed",
                                "value": "TRANS_EXEC_DELIVERY_CONFIRMED",
                                "checked": false
                            }
                        ]
                    },
                    "loadReadyDateTimeOriginTZ": {
                        "type": "date",
                        "dummytime": true,
                        "timeformat": "T",
                        "startTime": "[ 00:00:00.000]",
                        "endTime": "[ 23:59:59.999]",
                        "name": "Carrier Ready Date",
                        "shortName": "Carr. Ready Date",
                        "data": null
                    }
                },
                "columns": [
                    {
                        "title": "Shipment #",
                        "field": "shipmentNum",
                        "cellStyle": {
                            "minWidth": 160
                        },
                        "tableData": {
                            "columnOrder": 0,
                            "groupSort": "asc",
                            "width": "calc((100% - (0px)) / 16)",
                            "id": 0
                        }
                    },
                    {
                        "title": "Delivery #",
                        "field": "deliveryNum",
                        "cellStyle": {
                            "minWidth": 160
                        },
                        "tableData": {
                            "columnOrder": 1,
                            "groupSort": "asc",
                            "width": "calc((100% - (0px)) / 16)",
                            "id": 1
                        }
                    },
                    {
                        "title": "Shipment Status",
                        "field": "orderExecutionBucketDesc",
                        "cellStyle": {
                            "minWidth": 160
                        },
                        "tableData": {
                            "columnOrder": 2,
                            "groupSort": "asc",
                            "width": "calc((100% - (0px)) / 16)",
                            "id": 2
                        }
                    },
                    {
                        "title": "Order #",
                        "field": "orderNum",
                        "cellStyle": {
                            "minWidth": 160
                        },
                        "tableData": {
                            "columnOrder": 3,
                            "groupSort": "asc",
                            "width": "calc((100% - (0px)) / 16)",
                            "id": 3
                        }
                    },
                    {
                        "title": "Order Status",
                        "field": "orderStatus",
                        "cellStyle": {
                            "minWidth": 160
                        },
                        "tableData": {
                            "columnOrder": 4,
                            "groupSort": "asc",
                            "width": "calc((100% - (0px)) / 16)",
                            "id": 4
                        }
                    },
                    {
                        "title": "Order Type",
                        "field": "orderType",
                        "tableData": {
                            "columnOrder": 5,
                            "groupSort": "asc",
                            "width": "calc((100% - (0px)) / 16)",
                            "id": 5
                        }
                    },
                    {
                        "title": "Customer",
                        "field": "customer",
                        "tableData": {
                            "columnOrder": 6,
                            "groupSort": "asc",
                            "width": "calc((100% - (0px)) / 16)",
                            "id": 6
                        }
                    },
                    {
                        "title": "Origin ID",
                        "field": "originId",
                        "tableData": {
                            "columnOrder": 7,
                            "groupSort": "asc",
                            "width": "calc((100% - (0px)) / 16)",
                            "id": 7
                        }
                    },
                    {
                        "title": "Origin",
                        "field": "origin",
                        "cellStyle": {
                            "minWidth": 160
                        },
                        "tableData": {
                            "columnOrder": 8,
                            "groupSort": "asc",
                            "width": "calc((100% - (0px)) / 16)",
                            "id": 8
                        }
                    },
                    {
                        "title": "Dest City",
                        "field": "destinationCity",
                        "tableData": {
                            "columnOrder": 9,
                            "groupSort": "asc",
                            "width": "calc((100% - (0px)) / 16)",
                            "id": 9
                        }
                    },
                    {
                        "title": "Dest State",
                        "field": "destinationState",
                        "tableData": {
                            "columnOrder": 10,
                            "groupSort": "asc",
                            "width": "calc((100% - (0px)) / 16)",
                            "id": 10
                        }
                    },
                    {
                        "title": "Carrier Ready Date & Time",
                        "field": "loadReadyDateTime",
                        "cellStyle": {
                            "minWidth": 160
                        },
                        "tableData": {
                            "columnOrder": 11,
                            "groupSort": "asc",
                            "width": "calc((100% - (0px)) / 16)",
                            "id": 11
                        }
                    },
                    {
                        "title": "Carrier",
                        "field": "tariffServiceCode",
                        "tableData": {
                            "columnOrder": 12,
                            "groupSort": "asc",
                            "width": "calc((100% - (0px)) / 16)",
                            "id": 12
                        }
                    },
                    {
                        "title": "Ship Condition",
                        "field": "shippingCondition",
                        "tableData": {
                            "columnOrder": 13,
                            "groupSort": "asc",
                            "width": "calc((100% - (0px)) / 16)",
                            "id": 13
                        }
                    },
                    {
                        "title": "Reason for Alert",
                        "field": "reasonCodeString",
                        "cellStyle": {
                            "minWidth": 320
                        },
                        "tableData": {
                            "columnOrder": 14,
                            "groupSort": "asc",
                            "width": "calc((100% - (0px)) / 16)",
                            "id": 14
                        }
                    },
                    {
                        "title": "Fourkites ETA",
                        "field": "expectedDeliveryDateTime",
                        "cellStyle": {
                            "minWidth": 160
                        },
                        "disableClick": true
                    }
                ]
            },
            "transport": {},
            "orders": {
                "id": "2fc48d20-df7f-43cf-979d-d5868ac2f373",
                "name": "Another View",
                "default": true,
                "filters": {
                    "searchStringList": {
                        "type": "text",
                        "name": "Search",
                        "stringToArray": true,
                        "data": null
                    },
                    "destName": {
                        "type": "text",
                        "name": "Customer",
                        "data": null
                    },
                    "sites": {
                        "type": "sites",
                        "name": "Origin",
                        "data": null
                    },
                    "shippingConditionList": {
                        "type": "text",
                        "stringToArray": true,
                        "name": "Shipping Condition",
                        "data": null
                    },
                    "requestedDeliveryDate": {
                        "type": "date",
                        "name": "Requested Delivery Date",
                        "shortName": "Req. Delivery Date",
                        "data": null
                    },
                    "deliveryBlockCodeList": {
                        "type": "textcheck",
                        "stringToArray": true,
                        "name": "Delivery/Credit Block",
                        "placeholder": "Enter delivery block code",
                        "shortName": "Delivery Block Code",
                        "addon": "creditOnHold",
                        "data": null
                    },
                    "creditOnHold": {
                        "hidden": true,
                        "type": "text",
                        "name": "Show Only Credit Hold Orders",
                        "data": null
                    },
                    "confirmedEquivCubes": {
                        "type": "checkbox",
                        "name": "Confirmed Cube",
                        "shortName": "Confirmed Cube",
                        "data": [
                            {
                                "name": "< 1000",
                                "value": {
                                    "lte": 999.9999999
                                },
                                "checked": false
                            },
                            {
                                "name": "1000 - 2000",
                                "value": {
                                    "gte": 1000,
                                    "lte": 1999.9999999
                                },
                                "checked": false
                            },
                            {
                                "name": "2000 - 3000",
                                "value": {
                                    "gte": 2000,
                                    "lte": 2999.9999999
                                },
                                "checked": false
                            },
                            {
                                "name": "> 3000",
                                "value": {
                                    "gte": 3000
                                },
                                "checked": false
                            }
                        ]
                    },
                    "destState": {
                        "type": "text",
                        "name": "Destination State",
                        "data": "QLD"
                    },
                    "destCity": {
                        "type": "text",
                        "name": "Destination City",
                        "data": null
                    },
                    "matAvailDate": {
                        "type": "date",
                        "name": "Material Availability Date",
                        "shortName": "Mat.Avail Date",
                        "data": null
                    },
                    "orderStatusBucket": {
                        "type": "ordercheckbox",
                        "name": "Order Status",
                        "data": [
                            {
                                "name": "100% Confirmed Cube",
                                "value": "SO_COMPLETELY_CONFIRMED_CUBE",
                                "checked": false,
                                "parent": "Order Block Free"
                            },
                            {
                                "name": "Less than 100% Confirmed Cube",
                                "value": "SO_NOT_COMPLETELY_CONFIRMED_CUBE",
                                "checked": false,
                                "parent": "Order Block Free"
                            },
                            {
                                "name": "Back Orders Block Free",
                                "value": "SO_BACK_ORDER_BLOCK_FREE",
                                "checked": false,
                                "parent": "Order Block Free"
                            },
                            {
                                "name": "Visible in TMS",
                                "value": "SO_BLOCKED_TMS_PLANNED_VISIBLE",
                                "checked": false,
                                "parent": "Orders Blocked"
                            },
                            {
                                "name": "Not visible in TMS",
                                "value": "SO_BLOCKED_TMS_PLANNED_NOT_VISIBLE",
                                "checked": false,
                                "parent": "Orders Blocked"
                            },
                            {
                                "name": "Pickup not scheduled",
                                "value": "SO_BLOCKED_NON_TMS_PLANNED_PICKUP_NOT_SCHEDULED",
                                "checked": false,
                                "parent": "Orders Blocked"
                            },
                            {
                                "name": "Pickup/Package Multi Block",
                                "value": "SO_BLOCKED_NON_TMS_PLANNED_PICKUP_MULTI_BLOCK",
                                "checked": false,
                                "parent": "Orders Blocked"
                            },
                            {
                                "name": "Immediate Action",
                                "value": "SO_BLOCKED_NON_TMS_PLANNED_IMMEDIATE_ACTION",
                                "checked": false,
                                "parent": "Orders Blocked"
                            },
                            {
                                "name": "Back Orders Blocked",
                                "value": "SO_BACK_ORDER_BLOCKED",
                                "checked": false,
                                "parent": "Orders Blocked"
                            }
                        ]
                    },
                    "orderExecutionBucket": {
                        "type": "checkbox",
                        "name": "Shipment Status",
                        "data": [
                            {
                                "name": "Transportation to be planned",
                                "value": "TRANS_PLAN_UNASSIGNED",
                                "checked": false
                            },
                            {
                                "name": "Transportation planning",
                                "value": "TRANS_PLAN_OPEN",
                                "checked": false
                            },
                            {
                                "name": "Transportation processing",
                                "value": "TRANS_PLAN_PROCESSING",
                                "checked": false
                            },
                            {
                                "name": "Transportation Carrier committed",
                                "value": "TRANS_PLAN_TENDER_ACCEPTED",
                                "checked": false
                            },
                            {
                                "name": "Shipment Created",
                                "value": "SHIPMENT_CREATED",
                                "checked": false
                            },
                            {
                                "name": "Checked in by DC",
                                "value": "CHECK_IN",
                                "checked": false
                            },
                            {
                                "name": "Loading Started",
                                "value": "LOADING_STARTED",
                                "checked": false
                            },
                            {
                                "name": "Ready for Pickup",
                                "value": "TRANS_EXEC_READY_PICK_UP",
                                "checked": false
                            },
                            {
                                "name": "In Transit",
                                "value": "TRANS_EXEC_IN_TRANSIT",
                                "checked": false
                            },
                            {
                                "name": "Delivery confirmed",
                                "value": "TRANS_EXEC_DELIVERY_CONFIRMED",
                                "checked": false
                            }
                        ]
                    },
                    "inboundOrderExecutionBucket": {
                        "type": "checkbox",
                        "name": "Inbound Shipment Status",
                        "data": [
                            {
                                "name": "In Transit",
                                "value": "INBOUND_IN_TRANSIT",
                                "checked": false
                            },
                            {
                                "name": "In Yard",
                                "value": "INBOUND_IN_YARD",
                                "checked": false
                            },
                            {
                                "name": "Unloading",
                                "value": "INBOUND_UNLOADING",
                                "checked": false
                            },
                            {
                                "name": "Unloading Completed",
                                "value": "INBOUND_UNLOADING_COMPLETED",
                                "checked": false
                            }
                        ]
                    },
                    "orderTypes": {
                        "type": "checkbox",
                        "name": "Order Type",
                        "data": [
                            {
                                "name": "All Customer Orders",
                                "value": "CUST",
                                "checked": true
                            },
                            {
                                "name": "STOs",
                                "value": "STO",
                                "checked": false
                            }
                        ]
                    },
                    "documentTypes": {
                        "type": "checkbox",
                        "name": "Document type",
                        "data": [
                            {
                                "name": "Standard Orders (ZSTD)",
                                "value": "ZSTD",
                                "checked": false
                            },
                            {
                                "name": "Multi Plant Orders (ZMPF)",
                                "value": "ZMPF",
                                "checked": false
                            },
                            {
                                "name": "VMI Orders (ZVMI)",
                                "value": "ZVMI",
                                "checked": false
                            },
                            {
                                "name": "Merged Orders (ZMER)",
                                "value": "ZMER",
                                "checked": false
                            }
                        ]
                    },
                    "orderHealth": {
                        "type": "checkbox",
                        "name": "Health",
                        "data": [
                            {
                                "name": "Unhealthy",
                                "value": "RED",
                                "checked": false
                            },
                            {
                                "name": "Needs Attention",
                                "value": "YELLOW",
                                "checked": false
                            },
                            {
                                "name": "Healthy",
                                "value": "GREEN",
                                "checked": false
                            }
                        ]
                    }
                },
                "columns": [
                    {
                        "title": "Order #",
                        "field": "orderNum",
                        "cellStyle": {
                            "minWidth": 160
                        }
                    },
                    {
                        "title": "Customer",
                        "field": "customer",
                        "cellStyle": {
                            "minWidth": 160
                        }
                    },
                    {
                        "title": "Customer PO #",
                        "field": "customerPoNum",
                        "cellStyle": {
                            "minWidth": 160
                        }
                    },
                    {
                        "title": "LEQ",
                        "field": "loadEquivCube"
                    },
                    {
                        "title": "Confirmed LEQ",
                        "field": "confLoadEquivCube"
                    },
                    {
                        "title": "Allocated LEQ",
                        "field": "allocEquivCube"
                    },
                    {
                        "title": "Origin ID",
                        "field": "originId"
                    },
                    {
                        "title": "Origin",
                        "field": "origin",
                        "cellStyle": {
                            "minWidth": 160
                        }
                    },
                    {
                        "title": "MAD",
                        "field": "matAvailDate",
                        "cellStyle": {
                            "minWidth": 160
                        }
                    },
                    {
                        "title": "Original RDD",
                        "field": "originalRequestedDeliveryDatetime",
                        "cellStyle": {
                            "minWidth": 160
                        }
                    },
                    {
                        "title": "RDD",
                        "field": "requestedDeliveryDate",
                        "cellStyle": {
                            "minWidth": 160
                        }
                    },
                    {
                        "title": "Ship Condition",
                        "field": "shippingCondition"
                    },
                    {
                        "title": "Delivery Block",
                        "field": "deliveryBlocksString",
                        "cellStyle": {
                            "minWidth": 160
                        }
                    },
                    {
                        "title": "Shipment #",
                        "field": "shipmentNum"
                    },
                    {
                        "title": "Delivery #",
                        "field": "deliveryNum",
                        "cellStyle": {
                            "minWidth": 160
                        }
                    },
                    {
                        "title": "Carrier Ready Date & Time",
                        "field": "loadReadyDateTime",
                        "cellStyle": {
                            "minWidth": 160
                        }
                    },
                    {
                        "title": "Dest City",
                        "field": "destinationCity"
                    },
                    {
                        "title": "Dest State",
                        "field": "destinationState"
                    },
                    {
                        "title": "Trailer #",
                        "field": "trailerNum"
                    },
                    {
                        "title": "Shipment Status",
                        "field": "orderExecutionBucketDesc",
                        "cellStyle": {
                            "minWidth": 160
                        }
                    },
                    {
                        "title": "Inbound Shipment Status",
                        "field": "inboundOrderExecutionBucketDesc",
                        "cellStyle": {
                            "minWidth": 160
                        }
                    },
                    {
                        "title": "Order Status",
                        "field": "orderStatus",
                        "cellStyle": {
                            "minWidth": 160
                        }
                    },
                    {
                        "title": "Order Sub Status",
                        "field": "orderStatusBucketDesc",
                        "cellStyle": {
                            "minWidth": 160
                        }
                    },
                    {
                        "title": "Order Type",
                        "field": "orderType"
                    },
                    {
                        "title": "Business",
                        "field": "orderBusinessUnit"
                    },
                    {
                        "title": "Reason for Alert",
                        "field": "reasonCodeString",
                        "cellStyle": {
                            "minWidth": 320
                        }
                    },
                    {
                        "title": "Fourkites ETA",
                        "field": "expectedDeliveryDateTime",
                        "cellStyle": {
                            "minWidth": 160
                        }
                    },
                    {
                        "title": "Sales Org",
                        "field": "salesOrg",
                        "cellStyle": {
                            "minWidth": 160
                        }
                    },
                    {
                        "title": "Distribution Channel",
                        "field": "distributionChannel",
                        "cellStyle": {
                            "minWidth": 160
                        }
                    },
                    {
                        "title": "Sales Office",
                        "field": "salesOffice",
                        "cellStyle": {
                            "minWidth": 160
                        }
                    },
                    {
                        "title": "Sales Group",
                        "field": "salesGroup",
                        "cellStyle": {
                            "minWidth": 160
                        }
                    }
                ]
            },
            "distoutbound": {},
            "distinbound": {
                "id": 1,
                "name": "Inbound 1",
                "default": true,
                "filters": {
                    "searchString": {
                        "type": "text",
                        "name": "Search String",
                        "data": null
                    },
                    "inboundOrderExecutionBucket": {
                        "type": "checkbox",
                        "name": "Shipment Status",
                        "data": [
                            {
                                "name": "In Transit",
                                "value": "INBOUND_IN_TRANSIT",
                                "checked": false
                            },
                            {
                                "name": "In Yard",
                                "value": "INBOUND_IN_YARD",
                                "checked": false
                            },
                            {
                                "name": "Unloading",
                                "value": "INBOUND_UNLOADING",
                                "checked": false
                            },
                            {
                                "name": "Unloading Completed",
                                "value": "INBOUND_UNLOADING_COMPLETED",
                                "checked": false
                            }
                        ]
                    },
                    "destState": {
                        "type": "text",
                        "name": "Destination State",
                        "data": null
                    },
                    "destCity": {
                        "type": "text",
                        "name": "Destination City",
                        "data": null
                    },
                    "yardArrivalDateTimeDestTZ": {
                        "type": "date",
                        "dummytime": true,
                        "timeformat": "T",
                        "startTime": "[ 00:00:00.000]",
                        "endTime": "[ 23:59:59.999]",
                        "name": "Date Arrived in Yard",
                        "shortName": "Yard Arrival",
                        "data": null
                    },
                    "durationInYard": {
                        "type": "checkboxradio",
                        "name": "Duration in Yard",
                        "data": [
                            {
                                "name": "< 24 Hours",
                                "value": {
                                    "startTimeMins": null,
                                    "endTimeMins": 1439
                                },
                                "checked": false
                            },
                            {
                                "name": "24 - 48 Hours",
                                "value": {
                                    "startTimeMins": 1440,
                                    "endTimeMins": 2880
                                },
                                "checked": false
                            },
                            {
                                "name": "> 72 Hours",
                                "value": {
                                    "startTimeMins": 4321,
                                    "endTimeMins": null
                                },
                                "checked": true
                            }
                        ]
                    },
                    "inboundOrderExecutionHealth": {
                        "type": "checkbox",
                        "name": "Health",
                        "data": [
                            {
                                "name": "Unhealthy",
                                "value": "RED",
                                "checked": false
                            },
                            {
                                "name": "Needs Attention",
                                "value": "YELLOW",
                                "checked": false
                            },
                            {
                                "name": "Healthy",
                                "value": "GREEN",
                                "checked": false
                            }
                        ]
                    }
                },
                "columns": [
                    {
                        "title": "Order #",
                        "field": "orderNum",
                        "cellStyle": {
                            "minWidth": 160
                        },
                        "tableData": {
                            "columnOrder": 0,
                            "groupSort": "asc",
                            "width": "calc((100% - (0px)) / 9)",
                            "id": 0
                        },
                        "hidden": false
                    },
                    {
                        "title": "Duration in Yard",
                        "field": "durationInYardMS",
                        "tableData": {
                            "columnOrder": 1,
                            "groupSort": "asc",
                            "width": "calc((100% - (0px)) / 11)",
                            "id": 1
                        },
                        "hidden": true
                    },
                    {
                        "title": "Origin",
                        "field": "origin",
                        "tableData": {
                            "columnOrder": 2,
                            "groupSort": "asc",
                            "width": "calc((100% - (0px)) / 11)",
                            "id": 1
                        },
                        "hidden": true
                    },
                    {
                        "title": "Date and Time arrived in Yard",
                        "field": "yardArrivalDateTime",
                        "cellStyle": {
                            "minWidth": 160
                        },
                        "tableData": {
                            "columnOrder": 3,
                            "groupSort": "asc",
                            "width": "calc((100% - (0px)) / 9)",
                            "id": 3
                        },
                        "hidden": false
                    },
                    {
                        "title": "Shipment #",
                        "field": "shipmentNum",
                        "cellStyle": {
                            "minWidth": 160
                        },
                        "tableData": {
                            "columnOrder": 4,
                            "groupSort": "asc",
                            "width": "calc((100% - (0px)) / 6)",
                            "id": 2
                        },
                        "hidden": false
                    },
                    {
                        "title": "Dest",
                        "field": "customer",
                        "cellStyle": {
                            "minWidth": 160
                        },
                        "tableData": {
                            "columnOrder": 5,
                            "groupSort": "asc",
                            "width": "calc((100% - (0px)) / 6)",
                            "id": 3
                        },
                        "hidden": false
                    },
                    {
                        "title": "Trailer # ",
                        "field": "trailerNum",
                        "tableData": {
                            "columnOrder": 6,
                            "groupSort": "asc",
                            "width": "calc((100% - (0px)) / 9)",
                            "id": 6
                        },
                        "hidden": false
                    },
                    {
                        "title": "Dest State",
                        "field": "destinationState",
                        "tableData": {
                            "columnOrder": 7,
                            "groupSort": "asc",
                            "width": "calc((100% - (0px)) / 6)",
                            "id": 4
                        },
                        "hidden": false
                    },
                    {
                        "title": "Ship Condition",
                        "field": "shippingCondition",
                        "tableData": {
                            "columnOrder": 8,
                            "groupSort": "asc",
                            "width": "calc((100% - (0px)) / 6)",
                            "id": 5
                        },
                        "hidden": false
                    },
                    {
                        "title": "Dest City",
                        "field": "destinationCity",
                        "tableData": {
                            "columnOrder": 9,
                            "groupSort": "asc",
                            "width": "calc((100% - (0px)) / 6)",
                            "id": 6
                        },
                        "hidden": false
                    },
                    {
                        "title": "Shipment Status",
                        "field": "inboundOrderExecutionBucketDesc",
                        "tableData": {
                            "columnOrder": 10,
                            "groupSort": "asc",
                            "width": "calc((100% - (0px)) / 9)",
                            "id": 10
                        },
                        "hidden": false
                    }
                ]
            }
        },
        "initialLayouts": {
            "NETWORK": {
                "tableName": "network",
                "views": [
                    {
                        "id": "726e2559-baa9-4e41-9bd2-b84dff86b223",
                        "name": "Wallomart Unhealthy",
                        "default": false,
                        "filters": {
                            "sites": {
                                "type": "sites",
                                "name": "Origin",
                                "data": null
                            },
                            "searchStringList": {
                                "type": "text",
                                "name": "Search",
                                "stringToArray": true,
                                "data": null
                            },
                            "destName": {
                                "type": "text",
                                "name": "Customer",
                                "data": "walmart*"
                            },
                            "destState": {
                                "type": "text",
                                "name": "Destination State",
                                "data": null
                            },
                            "destCity": {
                                "type": "text",
                                "name": "Destination City",
                                "data": null
                            },
                            "orderStatusBucket": {
                                "type": "checkbox",
                                "name": "Order Status",
                                "data": [
                                    {
                                        "name": "Order Block Free",
                                        "value": "ORDER_BLOCK_FREE",
                                        "checked": false
                                    },
                                    {
                                        "name": "Order Blocked",
                                        "value": "ORDER_BLOCKED",
                                        "checked": true
                                    }
                                ]
                            },
                            "orderTypes": {
                                "type": "checkbox",
                                "name": "Order Type",
                                "data": [
                                    {
                                        "name": "Cust. Order",
                                        "value": "CUST",
                                        "checked": false
                                    },
                                    {
                                        "name": "STO",
                                        "value": "STO",
                                        "checked": false
                                    }
                                ]
                            },
                            "tariffServiceCodeList": {
                                "type": "text",
                                "stringToArray": true,
                                "name": "Carrier Service Code",
                                "data": null
                            },
                            "orderExecutionBucket": {
                                "type": "checkbox",
                                "name": "Shipment Status",
                                "data": [
                                    {
                                        "name": "Transportation to be planned",
                                        "value": "TRANS_PLAN_UNASSIGNED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation planning",
                                        "value": "TRANS_PLAN_OPEN",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation processing",
                                        "value": "TRANS_PLAN_PROCESSING",
                                        "checked": true
                                    },
                                    {
                                        "name": "Transportation Carrier committed",
                                        "value": "TRANS_PLAN_TENDER_ACCEPTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Shipment Created",
                                        "value": "SHIPMENT_CREATED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Checked in by DC",
                                        "value": "CHECK_IN",
                                        "checked": false
                                    },
                                    {
                                        "name": "Loading Started",
                                        "value": "LOADING_STARTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Ready for Pickup",
                                        "value": "TRANS_EXEC_READY_PICK_UP",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Transit",
                                        "value": "TRANS_EXEC_IN_TRANSIT",
                                        "checked": false
                                    },
                                    {
                                        "name": "Delivery confirmed",
                                        "value": "TRANS_EXEC_DELIVERY_CONFIRMED",
                                        "checked": false
                                    }
                                ]
                            },
                            "inboundOrderExecutionBucket": {
                                "type": "checkbox",
                                "name": "Inbound Shipment Status",
                                "data": [
                                    {
                                        "name": "In Transit",
                                        "value": "INBOUND_IN_TRANSIT",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Yard",
                                        "value": "INBOUND_IN_YARD",
                                        "checked": false
                                    },
                                    {
                                        "name": "Unloading",
                                        "value": "INBOUND_UNLOADING",
                                        "checked": false
                                    },
                                    {
                                        "name": "Unloading Completed",
                                        "value": "INBOUND_UNLOADING_COMPLETED",
                                        "checked": false
                                    }
                                ]
                            },
                            "loadReadyDateTimeOriginTZ": {
                                "type": "date",
                                "dummytime": true,
                                "timeformat": "T",
                                "startTime": "[ 00:00:00.000]",
                                "endTime": "[ 23:59:59.999]",
                                "name": "Carrier Ready Date",
                                "shortName": "Carr. Ready Date",
                                "data": null
                            },
                            "searchString": {
                                "type": "text",
                                "name": "Search String",
                                "data": null
                            },
                            "tariffServiceCode": {
                                "type": "text",
                                "name": "Carrier Service Code",
                                "data": null
                            }
                        },
                        "columns": [
                            {
                                "title": "Shipment #",
                                "field": "shipmentNum",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Delivery #",
                                "field": "deliveryNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 1,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 1
                                }
                            },
                            {
                                "title": "Shipment Status",
                                "field": "orderExecutionBucketDesc",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 2,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 2
                                }
                            },
                            {
                                "title": "Inbound Shipment Status",
                                "field": "inboundOrderExecutionBucketDesc",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 3,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 3
                                }
                            },
                            {
                                "title": "Order #",
                                "field": "orderNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 4,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 4
                                }
                            },
                            {
                                "title": "Order Status",
                                "field": "orderStatus",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 5,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 5
                                }
                            },
                            {
                                "title": "Order Type",
                                "field": "orderType",
                                "tableData": {
                                    "columnOrder": 6,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 6
                                }
                            },
                            {
                                "title": "Customer",
                                "field": "customer",
                                "tableData": {
                                    "columnOrder": 7,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 7
                                }
                            },
                            {
                                "title": "Origin ID",
                                "field": "originId",
                                "tableData": {
                                    "columnOrder": 8,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 8
                                }
                            },
                            {
                                "title": "Origin",
                                "field": "origin",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 9,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 9
                                }
                            },
                            {
                                "title": "Dest City",
                                "field": "destinationCity",
                                "tableData": {
                                    "columnOrder": 10,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 10
                                }
                            },
                            {
                                "title": "Dest State",
                                "field": "destinationState",
                                "tableData": {
                                    "columnOrder": 11,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 11
                                }
                            },
                            {
                                "title": "Original RDD",
                                "field": "originalRequestedDeliveryDatetime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 12,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 12
                                }
                            },
                            {
                                "title": "Carrier Ready Date & Time",
                                "field": "loadReadyDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 13,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 13
                                }
                            },
                            {
                                "title": "Carrier",
                                "field": "tariffServiceCode",
                                "tableData": {
                                    "columnOrder": 14,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 14
                                }
                            },
                            {
                                "title": "Ship Condition",
                                "field": "shippingCondition",
                                "tableData": {
                                    "columnOrder": 15,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 15
                                }
                            },
                            {
                                "title": "Reason for Alert",
                                "field": "reasonCodeString",
                                "cellStyle": {
                                    "minWidth": 320
                                },
                                "tableData": {
                                    "columnOrder": 16,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 16
                                }
                            },
                            {
                                "title": "Fourkites ETA",
                                "field": "expectedDeliveryDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "disableClick": true
                            },
                            {
                                "title": "Sales Org",
                                "field": "salesOrg",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 18,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 18
                                }
                            },
                            {
                                "title": "Distribution Channel",
                                "field": "distributionChannel",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 19,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 19
                                }
                            },
                            {
                                "title": "Sales Office",
                                "field": "salesOffice",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 20,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 20
                                }
                            },
                            {
                                "title": "Sales Group",
                                "field": "salesGroup",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 21,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 21
                                }
                            }
                        ]
                    },
                    {
                        "id": "9c0dcab2-256d-4922-89fe-96da76ea0ef8",
                        "name": "Blank",
                        "default": true,
                        "filters": {
                            "sites": {
                                "type": "sites",
                                "name": "Origin",
                                "data": [
                                    "2042",
                                    "2425",
                                    "2822"
                                ]
                            },
                            "searchString": {
                                "type": "text",
                                "name": "Search String",
                                "data": null
                            },
                            "destName": {
                                "type": "text",
                                "name": "Customer",
                                "data": null
                            },
                            "destState": {
                                "type": "text",
                                "name": "Destination State",
                                "data": null
                            },
                            "destCity": {
                                "type": "text",
                                "name": "Destination City",
                                "data": null
                            },
                            "orderStatusBucket": {
                                "type": "checkbox",
                                "name": "Order Status",
                                "data": [
                                    {
                                        "name": "Order Block Free",
                                        "value": "ORDER_BLOCK_FREE",
                                        "checked": true
                                    },
                                    {
                                        "name": "Order Blocked",
                                        "value": "ORDER_BLOCKED",
                                        "checked": true
                                    }
                                ]
                            },
                            "orderTypes": {
                                "type": "checkbox",
                                "name": "Order Type",
                                "data": [
                                    {
                                        "name": "Cust. Order",
                                        "value": "CUST",
                                        "checked": false
                                    },
                                    {
                                        "name": "STO",
                                        "value": "STO",
                                        "checked": false
                                    }
                                ]
                            },
                            "tariffServiceCode": {
                                "type": "text",
                                "name": "Carrier Service Code",
                                "data": null
                            },
                            "orderExecutionBucket": {
                                "type": "checkbox",
                                "name": "Shipment Status",
                                "data": [
                                    {
                                        "name": "Transportation to be planned",
                                        "value": "TRANS_PLAN_UNASSIGNED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation planning",
                                        "value": "TRANS_PLAN_OPEN",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation processing",
                                        "value": "TRANS_PLAN_PROCESSING",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation Carrier committed",
                                        "value": "TRANS_PLAN_TENDER_ACCEPTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Shipment Created",
                                        "value": "SHIPMENT_CREATED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Checked in by DC",
                                        "value": "CHECK_IN",
                                        "checked": false
                                    },
                                    {
                                        "name": "Loading Started",
                                        "value": "LOADING_STARTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Ready for Pickup",
                                        "value": "TRANS_EXEC_READY_PICK_UP",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Transit",
                                        "value": "TRANS_EXEC_IN_TRANSIT",
                                        "checked": false
                                    },
                                    {
                                        "name": "Delivery confirmed",
                                        "value": "TRANS_EXEC_DELIVERY_CONFIRMED",
                                        "checked": false
                                    }
                                ]
                            },
                            "loadReadyDateTimeOriginTZ": {
                                "type": "date",
                                "dummytime": true,
                                "timeformat": "T",
                                "startTime": "[ 00:00:00.000]",
                                "endTime": "[ 23:59:59.999]",
                                "name": "Carrier Ready Date",
                                "shortName": "Carr. Ready Date",
                                "data": null
                            }
                        },
                        "columns": [
                            {
                                "title": "Shipment #",
                                "field": "shipmentNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 0,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 0
                                }
                            },
                            {
                                "title": "Delivery #",
                                "field": "deliveryNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 1,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 1
                                }
                            },
                            {
                                "title": "Shipment Status",
                                "field": "orderExecutionBucketDesc",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 2,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 2
                                }
                            },
                            {
                                "title": "Order #",
                                "field": "orderNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 3,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 3
                                }
                            },
                            {
                                "title": "Order Status",
                                "field": "orderStatus",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 4,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 4
                                }
                            },
                            {
                                "title": "Order Type",
                                "field": "orderType",
                                "tableData": {
                                    "columnOrder": 5,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 5
                                }
                            },
                            {
                                "title": "Customer",
                                "field": "customer",
                                "tableData": {
                                    "columnOrder": 6,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 6
                                }
                            },
                            {
                                "title": "Origin ID",
                                "field": "originId",
                                "tableData": {
                                    "columnOrder": 7,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 7
                                }
                            },
                            {
                                "title": "Origin",
                                "field": "origin",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 8,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 8
                                }
                            },
                            {
                                "title": "Dest City",
                                "field": "destinationCity",
                                "tableData": {
                                    "columnOrder": 9,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 9
                                }
                            },
                            {
                                "title": "Dest State",
                                "field": "destinationState",
                                "tableData": {
                                    "columnOrder": 10,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 10
                                }
                            },
                            {
                                "title": "Carrier Ready Date & Time",
                                "field": "loadReadyDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 11,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 11
                                }
                            },
                            {
                                "title": "Carrier",
                                "field": "tariffServiceCode",
                                "tableData": {
                                    "columnOrder": 12,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 12
                                }
                            },
                            {
                                "title": "Ship Condition",
                                "field": "shippingCondition",
                                "tableData": {
                                    "columnOrder": 13,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 13
                                }
                            },
                            {
                                "title": "Reason for Alert",
                                "field": "reasonCodeString",
                                "cellStyle": {
                                    "minWidth": 320
                                },
                                "tableData": {
                                    "columnOrder": 14,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 14
                                }
                            },
                            {
                                "title": "Fourkites ETA",
                                "field": "expectedDeliveryDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "disableClick": true
                            }
                        ]
                    },
                    {
                        "id": "5298a993-15c7-47e4-ac59-91f769097ed0",
                        "name": "Blank_2",
                        "default": false,
                        "filters": {
                            "sites": {
                                "type": "sites",
                                "name": "Origin",
                                "data": [
                                    "2042",
                                    "2425",
                                    "2822"
                                ]
                            },
                            "searchString": {
                                "type": "text",
                                "name": "Search String",
                                "data": null
                            },
                            "destName": {
                                "type": "text",
                                "name": "Customer",
                                "data": null
                            },
                            "destState": {
                                "type": "text",
                                "name": "Destination State",
                                "data": null
                            },
                            "destCity": {
                                "type": "text",
                                "name": "Destination City",
                                "data": null
                            },
                            "orderStatusBucket": {
                                "type": "checkbox",
                                "name": "Order Status",
                                "data": [
                                    {
                                        "name": "Order Block Free",
                                        "value": "ORDER_BLOCK_FREE",
                                        "checked": true
                                    },
                                    {
                                        "name": "Order Blocked",
                                        "value": "ORDER_BLOCKED",
                                        "checked": true
                                    }
                                ]
                            },
                            "orderTypes": {
                                "type": "checkbox",
                                "name": "Order Type",
                                "data": [
                                    {
                                        "name": "Cust. Order",
                                        "value": "CUST",
                                        "checked": true
                                    },
                                    {
                                        "name": "STO",
                                        "value": "STO",
                                        "checked": false
                                    }
                                ]
                            },
                            "tariffServiceCode": {
                                "type": "text",
                                "name": "Carrier Service Code",
                                "data": null
                            },
                            "orderExecutionBucket": {
                                "type": "checkbox",
                                "name": "Shipment Status",
                                "data": [
                                    {
                                        "name": "Transportation to be planned",
                                        "value": "TRANS_PLAN_UNASSIGNED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation planning",
                                        "value": "TRANS_PLAN_OPEN",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation processing",
                                        "value": "TRANS_PLAN_PROCESSING",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation Carrier committed",
                                        "value": "TRANS_PLAN_TENDER_ACCEPTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Shipment Created",
                                        "value": "SHIPMENT_CREATED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Checked in by DC",
                                        "value": "CHECK_IN",
                                        "checked": false
                                    },
                                    {
                                        "name": "Loading Started",
                                        "value": "LOADING_STARTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Ready for Pickup",
                                        "value": "TRANS_EXEC_READY_PICK_UP",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Transit",
                                        "value": "TRANS_EXEC_IN_TRANSIT",
                                        "checked": false
                                    },
                                    {
                                        "name": "Delivery confirmed",
                                        "value": "TRANS_EXEC_DELIVERY_CONFIRMED",
                                        "checked": false
                                    }
                                ]
                            },
                            "loadReadyDateTimeOriginTZ": {
                                "type": "date",
                                "dummytime": true,
                                "timeformat": "T",
                                "startTime": "[ 00:00:00.000]",
                                "endTime": "[ 23:59:59.999]",
                                "name": "Carrier Ready Date",
                                "shortName": "Carr. Ready Date",
                                "data": null
                            }
                        },
                        "columns": [
                            {
                                "title": "Shipment #",
                                "field": "shipmentNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 0,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 0
                                }
                            },
                            {
                                "title": "Delivery #",
                                "field": "deliveryNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 1,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 1
                                }
                            },
                            {
                                "title": "Shipment Status",
                                "field": "orderExecutionBucketDesc",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 2,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 2
                                }
                            },
                            {
                                "title": "Order #",
                                "field": "orderNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 3,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 3
                                }
                            },
                            {
                                "title": "Order Status",
                                "field": "orderStatus",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 4,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 4
                                }
                            },
                            {
                                "title": "Order Type",
                                "field": "orderType",
                                "tableData": {
                                    "columnOrder": 5,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 5
                                }
                            },
                            {
                                "title": "Customer",
                                "field": "customer",
                                "tableData": {
                                    "columnOrder": 6,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 6
                                }
                            },
                            {
                                "title": "Origin ID",
                                "field": "originId",
                                "tableData": {
                                    "columnOrder": 7,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 7
                                }
                            },
                            {
                                "title": "Origin",
                                "field": "origin",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 8,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 8
                                }
                            },
                            {
                                "title": "Dest City",
                                "field": "destinationCity",
                                "tableData": {
                                    "columnOrder": 9,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 9
                                }
                            },
                            {
                                "title": "Dest State",
                                "field": "destinationState",
                                "tableData": {
                                    "columnOrder": 10,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 10
                                }
                            },
                            {
                                "title": "Carrier Ready Date & Time",
                                "field": "loadReadyDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 11,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 11
                                }
                            },
                            {
                                "title": "Carrier",
                                "field": "tariffServiceCode",
                                "tableData": {
                                    "columnOrder": 12,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 12
                                }
                            },
                            {
                                "title": "Ship Condition",
                                "field": "shippingCondition",
                                "tableData": {
                                    "columnOrder": 13,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 13
                                }
                            },
                            {
                                "title": "Reason for Alert",
                                "field": "reasonCodeString",
                                "cellStyle": {
                                    "minWidth": 320
                                },
                                "tableData": {
                                    "columnOrder": 14,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 14
                                }
                            },
                            {
                                "title": "Fourkites ETA",
                                "field": "expectedDeliveryDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "disableClick": true
                            }
                        ]
                    },
                    {
                        "id": "15b3c9a9-fe0a-4822-9693-ee020d5f7980",
                        "name": "Wallomart Unhealthy - BlockFree",
                        "default": false,
                        "filters": {
                            "sites": {
                                "type": "sites",
                                "name": "Origin",
                                "data": null
                            },
                            "searchStringList": {
                                "type": "text",
                                "name": "Search",
                                "stringToArray": true,
                                "data": null
                            },
                            "destName": {
                                "type": "text",
                                "name": "Customer",
                                "data": null
                            },
                            "destState": {
                                "type": "text",
                                "name": "Destination State",
                                "data": null
                            },
                            "destCity": {
                                "type": "text",
                                "name": "Destination City",
                                "data": null
                            },
                            "orderStatusBucket": {
                                "type": "checkbox",
                                "name": "Order Status",
                                "data": [
                                    {
                                        "name": "Order Block Free",
                                        "value": "ORDER_BLOCK_FREE",
                                        "checked": false
                                    },
                                    {
                                        "name": "Order Blocked",
                                        "value": "ORDER_BLOCKED",
                                        "checked": true
                                    }
                                ]
                            },
                            "orderTypes": {
                                "type": "checkbox",
                                "name": "Order Type",
                                "data": [
                                    {
                                        "name": "Cust. Order",
                                        "value": "CUST",
                                        "checked": false
                                    },
                                    {
                                        "name": "STO",
                                        "value": "STO",
                                        "checked": false
                                    }
                                ]
                            },
                            "tariffServiceCodeList": {
                                "type": "text",
                                "stringToArray": true,
                                "name": "Carrier Service Code",
                                "data": null
                            },
                            "orderExecutionBucket": {
                                "type": "checkbox",
                                "name": "Shipment Status",
                                "data": [
                                    {
                                        "name": "Transportation to be planned",
                                        "value": "TRANS_PLAN_UNASSIGNED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation planning",
                                        "value": "TRANS_PLAN_OPEN",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation processing",
                                        "value": "TRANS_PLAN_PROCESSING",
                                        "checked": true
                                    },
                                    {
                                        "name": "Transportation Carrier committed",
                                        "value": "TRANS_PLAN_TENDER_ACCEPTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Shipment Created",
                                        "value": "SHIPMENT_CREATED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Checked in by DC",
                                        "value": "CHECK_IN",
                                        "checked": false
                                    },
                                    {
                                        "name": "Loading Started",
                                        "value": "LOADING_STARTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Ready for Pickup",
                                        "value": "TRANS_EXEC_READY_PICK_UP",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Transit",
                                        "value": "TRANS_EXEC_IN_TRANSIT",
                                        "checked": false
                                    },
                                    {
                                        "name": "Delivery confirmed",
                                        "value": "TRANS_EXEC_DELIVERY_CONFIRMED",
                                        "checked": false
                                    }
                                ]
                            },
                            "inboundOrderExecutionBucket": {
                                "type": "checkbox",
                                "name": "Inbound Shipment Status",
                                "data": [
                                    {
                                        "name": "In Transit",
                                        "value": "INBOUND_IN_TRANSIT",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Yard",
                                        "value": "INBOUND_IN_YARD",
                                        "checked": false
                                    },
                                    {
                                        "name": "Unloading",
                                        "value": "INBOUND_UNLOADING",
                                        "checked": false
                                    },
                                    {
                                        "name": "Unloading Completed",
                                        "value": "INBOUND_UNLOADING_COMPLETED",
                                        "checked": false
                                    }
                                ]
                            },
                            "loadReadyDateTimeOriginTZ": {
                                "type": "date",
                                "dummytime": true,
                                "timeformat": "T",
                                "startTime": "[ 00:00:00.000]",
                                "endTime": "[ 23:59:59.999]",
                                "name": "Carrier Ready Date",
                                "shortName": "Carr. Ready Date",
                                "data": null
                            },
                            "searchString": {
                                "type": "text",
                                "name": "Search String",
                                "data": null
                            },
                            "tariffServiceCode": {
                                "type": "text",
                                "name": "Carrier Service Code",
                                "data": null
                            }
                        },
                        "columns": [
                            {
                                "title": "Shipment #",
                                "field": "shipmentNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 0,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 13)",
                                    "id": 0
                                }
                            },
                            {
                                "title": "Shipment Status",
                                "field": "orderExecutionBucketDesc",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 1,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 13)",
                                    "id": 1
                                }
                            },
                            {
                                "title": "Order #",
                                "field": "orderNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 2,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 13)",
                                    "id": 2
                                }
                            },
                            {
                                "title": "Order Status",
                                "field": "orderStatus",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 3,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 13)",
                                    "id": 3
                                }
                            },
                            {
                                "title": "Order Type",
                                "field": "orderType",
                                "tableData": {
                                    "columnOrder": 4,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 13)",
                                    "id": 4
                                }
                            },
                            {
                                "title": "Customer",
                                "field": "customer",
                                "tableData": {
                                    "columnOrder": 5,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 13)",
                                    "id": 5
                                }
                            },
                            {
                                "title": "Origin",
                                "field": "origin",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 6,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 13)",
                                    "id": 6
                                }
                            },
                            {
                                "title": "Dest City",
                                "field": "destinationCity",
                                "tableData": {
                                    "columnOrder": 7,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 13)",
                                    "id": 7
                                }
                            },
                            {
                                "title": "Dest State",
                                "field": "destinationState",
                                "tableData": {
                                    "columnOrder": 8,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 13)",
                                    "id": 8
                                }
                            },
                            {
                                "title": "Carrier Ready Date & Time",
                                "field": "loadReadyDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 9,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 13)",
                                    "id": 9
                                }
                            },
                            {
                                "title": "Carrier",
                                "field": "tariffServiceCode",
                                "tableData": {
                                    "columnOrder": 10,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 13)",
                                    "id": 10
                                }
                            },
                            {
                                "title": "Ship Condition",
                                "field": "shippingCondition",
                                "tableData": {
                                    "columnOrder": 11,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 13)",
                                    "id": 11
                                }
                            },
                            {
                                "title": "Reason for Alert",
                                "field": "reasonCodeString",
                                "cellStyle": {
                                    "minWidth": 320
                                },
                                "tableData": {
                                    "columnOrder": 12,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 13)",
                                    "id": 12
                                }
                            }
                        ]
                    },
                    {
                        "id": "e01242ae-2961-4658-ab9a-5b0dcda171bb",
                        "name": "400error",
                        "default": false,
                        "filters": {
                            "sites": {
                                "type": "sites",
                                "name": "Origin",
                                "data": null
                            },
                            "searchStringList": {
                                "type": "text",
                                "name": "Search",
                                "stringToArray": true,
                                "data": null
                            },
                            "destName": {
                                "type": "text",
                                "name": "Customer",
                                "data": null
                            },
                            "destState": {
                                "type": "text",
                                "name": "Destination State",
                                "data": null
                            },
                            "destCity": {
                                "type": "text",
                                "name": "Destination City",
                                "data": null
                            },
                            "orderStatusBucket": {
                                "type": "checkbox",
                                "name": "Order Status",
                                "data": [
                                    {
                                        "name": "Order Block Free",
                                        "value": "ORDER_BLOCK_FREE",
                                        "checked": false
                                    },
                                    {
                                        "name": "Order Blocked",
                                        "value": "ORDER_BLOCKED",
                                        "checked": true
                                    }
                                ]
                            },
                            "orderTypes": {
                                "type": "checkbox",
                                "name": "Order Type",
                                "data": [
                                    {
                                        "name": "Cust. Order",
                                        "value": "CUST",
                                        "checked": false
                                    },
                                    {
                                        "name": "STO",
                                        "value": "STO",
                                        "checked": false
                                    }
                                ]
                            },
                            "tariffServiceCodeList": {
                                "type": "text",
                                "stringToArray": true,
                                "name": "Carrier Service Code",
                                "data": null
                            },
                            "orderExecutionBucket": {
                                "type": "checkbox",
                                "name": "Shipment Status",
                                "data": [
                                    {
                                        "name": "Transportation to be planned",
                                        "value": "TRANS_PLAN_UNASSIGNED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation planning",
                                        "value": "TRANS_PLAN_OPEN",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation processing",
                                        "value": "TRANS_PLAN_PROCESSING",
                                        "checked": true
                                    },
                                    {
                                        "name": "Transportation Carrier committed",
                                        "value": "TRANS_PLAN_TENDER_ACCEPTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Shipment Created",
                                        "value": "SHIPMENT_CREATED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Checked in by DC",
                                        "value": "CHECK_IN",
                                        "checked": false
                                    },
                                    {
                                        "name": "Loading Started",
                                        "value": "LOADING_STARTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Ready for Pickup",
                                        "value": "TRANS_EXEC_READY_PICK_UP",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Transit",
                                        "value": "TRANS_EXEC_IN_TRANSIT",
                                        "checked": false
                                    },
                                    {
                                        "name": "Delivery confirmed",
                                        "value": "TRANS_EXEC_DELIVERY_CONFIRMED",
                                        "checked": false
                                    }
                                ]
                            },
                            "inboundOrderExecutionBucket": {
                                "type": "checkbox",
                                "name": "Inbound Shipment Status",
                                "data": [
                                    {
                                        "name": "In Transit",
                                        "value": "INBOUND_IN_TRANSIT",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Yard",
                                        "value": "INBOUND_IN_YARD",
                                        "checked": false
                                    },
                                    {
                                        "name": "Unloading",
                                        "value": "INBOUND_UNLOADING",
                                        "checked": false
                                    },
                                    {
                                        "name": "Unloading Completed",
                                        "value": "INBOUND_UNLOADING_COMPLETED",
                                        "checked": false
                                    }
                                ]
                            },
                            "loadReadyDateTimeOriginTZ": {
                                "type": "date",
                                "dummytime": true,
                                "timeformat": "T",
                                "startTime": "[00:00:00.000]",
                                "endTime": "[23:59:59.999]",
                                "name": "Carrier Ready Date",
                                "shortName": "Carr. Ready Date",
                                "data": null
                            }
                        },
                        "columns": [
                            {
                                "title": "Shipment #",
                                "field": "shipmentNum",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Delivery #",
                                "field": "deliveryNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 1,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 1
                                }
                            },
                            {
                                "title": "Shipment Status",
                                "field": "orderExecutionBucketDesc",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 2,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 2
                                }
                            },
                            {
                                "title": "Inbound Shipment Status",
                                "field": "inboundOrderExecutionBucketDesc",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 3,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 3
                                }
                            },
                            {
                                "title": "Order #",
                                "field": "orderNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 4,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 4
                                }
                            },
                            {
                                "title": "Order Status",
                                "field": "orderStatus",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 5,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 5
                                }
                            },
                            {
                                "title": "Order Type",
                                "field": "orderType",
                                "tableData": {
                                    "columnOrder": 6,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 6
                                }
                            },
                            {
                                "title": "Customer",
                                "field": "customer",
                                "tableData": {
                                    "columnOrder": 7,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 7
                                }
                            },
                            {
                                "title": "Origin ID",
                                "field": "originId",
                                "tableData": {
                                    "columnOrder": 8,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 8
                                }
                            },
                            {
                                "title": "Origin",
                                "field": "origin",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 9,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 9
                                }
                            },
                            {
                                "title": "Dest City",
                                "field": "destinationCity",
                                "tableData": {
                                    "columnOrder": 10,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 10
                                }
                            },
                            {
                                "title": "Dest State",
                                "field": "destinationState",
                                "tableData": {
                                    "columnOrder": 11,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 11
                                }
                            },
                            {
                                "title": "Original RDD",
                                "field": "originalRequestedDeliveryDatetime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 12,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 12
                                }
                            },
                            {
                                "title": "Carrier Ready Date & Time",
                                "field": "loadReadyDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 13,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 13
                                }
                            },
                            {
                                "title": "Carrier",
                                "field": "tariffServiceCode",
                                "tableData": {
                                    "columnOrder": 14,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 14
                                }
                            },
                            {
                                "title": "Ship Condition",
                                "field": "shippingCondition",
                                "tableData": {
                                    "columnOrder": 15,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 15
                                }
                            },
                            {
                                "title": "Reason for Alert",
                                "field": "reasonCodeString",
                                "cellStyle": {
                                    "minWidth": 320
                                },
                                "tableData": {
                                    "columnOrder": 16,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 16
                                }
                            },
                            {
                                "title": "Fourkites ETA",
                                "field": "expectedDeliveryDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "disableClick": true
                            },
                            {
                                "title": "Sales Org",
                                "field": "salesOrg",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 18,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 18
                                }
                            },
                            {
                                "title": "Distribution Channel",
                                "field": "distributionChannel",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 19,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 19
                                }
                            },
                            {
                                "title": "Sales Office",
                                "field": "salesOffice",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 20,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 20
                                }
                            },
                            {
                                "title": "Sales Group",
                                "field": "salesGroup",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 21,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 22)",
                                    "id": 21
                                }
                            }
                        ]
                    }
                ]
            },
            "ORDERS": {
                "tableName": "orders",
                "views": [
                    {
                        "id": "2fc48d20-df7f-43cf-979d-d5868ac2f373",
                        "name": "Another View",
                        "default": true,
                        "filters": {
                            "searchStringList": {
                                "type": "text",
                                "name": "Search",
                                "stringToArray": true,
                                "data": null
                            },
                            "destName": {
                                "type": "text",
                                "name": "Customer",
                                "data": null
                            },
                            "sites": {
                                "type": "sites",
                                "name": "Origin",
                                "data": null
                            },
                            "shippingConditionList": {
                                "type": "text",
                                "stringToArray": true,
                                "name": "Shipping Condition",
                                "data": null
                            },
                            "requestedDeliveryDate": {
                                "type": "date",
                                "name": "Requested Delivery Date",
                                "shortName": "Req. Delivery Date",
                                "data": null
                            },
                            "deliveryBlockCodeList": {
                                "type": "textcheck",
                                "stringToArray": true,
                                "name": "Delivery/Credit Block",
                                "placeholder": "Enter delivery block code",
                                "shortName": "Delivery Block Code",
                                "addon": "creditOnHold",
                                "data": null
                            },
                            "creditOnHold": {
                                "hidden": true,
                                "type": "text",
                                "name": "Show Only Credit Hold Orders",
                                "data": null
                            },
                            "confirmedEquivCubes": {
                                "type": "checkbox",
                                "name": "Confirmed Cube",
                                "shortName": "Confirmed Cube",
                                "data": [
                                    {
                                        "name": "< 1000",
                                        "value": {
                                            "lte": 999.9999999
                                        },
                                        "checked": false
                                    },
                                    {
                                        "name": "1000 - 2000",
                                        "value": {
                                            "gte": 1000,
                                            "lte": 1999.9999999
                                        },
                                        "checked": false
                                    },
                                    {
                                        "name": "2000 - 3000",
                                        "value": {
                                            "gte": 2000,
                                            "lte": 2999.9999999
                                        },
                                        "checked": false
                                    },
                                    {
                                        "name": "> 3000",
                                        "value": {
                                            "gte": 3000
                                        },
                                        "checked": false
                                    }
                                ]
                            },
                            "destState": {
                                "type": "text",
                                "name": "Destination State",
                                "data": "QLD"
                            },
                            "destCity": {
                                "type": "text",
                                "name": "Destination City",
                                "data": null
                            },
                            "matAvailDate": {
                                "type": "date",
                                "name": "Material Availability Date",
                                "shortName": "Mat.Avail Date",
                                "data": null
                            },
                            "orderStatusBucket": {
                                "type": "ordercheckbox",
                                "name": "Order Status",
                                "data": [
                                    {
                                        "name": "100% Confirmed Cube",
                                        "value": "SO_COMPLETELY_CONFIRMED_CUBE",
                                        "checked": false,
                                        "parent": "Order Block Free"
                                    },
                                    {
                                        "name": "Less than 100% Confirmed Cube",
                                        "value": "SO_NOT_COMPLETELY_CONFIRMED_CUBE",
                                        "checked": false,
                                        "parent": "Order Block Free"
                                    },
                                    {
                                        "name": "Back Orders Block Free",
                                        "value": "SO_BACK_ORDER_BLOCK_FREE",
                                        "checked": false,
                                        "parent": "Order Block Free"
                                    },
                                    {
                                        "name": "Visible in TMS",
                                        "value": "SO_BLOCKED_TMS_PLANNED_VISIBLE",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Not visible in TMS",
                                        "value": "SO_BLOCKED_TMS_PLANNED_NOT_VISIBLE",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Pickup not scheduled",
                                        "value": "SO_BLOCKED_NON_TMS_PLANNED_PICKUP_NOT_SCHEDULED",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Pickup/Package Multi Block",
                                        "value": "SO_BLOCKED_NON_TMS_PLANNED_PICKUP_MULTI_BLOCK",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Immediate Action",
                                        "value": "SO_BLOCKED_NON_TMS_PLANNED_IMMEDIATE_ACTION",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Back Orders Blocked",
                                        "value": "SO_BACK_ORDER_BLOCKED",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    }
                                ]
                            },
                            "orderExecutionBucket": {
                                "type": "checkbox",
                                "name": "Shipment Status",
                                "data": [
                                    {
                                        "name": "Transportation to be planned",
                                        "value": "TRANS_PLAN_UNASSIGNED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation planning",
                                        "value": "TRANS_PLAN_OPEN",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation processing",
                                        "value": "TRANS_PLAN_PROCESSING",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation Carrier committed",
                                        "value": "TRANS_PLAN_TENDER_ACCEPTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Shipment Created",
                                        "value": "SHIPMENT_CREATED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Checked in by DC",
                                        "value": "CHECK_IN",
                                        "checked": false
                                    },
                                    {
                                        "name": "Loading Started",
                                        "value": "LOADING_STARTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Ready for Pickup",
                                        "value": "TRANS_EXEC_READY_PICK_UP",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Transit",
                                        "value": "TRANS_EXEC_IN_TRANSIT",
                                        "checked": false
                                    },
                                    {
                                        "name": "Delivery confirmed",
                                        "value": "TRANS_EXEC_DELIVERY_CONFIRMED",
                                        "checked": false
                                    }
                                ]
                            },
                            "inboundOrderExecutionBucket": {
                                "type": "checkbox",
                                "name": "Inbound Shipment Status",
                                "data": [
                                    {
                                        "name": "In Transit",
                                        "value": "INBOUND_IN_TRANSIT",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Yard",
                                        "value": "INBOUND_IN_YARD",
                                        "checked": false
                                    },
                                    {
                                        "name": "Unloading",
                                        "value": "INBOUND_UNLOADING",
                                        "checked": false
                                    },
                                    {
                                        "name": "Unloading Completed",
                                        "value": "INBOUND_UNLOADING_COMPLETED",
                                        "checked": false
                                    }
                                ]
                            },
                            "orderTypes": {
                                "type": "checkbox",
                                "name": "Order Type",
                                "data": [
                                    {
                                        "name": "All Customer Orders",
                                        "value": "CUST",
                                        "checked": true
                                    },
                                    {
                                        "name": "STOs",
                                        "value": "STO",
                                        "checked": false
                                    }
                                ]
                            },
                            "documentTypes": {
                                "type": "checkbox",
                                "name": "Document type",
                                "data": [
                                    {
                                        "name": "Standard Orders (ZSTD)",
                                        "value": "ZSTD",
                                        "checked": false
                                    },
                                    {
                                        "name": "Multi Plant Orders (ZMPF)",
                                        "value": "ZMPF",
                                        "checked": false
                                    },
                                    {
                                        "name": "VMI Orders (ZVMI)",
                                        "value": "ZVMI",
                                        "checked": false
                                    },
                                    {
                                        "name": "Merged Orders (ZMER)",
                                        "value": "ZMER",
                                        "checked": false
                                    }
                                ]
                            },
                            "orderHealth": {
                                "type": "checkbox",
                                "name": "Health",
                                "data": [
                                    {
                                        "name": "Unhealthy",
                                        "value": "RED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Needs Attention",
                                        "value": "YELLOW",
                                        "checked": false
                                    },
                                    {
                                        "name": "Healthy",
                                        "value": "GREEN",
                                        "checked": false
                                    }
                                ]
                            }
                        },
                        "columns": [
                            {
                                "title": "Order #",
                                "field": "orderNum",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Customer",
                                "field": "customer",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Customer PO #",
                                "field": "customerPoNum",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "LEQ",
                                "field": "loadEquivCube"
                            },
                            {
                                "title": "Confirmed LEQ",
                                "field": "confLoadEquivCube"
                            },
                            {
                                "title": "Allocated LEQ",
                                "field": "allocEquivCube"
                            },
                            {
                                "title": "Origin ID",
                                "field": "originId"
                            },
                            {
                                "title": "Origin",
                                "field": "origin",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "MAD",
                                "field": "matAvailDate",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Original RDD",
                                "field": "originalRequestedDeliveryDatetime",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "RDD",
                                "field": "requestedDeliveryDate",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Ship Condition",
                                "field": "shippingCondition"
                            },
                            {
                                "title": "Delivery Block",
                                "field": "deliveryBlocksString",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Shipment #",
                                "field": "shipmentNum"
                            },
                            {
                                "title": "Delivery #",
                                "field": "deliveryNum",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Carrier Ready Date & Time",
                                "field": "loadReadyDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Dest City",
                                "field": "destinationCity"
                            },
                            {
                                "title": "Dest State",
                                "field": "destinationState"
                            },
                            {
                                "title": "Trailer #",
                                "field": "trailerNum"
                            },
                            {
                                "title": "Shipment Status",
                                "field": "orderExecutionBucketDesc",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Inbound Shipment Status",
                                "field": "inboundOrderExecutionBucketDesc",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Order Status",
                                "field": "orderStatus",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Order Sub Status",
                                "field": "orderStatusBucketDesc",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Order Type",
                                "field": "orderType"
                            },
                            {
                                "title": "Business",
                                "field": "orderBusinessUnit"
                            },
                            {
                                "title": "Reason for Alert",
                                "field": "reasonCodeString",
                                "cellStyle": {
                                    "minWidth": 320
                                }
                            },
                            {
                                "title": "Fourkites ETA",
                                "field": "expectedDeliveryDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Sales Org",
                                "field": "salesOrg",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Distribution Channel",
                                "field": "distributionChannel",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Sales Office",
                                "field": "salesOffice",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Sales Group",
                                "field": "salesGroup",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            }
                        ]
                    },
                    {
                        "id": "b2e66d3b-34e5-4c23-993c-204bfd5097dc",
                        "name": "Another View-2",
                        "default": false,
                        "filters": {
                            "searchStringList": {
                                "type": "text",
                                "name": "Search",
                                "stringToArray": true,
                                "data": null
                            },
                            "destName": {
                                "type": "text",
                                "name": "Customer",
                                "data": null
                            },
                            "sites": {
                                "type": "sites",
                                "name": "Origin",
                                "data": null
                            },
                            "shippingConditionList": {
                                "type": "text",
                                "stringToArray": true,
                                "name": "Shipping Condition",
                                "data": null
                            },
                            "requestedDeliveryDate": {
                                "type": "date",
                                "name": "Requested Delivery Date",
                                "shortName": "Req. Delivery Date",
                                "data": null
                            },
                            "deliveryBlockCodeList": {
                                "type": "textcheck",
                                "stringToArray": true,
                                "name": "Delivery/Credit Block",
                                "placeholder": "Enter delivery block code",
                                "shortName": "Delivery Block Code",
                                "addon": "creditOnHold",
                                "data": null
                            },
                            "creditOnHold": {
                                "hidden": true,
                                "type": "text",
                                "name": "Show Only Credit Hold Orders",
                                "data": null
                            },
                            "confirmedEquivCubes": {
                                "type": "checkbox",
                                "name": "Confirmed Cube",
                                "shortName": "Confirmed Cube",
                                "data": [
                                    {
                                        "name": "< 1000",
                                        "value": {
                                            "lte": 999.9999999
                                        },
                                        "checked": false
                                    },
                                    {
                                        "name": "1000 - 2000",
                                        "value": {
                                            "gte": 1000,
                                            "lte": 1999.9999999
                                        },
                                        "checked": false
                                    },
                                    {
                                        "name": "2000 - 3000",
                                        "value": {
                                            "gte": 2000,
                                            "lte": 2999.9999999
                                        },
                                        "checked": false
                                    },
                                    {
                                        "name": "> 3000",
                                        "value": {
                                            "gte": 3000
                                        },
                                        "checked": false
                                    }
                                ]
                            },
                            "destState": {
                                "type": "text",
                                "name": "Destination State",
                                "data": null
                            },
                            "destCity": {
                                "type": "text",
                                "name": "Destination City",
                                "data": null
                            },
                            "matAvailDate": {
                                "type": "date",
                                "name": "Material Availability Date",
                                "shortName": "Mat.Avail Date",
                                "data": null
                            },
                            "orderStatusBucket": {
                                "type": "ordercheckbox",
                                "name": "Order Status",
                                "data": [
                                    {
                                        "name": "100% Confirmed Cube",
                                        "value": "SO_COMPLETELY_CONFIRMED_CUBE",
                                        "checked": false,
                                        "parent": "Order Block Free"
                                    },
                                    {
                                        "name": "Less than 100% Confirmed Cube",
                                        "value": "SO_NOT_COMPLETELY_CONFIRMED_CUBE",
                                        "checked": false,
                                        "parent": "Order Block Free"
                                    },
                                    {
                                        "name": "Back Orders Block Free",
                                        "value": "SO_BACK_ORDER_BLOCK_FREE",
                                        "checked": false,
                                        "parent": "Order Block Free"
                                    },
                                    {
                                        "name": "Visible in TMS",
                                        "value": "SO_BLOCKED_TMS_PLANNED_VISIBLE",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Not visible in TMS",
                                        "value": "SO_BLOCKED_TMS_PLANNED_NOT_VISIBLE",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Pickup not scheduled",
                                        "value": "SO_BLOCKED_NON_TMS_PLANNED_PICKUP_NOT_SCHEDULED",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Pickup/Package Multi Block",
                                        "value": "SO_BLOCKED_NON_TMS_PLANNED_PICKUP_MULTI_BLOCK",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Immediate Action",
                                        "value": "SO_BLOCKED_NON_TMS_PLANNED_IMMEDIATE_ACTION",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Back Orders Blocked",
                                        "value": "SO_BACK_ORDER_BLOCKED",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    }
                                ]
                            },
                            "orderExecutionBucket": {
                                "type": "checkbox",
                                "name": "Shipment Status",
                                "data": [
                                    {
                                        "name": "Transportation to be planned",
                                        "value": "TRANS_PLAN_UNASSIGNED",
                                        "checked": true
                                    },
                                    {
                                        "name": "Transportation planning",
                                        "value": "TRANS_PLAN_OPEN",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation processing",
                                        "value": "TRANS_PLAN_PROCESSING",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation Carrier committed",
                                        "value": "TRANS_PLAN_TENDER_ACCEPTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Shipment Created",
                                        "value": "SHIPMENT_CREATED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Checked in by DC",
                                        "value": "CHECK_IN",
                                        "checked": false
                                    },
                                    {
                                        "name": "Loading Started",
                                        "value": "LOADING_STARTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Ready for Pickup",
                                        "value": "TRANS_EXEC_READY_PICK_UP",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Transit",
                                        "value": "TRANS_EXEC_IN_TRANSIT",
                                        "checked": false
                                    },
                                    {
                                        "name": "Delivery confirmed",
                                        "value": "TRANS_EXEC_DELIVERY_CONFIRMED",
                                        "checked": false
                                    }
                                ]
                            },
                            "inboundOrderExecutionBucket": {
                                "type": "checkbox",
                                "name": "Inbound Shipment Status",
                                "data": [
                                    {
                                        "name": "In Transit",
                                        "value": "INBOUND_IN_TRANSIT",
                                        "checked": true
                                    },
                                    {
                                        "name": "In Yard",
                                        "value": "INBOUND_IN_YARD",
                                        "checked": true
                                    },
                                    {
                                        "name": "Unloading",
                                        "value": "INBOUND_UNLOADING",
                                        "checked": false
                                    },
                                    {
                                        "name": "Unloading Completed",
                                        "value": "INBOUND_UNLOADING_COMPLETED",
                                        "checked": false
                                    }
                                ]
                            },
                            "orderTypes": {
                                "type": "checkbox",
                                "name": "Order Type",
                                "data": [
                                    {
                                        "name": "All Customer Orders",
                                        "value": "CUST",
                                        "checked": true
                                    },
                                    {
                                        "name": "STOs",
                                        "value": "STO",
                                        "checked": false
                                    }
                                ]
                            },
                            "documentTypes": {
                                "type": "checkbox",
                                "name": "Document type",
                                "data": [
                                    {
                                        "name": "Standard Orders (ZSTD)",
                                        "value": "ZSTD",
                                        "checked": false
                                    },
                                    {
                                        "name": "Multi Plant Orders (ZMPF)",
                                        "value": "ZMPF",
                                        "checked": false
                                    },
                                    {
                                        "name": "VMI Orders (ZVMI)",
                                        "value": "ZVMI",
                                        "checked": false
                                    },
                                    {
                                        "name": "Merged Orders (ZMER)",
                                        "value": "ZMER",
                                        "checked": false
                                    }
                                ]
                            },
                            "orderHealth": {
                                "type": "checkbox",
                                "name": "Health",
                                "data": [
                                    {
                                        "name": "Unhealthy",
                                        "value": "RED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Needs Attention",
                                        "value": "YELLOW",
                                        "checked": false
                                    },
                                    {
                                        "name": "Healthy",
                                        "value": "GREEN",
                                        "checked": true
                                    }
                                ]
                            }
                        },
                        "columns": [
                            {
                                "title": "Order #",
                                "field": "orderNum",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Customer",
                                "field": "customer",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 1,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 1
                                }
                            },
                            {
                                "title": "Customer PO #",
                                "field": "customerPoNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 2,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 2
                                }
                            },
                            {
                                "title": "LEQ",
                                "field": "loadEquivCube",
                                "tableData": {
                                    "columnOrder": 3,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 3
                                }
                            },
                            {
                                "title": "Confirmed LEQ",
                                "field": "confLoadEquivCube",
                                "tableData": {
                                    "columnOrder": 4,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 4
                                }
                            },
                            {
                                "title": "Allocated LEQ",
                                "field": "allocEquivCube",
                                "tableData": {
                                    "columnOrder": 5,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 5
                                }
                            },
                            {
                                "title": "Origin ID",
                                "field": "originId",
                                "tableData": {
                                    "columnOrder": 6,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 6
                                }
                            },
                            {
                                "title": "Origin",
                                "field": "origin",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 7,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 7
                                }
                            },
                            {
                                "title": "MAD",
                                "field": "matAvailDate",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 8,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 8
                                }
                            },
                            {
                                "title": "Original RDD",
                                "field": "originalRequestedDeliveryDatetime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 9,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 9
                                }
                            },
                            {
                                "title": "RDD",
                                "field": "requestedDeliveryDate",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 10,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 10
                                }
                            },
                            {
                                "title": "Ship Condition",
                                "field": "shippingCondition",
                                "tableData": {
                                    "columnOrder": 11,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 11
                                }
                            },
                            {
                                "title": "Delivery Block",
                                "field": "deliveryBlocksString",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 12,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 12
                                }
                            },
                            {
                                "title": "Shipment #",
                                "field": "shipmentNum",
                                "tableData": {
                                    "columnOrder": 13,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 13
                                }
                            },
                            {
                                "title": "Delivery #",
                                "field": "deliveryNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 14,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 14
                                }
                            },
                            {
                                "title": "Carrier Ready Date & Time",
                                "field": "loadReadyDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 15,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 15
                                }
                            },
                            {
                                "title": "Dest City",
                                "field": "destinationCity",
                                "tableData": {
                                    "columnOrder": 16,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 16
                                }
                            },
                            {
                                "title": "Dest State",
                                "field": "destinationState",
                                "tableData": {
                                    "columnOrder": 17,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 17
                                }
                            },
                            {
                                "title": "Trailer #",
                                "field": "trailerNum",
                                "tableData": {
                                    "columnOrder": 18,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 18
                                }
                            },
                            {
                                "title": "Shipment Status",
                                "field": "orderExecutionBucketDesc",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 19,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 19
                                }
                            },
                            {
                                "title": "Inbound Shipment Status",
                                "field": "inboundOrderExecutionBucketDesc",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 20,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 20
                                }
                            },
                            {
                                "title": "Order Status",
                                "field": "orderStatus",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 21,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 21
                                }
                            },
                            {
                                "title": "Order Sub Status",
                                "field": "orderStatusBucketDesc",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 22,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 22
                                }
                            },
                            {
                                "title": "Order Type",
                                "field": "orderType",
                                "tableData": {
                                    "columnOrder": 23,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 23
                                }
                            },
                            {
                                "title": "Business",
                                "field": "orderBusinessUnit",
                                "tableData": {
                                    "columnOrder": 24,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 24
                                }
                            },
                            {
                                "title": "Reason for Alert",
                                "field": "reasonCodeString",
                                "cellStyle": {
                                    "minWidth": 320
                                },
                                "tableData": {
                                    "columnOrder": 25,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 25
                                }
                            },
                            {
                                "title": "Fourkites ETA",
                                "field": "expectedDeliveryDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Sales Org",
                                "field": "salesOrg",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 27,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 27
                                }
                            },
                            {
                                "title": "Distribution Channel",
                                "field": "distributionChannel",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 28,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 28
                                }
                            },
                            {
                                "title": "Sales Office",
                                "field": "salesOffice",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 29,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 29
                                }
                            },
                            {
                                "title": "Sales Group",
                                "field": "salesGroup",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 30,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 31)",
                                    "id": 30
                                }
                            }
                        ]
                    }
                ]
            },
            "DISTINBOUND": {
                "tableName": "distinbound",
                "views": [
                    {
                        "id": 1,
                        "name": "Inbound 1",
                        "default": true,
                        "filters": {
                            "searchString": {
                                "type": "text",
                                "name": "Search String",
                                "data": null
                            },
                            "inboundOrderExecutionBucket": {
                                "type": "checkbox",
                                "name": "Shipment Status",
                                "data": [
                                    {
                                        "name": "In Transit",
                                        "value": "INBOUND_IN_TRANSIT",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Yard",
                                        "value": "INBOUND_IN_YARD",
                                        "checked": false
                                    },
                                    {
                                        "name": "Unloading",
                                        "value": "INBOUND_UNLOADING",
                                        "checked": false
                                    },
                                    {
                                        "name": "Unloading Completed",
                                        "value": "INBOUND_UNLOADING_COMPLETED",
                                        "checked": false
                                    }
                                ]
                            },
                            "destState": {
                                "type": "text",
                                "name": "Destination State",
                                "data": null
                            },
                            "destCity": {
                                "type": "text",
                                "name": "Destination City",
                                "data": null
                            },
                            "yardArrivalDateTimeDestTZ": {
                                "type": "date",
                                "dummytime": true,
                                "timeformat": "T",
                                "startTime": "[ 00:00:00.000]",
                                "endTime": "[ 23:59:59.999]",
                                "name": "Date Arrived in Yard",
                                "shortName": "Yard Arrival",
                                "data": null
                            },
                            "durationInYard": {
                                "type": "checkboxradio",
                                "name": "Duration in Yard",
                                "data": [
                                    {
                                        "name": "< 24 Hours",
                                        "value": {
                                            "startTimeMins": null,
                                            "endTimeMins": 1439
                                        },
                                        "checked": false
                                    },
                                    {
                                        "name": "24 - 48 Hours",
                                        "value": {
                                            "startTimeMins": 1440,
                                            "endTimeMins": 2880
                                        },
                                        "checked": false
                                    },
                                    {
                                        "name": "> 72 Hours",
                                        "value": {
                                            "startTimeMins": 4321,
                                            "endTimeMins": null
                                        },
                                        "checked": true
                                    }
                                ]
                            },
                            "inboundOrderExecutionHealth": {
                                "type": "checkbox",
                                "name": "Health",
                                "data": [
                                    {
                                        "name": "Unhealthy",
                                        "value": "RED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Needs Attention",
                                        "value": "YELLOW",
                                        "checked": false
                                    },
                                    {
                                        "name": "Healthy",
                                        "value": "GREEN",
                                        "checked": false
                                    }
                                ]
                            }
                        },
                        "columns": [
                            {
                                "title": "Order #",
                                "field": "orderNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 0,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 9)",
                                    "id": 0
                                },
                                "hidden": false
                            },
                            {
                                "title": "Duration in Yard",
                                "field": "durationInYardMS",
                                "tableData": {
                                    "columnOrder": 1,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 11)",
                                    "id": 1
                                },
                                "hidden": true
                            },
                            {
                                "title": "Origin",
                                "field": "origin",
                                "tableData": {
                                    "columnOrder": 2,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 11)",
                                    "id": 1
                                },
                                "hidden": true
                            },
                            {
                                "title": "Date and Time arrived in Yard",
                                "field": "yardArrivalDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 3,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 9)",
                                    "id": 3
                                },
                                "hidden": false
                            },
                            {
                                "title": "Shipment #",
                                "field": "shipmentNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 4,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 6)",
                                    "id": 2
                                },
                                "hidden": false
                            },
                            {
                                "title": "Dest",
                                "field": "customer",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 5,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 6)",
                                    "id": 3
                                },
                                "hidden": false
                            },
                            {
                                "title": "Trailer # ",
                                "field": "trailerNum",
                                "tableData": {
                                    "columnOrder": 6,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 9)",
                                    "id": 6
                                },
                                "hidden": false
                            },
                            {
                                "title": "Dest State",
                                "field": "destinationState",
                                "tableData": {
                                    "columnOrder": 7,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 6)",
                                    "id": 4
                                },
                                "hidden": false
                            },
                            {
                                "title": "Ship Condition",
                                "field": "shippingCondition",
                                "tableData": {
                                    "columnOrder": 8,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 6)",
                                    "id": 5
                                },
                                "hidden": false
                            },
                            {
                                "title": "Dest City",
                                "field": "destinationCity",
                                "tableData": {
                                    "columnOrder": 9,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 6)",
                                    "id": 6
                                },
                                "hidden": false
                            },
                            {
                                "title": "Shipment Status",
                                "field": "inboundOrderExecutionBucketDesc",
                                "tableData": {
                                    "columnOrder": 10,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 9)",
                                    "id": 10
                                },
                                "hidden": false
                            }
                        ]
                    },
                    {
                        "id": 2,
                        "name": "Inbound 2",
                        "default": false,
                        "filters": {
                            "searchString": {
                                "type": "text",
                                "name": "Search String",
                                "data": null
                            },
                            "inboundOrderExecutionBucket": {
                                "type": "checkbox",
                                "name": "Shipment Status",
                                "data": [
                                    {
                                        "name": "In Transit",
                                        "value": "INBOUND_IN_TRANSIT",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Yard",
                                        "value": "INBOUND_IN_YARD",
                                        "checked": true
                                    },
                                    {
                                        "name": "Unloading",
                                        "value": "INBOUND_UNLOADING",
                                        "checked": true
                                    },
                                    {
                                        "name": "Unloading Completed",
                                        "value": "INBOUND_UNLOADING_COMPLETED",
                                        "checked": false
                                    }
                                ]
                            },
                            "destState": {
                                "type": "text",
                                "name": "Destination State",
                                "data": null
                            },
                            "destCity": {
                                "type": "text",
                                "name": "Destination City",
                                "data": null
                            },
                            "yardArrivalDateTimeDestTZ": {
                                "type": "date",
                                "dummytime": true,
                                "timeformat": "T",
                                "startTime": "[ 00:00:00.000]",
                                "endTime": "[ 23:59:59.999]",
                                "name": "Date Arrived in Yard",
                                "shortName": "Yard Arrival",
                                "data": null
                            },
                            "durationInYard": {
                                "type": "checkboxradio",
                                "name": "Duration in Yard",
                                "data": [
                                    {
                                        "name": "< 24 Hours",
                                        "value": {
                                            "startTimeMins": null,
                                            "endTimeMins": 1439
                                        },
                                        "checked": false
                                    },
                                    {
                                        "name": "24 - 48 Hours",
                                        "value": {
                                            "startTimeMins": 1440,
                                            "endTimeMins": 2880
                                        },
                                        "checked": false
                                    },
                                    {
                                        "name": "> 72 Hours",
                                        "value": {
                                            "startTimeMins": 4321,
                                            "endTimeMins": null
                                        },
                                        "checked": false
                                    }
                                ]
                            },
                            "inboundOrderExecutionHealth": {
                                "type": "checkbox",
                                "name": "Health",
                                "data": [
                                    {
                                        "name": "Unhealthy",
                                        "value": "RED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Needs Attention",
                                        "value": "YELLOW",
                                        "checked": false
                                    },
                                    {
                                        "name": "Healthy",
                                        "value": "GREEN",
                                        "checked": false
                                    }
                                ]
                            }
                        },
                        "columns": [
                            {
                                "title": "Shipment #",
                                "field": "shipmentNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 3,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 11)",
                                    "id": 0
                                },
                                "hidden": false
                            },
                            {
                                "title": "Order #",
                                "field": "orderNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 0,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 11)",
                                    "id": 1
                                },
                                "hidden": false
                            },
                            {
                                "title": "Origin",
                                "field": "origin",
                                "tableData": {
                                    "columnOrder": 1,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 11)",
                                    "id": 2
                                },
                                "hidden": false
                            },
                            {
                                "title": "Dest",
                                "field": "customer",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 2,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 11)",
                                    "id": 3
                                },
                                "hidden": false
                            },
                            {
                                "title": "Date and Time arrived in Yard",
                                "field": "yardArrivalDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 4,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 11)",
                                    "id": 4
                                },
                                "hidden": false
                            },
                            {
                                "title": "Duration in Yard",
                                "field": "durationInYardMS",
                                "tableData": {
                                    "columnOrder": 5,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 11)",
                                    "id": 5
                                },
                                "hidden": false
                            },
                            {
                                "title": "Trailer # ",
                                "field": "trailerNum",
                                "tableData": {
                                    "columnOrder": 6,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 11)",
                                    "id": 6
                                },
                                "hidden": false
                            },
                            {
                                "title": "Shipment Status",
                                "field": "inboundOrderExecutionBucketDesc",
                                "tableData": {
                                    "columnOrder": 7,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 11)",
                                    "id": 7
                                },
                                "hidden": false
                            },
                            {
                                "title": "Ship Condition",
                                "field": "shippingCondition",
                                "tableData": {
                                    "columnOrder": 8,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 11)",
                                    "id": 8
                                },
                                "hidden": false
                            },
                            {
                                "title": "Dest City",
                                "field": "destinationCity",
                                "tableData": {
                                    "columnOrder": 9,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 11)",
                                    "id": 9
                                },
                                "hidden": false
                            },
                            {
                                "title": "Dest State",
                                "field": "destinationState",
                                "tableData": {
                                    "columnOrder": 10,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 11)",
                                    "id": 10
                                },
                                "hidden": false
                            }
                        ]
                    },
                    {
                        "id": "2a7f7316-dab3-44cd-9b2f-376391b4480d",
                        "name": "4k-s5",
                        "default": false,
                        "filters": {
                            "searchString": {
                                "type": "text",
                                "name": "Search String",
                                "data": "4k-s5"
                            },
                            "inboundOrderExecutionBucket": {
                                "type": "checkbox",
                                "name": "Shipment Status",
                                "data": [
                                    {
                                        "name": "In Transit",
                                        "value": "INBOUND_IN_TRANSIT",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Yard",
                                        "value": "INBOUND_IN_YARD",
                                        "checked": false
                                    },
                                    {
                                        "name": "Unloading",
                                        "value": "INBOUND_UNLOADING",
                                        "checked": false
                                    },
                                    {
                                        "name": "Unloading Completed",
                                        "value": "INBOUND_UNLOADING_COMPLETED",
                                        "checked": false
                                    }
                                ]
                            },
                            "destState": {
                                "type": "text",
                                "name": "Destination State",
                                "data": null
                            },
                            "destCity": {
                                "type": "text",
                                "name": "Destination City",
                                "data": null
                            },
                            "yardArrivalDateTimeDestTZ": {
                                "type": "date",
                                "dummytime": true,
                                "timeformat": "T",
                                "startTime": "[ 00:00:00.000]",
                                "endTime": "[ 23:59:59.999]",
                                "name": "Date Arrived in Yard",
                                "shortName": "Yard Arrival",
                                "data": null
                            },
                            "durationInYard": {
                                "type": "checkboxradio",
                                "name": "Duration in Yard",
                                "data": [
                                    {
                                        "name": "< 24 Hours",
                                        "value": {
                                            "startTimeMins": null,
                                            "endTimeMins": 1439
                                        },
                                        "checked": false
                                    },
                                    {
                                        "name": "24 - 48 Hours",
                                        "value": {
                                            "startTimeMins": 1440,
                                            "endTimeMins": 2880
                                        },
                                        "checked": false
                                    },
                                    {
                                        "name": "> 72 Hours",
                                        "value": {
                                            "startTimeMins": 4321,
                                            "endTimeMins": null
                                        },
                                        "checked": false
                                    }
                                ]
                            },
                            "inboundOrderExecutionHealth": {
                                "type": "checkbox",
                                "name": "Health",
                                "data": [
                                    {
                                        "name": "Unhealthy",
                                        "value": "RED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Needs Attention",
                                        "value": "YELLOW",
                                        "checked": false
                                    },
                                    {
                                        "name": "Healthy",
                                        "value": "GREEN",
                                        "checked": false
                                    }
                                ]
                            }
                        },
                        "columns": [
                            {
                                "title": "Shipment #",
                                "field": "shipmentNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 0,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 0
                                }
                            },
                            {
                                "title": "Delivery #",
                                "field": "deliveryNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 1,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 1
                                }
                            },
                            {
                                "title": "Order #",
                                "field": "orderNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 2,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 2
                                }
                            },
                            {
                                "title": "Origin ID",
                                "field": "originId",
                                "tableData": {
                                    "columnOrder": 3,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 3
                                }
                            },
                            {
                                "title": "Origin",
                                "field": "origin",
                                "tableData": {
                                    "columnOrder": 4,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 4
                                }
                            },
                            {
                                "title": "Dest",
                                "field": "customer",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 5,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 5
                                }
                            },
                            {
                                "title": "Date and Time arrived in Yard",
                                "field": "yardArrivalDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 6,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 6
                                }
                            },
                            {
                                "title": "Duration in Yard",
                                "field": "durationInYardMS",
                                "tableData": {
                                    "columnOrder": 7,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 7
                                }
                            },
                            {
                                "title": "Trailer # ",
                                "field": "trailerNum",
                                "tableData": {
                                    "columnOrder": 8,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 8
                                }
                            },
                            {
                                "title": "Shipment Status",
                                "field": "inboundOrderExecutionBucketDesc",
                                "tableData": {
                                    "columnOrder": 9,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 9
                                }
                            },
                            {
                                "title": "Ship Condition",
                                "field": "shippingCondition",
                                "tableData": {
                                    "columnOrder": 10,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 10
                                }
                            },
                            {
                                "title": "Dest City",
                                "field": "destinationCity",
                                "tableData": {
                                    "columnOrder": 11,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 11
                                }
                            },
                            {
                                "title": "Dest State",
                                "field": "destinationState",
                                "tableData": {
                                    "columnOrder": 12,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 12
                                }
                            },
                            {
                                "title": "Business",
                                "field": "orderBusinessUnit",
                                "cellStyle": {
                                    "minWidth": 320
                                },
                                "tableData": {
                                    "columnOrder": 13,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 13
                                }
                            },
                            {
                                "title": "Reason for Alert",
                                "field": "reasonCodeString",
                                "tableData": {
                                    "columnOrder": 14,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 16)",
                                    "id": 14
                                }
                            },
                            {
                                "title": "Fourkites ETA",
                                "field": "expectedDeliveryDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "disableClick": true
                            }
                        ]
                    }
                ]
            },
            "DISTOUTBOUND": {
                "tableName": "distoutbound",
                "views": []
            },
            "TRANSPORT": {
                "tableName": "transport",
                "views": [
                    {
                        "id": "5c7dc53c-addd-4463-a8c5-44c89c62aed0",
                        "name": "4ks-5",
                        "default": false,
                        "filters": {
                            "searchString": {
                                "type": "text",
                                "name": "Search String",
                                "data": "4k-s5"
                            },
                            "sites": {
                                "type": "sites",
                                "name": "Origin",
                                "data": null
                            },
                            "destName": {
                                "type": "text",
                                "name": "Customer",
                                "data": null
                            },
                            "orderExecutionBucket": {
                                "type": "checkbox",
                                "name": "Shipment Status",
                                "data": [
                                    {
                                        "name": "Transportation to be planned",
                                        "value": "TRANS_PLAN_UNASSIGNED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation planning",
                                        "value": "TRANS_PLAN_OPEN",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation processing",
                                        "value": "TRANS_PLAN_PROCESSING",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation Carrier committed",
                                        "value": "TRANS_PLAN_TENDER_ACCEPTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Ready for Pickup",
                                        "value": "TRANS_EXEC_READY_PICK_UP",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Transit",
                                        "value": "TRANS_EXEC_IN_TRANSIT",
                                        "checked": false
                                    },
                                    {
                                        "name": "Delivery confirmed",
                                        "value": "TRANS_EXEC_DELIVERY_CONFIRMED",
                                        "checked": false
                                    }
                                ]
                            },
                            "tmsShipmentStatus": {
                                "type": "checkbox",
                                "name": "TM Operational Status",
                                "data": [
                                    {
                                        "name": "Unassigned",
                                        "value": "Unassigned",
                                        "checked": false
                                    },
                                    {
                                        "name": "Open",
                                        "value": "Open",
                                        "checked": false
                                    },
                                    {
                                        "name": "Planned",
                                        "value": "Planned",
                                        "checked": false
                                    },
                                    {
                                        "name": "Tendered",
                                        "value": "Tendered",
                                        "checked": false
                                    },
                                    {
                                        "name": "Tender Rejected",
                                        "value": "Tender Rejected",
                                        "checked": false
                                    },
                                    {
                                        "name": "Tender Accepted",
                                        "value": "Tender Accepted",
                                        "checked": false
                                    },
                                    {
                                        "name": "Confirming",
                                        "value": "Confirming",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Transit",
                                        "value": "In Transit",
                                        "checked": false
                                    },
                                    {
                                        "name": "Completed",
                                        "value": "Completed",
                                        "checked": false
                                    }
                                ]
                            },
                            "appointmentType": {
                                "type": "checkbox",
                                "name": "Appointment Type",
                                "data": [
                                    {
                                        "name": "Manual",
                                        "value": "MANUAL",
                                        "checked": false
                                    },
                                    {
                                        "name": "Automated",
                                        "value": "AUTOMATED",
                                        "checked": false
                                    }
                                ]
                            },
                            "destState": {
                                "type": "text",
                                "name": "Destination State",
                                "data": null
                            },
                            "destCity": {
                                "type": "text",
                                "name": "Destination City",
                                "data": null
                            },
                            "appointmentRequired": {
                                "type": "radio",
                                "name": "Appointment Required",
                                "data": null
                            },
                            "appointmentStatus": {
                                "type": "checkbox",
                                "name": "Appointment Status",
                                "data": [
                                    {
                                        "name": "Notified",
                                        "value": "NOTIFIED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Planned",
                                        "value": "PLANNED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Suggested",
                                        "value": "SUGGESTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Confirmed",
                                        "value": "CONFIRMED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Not yet contacted",
                                        "value": "NOT_YET_CONTACTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Appt not made by Transport",
                                        "value": "NOT_MADE_BY_TRANSPORT",
                                        "checked": false
                                    }
                                ]
                            },
                            "liveLoad": {
                                "type": "radio",
                                "name": "Live Load",
                                "data": null
                            },
                            "equipmentTypeList": {
                                "type": "checkbox",
                                "name": "Equipment Type",
                                "data": [
                                    {
                                        "name": "Full Truck Load",
                                        "value": "FULL_TRUCK_LOAD",
                                        "checked": false
                                    },
                                    {
                                        "name": "Less Than Truck Load",
                                        "value": "LESS_THAN_TRUCK_LOAD",
                                        "checked": false
                                    }
                                ]
                            },
                            "shippingConditionList": {
                                "type": "checkbox",
                                "name": "Ship Condition",
                                "data": [
                                    {
                                        "name": "Truck Load (TL)",
                                        "value": "TL",
                                        "checked": false
                                    },
                                    {
                                        "name": "Intermodal (TF)",
                                        "value": "TF",
                                        "checked": false
                                    },
                                    {
                                        "name": "Open (OP) ",
                                        "value": "OP",
                                        "checked": false
                                    },
                                    {
                                        "name": "Package (PG)",
                                        "value": "PG",
                                        "checked": false
                                    },
                                    {
                                        "name": "Customer Pickup (PU)",
                                        "value": "PU",
                                        "checked": false
                                    }
                                ]
                            },
                            "onHold": {
                                "type": "radio",
                                "name": "Order on Hold",
                                "data": null
                            },
                            "deliveryApptDateTimeDestTZ": {
                                "type": "date",
                                "name": "Delivery Date",
                                "dummytime": true,
                                "timeformat": "T",
                                "startTime": "[ 00:00:00.000]",
                                "endTime": "[ 23:59:59.999]",
                                "shortName": "Del. Date",
                                "data": null
                            },
                            "loadReadyDateTimeOriginTZ": {
                                "type": "date",
                                "name": "Carrier Ready Date",
                                "shortName": "Car.Ready Date",
                                "data": null
                            },
                            "tariffServiceCode": {
                                "type": "text",
                                "name": "Carrier Service Code",
                                "data": null
                            },
                            "orderExecutionHealth": {
                                "type": "checkbox",
                                "name": "Health",
                                "data": [
                                    {
                                        "name": "Unhealthy",
                                        "value": "RED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Needs Attention",
                                        "value": "YELLOW",
                                        "checked": false
                                    },
                                    {
                                        "name": "Healthy",
                                        "value": "GREEN",
                                        "checked": false
                                    }
                                ]
                            }
                        },
                        "columns": [
                            {
                                "title": "Load #",
                                "field": "shipmentNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 0,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 0
                                }
                            },
                            {
                                "title": "Delivery #",
                                "field": "deliveryNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 1,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 1
                                }
                            },
                            {
                                "title": "Order #",
                                "field": "orderNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 2,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 2
                                }
                            },
                            {
                                "title": "Carrier",
                                "field": "tariffServiceCode",
                                "tableData": {
                                    "columnOrder": 3,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 3
                                }
                            },
                            {
                                "title": "Shipment Status",
                                "field": "orderExecutionBucketDesc",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 4,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 4
                                }
                            },
                            {
                                "title": "TM Status",
                                "field": "tmsShipmentStatus",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 5,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 5
                                }
                            },
                            {
                                "title": "Carrier Ready Date & Time",
                                "field": "loadReadyDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 6,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 6
                                }
                            },
                            {
                                "title": "Origin ID",
                                "field": "originId",
                                "tableData": {
                                    "columnOrder": 7,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 7
                                }
                            },
                            {
                                "title": "Origin",
                                "field": "origin",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 8,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 8
                                }
                            },
                            {
                                "title": "Customer",
                                "field": "customer",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 9,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 9
                                }
                            },
                            {
                                "title": "Live Load",
                                "field": "liveLoadInd",
                                "tableData": {
                                    "columnOrder": 10,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 10
                                }
                            },
                            {
                                "title": "Order On Hold",
                                "field": "orderOnHoldInd",
                                "tableData": {
                                    "columnOrder": 11,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 11
                                }
                            },
                            {
                                "title": "Cust Confirmed Delivery Date & Time",
                                "field": "deliverApptDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 12,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 12
                                }
                            },
                            {
                                "title": "Dest City",
                                "field": "destinationCity",
                                "tableData": {
                                    "columnOrder": 13,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 13
                                }
                            },
                            {
                                "title": "Dest St",
                                "field": "destinationState",
                                "tableData": {
                                    "columnOrder": 14,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 14
                                }
                            },
                            {
                                "title": "Ship Condition",
                                "field": "shippingCondition",
                                "tableData": {
                                    "columnOrder": 15,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 15
                                }
                            },
                            {
                                "title": "Appt Type",
                                "field": "appointmentType",
                                "tableData": {
                                    "columnOrder": 16,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 16
                                }
                            },
                            {
                                "title": "Appt Req'd",
                                "field": "appointmentRequired",
                                "tableData": {
                                    "columnOrder": 17,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 17
                                }
                            },
                            {
                                "title": "Appt Status",
                                "field": "appointmentStatusDesc",
                                "tableData": {
                                    "columnOrder": 18,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 18
                                }
                            },
                            {
                                "title": "Suspended Status",
                                "field": "shipmentSuspendedStatus",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 19,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 19
                                }
                            },
                            {
                                "title": "Short Lead",
                                "field": "tmsRushIndicator",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 20,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 20
                                }
                            },
                            {
                                "title": "Reason for Alert",
                                "field": "reasonCodeString",
                                "cellStyle": {
                                    "minWidth": 320
                                },
                                "tableData": {
                                    "columnOrder": 21,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 21
                                }
                            },
                            {
                                "title": "Fourkites ETA",
                                "field": "expectedDeliveryDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "disableClick": true
                            }
                        ]
                    },
                    {
                        "id": "4ec72e1b-0283-456d-9aeb-3bf2fdbb0a61",
                        "name": "4ks-5-p2",
                        "default": false,
                        "filters": {
                            "searchStringList": {
                                "type": "text",
                                "name": "Search",
                                "stringToArray": true,
                                "data": null
                            },
                            "sites": {
                                "type": "sites",
                                "name": "Origin",
                                "data": null
                            },
                            "destName": {
                                "type": "text",
                                "name": "Customer",
                                "data": null
                            },
                            "orderExecutionBucket": {
                                "type": "checkbox",
                                "name": "Shipment Status",
                                "data": [
                                    {
                                        "name": "Transportation to be planned",
                                        "value": "TRANS_PLAN_UNASSIGNED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation planning",
                                        "value": "TRANS_PLAN_OPEN",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation processing",
                                        "value": "TRANS_PLAN_PROCESSING",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation Carrier committed",
                                        "value": "TRANS_PLAN_TENDER_ACCEPTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Ready for Pickup",
                                        "value": "TRANS_EXEC_READY_PICK_UP",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Transit",
                                        "value": "TRANS_EXEC_IN_TRANSIT",
                                        "checked": false
                                    },
                                    {
                                        "name": "Delivery confirmed",
                                        "value": "TRANS_EXEC_DELIVERY_CONFIRMED",
                                        "checked": false
                                    }
                                ]
                            },
                            "tmsShipmentStatus": {
                                "type": "checkbox",
                                "name": "TM Operational Status",
                                "data": [
                                    {
                                        "name": "Unassigned",
                                        "value": "Unassigned",
                                        "checked": false
                                    },
                                    {
                                        "name": "Open",
                                        "value": "Open",
                                        "checked": false
                                    },
                                    {
                                        "name": "Planned",
                                        "value": "Planned",
                                        "checked": false
                                    },
                                    {
                                        "name": "Tendered",
                                        "value": "Tendered",
                                        "checked": false
                                    },
                                    {
                                        "name": "Tender Rejected",
                                        "value": "Tender Rejected",
                                        "checked": false
                                    },
                                    {
                                        "name": "Tender Accepted",
                                        "value": "Tender Accepted",
                                        "checked": false
                                    },
                                    {
                                        "name": "Confirming",
                                        "value": "Confirming",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Transit",
                                        "value": "In Transit",
                                        "checked": false
                                    },
                                    {
                                        "name": "Completed",
                                        "value": "Completed",
                                        "checked": false
                                    }
                                ]
                            },
                            "appointmentType": {
                                "type": "checkbox",
                                "name": "Appointment Type",
                                "data": [
                                    {
                                        "name": "Manual",
                                        "value": "MANUAL",
                                        "checked": false
                                    },
                                    {
                                        "name": "Automated",
                                        "value": "AUTOMATED",
                                        "checked": false
                                    }
                                ]
                            },
                            "destState": {
                                "type": "text",
                                "name": "Destination State",
                                "data": null
                            },
                            "destCity": {
                                "type": "text",
                                "name": "Destination City",
                                "data": null
                            },
                            "appointmentRequired": {
                                "type": "radio",
                                "name": "Appointment Required",
                                "data": null
                            },
                            "appointmentStatus": {
                                "type": "checkbox",
                                "name": "Appointment Status",
                                "data": [
                                    {
                                        "name": "Notified",
                                        "value": "NOTIFIED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Planned",
                                        "value": "PLANNED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Suggested",
                                        "value": "SUGGESTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Confirmed",
                                        "value": "CONFIRMED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Not yet contacted",
                                        "value": "NOT_YET_CONTACTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Appt not made by Transport",
                                        "value": "NOT_MADE_BY_TRANSPORT",
                                        "checked": false
                                    }
                                ]
                            },
                            "liveLoad": {
                                "type": "radio",
                                "name": "Live Load",
                                "data": null
                            },
                            "equipmentTypeList": {
                                "type": "checkbox",
                                "name": "Equipment Type",
                                "data": [
                                    {
                                        "name": "Full Truck Load",
                                        "value": "FULL_TRUCK_LOAD",
                                        "checked": false
                                    },
                                    {
                                        "name": "Less Than Truck Load",
                                        "value": "LESS_THAN_TRUCK_LOAD",
                                        "checked": false
                                    }
                                ]
                            },
                            "shippingConditionList": {
                                "type": "checkbox",
                                "name": "Ship Condition",
                                "data": [
                                    {
                                        "name": "Truck Load (TL)",
                                        "value": "TL",
                                        "checked": false
                                    },
                                    {
                                        "name": "Intermodal (TF)",
                                        "value": "TF",
                                        "checked": false
                                    },
                                    {
                                        "name": "Open (OP) ",
                                        "value": "OP",
                                        "checked": false
                                    },
                                    {
                                        "name": "Package (PG)",
                                        "value": "PG",
                                        "checked": false
                                    },
                                    {
                                        "name": "Customer Pickup (PU)",
                                        "value": "PU",
                                        "checked": false
                                    }
                                ]
                            },
                            "onHold": {
                                "type": "radio",
                                "name": "Order on Hold",
                                "data": null
                            },
                            "deliveryApptDateTimeDestTZ": {
                                "type": "date",
                                "name": "Delivery Date",
                                "dummytime": true,
                                "timeformat": "T",
                                "startTime": "[ 00:00:00.000]",
                                "endTime": "[ 23:59:59.999]",
                                "shortName": "Del. Date",
                                "data": null
                            },
                            "loadReadyDateTimeOriginTZ": {
                                "type": "date",
                                "name": "Carrier Ready Date",
                                "shortName": "Car.Ready Date",
                                "data": null
                            },
                            "tariffServiceCodeList": {
                                "type": "text",
                                "name": "Carrier Service Code",
                                "stringToArray": true,
                                "data": null
                            },
                            "orderExecutionHealth": {
                                "type": "checkbox",
                                "name": "Health",
                                "data": [
                                    {
                                        "name": "Unhealthy",
                                        "value": "RED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Needs Attention",
                                        "value": "YELLOW",
                                        "checked": true
                                    },
                                    {
                                        "name": "Healthy",
                                        "value": "GREEN",
                                        "checked": false
                                    }
                                ]
                            },
                            "searchString": {
                                "type": "text",
                                "name": "Search String",
                                "data": "4k-s5"
                            },
                            "shippingCondition": {
                                "type": "text",
                                "name": "Shipping Condition",
                                "data": null
                            },
                            "requestedDeliveryDate": {
                                "type": "date",
                                "hidetime": true,
                                "name": "Requested Delivery Date",
                                "shortName": "Req. Delivery Date",
                                "data": null
                            },
                            "deliveryBlockCode": {
                                "type": "textcheck",
                                "name": "Delivery/Credit Block",
                                "placeholder": "Enter delivery block code",
                                "shortName": "Delivery Block Code",
                                "addon": "creditOnHold",
                                "data": null
                            },
                            "creditOnHold": {
                                "hidden": true,
                                "type": "text",
                                "name": "Show Only Credit Hold Orders",
                                "data": null
                            },
                            "confirmedEquivCubes": {
                                "type": "checkbox",
                                "name": "Confirmed Cube",
                                "shortName": "Confirmed Cube",
                                "data": [
                                    {
                                        "name": "< 1000",
                                        "value": {
                                            "lte": 999.9999999
                                        },
                                        "checked": false
                                    },
                                    {
                                        "name": "1000 - 2000",
                                        "value": {
                                            "gte": 1000,
                                            "lte": 1999.9999999
                                        },
                                        "checked": false
                                    },
                                    {
                                        "name": "2000 - 3000",
                                        "value": {
                                            "gte": 2000,
                                            "lte": 2999.9999999
                                        },
                                        "checked": false
                                    },
                                    {
                                        "name": "> 3000",
                                        "value": {
                                            "gte": 3000
                                        },
                                        "checked": false
                                    }
                                ]
                            },
                            "matAvailDate": {
                                "type": "date",
                                "name": "Material Availability Date",
                                "shortName": "Mat.Avail Date",
                                "data": null
                            },
                            "orderStatusBucket": {
                                "type": "ordercheckbox",
                                "name": "Order Status",
                                "data": [
                                    {
                                        "name": "100% Confirmed Cube",
                                        "value": "SO_COMPLETELY_CONFIRMED_CUBE",
                                        "checked": false,
                                        "parent": "Order Block Free"
                                    },
                                    {
                                        "name": "Less than 100% Confirmed Cube",
                                        "value": "SO_NOT_COMPLETELY_CONFIRMED_CUBE",
                                        "checked": false,
                                        "parent": "Order Block Free"
                                    },
                                    {
                                        "name": "Back Orders Block Free",
                                        "value": "SO_BACK_ORDER_BLOCK_FREE",
                                        "checked": false,
                                        "parent": "Order Block Free"
                                    },
                                    {
                                        "name": "Visible in TMS",
                                        "value": "SO_BLOCKED_TMS_PLANNED_VISIBLE",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Not visible in TMS",
                                        "value": "SO_BLOCKED_TMS_PLANNED_NOT_VISIBLE",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Pickup not scheduled",
                                        "value": "SO_BLOCKED_NON_TMS_PLANNED_PICKUP_NOT_SCHEDULED",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Pickup/Package Multi Block",
                                        "value": "SO_BLOCKED_NON_TMS_PLANNED_PICKUP_MULTI_BLOCK",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Immediate Action",
                                        "value": "SO_BLOCKED_NON_TMS_PLANNED_IMMEDIATE_ACTION",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Back Orders Blocked",
                                        "value": "SO_BACK_ORDER_BLOCKED",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    }
                                ]
                            },
                            "orderTypes": {
                                "type": "checkbox",
                                "name": "Order Type",
                                "data": [
                                    {
                                        "name": "All Customer Orders",
                                        "value": "CUST",
                                        "checked": false
                                    },
                                    {
                                        "name": "STOs",
                                        "value": "STO",
                                        "checked": false
                                    }
                                ]
                            },
                            "documentTypes": {
                                "type": "checkbox",
                                "name": "Document type",
                                "data": [
                                    {
                                        "name": "Standard Orders (ZSTD)",
                                        "value": "ZSTD",
                                        "checked": false
                                    },
                                    {
                                        "name": "Multi Plant Orders (ZMPF)",
                                        "value": "ZMPF",
                                        "checked": false
                                    },
                                    {
                                        "name": "VMI Orders (ZVMI)",
                                        "value": "ZVMI",
                                        "checked": false
                                    },
                                    {
                                        "name": "Merged Orders (ZMER)",
                                        "value": "ZMER",
                                        "checked": false
                                    }
                                ]
                            },
                            "orderHealth": {
                                "type": "checkbox",
                                "name": "Health",
                                "data": [
                                    {
                                        "name": "Unhealthy",
                                        "value": "RED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Needs Attention",
                                        "value": "YELLOW",
                                        "checked": false
                                    },
                                    {
                                        "name": "Healthy",
                                        "value": "GREEN",
                                        "checked": false
                                    }
                                ]
                            },
                            "tariffServiceCode": {
                                "type": "text",
                                "name": "Carrier Service Code",
                                "data": null
                            }
                        },
                        "columns": [
                            {
                                "title": "Load #",
                                "field": "shipmentNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 0,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 0
                                }
                            },
                            {
                                "title": "Delivery #",
                                "field": "deliveryNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 1,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 1
                                }
                            },
                            {
                                "title": "Order #",
                                "field": "orderNum",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 2,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 2
                                }
                            },
                            {
                                "title": "Carrier",
                                "field": "tariffServiceCode",
                                "tableData": {
                                    "columnOrder": 3,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 3
                                }
                            },
                            {
                                "title": "Shipment Status",
                                "field": "orderExecutionBucketDesc",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 4,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 4
                                }
                            },
                            {
                                "title": "TM Status",
                                "field": "tmsShipmentStatus",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 5,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 5
                                }
                            },
                            {
                                "title": "Carrier Ready Date & Time",
                                "field": "loadReadyDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 6,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 6
                                }
                            },
                            {
                                "title": "Origin ID",
                                "field": "originId",
                                "tableData": {
                                    "columnOrder": 7,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 7
                                }
                            },
                            {
                                "title": "Origin",
                                "field": "origin",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 8,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 8
                                }
                            },
                            {
                                "title": "Customer",
                                "field": "customer",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 9,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 9
                                }
                            },
                            {
                                "title": "Live Load",
                                "field": "liveLoadInd",
                                "tableData": {
                                    "columnOrder": 10,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 10
                                }
                            },
                            {
                                "title": "Order On Hold",
                                "field": "orderOnHoldInd",
                                "tableData": {
                                    "columnOrder": 11,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 11
                                }
                            },
                            {
                                "title": "Cust Confirmed Delivery Date & Time",
                                "field": "deliverApptDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 12,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 12
                                }
                            },
                            {
                                "title": "Dest City",
                                "field": "destinationCity",
                                "tableData": {
                                    "columnOrder": 13,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 13
                                }
                            },
                            {
                                "title": "Dest St",
                                "field": "destinationState",
                                "tableData": {
                                    "columnOrder": 14,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 14
                                }
                            },
                            {
                                "title": "Ship Condition",
                                "field": "shippingCondition",
                                "tableData": {
                                    "columnOrder": 15,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 15
                                }
                            },
                            {
                                "title": "Appt Type",
                                "field": "appointmentType",
                                "tableData": {
                                    "columnOrder": 16,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 16
                                }
                            },
                            {
                                "title": "Appt Req'd",
                                "field": "appointmentRequired",
                                "tableData": {
                                    "columnOrder": 17,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 17
                                }
                            },
                            {
                                "title": "Appt Status",
                                "field": "appointmentStatusDesc",
                                "tableData": {
                                    "columnOrder": 18,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 18
                                }
                            },
                            {
                                "title": "Suspended Status",
                                "field": "shipmentSuspendedStatus",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 19,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 19
                                }
                            },
                            {
                                "title": "Short Lead",
                                "field": "tmsRushIndicator",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "tableData": {
                                    "columnOrder": 20,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 20
                                }
                            },
                            {
                                "title": "Reason for Alert",
                                "field": "reasonCodeString",
                                "cellStyle": {
                                    "minWidth": 320
                                },
                                "tableData": {
                                    "columnOrder": 21,
                                    "groupSort": "asc",
                                    "width": "calc((100% - (0px)) / 23)",
                                    "id": 21
                                }
                            },
                            {
                                "title": "Fourkites ETA",
                                "field": "expectedDeliveryDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "disableClick": true
                            }
                        ]
                    },
                    {
                        "id": "b501228d-e2e4-4c48-8608-d3aba29e3843",
                        "name": "4ks-5-TMS",
                        "default": false,
                        "filters": {
                            "searchStringList": {
                                "type": "text",
                                "name": "Search",
                                "stringToArray": true,
                                "data": null
                            },
                            "sites": {
                                "type": "sites",
                                "name": "Origin",
                                "data": null
                            },
                            "destName": {
                                "type": "text",
                                "name": "Customer",
                                "data": null
                            },
                            "orderExecutionBucket": {
                                "type": "checkbox",
                                "name": "Shipment Status",
                                "data": [
                                    {
                                        "name": "Transportation to be planned",
                                        "value": "TRANS_PLAN_UNASSIGNED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation planning",
                                        "value": "TRANS_PLAN_OPEN",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation processing",
                                        "value": "TRANS_PLAN_PROCESSING",
                                        "checked": false
                                    },
                                    {
                                        "name": "Transportation Carrier committed",
                                        "value": "TRANS_PLAN_TENDER_ACCEPTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Ready for Pickup",
                                        "value": "TRANS_EXEC_READY_PICK_UP",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Transit",
                                        "value": "TRANS_EXEC_IN_TRANSIT",
                                        "checked": false
                                    },
                                    {
                                        "name": "Delivery confirmed",
                                        "value": "TRANS_EXEC_DELIVERY_CONFIRMED",
                                        "checked": false
                                    }
                                ]
                            },
                            "tmsShipmentStatus": {
                                "type": "checkbox",
                                "name": "TM Operational Status",
                                "data": [
                                    {
                                        "name": "Unassigned",
                                        "value": "Unassigned",
                                        "checked": true
                                    },
                                    {
                                        "name": "Open",
                                        "value": "Open",
                                        "checked": false
                                    },
                                    {
                                        "name": "Planned",
                                        "value": "Planned",
                                        "checked": false
                                    },
                                    {
                                        "name": "Tendered",
                                        "value": "Tendered",
                                        "checked": false
                                    },
                                    {
                                        "name": "Tender Rejected",
                                        "value": "Tender Rejected",
                                        "checked": false
                                    },
                                    {
                                        "name": "Tender Accepted",
                                        "value": "Tender Accepted",
                                        "checked": false
                                    },
                                    {
                                        "name": "Confirming",
                                        "value": "Confirming",
                                        "checked": false
                                    },
                                    {
                                        "name": "In Transit",
                                        "value": "In Transit",
                                        "checked": false
                                    },
                                    {
                                        "name": "Completed",
                                        "value": "Completed",
                                        "checked": false
                                    }
                                ]
                            },
                            "appointmentType": {
                                "type": "checkbox",
                                "name": "Appointment Type",
                                "data": [
                                    {
                                        "name": "Manual",
                                        "value": "MANUAL",
                                        "checked": false
                                    },
                                    {
                                        "name": "Automated",
                                        "value": "AUTOMATED",
                                        "checked": false
                                    }
                                ]
                            },
                            "destState": {
                                "type": "text",
                                "name": "Destination State",
                                "data": null
                            },
                            "destCity": {
                                "type": "text",
                                "name": "Destination City",
                                "data": null
                            },
                            "appointmentRequired": {
                                "type": "radio",
                                "name": "Appointment Required",
                                "data": null
                            },
                            "appointmentStatus": {
                                "type": "checkbox",
                                "name": "Appointment Status",
                                "data": [
                                    {
                                        "name": "Notified",
                                        "value": "NOTIFIED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Planned",
                                        "value": "PLANNED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Suggested",
                                        "value": "SUGGESTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Confirmed",
                                        "value": "CONFIRMED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Not yet contacted",
                                        "value": "NOT_YET_CONTACTED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Appt not made by Transport",
                                        "value": "NOT_MADE_BY_TRANSPORT",
                                        "checked": false
                                    }
                                ]
                            },
                            "liveLoad": {
                                "type": "radio",
                                "name": "Live Load",
                                "data": null
                            },
                            "equipmentTypeList": {
                                "type": "checkbox",
                                "name": "Equipment Type",
                                "data": [
                                    {
                                        "name": "Full Truck Load",
                                        "value": "FULL_TRUCK_LOAD",
                                        "checked": false
                                    },
                                    {
                                        "name": "Less Than Truck Load",
                                        "value": "LESS_THAN_TRUCK_LOAD",
                                        "checked": false
                                    }
                                ]
                            },
                            "shippingConditionList": {
                                "type": "checkbox",
                                "name": "Ship Condition",
                                "data": [
                                    {
                                        "name": "Truck Load (TL)",
                                        "value": "TL",
                                        "checked": false
                                    },
                                    {
                                        "name": "Intermodal (TF)",
                                        "value": "TF",
                                        "checked": false
                                    },
                                    {
                                        "name": "Open (OP) ",
                                        "value": "OP",
                                        "checked": false
                                    },
                                    {
                                        "name": "Package (PG)",
                                        "value": "PG",
                                        "checked": false
                                    },
                                    {
                                        "name": "Customer Pickup (PU)",
                                        "value": "PU",
                                        "checked": false
                                    }
                                ]
                            },
                            "onHold": {
                                "type": "radio",
                                "name": "Order on Hold",
                                "data": null
                            },
                            "deliveryApptDateTimeDestTZ": {
                                "type": "date",
                                "name": "Delivery Date",
                                "dummytime": true,
                                "timeformat": "T",
                                "startTime": "[ 00:00:00.000]",
                                "endTime": "[ 23:59:59.999]",
                                "shortName": "Del. Date",
                                "data": null
                            },
                            "loadReadyDateTimeOriginTZ": {
                                "type": "date",
                                "name": "Carrier Ready Date",
                                "shortName": "Car.Ready Date",
                                "data": null
                            },
                            "tariffServiceCodeList": {
                                "type": "text",
                                "name": "Carrier Service Code",
                                "stringToArray": true,
                                "data": null
                            },
                            "orderExecutionHealth": {
                                "type": "checkbox",
                                "name": "Health",
                                "data": [
                                    {
                                        "name": "Unhealthy",
                                        "value": "RED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Needs Attention",
                                        "value": "YELLOW",
                                        "checked": false
                                    },
                                    {
                                        "name": "Healthy",
                                        "value": "GREEN",
                                        "checked": false
                                    }
                                ]
                            },
                            "searchString": {
                                "type": "text",
                                "name": "Search String",
                                "data": "4k-s5"
                            },
                            "shippingCondition": {
                                "type": "text",
                                "name": "Shipping Condition",
                                "data": null
                            },
                            "requestedDeliveryDate": {
                                "type": "date",
                                "hidetime": true,
                                "name": "Requested Delivery Date",
                                "shortName": "Req. Delivery Date",
                                "data": null
                            },
                            "deliveryBlockCode": {
                                "type": "textcheck",
                                "name": "Delivery/Credit Block",
                                "placeholder": "Enter delivery block code",
                                "shortName": "Delivery Block Code",
                                "addon": "creditOnHold",
                                "data": null
                            },
                            "creditOnHold": {
                                "hidden": true,
                                "type": "text",
                                "name": "Show Only Credit Hold Orders",
                                "data": null
                            },
                            "confirmedEquivCubes": {
                                "type": "checkbox",
                                "name": "Confirmed Cube",
                                "shortName": "Confirmed Cube",
                                "data": [
                                    {
                                        "name": "< 1000",
                                        "value": {
                                            "lte": 999.9999999
                                        },
                                        "checked": false
                                    },
                                    {
                                        "name": "1000 - 2000",
                                        "value": {
                                            "gte": 1000,
                                            "lte": 1999.9999999
                                        },
                                        "checked": false
                                    },
                                    {
                                        "name": "2000 - 3000",
                                        "value": {
                                            "gte": 2000,
                                            "lte": 2999.9999999
                                        },
                                        "checked": false
                                    },
                                    {
                                        "name": "> 3000",
                                        "value": {
                                            "gte": 3000
                                        },
                                        "checked": false
                                    }
                                ]
                            },
                            "matAvailDate": {
                                "type": "date",
                                "name": "Material Availability Date",
                                "shortName": "Mat.Avail Date",
                                "data": null
                            },
                            "orderStatusBucket": {
                                "type": "ordercheckbox",
                                "name": "Order Status",
                                "data": [
                                    {
                                        "name": "100% Confirmed Cube",
                                        "value": "SO_COMPLETELY_CONFIRMED_CUBE",
                                        "checked": false,
                                        "parent": "Order Block Free"
                                    },
                                    {
                                        "name": "Less than 100% Confirmed Cube",
                                        "value": "SO_NOT_COMPLETELY_CONFIRMED_CUBE",
                                        "checked": false,
                                        "parent": "Order Block Free"
                                    },
                                    {
                                        "name": "Back Orders Block Free",
                                        "value": "SO_BACK_ORDER_BLOCK_FREE",
                                        "checked": false,
                                        "parent": "Order Block Free"
                                    },
                                    {
                                        "name": "Visible in TMS",
                                        "value": "SO_BLOCKED_TMS_PLANNED_VISIBLE",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Not visible in TMS",
                                        "value": "SO_BLOCKED_TMS_PLANNED_NOT_VISIBLE",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Pickup not scheduled",
                                        "value": "SO_BLOCKED_NON_TMS_PLANNED_PICKUP_NOT_SCHEDULED",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Pickup/Package Multi Block",
                                        "value": "SO_BLOCKED_NON_TMS_PLANNED_PICKUP_MULTI_BLOCK",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Immediate Action",
                                        "value": "SO_BLOCKED_NON_TMS_PLANNED_IMMEDIATE_ACTION",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    },
                                    {
                                        "name": "Back Orders Blocked",
                                        "value": "SO_BACK_ORDER_BLOCKED",
                                        "checked": false,
                                        "parent": "Orders Blocked"
                                    }
                                ]
                            },
                            "orderTypes": {
                                "type": "checkbox",
                                "name": "Order Type",
                                "data": [
                                    {
                                        "name": "All Customer Orders",
                                        "value": "CUST",
                                        "checked": false
                                    },
                                    {
                                        "name": "STOs",
                                        "value": "STO",
                                        "checked": false
                                    }
                                ]
                            },
                            "documentTypes": {
                                "type": "checkbox",
                                "name": "Document type",
                                "data": [
                                    {
                                        "name": "Standard Orders (ZSTD)",
                                        "value": "ZSTD",
                                        "checked": false
                                    },
                                    {
                                        "name": "Multi Plant Orders (ZMPF)",
                                        "value": "ZMPF",
                                        "checked": false
                                    },
                                    {
                                        "name": "VMI Orders (ZVMI)",
                                        "value": "ZVMI",
                                        "checked": false
                                    },
                                    {
                                        "name": "Merged Orders (ZMER)",
                                        "value": "ZMER",
                                        "checked": false
                                    }
                                ]
                            },
                            "orderHealth": {
                                "type": "checkbox",
                                "name": "Health",
                                "data": [
                                    {
                                        "name": "Unhealthy",
                                        "value": "RED",
                                        "checked": false
                                    },
                                    {
                                        "name": "Needs Attention",
                                        "value": "YELLOW",
                                        "checked": false
                                    },
                                    {
                                        "name": "Healthy",
                                        "value": "GREEN",
                                        "checked": false
                                    }
                                ]
                            },
                            "tariffServiceCode": {
                                "type": "text",
                                "name": "Carrier Service Code",
                                "data": null
                            }
                        },
                        "columns": [
                            {
                                "title": "Load #",
                                "field": "shipmentNum",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Delivery #",
                                "field": "deliveryNum",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Order #",
                                "field": "orderNum",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Carrier",
                                "field": "tariffServiceCode"
                            },
                            {
                                "title": "Shipment Status",
                                "field": "orderExecutionBucketDesc",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "TM Status",
                                "field": "tmsShipmentStatus",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Carrier Ready Date & Time",
                                "field": "loadReadyDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Origin ID",
                                "field": "originId"
                            },
                            {
                                "title": "Origin",
                                "field": "origin",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Customer",
                                "field": "customer",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Live Load",
                                "field": "liveLoadInd"
                            },
                            {
                                "title": "Order On Hold",
                                "field": "orderOnHoldInd"
                            },
                            {
                                "title": "Cust Confirmed Delivery Date & Time",
                                "field": "deliverApptDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Dest City",
                                "field": "destinationCity"
                            },
                            {
                                "title": "Dest St",
                                "field": "destinationState"
                            },
                            {
                                "title": "Ship Condition",
                                "field": "shippingCondition"
                            },
                            {
                                "title": "Appt Type",
                                "field": "appointmentType"
                            },
                            {
                                "title": "Appt Req'd",
                                "field": "appointmentRequired"
                            },
                            {
                                "title": "Appt Status",
                                "field": "appointmentStatusDesc"
                            },
                            {
                                "title": "Suspended Status",
                                "field": "shipmentSuspendedStatus",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Short Lead",
                                "field": "tmsRushIndicator",
                                "cellStyle": {
                                    "minWidth": 160
                                }
                            },
                            {
                                "title": "Reason for Alert",
                                "field": "reasonCodeString",
                                "cellStyle": {
                                    "minWidth": 320
                                }
                            },
                            {
                                "title": "Fourkites ETA",
                                "field": "expectedDeliveryDateTime",
                                "cellStyle": {
                                    "minWidth": 160
                                },
                                "disableClick": true
                            }
                        ]
                    }
                ]
            }
        },
        "loading": false,
        "firstLoad": true
    }
}

export const mockLocationsProps = {
    "open": true,
    businessUnits: ["CONSUMER"],
    handleClick: () => { return true }
}

export const transportSearch = {
    "onPage": 1,
    "pageSize": 10,
    "totalPages": 18,
    "totalRecords": 180,
    "orders": [
        {
            "orderNum": "",
            "tmsShipmentStatus": undefined,
            "orderExecutionBucketDesc": "Transportation to be Planned",
            "orderDestination": {
                "name": "TARGET #579",
                "city": null,
                "state": "NY"
            },
            "orderItems": [{ "materialNum": "104780402" }],
            "originSiteNum": "2336",
            "orderOrigin": {
                "id": "2336",
                "name": "Eastern Regional DC",
                "city": "PITTSTON",
                "state": "PA",
                "country": "USA",
                "street": "325 CENTERPOINT BOULEVARD",
                "geoLocation": {
                    "longitude": -75.7501646,
                    "latitude": 41.2942816
                },
                "timezone": "US/Eastern",
                "postalCode": "18640-6139"
            },
            "appointmentRequired": "N",
            "orderOnHoldInd": null,
            "tmsOrderCreateDateTime": "2020-10-20T17:02:08.000Z",
            "customerNotifiedDateTime": [],
            "deliveryApptConfirmedDateTimeList": [],
            "orderExecutionBucket": "TRANS_PLAN_UNASSIGNED",
            "orderExecutionHealth": "RED",
            "originTimeZone": "US/Eastern",
            "durationInYardMS": 0,
            "orderHealthAndExecutionHealth": "RED"
        },
        {
            "orderNum": "2006227964",
            "tmsShipmentStatus": "Unassigned",
            "orderExecutionBucketDesc": "Transportation to be Planned",
            "orderDestination": {
                "name": "TARGET #589",
                "city": "CHAMBERSBURG",
                "state": "PA"
            },
            "originSiteNum": "2336",
            "orderOrigin": {
                "id": "2336",
                "name": "Eastern Regional DC",
                "city": "PITTSTON",
                "state": "PA",
                "country": "USA",
                "street": "325 CENTERPOINT BOULEVARD",
                "geoLocation": {
                    "longitude": -75.7501646,
                    "latitude": 41.2942816
                },
                "timezone": "US/Eastern",
                "postalCode": "18640-6139"
            },
            "appointmentRequired": "N",
            "orderOnHoldInd": "Y",
            "tmsOrderCreateDateTime": "2020-10-20T16:32:05.000Z",
            "customerNotifiedDateTime": [],
            "deliveryApptConfirmedDateTimeList": [],
            "orderExecutionBucket": "TRANS_PLAN_UNASSIGNED",
            "orderExecutionHealth": "RED",
            "originTimeZone": "US/Eastern",
            "durationInYardMS": 0,
            "orderHealthAndExecutionHealth": "RED"
        },
        {
            "orderNum": "2006227878",
            "tmsShipmentStatus": "Unassigned",
            "orderExecutionBucketDesc": "Transportation to be Planned",
            "orderDestination": {
                "name": "TARGET #589",
                "city": "CHAMBERSBURG",
                "state": "PA"
            },
            "originSiteNum": "2336",
            "orderOrigin": {
                "id": "2336",
                "name": "Eastern Regional DC",
                "city": "PITTSTON",
                "state": "PA",
                "country": "USA",
                "street": "325 CENTERPOINT BOULEVARD",
                "geoLocation": {
                    "longitude": -75.7501646,
                    "latitude": 41.2942816
                },
                "timezone": "US/Eastern",
                "postalCode": "18640-6139"
            },
            "appointmentRequired": "N",
            "orderOnHoldInd": "Y",
            "tmsOrderCreateDateTime": "2020-10-20T16:57:10.000Z",
            "customerNotifiedDateTime": [],
            "deliveryApptConfirmedDateTimeList": [],
            "orderExecutionBucket": "TRANS_PLAN_UNASSIGNED",
            "orderExecutionHealth": "RED",
            "originTimeZone": "US/Eastern",
            "durationInYardMS": 0,
            "orderHealthAndExecutionHealth": "RED"
        },
        {
            "orderNum": "2006227921",
            "tmsShipmentStatus": "Unassigned",
            "orderExecutionBucketDesc": "Transportation to be Planned",
            "orderDestination": {
                "name": "AMAZON.COM EWR4",
                "city": "ROBBINSVILLE",
                "state": "NJ"
            },
            "originSiteNum": "2336",
            "orderOrigin": {
                "id": "2336",
                "name": "Eastern Regional DC",
                "city": "PITTSTON",
                "state": "PA",
                "country": "USA",
                "street": "325 CENTERPOINT BOULEVARD",
                "geoLocation": {
                    "longitude": -75.7501646,
                    "latitude": 41.2942816
                },
                "timezone": "US/Eastern",
                "postalCode": "18640-6139"
            },
            "appointmentRequired": "N",
            "orderOnHoldInd": "N",
            "tmsOrderCreateDateTime": "2020-10-14T13:55:11.000Z",
            "customerNotifiedDateTime": [],
            "deliveryApptConfirmedDateTimeList": [],
            "orderExecutionBucket": "TRANS_PLAN_UNASSIGNED",
            "orderExecutionHealth": "RED",
            "originTimeZone": "US/Eastern",
            "durationInYardMS": 0,
            "orderHealthAndExecutionHealth": "RED"
        },
        {
            "orderNum": "2006227910",
            "tmsShipmentStatus": "Unassigned",
            "orderExecutionBucketDesc": "Transportation to be Planned",
            "orderDestination": {
                "name": "STAPLES # 472",
                "city": "PUTNAM",
                "state": "CT"
            },
            "originSiteNum": "2336",
            "orderOrigin": {
                "id": "2336",
                "name": "Eastern Regional DC",
                "city": "PITTSTON",
                "state": "PA",
                "country": "USA",
                "street": "325 CENTERPOINT BOULEVARD",
                "geoLocation": {
                    "longitude": -75.7501646,
                    "latitude": 41.2942816
                },
                "timezone": "US/Eastern",
                "postalCode": "18640-6139"
            },
            "appointmentRequired": "N",
            "orderOnHoldInd": "Y",
            "tmsOrderCreateDateTime": "2020-10-14T13:15:28.000Z",
            "customerNotifiedDateTime": [],
            "deliveryApptConfirmedDateTimeList": [],
            "orderExecutionBucket": "TRANS_PLAN_UNASSIGNED",
            "orderExecutionHealth": "RED",
            "originTimeZone": "US/Eastern",
            "durationInYardMS": 0,
            "orderHealthAndExecutionHealth": "RED"
        },
        {
            "orderNum": "2006227821",
            "tmsShipmentStatus": "Unassigned",
            "orderExecutionBucketDesc": "Transportation to be Planned",
            "orderDestination": {
                "name": "WALMART DC #6038",
                "city": "MARCY",
                "state": "NY"
            },
            "originSiteNum": "2336",
            "orderOrigin": {
                "id": "2336",
                "name": "Eastern Regional DC",
                "city": "PITTSTON",
                "state": "PA",
                "country": "USA",
                "street": "325 CENTERPOINT BOULEVARD",
                "geoLocation": {
                    "longitude": -75.7501646,
                    "latitude": 41.2942816
                },
                "timezone": "US/Eastern",
                "postalCode": "18640-6139"
            },
            "appointmentRequired": "N",
            "orderOnHoldInd": "Y",
            "tmsOrderCreateDateTime": "2020-10-13T16:45:06.000Z",
            "customerNotifiedDateTime": [],
            "deliveryApptConfirmedDateTimeList": [],
            "orderExecutionBucket": "TRANS_PLAN_UNASSIGNED",
            "orderExecutionHealth": "RED",
            "originTimeZone": "US/Eastern",
            "durationInYardMS": 0,
            "orderHealthAndExecutionHealth": "RED"
        },
        {
            "orderNum": "2006227957",
            "tmsShipmentStatus": "Unassigned",
            "orderExecutionBucketDesc": "Transportation to be Planned",
            "orderDestination": {
                "name": "TARGET #3802",
                "city": "AMSTERDAM",
                "state": "NY"
            },
            "originSiteNum": "2336",
            "orderOrigin": {
                "id": "2336",
                "name": "Eastern Regional DC",
                "city": "PITTSTON",
                "state": "PA",
                "country": "USA",
                "street": "325 CENTERPOINT BOULEVARD",
                "geoLocation": {
                    "longitude": -75.7501646,
                    "latitude": 41.2942816
                },
                "timezone": "US/Eastern",
                "postalCode": "18640-6139"
            },
            "appointmentRequired": "N",
            "orderOnHoldInd": "Y",
            "tmsOrderCreateDateTime": "2020-10-19T19:30:10.000Z",
            "customerNotifiedDateTime": [],
            "deliveryApptConfirmedDateTimeList": [],
            "orderExecutionBucket": "TRANS_PLAN_UNASSIGNED",
            "orderExecutionHealth": "RED",
            "originTimeZone": "US/Eastern",
            "durationInYardMS": 0,
            "orderHealthAndExecutionHealth": "RED"
        },
        {
            "orderNum": "2006227770",
            "tmsShipmentStatus": "Unassigned",
            "orderExecutionBucketDesc": "Transportation to be Planned",
            "orderDestination": {
                "name": "WALMART DC #6080",
                "city": "TOBYHANNA",
                "state": "PA"
            },
            "originSiteNum": "2336",
            "orderOrigin": {
                "id": "2336",
                "name": "Eastern Regional DC",
                "city": "PITTSTON",
                "state": "PA",
                "country": "USA",
                "street": "325 CENTERPOINT BOULEVARD",
                "geoLocation": {
                    "longitude": -75.7501646,
                    "latitude": 41.2942816
                },
                "timezone": "US/Eastern",
                "postalCode": "18640-6139"
            },
            "appointmentRequired": "N",
            "orderOnHoldInd": "Y",
            "tmsOrderCreateDateTime": "2020-10-08T20:15:29.000Z",
            "customerNotifiedDateTime": [],
            "deliveryApptConfirmedDateTimeList": [],
            "orderExecutionBucket": "TRANS_PLAN_UNASSIGNED",
            "orderExecutionHealth": "RED",
            "originTimeZone": "US/Eastern",
            "durationInYardMS": 0,
            "orderHealthAndExecutionHealth": "RED"
        },
        {
            "orderNum": "2006227936",
            "tmsShipmentStatus": "Unassigned",
            "orderExecutionBucketDesc": "Transportation to be Planned",
            "orderDestination": {
                "name": "CVS",
                "city": "WOONSOCKET",
                "state": "RI"
            },
            "originSiteNum": "2336",
            "orderOrigin": {
                "id": "2336",
                "name": "Eastern Regional DC",
                "city": "PITTSTON",
                "state": "PA",
                "country": "USA",
                "street": "325 CENTERPOINT BOULEVARD",
                "geoLocation": {
                    "longitude": -75.7501646,
                    "latitude": 41.2942816
                },
                "timezone": "US/Eastern",
                "postalCode": "18640-6139"
            },
            "appointmentRequired": "N",
            "orderOnHoldInd": "N",
            "tmsOrderCreateDateTime": "2020-10-16T11:05:10.000Z",
            "customerNotifiedDateTime": [],
            "deliveryApptConfirmedDateTimeList": [],
            "orderExecutionBucket": "TRANS_PLAN_UNASSIGNED",
            "orderExecutionHealth": "RED",
            "originTimeZone": "US/Eastern",
            "durationInYardMS": 0,
            "orderHealthAndExecutionHealth": "RED"
        },
        {
            "orderNum": "2006227824",
            "tmsShipmentStatus": "Unassigned",
            "orderExecutionBucketDesc": "Transportation to be Planned",
            "orderDestination": {
                "name": "WALMART DC #6038",
                "city": "MARCY",
                "state": "NY"
            },
            "originSiteNum": "2336",
            "orderOrigin": {
                "id": "2336",
                "name": "Eastern Regional DC",
                "city": "PITTSTON",
                "state": "PA",
                "country": "USA",
                "street": "325 CENTERPOINT BOULEVARD",
                "geoLocation": {
                    "longitude": -75.7501646,
                    "latitude": 41.2942816
                },
                "timezone": "US/Eastern",
                "postalCode": "18640-6139"
            },
            "appointmentRequired": "N",
            "orderOnHoldInd": "Y",
            "tmsOrderCreateDateTime": "2020-10-13T19:05:11.000Z",
            "customerNotifiedDateTime": [],
            "deliveryApptConfirmedDateTimeList": [],
            "orderExecutionBucket": "TRANS_PLAN_UNASSIGNED",
            "orderExecutionHealth": "RED",
            "originTimeZone": "US/Eastern",
            "durationInYardMS": 0,
            "orderHealthAndExecutionHealth": "RED"
        }
    ]
}

export const mockChartDonutGetService = { "ORDER": [{ "state": "SO_BLOCK_FREE", "stateDesc": "Customer Orders Block free", "totalCount": 35800, "redCount": 1, "yellowCount": 35755 }, { "state": "SO_BLOCKED", "stateDesc": "Customer Orders Blocked", "totalCount": 163, "redCount": 163, "yellowCount": 0 }, { "state": "STO", "stateDesc": "Stock Transfer Orders", "totalCount": 14, "redCount": 0, "yellowCount": 0 }] }

export const performanceView = [
    {
        "siteNum": "2023",
        "shortName": "Neenah Cold Spring Facility",
        "type": "MILL",
        "latitude": 44.2142929,
        "longitude": -88.4917463,
        "aheadOrBehind": "BEHIND",
        "hours": 10
    },
    {
        "siteNum": "2024",
        "shortName": "Maumelle Mill",
        "type": "MILL",
        "latitude": 34.8643233,
        "longitude": -92.3928768,
        "aheadOrBehind": "BEHIND",
        "hours": 4
    },
    {
        "siteNum": "2027",
        "shortName": "Jenks Mill",
        "type": "MILL",
        "latitude": 35.9668902,
        "longitude": -95.929945,
        "aheadOrBehind": "AHEAD",
        "hours": 0
    },
    {
        "siteNum": "2028",
        "shortName": "Beech Island Mill",
        "type": "MILL",
        "latitude": 33.4163503,
        "longitude": -81.8986332,
        "aheadOrBehind": "AHEAD",
        "hours": 3
    },
    {
        "siteNum": "2029",
        "shortName": "Ogden Mill",
        "type": "MILL",
        "latitude": 41.2940215,
        "longitude": -112.0065929,
        "aheadOrBehind": "AHEAD",
        "hours": 7
    },
    {
        "siteNum": "2031",
        "shortName": "New Milford Mill",
        "type": "MILL",
        "latitude": 41.5579034,
        "longitude": -73.4111248,
        "aheadOrBehind": "AHEAD",
        "hours": 2
    },
    {
        "siteNum": "2032",
        "shortName": "Paris Mill",
        "type": "MILL",
        "latitude": 33.6408131,
        "longitude": -95.593167,
        "aheadOrBehind": "BEHIND",
        "hours": 1
    },
    {
        "siteNum": "2034",
        "shortName": "Sofidel WI",
        "type": "DC",
        "latitude": 44.533441,
        "longitude": -88.085268,
        "aheadOrBehind": "BEHIND",
        "hours": 6
    },
    {
        "siteNum": "2042",
        "shortName": "Corinth Mill",
        "type": "MILL",
        "latitude": 34.9505695,
        "longitude": -88.4488715,
        "aheadOrBehind": "BEHIND",
        "hours": 5
    },
    {
        "siteNum": "2044",
        "shortName": "LaGrange Mill",
        "type": "DC",
        "latitude": 32.9954534,
        "longitude": -85.0381346,
        "aheadOrBehind": "AHEAD",
        "hours": 0
    },
    {
        "siteNum": "2047",
        "shortName": "Los Angeles DC",
        "type": "DC",
        "latitude": 34.040893,
        "longitude": -117.602477,
        "aheadOrBehind": "AHEAD",
        "hours": 0
    },
    {
        "siteNum": "2048",
        "shortName": "Loudon Mill",
        "type": "MILL",
        "latitude": 35.7714476,
        "longitude": -84.331066,
        "aheadOrBehind": "BEHIND",
        "hours": 9
    },
    {
        "siteNum": "2050",
        "shortName": "Mid-South DC",
        "type": "DC",
        "latitude": 35.0579005,
        "longitude": -92.4057446,
        "aheadOrBehind": "BEHIND",
        "hours": 2
    },
    {
        "siteNum": "2051",
        "shortName": "Fayard",
        "type": "DC",
        "latitude": 30.563955,
        "longitude": -88.133537,
        "aheadOrBehind": "BEHIND",
        "hours": 8
    },
    {
        "siteNum": "2066",
        "shortName": "Riverport",
        "type": "DC",
        "latitude": 37.7981,
        "longitude": -87.149625,
        "aheadOrBehind": "BEHIND",
        "hours": 3
    },
    {
        "siteNum": "2091",
        "shortName": "Geodis",
        "type": "DC",
        "latitude": 38.6759082,
        "longitude": -90.1541715,
        "aheadOrBehind": "AHEAD",
        "hours": 0
    },
    {
        "siteNum": "2172",
        "shortName": "Tufco",
        "type": "DC",
        "latitude": 44.466037,
        "longitude": -88.090633,
        "aheadOrBehind": "AHEAD",
        "hours": 0
    },
    {
        "siteNum": "2183",
        "shortName": "Swanson",
        "type": "DC",
        "latitude": 44.04918,
        "longitude": -88.52973,
        "aheadOrBehind": "AHEAD",
        "hours": 0
    },
    {
        "siteNum": "2190",
        "shortName": "Richmond Bonded Warehouse",
        "type": "DC",
        "latitude": 33.3690206,
        "longitude": -81.9788915,
        "aheadOrBehind": "AHEAD",
        "hours": 0
    },
    {
        "siteNum": "2283",
        "shortName": "Huntsville Mill",
        "type": "MILL",
        "latitude": 45.3461429,
        "longitude": -79.242198,
        "aheadOrBehind": "AHEAD",
        "hours": 0
    },
    {
        "siteNum": "2292",
        "shortName": "North Central DC",
        "type": "DC",
        "latitude": 41.6561017,
        "longitude": -88.1021821,
        "aheadOrBehind": "AHEAD",
        "hours": 0
    },
    {
        "siteNum": "2299",
        "shortName": "Southern Regional DC",
        "type": "DC",
        "latitude": 33.4042901,
        "longitude": -84.1929612,
        "aheadOrBehind": "AHEAD",
        "hours": 0
    },
    {
        "siteNum": "2323",
        "shortName": "KCP Romeoville DC",
        "type": "DC",
        "latitude": 41.6505626,
        "longitude": -88.1306639,
        "aheadOrBehind": "AHEAD",
        "hours": 0
    },
    {
        "siteNum": "2340",
        "shortName": "Saddle Creek",
        "type": "DC",
        "latitude": 34.9044511,
        "longitude": -88.5213648,
        "aheadOrBehind": "AHEAD",
        "hours": 0
    },
    {
        "siteNum": "2358",
        "shortName": "North Central Overflow DC",
        "type": "DC",
        "latitude": 41.6597135,
        "longitude": -88.097522,
        "aheadOrBehind": "AHEAD",
        "hours": 0
    },
    {
        "siteNum": "2360",
        "shortName": "Chester RDC",
        "type": "DC",
        "latitude": 39.7669725,
        "longitude": -75.3830806,
        "aheadOrBehind": "AHEAD",
        "hours": 0
    },
    {
        "siteNum": "2425",
        "shortName": "GBC North",
        "type": "DC",
        "latitude": 44.458983,
        "longitude": -88.1450964,
        "aheadOrBehind": "AHEAD",
        "hours": 0
    },
    {
        "siteNum": "2467",
        "shortName": "GBC South",
        "type": "DC",
        "latitude": 31.2998829,
        "longitude": -89.2752485,
        "aheadOrBehind": "AHEAD",
        "hours": 0
    },
    {
        "siteNum": "2474",
        "shortName": "Skin Care",
        "type": "DC",
        "latitude": 41.6517,
        "longitude": -88.13316,
        "aheadOrBehind": "AHEAD",
        "hours": 0
    },
    {
        "siteNum": "2496",
        "shortName": "South Dallas DC",
        "type": "DC",
        "latitude": 32.6761483,
        "longitude": -96.9650634,
        "aheadOrBehind": "AHEAD",
        "hours": 0
    },
    {
        "siteNum": "2504",
        "shortName": "GBC North Overflow",
        "type": "DC",
        "latitude": 44.46613,
        "longitude": -88.104704,
        "aheadOrBehind": "BEHIND",
        "hours": 9
    },
    {
        "siteNum": "2508",
        "shortName": "Northeast DC",
        "type": "DC",
        "latitude": 40.522329,
        "longitude": -75.966777,
        "aheadOrBehind": "BEHIND",
        "hours": 1
    },
    {
        "siteNum": "2513",
        "shortName": "Sofidel MS",
        "type": "DC",
        "latitude": 31.2561934,
        "longitude": -89.2642888,
        "aheadOrBehind": "BEHIND",
        "hours": 2
    },
    {
        "siteNum": "2528",
        "shortName": "Milton Ontario DC",
        "type": "DC",
        "latitude": 43.5410442,
        "longitude": -79.9133878,
        "aheadOrBehind": "BEHIND",
        "hours": 2.7
    },
    {
        "siteNum": "2822",
        "shortName": "Chester Mill",
        "type": "MILL",
        "latitude": 39.8470955,
        "longitude": -75.3556132,
        "aheadOrBehind": "BEHIND",
        "hours": 5
    },
    {
        "siteNum": "2827",
        "shortName": "Mobile Mill",
        "type": "MILL",
        "latitude": 30.7366615,
        "longitude": -88.0501943,
        "aheadOrBehind": "AHEAD",
        "hours": 9
    },
    {
        "siteNum": "2833",
        "shortName": "Marinette Mill",
        "type": "MILL",
        "latitude": 45.1018741,
        "longitude": -87.650146,
        "aheadOrBehind": "AHEAD",
        "hours": 7
    },
    {
        "siteNum": "2837",
        "shortName": "Owensboro Mill",
        "type": "MILL",
        "latitude": 37.8188063,
        "longitude": -87.3031246,
        "aheadOrBehind": "AHEAD",
        "hours": 6
    },
    {
        "siteNum": "2848",
        "shortName": "Northwest DC",
        "type": "DC",
        "latitude": 47.115485,
        "longitude": -122.628625,
        "aheadOrBehind": "AHEAD",
        "hours": 9
    }
];

export const mockShipmentDetailsStopsDetails = {
    "shipmentNum": "shipmentNum1234",
    "origin": {
        "name": "name1",
        "city": "NY",
        "state": "NY",
        "timezone": "America/New_York"
    },
    "finalDestination": {
        "name": "name1",
        "city": "NY",
        "state": "NY"
    },
    "tariffServiceCode": "TTYL",
    "equipmentTypeMode": "Truck Load",
    "nextStopETA": "2021-02-06T13:18:56.000Z",
    "nextStopTimezone": "America/New_York",
    "currentLocation": {
        "city": "New York",
        "state": "NY",
        "timestamp": "2021-02-06T13:18:56.000Z",
        "geoLocation": {
            "longitude": -88.13316,
            "latitude": 41.6517
        }
    },
    "stops": [
        {
            "stopNum": 1,
            "stopType": "PICKUP",
            "location": {
                "name": "KCDC 1",
                "city": "New York",
                "state": "NY",
                "timezone": "America/New_York",
                "geoLocation": {
                    "longitude": -88.13316,
                    "latitude": 41.6517
                }
            },
            "plannedArrivalDateTime": "2021-02-06T13:18:56.000Z",
            "actualArrivalDateTime": "2021-02-06T13:18:56.000Z",
            "actualDepartureDateTime": "2021-02-06T15:18:56.000Z"
        },
        {
            "stopNum": 2,
            "stopType": "DELIVERY",
            "stopName": "Walmart #2",
            "location": {
                "name": "Walmart #2",
                "city": "New York",
                "state": "NY",
                "timezone": "America/New_York",
                "geoLocation": {
                    "longitude": -88.13316,
                    "latitude": 41.6517
                }
            },
            "plannedArrivalDateTime": "2021-02-06T13:18:56.000Z",
            "actualArrivalDateTime": "2021-02-06T13:18:56.000Z",
            "actualDepartureDateTime": "2021-02-06T15:18:56.000Z"
        },
        {
            "stopNum": 3,
            "stopType": "DELIVERY",
            "location": {
                "name": "Walmart #3",
                "city": "New York",
                "state": "NY",
                "timezone": "America/New_York",
                "geoLocation": {
                    "longitude": -88.13316,
                    "latitude": 41.6517
                }
            },
            "plannedArrivalDateTime": "2021-02-06T13:18:56.000Z",
            "actualArrivalDateTime": "2021-02-06T13:18:56.000Z",
            "actualDepartureDateTime": "2021-02-06T15:18:56.000Z"
        }
    ],
    "locationHistory": [
        {
            "city": "New York",
            "state": "NY",
            "timestamp": "2021-02-06T13:18:56.000Z",
            "geoLocation": {
                "longitude": -88.13316,
                "latitude": 41.6517
            }
        },
        {
            "city": "New York",
            "state": "NY",
            "timestamp": "2021-02-06T13:18:56.000Z",
            "geoLocation": {
                "longitude": -88.13316,
                "latitude": 41.6517
            }
        },
        {
            "city": "New York",
            "state": "NY",
            "timestamp": "2021-02-06T13:18:56.000Z",
            "geoLocation": {
                "longitude": -88.13316,
                "latitude": 41.6517
            }
        },
        {
            "city": "New York",
            "state": "NY",
            "timestamp": "2021-02-06T13:18:56.000Z",
            "geoLocation": {
                "longitude": -88.13316,
                "latitude": 41.6517
            }
        },
        {
            "city": "New York",
            "state": "NY",
            "timestamp": "2021-02-06T13:18:56.000Z",
            "geoLocation": {
                "longitude": -88.13316,
                "latitude": 41.6517
            }
        },
        {
            "city": "New York",
            "state": "NY",
            "timestamp": "2021-02-06T13:18:56.000Z",
            "geoLocation": {
                "longitude": -88.13316,
                "latitude": 41.6517
            }
        },
        {
            "city": "New York",
            "state": "NY",
            "timestamp": "2021-02-06T13:18:56.000Z",
            "geoLocation": {
                "longitude": -88.13316,
                "latitude": 41.6517
            }
        },
        {
            "city": "New York",
            "state": "NY",
            "timestamp": "2021-02-06T13:18:56.000Z",
            "geoLocation": {
                "longitude": -88.13316,
                "latitude": 41.6517
            }
        },
        {
            "city": "New York",
            "state": "NY",
            "timestamp": "2021-02-06T13:18:56.000Z",
            "geoLocation": {
                "longitude": -88.13316,
                "latitude": 41.6517
            }
        },
        {
            "city": "New York",
            "state": "NY",
            "timestamp": "2021-02-06T13:18:56.000Z",
            "geoLocation": {
                "longitude": -88.13316,
                "latitude": 41.6517
            }
        },
        {
            "city": "New York",
            "state": "NY",
            "timestamp": "2021-02-06T13:18:56.000Z",
            "geoLocation": {
                "longitude": -88.13316,
                "latitude": 41.6517
            }
        },
        {
            "city": "New York",
            "state": "NY",
            "timestamp": "2021-02-06T13:18:56.000Z",
            "geoLocation": {
                "longitude": -88.13316,
                "latitude": 41.6517
            }
        },
        {
            "city": "New York",
            "state": "NY",
            "timestamp": "2021-02-06T13:18:56.000Z",
            "geoLocation": {
                "longitude": -88.13316,
                "latitude": 41.6517
            }
        },
        {
            "city": "New York",
            "state": "NY",
            "timestamp": "2021-02-06T13:18:56.000Z",
            "geoLocation": {
                "longitude": -88.13316,
                "latitude": 41.6517
            }
        },
        {
            "city": "New York",
            "state": "NY",
            "timestamp": "2021-02-06T13:18:56.000Z",
            "geoLocation": {
                "longitude": -88.13316,
                "latitude": 41.6517
            }
        },
        {
            "city": "New York",
            "state": "NY",
            "timestamp": "2021-02-06T13:18:56.000Z",
            "geoLocation": {
                "longitude": -88.13316,
                "latitude": 41.6517
            }
        },
        {
            "city": "New York",
            "state": "NY",
            "timestamp": "2021-02-06T13:18:56.000Z",
            "geoLocation": {
                "longitude": -88.13316,
                "latitude": 41.6517
            }
        },
        {
            "city": "New York",
            "state": "NY",
            "timestamp": "2021-02-06T13:18:56.000Z",
            "geoLocation": {
                "longitude": -88.13316,
                "latitude": 41.6517
            }
        },
        {
            "city": "New York",
            "state": "NY",
            "timestamp": "2021-02-06T13:18:56.000Z",
            "geoLocation": {
                "longitude": -88.13316,
                "latitude": 41.6517
            }
        },
        {
            "city": "New York",
            "state": "NY",
            "timestamp": "2021-02-06T13:18:56.000Z",
            "geoLocation": {
                "longitude": -88.13316,
                "latitude": 41.6517
            }
        },
        {
            "city": "New York",
            "state": "NY",
            "timestamp": "2021-02-06T13:18:56.000Z",
            "geoLocation": {
                "longitude": -88.13316,
                "latitude": 41.6517
            }
        },
        {
            "city": "New York",
            "state": "NY",
            "timestamp": "2021-02-06T13:18:56.000Z",
            "geoLocation": {
                "longitude": -88.13316,
                "latitude": 41.6517
            }
        },
        {
            "city": "New York",
            "state": "NY",
            "timestamp": "2021-02-06T13:18:56.000Z",
            "geoLocation": {
                "longitude": -88.13316,
                "latitude": 41.6517
            }
        },
        {
            "city": "New York",
            "state": "NY",
            "timestamp": "2021-02-06T13:18:56.000Z",
            "geoLocation": {
                "longitude": -88.13316,
                "latitude": 41.6517
            }
        }
    ]
}


export const mockGuardrailsDetails = {
	"unit" : "Loads",				// Possible values "Loads" or "Hours"
	"weekdays":	[
			{
				"day" : "Sunday",
				"dateTime" : "2021-11-21",
				"totalGuardrailFL": 100,
				"totalGuardrailLTL" : 100,
				"totalCountFL": 88,
				"totalCountLTL" : 102,
				"orderBusinessUnit":[
					{
						"businessUnit" : "CONSUMER",
						"countFL": 32,
						"countLTL": 20,
						"guardrailFL": 18,
						"guardrailLTL":18
					},
					{
						"businessUnit" : "PROFESSIONAL",
						"countFL": 20,
						"countLTL": 32,
						"guardrailFL": 36,
						"guardrailLTL":36
					},
					{
						"businessUnit" : "ALL",
						"countFL": 36,
						"countLTL": 50,
						"guardrailFL": 36,
						"guardrailLTL":36
					}
					
				]
			},
			{
				"day" : "Monday",
                "today": "Y",
				"dateTime" : "2021-11-22",
				"totalGuardRailFL": 100,
				"totalGuardRailLTL" : 100,
				"totalCountFL": 88,
				"totalCountLTL" : 102,
				"orderBusinessUnit":[
					{
						"businessUnit" : "CONSUMER",
						"countFL": 32,
						"countLTL": 20,
						"guardrailFL": 18,
						"guardrailLTL":18
					},
					{
						"businessUnit" : "PROFESSIONAL",
						"countFL": 20,
						"countLTL": 32,
						"guardrailFL": 36,
						"guardrailLTL":36
					},
					{
						"businessUnit" : "ALL",
						"countFL": 36,
						"countLTL": 50,
						"guardrailFL": 36,
						"guardrailLTL":36
					}
					
				]
			},
			{
				"day" : "Tuesday",
				"dateTime" : "2021-11-23",
				"totalGuardRailFL": 100,
				"totalGuardRailLTL" : 100,
				"totalCountFL": 88,
				"totalCountLTL" : 102,
				"orderBusinessUnit":[]
			},
			{
				"day" : "Sunday",
				"dateTime" : "2021-11-28",
				"totalGuardRailFL": 100,
				"totalGuardRailLTL" : 100,
				"totalCountFL": 88,
				"totalCountLTL" : 102,
				"orderBusinessUnit":[
					{
						"businessUnit" : "CONSUMER",
						"countFL": 32,
						"countLTL": 20,
						"guardrailFL": 18,
						"guardrailLTL":18
					},
					{
						"businessUnit" : "PROFESSIONAL",
						"countFL": 20,
						"countLTL": 32,
						"guardrailFL": 36,
						"guardrailLTL":36
					},
					{
						"businessUnit" : "ALL",
						"countFL": 36,
						"countLTL": 50,
						"guardrailFL": 36,
						"guardrailLTL":36
					}
					
				]
			},
		]
}


export const mockStockConstraintReport = {
    shipmentFullListResponse: ["543132345", "543232342", "543332343", "543432344", "543152346", "543132849", "543142347"],
    shipmentMaterialsResponse: {
        "siteNum": "2323",
        "region": "NA",
        "shipmentNum":"543132345",
        "plannedLoadingStartDatetime": "2022-01-05T23:00:00.000Z",
        "status": "CHECK_IN",
        "trailerNum": "6787",
        "carrierServiceCode": "FEDEX",
        "materialList": [{
            "materialNum": "109137107",
            "trailerNum": "6787",
            "carrierServiceCode": "FEDEX",
            "totalDemandQty": 1234,
            "totalUnRestrictedQty": -214,
            "deliveryQty": 30,
            "stockBalance": 77,
            "isShortage":true
        },
        {
            "materialNum": "109137193",
            "trailerNum": "6787",
            "carrierServiceCode": "FEDEX",
            "totalDemandQty": 1234,
            "totalUnRestrictedQty": -214,
            "deliveryQty": 30,
            "stockBalance": 77,
            "isShortage":true
        }
        
        ]
    },
    shipmentSearchResponse: {
        "siteNum": "2323",
        "region": "NA",
        "shipmentNum":"543132345",
        "plannedLoadingStartDatetime": "2022-01-05T23:00:00.000Z",
        "status": "CHECK_IN",
        "trailerNum": "6787",
        "carrierServiceCode": "FEDEX",
        "materialList": [{
            "materialNum": "109137107",
            "trailerNum": "6787",
            "carrierServiceCode": "FEDEX",
            "totalDemandQty": 1234,
            "totalUnRestrictedQty": -214,
            "deliveryQty": 30,
            "stockBalance": 77,
            "isShortage":true
        },
        {
            "materialNum": "109137193",
            "trailerNum": "6787",
            "carrierServiceCode": "FEDEX",
            "totalDemandQty": 1234,
            "totalUnRestrictedQty": -214,
            "deliveryQty": 30,
            "stockBalance": 77,
            "isShortage":true
        }
        
        ]
    },
    shipmentRecommendationResponse: [
        {
            "shipmentNum": "543132345",
            "materialNum": "109137193",
            "trailerNum": "6787",
            "carrierServiceCode": "FEDEX",
            "deliveryQty": 100,
            "status": "INBOUND_UNLOADING",
            "yardArrivalDestDatetime": "2021-12-29T21:23:07.000Z",
            "inboundShipmentUnloadingStartDatetime": "2021-12-29T21:23:07.000Z",
            "fourkitesExpectedDeliveryDatetime": "2021-12-29T21:23:07.000Z",
            "plannedShipmentEndDatetime": "2021-11-27T00:00:00.000Z",
            "isTrackedBy4kites": false,
            "reportDateDesc": "shipment unloading start dateTime",
            "reportDate": "2021-12-29T21:23:07.000Z",
            "estimatedUnloadTime":"01:30"
        },
        {
            "shipmentNum": "543132345",
            "materialNum": "109137193",
            "trailerNum": "6787",
            "carrierServiceCode": "FEDEX",
            "deliveryQty": 100,
            "status": "INBOUND_UNLOADING",
            "yardArrivalDestDatetime": "2021-12-29T21:23:07.000Z",
            "inboundShipmentUnloadingStartDatetime": "2021-12-29T21:23:07.000Z",
            "fourkitesExpectedDeliveryDatetime": "2021-12-29T21:23:07.000Z",
            "plannedShipmentEndDatetime": "2021-11-27T00:00:00.000Z",
            "isTrackedBy4kites": false,
            "reportDateDesc": "shipment unloading start dateTime",
            "reportDate": "2021-12-29T21:23:07.000Z",
            "estimatedUnloadTime":"01:30"
        }
    ]
}