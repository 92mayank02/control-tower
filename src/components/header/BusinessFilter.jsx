import React from 'react';
import { useSelector, useDispatch } from "react-redux";
import { Grid, Typography , useMediaQuery, useTheme, makeStyles, IconButton} from '@material-ui/core';
import {Favorite,CheckCircleOutline,CheckCircle,FavoriteBorder, ArrowBack} from "@material-ui/icons";

import clsx from "clsx";
import {
    setBusinessService,
    setBusinessLocalAction
} from "reduxLib/services";
import Snack from "components/common/Helpers/Snack";
import CustomizedTooltip from "components/common/CustomizedTooltip";
import { businessUnits as businessUnitsArray } from "configs/appConstants";

const useStyles = makeStyles((theme) => ({
    root: {
        marginTop: theme.spacing(1.5),
        marginBottom: theme.spacing(1.5),
        paddingRight: theme.spacing(1.5),
        align: 'left',
        size: theme.spacing(2),
    },
    item: {
        padding: theme.spacing(2)
    },
    icons: {
        cursor: 'pointer',
        color: 'white',
        marginRight: theme.spacing(2)
    },
    iconSet: {
        color: 'black'
    },
    iconUnset: {
        color: 'grey',
    },
    actionButton: {
        textTransform: 'capitalize',
        position: 'absolute',
        bottom: theme.spacing(2.5),
        color: theme.palette.white,
        border: `1px solid ${theme.palette.white}`
    },
}));


const BusinessFilter = (props) => {

    const { handleTabHideShow } = props
    const classes = useStyles();
    const dispatch = useDispatch();

    const isMobile = useMediaQuery(useTheme().breakpoints.down("sm"));

    const [snack, setSnack] = React.useState({
        open: false,
        severity: null,
        message: null
    });

    const { businessUnits, tempBusinessUnit } = useSelector(({ favorites }) => favorites);

    const businessUnitsLocal = tempBusinessUnit?.length === 0 ? businessUnitsArray : tempBusinessUnit;

    const setBusinessValue = (value, type) => {
        let business = [...(businessUnitsLocal ? businessUnitsLocal: []) ];
        if (type === "add") {
            business.push(value);
        } else {
            business = business.filter(d => d !== value)
        }

        if (business.length === 0) {
            setSnack({
                open: true,
                severity: "warning",
                message: "Atleast one business unit needs to selected"
            });
        } else {
            dispatch(setBusinessLocalAction([...business]));
        }
    }

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setSnack({
            ...snack,
            open: false
        });
    };
    return (
        <Grid className={classes.root} container xs={12} sm={12} md={6} lg={6} data-testid='businessfilter' >
            {isMobile && <IconButton data-testid='bumobiletab' onClick={()=>handleTabHideShow(true)}>
                    <ArrowBack/><Typography>BUSINESS UNIT</Typography>
                </IconButton>
            }
            {
                businessUnitsArray.map(d => {
                    const selected = businessUnitsLocal?.includes(d) || businessUnitsLocal?.length === 2;
                    return (
                        <Grid container className={classes.item}>
                            <Grid item xs={8} sm={2} md={4} lg={4} >
                                <Typography variant="body1" color="primary" component="div">
                                    <CustomizedTooltip >
                                        <span>
                                            {d}
                                        </span>
                                    </CustomizedTooltip>
                                </Typography>
                            </Grid>
                            <Grid item xs={1} className={clsx(classes.iconSet, classes.icons)}>
                                {
                                    selected ? <CheckCircle data-testid='uncheckicon' onClick={() => setBusinessValue(d, "remove")} name="uncheck" color="primary" /> :
                                        <CheckCircleOutline data-testid='checkicon' onClick={() => setBusinessValue(d, "add")} name="check" color="primary" />
                                }
                            </Grid>
                            <Grid item xs={1} className={clsx(classes.iconSet, classes.icons)}>
                                {
                                    businessUnits?.length === 2 || businessUnits?.includes(d) ?
                                        <Favorite name="unfav" onClick={(e) => {
                                            dispatch(setBusinessService({ value: { value: d }, type: "unfav", bunits: businessUnits, lbunits:businessUnitsLocal  }));
                                        }} className={classes.icons} /> :
                                        <FavoriteBorder name="fav" onClick={(e) => {
                                            dispatch(setBusinessService({ value: { value: d }, type: "fav", bunits: businessUnits, lbunits:businessUnitsLocal }));
                                        }} className={classes.icons} />
                                }
                            </Grid>
                        </Grid>
                    )
                })
            }
            <Snack {...snack} handleClose={handleClose} />
        </Grid>
    )
}


export default BusinessFilter;
