import React from 'react';
import { makeStyles, withStyles, Tabs, Tab, Divider, ListItem, ListItemText, ListItemIcon, useTheme, useMediaQuery } from '@material-ui/core';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import TabPanel from "./TabPanel";
import FavoritesModule from "./FavouritesModule"
import {FavBlock} from "./FavouritesModule"

const CustomTabs = withStyles((theme) => ({
  root: {
    borderBottom: `1px solid ${theme.palette.white}`,
  },
  indicator: {
    backgroundColor: theme.palette.secondary.accent,
    height: theme.spacing(.5)
  },
}))(Tabs);

const CustomTab = withStyles((theme) => ({
  root: {
    textTransform: 'none',
    minWidth: '45%',
    fontWeight: theme.typography.fontWeightRegular,
    '&:hover': {
      color: theme.palette.primary.main,
      opacity: 1,
    },
    '&$selected': {
      color: theme.palette.primary.main,
      fontWeight: theme.typography.fontWeightMedium,
    },
    '&:focus': {
      color: theme.palette.primary.main,
    },
  },
  selected: {},
}))((props) => <Tab disableRipple {...props} />);


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  listitem: {
    height: theme.spacing(8)
  },
  icon: {
    padding: 0,
    margin: 0,
    minWidth: 0
  }
}));

const TabsMenu = ({ setSecondaryTab, secondaryTab, primaryTab, setPrimaryTab, handleTabHideShow }) => {
  const classes = useStyles();
  const isMobile = useMediaQuery(useTheme().breakpoints.down("sm"));

  const primaryTabEvent = (e, tabindex) => {
    setPrimaryTab(tabindex);
    if (tabindex === 1) {
      setSecondaryTab(3);
    } else {
      setSecondaryTab(0);
    }
  }

  const tabsList = ['Business', 'Location', "Sales Office / Customer"] 

  return (
    <div className={classes.root} data-testid="tabsmenu">
      <CustomTabs value={primaryTab} onChange={primaryTabEvent}  >
        <CustomTab label="Menu" title="Menu" />
        <CustomTab label="Favourites" title="Favourites" />
      </CustomTabs>
      <TabPanel value={primaryTab} index={0}>
        {tabsList.map((text, index) => (
          <>
            <ListItem
              name="listitem"
              data-testid={`${text}_tab`}
              onClick={(event) => {                
                setSecondaryTab(index)
                handleTabHideShow(false)
              }}
              button key={text} selected={secondaryTab === index} className={classes.listitem} >
              <ListItemText primary={text} />
              <ListItemIcon className={classes.icon}>
                <ChevronRightIcon />
              </ListItemIcon>
            </ListItem>
            <Divider />
          </>
        ))}
      </TabPanel>
      <TabPanel value={primaryTab} index={1}>
        { isMobile ? 
          <FavoritesModule NoFavorites={FavBlock} />:
          <FavBlock />
        }
      </TabPanel>
    </div>
  );
}


export default TabsMenu;