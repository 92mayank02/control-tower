import React from "react";
import { Typography, makeStyles } from "@material-ui/core";
import MapNoResult from "../../assets/images/map-not-found.svg";

const useStyles = makeStyles((theme) => ({
    root: {
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        width: '500px',
        margin: "auto"

    },
    icon: {
        width: '40%'
    },
    text1: {
        textAlign: "center",
        paddingTop: '10px'
    },
    text2: {
        textAlign: "center",
        paddingTop: '10px'
    }
}));

const NoResult = ({ value, searchType }) => {
    const classes = useStyles();
    return (
        <div data-testid='noresult' className={classes.root}>
            <img src={MapNoResult} className={classes.icon} alt="No Results"/>
            <Typography variant="h2" color="primary" className={classes.text1} component="div">No {searchType} Found</Typography>
            <Typography variant="subtitle1" component="div" color="primary" className={classes.text2} >
                No {searchType} found matching the search term “{value}”
            </Typography>
        </div>
    )
}

export default NoResult;