import React from 'react';
import { Typography, Grid, Divider } from '@material-ui/core';
import FavoriteIcon from '@material-ui/icons/Favorite';
import { makeStyles } from '@material-ui/core/styles';
import { setBusinessService } from "reduxLib/services";
import { connect } from "react-redux";
import { get } from 'lodash';
import clsx from "clsx";
import { formatToSentenceCase } from "helpers";

const useStyles = makeStyles((theme) => ({
    root: {
    },
    businessfav: {
        overflowX: 'auto',
        paddingTop: theme.spacing(2),
        marginTop: theme.spacing(2),
        paddingRight: theme.spacing(1),
        align: 'left',
        size: '16px',
        lineHeight: '22px'
    },
    icons: {
        cursor: 'pointer'
    },
    iconSet: {
        color: theme.palette.black
    },
    iconUnset: {
        color: theme.palette.text.secondary,
    },
    title: {
        fontSize: 16,
        fontWeight: 300
    },
    divider: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1)
    }
}));

const BusinessFavs = (props) => {
    const classes = useStyles();
    const { businessUnits, setBusinessService } = props;
    return (
        <div data-testid="businessfavs">
            {
                businessUnits.length === 0 ? null :
                    <Grid item xs={12} className={classes.businessfav}>
                        <Typography className={classes.title}>Business</Typography>
                        <Grid container >
                            <Grid item xs={11}>
                                <Typography variant="h6" color="primary" component="div">
                                    {
                                        businessUnits.map(d => formatToSentenceCase(d)).join(" and ")
                                    }
                                </Typography>
                            </Grid>
                            <Grid item xs={1} className={clsx(classes.iconSet, classes.icons)}><FavoriteIcon onClick={() => setBusinessService([])} color="primary" name="favbutton" /></Grid>
                        </Grid>
                        <Divider className={classes.divider} />
                    </Grid>
            }
        </div>

    )
}

const mapStatetoProps = (state) => {
    return {
        businessUnits: get(state, "favorites.businessUnits", [])
    }
}
const mapDispatchToProps = {
    setBusinessService
};


export default connect(mapStatetoProps, mapDispatchToProps)(BusinessFavs);
