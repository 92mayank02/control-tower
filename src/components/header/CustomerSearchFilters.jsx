import React from 'react';
import { distributionChannel } from 'configs/appConstants';
import { Box, FormControlLabel, Grid, makeStyles, RadioGroup, Typography, FormControl, FormGroup, Accordion, AccordionSummary, AccordionDetails, useMediaQuery, useTheme } from '@material-ui/core';
import KCCheckBox from "components/common/Elements/KCCheckbox";
import KCRadio from 'components/common/Elements/KCRadio';
import { filterStyles } from 'theme';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';


const useStyles = makeStyles(filterStyles)


export default (props) => {
    const { handleChange, expanded } = props
    const classes = useStyles()
    const isMobile = useMediaQuery(useTheme().breakpoints.down("sm"))
    const { saleOrgsCheckboxArray, filterViewTypeEnum, filterViewType, customerFiltersHandlers, selectedDC } = props
    return (
        <Grid className={classes.customerFilterRoot} item xs={12} sm={12} md={4} lg={3}>
        <Accordion className={classes.accordian} disabled={!isMobile} expanded={ isMobile ? expanded === 'leftpanel' : true} onChange={handleChange('leftpanel')}>
            <AccordionSummary
                expandIcon={isMobile && <ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
                className={classes.accordianSummary}
            >
                <Typography variant='body1' className={classes.underline}> FilterBy</Typography>
            </AccordionSummary>
            <AccordionDetails className={classes.accordianDetails}>
                <Box mt={1} mb={1} >
                    <Typography variant="h6" >View By</Typography>
                    <FormControl component="fieldset">
                        <RadioGroup row aria-label="position" defaultValue="end" name="searchType" value={filterViewType} onChange={customerFiltersHandlers.searchType.handleChange}>
                            <FormControlLabel color="primary" value={filterViewTypeEnum.customer} control={<KCRadio />}
                                label={<Typography color="primary" className={classes.button}>Customer</Typography>}
                            />
                            <FormControlLabel  color="primary" value={filterViewTypeEnum.salesOfc} control={<KCRadio />}
                                label={<Typography color="primary" className={classes.button}>SalesOffice</Typography>}
                            />
                        </RadioGroup>
                    </FormControl>
                </Box>
                <Box mt={1} mb={1} >
                    <Typography variant="h6" >Sales Org</Typography>
                    <FormControl component="fieldset">
                        <FormGroup>
                            {
                                saleOrgsCheckboxArray.map((salesOrg, index) => {
                                    return (<FormControlLabel
                                        key={index}
                                        className={classes.label}
                                        control={<KCCheckBox name={salesOrg.value} color="primary" checked={salesOrg.checked} />}
                                        onChange={()=>customerFiltersHandlers.selectedSalesOrg.handleChange(salesOrg.value)}
                                        label={<Typography color="primary" className={classes.button}>{salesOrg.value} - {salesOrg.name}</Typography>}
                                    />)
                                })
                            }
                        </FormGroup>
                    </FormControl>
                </Box>
                <Box mt={1} mb={1} >
                    <Typography variant="h6" >Distribution Channel</Typography>
                    <FormControl component="fieldset">
                        <RadioGroup row aria-label="position" defaultValue="end" name="selectedDC" value={selectedDC} onChange={customerFiltersHandlers.distributionChannel.handleChange}>
                            { distributionChannel.map(({name,value}) => 
                                <FormControlLabel color="primary" value={value} control={<KCRadio />}
                                label={<Typography color="primary" className={classes.button}>{name}</Typography>}
                            />)}
                        </RadioGroup>
                    </FormControl>
                </Box>
            </AccordionDetails>
        </Accordion>
        </Grid>
    ) 
}