import React from "react";
import { Typography, makeStyles } from "@material-ui/core";
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import MapFavorite from "../../assets/images/map-favorite.svg";

const useStyles = makeStyles((theme) => ({
    root: {
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        color: theme.palette.white,
        width: '100%',
    },
    icon: {
        marginTop: '30px',
        width: '50%',
        [theme.breakpoints.down("sm")]:{
            width:"25%"
        }
    },
    text1: {
        textAlign: "center",
    },
    text2: {
        textAlign: "justify",
        paddingTop: '5px'
    }
}));

const NoFav = () => {
    const classes = useStyles();
    return (
        <div data-testid='nofav' className={classes.root}>
            <img src={MapFavorite} className={classes.icon} alt="Favourites"/>
            <Typography variant="h1" color="primary" className={classes.text1} component="div">You can Favourite Locations!!</Typography>
            <Typography variant="subtitle1" component="div" color="primary" className={classes.text2} >
                Favourited locations will appear as default selected location every time you visit Control Tower
            </Typography>
            <Typography variant="subtitle1" component="div" color="primary" className={classes.text2} >
                To favourite the locations just click on the fav icon <FavoriteBorderIcon /> next to the location name.
            </Typography>

        </div>
    )
}

export default NoFav;
