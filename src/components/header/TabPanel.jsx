import React from 'react';
import { Box, Typography } from '@material-ui/core';

const TabPanel = (props) => {
  const { children, value, index, classes, ...other } = props;

  return (
    <div
      role="tabpanel"
      data-testid="tabpanel"
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={0} className={classes?.padding}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

export default TabPanel;
