import React, { useState, useEffect } from 'react';
import {
  makeStyles,
  Drawer,
  IconButton,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  ListSubheader,
  Typography,
  Collapse, useMediaQuery, useTheme
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import HomeIcon from '../../assets/images/home.svg';
import OrdersIcon from '../../assets/images/cart.svg';
import DistributionIcon from '../../assets/images/warehouse.svg';
import TransportIcon from '../../assets/images/truck.svg';
import GlossaryIcon from '../../assets/images/glossary-icon.svg';
import AnalyticsIcon from '../../assets/images/analytics.svg';
import { CTLink } from './CTLink';
import { chartDataIdentifierForAPI as chartDataIdentifierForAPI } from '../../components/pages/PageRouter';
import MenuIcon from '@material-ui/icons/Menu';
import { ExpandLess, ExpandMore } from '@material-ui/icons';
import { intersection, isEmpty } from 'lodash';
import { useFeatureFlag } from 'configs/useFeatureFlag';
import { useHistory } from 'react-router-dom'

const drawerWidth = 300;

const useStyles = makeStyles((theme) => ({
  drawer: {
    flexShrink: 0
  },
  drawerPaper: {
    width: drawerWidth,
    backgroundColor: theme.palette.secondary.base,
    borderRight: 'none',
    top: theme.spacing(8)
  },
  drawerHeader: {
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar
  },
  listHeader: {
    height: theme.spacing(4),
    float: 'right'
  },
  close: {
    color: theme.palette.white,
    display: 'inline-flex',
    cursor: 'pointer'
  },
  closeIcon: {
    fontSize: 'medium',
    marginRight: theme.spacing(0.5),
    marginTop: theme.spacing(0.5)
  },
  listItems: {
    borderBottom: `1px solid ${theme.palette.white}`
  },
  menuButton: {
    marginTop: theme.spacing(1.5),
    padding: theme.spacing(1)
  },
  menuButtonFocus: {
    background: theme.palette.secondary.base,
    borderRadius: theme.spacing(0.5),
    marginTop: theme.spacing(1.5),
    padding: theme.spacing(1)
  },
  analyticsList: {
    background: theme.palette.menu?.base
  }
}));

const AppDrawer = ({ setDetails, pageDetails }) => {
  const classes = useStyles();
  const history = useHistory()
  const [openDrawer, setDrawerOpen] = useState(false);
  const activeMenu = intersection(
    Object.values(chartDataIdentifierForAPI),
    Object.keys(pageDetails)
  )[0];
  const [selectedIndex, setSelectedIndex] = useState(activeMenu);
  const [extend, setExtend] = React.useState(false);

  const isMobile = useMediaQuery(useTheme().breakpoints.down('sm'));
  const analyticsReports = useFeatureFlag('analyticsReports');
  const showAnalyticsLink = useFeatureFlag('showAnalyticsLink')?.enabled;

  const menuList = [
    {
      name: 'Home',
      iconUrl: HomeIcon,
      viewUrl: chartDataIdentifierForAPI.network
    },
    {
      name: 'Order Management',
      iconUrl: OrdersIcon,
      viewUrl: chartDataIdentifierForAPI.order
    },
    {
      name: 'Distribution',
      iconUrl: DistributionIcon,
      viewUrl: chartDataIdentifierForAPI.dist
    },
    {
      name: 'Transportation',
      iconUrl: TransportIcon,
      viewUrl: chartDataIdentifierForAPI.transPlan
    },
    {
      name: 'Glossary',
      iconUrl: GlossaryIcon,
      link: process.env.REACT_APP_GLOSSARY_URL
    }
  ];

  const [analyticsList, updateAnalyticsList] = useState(analyticsReports.description || []);

  const collapseAll = () => {
    setExtend(false);
    const collapseLists = analyticsList.map((d) =>
      d.open ? { ...d, open: false } : d
    );
    updateAnalyticsList(collapseLists);
  };

  const handleListItemClick = (category, reportUrl) => {
    setSelectedIndex(category);
    if (Object.values(chartDataIdentifierForAPI).includes(category)) {
      collapseAll();
    }
    setDrawerOpen(!openDrawer);

    history.push(`/dashboard/${category}`)
    // setDetails(
    //   pageDetails.type,
    //   Object.values(chartDataIdentifierForAPI).includes(category)
    //     ? category
    //     : chartDataIdentifierForAPI.analytics,
    //   true,
    //   !Object.values(chartDataIdentifierForAPI).includes(category)
    //     ? { reportURL: reportUrl } : {}
    // );
  };

  const onAnalyticsClick = (analyticFolderIndex) => {
    const activeLists = analyticsList.map((d, index) =>
      analyticFolderIndex == index ? { ...d, open: !d.open } : d
    );
    updateAnalyticsList(activeLists);
  };

  useEffect(() => {
    updateAnalyticsList(
      analyticsReports.description
    );
  }, [analyticsReports]);

  useEffect(() => {
    if (activeMenu && activeMenu !== chartDataIdentifierForAPI.analytics)
      setSelectedIndex(activeMenu);
  }, [activeMenu]);

  return (
    <>
      <IconButton
        color="inherit"
        data-testid="menu"
        aria-label="open drawer"
        onClick={() => setDrawerOpen(!openDrawer)}
        edge="start"
        className={openDrawer ? classes.menuButtonFocus : classes.menuButton}>
        <MenuIcon />
      </IconButton>
      <Drawer
        data-testid="app-drawer"
        className={classes.drawer}
        variant="persistent"
        anchor="left"
        open={openDrawer}
        classes={{ paper: classes.drawerPaper }}>
        <List
          subheader={
            <ListSubheader
              component="div"
              data-testid="nested-list-subheader"
              className={classes.listHeader}
              onClick={() => setDrawerOpen(!openDrawer)}>
              <CTLink className={classes.close} data-testid="closeicon">
                <CloseIcon className={classes.closeIcon} />
                <Typography variant="subtitle1">Close Menu</Typography>
              </CTLink>
            </ListSubheader>
          }></List>

        {menuList &&
          menuList.map((menuItem, index) => (
            <ListItem
              button
              className={classes.listItems} key={index}
              selected={selectedIndex === menuItem.viewUrl}>
              {menuItem.viewUrl && (
                <CTLink
                  className={classes.close}
                  onClick={() => handleListItemClick(menuItem.viewUrl, false)}>
                  <ListItemIcon>
                    {' '}
                    <img src={menuItem.iconUrl} />{' '}
                  </ListItemIcon>
                  <ListItemText primary={menuItem.name} />
                </CTLink>
              )}
              {menuItem.link && (
                <CTLink
                  className={classes.close}
                  target="_blank"
                  href={menuItem.link}
                  onClick={() => setDrawerOpen(!openDrawer)}>
                  <ListItemIcon>
                    {' '}
                    <img src={menuItem.iconUrl} />{' '}
                  </ListItemIcon>
                  <ListItemText primary={menuItem.name} />
                </CTLink>
              )}
            </ListItem>
          ))}

        {!isMobile && showAnalyticsLink && !isEmpty(analyticsList) && (
          <>
            <ListItem
              button
              className={classes.listItems}
              data-testid="analytics"
              onClick={() => setExtend(!extend)}>
              <ListItemIcon>
                {' '}
                <img src={AnalyticsIcon} />{' '}
              </ListItemIcon>
              <ListItemText primary="Supply Chain Analytics" />
              {extend ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse
              in={extend}
              className={classes.analyticsList}
              timeout="auto"
              unmountOnExit>
              {analyticsList.map((analyticFolder, analyticFolderIndex) => (
                <List component="div" disablePadding>
                  <ListItem
                    button
                    key={analyticFolder.name}
                    className={classes.listItems}
                    data-testid="analyticsFolder"
                    onClick={() => onAnalyticsClick(analyticFolderIndex)}>
                    <ListItemText primary={analyticFolder.name} />
                    {analyticFolder.open ? <ExpandLess /> : <ExpandMore />}
                  </ListItem>

                  <Collapse
                    in={analyticFolder.open}
                    timeout="auto"
                    unmountOnExit>
                    <List component="div" disablePadding>
                      {analyticFolder.reports.map((report) => (
                        <ListItem
                          button
                          key={report.reportName}
                          data-testid="analyticsReport"
                          selected={selectedIndex === report.reportName}
                          onClick={() =>
                            handleListItemClick(
                              report.reportName,
                              report.reportUrl
                            )
                          }>
                          <ListItemText primary={report.reportName} />
                        </ListItem>
                      ))}
                    </List>
                  </Collapse>
                </List>
              ))}
            </Collapse>
          </>
        )}
      </Drawer>
    </>
  );
};

export default AppDrawer;
