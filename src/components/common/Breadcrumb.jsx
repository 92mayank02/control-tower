import React from 'react';
import { Typography, makeStyles } from '@material-ui/core';
import { isEmpty } from "lodash" 

const useStyles = makeStyles((theme) => ({
    link: {
      cursor: 'pointer',
      textDecoration: 'underline'
    },
    container:{
      [theme.breakpoints.down("sm")]:{
        padding: ` 0px ${theme.spacing(2)}px`
      }
    }
  })
);
      
export const Breadcrumb = ({setDetails,type,label='', navHistory=[]}) => {

  const classes = useStyles();
  const handleClick = (id=null) => {
    if (typeof (setDetails) === "function") {
      setDetails(type, id, id? true :false)
    }
  }
  return (
    <>
      <Typography className={classes.container} variant="subtitle1" data-testid='breadcrumb' >
        <span data-testid='breadcrumb_home' className={classes.link} onClick={()=>handleClick()}>
          Home
        </span>
        { !isEmpty(navHistory) && navHistory.map((historyItem) => (
           <>&nbsp;&gt; <span data-testid={`intermediatebreadcrumb_${historyItem.id}`} className={classes.link} onClick={() => handleClick(historyItem.id)}>
           {historyItem.label}
          </span></>
        ))}
        &nbsp;&gt; {label}
      </Typography>
    </>
  ) 

}      