import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import { connect } from "react-redux";
import { Card, Typography } from '@material-ui/core';
import clsx from 'clsx';
import FormHelperText from '@material-ui/core/FormHelperText';
import SearchFilter from "./SearchFilter"
import CheckBoxElement from "../CheckBoxElement";
import RadioElement from "../RadioElement";
import DateFilter from "../DateFilter";
import TextCheckFilterElement from "../TextCheckFilterElement";
import OrdersCheckBoxElement from "./OrdersCheckBoxElement";
import { saveFilters, resetFilters } from "reduxLib/services";
import TextFilterElement from "../TextFilterElement";
import OriginFilter from "./OriginFilter";
import { filterStyles } from "theme";
import * as filters from "reduxLib/constdata/filters";
import FilterComponentContainer from './FilterComponentContainer';
import { allowedPlaces } from "theme/layouts/MainLayout"

const useStyles = makeStyles(filterStyles);

const Component = (props) => {
    const filterModules = {
        text: TextFilterElement,
        checkbox: CheckBoxElement,
        radio: RadioElement,
        date: DateFilter,
        sites: OriginFilter,
        checkboxradio: CheckBoxElement,
        ordercheckbox: OrdersCheckBoxElement,
        textcheck: TextCheckFilterElement
    }
    const { filter, type, subtype, open, filterKey } = props;
    const FilterComponent = filterModules[props.filter.type];
    return <FilterComponent
        type={type}
        filterValues={filter}
        subtype={subtype}
        openAll={open}
        filterKey={filterKey}
        title={filter.name}
        resetOthers={props.filter.type === "checkboxradio"}
        placeholder={`Enter ${filter.name}`}
        {...props} />
}

const Filters = ({ saveFilters, resetFilters, searchText, saveLoadDate, ...props }) => {
    const { type, subtype, exclude, filtertype } = props;
    const classes = useStyles();
    const [open, setOpen] = React.useState(props.open);
    // const allowedPlaces = ["network", "mylocation"];

    return (
        <div data-testid="filterMainContaner" className={props.open ? classes.show : classes.hide}>
            <Card className={classes.root} elevation={10}>
                <div className={classes.title}>
                    <Typography variant="h2" component="div" color="primary">FILTERS SEARCH</Typography>
                </div>
                <div className={classes.serchSpace}>
                    <Typography variant="h3" component="div" color="primary">SEARCH</Typography>
                    <SearchFilter
                        type={type}
                        subtype={subtype}
                        filterKey="searchStringList"
                    />
                    <FormHelperText className={classes.subtitle} style={{ color: "white" }} color="primary">{searchText}</FormHelperText>
                </div>
                <Grid justify="space-between" container alignItems="center" className={clsx(classes.container, classes.controls, classes.border)}>
                    <Grid item>
                        <Typography
                            align="left"
                            className={classes.controlText} variant="subtitle1" component="div" color="primary" onClick={() => {
                                resetFilters({
                                    filter: {}, type, subtype
                                })
                            }}>Clear All</Typography>
                    </Grid>
                    <Grid item>
                        <Typography
                            onClick={() => {
                                setOpen(!open);
                            }}
                            align="left"
                            className={classes.controlText} variant="subtitle1" component="div" color="primary">{!open ? "Expand" : "Collapse"} all</Typography>
                    </Grid>
                </Grid>
                {
                    Object.keys(filters[filtertype]).map((filterKey, index) => {
                        if (exclude.includes(filterKey)) {
                            return null;
                        }
                        const filter = filters[filtertype][filterKey];
                        return (
                            !allowedPlaces?.includes(type) && filterKey === 'sites'
                            ? 
                            null
                            :
                            <FilterComponentContainer
                                key={index}
                                type={type}
                                subtype={subtype}
                                openAll={open}
                                filter={filter}
                                filterKey={filterKey}
                                title={filter.name}
                            >
                                <Component
                                    key={index}
                                    type={type}
                                    subtype={subtype}
                                    openAll={open}
                                    filter={filter}
                                    filterKey={filterKey}
                                />
                            </FilterComponentContainer>
                        )
                    })
                }
            </Card>
        </div>

    )
};

const mapDispatchToProps = {
    saveFilters,
    resetFilters
};

export default connect(null, mapDispatchToProps)(Filters);
