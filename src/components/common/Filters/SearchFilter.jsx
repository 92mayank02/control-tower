import React from "react";
import SearchBar from "../SearchBar"
import { get } from "lodash";
import { connect } from "react-redux";
import { saveFilters  } from "../../../reduxLib/services";
import { stringToArray } from 'helpers';

const SearchFilter = (props) => {
    const { filterKey, filters, saveFilters, type , subtype} = props;
    const filter = get(filters, `${filterKey}.data`, null);

    const [search, setSearch] = React.useState(filter);

    const submitSearch = (e) => {
        e.preventDefault();
        setSearch(stringToArray(search))
        saveFilters({
            filter: {
                [filterKey]: {
                    ...filters[filterKey],
                    data: stringToArray(search) || null
                }
            }, type, subtype
        });
    }


    React.useEffect(() => {
        setSearch(filter);
    }, [filter])

    return (
        <SearchBar handleSubmit={submitSearch} searchPhrase={search} handleChange={(value)=> setSearch(value)} placeholder={"Search"} />
    )
}

const mapStatetoProps = (state, ownProps) => {
    const { subtype } = ownProps;
    return {
        filters: get(state, `options.filters.${subtype}`, {})
    }
};

const mapDispatchToProps = {
    saveFilters
};

export default connect(mapStatetoProps, mapDispatchToProps)(SearchFilter);
