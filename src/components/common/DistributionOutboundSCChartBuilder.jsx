import React from "react";
import { Grid } from '@material-ui/core';
import { getChartSegmentTotal } from "./ChartComponent";
import BarChart from "components/D3Charts/BarChart";
import { formatColumnData, columnKeys } from "../pages/DistributionView";


export const DistributionOutboundSCChartBuilder = (props) => {

  // React.useEffect(() => {
  //   rest.setRefresh({
  //     refresh: !rest.refresh,
  //     siteNums,
  //     time: new Date()
  //   })
  // }, []);

  const { margins, height, type, subtype, chartsData, keys, setDetails, setHeaderTotalCount } = props
  
  const data = formatColumnData(chartsData, columnKeys, "SHIPMENT_CREATED")
  setHeaderTotalCount(getChartSegmentTotal({keys,data}))
  
  return (
      <Grid container data-testid="distributionOutbound">
        <Grid item xs={12}><BarChart  keys={keys} data={formatColumnData(chartsData, columnKeys, "SHIPMENT_CREATED")} xKey="name" margins={margins} height={height} type={type} subtype={subtype} setDetails={setDetails} /></Grid>
      </Grid>
  );
}
