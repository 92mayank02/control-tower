import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { connect } from "react-redux";
import { Typography } from '@material-ui/core';
import clsx from 'clsx';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { get } from "lodash";
import { saveFilters } from "../../reduxLib/services";
import { filterStyles } from "theme";
// import FilterComponentContainer from "./Filters/FilterComponentContainer";
import KCCheckBox from "components/common/Elements/KCCheckbox";
const useStyles = makeStyles(filterStyles);

export const CheckBoxElement = (props) => {

    const { filters, filterKey, saveFilters, type, subtype, resetOthers } = props;

    const classes = useStyles();
    const filter = get(filters, `${filterKey}.data`, [])

    const setFilter = ({ item, target }) => {
        const checked = target.target.checked;
        const updatedobject = filter.map(d => {
            if (d.value === item.value) {
                return { ...d, checked }
            } else {
                if (resetOthers) {
                    return { ...d, checked: false }
                } else {
                    return d;
                }
            }
        });

        saveFilters({
            filter: {
                [filterKey]: {
                    ...filters[filterKey],
                    data: updatedobject
                }
            }, type, subtype
        });
    }
    return (
        // <FilterComponentContainer testId='checkBoxElement' {...props}>
            <FormControl component="fieldset" data-testid="checkBoxElement">
                <FormGroup>
                    {
                        filter.map((d, i) => {
                            return (<FormControlLabel
                                key={i}
                                control={<KCCheckBox name={d.value} color="primary" onChange={(e) => setFilter({ item: d, target: e })} checked={d.checked} />}
                                label={<Typography color="primary" className={clsx(classes.button, classes.contentText)}>{d.name || "-"}</Typography>}
                            />)
                        })
                    }
                </FormGroup>
            </FormControl>
        // </FilterComponentContainer>
    )
}

const mapStatetoProps = (state, ownProps) => {
    const { subtype } = ownProps;
    return {
        filters: get(state, `options.filters.${subtype}`, {})
    }
};

const mapDispatchToProps = {
    saveFilters
};

export default connect(mapStatetoProps, mapDispatchToProps)(CheckBoxElement);
