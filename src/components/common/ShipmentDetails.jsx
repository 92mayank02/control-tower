import React, { useEffect } from 'react'
import { Grid, Box, Typography, makeStyles, AppBar, Tabs, Tab } from "@material-ui/core"
import { Breadcrumb } from "../common/Breadcrumb"
import { useSelector, useDispatch } from 'react-redux';
import moment from "moment-timezone";
import getShipmentDetailsService from '../../reduxLib/services/getShipmentDetailsService'
import ShipmentTrackingDetails from './ShipmentTrackingDetails';
import { detailsConstants } from '../../reduxLib/constants';
import {shipmentStyles} from "theme"
import { saveFilters, setCurrentView } from 'reduxLib/services';

const useStyles = makeStyles(shipmentStyles);


function TabPanel(props) {
    const { children, value, index, ...other } = props;
    return (
        <div 
            className={other.className}
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-auto-tabpanel-${index}`}
            aria-labelledby={`scrollable-auto-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    {children}
                </Box>
            )}
        </div>
    );
}

export const ShipmentDetails = ({ setDetails, type, pageDetails }) => {

    const classes = useStyles();
    const dispatch = useDispatch();
    const getShippingDetails = (activeSearchType) => {
        dispatch(getShipmentDetailsService({ shipmentNum: pageDetails.filterParams.shipmentNum, searchType: activeSearchType }));
    }
    const resetFilterAndViewsToDefault = () => {
        dispatch(saveFilters({defaultViewsFilters}))
        dispatch(setCurrentView())
    }

    const [value, setValue] = React.useState(0);
    const shipmentDetails = useSelector(({ shipment: { details } }) => details);
    const shipmentTrackingData = useSelector(({ shipment: { details: {origin}, locationHistory, stops } }) => ({ origin, locationHistory, stops }) );
    const defaultViewsFilters = useSelector(({user:{defaultViewsFilters}}) => defaultViewsFilters)

    const handleChange = (event, newValue) => {
        setValue(newValue);
        if (newValue) {
            getShippingDetails(detailsConstants.TRACKING_DETAILS);
        }
    };

    useEffect(() => {
        resetFilterAndViewsToDefault()
        getShippingDetails(detailsConstants.STOPS_DETAILS);
    }, [])

    return (
        <Grid className={classes.component}>
            <Breadcrumb setDetails={setDetails} navHistory={pageDetails.filterParams.navHistory} type={type} label='Track & Trace' />
            <Grid className={classes.container} data-testid="shipmentdetails" justify='space-between' container >
                <Grid  item container xs={12} sm={12} md={2} md={2} className={classes.leftBox} >
                    <Grid item xs={6} sm={6} md={12} md={12} >
                        <Typography variant="h3">Shipment/Load Number</Typography>
                        <Typography className={classes.shipmentNumber}>{shipmentDetails.shipmentNum}</Typography>
                    </Grid>
                    <Grid item xs={6} sm={6} md={12} md={12} >
                        <Typography variant="h3" >Fourkites ETA</Typography>
                        <Typography variant="h6">{shipmentDetails?.nextStopETA ? moment(shipmentDetails.nextStopETA)?.tz(shipmentDetails.nextStopTimezone)?.format(`MMM DD YYYY HH:mm z`) : "-" }</Typography>
                    </Grid>
                </Grid>
                <Grid container item xs={12} sm={12} md={9} md={9} spacing={4} >
                    <Grid item xs={6} sm={6} md={4} md={4} >
                        <Typography variant="h3" >Origin</Typography>
                        <Typography variant="h6" >{shipmentDetails.origin?.name}</Typography>
                    </Grid>
                    <Grid item xs={6} sm={6} md={4} md={4} >
                        <Typography variant="h3" >Carrier</Typography>
                        <Typography variant="h6" >{shipmentDetails.tariffServiceCode || "-" }</Typography>
                    </Grid>
                    <Grid item xs={6} sm={6} md={4} md={4} >
                        <Typography variant="h3" >Latest Location</Typography>
                        {shipmentDetails?.currentLocation && <Typography variant="body1" >{shipmentDetails.currentLocation?.city}, {shipmentDetails.currentLocation?.state}</Typography>}
                        <Typography variant="h6" >{shipmentDetails?.currentLocation ? moment(shipmentDetails.currentLocation?.timestamp)?.tz(shipmentDetails.origin?.timezone)?.format(`MMM DD YYYY HH:mm z`) : "-" }</Typography>
                    </Grid>
                    <Grid item xs={6} sm={6} md={4} md={4} >
                        <Typography variant="h3" >Destination</Typography>
                        <Typography variant="h6" >{ shipmentDetails?.finalDestination ?  shipmentDetails.finalDestination?.name : "-" }</Typography>
                    </Grid>
                    <Grid item xs={6} sm={6} md={4} md={4} >
                        <Typography variant="h3" >Mode</Typography>
                        <Typography variant="h6" >{shipmentDetails.equipmentTypeMode || "-" }</Typography>
                    </Grid>
                </Grid>
            </Grid>
            <Grid>
                <div className={classes.root} data-testid="shipmentdetailstabs">
                    <AppBar position="static" className={classes.appbar} elevation={0}>
                        <Tabs
                            value={value}
                            onChange={handleChange}
                            indicatorColor="secondary"
                            textColor="primary"
                        >
                            <Tab className={classes.tab} selectionFollowsFocus
                                label={<Typography className={classes.tabName} variant="subtitle1" >Stops</Typography>}
                                id="scrollable-auto-tab-0"
                                aria-controls="scrollable-auto-tabpanel-0"
                            />
                            <Tab className={classes.tab} selectionFollowsFocus
                                label={<Typography className={classes.tabName} variant="subtitle1" >Tracking Details</Typography>}
                                id="scrollable-auto-tab-1"
                                aria-controls="scrollable-auto-tabpanel-1"
                            />
                        </Tabs>
                    </AppBar>
                    <TabPanel className={classes.tabPanel} value={value} index={0} >
                        <Typography>STOPS</Typography>
                        {shipmentDetails?.stops && shipmentDetails.stops.map((stop, index) => (
                            <Grid container className={classes.stops} key={index} spacing={2}>
                                <Grid item variant="body1" xs={12} sm={12} md={3} md={3} >
                                    <Typography>{stop.stopType}</Typography>
                                </Grid>
                                <Grid item center variant="h6" xs={6} sm={6} md={2} md={2} >
                                    <Typography>{stop.location?.name}, {stop.location?.city}, {stop.location?.state}</Typography>
                                </Grid>
                                <Grid item center xs={6} sm={6} md={2} md={2} >
                                    { stop.plannedArrivalDateTime && <><Typography variant="h6">{stop.stopNum===1 ? "Planned Arrival" : "Appointment Time"}</Typography>
                                    <Typography>{moment(stop.plannedArrivalDateTime)?.tz(stop.location.timezone)?.format(`MMM DD YYYY HH:mm z`)}</Typography></>}
                                </Grid>
                                <Grid item center xs={6} sm={6} md={2} md={2} >
                                    { stop.actualArrivalDateTime && <><Typography variant="h6" >Detected Near Facility</Typography>
                                    <Typography>{moment(stop.actualArrivalDateTime)?.tz(stop.location.timezone)?.format(`MMM DD YYYY HH:mm z`)}</Typography></>}
                                </Grid>
                                {stop.actualDepartureDateTime && <Grid item xs={6} sm={6} md={2} md={2} >
                                    <Typography center variant="body1" >Departed Facility</Typography>
                                    <Typography>{moment(stop.actualDepartureDateTime)?.tz(stop.location.timezone)?.format(`MMM DD YYYY HH:mm z`)}</Typography>
                                </Grid>}
                            </Grid>
                        ))}
                    </TabPanel>
                    <TabPanel className={classes.tabPanel} value={value} index={1} >
                        <ShipmentTrackingDetails type={type} shipmentTrackingData={shipmentTrackingData}/>
                    </TabPanel>
                </div>
            </Grid>
        </Grid>
    )
}

export default ShipmentDetails;
