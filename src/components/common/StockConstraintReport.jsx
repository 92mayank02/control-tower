import React from 'react'
import { useSelector } from "react-redux"
import { Grid, makeStyles, Typography } from "@material-ui/core"
import { Breadcrumb } from "../common/Breadcrumb"
import DateFilter from "./DateFilter"

const useStyles = makeStyles((theme) => ({
    plantName: {
        color: theme.palette.menu.base
    },
    container: {
        padding: `${theme.spacing(4)}px ${theme.spacing(4)}px`,
        [theme.breakpoints.down("sm")]: {
            padding: `0px ${theme.spacing(3)}px`,
        }
    },
    component: {
        marginTop: theme.spacing(8)
    },
    item: {
        paddingLeft: theme.spacing(8),
        paddingRight: theme.spacing(3),
        borderLeft: `2px solid ${theme.palette.white}`,
        [theme.breakpoints.down("sm")]: {
            paddingLeft: 0,
            paddingRight: 0,
            borderLeft: 'none',
            marginTop: theme.spacing(1),
        }
    },
})
);

const StockConstraintReport = ({ setDetails, type, pageDetails }) => {
    const classes = useStyles();
    const locations = useSelector(({ sites }) => sites.locations);
    const plantName = locations.filter(item => item.siteNum === type)[0].shortName;

    return (
        <Grid className={classes.component}>
            <Breadcrumb setDetails={setDetails} navHistory={pageDetails.filterParams.navHistory} type={type} label='Stock Constraint Report' />
            <Grid className={classes.container} data-testid="stockConstraintReport" container >
                <Grid item xs={6} sm={6} md={4}>
                    <Typography variant="h2" >{`Stock Constraint report: ${plantName}`}</Typography>
                    <Typography variant="h2" className={classes.plantName}>{`PLANT: ${type}`}</Typography>
                </Grid>
                <Grid item xs={6} sm={6} md={8} className={classes.item}>
                    <Grid item xs={12} sm={12} md={8} style={{ display: 'flex' }}>
                        <Grid item xs={2} sm={2} md={3} >
                            <Typography variant="h2" >{`LOAD DATE`}</Typography>
                        </Grid>
                        <Grid item xs={6} sm={6} md={12} style={{ display: 'flex' }}>
                            <DateFilter
                                filterKey={"loadDateTimeOriginTZ"}
                                type={type}
                                subtype={'stockConstraintReport'}
                                disableStartDate={true}
                            />
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    )
}

export default StockConstraintReport
