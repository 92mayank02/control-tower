import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ResponsiveWrapper from "../charts/ResponsiveWrapper";
import { Typography } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  skeleton: {
    backgroundColor: theme.palette.primary.dark,
    padding: theme.spacing(2)
  },
  loader: {
  }
}));

const SkeletonLoader = ({ dimensions, parentWidth }) => {
  const classes = useStyles();
  return (
    
    <div data-testid="skeletonLoader" className={classes.loader} style={{ height: dimensions.height + 6}}>
        <Typography color="primary">
        Please Wait...
          </Typography> 

      </div>
  );
}

export default ResponsiveWrapper(SkeletonLoader);
