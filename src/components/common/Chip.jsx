import React from 'react';
import { makeStyles, Chip } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    root: {
        border: `1px solid white`,
        height: 35,
        margin: theme.spacing(0.5),
        padding: theme.spacing(1),
        [theme.breakpoints.down("sm")]:{
            marginBottom: theme.spacing(1)
        }
    },
    deleteIconOutlinedColorPrimary: {
        color: theme.palette.white
    }
}));

const KCChip = (props) => {
    const classes = useStyles();

    return (
        <Chip
            data-testid='chip'
            classes={classes}
            color="primary"
            variant="outlined"
            {...props}
        />
    )
};

export default  KCChip;