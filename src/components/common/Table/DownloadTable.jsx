import React from 'react';
import {
  IconButton,
  makeStyles,
  Typography,
  ClickAwayListener,
  Box,
  Grid,
  useMediaQuery,
  useTheme,
  Card
} from '@material-ui/core';
import clsx from "clsx";
import LaunchIcon from '@material-ui/icons/Launch';
import { connect } from 'react-redux';
import { get } from 'lodash';
import { downloadService, resetDownload } from 'reduxLib/services';
import ExcelIcon from 'assets/images/excel.png';
import Snack from '../Helpers/Snack';

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative',
  },
  title: {
    backgroundColor: theme.palette.secondary.dark,
    textAlign: 'left',
    paddingLeft: theme.spacing(2),
    paddingTop: theme.spacing(.5),
    paddingBottom: theme.spacing(.5),
    borderTop: `2px solid ${theme.palette.border}`,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  titleHeading: {
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1),
    backgroundColor: theme.palette.secondary.base,
  },
  dropdown: {
    position: 'absolute',
    zIndex: 100,
    borderRadius: '0 !important',
    width: 350,
    right: 0,
    justifyContent: 'left'
  },
  icon: {
    height: 20,
    width: 20
  },
}));

const DownloadTable = (props) => {
  const { downloadService, downloads, downloadConfig } = props;

  const { downloading } = downloads;
  const [open, setOpen] = React.useState(false);

  const handleClick = () => {
    setOpen((prev) => !prev);
  };

  const handleClickAway = () => {
    setOpen(false);
  };

  const classes = useStyles();

  const [snack, setSnack] = React.useState({
    open: false,
    severity: null,
    message: null
  });

  const handleSnackClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setSnack({
      ...snack,
      open: false
    });
  };

  const isMobile = useMediaQuery(useTheme().breakpoints.down('sm'));

  const downloadExcel = (itemInfoRequired = false) => {
    if (!downloading) {
      downloadService({ ...downloadConfig, itemInfoRequired });
      setSnack({
        open: true,
        severity: 'info',
        message: `File Download started. This might take a couple of seconds.`
      });
    } else {
      setSnack({
        open: true,
        severity: 'warning',
        message: `File download is already in progress`
      });
    }
  }

  return (
    <Grid item className={classes.root} data-testid="downloadtable" justify="flex-start">
      <Box textAlign="center" aria-label="Export Table" color="primary">
        <ClickAwayListener onClickAway={handleClickAway}>
          <div className={classes.root}>
            <IconButton
              aria-label="ExportTable"
              color="primary" onClick={handleClick}>
              <img src={ExcelIcon} alt="CSV" />
            </IconButton>
            {!isMobile && <Typography variant="body2">Export</Typography>}
            {open ? (
              <Card className={classes.dropdown}>
                <div className={clsx(classes.title, classes.titleHeading)}>
                  <Typography variant="h2" component="p" color="primary">EXPORT VIEW</Typography>
                </div>
                <div className={classes.title}>
                  <Typography variant="h2" component="p" color="primary">WITH PRODUCT DETAILS</Typography>
                  <IconButton><LaunchIcon className={classes.icon} aria-label="exportWith" onClick={() => downloadExcel(true)} /></IconButton>
                </div>
                <div className={classes.title}>
                  <Typography variant="h2" component="p" color="primary">WITHOUT PRODUCT DETAILS</Typography>
                  <IconButton><LaunchIcon className={classes.icon} aria-label="exportWithOut" onClick={() => downloadExcel(false)} /></IconButton>
                </div>
              </Card>
            ) : null}
          </div>
        </ClickAwayListener>
      </Box>
      <Snack {...snack} handleClose={handleSnackClose} />
    </Grid>
  );
};

const mapStateToProps = (state, ownProps) => {
  const {
    downloadConfig: { type, subtype }
  } = ownProps;
  return {
    downloads: get(state, `downloads.${type}.${subtype}`, {})
  };
};

const mapDispatchToProps = {
  downloadService,
  resetDownload
};

export default connect(mapStateToProps, mapDispatchToProps)(DownloadTable);
