import React from "react";
import { makeStyles, Typography, ClickAwayListener } from "@material-ui/core"
import { ExpandMore, ExpandLess } from '@material-ui/icons';
import KCButton from "components/common/Button";
import { ColumnsView } from "./TableElements/ColumnsView";

const useStyles = makeStyles(theme => ({
    root: {
        paddingLeft: theme.spacing(1.5),
        paddingRight: theme.spacing(1.5),
        [theme.breakpoints.down("sm")]:{
            padding:0
        }
    },
    text: {
        pointerEvents: 'none'
    },
    button: {
        marginTop: theme.spacing(1),
        width: "250px",
        height: theme.spacing(5),
        flexGrow: 1,
        display: "flex",
        justifyContent: "space-between",
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2),
        border: `1px solid ${theme.palette.border}`,
        textTransform: 'capitalize',
        [theme.breakpoints.down("sm")]:{
            maxWidth: "180px",

        }
    },
}));

export const ColumnManager = ({ columns, columnsAction }) => {
    const classes = useStyles();

    const [open, setOpen] = React.useState(false);

    const handleButtonClick = () => {
        setOpen((prev) => !prev);
    };

    const handleClose = () => {
        setOpen(false)
    }

    return (
        <ClickAwayListener onClickAway={handleClose}>
            <div className={classes.root} data-testid="columnmanager">
                <KCButton
                    endIcon={!open ? <ExpandMore /> : <ExpandLess />}
                    className={classes.button}
                    onClick={handleButtonClick}
                    color="primary"
                >
                    <Typography className={classes.text}>Manage Columns</Typography>
                </KCButton>
                <ColumnsView openManager={open} columns={columns} columnsAction={columnsAction} />
            </div>
        </ClickAwayListener>
    )
}

