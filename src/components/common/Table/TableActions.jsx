import React from "react";
import { IconButton, makeStyles, ClickAwayListener, Typography, Box, Grid, useMediaQuery, useTheme } from "@material-ui/core"
import { Refresh as RefreshIcon, FilterList as FilterListIcon } from '@material-ui/icons';
import DownloadTable from "./DownloadTable";
import FilterViewsMenu from '../FilterViewsMenu';
import { ColumnManager } from "./ColumnManager";
import clsx from "clsx";
const useStyles = makeStyles(theme => ({
    root: {
        paddingLeft: theme.spacing(1.5),
        paddingRight: theme.spacing(1.5),
        [theme.breakpoints.down('sm')]:{
            paddingLeft: theme.spacing(0),
            paddingRight: theme.spacing(0),
        }
    },
    inline: {
        display: 'flex'
    },
    order:{
        [theme.breakpoints.down("sm")]:{
            order:1,
            marginTop:theme.spacing(0)
        }
    }
}));


const TableActionsFilters = (props) => {

    const isMobile = useMediaQuery(useTheme().breakpoints.down('sm'));

    const { refreshTable, handleClose, handleClick, filters, downloads, filterBody, subtype, type, columns, columnsAction, defaultColumns } = props;
    const classes = useStyles();

    return (<Grid container className={classes.inline} color="primary" justify='flex-end' data-testid="tableactions">
        <Grid item className={clsx(classes.root, classes.order)}>
            <Box textAlign='center' aria-label="Manage Columns" color="primary">
                <ColumnManager columns={columns} columnsAction={columnsAction} />
            </Box>
        </Grid>
        <Grid item className={classes.root}>
            <Box textAlign='center' aria-label="Refrsh Table" color="primary">
                <IconButton onClick={refreshTable} aria-label="Refrsh Table" color="primary">
                    <RefreshIcon />
                </IconButton>
                { !isMobile && <Typography variant='body2'>Refresh</Typography>}
            </Box>
        </Grid>
        <DownloadTable downloadConfig={downloads} />
        <Grid item className={classes.root}>
            <Box textAlign='center' aria-label="Refrsh Table" color="primary">
                <FilterViewsMenu defaultColumns={defaultColumns} filterBody={filterBody} type={type} tableName={subtype} />
            </Box>
        </Grid>
        <ClickAwayListener onClickAway={handleClose}>
            <div className={classes.root}>
                <Box textAlign='center' aria-label="Filters" color="primary">
                    <IconButton onClick={handleClick} aria-label="Filters" color="primary">
                        <FilterListIcon />
                    </IconButton>
                    { !isMobile && <Typography variant='body2'>Filters</Typography>}
                </Box>
                {
                    filters
                }
            </div>
        </ClickAwayListener>
    </Grid>)
}

export default TableActionsFilters;
