import React from "react";
import { useJsonToCsv } from 'react-json-csv';
import { connect } from "react-redux";
import { get } from "lodash";
import { resetDownload } from "reduxLib/services";
import moment from "moment-timezone";
import { generateDefaultData } from "helpers";
import Snack from "../Helpers/Snack";


export const DownloadManager = (props) => {

    const { downloads, resetDownload } = props;

    const { saveAsCsv } = useJsonToCsv();

    const date = moment(new Date()).format("MM-DD-YYYY");

    const [snack, setSnack] = React.useState({
        open: false,
        severity: null,
        message: null
    });

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setSnack({
            ...snack,
            open: false
        });
    };

    React.useEffect(() => {
        Object.keys(downloads).map(type => {
            const filenames = {
                network: `Network_Table_${type}_${date}`,
                orders: `Order_Management_${type}_${date}`,
                transport: `Transportation_${type}_${date}`,
                distinbound: `Distribution_Inbound_${type}_${date}`,
                distoutbound: `Distribution_Outbound_${type}_${date}`
            }
            if (type) {
                Object.keys(downloads?.[type]).map(subtype => {
                    const { status, data, process, detailsProcess, itemInfoRequired } = downloads[type][subtype];
                    const allColumns = itemInfoRequired ? [...get(downloads[type][subtype], "columns", []), ...get(downloads[type][subtype], "detailsColumns", [])] : [...get(downloads[type][subtype], "columns", [])];
                    if (status === "Success" && data) {
                        if (allColumns) {
                            let fields = {};
                            allColumns.map(d => {
                                if (d.field === "slno") return;
                                fields = {
                                    ...fields,
                                    [d.field]: d.title
                                };
                                return d;
                            });
                            const processed = data.map(d => {

                                let data1 = {
                                    ...generateDefaultData(allColumns),
                                    ...d,
                                    ...process(d),
                                    ...detailsProcess(d)
                                };

                                let processed1 = {};

                                Object.keys(data1).map(item => {
                                    try {
                                        if (typeof data1[item] === "string") {
                                            processed1[item] = data1[item].replace(/[,\s]+|[,\s]+/g, ' ');
                                        } else {
                                            processed1[item] = data1[item];
                                        }
                                    } catch (e) {
                                        processed1[item] = data1[item];
                                    }
                                    return item;
                                });

                                return processed1
                            })
                            saveAsCsv({ data: processed || [], fields, filename: filenames[subtype] });
                        }
                        resetDownload({ type, subtype });
                        setSnack({ open: false })
                        setSnack({
                            open: true,
                            severity: "success",
                            message: `Download Successful!`
                        });
                    } else if (status === "Failed") {
                        setSnack({
                            open: true,
                            severity: "error",
                            message: `Sorry! File Download Failed. Please try again.`
                        })
                    }
                    return subtype;
                })
            }
            return null;
        })
    }, [downloads])

    return (
        <div data-testid="downloadmanager">
            <Snack {...snack} handleClose={handleClose} />
        </div>

    )

}

const mapStateToProps = (state, ownProps) => {
    return {
        downloads: get(state, `downloads`, {}),
    }
};

const mapDispatchToProps = {
    resetDownload
}

export default connect(mapStateToProps, mapDispatchToProps)(DownloadManager);