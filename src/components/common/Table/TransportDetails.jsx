import React from "react";
import { Grid, Typography, makeStyles, Divider } from "@material-ui/core"
import moment from "moment-timezone";
import { calcPercentage } from "helpers";
import { get } from "lodash";
import { tableStyles } from 'theme';

const useStyles = makeStyles(tableStyles);

export const columns = {
    columnOrder: [
        { title: 'Equipment Type', field: 'equipmentType', emptyValue: "-" },
        { title: 'Trailer Status', field: 'trailerLoadingStatus', emptyValue: "-" },
        { title: '% Of Trailer Loaded', field: 'estimatedLoadTimeMS' },
        { title: 'Business Unit', field: 'orderBusinessUnit', emptyValue: "-" },
        { title: 'Order Created in TMS', field: 'tmsOrderCreateDateTime' },
        { title: 'Customer Notified', field: 'customerNotifiedDateTime' },
        { title: 'Delivery Appt Confirmed by Customer', field: 'deliveryApptConfirmedDateTimeList' }
    ],
    columnConfiguration: (d) => {
        const e = d.estimatedLoadTimeMS;
        const r = d.remainingLoadTimeMS;
        const trailerStatus = calcPercentage(e, r);

        return {
            equipmentType: get(d, "equipmentType", "-"),
            trailerLoadingStatus: get(d, "trailerLoadingStatus", "-"),
            itemDesc: get(d, "itemDesc", "-"),
            estimatedLoadTimeMS: trailerStatus || "-",
            orderBusinessUnit: get(d, "orderBusinessUnit", "-"),
            tmsOrderCreateDateTime: d?.tmsOrderCreateDateTime ? moment(d.tmsOrderCreateDateTime).tz("US/Central").format(' MMM DD YYYY HH:mm z') : " -",
            customerNotifiedDateTime: d?.customerNotifiedDateTime?.map(d1 => {
                return d1 ? moment(d1).tz("US/Central").format(' MMM DD YYYY HH:mm z') : "-"
            }).join(" | ") || "-",
            deliveryApptConfirmedDateTimeList: d?.deliveryApptConfirmedDateTimeList?.map(d1 => {
                return d1 ? moment(d1).tz("US/Central").format(' MMM DD YYYY HH:mm z') : "-"
            }).join(" | ") || "-"
        }
    }
}

const TransportDetails = ({ data }) => {

    const classes = useStyles();

    const e = data.estimatedLoadTimeMS;
    const r = data.remainingLoadTimeMS;
    const trailerStatus = calcPercentage(e, r);

    return (
        <div className={classes.outline} data-testid="transportdetails">
            <div className={classes.expand}>
                <Grid container className={classes.grid} spacing={6}>
                    <Grid item>
                        <Grid container justify="space-between" spacing={1}>
                            <Typography align="left" variant="h3" className={classes.text}>Equipment Type</Typography>
                            <Typography align="right" variant="subtitle1" className={classes.subtext}>{data.equipmentType || "-"}</Typography>
                        </Grid>
                    </Grid>
                    <Grid item>
                        <Grid container justify="space-between" spacing={1}>
                            <Typography align="left" variant="h3" className={classes.text}>Trailer Status</Typography>
                            <Typography align="right" variant="subtitle1" className={classes.subtext}>{data.trailerLoadingStatus || "-"}</Typography>
                        </Grid>
                    </Grid>
                    <Grid item>
                        <Grid container justify="space-between" spacing={1}>
                            <Typography align="left" variant="h3" className={classes.text}>% Of Trailer Loaded</Typography>
                            <Typography align="right" variant="subtitle1" className={classes.subtext}>{trailerStatus}</Typography>
                        </Grid>
                    </Grid>
                    <Grid item>
                        <Grid container justify="space-between" spacing={1}>
                            <Typography align="left" variant="h3" className={classes.text}>Business Unit</Typography>
                            <Typography align="right" variant="subtitle1" className={classes.subtext}>{data.orderBusinessUnit || "-"}</Typography>
                        </Grid>
                    </Grid>
                </Grid>
                <Divider style={{ backgroundColor: 'gray', margin: 5 }} />
                <Grid container className={classes.grid} spacing={6}>
                    <Grid item>
                        <Grid container justify="space-between" spacing={1}>
                            <Typography align="left" variant="h3" className={classes.text}>CUSTOMER APPOINTMENT TIME</Typography>
                            <Typography align="left" variant="subtitle1" className={classes.subtext}>
                                Order Created in TMS:
                                {
                                    data?.tmsOrderCreateDateTime ?
                                        moment(data.tmsOrderCreateDateTime).tz("US/Central").format(' MMM DD YYYY HH:mm z') : " -"
                                }
                                <br />
                                Customer Notified: {
                                    data?.customerNotifiedDateTime?.map(d => {
                                        return d ? moment(d).tz("US/Central").format(' MMM DD YYYY HH:mm z') : "-"
                                    }).join(" , ") || "-"
                                }
                                <br />
                                Delivery Appt Confirmed by Customer: {
                                    data?.deliveryApptConfirmedDateTimeList?.map(d => {
                                        return d ? moment(d).tz("US/Central").format(' MMM DD YYYY HH:mm z') : "-"
                                    }).join(" , ") || "-"
                                }
                                <br />
                            </Typography>
                        </Grid>
                    </Grid>
                </Grid>
            </div>
        </div>
    )
}

export default TransportDetails;