import React from 'react'
import { appTheme } from 'theme';
import { get } from "lodash";
import { ItemDetailsTable } from "components/common/ItemDetailsTable"

const rowStyle = (d) => {
    return {
        backgroundColor: appTheme.palette.secondary.dark,
        flexGrow: 1,
        whiteSpace: 'nowrap',
        borderBottom: 'red',
        borderLeft: ["OVERAGE", "SHORTAGE"].includes(d.qtyDiscrepancy) ? '5px solid #FF4081' : ''
    }
}


export const columns = {
    columnOrder: [
        { title: 'Sl.No. ', field: 'slno', emptyValue: "-" },
        { title: 'Material ID', field: 'materialNum', emptyValue: "-" },
        { title: 'Customer Material ID', field: 'custMaterialNum', emptyValue: "-" },
        { title: 'Material Description', field: 'itemDesc', emptyValue: "-" },
        {
            title: 'Ordered Qty', render: (d) => {
                if (d.orderedQty >= 0) {
                    return `${d.orderedQty} ${d.qtyUom}`
                } else {
                    return '-'
                }
            }, field: 'orderedQty', emptyValue: "-"
        },
        {
            title: 'Confirmed Qty', render: (d) => {
                if (d.confirmedQty >= 0) {
                    return `${d.confirmedQty} ${d.qtyUom}`
                } else {
                    return '-'
                }
            }, field: 'confirmedQty', emptyValue: "-"
        },
        {
            title: 'Load Qty', render: (d) => {
                if (d.loadedQty >= 0) {
                    return `${d.loadedQty} ${d.qtyUom}`
                } else {
                    return '-'
                }
            }, field: 'loadedQty', emptyValue: "-"
        },
        {
            title: 'Difference Qty', render: (d) => {
                if (!isNaN(d.differenceQty)) {
                    return `${d.differenceQty} ${d.qtyUom}`
                } else {
                    return '-'
                }
            }, field: 'differenceQty', emptyValue: "-"
        },
        { title: 'Delivery Block', field: "deliveryBlockCode", emptyValue: "-" },
        { title: 'MAD', field: "matAvailDate", emptyValue: "-" },
    ],
    columnConfiguration: (d) => {
        return {
            materialNum: get(d, "materialNum", "-"),
            custMaterialNum: get(d, "custMaterialNum", "-"),
            itemDesc: get(d, "itemDesc", "-"),
            orderedQty: d.orderedQty >= 0 ? `${d.orderedQty} ${d.qtyUom}`: '-',
            confirmedQty: d.confirmedQty >= 0 ? `${d.confirmedQty} ${d.qtyUom}`: '-',
            loadedQty: d.loadedQty >= 0 ? `${d.loadedQty} ${d.qtyUom}`: '-',
            differenceQty:  !isNaN(d.differenceQty) ? `${d.differenceQty} ${d.qtyUom}`: '-',
            deliveryBlockCode: get(d, "deliveryBlockCode", "-"),
            matAvailDate: get(d, "matAvailDate", "-"),
        }
    }
}

const ItemDetailsTableOutBound = ({ data: { orderStatusTableRowId } }) => {

    return (
        <ItemDetailsTable testId="itemdetailsoutbound" orderStatusTableRowId={orderStatusTableRowId} columns={columns.columnOrder} rowStyle={rowStyle} />
    )
}

export default ItemDetailsTableOutBound;

