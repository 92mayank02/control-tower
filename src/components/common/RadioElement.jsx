import React from 'react';
import { makeStyles} from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';
import clsx from 'clsx';
import { get } from "lodash";
import { saveFilters  } from "../../reduxLib/services";
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import { connect } from "react-redux";
import { filterStyles } from "theme";
// import FilterComponentContainer from "./Filters/FilterComponentContainer";
import KCRadio from './Elements/KCRadio';

const useStyles = makeStyles(filterStyles)

const RadioElement = (props) => {

    const { filters, filterKey, saveFilters, type, subtype } = props;
    
    const classes = useStyles();
    const filter = get(filters, `${filterKey}.data`, null)

    const [value, setValue] = React.useState(filter);
    React.useEffect(() => {
        setValue(filter);
    }, [filter])

    const handleChange = (e) => {
        setValue(e.target.value)
    }

    const resetValue = (val) => {
        if (val === value) {
            setValue(null);
        }
    }

    React.useEffect(() => {
        if (filters[filterKey]) {
            saveFilters({
                filter: {
                    [filterKey]: {
                        ...filters[filterKey],
                        data: value
                    }
                }, type, subtype
            })
        }
    }, [value]);

    return (
        // <FilterComponentContainer testId='radioElement' {...props}>
            <FormControl component="fieldset" data-testid="radioElement">
                <RadioGroup row aria-label="position" defaultValue="end" name="Radio1" value={value} onChange={handleChange}>
                    <FormControlLabel className={classes.contentText} color="primary" value="Y" onClick={() => resetValue("Y")} control={<KCRadio />}
                        label={<Typography color="primary" className={clsx(classes.button, classes.contentText)}>Yes</Typography>}
                    />
                    <FormControlLabel className={classes.contentText} color="primary" value="N" onClick={() => resetValue("N")} control={<KCRadio />}
                        label={<Typography color="primary" className={clsx(classes.button, classes.contentText)}>No</Typography>}

                    />
                </RadioGroup>
            </FormControl>
        // </FilterComponentContainer>
    )
}

const mapStatetoProps = (state, ownProps) => {
    const { subtype } = ownProps;

    return {
        filters: get(state, `options.filters.${subtype}`, {})
    }
};


const mapDispatchToProps = {
    saveFilters
};

export default connect(mapStatetoProps, mapDispatchToProps)(RadioElement);

