import React, { useState, useEffect } from 'react'
import MaterialTable from 'material-table'
import { Paper, useTheme, Grid, makeStyles, TablePagination } from "@material-ui/core";
import 'bootstrap-daterangepicker/daterangepicker.css';
import { get, intersection, isEmpty, omit } from "lodash";
import { connect, useDispatch } from "react-redux";
import isEqual from "react-fast-compare";
import { generateTablebody, generateDefaultData, getUpdatedColumns, createHyperLinkFilterObject } from "helpers";
import LinearProgress from '@material-ui/core/LinearProgress';
import { tableStyles } from 'theme';
import { stickyHeaderCell, stickyBodyCell } from '../../helpers/tableStyleOverride';
import {
    saveColumnState,
    getTableData,
    setTableBodyAction
} from "reduxLib/services";
import FilterView from './FilterView'
import TableActionsWrapper from './TableActionsWrapper';
import { OrderHealthTooltip } from "../common/OrderHealthTooltip"
import { chartDataIdentifierForAPI } from "../../components/pages/PageRouter"
import TablePaginationActions from "./Table/TablePaginationActions";
import { useUpdateEffect } from 'react-use';


const useStyles = makeStyles(tableStyles);
const useStylesPagination = makeStyles((theme) => ({
    spacer: {
        flex: "1 1 auto"
    }
}));

export const fixFirstColumn = (columns) => {

    let firstUnhiddenIndex = null
    const fixedColumnObject = columns.map((columnObject, index) => {

        firstUnhiddenIndex = !columnObject.hidden && (firstUnhiddenIndex === null || index <= firstUnhiddenIndex) ? index : firstUnhiddenIndex

        if ((firstUnhiddenIndex === index)) {
            return { ...columnObject, cellStyle: { ...columnObject.cellStyle, ...stickyBodyCell }, headerStyle: { ...columnObject.headerStyle, ...stickyHeaderCell } }
        } else {
            const cellStyle = omit(columnObject.cellStyle, Object.keys(stickyBodyCell))
            const headerStyle = omit(columnObject.headerStyle, Object.keys(stickyHeaderCell))
            return { ...columnObject, headerStyle, cellStyle }
        }
    });

    return fixedColumnObject;
}



const TableComponent = (props) => {


    let { title, tableName, excludeArray,
        prevPageDetails, activeSubTableTab,
        searchTextPlaceholder, type, setDetails, pageTitle,
        ItemDetailSection, defaultFilterArgs, columns, conditionalFilterReplacement,
        filterBody = {}, pageDetails, filterType, topDateFilter,
        fetchEndPoint, filters, tableStyleConfig, tableRef, blockOnTabChange,
        ...rest } = props

    const classes = useStyles();
    const dispatch = useDispatch();
    const paginationClasses = useStylesPagination();
    const theme = useTheme();

    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);

    const { globalFilterSelection, itemDetailsColumns } = rest;
    const { siteNums } = globalFilterSelection || {};

    const makeColumnHyperlink = (columnArray) => {
        return columnArray.map((column) => column.field === "expectedDeliveryDateTime" ? {
            ...column,
            render: rowData => {
                return (rowData.expectedDeliveryDateTime && rowData.expectedDeliveryDateTime !== "-") ? (
                    <div data-testid="4keta" className={classes.etabutton} onClick={() => (
                        setDetails(type, chartDataIdentifierForAPI.shipmentdetails, true, {
                            shipmentNum: rowData.shipmentNum,
                            navHistory: [{ id: intersection(Object.values(chartDataIdentifierForAPI), Object.keys(pageDetails))[0], label: pageTitle, type }].filter(({ id }) => id !== chartDataIdentifierForAPI.network)
                        }))}>
                        {rowData.expectedDeliveryDateTime}
                    </div>) : "-";
            }
        } : column)
    }

    const [columnOrder, setColumnOrder] = useState(fixFirstColumn(makeColumnHyperlink(props?.columnOrder)) || []);

    const [fetchAllData, setFetchAllData] = useState({});

    const [businessUnits, setBusinessUnits] = useState(rest.businessUnits)

    const [tablebody, setTableBody] = useState(isEmpty(props.tablebody) ? {
        ...defaultFilterArgs,
        businessUnits,
        [tableName === "distinbound" ? "destSites" : "sites"]: siteNums,
        customerOrSalesOffice: globalFilterSelection?.customerOrSalesOffice,
        //...conditionalFilterReplacement(generateTablebody(filters, siteNums))
    } : props.tablebody);

    const [hyperLinkfilters, setHyperLinkFilters] = useState(createHyperLinkFilterObject({ pageDetails, tableName, filterBody }).fullObject)

    React.useEffect(() => {
        setHyperLinkFilters(createHyperLinkFilterObject({ pageDetails, tableName, filterBody }).fullObject);
    }, [pageDetails.filterParams])

    React.useEffect(() => {
        setBusinessUnits(rest.businessUnits);
    }, [rest.businessUnits])

    useEffect(() => {

        let newfilters = generateTablebody(filters, siteNums);

        conditionalFilterReplacement(newfilters)

        const newbody = {
            ...tablebody,
            businessUnits,
            ...newfilters,
            customerOrSalesOffice: globalFilterSelection?.customerOrSalesOffice
        };

        let hyperfilters = generateTablebody(hyperLinkfilters);
        conditionalFilterReplacement(hyperfilters)

        hyperfilters = {
            ...newbody,
            ...hyperfilters
        }

        if (!isEqual(tablebody, newbody) && !(!isEmpty(pageDetails.filterParams) && !isEqual(newbody, hyperfilters) && !isEmpty(hyperLinkfilters)) && !blockOnTabChange) {
            setTableBody(newbody);
            dispatch(setTableBodyAction({ body: newbody, tableName }));
            setHyperLinkFilters({})
            setPage(0);
            dispatch(getTableData({ fetchEndPoint, tablebody: newbody, pageSize: rowsPerPage, page: 0 }));
        }

    }, [filters, businessUnits, globalFilterSelection]);

    const refreshTable = ({ pageSize, page }) => {
        dispatch(getTableData({ fetchEndPoint, tablebody, pageSize, page }));
    }

    useUpdateEffect(() => {
        if (!rest.loading) {
            refreshTable({ pageSize: rowsPerPage, page });
        }
    }, [rest.refresh])

    const saveFetchAllData = (totalRecords) => {
        setFetchAllData({
            config: {
                url: fetchEndPoint,
                body: tablebody,
                totalRecords: totalRecords || 0,
                columns: columnOrder,
            },
            type,
            subtype: tableName,
            process: columns.columnConfiguration,
            detailsColumns: itemDetailsColumns.columnOrder,
            detailsProcess: itemDetailsColumns.columnConfiguration
        });
    }

    const setColumnsCustom = (cols, order) => {
        setColumnOrder(fixFirstColumn(makeColumnHyperlink(cols)));
        if (order && tableRef?.current) {
            tableRef.current.dataManager.changeByDrag({
                combine: null,
                draggableId: "0",
                mode: "FLUID",
                reason: "DROP",
                source: { index: order?.source?.index || 0, droppableId: "headers" },
                destination: { index: order?.destination?.index || 0, droppableId: "headers" }
            });
        }
        dispatch(saveColumnState({ columns: cols, subtype: tableName }));
    }



    useEffect(() => {
        setColumnOrder(fixFirstColumn(makeColumnHyperlink(props.columnOrder)));
        if (tableRef.current) {
            tableRef.current.props.data.map((d, i) => {
                if (d.tableData.showDetailPanel) {
                    d.tableData.showDetailPanel = "";
                }
            })
        }
    }, [props.columnOrder])


    const handleChangePage = (event, newPage) => {
        setPage(newPage);
        refreshTable({ pageSize: rowsPerPage, page: newPage });
    };


    const handleChangeRowsPerPage = (event) => {
        const pageSize = parseInt(event.target.value, 10)
        setRowsPerPage(pageSize);
        setPage(0);
        refreshTable({ pageSize: pageSize, page: 0 });
    };

    const [tabledata, setTableData] = React.useState(rest.tabledata)

    React.useEffect(() => {
        const tempData = rest.tabledata?.orders?.map(data => {
            let d = {};
            Object.keys(data).map(item => {
                if (data[item] !== 0 && !data[item]) {
                    d[item] = "-";
                } else {
                    d[item] = data[item];
                }
            })
            return {
                ...generateDefaultData(columnOrder),
                ...d,
                ...columns.columnConfiguration(d)
            }
        }) || [];

        setTableData({ ...(rest.tabledata || {}), orders: tempData });
        saveFetchAllData(rest?.tabledata?.totalRecords || 0);
        if (tableRef && tableRef.current) {
            tableRef.current.dataManager.changePageSize(rowsPerPage || 10);
        }
    }, [rest.tabledata])

    const { orders, totalRecords } = tabledata;

    React.useEffect(() => {
        // Every time we unmount the component we have to reset tablebody from the redux state
        return () => dispatch(setTableBodyAction({ body: {}, tableName }));
    }, []);

    return (
        <Grid className={classes.root}>
            <div id="scrollIntoView" ref={props.setRef} />
            <TableActionsWrapper {...{
                title,
                topDateFilter,
                type,
                tableName,
                preSetStatusFilter: createHyperLinkFilterObject({ pageDetails, tableName, filterBody }).preSetStatusFilter,
                fetchAllData,
                columnOrder,
                columns,
                setColumnsCustom,
                filterType,
                excludeArray,
                searchTextPlaceholder,
                filterBody,
                refreshTable: () => refreshTable({ pageSize: rowsPerPage, page })
            }} />
            <FilterView hyperLinkfilters={hyperLinkfilters} prevPageDetails={prevPageDetails} activeSubTableTab={activeSubTableTab} pageDetails={pageDetails} type={type} tableName={tableName} defaultColumns={columns?.columnOrder} filterBody={filterBody}></FilterView>
            <Grid className={classes.table}>
                <MaterialTable
                    isLoading={rest.loading}
                    tableRef={tableRef}
                    components={{
                        Container: props_ => <Paper {...props_} elevation={0} />,
                        OverlayLoading: props_ => <LinearProgress />,
                        Action: props_ => (<OrderHealthTooltip tableName={tableName} {...props_} />),
                        Pagination: props_ => (<TablePagination
                            showFirstButton
                            showLastButton
                            rowsPerPageOptions={[5, 10, 20, 50, 100]}
                            component="div"
                            count={totalRecords || 0}
                            page={page}
                            onChangePage={handleChangePage}
                            rowsPerPage={rowsPerPage}
                            classes={paginationClasses}
                            onChangeRowsPerPage={handleChangeRowsPerPage}
                            ActionsComponent={TablePaginationActions}
                        />)
                    }}
                    options={tableStyleConfig}
                    style={{
                        backgroundColor: theme.palette.primary.base,
                        color: 'white',
                        borderBottom: '1px solid white',
                    }}
                    columns={columnOrder}
                    data={orders}
                    detailPanel={
                        [
                            rowData => ({
                                icon: () => <OrderHealthTooltip tableName={tableName} data={rowData} />,
                                openIcon: () => <OrderHealthTooltip tableName={tableName} data={rowData} />,
                                render: rowData_ => (<ItemDetailSection data={rowData_} />)
                            }),
                            {
                                render: rowData_ => (<ItemDetailSection data={rowData_} />)
                            },
                        ]
                    }
                />
            </Grid>
        </Grid>
    )
}

const mapStateToProps = (state, ownProps) => {
    const { tableName, columns } = ownProps;
    const updatedColumns = getUpdatedColumns(get(state, `user.currentView.${tableName}.columns`, columns?.columnOrder), columns?.columnOrder);
    return {
        columnOrder: get(state, `options.columns.${tableName}`, updatedColumns),
        tablebody: get(state, 'options.tablebody', {}),
        filters: get(state, `options.filters.${tableName}`, {}),
        businessUnits: get(state, "favorites.tempBusinessUnit", []),
        tabledata: get(state, "tabledata.data", {}),
        loading: get(state, 'tabledata.loading', true),
        blockOnTabChange: get(state, 'items.blockOnTabChange', false),
    };
};


export default connect(mapStateToProps, null)(TableComponent);
