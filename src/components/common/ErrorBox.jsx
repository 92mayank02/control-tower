import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ResponsiveWrapper from "../charts/ResponsiveWrapper";
import { Typography } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    skeleton: {
        backgroundColor: theme.palette.primary.dark,
        padding: theme.spacing(2)
    },
    loader: {
    }
}));

const ErrorBox = ({ message = "Something Went Wrong", dimensions, parentWidth }) => {
    const classes = useStyles();
    return (
        <span data-testid='errorBox' className={classes.loader} style={{ height: dimensions.height + 6 }}>
            <Typography color="error">
                Something Went Wrong
          </Typography>
        </span>
    );
}

export default ResponsiveWrapper(ErrorBox);
