import React from "react";
import PieChart from "components/D3Charts/PieChart";

export const SuspendedDetailsChartBuilder= (props) => {
 
    const { margins, height, chartsData } = props
    const xKey = "stateDesc";
    const barData = chartsData ? chartsData.map(d => {
        return {
            ...d,
            name: d[xKey],
            value: d.totalCount,
            blueCount: d.greenCount
        }
    }) : [];
    return (
        <div data-testid="transportSuspendedStatus" >
            <PieChart center="Loads"  footer="&nbsp;" data={barData} xKey="name" margins={margins} height={height} legend />
        </div>
    );
}
