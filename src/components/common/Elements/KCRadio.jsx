import React from 'react';
import { withStyles, Radio } from '@material-ui/core';

export default withStyles({
    root: {
        color: 'white',
        '&$checked': {
            color: "white",
        },
    },
    checked: {},
})((props) => <Radio color="default" {...props} />);
