import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Checkbox from '@material-ui/core/Checkbox';
import { appTheme } from "theme";


const KCCheckBox = withStyles({
    root: {
        color: appTheme?.palette.white,
        '&$checked': {
            color: appTheme?.palette.white,
        },
    },
    checked: {},
})((props) => <Checkbox color="default" {...props} />);

export default KCCheckBox;