import React from "react";
import { Grid, Button, makeStyles, Typography } from "@material-ui/core";
import { viewStyles } from "theme";
import ExpandLess from '@material-ui/icons/ExpandLessRounded';
import ExpandMore from '@material-ui/icons/ExpandMoreRounded';

const useStyles = makeStyles(viewStyles);

export default ({ expand, change }) => {
  const classes = useStyles();
  return (
    <Grid container justify="center">
      <Button data-testid='expandbutton' className={classes.expand} onClick={() => change(!expand)}>
        {
          !expand ? <ExpandMore className={classes.icon} /> : <ExpandLess className={classes.icon} />
        }
        <Typography variant="h3" style={{ pointerEvents: 'none' }}>View {!expand ? 'More' : 'Less'}</Typography>
      </Button>
    </Grid>

  )
}