import React from 'react';
import { get } from "lodash";
import { appTheme } from 'theme';
import { ItemDetailsTable } from "components/common/ItemDetailsTable"

const rowStyle = (d) => {
    return {
        backgroundColor: appTheme.palette.secondary.dark,
        flexGrow: 1,
        whiteSpace: 'nowrap',
        borderBottom: 'red',
        borderLeft: d.orderExecutionHealth === "RED" ? '5px solid #FF4081' : ''
    }
}

export const columns = {
    columnOrder: [
        { title: 'SL.No. ', field: 'slno', emptyValue: "-" },
        { title: 'MATERIAL ID', field: 'materialNum', emptyValue: "-" },
        { title: 'MATERIAL DESCRIPTION', field: 'itemDesc', emptyValue: "-" },
        {
            title: 'LOAD QTY', render: (d) => {
                if (d.loadedQty >= 0) {
                    return `${d.loadedQty} ${d.qtyUom}`
                } else {
                    return '-'
                }
            }, field: 'loadedQty'
        },
    ],
    columnConfiguration: (d) => {
        return {
            materialNum: get(d, "materialNum", "-"),
            itemDesc: get(d, "itemDesc", "-"),
            loadedQty: d.loadedQty >= 0 ? `${d.loadedQty} ${d.qtyUom}` : '-'
        }
    }
}

const ItemDetailsTableInBound = ({ data: { orderStatusTableRowId } }) => {
    return (
        <ItemDetailsTable testId="itemdetailsinbound" orderStatusTableRowId={orderStatusTableRowId} columns={columns.columnOrder} rowStyle={rowStyle} />
    )
}

export default ItemDetailsTableInBound;
