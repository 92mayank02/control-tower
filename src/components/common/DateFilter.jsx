import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import { connect } from "react-redux";
import { get } from "lodash";
import { filterStyles } from "theme";
import { saveFilters as saveFiltersService } from "../../reduxLib/services";
import TextField from '@material-ui/core/TextField';
import moment from "moment-timezone";
// import FilterComponentContainer from "./Filters/FilterComponentContainer";
import { useDeepCompareEffect } from 'react-use';

const useStyles = makeStyles(filterStyles);

const CssTextField = withStyles(theme => ({
    root: {
        width: '100%',
        padding: 0,
        marginBottom: theme.spacing(2),
        "& label": {
            color: theme.palette.primary.contrastText
        },
        '& label.Mui-focused': {
            color: theme.palette.primary.contrastText,
        },
        '& .MuiInput-underline:after': {
            borderBottomColor: theme.palette.primary.contrastText,
        }
    },
}))(TextField);


const DateFilter = (props) => {

    const { filters, filterKey, saveFilters, type, subtype, disableStartDate } = props;

    const filter = get(filters, `${filterKey}.data`, { startTime: "", endTime: "" })

    const [datetime, setDatetime] = React.useState(filter || {});

    const changeDatetime = (value, dateType) => {
       let date = moment(value, "YYYY-MM-DD").format(`YYYY-MM-DD`);
        if (filters[filterKey].dummytime) {
            date = moment(value, "YYYY-MM-DD").format(`YYYY-MM-DD ${filters[filterKey][dateType]}`);
        }
        setDatetime({ ...datetime, [dateType]: date });
    }

    React.useEffect(() => {
        if (datetime?.startTime && datetime?.endTime) {
            saveFilters({
                filter: {
                    [filterKey]: {
                        ...filters[filterKey],
                        data: datetime || null
                    }
                }, type, subtype
            });
        }
    }, [datetime?.startTime, datetime?.endTime])

    useDeepCompareEffect(() => {
        setDatetime(filter || { startTime: "", endTime: "" });
    }, [filters?.[filters]?.data]);

    return (
        // <FilterComponentContainer testId='dateFilterElement' {...props}>
        <div data-testid="dateFilterElement" style={{display: disableStartDate ? "flex" : ""}}>
            <CssTextField
                label="Start Date"
                InputLabelProps={{
                    shrink: true,
                }}
                value={datetime?.startTime ? moment(datetime?.startTime).format('YYYY-MM-DD') : ""}
                type="date"
                variant="outlined"
                onChange={(e) => changeDatetime(e.target.value, "startTime")}
                inputProps={{ "aria-label": "startdate" }}
                disabled={disableStartDate ? true : false}
                style={disableStartDate ? {"paddingRight": 10}: null }
            />
            <CssTextField
                label="End Date"
                InputLabelProps={{
                    shrink: true,
                }}
                value={datetime?.endTime ? moment(datetime.endTime).format('YYYY-MM-DD') : ""}
                type="date"
                onChange={(e) => changeDatetime(e.target.value, "endTime")}
                variant="outlined"
                inputProps={{ 
                    "aria-label": "enddate",
                    min: disableStartDate ? moment().add(2, "days").format("YYYY-MM-DD") : ""
                }}
                

            />
        {/* </FilterComponentContainer> */}
        </div>
    )
}

const mapStatetoProps = (state, ownProps) => {
    const { subtype } = ownProps;
    return {
        filters: get(state, `options.filters.${subtype}`, {})
    }
};

const mapDispatchToProps = {
    saveFilters: saveFiltersService
};

export default connect(mapStatetoProps, mapDispatchToProps)(DateFilter);
