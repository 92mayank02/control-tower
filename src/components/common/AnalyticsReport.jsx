import React from 'react';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  powerBiReport: {
    height: `${theme.spacing(12.25)}vh`,
    width: '100%',
    marginTop: theme.spacing(4),
    padding: `0px ${theme.spacing(1)}px`
  }
}));

const AnalyticsReport = ({pageDetails: {filterParams : {reportURL}}}) => {
  const classes = useStyles();
  
  return (
    <iframe data-testid="powerBiReport"
      className={classes.powerBiReport}
      src={reportURL}
      frameborder="0"
      allowFullScreen="true"></iframe>
  );
};

export default AnalyticsReport;
