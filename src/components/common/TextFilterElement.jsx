import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import { connect } from "react-redux";
import { get } from "lodash";
import { filterStyles } from "theme";
import { saveFilters } from "../../reduxLib/services";
import TextField from '@material-ui/core/TextField';
// import FilterComponentContainer from "./Filters/FilterComponentContainer";
import { stringToArray } from 'helpers';

const useStyles = makeStyles(filterStyles);

const CssTextField = withStyles(theme => ({
    root: {
        width: '100%',
        "& label": {
            color: theme.palette.primary.contrastText
        },
        '& label.Mui-focused': {
            color: theme.palette.primary.contrastText,
        },
        '& .MuiInput-underline:after': {
            borderBottomColor: theme.palette.primary.contrastText,
        }
    },
}))(TextField);

const TextFilterElement = (props) => {

    const { title, filters, filterKey, saveFilters, type, subtype, placeholder, filter: filterBase } = props;

    const filter = get(filters, `${filterKey}.data`, null)
    const [input, setInput] = React.useState(filter);

    const submitChange = (e) => {
        e.preventDefault();
        setInput(filterBase?.stringToArray ? stringToArray(input): input?.trim())
        saveFilters({
            filter: {
                [filterKey]: {
                    ...filters[filterKey],
                    data: filterBase?.stringToArray ? stringToArray(input): input?.trim() || null
                }
            }, type, subtype
        });
    }


    React.useEffect(() => {
        setInput(filter);
    }, [filter])

    return (
        // <FilterComponentContainer testId="textFilterElement" {...props}>
            <form onSubmit={submitChange} data-testid="textFilterElement">
                <CssTextField
                    value={input || ""}
                    title="textbox"
                    inputProps={{ "data-testid": "textfilter" }}
                    onChange={e => setInput(e.target.value)}
                    label={placeholder || `Enter ${title.toLowerCase()}`}
                    variant="outlined"
                />
            </form>
        // </FilterComponentContainer>
    )
}

const mapStatetoProps = (state, ownProps) => {
    const { subtype } = ownProps;
    return {
        filters: get(state, `options.filters.${subtype}`, {})
    }
};

const mapDispatchToProps = {
    saveFilters
};

export default connect(mapStatetoProps, mapDispatchToProps)(TextFilterElement);
