import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles, Grid, AppBar, Tabs, Tab, Typography, Box } from '@material-ui/core';
import distinboundFilters from '../../reduxLib/constdata/distinboundFilters';
import distoutboundFilters from '../../reduxLib/constdata/distoutboundFilters';
import { defaultInboundFilterArgs, defaultOutboundFilterArgs } from "../../reduxLib/constdata/filters"
import { defaultInboundColumns } from "../../reduxLib/constdata/distinboundColumns";
import { defaultOutboundColumns } from "../../reduxLib/constdata/distoutboundColumns";
import { tableoptions } from '../../helpers/tableStyleOverride';
import { endpoints } from "helpers";
import { get } from "lodash";
import TableComponent from "../common/TableComponent";
import ItemDetailsTableOutBound, { columns as outBoundDetailsColumns } from "../common/ItemDetailsTableOutBound";
import ItemDetailsTableInBound, { columns as inBoundDetailsColumns } from "../common/ItemDetailsTableInBound";
import { viewStyles } from "theme"

export const tabTitleEnum = {
    outbound: 'DIST_OUTBOUND_SHIPMENTS',
    inbound: 'DIST_INBOUND_SHIPMENTS',
}

function TabPanel(props) {
    const { children, value, index, pageDetails, ...other } = props;
    return (
        <Grid
            className={useStyles().tabpanel}
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-auto-tabpanel-${index}`}
            aria-labelledby={`scrollable-auto-tab-${index}`}
            {...other}
        >
            {((value === index && !pageDetails?.filterParams?.state) || (value === index && pageDetails?.filterParams?.state === index) || (value === index && pageDetails?.filterParams?.state === 'DIST')) && (
                <Box p={3}>
                    {children}
                </Box>
            )}
        </Grid>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `scrollable-auto-tab-${index}`,
        'aria-controls': `scrollable-auto-tabpanel-${index}`,
    };
}

const useStyles2 = makeStyles(viewStyles);

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        width: '100%',
        backgroundColor: theme.palette.primary.base,
    },
    appbar: {
        background: theme.palette.primary.base,
        borderBottom: '1px solid #279CFF',
    },
    tab: {
        width: 10,
        [theme.breakpoints.down("sm")]: {
            flexBasis: "50%"
        }
    },
    tabpanel: {
        padding: `${theme.spacing(2)} 0px`
    },
    tabName: {
        fontFamily: 'Gilroy',
        fontSize: 16,
        fontWeight: 600,
        textAlign: "center",
        cursor: 'pointer',
        textTransform: 'capitalize'
    }
}));

export const inboundConditionalFilterReplacement = (newfilters) => {
    if (get(newfilters, "inboundOrderExecutionBucket", []).length === 0) {
        newfilters.inboundOrderExecutionBucket = defaultInboundFilterArgs.inboundOrderExecutionBucket;
    }
}

export const outboundConditionalFilterReplacement = (newfilters) => {
    if (get(newfilters, "orderExecutionBucket", []).length === 0) {
        newfilters.orderExecutionBucket = defaultOutboundFilterArgs.orderExecutionBucket;
    }
}

export const tableStyleConfigOutbound = {
    ...tableoptions,
    rowStyle: (d) => {
        const healthStatusBucket = d.orderExecutionHealth ? d.orderExecutionHealth : "";
        return {
            ...tableoptions.rowStyle,
            borderLeft: healthStatusBucket === "RED" ? '5px solid #FF4081' : healthStatusBucket === "YELLOW" ? '5px solid #F5B023' : ''
        }
    },
}

export const tableStyleConfigInbound = {
    ...tableoptions,
    rowStyle: (d) => {
        const healthStatusBucket = d.inboundOrderExecutionHealth ? d.inboundOrderExecutionHealth : "";
        return {
            ...tableoptions.rowStyle,
            borderLeft: healthStatusBucket === "RED" ? '5px solid #FF4081' : healthStatusBucket === "YELLOW" ? '5px solid #F5B023' : ''
        }
    },
}

const DistributionTabs = ({ pageTitle, setDetails, siteNums, type, pageDetails, setActiveTopNavTab, prevPageDetails, ...rest }) => {
    const classes = useStyles();

    const [value, setValue] = React.useState((prevPageDetails?.secondaryActiveTab === tabTitleEnum.outbound || !prevPageDetails?.secondaryActiveTab) ? tabTitleEnum.outbound : tabTitleEnum.inbound);
    const tableRef = React.createRef();

    const handleChange = (event, newValue) => {
        setDetails(type, 'DIST', true)
        setActiveTopNavTab({ ...prevPageDetails, secondaryActiveTab: newValue })
        setValue(newValue);
    };

    useEffect(() => {
        if (pageDetails.filterParams.state === tabTitleEnum.outbound) {
            setValue(pageDetails.filterParams.state)
        } else if (pageDetails.filterParams.state === tabTitleEnum.inbound) {
            setValue(pageDetails.filterParams.state)
        }
    }, [pageDetails.filterParams.state])

    return (
        <div className={classes.root} data-testid="distributiontabs">
            <AppBar position="static" className={classes.appbar} elevation={0}>
                <Tabs
                    value={value}
                    onChange={handleChange}
                    indicatorColor="secondary"
                    textColor="primary"
                >
                    <Tab className={classes.tab} selectionFollowsFocus value={tabTitleEnum.outbound}
                        label={<Typography className={classes.tabName} variant="h2" >Outbound</Typography>}
                        {...a11yProps(0)} />
                    <Tab className={classes.tab} selectionFollowsFocus value={tabTitleEnum.inbound}
                        label={<Typography className={classes.tabName} variant="h2" >Inbound</Typography>}
                        {...a11yProps(1)} />
                </Tabs>
            </AppBar>
            <TabPanel value={value} index={tabTitleEnum.outbound} pageDetails={pageDetails}>
                <TableComponent {...rest}
                    pageTitle={pageTitle}
                    setDetails={setDetails}
                    prevPageDetails={prevPageDetails}
                    activeSubTableTab={value}
                    pageDetails={pageDetails}
                    siteNums={siteNums}
                    tableName="distoutbound"
                    excludeArray={["searchStringList"]}
                    searchTextPlaceholder="Enter Shipment #, Order#, Delivery #"
                    type={type}
                    tableRef={tableRef}
                    ItemDetailSection={ItemDetailsTableOutBound}
                    itemDetailsColumns={outBoundDetailsColumns}
                    defaultFilterArgs={defaultOutboundFilterArgs}
                    columns={defaultOutboundColumns}
                    conditionalFilterReplacement={outboundConditionalFilterReplacement}
                    filterBody={distoutboundFilters}
                    filterType="distoutboundfilters"
                    fetchEndPoint={endpoints.table.distributionOutbound}
                    tableStyleConfig={tableStyleConfigOutbound}
                />
            </TabPanel>
            <TabPanel value={value} index={tabTitleEnum.inbound} pageDetails={pageDetails}>
                <TableComponent {...rest}
                    pageTitle={pageTitle}
                    setDetails={setDetails}
                    prevPageDetails={prevPageDetails}
                    activeSubTableTab={value}
                    pageDetails={pageDetails}
                    siteNums={siteNums}
                    tableName="distinbound"
                    excludeArray={["searchStringList"]}
                    searchTextPlaceholder="Enter Shipment #, Order#, Trailer id, Delivery#"
                    type={type}
                    tableRef={tableRef}
                    ItemDetailSection={ItemDetailsTableInBound}
                    itemDetailsColumns={inBoundDetailsColumns}
                    defaultFilterArgs={defaultInboundFilterArgs}
                    columns={defaultInboundColumns}
                    conditionalFilterReplacement={inboundConditionalFilterReplacement}
                    filterBody={distinboundFilters}
                    filterType="distinboundfilters"
                    fetchEndPoint={endpoints.table.distributionInbound}
                    tableStyleConfig={tableStyleConfigInbound}
                />
            </TabPanel>
        </div>
    );
}



export default DistributionTabs