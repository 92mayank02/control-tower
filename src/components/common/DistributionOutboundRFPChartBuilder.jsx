import React from "react";
import { Grid, makeStyles } from '@material-ui/core';
import BarChart from "components/D3Charts/BarChart";
import { getChartSegmentTotal } from "./ChartComponent";
import {chartElementStyles} from "../../theme"
import { formatColumnData, rfpColumns } from "../pages/DistributionView";

const useStyles = makeStyles(chartElementStyles)

export const DistributionOutboundRFPChartBuilder = (props) => {

  // React.useEffect(() => {
  //   rest.setRefresh({
  //     refresh: !rest.refresh,
  //     siteNums,
  //     time: new Date()
  //   })
  // }, []);

  const { margins, height, type, subtype, chartsData, keys, setDetails, setHeaderTotalCount } = props

  const data = formatColumnData(chartsData, rfpColumns, "TRANS_EXEC_READY_PICK_UP")

  setHeaderTotalCount(getChartSegmentTotal({keys,data}))

  return (
      <Grid container data-testid="distributionOutbound">
        <Grid item xs={12}><BarChart  wrapit keys={keys} data={data} xKey="name" margins={margins} height={height} type={type} subtype={subtype} setDetails={setDetails} /></Grid>
      </Grid>
  );
}
