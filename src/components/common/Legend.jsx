import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  bluesquare: {
    height: 10,
    width: 10,
    backgroundColor: theme.palette?.chartColors?.healthy,
    display: 'inline-block'
  },
  redsquare: {
    height: 10,
    width: 10,
    backgroundColor: theme.palette?.chartColors?.unhealthy,
    display: 'inline-block'
  },
  yellowsquare: {
    height: 10,
    width: 10,
    backgroundColor: theme.palette?.chartColors?.risk,
    display: 'inline-block'
  },
  legend: {
    padding: theme.spacing(1),
    display: 'block',
    width: '150%'
  },
  textContent: {
    paddingRight: theme.spacing(1),
    paddingLeft: theme.spacing(1),
    display: 'inline-block'
  },
  purplesquare: {
    height: 10,
    width: 10,
    backgroundColor: theme.palette.guardrailColors?.consumer,
    display: 'inline-block'
  },
  pinksquare: {
    height: 10,
    width: 10,
    backgroundColor: theme.palette.guardrailColors?.professional,
    display: 'inline-block'
  },
  greensquare: {
    height: 10,
    width: 10,
    backgroundColor: theme.palette.guardrailColors?.STO,
    display: 'inline-block'
  },
  reddots: {
    fontSize: theme.spacing(2),
    color: theme.palette.legendColors?.pink,
    display: 'inline-block',
    '&::after' : {
      content: `"\\2026"`
    },
  },
  bluedots: {
    fontSize: theme.spacing(2),    
    color: theme.palette.chartColors?.healthy,
    display: 'inline-block',
    '&::after' : {
      content: `"\\2026"`
    },
  }
}));

const Legend = ({ isGuardrail }) => {
  const classes = useStyles();

  return (
    <>
      {isGuardrail ? (
        <span data-testid="gurdrailLegend" color="primary">
          <div className={classes.legend}>
            <div className={classes.purplesquare} />
            <div className={classes.textContent}>Consumer</div>
            <div className={classes.pinksquare} />
            <div className={classes.textContent}>Professional</div>
            <div className={classes.greensquare} />
            <div className={classes.textContent}>STO</div>
            <div className={classes.reddots} />
            <div className={classes.textContent}>Above Guard Rail</div>
            <div className={classes.bluedots} />
            <div className={classes.textContent}>Below Guard Rail</div>
          </div>
        </span>
      ) : (
        <span data-testid="legend" color="primary">
          <div className={classes.legend}>
            <div className={classes.bluesquare} />
            <div className={classes.textContent}>Healthy</div>
            <div className={classes.yellowsquare} />
            <div className={classes.textContent}>Needs Attention</div>
            <div className={classes.redsquare} />
            <div className={classes.textContent}>Unhealthy</div>
          </div>
        </span>
      )}
    </>
  );
};

export default Legend;
