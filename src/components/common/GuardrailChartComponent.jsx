import React, { useRef, useLayoutEffect, useState, useEffect } from "react";
import { makeStyles, Typography, Grid } from '@material-ui/core';
import { connect } from 'react-redux';
import { getGuardrailsService } from "../../reduxLib/services/getChartsService";
import { get, isEmpty } from 'lodash';
import RefreshIcon from '@material-ui/icons/Refresh';
import Card from "./Card"
import ErrorBox from "./ErrorBox";
import { chartElementStyles } from "../../theme"
import SkeletonLoader from "./SkeletonLoader";
import Legend from "./Legend";

const useStyles = makeStyles(chartElementStyles)

export const GuardrailChartComponent = ({ businessUnits, sitesKey, showLegends,
  showViewDetails, innerCard, divider,
  BuilderComponent, globalFilterSelection = {}, params, name, type, dummy, guardrailData,
  guardrailLoading, getGuardrailsService, charttype = "vertical", ...rest }) => {
  const classes = useStyles();
  const { setDetails, height, chartConfig } = rest;
  const margins = { ...rest.margins, top: 20, left: 50 };
  const horizontalMargins = { ...rest.margins, left: 100 }

  const targetRef = useRef();
  const [dimensions, setDimensions] = useState({ width: 0, height: 0 });
  const [unit, setUnit] = useState(guardrailData?.unit);
  const [viewDetails, setviewDetails] = useState(false);
  const [chartData, setChartData] = React.useState(guardrailData?.weekdays)
  const body = {}

  if (!isEmpty(globalFilterSelection?.siteNums)) {
    body[sitesKey ? sitesKey : "originSites"] = globalFilterSelection.siteNums
  }

  if (!isEmpty(globalFilterSelection?.customerOrSalesOffice)) {
    body["customerOrSalesOffice"] = globalFilterSelection.customerOrSalesOffice
  }

  const getCharts = () => {
    getGuardrailsService({
      body: {
        siteNum: type
      }
    });
  }

  const setHeaderUnit = () => {
    setUnit(unit)
  }

  useLayoutEffect(() => {
    if (targetRef.current) {
      if (targetRef.current) {
        setDimensions({
          width: targetRef.current.offsetWidth - 100,
          height: targetRef.current.offsetHeight - 100
        });
      }
    }
  }, []);

  useEffect(() => {
    if (!rest.blockApi) { getCharts() }
  }, [rest.refresh, businessUnits]);

  useEffect(() => {
    setUnit(guardrailData.unit);
    setChartData(guardrailData?.weekdays);
  }, [guardrailData]);


  const ActionItems = () => {
    return (
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="baseline"
      >
        <Grid item >
          <Typography className={classes.link} variant="subtitle1" color="primary" onClick={() =>
            setviewDetails(!viewDetails)
          }>
            {viewDetails ? "View Less" : "View More"}
          </Typography>
        </Grid>

        <Grid item >
          {!isEmpty(guardrailData) && <Typography variant='subtitle1' className={classes.label} >
            TIME ZONE: {guardrailData.siteTimezone}
          </Typography>}
        </Grid>
      </Grid>
    )
  }

  const Title = () => {
    return (
      <Grid data-testid='guardrailTitle'
        container
        justify="space-between"
      >
        <Grid item>
          <Grid><Typography variant="h3">{name}</Typography></Grid>
          {!isEmpty(guardrailData) && <Typography variant='subtitle1' className={classes.label} >In {unit}</Typography>}
        </Grid>
        <Grid item className={classes.refreshIcon}>
        <RefreshIcon className={guardrailLoading ? classes.spining : classes.spin} onClick={() => getCharts()} />
        </Grid>
      </Grid>
    )
  }

  return (
    <Grid ref={targetRef} className={classes.root} style={{ height: 'fit-content', minHeight: 336 }}>
      <Card
        divider
        innercard
        cardtitle={<Title />}
        cardactions={(!guardrailLoading && !isEmpty(guardrailData) &&
          <ActionItems loading={guardrailLoading} dimensions={dimensions} />
        )}
      >
        {
          guardrailLoading ? <SkeletonLoader style={{ height: "100%" }} dimensions={dimensions} /> :
          !guardrailData ? <ErrorBox message="Unable to Load" dimensions={dimensions} /> :
            <BuilderComponent {...{
              charttype,
              margins,
              horizontalMargins,
              height,
              type,
              guardrailData: chartData,
              dimensions,
              setDetails,
              setHeaderUnit,
              viewDetails,
              chartConfig
            }} />
        }
      </Card>
      {showLegends && <Legend isGuardrail={rest.guardrails} />}
    </Grid>
  );
}
const mapStateToProps = (state) => {
  return {
    businessUnits: get(state, "favorites.tempBusinessUnit", []),
    guardrailLoading: get(state.charts.loaders, "guardrails.loading", false),
    guardrailData: state.charts.charts.guardrails || []
  }
};

const mapDispatchToProps = {
  getGuardrailsService
}

export default connect(mapStateToProps, mapDispatchToProps)(GuardrailChartComponent);