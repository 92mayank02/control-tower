import React from "react";
import moment from "moment-timezone";


moment.fn.fromJustNow = function (a) {
    if (Math.abs(moment().diff(this)) < 15000) {
        return 'just now';
    }
    return this.fromNow(a);
}

const Timer = ({date}) => {

    const [timer, setTimer] = React.useState(moment(date).fromJustNow());
    React.useEffect(() => {
        const interval = setInterval(() => {
            setTimer(moment(date).fromJustNow())
          }, 30000);
          return () => clearInterval(interval);    
    }, [timer]);

    React.useEffect(() => {
        setTimer(moment(date).fromJustNow())
    }, [date])

    return (
        <span data-testid="timer">
            Last updated {timer}
        </span>
    )
}

export default Timer;