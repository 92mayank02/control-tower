import React from "react";
import { Snackbar } from "@material-ui/core"
import MuiAlert from '@material-ui/lab/Alert';


const Alert = (props) => {
    return <MuiAlert elevation={6} variant="filled" {...props} />
}

const Snack = ({ open, handleClose, severity, message }) => {
    return (
        <Snackbar data-testid='snackbar' open={open} autoHideDuration={3000} onClose={handleClose} TransitionComponent="SlideTransition" anchorOrigin={{ vertical: 'top', horizontal: 'center' }}>
            <Alert onClose={handleClose} severity={severity}>
                {message}
            </Alert>
        </Snackbar>
    )
}

export default Snack;