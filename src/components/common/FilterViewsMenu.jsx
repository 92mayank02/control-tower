import React, { useState, useEffect } from 'react';
import { connect } from "react-redux";
import { withStyles, Menu, MenuItem, ListItemText, IconButton, Typography, ListItemSecondaryAction, makeStyles, useMediaQuery, useTheme } from '@material-ui/core';
import { Favorite, FavoriteBorderOutlined, DeleteForeverOutlined } from "@material-ui/icons";
import { setFilterLayouts , 
  addNewFilterLayout , 
  setCurrentView , 
  setAsFavorite  } from "../../reduxLib/services";
import { get } from 'lodash';
import TextField from '@material-ui/core/TextField';
import KCButton from '../common/Button';
import FilterViewIcon from "../../assets/images/table-view.svg";
import { saveView, deleteView } from './FilterView'
import Snack from "./Helpers/Snack";

const CssTextField = withStyles(theme => ({
  root: {
    width: '100%',
    "& label": {
      color: theme.palette.primary.contrastText
    },
    '& label.Mui-focused': {
      color: theme.palette.primary.contrastText,
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: theme.palette.primary.contrastText,
    },
    "& .MuiOutlinedInput-input": {
      padding: theme.spacing(1.5)
    },
    "& .MuiInputLabel-outlined": {
      transform: "translate(14px, 14px) scale(1)"
    },
    "& .MuiInputLabel-outlined.MuiInputLabel-shrink": {
      transform: "translate(14px, -6px) scale(.75)"
    },
  },
}))(TextField);

const StyledMenu = withStyles((theme) => ({
  paper: {
    backgroundColor: theme.palette.secondary.base,
    "& li": {
      display: "block",
    }
  },
}))((props) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'center',
    }}
    {...props}
  />
));

const useStyles = makeStyles((theme) => ({
  menu: {
    display: "block",
    borderTop: `1px solid ${theme.palette.common.white}`
  },    
  label:{
    wordWrap:'break-word',
    paddingTop: theme.spacing(2)
  },
  button: {
    margin: `${theme.spacing(1.5)}px 0px`,
    background: theme.palette.secondary.base,
    border: `1px solid ${theme.palette.common.white}`,
    borderRadius: theme.spacing(1)
  }
}));

const StyledMenuItem = withStyles((theme) => ({
  root: {
    width: theme.spacing(40),
    '&:focus': {
      backgroundColor: theme.palette.secondary.base,
      '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
        color: theme.palette.common.white,
      },
    },
  },
}))(MenuItem);


const FilterViewsMenu = ({ type, views, tableName, setCurrentView, filterBody, filters, initialLayouts, setAsFavorite, currentView, setFilterLayouts, defaultColumns, defaultView }) => {

  const [anchorEl, setAnchorEl] = useState(null);
  const [viewName, setViewName] = useState("")

  const isMobile = useMediaQuery(useTheme().breakpoints.down('sm'));

  const classes = useStyles()

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const applyFilterView = (id) => {
    setCurrentView(views.filter(view => view.id === id)[0], tableName);
    setAnchorEl(null);

  }

  const markFavorite = (id) => {

    const actionObject = {
      ...initialLayouts, [(tableName).toUpperCase()]: {
        tableName: (tableName).toLowerCase(),
        views: initialLayouts[(tableName).toUpperCase()]?.views.map(view => id === view.id ? { ...view, default: !view.default } : { ...view, default: false })
      }
    }
    setAsFavorite({ actionObject })

  }

  const [snack, setSnack] = useState({
      open: false,
      severity: null,
      message: null
  })

  const handleSnackbarClose = (event, reason) => {
      if (reason === 'clickaway') {
          return;
      }
      setSnack({
          ...snack,
          open: false
      });
  };

  const showFilterViewNotifications = ({ notificationType, viewName: viewName1 }) => {

      const notificationTypeDescriptionMap = {
          save: `${viewName1} saved successfully`,
          add: `${viewName1 || "Untitled View"} created`,
          delete: `${viewName1} deleted`,
      }    
      setSnack({
          open: true,
          severity: "info",
          message: notificationTypeDescriptionMap[notificationType]
      });
  }
  useEffect(() => {
      setViewName(currentView?.name || "")
  }, [currentView])

  const viewLimit = process.env.REACT_APP_VIEWLIMIT

  return (
    <div data-testid="filterviewmenu" >
      <IconButton data-testid="filterviewmenubutton" onClick={handleClick} aria-label="Refrsh Table" color="primary">
        <img src={FilterViewIcon}></img>
      </IconButton>
      { !isMobile && <Typography variant='body2'>Views</Typography> }
      <StyledMenu
        id="customized-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >

        {(!views || views?.length <= viewLimit) &&
          <StyledMenuItem className={classes.menuitem} >
            <form onSubmit={((e) => {
              e.preventDefault()
              saveView({ showFilterViewNotifications, createNewView: true, tableName, views, viewName, filterBody, filters, currentView, initialLayouts, setFilterLayouts, defaultColumns });
            })} >
              <CssTextField
                name='viewname'
              
                inputProps={{ "data-testid": "savenewviewforminput" }}
                onChange={(e) => { setViewName(e.target.value) }}
                label="Enter a view name"
                variant="outlined"
              />
            </form>
            <Typography variant='body2' className={classes.label}>Creates a new view with default settings</Typography>
            <KCButton type="button" className={classes.button} onClick={() => {
              saveView({showFilterViewNotifications, createNewView: true, tableName, views, viewName, filterBody, filters, currentView, initialLayouts, setFilterLayouts, defaultColumns })
            }} >Add View</KCButton>
          </StyledMenuItem>
        }
        {views && views.map((view, index) => {
          return (<StyledMenuItem className={classes.root} key={view.id} onClick={() => { applyFilterView(view.id) }}>
            <ListItemText primary={view.name} />
            <ListItemSecondaryAction>
              <IconButton edge="end" data-testid="markfavourite" aria-label="FavUnfav" onClick={() => { markFavorite(view.id) }}>
                {view.default === true ? <Favorite></Favorite> : <FavoriteBorderOutlined />}
              </IconButton>
              <IconButton
                data-testId="deletecurrentview"
                onClick={() => {
                  deleteView({showFilterViewNotifications, viewId: view.id, tableName, filterBody, initialLayouts, setFilterLayouts, views, currentView, deleteCurrentView: view?.id === currentView?.id ? true : false })
                }} color="primary">
                  <DeleteForeverOutlined/>
                </IconButton>
            </ListItemSecondaryAction>
          </StyledMenuItem>)
        })}
      </StyledMenu>
      <Snack {...snack} handleClose={handleSnackbarClose} />
    </div>
  );
}

const mapStateToProps = (state, ownProps) => {
  const { tableName } = ownProps;
  return {
    views: get(state, `user.filterLayouts.${(tableName).toUpperCase()}.views`, []),
    filters: get(state, `options.filters.${tableName}`, {}),
    initialLayouts: get(state, `user.initialLayouts`, []),
    currentView: get(state, `user.currentView.${tableName}`) || {},
    defaultView: (get(state, `user.filterLayouts.${(tableName).toUpperCase()}.views`))?.filter(view => view.default === true)[0] || {}
  }
};
const mapDispatchToProps = {
  addNewFilterLayout,
  setFilterLayouts,
  setCurrentView,
  setAsFavorite
}

export default connect(mapStateToProps, mapDispatchToProps)(FilterViewsMenu);
