import React, {useState} from 'react'
import { Typography, Grid, makeStyles,TextField, withStyles } from '@material-ui/core';
import {useDispatch, useSelector} from 'react-redux'
import KCSelect from 'components/common/Select';
import { isEmpty } from 'lodash';
import { saveGuardrails } from 'reduxLib/services/getAllGuardRailsService';
import Button from 'components/common/Button';
import { Close as CloseIcon } from '@material-ui/icons';


const useStyles = makeStyles(theme =>({
    root:{
        width: "80%",
        background: theme.palette.primary.base,
        padding:theme.spacing(2.5),
        margin:`${theme.spacing(16)}px auto`,
        position:'relative',
        "&>div:nth-child(3)":{
            margin:`${theme.spacing(2)}px 0px`,
            background:theme.palette.card.base,
            borderRadius:theme.spacing(.5),
            width:"100%",
            padding:theme.spacing(2.5),
        }
    },
    tableHeader:{
        background:theme.palette.secondary.dark,
        borderBottom:`1px solid ${theme.palette.legendColors.greyLight}`,
        padding:`${theme.spacing(1.5)}px ${theme.spacing(1.5)}px`,

    },
    rowTitle:{
        padding:`${theme.spacing(2)}px ${theme.spacing(0.5)}px`,
    },
    tableContainer:{
        margin:`${theme.spacing(2.5)}px 0px`,
    },
    closeicon: {
        position: "absolute",
        right: theme.spacing(2),
        top: theme.spacing(1.5),
        cursor: "pointer",
        zIndex:10
    }
}))

const CssTextField = withStyles(theme => ({
    root: {
        width: '100%',
        "& label": {
            color: theme.palette.primary.contrastText
        },
        '& label.Mui-focused': {
            color: theme.palette.primary.contrastText,
        },
        '& .MuiInput-underline:after': {
            borderBottomColor: theme.palette.primary.contrastText,
        },
        "& input":{
            padding:theme.spacing(1.5),
            background:theme.palette.secondary.base
        },
        "& > div":{
            margin:`${theme.spacing(1.25)}px ${theme.spacing(0.5)}px`
        }
    },
}))(TextField);

const rowDataOrder = ['CONSUMER', 'PROFESSIONAL', 'ALL']
const weekdayIndexArray = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]

export const GuardrailDataDisplayGrid = ({data, title, propertyKey, updateGuardrails, allowedToEdit}) => {
    const classes = useStyles()
    return (
        <Grid container data-testid="guardraildatadisplaygrid" className={classes.tableContainer}>
            <Grid container item direction='column' justify='space-around' xs={5} >
                <Grid item className={classes.tableHeader}>
                    <Typography variant='h4'>{title}</Typography>    
                </Grid>
                <Grid item className={classes.rowTitle} >
                    <Typography variant='h4'>Consumer</Typography>    
                </Grid>
                <Grid item className={classes.rowTitle}>
                    <Typography variant='h4'>Professional</Typography>    
                </Grid>
                <Grid item className={classes.rowTitle}>
                    <Typography variant='h4'>STO</Typography>
                </Grid>
                <Grid className={classes.rowTitle}>
                    <Typography variant='h4'>Total</Typography>
                </Grid>
            </Grid>
            {!isEmpty(data) && data.weekdays.map((item)=>(
            <Grid container item direction='column' justify='space-around' xs={1}>
                <Grid item className={classes.tableHeader}>
                    <Typography variant='h4'>{item.day.toUpperCase().substr(0,3)}</Typography>
                </Grid>
                {
                    rowDataOrder.map(bu =>(
                        <Grid item>
                            {<CssTextField
                                value={item.orderBusinessUnit.filter(({businessUnit}) => businessUnit === bu)[0][propertyKey]}
                                title="textbox"
                                disabled={!allowedToEdit}
                                type='number'
                                inputProps={{ "data-testid": `textfilter_${bu}_${propertyKey}` }}
                                onChange={e => 
                                    updateGuardrails({
                                        ...item,
                                        orderBusinessUnit: item.orderBusinessUnit.map(obj => obj.businessUnit === bu ?{...obj, [propertyKey]: e.target.value} :obj)
                                    })
                                }
                                variant="outlined"
                            /> }
                        </Grid>
                    ))
                }
                <Grid item align='center'>
                    <Typography component='span' variant='h3'>{item.orderBusinessUnit.reduce((total,obj)=> total+parseInt(obj[propertyKey]),0)}</Typography> 
                </Grid>
            </Grid>
            ))}
        </Grid>
    )
}


export const guardrailsAllSitesandDays = ({siteNum,guardrailsData}) => {
    const siteGuardrailsArray = !isEmpty(guardrailsData.data) ? guardrailsData.data.filter((site)=> site.siteNum === siteNum):[]
    
    if(isEmpty(siteGuardrailsArray)){
        return {
            siteNum,
            unit:"LOADS",
            weekdays:weekdayIndexArray.map((value)=>({
                day:value,
                orderBusinessUnit:rowDataOrder.map((value)=>({
                    businessUnit:value,
                    guardrailFL:0,
                    guardrailLTL:0
                }))
            }))
        }

    }else{
        const siteGuardrailsData = siteGuardrailsArray[0]
        return {
            ...siteGuardrailsData,
            weekdays:weekdayIndexArray.map((value)=>{
                const findDay = siteGuardrailsData?.weekdays.filter(({day}) => day === value)[0]
                if(!findDay){
                    return {
                        day:value,
                        orderBusinessUnit:rowDataOrder.map((value)=>({
                            businessUnit:value,
                            guardrailFL:0,
                            guardrailLTL:0
                        }))
                    }
                }else{
                    return {
                        day:value,
                        orderBusinessUnit:rowDataOrder.map((value)=>({
                            businessUnit:value,
                            guardrailFL:findDay.orderBusinessUnit.filter(({businessUnit}) => value === businessUnit)[0]?.guardrailFL || 0,
                            guardrailLTL:findDay.orderBusinessUnit.filter(({businessUnit}) => value === businessUnit)[0]?.guardrailLTL || 0
                        }))
                    }

                }

            })
        }
    }


}

export default ({onClose}) => {
  
    const classes = useStyles()
    const dispatch = useDispatch()
    const guardrailsData = useSelector(({guardrails})=> guardrails.allSiteGuardrails)
    const locations = useSelector(({sites})=> sites.locations)


    const [selectedSite,setSelectedSite] = useState( locations[0].siteNum )  

    const [sortfilterDataBySite,setsortfilterDataBySite] = useState(guardrailsAllSitesandDays({siteNum:selectedSite,guardrailsData}))
    
    const handleSiteChange = (value) => {
        setSelectedSite(value)
        setsortfilterDataBySite(guardrailsAllSitesandDays({siteNum:value,guardrailsData}))
    }
            
    const handleUnitChange = (value) => setsortfilterDataBySite({...sortfilterDataBySite, unit:value})
    const updateGuardrails = (item) => {
        setsortfilterDataBySite({
            ...sortfilterDataBySite, 
            weekdays: [...sortfilterDataBySite.weekdays.filter(({day}) => day !== item.day), item].sort((item1, item2)=> weekdayIndexArray.indexOf(item1.day) - weekdayIndexArray.indexOf(item2.day))
        })        
    }

    const saveChanges = () => {
        const payloadObjectFrame = sortfilterDataBySite
        dispatch(saveGuardrails(payloadObjectFrame))
    }

    return (
        <Grid  data-testid="updateguardrails" className={classes.root} >
            <Typography component='span' variant='h3'>View / Edit Guardrails</Typography>
            <span title='closeicon' data-testid='closeicon' className={classes.closeicon} onClick={onClose}><CloseIcon /></span>
            <Grid container spacing='5'>
                <Grid item md={3}>
                    <Typography variant='h4'>Location</Typography>
                    <KCSelect classes={classes.selectOption} disableLabel='true' color="primary" value={selectedSite}
                    onChange={handleSiteChange}
                    options={locations.map(({siteNum, shortName})=> ({name:`${shortName} - ${siteNum}`, value:siteNum}))} />
                </Grid>
                <Grid item md={3}>
                    <Typography variant='h4'>UNIT</Typography>
                    <KCSelect classes={classes.selectOption} disableLabel='true' color="primary" value={sortfilterDataBySite.unit}
                    onChange={handleUnitChange}
                    options={[
                        { name: "LOADS", value: "LOADS" },
                        { name: "HOURS", value: "HOURS" },

                ]} />
                </Grid>
                <GuardrailDataDisplayGrid title="TRUCKLOAD GUARDRAILS" allowedToEdit={guardrailsData.allowedToEdit} propertyKey="guardrailFL" data={sortfilterDataBySite} updateGuardrails={updateGuardrails} ></GuardrailDataDisplayGrid>
                <GuardrailDataDisplayGrid title="LESS THAN TRUCKLOAD GUARDRAILS" allowedToEdit={guardrailsData.allowedToEdit} propertyKey="guardrailLTL" data={sortfilterDataBySite} updateGuardrails={updateGuardrails}></GuardrailDataDisplayGrid>
                <Grid item xs={12} align='center'>
                    <Button onClick={saveChanges} disabled={!guardrailsData.allowedToEdit} color='primary' variant='outlined'> Update Changes </Button>
                </Grid> 
            </Grid>
        </Grid>
    )
}