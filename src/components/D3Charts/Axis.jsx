import React from 'react'
import * as d3Axis from 'd3-axis'
import { select as d3Select, selectAll } from 'd3'

const wrap = (text, width, color) => {
  text.each(function (d, i) {
    const element = d3Select(this);
    const words = element.html(d).text();
    const transform = `translate(${-width / 2}, ${0})`;

    const foreign = document.createElementNS('http://www.w3.org/2000/svg', "foreignObject");
    foreign.setAttribute('width', width);
    foreign.setAttribute('height', 80);
    foreign.setAttribute("transform", transform)

    const iDiv = document.createElement('div');
    iDiv.setAttribute("class", "chartlabel")
    iDiv.setAttribute('style', `width: ${width}px`)
    const wordContent = document.createTextNode(words);
    const iText = document.createElement('p');
    iText.setAttribute("class", "chartlabeltext");
    iText.appendChild(wordContent)
    iDiv.appendChild(iText);
    foreign.appendChild(iDiv);
    d3Select(this).node().appendChild(foreign);
  });
}

const Axis = (props) => {
  const { wrapit, horizontal, diverging, divergingScales } = props;

  const axisElement = React.createRef();

  const renderAxis = (ref) => {
    const axisType = `axis${props.orient}`
    const axis = diverging ? (props.orient === "Left" ? divergingScales.divergingY : divergingScales.divergingX ) : d3Axis[axisType]()
      .scale(props.scale)
      // .tickSize(-props.tickSize)
      .tickPadding([3])
      .ticks([4])
    


    d3Select(ref.current).call(axis);
    if (wrapit && !horizontal && !diverging) {
      selectAll(ref.current.querySelectorAll('g')).call(wrap, props.scale.bandwidth() + 20, 'white');
    }else if( diverging && divergingScales.divergingY){
      selectAll(ref.current.querySelectorAll('.Axis-Left .tick text')).attr("text-anchor", "start")
      .attr("x", 6).attr("display","none")
    }

    
  }

  React.useEffect(() => {
    renderAxis(axisElement);
  }, [props.svgDimensions]);

  return (
    <g
      data-testid='d3axis'
      className={`Axis Axis-${props.orient}`}
      ref={axisElement}
      transform={props.translate}
    />
  )
}

export default Axis;