import React from 'react';
import { useTheme } from '@material-ui/core/styles';

const ReferenceBar = (props) => {
    const { margins, scales: { xScale, yScale }, data, svgDimensions, xKey, refLine, showRef } = props;

    const height = svgDimensions.height - margins.bottom;
    const theme = useTheme();

    const color = {
        blue: theme.palette.legendColors.pink,
        red: theme.palette.chartColors.healthy
    }
    return (
        <g data-testid='referencebars'>
            {
                data.map((d, i) => {
                    if (i === 0 && d.today !== "Y" && !showRef) return null;
                    const x = xScale(d[xKey]);
                    const y = yScale(d[refLine]);
                    const tHeight = height - yScale(d[refLine]);
                    const tWidth = xScale.bandwidth()
                    const fill = d[refLine] > d.total ? color.red : color.blue;
                    return (
                        <g>
                            <line
                                stroke={fill}
                                strokeDasharray={("3, 3")}
                                strokeWidth={2}
                                x1={x + tWidth}
                                y1={y}
                                x2={x + tWidth}
                                y2={y + tHeight}
                                d={1}
                            />
                            <line
                                stroke={fill}
                                strokeDasharray={("3, 3")}
                                strokeWidth={2}
                                x1={x}
                                y1={y}
                                x2={x + tWidth}
                                y2={y}
                                d={1}
                            />
                            <text
                                alignmentBaseline="middle"
                                dx={'-0.3em'}
                                fontSize={11}
                                fill="white"
                                x={x + tWidth + 10}
                                y={y - 20}
                            >
                                {Math.floor(d[refLine])}
                            </text>
                        </g>
                    )
                })
            }
        </g>
    )
}

export default ReferenceBar;