import React from 'react'
import { scaleBand, scaleLinear, scaleOrdinal, max, sum } from 'd3'
import { useTheme } from '@material-ui/core/styles';
import Axes from './Axes';
import Bars from './Bars';
import ReferenceBar from './ReferenceBar'
import ResponsiveWrapper from '../charts/ResponsiveWrapper'

const GuardRaliBarChart = ({ setDetails, wrapit, horizontal, header, keys, xKey, data, total, type, subtype, refLine, allkeys, ...props }) => {

    const theme = useTheme();

    let { margins, showRef } = props;
    margins = { ...margins, bottom: 50 }

    const svgDimensions = {
        width: props.parentWidth,
        height: props.height + 20 || 250
    }

    const colors = scaleOrdinal()
        .domain(allkeys)
        .range([theme.palette.guardrailColors.consumer, theme.palette.guardrailColors.professional, theme.palette.guardrailColors.STO]);

    let totalSum = 0;
    data.forEach((d) => {
        d.total = sum(keys, k => +d[k]);
        totalSum += d.total;
        return d
    });

    let yDomain = [0, 0];
    if (data?.length > 0) {        
        yDomain = [0, Math.max(max(data, d => sum(keys, k => +d[k])), max(data, d => sum([refLine], k => +d[k]))) + 10]
    }

    let xScale = scaleBand()
        .padding(0.5)
        .paddingInner(0.5)
        .domain(data.map(d => d[xKey]))
        .range([margins.left, svgDimensions.width - margins.right])

    let yScale = scaleLinear()
        .domain(yDomain)
        .nice()
        .range([svgDimensions.height - margins.bottom, margins.top]);


    const xAccessor = d => d[xKey];
    const centerPos = [svgDimensions.width / 2, svgDimensions.height / 2]

    const NoDataFound = ({ text }) => {
        return (
            <g transform={`translate(${centerPos})`}>
                <text textAnchor="middle" fill="white">No Data Found</text>
            </g>
        )
    }

    return (
        <span data-testid='gbarChart' style={{ textAlign: 'center' }}>
            <svg width={svgDimensions.width} height={svgDimensions.height}>
                {
                    !totalSum ? <NoDataFound /> :
                        <g>

                            <Bars
                                horizontal={horizontal}
                                scales={{ xScale, yScale }}
                                margins={margins}
                                xKey={xKey}
                                data={data}
                                xAccessor={xAccessor}
                                colors={colors}
                                keys={keys}
                                type={type}
                                subtype={subtype}
                                isLink={true}
                            />
                           <ReferenceBar
                                    horizontal={horizontal}
                                    scales={{ xScale, yScale }}
                                    margins={margins}
                                    xKey={xKey}
                                    data={data}
                                    xAccessor={xAccessor}
                                    colors={colors}
                                    keys={keys}
                                    type={type}
                                    subtype={subtype}
                                    svgDimensions={svgDimensions}
                                    refLine={refLine}
                                    showRef={showRef}
                                /> 
                            <Axes
                                horizontal={horizontal}
                                wrapit={wrapit}
                                scales={{ xScale, yScale }}
                                margins={margins}
                                svgDimensions={svgDimensions}
                            />
                        </g>}

            </svg>
        </span>
    )
}

export default ResponsiveWrapper(GuardRaliBarChart)
