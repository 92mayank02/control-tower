import React from 'react'
import { stack } from 'd3'
import { makeStyles, Grid} from '@material-ui/core';
import {tooltipLinkArgumentsMap} from '../../reduxLib/constdata/filters'
import D3Tooltip from "./D3Tooltip";
import clsx from "clsx";

const useStyles = makeStyles((theme) => ({
  text: {
    pointerEvents: 'none'
  },
  legend:{
    height:theme.spacing(1), 
    width:theme.spacing(1)
  },
  link:{
    cursor:'pointer',
    textDecoration:'underline',
    letterSpacing: '1px',
    padding : theme.spacing(.4),
    '&:hover' :{
      color: theme.palette.link.hover
    }
  },
}));
const Bars = (props) => {
  const classes = useStyles();
  const { setDetails, scales, data, xAccessor, colors, keys, horizontal, xKey, type, subtype } = props
  const { xScale, yScale } = scales
  const stacked = stack().keys(keys);
  const stackedData = stacked(data);
  const renderItem = (item, key, index, i, type_) => {
    let x = null;
    let y = null;
    let width = null;
    let height = null;

    if (horizontal) {
      x = xScale(item[0])
      y = yScale(xAccessor(item.data));
      height = yScale.bandwidth();
      width = xScale(item[1]) - xScale(item[0]);
    } else {
      x = xScale(xAccessor(item.data))
      y = yScale(item[1]);
      height = yScale(item[0]) - yScale(item[1]);
      width = xScale.bandwidth();
    }

    const value = item[1] - item[0];
    return (
      <g key={i}>
        {
          height && width ?
            <D3Tooltip 
              placement={horizontal ? "top" : "right"} 
              interactive color={colors(key)} 
              title={
                <div className={clsx(classes.link)} onClick={() => {
                    if (typeof (setDetails) === "function") {
                      setDetails(type_, subtype, true, {args:tooltipLinkArgumentsMap[item.data.state], health:keyColorLabelMap[key].name, state:subtype}, true) 
                   };
                  }}>
                      {item.data[xKey]} : {Math.floor(value)}
                </div>
              }
            >
              <rect
                x={horizontal ? x + 1 : x}
                y={y}
                key={i}
                height={height}
                width={width}
                fill={colors(key)}
                item={item}
                item-key={key}
                item-index={index}
              />
            </D3Tooltip>
            : null
        }
        <text
          x={!horizontal ? x + width / 2 + 3 : xScale(item[0]) + yScale.bandwidth()}
          y={!horizontal ? yScale(item[0]) - 5 : yScale(item.data[xKey]) + yScale.bandwidth() / 2}
          dx={'-0.3em'}
          fontSize={11}
          textAnchor="middle"
          alignmentBaseline="middle"
          fill="black"
          className={classes.text}
        >
          {
            horizontal && width > 30 ?  Math.floor(value) || null : null
          }
          {
            !horizontal  && height > 12 ?  Math.floor(value) || null : null
          }
        </text>
      </g>
    );
  }





  const renderSeries = (type2) => {
    return stackedData.map(item => {
      const { key, index } = item
      const data1 = Array.from(item)
      return data1.map((d, i) => {
        return renderItem(d, key, index, i, type2)
      })
    });
  }
  
  const keyColorLabelMap = {
    redCount:{status:'Unhealthy', name: 'RED', cssClass : classes.red},
    yellowCount:{status:'Needs Attention', name: 'YELLOW' , cssClass : classes.yellow},
    blueCount:{status:'Healthy', name: 'GREEN', cssClass : classes.blue},
  }

  const ParentTooltipContent = ({segmentData}) => {
    return (
      <Grid container >
        Total - {Math.floor(segmentData.totalCount)}
        {segmentData['redCount'] > 0 &&
        <Grid 
          className={clsx(classes.link)} container xs={12} s={12} item wrap='wrap' justify='space-between' alignItems='center' 
          onClick={() => {
            if (typeof (setDetails) === "function") {
              setDetails(type, subtype, true,{args:tooltipLinkArgumentsMap[segmentData.state], health:keyColorLabelMap.redCount.name, state:subtype}, true)
            };
          }}  
        >
          <Grid item className={classes.legend} style={{backgroundColor:colors('redCount')}}></Grid>
          <Grid item xs={11} title="clicktextred">{segmentData['redCount']} - {keyColorLabelMap.redCount.status}</Grid>
        </Grid>
        }
        {segmentData['yellowCount'] > 0 &&
        <Grid 
          className={clsx(classes.link)} item wrap='wrap' justify='space-between' alignItems='center' container xs={12} s={12}
          onClick={() => {
            if (typeof (setDetails) === "function") {
              setDetails(type, subtype, true,{args:tooltipLinkArgumentsMap[segmentData.state], health:keyColorLabelMap.yellowCount.name, state:subtype}, true)
            };
          }}  
        >
          <Grid item className={classes.legend} style={{backgroundColor:colors('yellowCount')}}></Grid>
          <Grid item xs={11} title="clicktextyellow" >{segmentData['yellowCount']} - {keyColorLabelMap.yellowCount.status}</Grid>
        </Grid>
        }
        {segmentData['blueCount'] > 0 && 
        <Grid 
          className={clsx(classes.link)} item wrap='wrap' justify='space-between' alignItems='center' container xs={12} s={12} 
          onClick={() => {
            if (typeof (setDetails) === "function") {
              setDetails(type, subtype, true,{args:tooltipLinkArgumentsMap[segmentData.state], health:keyColorLabelMap.blueCount.name, state:subtype}, true)
            };
          }}  
        >
          <Grid item className={classes.legend} style={{backgroundColor:colors('blueCount')}}></Grid>
          <Grid item xs={11} title="clicktextblue">{segmentData['blueCount']} - {keyColorLabelMap.blueCount.status}</Grid>
        </Grid>      
        }
      </Grid>
    )
  }
  return (
    <g data-testid='bars'>
      {renderSeries()}
      {
        data.map((item, i) => {
          let barProps = {};
          if (horizontal) {
            barProps = {
              y: yScale(item[xKey]) + yScale.bandwidth() / 2,
              x: xScale(item.total) + 15
            }
          } else {
            barProps = {
              x: xScale(item[xKey]) + xScale.bandwidth() / 2,
              y: yScale(item.total) - 5
            }
          }
          const tooltipRefComponent= {x:barProps.x-20,y:barProps.y-8}
          return (
            <g>
              {item.totalCount > 0 && <D3Tooltip placement={"top"} interactive title={<ParentTooltipContent segmentData={item} />}>
                <rect
                {...tooltipRefComponent}
                key={i}
                height={15}
                width={40}
                fill={'transparent'}
                item={item}
                />
              </D3Tooltip>}
              <text
                key={i}
                {...barProps}
                fontSize={12}
                textAnchor="middle"
                alignmentBaseline="middle"
                fill="white"
                className={classes.text}
              >
                {Math.floor(item.total) || null}
              </text>
            </g>
          )
        })
      }
    </g>
  )
}

export default Bars;