import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import clsx from "clsx";


const useStylesBootstrap = makeStyles((theme) => ({
    arrow: {
        color: theme.palette.white,
    },
    tooltip: {
        backgroundColor: theme.palette.white,
        color: 'black',
        maxWidth: 220,
        fontSize: theme.typography.pxToRem(12),
        border: `1px solid ${theme.palette.iron}`,
        minWidth : 190,
    },
}));

const template = makeStyles(theme => ({
    bar: {

        paddingLeft: 5,
        height: "100%",
        marginLeft: 0,
    },
    color:theme.palette.white
    
}))

export default function BootstrapTooltip(props) {
    const classes = useStylesBootstrap();
    const classes2 = template();
    let colorProps = {};
    if (props.color) {
        colorProps = {
                borderLeftStyle: 'solid',
                borderLeftColor: props.color || classes2.color,
        }
    }

    const Title = () => {
        return (
            <div data-testid='bootstrapTooltip' className={clsx(classes2.bar)} style={colorProps} >
                {props.title}
            </div>
        )
    }
    
    return <Tooltip elevation={10} arrow interactive placement={props.placement} enterTouchDelay='10' classes={classes} {...props} title={<Title />} />;
}