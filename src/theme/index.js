import { createMuiTheme, fade } from '@material-ui/core';
import { dark } from './palette';
import { typography, gilroy } from './typography';

const colorPanel = dark;

export const appTheme = createMuiTheme({
  palette: colorPanel,
  typography,
  overrides: {
    MuiCssBaseline: {
      '@global': {
        '@font-face': [gilroy],
      },
    },
    MuiListItem: {
      root: {
        "&$selected": {
          backgroundColor: colorPanel.secondary.dark
        }
      }
    },
    MuiMenu: {
      list: {
        backgroundColor: colorPanel.form.baseLight
      }
    }
  },
  type: colorPanel.type,
  zIndex: {
    appBar: 1200,
    drawer: 1100
  }
});

export const testTheme = createMuiTheme({
  palette: colorPanel,
  typography,
  type: colorPanel.type,
  zIndex: {
    appBar: 1200,
    drawer: 1100
  }
});

export const chartElementStyles = (theme => ({
  root: {
    height: theme.spacing(42),
    position: 'relative',
    [theme.breakpoints.down('sm')]: {
      marginBottom:theme.spacing(4)
    },
  },
  link: {
    color: colorPanel.secondary.accent,
    cursor: 'pointer',
    textDecoration: 'none'
  },
  spining: {
    pointerEvents: "none",
    height: '0.9em',
    animation: "App-logo-spin infinite 1s linear !important"
  },
  spin: {
    height: '0.9em',
    cursor: "pointer",
  },
  refreshIcon: {
    position: 'absolute',
    right: theme.spacing(2),
    top: '51%',
    transform: 'translateY(-50%)'

  },
  label: {
    color: theme.palette.menu.base
  },
  verticalDivider: {
    flexBasis: '2px',
    "& hr": {
      padding: 0,
      backgroundColor: 'transparent',
      borderLeft: `2px dashed ${theme.palette.divider}`,
    }
  },
  count: {
    fontSize: theme.spacing(6)
  },
  container: {
    position: "absolute",
    top: "50%",
    transform: "translateY(-35%)",
    textAlign: "center",
    width: "100%",
    verticalDivider_solid: {
      flexBasis: '2px',
      "& hr": {
        padding: 0,
        backgroundColor: 'transparent',
        borderLeft: `2px solid ${theme.palette.divider}`,
      }
    },
  },
  hyperlink: {
    cursor: 'pointer',
    textDecoration: 'underline',
    letterSpacing: '1px',
    padding: theme.spacing(.4),
    '&:hover': {
      color: theme.palette.link.hover
    }
  }
}));


export const cardStyles = (theme) => ({
  root: {
    display: 'block',
    flexWrap: 'wrap',
    width: '100%',
    height: "100%",
    borderRadius: '6px',
    backgroundColor: theme.palette.primary.base,
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
  },
  addPadding: {
    paddingBottom: theme.spacing(2)
  },
  content: {
    paddingBottom: 0,
    padding: '12px 0px 0px',
  },
  title: {
    padding: theme.spacing(1.5),
    position: 'relative',
    textTransform: 'uppercase'

  },
  actions: {
    display: 'block',
    marginLeft: theme.spacing(1),
    bottom: theme.spacing(2),
    position: 'absolute',
    width: `${theme.spacing(12)}%`
  },
  divider: {
    padding: 0,
    height: '2.03px',
    marginLeft: -(theme.spacing(1)),
    marginRight: -(theme.spacing(1)),
    backgroundColor: theme.palette.secondary.base
  },
  innercard: {
    backgroundColor: theme.palette.card.base,
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1),
    position: "relative"
    // marginTop: theme.spacing(0),
    // marginBottom: theme.spacing(0),
  }
});


export const filterStyles = (theme) => ({
  root: {
    flexGrow: 1,
    position: 'absolute',
    marginTop: 10,
    marginRight: theme.spacing(3),
    right: 0,
    zIndex: 100,
    width: 350,
    borderRadius: 0,
    backgroundColor: theme.palette.secondary.base,
  },
  shifttab: {
    marginLeft: theme.spacing(1)
  },
  show: {
    display: 'block'
  },
  hide: {
    display: 'none'
  },
  container: {
    paddingRight: theme.spacing(2),
    paddingLeft: theme.spacing(2),
  },
  title: {
    backgroundColor: theme.palette.secondary.base,
    fontFamily: 'Gilroy',
    padding: theme.spacing(2)
  },
  serchSpace: {
    backgroundColor: theme.palette.secondary.dark,
    padding: theme.spacing(2),
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
  },
  subtitle: {
    fontFamily: 'Gilroy',
    fontSize: 12,
    fontWeight: 200,
    letterSpacing: 0.5
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    height: 40,
    width: '100%',
    border: `1px solid #303862`,
    marginTop: 4

  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: '12ch',
      '&:focus': {
        width: '20ch',
      },
    },
  },
  controls: {
    height: 45,
    display: 'flex',
    alignContent: 'left',
    justifyContent: 'middle',
    cursor: 'pointer'
  },
  controlText: {
    color: theme.palette.extra.iron,
    fontSize: 14,
    letterSpacing: 0,
    textDecoration: 'underline',
    userSelect: "none",
    cursor: "pointer",
  },
  button: {
    cursor: 'pointer',
    userSelect: "none",
  },
  expandButton: {
    cursor: 'pointer',
    userSelect: "none",
    pointerEvents: 'none'
  },
  boxTitle: {
    fontFamily: "Gilroy",
    fontSize: 14,
    fontWeight: 300,
    letterSpacing: 0.58,
    textTransform: "uppercase"
  },
  openBox: {
    backgroundColor: theme.palette.secondary.dark,
  },
  closedBox: {
    backgroundColor: theme.palette.secondary.base,
  },
  border: {
    borderBottom: '2px solid #8CA0FF'
  },
  contentBox: {
    height: "100%",
    padding: theme.spacing(2)
  },
  contentText: {
    fontFamily: "Gilroy",
    fontSize: 16,
  },
  customerFilterRoot: {
    borderRight: `1px solid ${theme.palette.white}`,
    padding: `${theme.spacing(2)}px ${theme.spacing(2)}px !important`,
    [theme.breakpoints.down("sm")]: {
      borderRight: "none",
      padding: `${theme.spacing(0)}px ${theme.spacing(2)}px !important`,

    }
  },
  underline: {
    textDecoration: 'underline'
  },
  label: {
    textTransform: "capitalize"
  },
  accordian: {
    background: "transparent !important",
    boxShadow: "none",
  },
  accordianSummary: {
    minHeight: "10px !important",
    padding: 0,
    opacity: "1 !important",
    "& > .Mui-expanded": {
      margin: `0 !important`,
    }
  },
  accordianDetails: {
    display: "block",
    padding: 0,
  }
})

export const tableStyles = (theme) => ({
  root: {
    [theme.breakpoints.down("sm")]: {
      padding: 0
    }
  },
  expand: {
    backgroundColor: theme.palette.card.base,
    padding: theme.spacing(4),
    width: "70%",
  },
  outline: {
    backgroundColor: theme.palette.card.base,
  },
  grid: {
    borderBotton: '1px solid red',
    padding: theme.spacing(1)
  },
  text: {
    textTransform: "uppercase",
    fontFamily: 'Gilroy',
    letterSpacing: '0.1em'

  },
  subtext: {
    marginTop: -3,
    fontSize: "13px",
    lineHeight: "25px",
    fontFamily: 'Gilroy',
    paddingLeft: theme.spacing(3),
    letterSpacing: '0.03em'
  },
  location: {
    // width: "250px",
    height: '45px',
    flexGrow: 1,
    display: "flex",
    justifyContent: "space-between",
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
    border: `1px solid gray`,
    paddingTop: 15,
    textTransform: 'capitalize'
  },
  align: {
    display: 'flex',
    alignContent: 'left',
    justifyContent: 'middle',
    marginRight: theme.spacing(2),
  },
  filtericon: {
    border: '1px solid white',
    borderRadius: '50%',
    height: 30,
    width: 30,
    padding: 5
  },
  table: {
    width: '100%',
    marginTop: theme.spacing(2),
    '& div::-webkit-scrollbar': {
      height: `${theme.spacing(1.5)}px !important`,
      width: `${theme.spacing(1.5)}px !important`,

    },
    '& div::-webkit-scrollbar-track': {
      border: `5px solid ${theme.palette.primary.base}`,
      borderRadius: 0,
      background: theme.palette.divider
    },
    '& div::-webkit-scrollbar-thumb': {
      background: theme.palette.divider,
      borderRadius: '10px',
      border: `2px solid ${theme.palette.white}`
    },
  },
  etabutton: {
    cursor: "pointer",
    color: theme.palette.secondary.accent,
    "&:hover": {
      color: theme.palette.link.hover
    }
  }
})

export const viewStyles = ((theme) => ({
  marginTop: {
    marginTop: theme.spacing(1)
  },
  separator: {
    "& >div": {
      marginTop: theme.spacing(4),
      marginBottom: theme.spacing(4)
    }
  },
  hide: {
    visibility: 'hidden',
    display: 'none'
  },
  expand: {
    marginTop: theme.spacing(1),
    display: 'block',
    textTransform: "capitalize",
  },
  addPadding: {
    [theme.breakpoints.up('md')]: {
      padding: `0px ${theme.spacing(1)}px`
    },
    [theme.breakpoints.between('xs', 'md')]: {
      padding: `${theme.spacing(2)}px ${theme.spacing(1)}px`
    },
    [theme.breakpoints.down('xs')]: {
      padding: '0px'
    }
  },
  extendTitle: {
    marginTop: theme.spacing(2),
    padding: `0 ${theme.spacing(0)}px`,
    "& >div": {
      borderBottom: `2px solid ${theme.palette.secondary.accent}`
    },
    "& >div:first-child": {
      flexBasis: "79.5%",
      maxWidth: "80%"
    },
    "& >div:nth-child(2)": {
      flexBasis: "19.5%",
      maxWidth: "20%"
    },
    [theme.breakpoints.down('sm')]: {
      display: "none"
    },
  },
  modalBody:{
    overflow:"auto"
  },
  scrLinkbutton: {
    cursor: "pointer",
    color: theme.palette.secondary.accent,
    "&:hover": {
      color: theme.palette.link.hover
    }
  }
}))


export const itemDetailsStyles = ((theme) => ({
  root: {
  },
  tablebody: {
    padding: theme.spacing(5),
  },
  title: {
    paddingBottom: theme.spacing(1)
  },
  table: {
    marginTop: theme.spacing(2),
    fontSize: "14px"
  }
}));

export const selectStyles = theme => ({
  root: {
    "& > div > ul":{
      maxHeight: theme.spacing(50),
      overflow: "auto",
      '&::-webkit-scrollbar': {
        height: `${theme.spacing(1.5)}px !important`,
        width: `${theme.spacing(1.5)}px !important`,
      },
      '&::-webkit-scrollbar-track': {
        border: `5px solid ${theme.palette.text.underline}`,
        borderRadius: 0,
        background: theme.palette.divider
      },
      '&::-webkit-scrollbar-thumb': {
        background: theme.palette.divider,
        borderRadius: '10px',
        border: `2px solid ${theme.palette.white}`
      },
  }
  },
  button: {
    display: 'flex',
    border: `1px solid ${theme.palette.white}`,
    minWidth: 200,
    borderRadius: '4px',
    padding: theme.spacing(2),
    '& .MuiButton-label': {
      justifyContent: 'space-between'
    },
    cursor: 'pointer',
    textTransform: 'initial',
    [theme.breakpoints.down("sm")]: {
      minWidth: "auto"
    }
  },
  lowercase: {
    textTransform: 'lowercase',
  },
  text: {
    pointerEvents: 'none',
  },
  textTransform: {
    pointerEvents: 'none',
    "&:first-letter": {
      textTransform: 'initial'
    },
  },
  business: {
    textAlign: 'left',
    minWidth: theme.spacing(20),
    height: theme.spacing(5),
    justifyContent: "space-between",
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
  },
  capitalize: {
    textTransform: 'capitalize'
  },
  icon: {
    color: theme.palette.black,
  },
  menuList: {
    backgroundColor: theme.palette.white,
    color: theme.palette.black
  },
  menuItem: {
    '&:hover': {
      backgroundColor: "rgba(0, 0, 0, 0.05)",
    },
    '&:focus': {
      backgroundColor: "rgba(0, 0, 0, 0.1)",
      '&:hover': {
        backgroundColor: "rgba(0, 0, 0, 0.05)",
      },
    },
  },
  popper: {
    width: 198,
    borderRadius: '50%',
    zIndex: 9
  },
  toplabel: {
    top: theme.spacing(1.5), //15
    width: "fit-content",
    zIndex: 100,
    color: theme.palette.white,
    border: `1px solid ${theme.palette.white}`,
    paddingLeft: theme.spacing(1),
    borderRadius: theme.spacing(0.6),
    paddingRight: theme.spacing(1),
    paddingBottom: theme.spacing(0.2),
    backgroundColor: theme.palette.secondary.base,
    position: 'absolute',
    marginLeft: theme.spacing(1),
    fontSize: 11,
  },
})


export const mapStyles = theme => ({
  root: {
  },
  map: {
    height: (props) => {
      return `${props.parentHeight}px !important`;
    },
    width: (props) => {
      return `${props.parentWidth}px !important`;
    }
  },
  tooltip: {
    textAlign: 'left',
    paddingLeft: theme.spacing(1),
  },
  subtitle: {
    fontSize: 14,
    color: theme.palette.black,
    fontWeight: 500,
  },
  tooltipname: {
    fontSize: 16,
    marginBottom: 5,
    fontWeight: "bold",
    width: 'fit-content',
    color: theme.palette.black,
    cursor: 'pointer',
    textDecoration: 'underline'
  },
  legend: {
    minWidth: 400,
    textAlign: 'left',
    bottom: `${theme.spacing(1)}px !important`
  },
  circle: {
    borderRadius: '50%',
    width: 20,
    height: 20
  },
  redFill: {
    backgroundColor: theme.palette.legendColors.unhealthy
  },
  blueFill: {
    backgroundColor: theme.palette.legendColors.healthy
  },
  image: {
    width: 20
  },
  healthy: {
    borderLeft: `6px solid ${theme.palette.legendColors.healthyGreen}`,
  },
  critical: {
    borderLeft: `6px solid ${theme.palette.legendColors.criticalRed}`,
  },
  behind: {
    borderLeft: `6px solid ${theme.palette.legendColors.pink}`,
  },
  monitor: {
    borderLeft: `6px solid ${theme.palette.legendColors.risk}`,
  }
});

export const shipmentStyles = theme => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: "transparent",
  },
  appbar: {
    background: "transparent",
    borderBottom: `1px solid ${theme.palette.white}`,
  },
  tab: {
    [theme.breakpoints.down("sm")]: {
      flexBasis: "50%"
    }
  },
  tabName: {
    textAlign: "center",
    cursor: 'pointer',
  },
  shipmentNumber: {
    fontSize: theme.spacing(4),
    fontWeight: "500"
  },
  leftBox: {
    borderRight: `2px solid ${theme.palette.white}`,
    "& > div": {
      marginBottom: theme.spacing(4)
    },
    [theme.breakpoints.down("sm")]: {
      borderRight: 'none',
      "& > div": {
        marginBottom: theme.spacing(2)
      },
    }
  },
  container: {
    margin: `${theme.spacing(4)}px 0px`,
    [theme.breakpoints.down("sm")]: {
      padding: `0px ${theme.spacing(3)}px`,
    }
  },
  tabPanel: {
    background: theme.palette.text.underline
  },
  stops: {
    background: theme.palette.secondary.dark,
    borderRadius: theme.spacing(.5),
    padding: `${theme.spacing(4)}px ${theme.spacing(2)}px`,
    margin: `${theme.spacing(2.5)}px 0px`,
    [theme.breakpoints.down("sm")]: {
      padding: theme.spacing(2),
      width: "100%"
    }
  },
  component: {
    marginTop: theme.spacing(8)
  }
})

export const tabsBarStyles = (theme) => ({
  root: {
    flexGrow: 1,
    width: '100%',
  },
  appbar: {
    background: theme.palette.secondary.dark,
    paddingLeft: theme.spacing(5),
    paddingRight: theme.spacing(5),
    [theme.breakpoints.down('xs')]: {
      paddingRight: theme.spacing(2),
      paddingLeft: theme.spacing(2),
    },
  },
  tabpanel: {
  },
  tabName: {
    float: 'left',
    textAlign: "left",
    display: 'flex'
  },
  name: {
    textAlign: "left",
    color: theme.palette.whte,
    pointerEvents: 'none',
    whiteSpace: 'nowrap',
    display: 'inline-block',
    overflow: 'hidden',
    marginTop: theme.spacing(.25),
    textTransform: 'capitalize'
  },
  icon: {
    display: 'inline-block',
    paddingRight: 10,
  },
  addButton: {
    color: "white",
    cursor: 'pointer'
  },
  showTabs: {
    display: 'flex',
    borderLeft: `3px solid ${theme.palette.white}`,
    marginLeft: theme.spacing(2)
  },
  showSingleLocation: {
    display: 'flex',
  },
  customtab: {
    maxWidth: '100%'
  },
  hideTabs: {
    display: 'none'
  },
  viewmore: {
    borderRadius: '3px',
    border: `1px solid ${theme.palette.secondary.accent}`,
    padding: theme.spacing(.5),
    textTransform: 'capitalize'
  },
})

export const performanceTableStyle = (theme) => ({
  main: {
    maxHeight: '550px',
    overflow: 'auto',
    '&::-webkit-scrollbar': {
      height: `${theme.spacing(1.5)}px !important`,
      width: `${theme.spacing(1.5)}px !important`,
    },
    '&::-webkit-scrollbar-track': {
      border: `5px solid ${theme.palette.text.underline}`,
      borderRadius: 0,
      background: theme.palette.divider
    },
    '&::-webkit-scrollbar-thumb': {
      background: theme.palette.divider,
      borderRadius: '10px',
      border: `2px solid ${theme.palette.white}`
    },
  },
  innerCard: {
    minHeight: props => theme.spacing(props.singleLocation ? 0 : 45),
    width: "100%",
    backgroundColor: theme.palette.card.base
  },
  title: {
    paddingTop: theme.spacing(2)
  },
  legends: {
    display: 'inline-flex',
    paddingTop: theme.spacing(0),
    paddingBottom: theme.spacing(1.25),
    flexFlow: `row nowrap`
  },
  sitecolumn: {
    paddingLeft: theme.spacing(1.5)
  },
  aheadcolumn: {
    paddingLeft: theme.spacing(0.5)
  },
  hourcolumn: {
    paddingRight: theme.spacing(1),
    textAlign: 'end'
  },
  icons: {
    marginLeft: 'auto',
    [theme.breakpoints.down('sm')]: {
      marginRight: theme.spacing(3)

    }
  },
  nodata: {
    width: '75%'
  },
  link: {
    cursor: 'pointer',
    textDecoration: 'underline',
    "&:hover": {
      color: theme.palette.secondary.accent
    },
    [theme.breakpoints.down('xs')]: {
      fontSize: theme.spacing(1.25),
      whiteSpace: "nowrap"
    }
  },
  filters: {
    width: theme.spacing(37.5),
    height: theme.spacing(50),
    backgroundColor: theme.palette.secondary.base,
    padding: theme.spacing(1.5),
    [theme.breakpoints.down('sm')]: {
      width: "auto",
      height: "auto",
      marginBottom: theme.spacing(2)
    }
  },
  tableBody: {
    marginBottom: theme.spacing(1.25),
    height: theme.spacing(2.75)
  },
  tableHeader: {
    marginBottom: theme.spacing(6)
  },
  aheadicon: {
    paddingLeft: theme.spacing(2.5)
  },
  behindicon: {
    paddingLeft: theme.spacing(2.25)
  },
  chart: {
    position: "relative",
    top: `-${theme.spacing(6)}px`,
  }
})


export const filterbarStyles = (theme) => ({
  root: {
    paddingBottom: theme.spacing(2),
    marginTop: theme.spacing(9.5),
    paddingRight: theme.spacing(5),
    paddingLeft: theme.spacing(5),
    borderRadius: 0,
    backgroundColor: theme.palette.filterbar,
    borderBottom: `1px solid ${theme.palette.border}`,
    [theme.breakpoints.down('sm')]: {
      paddingRight: theme.spacing(2),
      paddingLeft: theme.spacing(2),
    },
  },
  dropdown: {
    position: 'absolute',
    top: 124,
    right: 1,
    left: 1,
    margin: 0,
    zIndex: 1,
    width: 'auto',
    height: '436px',
    padding: theme.spacing(1),
  },
  filter: {
    display: 'flex',
    alignContent: 'left',
    justifyContent: 'middle',
    marginRight: theme.spacing(2),
  },
  location: {
    width: theme.spacing(26),
    height: theme.spacing(5),
    flexGrow: 1,
    display: "flex",
    justifyContent: "space-between",
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
    border: `1px solid ${theme.palette.border}`,
    textTransform: 'capitalize',
    [theme.breakpoints.down("xs")]: {
      minWidth: theme.spacing(16),
      maxWidth: theme.spacing(23),

    }
  },
  text: {
    marginRight: theme.spacing(2),
  },
  textHead: {
    marginRight: theme.spacing(2)
  },
  othertext: {
    marginRight: theme.spacing(1),

  },
  showfavs: {
    width: theme.spacing(20),
    marginLeft: 0,
    marginRight: 0,
  },
  // chipsBox: {
  //   marginLeft: theme.spacing(2),
  //   marginRight: theme.spacing(2),
  // },
  chip: {
    border: `1px solid ${theme.palette.border}`,
    backgroundColor: theme.palette.secondary.dark,
    margin: theme.spacing(0.5),
  },
})

export const locationfilterlayoutStyles = (theme) => ({
  root: {
    margin: 0,
    width: 'auto',
    borderRadius: 0,
    zIndex: "999",
    '& div::-webkit-scrollbar': {
      height: `${theme.spacing(1.5)}px !important`,
      width: `${theme.spacing(1.5)}px !important`,

    },
    '& div::-webkit-scrollbar-track': {
      border: `5px solid ${theme.palette.secondary.base}`,
      borderRadius: 0,
      background: theme.palette.divider
    },
    '& div::-webkit-scrollbar-thumb': {
      background: theme.palette.divider,
      borderRadius: '10px',
      border: `2px solid ${theme.palette.white}`
    },
    [theme.breakpoints.down("sm")]: {
      top: `${theme.spacing(15.25)}px`,
      height: "auto"
    }
  },
  overflow: {
    height: theme.spacing(50),
    overflowX: 'auto',
    paddingTop: theme.spacing(2),
    paddingLeft: theme.spacing(2),
    [theme.breakpoints.down("sm")]: {
      height: "auto"
    }
  },
  overflowLocations: {
    maxHeight: theme.spacing(35),
    overflowX: 'auto',
    marginTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
    '&>div': {
      padding: `0px ${theme.spacing(4)}px`,
    },
    '& >div:not(:nth-child(3n))': {
      borderRight: '1px solid'
    },
    [theme.breakpoints.down("sm")]: {
      maxHeight: theme.spacing(40),
      '&>div': {
        padding: `0px ${theme.spacing(2)}px`,
      },
      '& >div:not(:nth-child(3n))': {
        borderRight: 'none'
      },
      '& >div:nth-child(odd)': {
        borderRight: '1px solid'
      },
    }
  },
  height: {
    height: theme.spacing(25),
    [theme.breakpoints.down("sm")]: {
      height: "40vh"
    }
  },
  favorites: {
    backgroundColor: theme.palette.secondary.base,
    height: theme.spacing(65),
    [theme.breakpoints.down("sm")]: {
      height: "auto"
    }
  },
  locations: {
    padding: theme.spacing(3),
    height: theme.spacing(65),
    [theme.breakpoints.down("sm")]: {
      height: "auto",
      padding: theme.spacing(2),
      paddingBottom: theme.spacing(11)
    }
  },
  actionButton: {
    textTransform: 'capitalize',
    position: 'absolute',
    bottom: theme.spacing(2.5),
    color: theme.palette.white,
    border: `1px solid ${theme.palette.white}`
  },
  actionButtonCUSO: {
    bottom: "-56px",
    [theme.breakpoints.down("sm")]: {
      bottom: `-${theme.spacing(12)}px`
    }
  },
  closeicon: {
    position: "absolute",
    right: 25,
    top: 25,
    cursor: "pointer"
  },
  formControl: {
    border: `white`,
    borderRadius: '5px'
  },
  input: {
  },
  textField: {
    color: theme.palette.primary.contrastText,
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    width: 200,
  },
  center: {
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  },
  cssLabel: {
    color: theme.palette.primary.contrastText
  },

  cssOutlinedInput: {
    color: theme.palette.primary.contrastText,
    '&$cssFocused $notchedOutline': {
      borderColor: `${theme.palette.primary.contrastText} !important`,
    }
  },
  insidefilter: {
    marginBottom: theme.spacing(1)
  },
  menuitem: {
    color: 'black'
  },
  notchedOutline: {
    color: theme.palette.primary.contrastText,

    borderWidth: '1px',
    borderColor: `${theme.palette.primary.contrastText} !important`
  },
  noresultContainer: {
    height: theme.spacing(10)
  },
  link: {
    color: theme.palette.secondary.accent
  },
  searchCountText: {
    color: theme.palette.legendColors.greyLight
  },
  searchBar: {
    position: "absolute",
    top: "45%",
    transform: "translateY(-45%)",
    [theme.breakpoints.down("sm")]: {
      position: "static",
      marginTop: theme.spacing(3),
      minWidth: theme.spacing(18)
    }
  }
})

export const searchFilterStyles = (theme) => ({
  root: {
    flexGrow: 1
  },
  search: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: theme.palette.secondary.base,
    border: `1px solid ${theme.palette.white}`,
    "&:hover": {
      backgroundColor: theme.palette.secondary.hover
    },
    width: "100%",
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  inputRoot: {
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
  },
  customInput1: {
    border: `1px solid`,
    borderRadius: theme.spacing(.5)
  }
})

export const KCReactTabs = (theme) => ({
  tabs: {
    width: "100%",
    "& .react-tabs__tab--selected[aria-selected='true']": {
      background: "transparent",
      border: "none",
      color: theme.palette.white,
      borderBottom: `5px solid ${theme.palette.secondary.accent}`,
    }
  }
})

export const mainLayoutStyles = theme => ({
  mainLayoutContainer: {
    paddingTop: theme.spacing(2),
    position: 'relative',
    height: '100%',
    [theme.breakpoints.up('xs')]: {
      marginRight: theme.spacing(0),
      marginLeft: theme.spacing(0),
    }
  },
  shiftContent: {},
  content: {
    height: '100%'
  },
  filter: {
    marginTop: '65px',
    height: 200
  },
  timer: {
    position: 'absolute',
    right: 20,
    fontSize: 13,
    color: 'gray',
    top: 0,
    [theme.breakpoints.down("sm")]: {
      top: theme.spacing(2),
      textAlign: 'right',
      maxWidth: theme.spacing(12)
    }
  },
  padding: {
    padding: theme.spacing(3),
    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing(0),
    }
  }
})