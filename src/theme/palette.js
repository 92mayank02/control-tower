import { colors } from '@material-ui/core';

const white = '#FFF'
const black = colors.grey[900];

const coloursDark = {
  background: '#131834',//background //filterbar

  primaryBase: '#1C2340', //bg4 //primaryBase //topbar
  primaryHover: "#303751",
  primarySelected: "#2E354F",
  primaryFocus: "#373D57",
  primaryPressed: "#333953",
  primaryDragged: "3B4472", //add box shadow from figma when using

  secondaryBase: "#465083", //FormLightBase
  secondaryDark: "#303862", //FormDarkBase
  secondaryHover: "#4D5788", //FormLightHover
  secondarySelected: "#57608E", 
  secondaryFocus:"#5C6592",  //FormLightActive
  secondaryPressed: "#555E8D", //Secondary Dragged
  
  menuBase: "#5C67A4",
  
  cardBase: "#272C4F",
  cardHover: "#303651",
  cardFocus: "#414760",

  formDarkHover : "#384068",
  formDarkActive: "#495075",

  greyMedium : "#939597",
  greyLight: "#CACACA",

  healthy: "#56B1FA",
  unhealthy: "#FF2972",
  risk: "#FFC654", //Need attention
  consumer: "#9772D5",
  professional: "#FFD5F9",
  STO: "#83E2DB",
  healthyGreen: "#00FF81",
  accent: '#0091EA', //Accent

  
  malibu80:'#97dffc', //Also linkDefault from design
  ultramarine: '#10059F', //Link Hover
  
  //ApprovedLegend colors
  malibu100:'#56b1FA', //Also linkActive //Healthy
  radicalRed:'#FA1A66',
  goldTainoi:'#FFC654',

  //Pie and Doughnut Colors
  frenchLilac:'#78578A',
  purpleMountain:'#9973B5',
  floralLavender:'#BA8FE0',
  lilac:'#CCA2C6',
  desertSand:'#E7C1B1',
  unbleachedSilk:"#EFEDBD",
  champagnePink:"#F3DACE",
  mistyRose:"#F5E2DF",
  cultured:"#F6F4F4",

  iron:'#D5D3D7', // From color palette 4
  chambray: '#384680',//kc_dark_border // ChipDefaultBorder

  
  //Error Message Colors
  coralRed100: '#b71c1c',
  coralRed60: '#e53935',
  coralRed40: '#ef5350',

   //Chart Colors New
  criticalRed: '#FF0000',
  pink: '#FF4081',
};

const dark = {
  type:'dark',
  primary: {
    contrastText: white,
    main: white,
    base:coloursDark.primaryBase,
    hover:coloursDark.primaryHover,
    focus:coloursDark.primaryFocus,
  },
  secondary: {
    dark: coloursDark.secondaryDark,
    base:coloursDark.secondaryBase,
    accent:coloursDark.accent,
    contrastText: white,
    main:coloursDark.accent,
    hover:coloursDark.secondaryHover
  },
  error: {
    contrastText: white,
    dark: coloursDark.coralRed100,
    main: coloursDark.coralRed60,
    light: coloursDark.coralRed40
  },
  text: {
    primary: white,
    secondary: coloursDark.greyMedium,
    link: coloursDark.malibu80,
    underline:coloursDark.cardBase
  },
  form: {
    baseDark:coloursDark.secondaryDark,
    baseLight:coloursDark.secondaryBase
  },
  menu:{
    base:coloursDark.menuBase
  },
  card:{
    base:coloursDark.cardBase
  },
  chip:{
    borderActive:white,
    borderDefault:coloursDark.chambray,
    backgroundActive:coloursDark.menuBase,
    backgroundDefault:coloursDark.secondaryBase
  },
  link:{
    default:coloursDark.secondaryDark,
    hover:coloursDark.ultramarine
  },
  icon: coloursDark.malibu80,
  background: {
    default: coloursDark.background,
    paper: white
  },
  divider: coloursDark.greyMedium,  
  topbar: coloursDark.primaryBase,
  filterbar: coloursDark.background,
  black,
  white,
  chartColors: {
    healthy:coloursDark.healthy,
    risk:coloursDark.risk,
    unhealthy:coloursDark.unhealthy
  },
  legendColors: {
    healthy:coloursDark.healthy,
    risk:coloursDark.risk,
    unhealthy:coloursDark.unhealthy,
    healthyGreen: coloursDark.healthyGreen,
    criticalRed: coloursDark.criticalRed,
    pink: coloursDark.pink,
    greyLight: coloursDark.greyLight
  },
  chartColorsMulti:   [coloursDark.frenchLilac, coloursDark.purpleMountain, coloursDark.floralLavender, coloursDark.lilac, coloursDark.desertSand, coloursDark.unbleachedSilk, coloursDark.champagnePink, coloursDark.mistyRose, coloursDark.cultured ],
  border: coloursDark.chambray,
  extra:{
    iron:coloursDark.iron,
  },
  guardrailColors: {
    consumer:coloursDark.consumer,
    professional:coloursDark.professional,
    STO:coloursDark.STO
  }
};

export { dark }