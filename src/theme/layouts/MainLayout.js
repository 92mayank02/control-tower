import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { useMediaQuery, useTheme, makeStyles } from '@material-ui/core';
import { withOktaAuth } from '@okta/okta-react';
import FilterBar from "./FilterBar";
import TabsBar from "./TabsBar";
import { connect } from "react-redux";
import { addUser, removeUser, setBlockOnTabChange } from "../../reduxLib/services";
import DownloadManager from 'components/common/Table/DownloadManager';
import { useDeepCompareEffect, usePrevious } from 'react-use';
import { get } from "lodash";
import Topbar from "./Topbar";
import TabPanel from "components/header/TabPanel";
import ChipFilters from './ChipFilters';
import { viewByConstants } from "configs/appConstants";
import { getTabsData, generateHeader, defaultShowTabsBy } from 'helpers';
import useScrollInfo from './ScrollHelper';
import AppContext from './AppContext';
import { PageRouter } from 'components/pages/PageRouter';
import { mainLayoutStyles } from 'theme';
import useUniqTab from 'components/common/Helpers/UniqTab';
import { useHistory, useLocation } from 'react-router-dom';
import { chartDataIdentifierForAPI } from "../../components/pages/PageRouter";
import qs from "qs"

const useStyles = makeStyles(mainLayoutStyles);
export const allowedPlaces = ["network", "mylocation"];

export const MainLayout = props => {

  const { selections, stopfavload, showTabsBy, blockOnTabChange, setBlockOnTabChange, match, reduxState } = props;

  const classes = useStyles();
  const location = useLocation()
  const history = useHistory()
  const isDesktop = useMediaQuery(useTheme().breakpoints.up('lg'), {
    defaultMatches: true
  });

  useUniqTab();

  const [mainTab, setMainTab] = useState({ tab1: false, tab2: false });
  const [pageDetails, setDetailsMain] = useState({ [chartDataIdentifierForAPI.network]: true, type: allowedPlaces[0], filterParams:{}});

  const [activeTopNavTabObject, setActiveTopNavTab] = useState({ activeTopNavTab: allowedPlaces[0], secondaryActiveTab: null });
  const prevPageDetails = usePrevious(activeTopNavTabObject);

  const items = getTabsData(selections[showTabsBy], viewByConstants[showTabsBy]);
  const { id } = viewByConstants[showTabsBy];
  const visibility = !pageDetails[chartDataIdentifierForAPI.shipmentdetails] && !pageDetails[chartDataIdentifierForAPI.analytics] && !pageDetails[chartDataIdentifierForAPI.stockConstraintReport]

  const manageTabs = () => {
    const selectedTabsList = getTabsData(selections[showTabsBy], viewByConstants[showTabsBy]);

    if (selectedTabsList?.length > 1) {
      if (mainTab.tab1 !== 1) {
        setMainTab({ tab1: 1, tab2: false })
      }
    } else {
      if (selectedTabsList?.length === 1) {
        setMainTab({ tab1: false, tab2: selectedTabsList[0]?.custom?.id })
      } else {
        setMainTab({ tab1: 0, tab2: false })
      }
    }
  }


  const toggleMainTab = ({ tab1, tab2 }) => {
    setMainTab({ tab1, tab2 })
  }

  const setDetails = (type, subtype, value, filterParams = {}, scrollTo = false) => {
    console.log(subtype)
    if (!subtype) {
      setDetailsMain({ [chartDataIdentifierForAPI.network]: true, filterParams, type });
    }
    else if (subtype && Object.values(chartDataIdentifierForAPI).includes(subtype)) {
      setDetailsMain({ [subtype]: true, filterParams, type })
    }
    else {
      setDetailsMain((state) => ({ ...state, filterParams, type }))
    }
    if (scrollTo) {
      executeScroll()
    }
  }

  const perfTableRedirection = (siteNum,type)=>{
    //Putting two state changes inside a single function will cause only one rerender
    setMainTab({tab1:false,tab2:siteNum})
    setDetailsMain({ [chartDataIdentifierForAPI.dist]: true,filterParams:{}, type });
    setBlockOnTabChange()
  }

  const siteNums = items.map(d => d[id]) || [];
  const [scrollInfo, setRef, ref] = useScrollInfo();

  const executeScroll = () => {
    return ref?.current?.scrollIntoView({
      behavior: "smooth",
    });
  }

  useDeepCompareEffect(() => {
    if (!stopfavload && !blockOnTabChange) {
      manageTabs()
    }
  }, [selections[showTabsBy], showTabsBy, stopfavload]);


  useEffect(() => {
    if (!mainTab.tab2) {
      setActiveTopNavTab({ ...activeTopNavTabObject, activeTopNavTab: mainTab.tab1 === 0 ? allowedPlaces[0] : mainTab.tab1 === 1 ? 'mylocation' : undefined })
    }
    else {
      setActiveTopNavTab({ ...activeTopNavTabObject, activeTopNavTab: `${mainTab.tab2}` })
    }
  }, [mainTab])

  useDeepCompareEffect(()=>{
    //Run qs.parse(tableDataPayload,{ encode: true, strictNullHandling:true}) to know the payload
    const tableDataPayload = qs.stringify(reduxState.tabledata.tablebody,{ encode: true, strictNullHandling:true })
    console.log(location)
    history.push(`${location.pathname}?filters=${tableDataPayload}`,reduxState)
  },
  [reduxState.tabledata,reduxState.charts])
   
  return (
    <AppContext.Provider value={{ executeScroll, setRef, ref }}>
      <Topbar {...props} setDetails={setDetails} pageDetails={pageDetails} showHamburgerMenu={true} />
      <div data-testid="mainlayout"
        className={clsx({
          [classes.root]: true,
          [classes.shiftContent]: isDesktop
        })}
      >
        {visibility && <FilterBar className={classes.filter} history={props.history} />}
        {
          (!stopfavload) ?
            <>
              <DownloadManager />
              {visibility &&
                <TabsBar
                  history={props.history}
                  toggleMainTab={toggleMainTab}
                  mainTab={mainTab}
                />
              }
              <TabPanel classes={classes} value={mainTab.tab1} index={0}>
                {visibility &&
                  <ChipFilters filter={null} rootTab={allowedPlaces[0]} />
                }
                {
                  mainTab.tab1 === 0 && <PageRouter
                    prevPageDetails={prevPageDetails}
                    setActiveTopNavTab={setActiveTopNavTab}
                    pageDetails={pageDetails}
                    type={allowedPlaces[0]}
                    siteNums={null}
                    showTabsBy={showTabsBy}
                    perfTableRedirection={perfTableRedirection}
                    globalFilterSelection={generateHeader({ selections, type: allowedPlaces[0], showTabsBy })}
                    match={match}
                    setDetails={setDetails} />
                }
              </TabPanel>
              <TabPanel classes={classes} value={mainTab.tab1} index={1}>
                {visibility &&
                  <ChipFilters filter={items} rootTab={"mylocation"} />
                }
                {
                  mainTab.tab1 === 1 && siteNums.length > 0 && <PageRouter
                    prevPageDetails={prevPageDetails}
                    setActiveTopNavTab={setActiveTopNavTab}
                    pageDetails={pageDetails}
                    type="mylocation"
                    siteNums={siteNums}
                    showTabsBy={showTabsBy}
                    perfTableRedirection={perfTableRedirection}
                    globalFilterSelection={generateHeader({ selections, type: allowedPlaces[1], showTabsBy })}
                    setDetails={setDetails}
                    match={match} />
                }
              </TabPanel>
              {
                items.map((d, i) => {
                  return (
                    <TabPanel classes={classes} key={d?.custom?.id} value={mainTab.tab2} index={d?.custom?.id}>
                      {visibility &&
                        <ChipFilters filter={[d]} rootTab={"tab"} />
                      }
                      <PageRouter
                        pageDetails={pageDetails}
                        prevPageDetails={prevPageDetails}
                        setActiveTopNavTab={setActiveTopNavTab}
                        type={d?.custom?.id}
                        siteNums={[d[id]]}
                        showTabsBy={showTabsBy}
                        perfTableRedirection={perfTableRedirection}
                        globalFilterSelection={generateHeader({ selections, type: "tab", showTabsBy, item: d })}
                        setDetails={setDetails}
                        match={match}
                      />
                    </TabPanel>
                  )
                })
              }</> : null}
      </div>
    </AppContext.Provider>
  );
};

MainLayout.propTypes = {
  children: PropTypes.node
};

const mapStateToProps = state => {
  const showTabsBy = defaultShowTabsBy(state);
  return {
    stopfavload: state.items.stopfavload,
    selections: { locations: state.items.items, customer: [...(get(state, "items.cuso.CUST", [])), ...(get(state, "items.cuso.SALES_OFFICE", []))] },
    showTabsBy,
    blockOnTabChange: get(state, 'items.blockOnTabChange', false),
    reduxState:state
  }
}
const mapDispatchToProps = {
  addUser,
  removeUser,
  setBlockOnTabChange
}

export default connect(mapStateToProps, mapDispatchToProps)(withOktaAuth(MainLayout));
