import React from "react";

const AppContext = React.createContext({});

export const useAppContext = () => React.useContext(AppContext);

export default AppContext;