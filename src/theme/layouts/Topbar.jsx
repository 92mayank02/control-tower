import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Grid, AppBar, Toolbar, Box, Avatar, Badge, Typography, makeStyles, Typography as MuiTypography } from '@material-ui/core';
import logo_dark from '../../assets/images/logo_dark.svg';
import logo_light from '../../assets/images/logo.png';
import { connect } from "react-redux";
import LinearLoader from "../../components/common/LinearLoader";
import { addUser , removeUser } from "../../reduxLib/services";
import { withOktaAuth } from '@okta/okta-react';
import clsx from 'clsx';
import { CTLink } from '../../components/common/CTLink';
import AppDrawer from "../../components/common/AppDrawer";
import FeedBack from "../../components/common/FeedBack";
import FeedbackIcon from '@material-ui/icons/Feedback';

const useStyles = makeStyles(theme => ({
  root: {
    boxShadow: 'none',
    flexGrow: 1,
  },
  app: {
    height: theme.spacing(8),
    verticalAlign: "middle",
    display: "inline-block",
    backgroundColor: theme.palette.topbar,
    paddingRight: theme.spacing(5),
    paddingLeft: theme.spacing(5),
    borderBottom: `1px solid ${theme.palette.border}`,
    [theme.breakpoints.down('sm')]: {
      paddingRight: theme.spacing(2),
      paddingLeft: theme.spacing(2),
    }
  },
  toolbar: {
    padding: 0
  },
  logo: {
    height: '47px',
    width: '204px',
    margin: `${theme.spacing(1)}px 0px`,
    background: `url(${theme.type === "dark" ? logo_dark : logo_light}) no-repeat`,
    backgroundSize: '90%'

  },
  avatar: {
    border: `2px solid ${theme.palette.white}`,
    color: theme.palette.white,
    backgroundColor: theme.palette.topbar,
  },
  avatarText: {
    color: theme.palette.white,
  },
  username: {
    color: theme.palette.white,
    paddingRight: theme.spacing(1),
    paddingLeft: theme.spacing(1)
  },
  switch: {
    paddingRight: theme.spacing(2),
  },
}));

const Topbar = props => {
  const { loading, favsLoading, user, addUser, authService, stopfavload,buloading, setDetails, pageDetails, showHamburgerMenu } = props;
  const classes = useStyles();

  const username = user ? `${user.given_name} ${user.family_name}` : "   ";

  global.authService = authService;

  React.useEffect(() => {
    if (!user) {
      authService.getUser().then(d => addUser(d)).catch(err => addUser(null));
    } else {
      window.FS.identify(user.sub, {
        displayName: username
      });
    }
  }, [addUser, user, authService]);

  const [openFeedback, setOpen] = React.useState(false);

  return (
    <div className={classes.root}>
      <AppBar elevation={0} className={classes.app}>
        <Toolbar className={classes.toolbar}>
          <Grid container justify='space-between' nowrap>
            {showHamburgerMenu && <Grid item >
              <AppDrawer setDetails={setDetails} pageDetails={pageDetails} />
            </Grid>}
            <Grid xs={6} sm={9} md={6} lg={8} item>
              <RouterLink to="/" >
                <MuiTypography
                  className={classes.title}
                  variant="h3"
                >
                  <Box className={clsx(classes.logo)} />
                </MuiTypography>
              </RouterLink>
            </Grid>
            <Grid item container xs={4} sm={2} md={5} lg={3} justify='space-evenly' alignItems='center'>
              <Grid item>
                <Box display={{ xs: 'inline-block' }} >
                  <Badge color="secondary" >
                    <Avatar className={classes.avatar}>
                      <Typography variant="h1" className={classes.avatarText} color="primary">{
                        `${username.split(" ")[0].charAt(0)}${username.split(" ")[1].charAt(0)}`
                      }</Typography>
                    </Avatar>
                  </Badge>
                </Box>
                <Box display={{ xs: 'none', md: 'inline-block' }}>
                  <Badge color="primary">
                    <Typography variant="h6" className={classes.username} color="primary">{username}</Typography>
                  </Badge>
                </Box>
              </Grid>
              <Grid color="secondary" onClick={() => setOpen(!openFeedback)}>
                <CTLink onClick={() => setOpen(!openFeedback)} >
                  <Box display={{ xs: 'inline-block' }} >
                    <Badge color="secondary">
                      <FeedbackIcon fontSize="medium" />
                    </Badge>
                  </Box>
                  <Box display={{ xs: 'none', md: 'inline-block' }}>
                    <Badge color="primary">
                      <Typography variant="body1" className={classes.username} color="primary">Feedback</Typography>
                    </Badge>
                  </Box>
                </CTLink>
              </Grid>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
      {showHamburgerMenu && (loading || favsLoading || stopfavload || buloading) && <LinearLoader />}
      <FeedBack openFeedback={openFeedback} openAction={setOpen} />
    </div>
  );
};

Topbar.propTypes = {
  className: PropTypes.string,
  onSidebarOpen: PropTypes.func,
  authState: PropTypes.any,
  authService: PropTypes.any
};


const mapStateToProps = (state) => {
  return {
    loading: state.sites.loading,
    favsLoading: state.favorites.mark_loading,
    showfavs: state.options.showfavs,
    user: state.auth.user,
    stopfavload: state.items.stopfavload,
    buloading: state.favorites.add_bu_loading
  }
}

const mapDispatchToProps = {
  addUser,
  removeUser
}

export default connect(mapStateToProps, mapDispatchToProps)(withOktaAuth(Topbar));
