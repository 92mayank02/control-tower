import React from 'react';
import { ExpandMore, ExpandLess } from '@material-ui/icons';
import clsx from 'clsx';
import KCButton from "../../components/common/Button";
import IOSSwitch from "../../components/common/IOSSwitch";
import { Typography, Box, FormControlLabel, Grid, useMediaQuery, useTheme, makeStyles } from '@material-ui/core';
import { connect } from 'react-redux';
import LocationsFilterLayout from '../../components/header/LocationsFilterLayout';
import { get, intersection } from "lodash";
import KCSelect from 'components/common/Select';
import { viewByConstants } from "configs/appConstants";
import {
  addItem,
  removeItem,
  clearAll,
  getLocationsService,
  showhideFavourites,
  addItemsLocations as addItemsLocationsAction,
  removeFavs,
  getFilterLayouts,
  setBusinessLocalAction,
  setViewBy,
  getSalesOfficeService,
  addItemsCustomer as addItemsCustomerAction,
  joinFavstoCuso,
  removeFavsCuso
} from '../../reduxLib/services';
import Snack from "components/common/Helpers/Snack";
import { filterbarStyles } from "theme";
import { defaultShowTabsBy, getTabsData } from 'helpers';

const useStyles = makeStyles(filterbarStyles);

const FilterBar = React.memo(({
  showhidefavs,
  showhideFavourites,
  addItemsLocations,
  getLocationsService,
  getSalesOfficeService,
  getFilterLayouts,
  locations,
  setBusinessLocalAction,
  selections,
  addItemsCustomer,
  ...rest
}) => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const [snack, setSnack] = React.useState({
    open: false,
    severity: null,
    message: null
  });

  const [viewby, setViewBy] = React.useState(rest.showTabsBy);
  const [isFav, setFav] = React.useState(showhidefavs);
  const [allFavs, setAllFavs] = React.useState([...getTabsData(rest.favorites.customer, viewByConstants.customer), ...getTabsData(rest.favorites.locations, viewByConstants.locations) ]);

  React.useEffect(() => {
    setViewBy(rest.showTabsBy);
  }, [rest.showTabsBy])

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setSnack({
      ...snack,
      open: false
    });
  };


  const setShowFavs = (status) => {
    setFav(status);
    showhideFavourites(status);

    if (!status) {
      rest.removeFavs(rest.favorites.locations)
      rest.removeFavsCuso(rest.favorites.customer)
    } else {
      rest.joinFavstoCuso(rest.favorites.customer);
      addItemsLocations(rest.favorites.locations)
    }
  };

  React.useEffect(() => {
    const allSelections = [...getTabsData(selections.customer, viewByConstants.customer), ...getTabsData(selections.locations, viewByConstants.locations) ].map(d => d.custom.id);
    const allFavorites = [...getTabsData(rest.favorites.customer, viewByConstants.customer), ...getTabsData(rest.favorites.locations, viewByConstants.locations) ].map(d => d.custom.id);
    
    setAllFavs(allFavorites);
    setFav(intersection(allSelections, allFavorites).length !== 0)
    
  }, [selections, rest.favorites]);

  const handleClick = () => {
    setOpen((prev) => !prev);
  };

  React.useEffect(() => {
    if (!showhidefavs) {
      setFav(showhidefavs)
    }
  }, [showhidefavs])


  React.useEffect(() => {
    if (locations.length === 0) {
      getLocationsService();
    }
    getSalesOfficeService()
    getFilterLayouts()
  }, []);

  const isMobile = useMediaQuery(useTheme().breakpoints.down("sm"))


  return (
    <>
      <Grid container className={classes.root} >
        <Box mr={2} alignItems="center" className={clsx(classes.filter)}>
          <KCButton
            disableRipple
            endIcon={!open ? <ExpandMore /> : <ExpandLess />}
            className={classes.location}
            onClick={handleClick}
            color="primary"
          >
            Filter By
          </KCButton>
        </Box>
        {
          selections.locations.length > 0 && selections.customer.length > 0 ?
            <Box mr={2} alignItems="center" className={clsx(classes.filter)}>
                {!isMobile && <Typography className={classes.textHead}>Show tabs by </Typography>}
              <KCSelect customClasses={{ button: classes.location }} color="primary"
                value={viewby}
                onChange={(value) => {
                  setViewBy(value);
                  rest.setViewBy(value);
                }}
                label="Show tabs by"
                options={[
                  { name: "Location", value: "locations" },
                  { name: "Customer/Sales Office", value: "customer" },
                ]} />
            </Box> : null}

        {
          allFavs.length > 0 ?
            <Box mr={2} >
              <FormControlLabel
                className={classes.showfavs}
                color="primary.main"
                value="showfavs"
                control={
                  <IOSSwitch checked={isFav} switchEvent={setShowFavs} />
                }
                label={<Typography variant="subtitle1" color="primary" >{isFav ? 'Hide' : 'Show'} Favourites</Typography>}
                labelPlacement="start"
              /></Box> : null
        }
      </Grid>
      <LocationsFilterLayout open={open} handleClick={handleClick} />
      <Snack {...snack} handleClose={handleClose} />
    </>
  );
});

const mapStatetoProps = (state) => {
  const showTabsBy = defaultShowTabsBy(state);
  return {
    loading: state.sites.loading,
    showhidefavs: state.options.showfavs,
    user: state.auth.user || null,
    favsLoading: state.favorites.loading,
    cusoFavs: state.favorites.favCuso,
    favorites: { locations: state.favorites.favorites, customer: [...(get(state, "favorites.favCuso.FAV_CUST", [])), ...(get(state, "favorites.favCuso.FAV_SALES_OFFICE", []))] },
    locations: state.sites.locations || [],
    filterLayout: state.user.filterLayout,
    businessUnits: get(state, "favorites.tempBusinessUnit", []),
    selections: { locations: state.items.items, customer: [...(get(state, "items.cuso.CUST", [])), ...(get(state, "items.cuso.SALES_OFFICE", []))] },
    showTabsBy,
    optionsShowTabsBy: get(state, "options.showTabsBy", null)
  };
};

const mapDispatchToProps = {
  showhideFavourites,
  addItemsLocations: addItemsLocationsAction,
  removeFavs,
  addItem,
  removeItem,
  clearAll,
  getLocationsService,
  getSalesOfficeService,
  getFilterLayouts,
  setBusinessLocalAction,
  setViewBy,
  addItemsCustomer: addItemsCustomerAction,
  joinFavstoCuso,
  removeFavsCuso
};

export default connect(mapStatetoProps, mapDispatchToProps)(FilterBar);
