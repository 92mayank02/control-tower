import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reduxLib/reducers';
import { createBrowserHistory } from 'history' 
import { routerMiddleware } from 'connected-react-router'

export const history = createBrowserHistory()

const initialState = {
  user: {
    filterLayouts: {
      NETWORK: {
        tableName: "network",
        views: []
      }
    },
    currentView: {},
    initialLayouts: {}
  }
};

let middleware = [thunk, routerMiddleware(history)];
let enhancers = [applyMiddleware(...middleware)];

const configureStore = () => {
  return createStore(
    rootReducer(history),
    initialState,
    compose(...enhancers)
  );
}

export default configureStore;