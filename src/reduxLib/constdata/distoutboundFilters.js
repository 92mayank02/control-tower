const distoutboundfilters = {
    searchStringList: {
        type: "text",
        name: "Search",
        stringToArray: true,
        data: null
    },
    orderExecutionBucket: {
        type: "checkbox",
        name: "Shipment Status",
        data: [
            { name: "Shipment created", value: "SHIPMENT_CREATED", checked: false },
            { name: "Checked in by DC", value: "CHECK_IN", checked: false },
            { name: "Loading started", value: "LOADING_STARTED", checked: false },
            { name: "Ready for Pickup", value: "TRANS_EXEC_READY_PICK_UP", checked: false },
        ]
    },
    orderTypes: {
        type: "checkbox",
        name: "Order Type",
        data: [
            { name: "Cust. Order", value: "CUST", checked: false },
            { name: "STO", value: "STO", checked: false }
        ]
    },
    sites: {
        type: "sites",
        name: "Origin",
        data: null
    },
    destName: {
        type: "text",
        name: "Customer",
        data: null
    },
    destState: {
        type: "text",
        name: "Destination State",
        data: null
    },
    destCity: {
        type: "text",
        name: "Destination City",
        data: null
    },
    loadReadyDateTimeOriginTZ: {
        type: "date",
        dummytime: true,
        timeformat: "T",
        startTime: "[00:00:00.000]",
        endTime: "[23:59:59.999]",
        name: "Carrier Ready Date",
        shortName: 'Carr. Ready Date',
        data: null
    },
    loadStartDateTimeOriginTZ: {
        type: "date",
        dummytime: true,
        timeformat: "T",
        startTime: "[00:00:00.000]",
        endTime: "[23:59:59.999]",
        name: "Load Start Date",
        shortName: 'Load Start Date',
        data: null
    },
    tariffServiceCodeList : {
        type: "text",
        stringToArray: true,
        name: "Carrier Service Code",
        data: null
    },
    orderExecutionHealth: {
        type: "checkbox",
        name: "Health",
        data: [
            { name: "Unhealthy", value: "RED", checked: false },
            { name: "Needs Attention", value: "YELLOW", checked: false },
            { name: "Healthy", value: "GREEN", checked: false }

        ]
    },
    liveLoad: {
        type: "radio",
        name: "Live Load",
        data: null
    },
    materialNumberList: {
        type: "text",
        name: "Material ID",
        stringToArray: true,
        data: null
    }
}

export default distoutboundfilters;