import React from 'react';
import moment from "moment-timezone";
import { shortTitleCellStyle, longTitleCellStyle } from 'helpers/tableStyleOverride';
import { formatToSentenceCase } from "helpers";
import { get, isEmpty } from "lodash";
import { orderHealthReasonCodeMap } from "../../theme/orderHealthReasonCodeMap";


export const defaultTransportColumns = {
    columnOrder: [
        { title: 'Load #', field: "shipmentNum", cellStyle: shortTitleCellStyle },
        { title: 'Delivery #', field: "deliveryNum", cellStyle: shortTitleCellStyle },
        { title: 'Order #', field: "orderNum", cellStyle: shortTitleCellStyle },
        { title: 'Carrier', field: "tariffServiceCode" },
        { title: 'Shipment Status', field: "orderExecutionBucketDesc", cellStyle: shortTitleCellStyle },
        { title: 'TM Status', field: "tmsShipmentStatus", cellStyle: shortTitleCellStyle },
        { title: 'Carrier Ready Date & Time', field: "loadReadyDateTime", cellStyle: shortTitleCellStyle },
        { title: 'Origin ID', field: "originId" },
        { title: 'Origin', field: "origin", cellStyle: shortTitleCellStyle },
        { title: 'Customer', field: "customer", cellStyle: shortTitleCellStyle },
        { title: 'Live Load', field: 'liveLoadInd' },
        { title: 'Order On Hold', field: "orderOnHoldInd" },
        { title: 'Cust Confirmed Delivery Date & Time', field: "deliverApptDateTime", cellStyle: shortTitleCellStyle },
        { title: 'Dest City', field: "destinationCity" },
        { title: 'Dest St', field: "destinationState" },
        { title: 'Ship Condition', field: "shippingCondition" },
        { title: 'Appt Type', field: "appointmentType" },
        { title: 'Appt Req\'d', field: "appointmentRequired" },
        { title: 'Appt Status', field: "appointmentStatusDesc" },
        { title: 'Suspended Status', field: "shipmentSuspendedStatus", cellStyle: shortTitleCellStyle },
        { title: 'Short Lead', field: "tmsRushIndicator", cellStyle: shortTitleCellStyle },
        {
            title: 'Reason for Alert',
            field: "reasonCodeString",
            cellStyle: longTitleCellStyle,
            render: rowData => {
                const codeString = rowData?.orderExecutionReasonCodes?.map((code) => <div>{orderHealthReasonCodeMap[code]}</div>);
                return !isEmpty(codeString) ? codeString : "-";
            }
        },
        { title: 'Fourkites ETA', field: "expectedDeliveryDateTime", cellStyle: shortTitleCellStyle, disableClick: true },
        { title: 'Delivered Date & Time', field: "actualDeliveredDateTime", cellStyle: shortTitleCellStyle },
        { title: 'Loading Completion Date/Time', field: "loadingCompletionDateTime", cellStyle: shortTitleCellStyle },
        { title: 'Loading Completed on Time', field: 'loadingCompletedOnTime' },
        { title: 'Hours in Ready for Pickup', field: "hoursInReadyForPickup", cellStyle: shortTitleCellStyle },
    ],
    columnConfiguration: (d) => {
        return {
            origin: d?.orderOrigin?.name ? get(d, "orderOrigin.name", "-") : "-",
            originId: get(d, "orderOrigin.id", "-"),
            liveLoadInd: d.liveLoadInd ? d.liveLoadInd === "Y" ? "Yes" : "No" : "-",
            loadingCompletedOnTime: d.loadingCompletedOnTime ? d.loadingCompletedOnTime === "Y" ? "Yes" : "No" : "-",
            appointmentRequired: d.appointmentRequired ? d.appointmentRequired === "Y" ? "Yes" : "No" : "-",
            orderOnHoldInd: d.orderOnHoldInd ? d.orderOnHoldInd === "Y" ? "Yes" : "No" : "-",
            deliverApptDateTime: (d?.deliverApptDateTime && d?.destinationTimeZone) ? moment(d.deliverApptDateTime)?.tz(d.destinationTimeZone)?.format(`MMM DD YYYY HH:mm z`) : "-",
            expectedDeliveryDateTime: (d?.expectedDeliveryDateTime && d?.destinationTimeZone) ? moment(d.expectedDeliveryDateTime)?.tz(d.destinationTimeZone)?.format(`MMM DD YYYY HH:mm z`) : "-",
            loadReadyDateTime: (d?.loadReadyDateTime && d?.originTimeZone) ? moment(d?.loadReadyDateTime)?.tz(d?.originTimeZone)?.format(`MMM DD YYYY HH:mm z`) : "-",
            actualDeliveredDateTime:(d?.actualDeliveredDateTime && d?.destinationTimeZone) ? moment(d.actualDeliveredDateTime)?.tz(d.destinationTimeZone)?.format(`MMM DD YYYY HH:mm z`) : "-",
            loadingCompletionDateTime: (d?.loadingCompletionDateTime && d?.originTimeZone) ? moment(d?.loadingCompletionDateTime)?.tz(d?.originTimeZone)?.format(`MMM DD YYYY HH:mm z`) : "-",
            customer: formatToSentenceCase(get(d, "orderDestination.name", "-")),
            appointmentStatusDesc: formatToSentenceCase(get(d, "appointmentStatusDesc", "-")),
            appointmentType: formatToSentenceCase(get(d, "appointmentType", "-")),
            destinationCity: formatToSentenceCase(get(d, "orderDestination.city", "-")),
            destinationState: get(d, "orderDestination.state", "-"),
            shipmentSuspendedStatus: get(d, "shipmentSuspendedStatus", "-"),
            tmsRushIndicator: d.tmsRushIndicator ? d.tmsRushIndicator === "Y" ? "Yes" : "No" : "-",
            reasonCodeString: d?.orderExecutionReasonCodes?.reduce((codeString, code, index) => index === (d.orderExecutionReasonCodes.length - 1) ? `${codeString} ${orderHealthReasonCodeMap[code]}` : `${codeString} ${orderHealthReasonCodeMap[code]} |`, "") || "-"
        }
    }
}