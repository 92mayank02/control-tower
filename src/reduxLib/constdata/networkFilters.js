const networkfilters = {
    sites: {
        type: "sites",
        name: "Origin",
        data: null
    },
    searchStringList: {
        type: "text",
        name: "Search",
        stringToArray: true,
        data: null
    },
    destName: {
        type: "text",
        name: "Customer",
        data: null
    },
    destState: {
        type: "text",
        name: "Destination State",
        data: null
    },
    destCity: {
        type: "text",
        name: "Destination City",
        data: null
    },
    orderStatusBucket: {
        type: "checkbox",
        name: "Order Status",
        data: [
            { name: "Order Block Free", value: "ORDER_BLOCK_FREE", checked: false },
            { name: "Order Blocked", value: "ORDER_BLOCKED", checked: false },
        ]
    },
    orderTypes: {
        type: "checkbox",
        name: "Order Type",
        data: [
            { name: "Cust. Order", value: "CUST", checked: false },
            { name: "STO", value: "STO", checked: false }
        ]
    },
    tariffServiceCodeList : {
        type: "text",
        stringToArray: true,
        name: "Carrier Service Code",
        data: null
    },
    orderExecutionBucket: {
        type: "checkbox",
        name: "Shipment Status",
        data: [
            { name: "Transportation to be planned", value: "TRANS_PLAN_UNASSIGNED", checked: false },
            { name: "Transportation planning", value: "TRANS_PLAN_OPEN", checked: false },
            { name: "Transportation processing", value: "TRANS_PLAN_PROCESSING", checked: false },
            { name: "Transportation Carrier committed", value: "TRANS_PLAN_TENDER_ACCEPTED", checked: false },
            { name: "Shipment Created", value: "SHIPMENT_CREATED", checked: false },
            { name: "Checked in by DC", value: "CHECK_IN", checked: false },
            { name: "Loading Started", value: "LOADING_STARTED", checked: false },
            { name: "Ready for Pickup", value: "TRANS_EXEC_READY_PICK_UP", checked: false },
            { name: "In Transit", value: "TRANS_EXEC_IN_TRANSIT", checked: false },
            { name: "Delivery confirmed", value: "TRANS_EXEC_DELIVERY_CONFIRMED", checked: false }
        ]
    },
    inboundOrderExecutionBucket: {
        type: "checkbox",
        name: "Inbound Shipment Status",
        data: [
            { name: "In Transit", value: "INBOUND_IN_TRANSIT", checked: false },
            { name: "In Yard", value: "INBOUND_IN_YARD", checked: false },
            { name: "Unloading", value: "INBOUND_UNLOADING", checked: false },
            { name: "Unloading Completed", value: "INBOUND_UNLOADING_COMPLETED", checked: false },
        ]
    },
    loadReadyDateTimeOriginTZ: {
        type: "date",
        dummytime: true,
        timeformat: "T",
        startTime: "[00:00:00.000]",
        endTime: "[23:59:59.999]",
        name: "Carrier Ready Date",
        shortName: 'Carr. Ready Date',
        data: null
    },
    materialNumberList: {
        type: "text",
        name: "Material ID",
        stringToArray: true,
        data: null
    },
    matAvailDate: {
        type: "date",
        name: "Material Availability Date",
        shortName: 'Mat.Avail Date',
        data:null
    },
    originalRequestedDeliveryDate: {
        type: "date",
        name: "Original RDD",
        shortName: 'Original RDD',
        data: null
    },
    requestedDeliveryDate: {
        type: "date",
        name: "Requested Delivery Date",
        shortName: 'Req. Delivery Date',
        data: null
    },
    actualDeliveredDateTime: {
        type: "date",
        name: "Delivered Date/Time ",
        shortName: 'Delivered Date/Time ',
        data: null
    },
    deliveryApptDateTimeDestTZ: {
        type: "date",
        name: "Customer Confirmed Delivery Date",
        dummytime: true,
        timeformat: "T",
        startTime: "[00:00:00.000]",
        endTime: "[23:59:59.999]",
        shortName: 'Cust Confirmed Del. Date',
        data: null
    },
    orderHealthAndExecutionHealth: {
        type: "checkbox",
        name: "Health",
        data: [
            { name: "Unhealthy", value: "RED", checked: false },
            { name: "Needs Attention", value: "YELLOW", checked: false },
            { name: "Healthy", value: "GREEN", checked: false }

        ]
    },
}

export default networkfilters;