import React from 'react';
import moment from "moment-timezone";
import { shortTitleCellStyle, longTitleCellStyle } from 'helpers/tableStyleOverride';
import { formatToSentenceCase } from "helpers";
import { get, isEmpty } from "lodash";
import { orderHealthReasonCodeMap } from "../../theme/orderHealthReasonCodeMap";

const orderStatusBucket = {
    SO_COMPLETELY_CONFIRMED_CUBE: "100 % confirmed Cube",
    SO_NOT_COMPLETELY_CONFIRMED_CUBE: "Less than 100% confirmed Cube",
    SO_BLOCKED_TMS_PLANNED_VISIBLE: "Visible In tms",
    SO_BLOCKED_TMS_PLANNED_NOT_VISIBLE: "Not Visible In tms",
    SO_BLOCKED_NON_TMS_PLANNED_PICKUP_NOT_SCHEDULED: "Pickup Not Scheduled",
    SO_BLOCKED_NON_TMS_PLANNED_PICKUP_MULTI_BLOCK: "Pickup/package multi block",
    SO_BLOCKED_NON_TMS_PLANNED_IMMEDIATE_ACTION: "Immediate Action",
    SO_BACK_ORDER_BLOCK_FREE: "Back Orders",
    SO_BACK_ORDER_BLOCKED: "Back Orders"
  }

export const defaultNetworkColumns = {
    columnOrder: [
      { title: 'Shipment #', field: "shipmentNum", cellStyle: shortTitleCellStyle },
      { title: 'Delivery #', field: "deliveryNum", cellStyle: shortTitleCellStyle },
      { title: 'Shipment Status', field: "orderExecutionBucketDesc", cellStyle: shortTitleCellStyle },
      { title: 'Inbound Shipment Status', field: "inboundOrderExecutionBucketDesc", cellStyle: shortTitleCellStyle },
      { title: 'Order #', field: "orderNum", cellStyle: shortTitleCellStyle },
      { title: 'Order Status', field: "orderStatus", cellStyle: shortTitleCellStyle },
      { title: 'Order Type', field: "orderType" },
      { title: 'Customer', field: "customer" },
      { title: 'Origin ID', field: "originId" },
      { title: 'Origin', field: "origin", cellStyle: shortTitleCellStyle },
      { title: 'Dest City', field: "destinationCity" },
      { title: 'Dest State', field: "destinationState" },
      { title: 'Original RDD', field: "originalRequestedDeliveryDatetime", cellStyle: shortTitleCellStyle },
      { title: 'RDD', field: "requestedDeliveryDate", cellStyle: shortTitleCellStyle },
      { title: 'Carrier Ready Date & Time', field: "loadReadyDateTime", cellStyle: shortTitleCellStyle },
      { title: 'Carrier', field: "tariffServiceCode" },
      { title: 'Ship Condition', field: "shippingCondition" },
      { 
        title: 'Reason for Alert', 
        field: "reasonCodeString",
        cellStyle: longTitleCellStyle,
        render : rowData => {
          const codeString = rowData?.orderStatusAndExecutionStatusReasonCodes?.map((code) => <div>{orderHealthReasonCodeMap[code]}</div>);
          return !isEmpty(codeString) ? codeString : "-" ;
        }
      },
      { title: 'Fourkites ETA', field: "expectedDeliveryDateTime", cellStyle: shortTitleCellStyle, disableClick: true },

      { title: 'Sales Org', field: "salesOrg", cellStyle: shortTitleCellStyle },
      { title: 'Distribution Channel', field: "distributionChannel", cellStyle: shortTitleCellStyle },
      { title: 'Sales Office', field: "salesOffice", cellStyle: shortTitleCellStyle },
      { title: 'Sales Group', field: "salesGroup", cellStyle: shortTitleCellStyle },
      { title: 'MAD', field: "matAvailDate", cellStyle: shortTitleCellStyle },
      { title: 'Cust Confirmed Delivery Date & Time', field: "deliverApptDateTime", cellStyle: shortTitleCellStyle },
      { title: 'Delivered Date & Time', field: "actualDeliveredDateTime", cellStyle: shortTitleCellStyle },
    ],
    columnConfiguration: (d) => {
  
      return {
        liveLoadInd: d.liveLoadInd ? d.liveLoadInd === "Y" ? "Yes" : "No" : "-",
        appointmentRequired: d.appointmentRequired ? d.appointmentRequired === "Y" ? "Yes" : "No" : "-",
        orderOnHoldInd: d.orderOnHoldInd ? d.orderOnHoldInd === "Y" ? "Yes" : "No" : "-",
        deliverApptDateTime: (d?.deliverApptDateTime && d?.destinationTimeZone) ? moment(d.deliverApptDateTime)?.tz(d.destinationTimeZone)?.format(`MMM DD YYYY HH:mm z`) : "-",
        expectedDeliveryDateTime: (d?.expectedDeliveryDateTime && d?.destinationTimeZone) ? moment(d.expectedDeliveryDateTime)?.tz(d.destinationTimeZone)?.format(`MMM DD YYYY HH:mm z`) : "-",
        loadReadyDateTime: (d?.loadReadyDateTime && d?.originTimeZone) ? moment(d?.loadReadyDateTime)?.tz(d?.originTimeZone)?.format(`MMM DD YYYY HH:mm z`) : "-",
        actualDeliveredDateTime:(d?.actualDeliveredDateTime && d?.destinationTimeZone) ? moment(d.actualDeliveredDateTime)?.tz(d.destinationTimeZone)?.format(`MMM DD YYYY HH:mm z`) : "-",
        customer: formatToSentenceCase(get(d, "orderDestination.name", "-")),
        origin: d?.orderOrigin?.name ? get(d, "orderOrigin.name", "-") : "-" ,
        originId: get(d, "orderOrigin.id", "-"),
        destinationCity: formatToSentenceCase(get(d, "orderDestination.city", "-")),
        destinationState: get(d, "orderDestination.state", "-"),
        orderStatusBucket: orderStatusBucket[d.orderStatusBucket] || "-",
        shippingCondition: get(d, "shippingCondition", "-"),
        reasonCodeString: d?.orderStatusAndExecutionStatusReasonCodes?.reduce((codeString,code,index) => index === (d.orderStatusAndExecutionStatusReasonCodes.length - 1) ? `${codeString} ${orderHealthReasonCodeMap[code]}` : `${codeString} ${orderHealthReasonCodeMap[code]} |`, "") || "-",
        salesOrg: get(d, "salesOrg", "-"),
        distributionChannel: get(d, "distributionChannel", "-"),
        salesOffice: get(d, "salesOffice", "-"),
        salesGroup: get(d, "salesGroup", "-")

      }
    }
  }