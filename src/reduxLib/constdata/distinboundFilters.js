
const distinboundfilters = {
    searchStringList: {
        type: "text",
        stringToArray: true,
        name: "Search",
        data: null
    },
    inboundOrderExecutionBucket: {
        type: "checkbox",
        name: "Shipment Status",
        data: [
            { name: "In Transit", value: "INBOUND_IN_TRANSIT", checked: false },
            { name: "In Yard", value: "INBOUND_IN_YARD", checked: false },
            { name: "Unloading", value: "INBOUND_UNLOADING", checked: false },
            { name: "Unloading Completed", value: "INBOUND_UNLOADING_COMPLETED", checked: false },
        ]
    },
    destState: {
        type: "text",
        name: "Destination State",
        data: null
    },
    destCity: {
        type: "text",
        name: "Destination City",
        data: null
    },
    yardArrivalDateTimeDestTZ: {
        type: "date",
        dummytime: true,
        timeformat: "T",
        startTime: "[00:00:00.000]",
        endTime: "[23:59:59.999]",
        name: "Date Arrived in Yard",
        shortName: 'Yard Arrival',
        data: null
    },
    durationInYard: {
        type: "checkboxradio",
        name: "Duration in Yard",
        data: [
            { name: "< 24 Hours", value: { startTimeMins: null, endTimeMins: 1439 }, checked: false },
            { name: "24 - 48 Hours", value: { startTimeMins: 1440, endTimeMins: 2880 }, checked: false },
            { name: "> 72 Hours", value: { startTimeMins :4321 , endTimeMins: null }, checked: false },
        ]
    },
    inboundOrderExecutionHealth: {
        type: "checkbox",
        name: "Health",
        data: [
            { name: "Unhealthy", value: "RED", checked: false },
            { name: "Needs Attention", value: "YELLOW", checked: false },
            { name: "Healthy", value: "GREEN", checked: false }

        ]
    },
    materialNumberList: {
        type: "text",
        name: "Material ID",
        stringToArray: true,
        data: null
    }
}

export default distinboundfilters;
