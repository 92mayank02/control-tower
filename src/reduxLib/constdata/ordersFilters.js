import moment from "moment-timezone";

const ordersfilters = {
    searchStringList: {
        type: "text",
        name: "Search",
        stringToArray: true,
        data: null
    },
    destName: {
        type: "text",
        name: "Customer",
        data: null
    },
    sites: {
        type: "sites",
        name: "Origin",
        data: null
    },
    shippingConditionList: {
        type: "text",
        stringToArray: true,
        name: "Shipping Condition",
        data: null
    },
    requestedDeliveryDate: {
        type: "date",
        name: "Requested Delivery Date",
        shortName: 'Req. Delivery Date',
        data: null
    },
    deliveryBlockCodeList: {
        type: "textcheck",
        stringToArray: true,
        name: "Delivery/Credit Block",
        placeholder: 'Enter delivery block code',
        shortName: "Delivery Block Code",
        addon: "creditOnHold",
        data: null
    },
    creditOnHold: {
        hidden: true,
        type: "text",
        name: "Show Only Credit Hold Orders",
        data: null
    },
    confirmedEquivCubes: {
        type: "checkbox",
        name: "Confirmed Cube",
        shortName: 'Confirmed Cube',
        data: [
            { name: "< 1000", value: { lte: 999.9999999 }, checked: false },
            { name: "1000 - 2000", value: { gte: 1000, lte: 1999.9999999 }, checked: false },
            { name: "2000 - 3000", value: { gte: 2000, lte: 2999.9999999 }, checked: false },
            { name: "> 3000", value: { gte: 3000 }, checked: false },
        ]
    },
    destState: {
        type: "text",
        name: "Destination State",
        data: null
    },
    destCity: {
        type: "text",
        name: "Destination City",
        data: null
    },
    matAvailDate: {
        type: "date",
        name: "Material Availability Date",
        shortName: 'Mat.Avail Date',
        data: {
            startTime: moment().format("YYYY-MM-DD"),
            endTime: moment().add(2, "days").format("YYYY-MM-DD")
        }
    },
    actualDeliveredDateTime: {
        type: "date",
        name: "Delivered Date/Time ",
        shortName: 'Delivered Date/Time ',
        data: null
    },
    orderStatusBucket: {
        type: "ordercheckbox",
        name: "Order Status",
        data: [
            { name: "100% Confirmed Cube", value: "SO_COMPLETELY_CONFIRMED_CUBE", checked: false, parent: "Order Block Free", },
            { name: "Less than 100% Confirmed Cube", value: "SO_NOT_COMPLETELY_CONFIRMED_CUBE", checked: false, parent: "Order Block Free", },
            { name: "Back Orders Block Free", value: "SO_BACK_ORDER_BLOCK_FREE", checked: false, parent: "Order Block Free", },
            { name: "Visible in TMS", value: "SO_BLOCKED_TMS_PLANNED_VISIBLE", checked: false, parent: "Orders Blocked" },
            { name: "Not visible in TMS", value: "SO_BLOCKED_TMS_PLANNED_NOT_VISIBLE", checked: false, parent: "Orders Blocked" },
            { name: "Pickup not scheduled", value: "SO_BLOCKED_NON_TMS_PLANNED_PICKUP_NOT_SCHEDULED", checked: false, parent: "Orders Blocked" },
            { name: "Pickup/Package Multi Block", value: "SO_BLOCKED_NON_TMS_PLANNED_PICKUP_MULTI_BLOCK", checked: false,parent: "Orders Blocked" },
            { name: "Immediate Action", value: "SO_BLOCKED_NON_TMS_PLANNED_IMMEDIATE_ACTION", checked: false, parent: "Orders Blocked" },
            { name: "Back Orders Blocked", value: "SO_BACK_ORDER_BLOCKED", checked: false, parent: "Orders Blocked" },
        ]
    },
    orderExecutionBucket: {
        type: "checkbox",
        name: "Shipment Status",
        data: [
            { name: "Transportation to be planned", value: "TRANS_PLAN_UNASSIGNED", checked: false },
            { name: "Transportation planning", value: "TRANS_PLAN_OPEN", checked: false },
            { name: "Transportation processing", value: "TRANS_PLAN_PROCESSING", checked: false },
            { name: "Transportation Carrier committed", value: "TRANS_PLAN_TENDER_ACCEPTED", checked: false },
            { name: "Shipment Created", value: "SHIPMENT_CREATED", checked: false },
            { name: "Checked in by DC", value: "CHECK_IN", checked: false },
            { name: "Loading Started", value: "LOADING_STARTED", checked: false },
            { name: "Ready for Pickup", value: "TRANS_EXEC_READY_PICK_UP", checked: false },
            { name: "In Transit", value: "TRANS_EXEC_IN_TRANSIT", checked: false },
            { name: "Delivery confirmed", value: "TRANS_EXEC_DELIVERY_CONFIRMED", checked: false }
        ]
    },
    inboundOrderExecutionBucket: {
        type: "checkbox",
        name: "Inbound Shipment Status",
        data: [
            { name: "In Transit", value: "INBOUND_IN_TRANSIT", checked: false },
            { name: "In Yard", value: "INBOUND_IN_YARD", checked: false },
            { name: "Unloading", value: "INBOUND_UNLOADING", checked: false },
            { name: "Unloading Completed", value: "INBOUND_UNLOADING_COMPLETED", checked: false },
        ]
    },
    orderTypes: {
        type: "checkbox",
        name: "Order Type",
        data: [
            { name: "All Customer Orders", value: "CUST", checked: true },
            { name: "STOs", value: "STO", checked: false }
        ]
    },
    documentTypes: {
        type: "checkbox",
        name: "Document type",
        data: [
            { name: "Standard Orders (ZSTD)", value: "ZSTD", checked: false },
            { name: "Multi Plant Orders (ZMPF)", value: "ZMPF", checked: false },
            { name: "VMI Orders (ZVMI)", value: "ZVMI", checked: false },
            { name: "Merged Orders (ZMER)", value: "ZMER", checked: false }
        ]
    },
    orderHealth: {
        type: "checkbox",
        name: "Health",
        data: [
            { name: "Unhealthy", value: "RED", checked: false },
            { name: "Needs Attention", value: "YELLOW", checked: false },
            { name: "Healthy", value: "GREEN", checked: false }

        ]
    },
    materialNumberList: {
        type: "text",
        name: "Material ID",
        stringToArray: true,
        data: null
    }
}

export default ordersfilters;