import React from 'react';
import moment from "moment-timezone";
import { shortTitleCellStyle, longTitleCellStyle } from 'helpers/tableStyleOverride';
import { formatToSentenceCase, msToHMS } from "helpers";
import { get, isEmpty, isArray } from "lodash";
import { orderHealthReasonCodeMap } from "../../theme/orderHealthReasonCodeMap";


export const defaultOutboundColumns = {
    columnOrder : [
        { title: 'Shipment #', field: "shipmentNum", cellStyle: shortTitleCellStyle },
        { title: 'Delivery #', field: "deliveryNum", cellStyle: shortTitleCellStyle },
        { title: 'Shipment Status', field: "orderExecutionBucketDesc", cellStyle: shortTitleCellStyle },
        { title: 'Order #', field: "orderNum", cellStyle: shortTitleCellStyle },
        { title: 'Carrier Ready Date & Time', field: "loadReadyDateTime", cellStyle: shortTitleCellStyle },
        { title: 'Load Start Date & Time', field: "loadingStartDateTime", cellStyle: shortTitleCellStyle },
        { title: 'Estd Load Hrs Rem', field: "remainingLoadTimeMS", cellStyle: shortTitleCellStyle },
        { title: 'Total Load Hrs', field: "estimatedLoadTimeMS", cellStyle: shortTitleCellStyle },
        { title: 'Customer', field: "customer", cellStyle: shortTitleCellStyle },
        { title: 'Origin ID', field: "originId" },
        { title: 'Origin', field: "origin", cellStyle: shortTitleCellStyle },
        { title: 'Order Status', field: "orderStatus", cellStyle: shortTitleCellStyle },
        { title: 'Order Type', field: 'orderType' },
        { title: 'Shipping Condition', field: "shippingCondition", cellStyle: shortTitleCellStyle },
        { title: 'Live Load', field: "liveLoadInd" },
        { title: 'Carrier', field: "tariffServiceCode" },
        { title: 'Dest City', field: "destinationCity" },
        { title: 'Dest State', field: "destinationState" },
        { 
            title: 'Business', 
            field: "orderBusinessUnit",
            render : rowData => {
              const codeString = isArray(rowData?.orderBusinessUnit) ? rowData?.orderBusinessUnit?.map((bu) => <div>{bu}</div>) : rowData?.orderBusinessUnit ;
              return !isEmpty(codeString) ? codeString : "-" ;
            }
        },
        { 
            title: 'Reason for Alert', 
            field: "reasonCodeString",
            cellStyle: longTitleCellStyle,
            render : rowData => {
              const codeString = rowData?.orderExecutionReasonCodes?.map((code) => <div>{orderHealthReasonCodeMap[code]}</div>);
              return !isEmpty(codeString) ? codeString : "-" ;
            }
        }

      ],
    columnConfiguration : (d) => {
        return {
            origin: d?.orderOrigin?.name ? get(d, "orderOrigin.name", "-") : "-" ,
            originId: get(d, "orderOrigin.id", "-"),
            liveLoadInd: d.liveLoadInd ? d.liveLoadInd === "Y" ? "Yes" : "No" : "-",
            appointmentRequired: d.appointmentRequired ? d.appointmentRequired === "Y" ? "Yes" : "No" : "-",
            orderOnHoldInd: d.orderOnHoldInd ? d.orderOnHoldInd === "Y" ? "Yes" : "No" : "-",
            // deliverApptDateTime: d?.deliverApptDateTime ? moment(d.deliverApptDateTime)?.tz(d?.destinationTimeZone)?.format(`MMM DD YYYY HH:mm z`) : "-",
            loadingStartDateTime: d?.loadingStartDateTime ? moment(d?.loadingStartDateTime)?.tz(d?.originTimeZone)?.format(`MMM DD YYYY HH:mm z`) : "-",
            loadReadyDateTime: d?.loadReadyDateTime ? moment(d?.loadReadyDateTime)?.tz(d?.originTimeZone)?.format(`MMM DD YYYY HH:mm z`) : "-",
            customer: formatToSentenceCase(get(d, "orderDestination.name", "-")),
            destinationCity: formatToSentenceCase(get(d, "orderDestination.city", "-")),
            destinationState: get(d, "orderDestination.state", "-"),
            remainingLoadTimeMS: msToHMS(parseInt(get(d, "remainingLoadTimeMS", 0)), d.remainingLoadTimeMS),
            estimatedLoadTimeMS: msToHMS(parseInt(get(d, "estimatedLoadTimeMS", 0)), d.estimatedLoadTimeMS),
            shippingCondition: get(d, "shippingCondition", "-"),
            reasonCodeString: d?.orderExecutionReasonCodes?.reduce((codeString,code,index) => index === (d.orderExecutionReasonCodes.length - 1) ? `${codeString} ${orderHealthReasonCodeMap[code]}` : `${codeString} ${orderHealthReasonCodeMap[code]} |`, "") || "-"

        }
    }    
}

  