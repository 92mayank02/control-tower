import React from 'react';
import moment from "moment-timezone";
import { shortTitleCellStyle, longTitleCellStyle } from 'helpers/tableStyleOverride';
import { formatToSentenceCase, msToHMS } from "helpers";
import { get, isEmpty, isArray } from "lodash";
import { orderHealthReasonCodeMap } from "../../theme/orderHealthReasonCodeMap";

export const defaultInboundColumns = {
    columnOrder :  [
        { title: 'Shipment #', field: "shipmentNum", cellStyle: shortTitleCellStyle },
        { title: 'Delivery #', field: "deliveryNum", cellStyle: shortTitleCellStyle },
        { title: 'Order #', field: "orderNum", cellStyle: shortTitleCellStyle },
        { title: 'Origin ID', field: "originId" },
        { title: 'Origin', field: "origin" },
        { title: 'Dest', field: "customer", cellStyle: shortTitleCellStyle },
        { title: 'Date and Time arrived in Yard', field: 'yardArrivalDateTime', cellStyle: shortTitleCellStyle },
        { title: 'Duration in Yard', field: "durationInYardMS" },
        { title: 'Trailer # ', field: "trailerNum" },
        { title: 'Shipment Status', field: "inboundOrderExecutionBucketDesc" },
        { title: 'Ship Condition', field: "shippingCondition" },
        { title: 'Dest City', field: "destinationCity" },
        { title: 'Dest State', field: "destinationState" },
        { 
            title: 'Business', 
            field: "orderBusinessUnit",
            cellStyle: longTitleCellStyle,
            render : rowData => {
              const codeString = isArray(rowData?.orderBusinessUnit) ? rowData?.orderBusinessUnit?.map((bu) => <div>{bu}</div>) : rowData?.orderBusinessUnit ;
              return !isEmpty(codeString) ? codeString : "-" ;
            }
        },
        { 
            title: 'Reason for Alert', 
            field: "reasonCodeString",
            render : rowData => {
              const codeString = rowData?.inboundOrderExecutionReasonCodes?.map((code) => <div>{orderHealthReasonCodeMap[code]}</div>);
              return !isEmpty(codeString) ? codeString : "-" ;
            }
        },
        { title: 'Fourkites ETA', field: "expectedDeliveryDateTime", cellStyle: shortTitleCellStyle, disableClick: true }
    ],
    columnConfiguration : (d) => {
        return {
            origin: d?.orderOrigin?.name ? get(d, "orderOrigin.name", "-") : "-" ,
            originId: get(d, "orderOrigin.id", "-"),
            liveLoadInd: d.liveLoadInd ? d.liveLoadInd === "Y" ? "Yes" : "No" : "-",
            appointmentRequired: d.appointmentRequired ? d.appointmentRequired === "Y" ? "Yes" : "No" : "-",
            orderOnHoldInd: d.orderOnHoldInd ? d.orderOnHoldInd === "Y" ? "Yes" : "No" : "-",
            deliverApptDateTime: d?.deliverApptDateTime ? moment(d.deliverApptDateTime)?.tz(d.destinationTimeZone)?.format(`MMM DD YYYY HH:mm z`) : "-",
            expectedDeliveryDateTime: (d?.expectedDeliveryDateTime && d?.destinationTimeZone) ? moment(d.expectedDeliveryDateTime)?.tz(d.destinationTimeZone)?.format(`MMM DD YYYY HH:mm z`) : "-",
            loadReadyDateTime: d?.loadReadyDateTime ? moment(d?.loadReadyDateTime)?.tz(d?.originTimeZone)?.format(`MMM DD YYYY HH:mm z`) : "-",
            yardArrivalDateTime: d?.yardArrivalDateTime ? moment(d?.yardArrivalDateTime)?.tz(d?.destinationTimeZone)?.format(`MMM DD YYYY HH:mm z`) : "-",
            customer: formatToSentenceCase(get(d, "orderDestination.name", "-")),
            destinationCity: formatToSentenceCase(get(d, "orderDestination.city", "-")),
            destinationState: get(d, "orderDestination.state", "-"),
            durationInYardMS: msToHMS(parseInt(get(d, "durationInYardMS", 0))),
            shippingCondition: get(d, "shippingCondition", "-"),
            reasonCodeString: d?.inboundOrderExecutionReasonCodes?.reduce((codeString,code,index) => index === (d.inboundOrderExecutionReasonCodes.length - 1) ? `${codeString} ${orderHealthReasonCodeMap[code]}` : `${codeString} ${orderHealthReasonCodeMap[code]} |`, "") || "-"

        }
    }    

}

  