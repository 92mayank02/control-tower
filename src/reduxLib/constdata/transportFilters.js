import moment from "moment-timezone";

const transportfilters = {
    searchStringList: {
        type: "text",
        name: "Search",
        stringToArray: true,
        data: null
    },
    sites: {
        type: "sites",
        name: "Origin",
        data: null
    },
    destName: {
        type: "text",
        name: "Customer",
        data: null
    },
    orderExecutionBucket: {
        type: "checkbox",
        name: "Shipment Status",
        data: [
            { name: "Transportation to be planned", value: "TRANS_PLAN_UNASSIGNED", checked: false },
            { name: "Transportation planning", value: "TRANS_PLAN_OPEN", checked: false },
            { name: "Transportation processing", value: "TRANS_PLAN_PROCESSING", checked: false },
            { name: "Transportation Carrier committed", value: "TRANS_PLAN_TENDER_ACCEPTED", checked: false },
            { name: "Ready for Pickup", value: "TRANS_EXEC_READY_PICK_UP", checked: false },
            { name: "In Transit", value: "TRANS_EXEC_IN_TRANSIT", checked: false },
            { name: "Delivery confirmed", value: "TRANS_EXEC_DELIVERY_CONFIRMED", checked: false }
        ]
    },
    tmsShipmentStatus: {
        type: "checkbox",
        name: "TM Operational Status",
        data: [
            { name: "Unassigned", value: "Unassigned", checked: false },
            { name: "Open", value: "Open", checked: false },
            { name: "Planned", value: "Planned", checked: false },
            { name: "Tendered", value: "Tendered", checked: false },
            { name: "Tender Rejected", value: "Tender Rejected", checked: false },
            { name: "Tender Accepted", value: "Tender Accepted", checked: false },
            { name: "Confirming", value: "Confirming", checked: false },
            { name: "In Transit", value: "In Transit", checked: false },
            { name: "Completed", value: "Completed", checked: false }
        ]
    },
    appointmentType: {
        type: "checkbox",
        name: "Appointment Type",
        data: [
            { name: "Manual", value: "MANUAL", checked: false },
            { name: "Automated", value: "AUTOMATED", checked: false }
        ]
    },
    destState: {
        type: "text",
        name: "Destination State",
        data: null
    },
    destCity: {
        type: "text",
        name: "Destination City",
        data: null
    },
    appointmentRequired: {
        type: "radio",
        name: "Appointment Required",
        data: null
    },
    appointmentStatus: {
        type: "checkbox",
        name: "Appointment Status",
        data: [
            { name: "Notified", value: "NOTIFIED", checked: false },
            { name: "Planned", value: "PLANNED", checked: false },
            { name: "Suggested", value: "SUGGESTED", checked: false },
            { name: "Confirmed", value: "CONFIRMED", checked: false },
            { name: "Not yet contacted", value: "NOT_YET_CONTACTED", checked: false },
            { name: "Appt not made by Transport", value: "NOT_MADE_BY_TRANSPORT", checked: false },
        ]
    },
    liveLoad: {
        type: "radio",
        name: "Live Load",
        data: null
    },
    equipmentTypeList: {
        type: "checkbox",
        name: "Equipment Type",
        data: [
            { name: "Full Truck Load", value: "FULL_TRUCK_LOAD", checked: false },
            { name: "Less Than Truck Load", value: "LESS_THAN_TRUCK_LOAD", checked: false }
        ]
    },
    shippingConditionList: {
        type: "checkbox",
        name: "Ship Condition",
        data: [
            { name: "Truck Load (TL)", value: "TL", checked: false },
            { name: "Intermodal (TF)", value: "TF", checked: false },
            { name: "Open (OP) ", value: "OP", checked: false },
            { name: "Package (PG)", value: "PG", checked: false },
            { name: "Customer Pickup (PU)", value: "PU", checked: false }
        ]
    },
    onHold: {
        type: "radio",
        name: "Order on Hold",
        data: null
    },
    deliveryApptDateTimeDestTZ: {
        type: "date",
        name: "Delivery Date",
        dummytime: true,
        timeformat: "T",
        startTime: "[00:00:00.000]",
        endTime: "[23:59:59.999]",
        shortName: 'Del. Date',
        data: null
    },
    loadReadyDateTimeOriginTZ: {
        type: "date",
        name: "Carrier Ready Date",
        shortName: 'Car.Ready Date',
        dummytime: true,
        timeformat: "T",
        startTime: "[00:00:00.000]",
        endTime: "[23:59:59.999]",
        data: {
            startTime: moment().format("YYYY-MM-DD [00:00:00.000]"),
            endTime: moment().add(2, "days").format("YYYY-MM-DD [23:59:59.000]")
        }
    },
    tariffServiceCodeList : {
        type: "text",
        name: "Carrier Service Code",
        stringToArray: true,
        data: null
    },
    orderExecutionHealth: {
        type: "checkbox",
        name: "Health",
        data: [
            { name: "Unhealthy", value: "RED", checked: false },
            { name: "Needs Attention", value: "YELLOW", checked: false },
            { name: "Healthy", value: "GREEN", checked: false }

        ]
    },
    loadingCompletedOnTime: {
        type: "radio",
        name: "Loading Completed on Time",
        data: null
    },
    actualDeliveredDateTime: {
        type: "date",
        name: "Delivered Date/Time ",
        shortName: 'Delivered Date/Time ',
        data: null
    }
}

export default transportfilters;