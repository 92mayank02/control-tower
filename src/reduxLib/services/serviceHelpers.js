import config from "configs";

const fetchService = async (url, options, timeout = 30000) => {
    const authService = global.authService;

    try {

        let token = await authService.getIdToken();
        let authState = await authService.getAuthState();

        if (!authState?.isAuthenticated || !token) {
            await authService.getTokenManager().renew("idToken")
            authState = await authService.getAuthState();
            const tokenObject = await authService.getTokenManager().get('idToken');
            token = tokenObject?.idToken;
            if (!token || !authState?.isAuthenticated) {
                authService.logout();
            }
        }

        const tokenData = `Bearer ${token}`;
        const tabId = sessionStorage.getItem("Tab-Id");
        const deviceType = sessionStorage.getItem("Device-Type");
        const { api_base_path } = config;
        let optionsModified = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': tokenData,
                //"accessKey": "abcd",
                "X-Tab-Id": tabId,
                "X-Device-Type": deviceType
            }
        }
        if (options) {
            optionsModified = { ...options, ...optionsModified };
            if (options.body) {
                let body = options.body;
                if (body?.businessUnits && body?.businessUnits?.length === 0) {
                    body = {
                        ...body,
                        businessUnits: ["CONSUMER", "PROFESSIONAL"]
                    }
                }
                optionsModified = {
                    ...optionsModified, body: JSON.stringify({
                        ...body
                    })
                };
            }
        }

        return Promise.race([
            fetch(`${api_base_path}${url}`, optionsModified),
            new Promise((_, reject) =>
                setTimeout(() => reject(new Error('timeout')), timeout)
            )
        ]);
    } catch (e) {
        authService.redirect({
            sessionToken: await authService.getIdToken()
          });
        return new Promise((_, reject) => reject(new Error('Unable to Process Request')))
    }
}

export default fetchService;