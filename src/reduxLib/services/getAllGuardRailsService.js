import { guardrailsConstants } from "../constants";
import fetch from "./serviceHelpers";
import { endpoints } from "helpers";

export const getAllGuardrails = () => dispatch => {
    dispatch({
        type: guardrailsConstants.GUARDRAILS_FETCH_LOADING,
        payload: true
    });

    let allowedToEditFlag = false

    return fetch(endpoints.guardrails.allData, {})
        .then(d => {    
            if(d.status === 200 || d.status === 401 ){
                allowedToEditFlag = d.status === 200 ? true : false
                return d.json()
            }
            else throw Error(d)
        }
        )
        .then(d => {
            dispatch({
                type: guardrailsConstants.GUARDRAILS_FETCH_SUCCESS,
                payload: {data:d, allowedToEdit:allowedToEditFlag}
            });
            dispatch({
                type: guardrailsConstants.GUARDRAILS_FETCH_LOADING,
                payload: false
            })
        })
        .catch(err => {
            dispatch({
                type: guardrailsConstants.GUARDRAILS_FETCH_FAILED,
                payload: err
            });
            dispatch({
                type: guardrailsConstants.GUARDRAILS_FETCH_LOADING,
                payload: false
            });
        }).finally(()=> allowedToEditFlag = false)
}



export const saveGuardrails = (variables) => dispatch => {
    dispatch({
        type: guardrailsConstants.GUARDRAILS_UPDATE_LOADING,
        payload: true
    });

    //Snapshot API

    return fetch(endpoints.guardrails.update,{
        method: "PUT",
        body: variables,
        })
        .then(d => {    
            if(d.status === 200) return d.statusText
            else throw Error(d)
        }
        )
        .then(d => {
            dispatch({
                type: guardrailsConstants.GUARDRAILS_UPDATE_SUCCESS,
                payload: variables
            });
            dispatch({
                type: guardrailsConstants.GUARDRAILS_UPDATE_LOADING,
                payload: false
            })
        })
        .catch(err => {
            dispatch({
                type: guardrailsConstants.GUARDRAILS_UPDATE_FAILED,
                payload: err
            });
            dispatch({
                type: guardrailsConstants.GUARDRAILS_UPDATE_LOADING,
                payload: false
            });
        })
}


