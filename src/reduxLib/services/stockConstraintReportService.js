import { stockConstraintReportConstants } from "../constants"
import fetch from "./serviceHelpers";
import { endpoints } from "helpers";

export const fetchShipmentList = variables => dispatch => {
    dispatch({
        type: stockConstraintReportConstants.SHIPMENT_LIST_LOADING,
        payload: true
    })

    return fetch(endpoints.stockConstraintReport.shipmentList, {
        method: "POST",
        body: variables,
    })
        .then(data => {
            if (data.status === 200) {
                return data.json()
            } else {
                throw Error(data);
            }
        })
        .then(data => {
            dispatch({
                type: stockConstraintReportConstants.FETCH_SHIPMENT_LIST_SUCCESS,
                payload: data
            })

        })
        .catch(err => {
            dispatch({
                type: stockConstraintReportConstants.SHIPMENT_LIST_FAILED,
                payload: []
            })
        })
        .finally(() => {
            dispatch({
                type: stockConstraintReportConstants.SHIPMENT_LIST_LOADING,
                payload: false
            })
        })
}

export const fetchShipmentMaterial = variables => dispatch => {
    dispatch({
        type: stockConstraintReportConstants.SHIPMENT_MATERIALS_LOADING,
        payload: true
    })

    return fetch(endpoints.stockConstraintReport.shipmentMaterials, {
        method: "POST",
        body: variables,
    })
        .then(data => {
            if (data.status === 200) {
                return data.json()
            } else {
                throw Error(data);
            }
        })
        .then(data => {
            dispatch({
                type: stockConstraintReportConstants.FETCH_SHIPMENT_MATERIALS_SUCCESS,
                payload: data
            })
        })
        .catch(err => {
            dispatch({
                type: stockConstraintReportConstants.SHIPMENT_MATERIALS_FAILED,
                payload: {}
            })
        })
        .finally(() => {
            dispatch({
                type: stockConstraintReportConstants.SHIPMENT_MATERIALS_LOADING,
                payload: false
            })
        })
}

export const searchShipment = variables => dispatch => {
    dispatch({
        type: stockConstraintReportConstants.SEARCH_SHIPMENT_LOADING,
        payload: true
    })

    return fetch(endpoints.stockConstraintReport.searchShipment, {
        method: "POST",
        body: variables,
    })
        .then(data => {
            if (data.status === 200) {
                return data.json()
            } else {
                throw Error(data);
            }
        })
        .then(data => {
            dispatch({
                type: stockConstraintReportConstants.SEARCH_SHIPMENT_SUCCESS,
                payload: data
            })
        })
        .catch(err => {
            dispatch({
                type: stockConstraintReportConstants.SEARCH_SHIPMENT_FAILED,
                payload: {}
            })
        })
        .finally(() => {
            dispatch({
                type: stockConstraintReportConstants.SEARCH_SHIPMENT_LOADING,
                payload: false
            })
        })
}

export const fetchRecomendation = variables => dispatch => {
    dispatch({
        type: stockConstraintReportConstants.RECOMENDATIONS_LOADING,
        payload: true
    })

    return fetch(endpoints.stockConstraintReport.shipmentMaterialRecomendation, {
        method: "POST",
        body: variables,
    })
        .then(data => {
            if (data.status === 200) {
                return data.json()
            } else {
                throw Error(data);
            }
        })
        .then(data => {
            dispatch({
                type: stockConstraintReportConstants.FETCH_RECOMENDATIONS_SUCCESS,
                payload: data
            })
        })
        .catch(err => {
            dispatch({
                type: stockConstraintReportConstants.RECOMENDATIONS_FAILED,
                payload: []
            })
        })
        .finally(() => {
            dispatch({
                type: stockConstraintReportConstants.RECOMENDATIONS_LOADING,
                payload: false
            })
        })
}