export default {
    CHART_DATA_FETCH_SUCCESS: 'CHART_DATA_FETCH_SUCCESS',
    CHART_DATA_FETCH_FAILED: 'CHART_DATA_FETCH_FAILED',
    LOADING: 'CHART_LOADING',
    GUARDRAIL_DATA_FETCH_SUCCESS: 'GUARDRAIL_DATA_FETCH_SUCCESS',
    GUARDRAIL_DATA_FETCH_FAILED: 'GUARDRAIL_DATA_FETCH_FAILED',
    GUARDRAIL_LOADING: 'GUARDRAIL_LOADING'
}