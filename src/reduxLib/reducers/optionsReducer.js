import { optionsConstants } from '../constants';
import { transportfilters, distoutboundfilters, distinboundfilters, ordersfilters, networkfilters, stockConstraintReportFilter } from "../constdata/filters";
import { get, isEmpty } from "lodash";

const defaultdata = {
    transport: transportfilters,
    distoutbound: distoutboundfilters,
    distinbound: distinboundfilters,
    orders: ordersfilters,
    network: networkfilters,
    stockConstraintReport: stockConstraintReportFilter
}
const initialState = {
    showfavs: true,
    theme: "dark",
    filters: {}
}

const optionsReducer = (state = initialState, action) => {

    const { filter, subtype } = action.payload || {};
    let filters = get(state, `filters.${subtype}`, null);
    const defaultFilters = defaultdata[subtype];

    if (!filters) {
        filters = defaultFilters;
    }
    switch (action.type) {
        case optionsConstants.SHOW_HIDE_FAVOURITES:
            return { ...state, showfavs: action.payload }

        case optionsConstants.SET_FILTERS:
            const { defaultViewsFilters = {} } = action.payload
            const spreadFilters = isEmpty(defaultViewsFilters) ? state.filters : defaultViewsFilters
            return {
                ...state,
                filters: {
                    ...spreadFilters,
                    [subtype]: {
                        ...filters,
                        ...filter
                    }
                }
            }
        case optionsConstants.RESET_FILTERS:
            return {
                ...state,
                filters: {
                    ...state.filters,
                    [subtype]: {
                        ...defaultFilters,
                    }
                }
            }

        case optionsConstants.SAVE_COLUMN_STATE:
            return {
                ...state,
                columns: {
                    ...state.columns,
                    [subtype]: [
                        ...action?.payload?.columns || [],
                    ]
                }
            }
        case optionsConstants.RESET_COLUMN_STATE:
            let tempCols = state?.columns;
            delete tempCols?.[subtype];
            return {
                ...state,
                columns: {
                    ...tempCols
                }
            }

        case optionsConstants.SET_VIEW_BY:
            return {
                ...state,
                showTabsBy: action.payload
            }

        case optionsConstants.SET_TABLE_BODY:
            const { body, tableName } = action.payload;
            return {
                ...state,
                tablebody: {
                    [tableName]: body
                }
            }

        default:
            return state
    }
}

export default optionsReducer;
