import { guardrailsConstants } from '../constants';

const guardrailsReducer = (state = { getGaurdrailsLoading: false, updateGaurdrailsLoading: false, allSiteGuardrails: {data:[], allowedToEdit:false}, notification:{}, disableButton:true}, action) => {
    switch (action.type) {

        case guardrailsConstants.GUARDRAILS_FETCH_SUCCESS:
            return { ...state, allSiteGuardrails: action.payload, notification:{}, disableButton:false }

        case guardrailsConstants.GUARDRAILS_FETCH_FAILED:
            return { ...state, notification:{}, allSiteGuardrails: {data:[], allowedToEdit:false} }
        
        case guardrailsConstants.GUARDRAILS_UPDATE_SUCCESS:
            const newGuardrailsObject = action.payload
            return { ...state, notification:{message:"Changes updated successfully", severity:"info"}, 
            allSiteGuardrails: {
                ...state.allSiteGuardrails, 
                data: [...state.allSiteGuardrails.data.filter(({siteNum}) => siteNum !== newGuardrailsObject.siteNum), newGuardrailsObject] 
            }}

        case guardrailsConstants.GUARDRAILS_UPDATE_FAILED:
            return { ...state, notification:{message:" You are not authorized to make changes", severity:"error"} }
        
        case guardrailsConstants.GUARDRAILS_FETCH_LOADING:
            return { ...state, getGaurdrailsLoading: action.payload, notification:{}}
            
        case guardrailsConstants.GUARDRAILS_UPDATE_LOADING:
            return { ...state, updateGaurdrailsLoading: action.payload, notification:{} }
                                    
        default:
            return state
    }
}

export default guardrailsReducer;