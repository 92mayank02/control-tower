import { analyticsConstants } from "../constants"

const analyticsReducer = (state = { loading: false, cstUpdated: null }, action) => {
  
  switch (action.type) {
    case analyticsConstants.UPDATE_CST_RANK:
      return { ...state, cstUpdated: true };

    case analyticsConstants.UPDATE_FAILED:
      return { ...state, cstUpdated: action.payload }          

    case analyticsConstants.UPDATE_LOADING:
      return { ...state, loading: action.payload }

    default:
      return state
  }
}

export default analyticsReducer;