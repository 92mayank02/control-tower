import { stockConstraintReportConstants } from "../constants";

const stockConstraintReportReducer = (state = { 
    shipment_list_loading: false, 
    shipmentList: [],
    shipment_material_loading: false,
    shipmentMaterials: {},
    shipment_data_loading: false,
    shipment_data: {},
    recomendation_data_loading: false,
    recomendation_data: []
}, action) => {
    switch(action.type) {
        case stockConstraintReportConstants.FETCH_SHIPMENT_LIST_SUCCESS:
        case stockConstraintReportConstants.SHIPMENT_LIST_FAILED:
            return {
                ...state,
                shipmentList: action.payload
            }
        case stockConstraintReportConstants.SHIPMENT_LIST_LOADING:
            return {
                ...state,
                shipment_list_loading: action.payload
            }
        case stockConstraintReportConstants.FETCH_SHIPMENT_MATERIALS_SUCCESS:
        case stockConstraintReportConstants.SHIPMENT_MATERIALS_FAILED:
            return {
                ...state,
                shipmentMaterials: action.payload
            }
        case stockConstraintReportConstants.SHIPMENT_MATERIALS_LOADING:
            return {
                ...state,
                shipment_material_loading: action.payload
            }
        case stockConstraintReportConstants.SEARCH_SHIPMENT_SUCCESS:
        case stockConstraintReportConstants.SEARCH_SHIPMENT_FAILED:
            return {
                ...state,
                shipment_data: action.payload
            }
        case stockConstraintReportConstants.SEARCH_SHIPMENT_LOADING:
            return {
                ...state,
                shipment_data_loading: action.payload
            }
        case stockConstraintReportConstants.FETCH_RECOMENDATIONS_SUCCESS:
        case stockConstraintReportConstants.RECOMENDATIONS_FAILED:
            return {
                ...state,
                recomendation_data: action.payload
            }
        case stockConstraintReportConstants.RECOMENDATIONS_LOADING:
            return {
                ...state,
                recomendation_data_loading: action.payload
            }
        default:
            return state;
    }
}

export default stockConstraintReportReducer;