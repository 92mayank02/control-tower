import { combineReducers } from 'redux';
import getLocationsReducer from './getLocationsReducer';
import getFavUnfavReducer from './getFavUnfavReducer';
import selectionsReducer from "./selectionsReducer"
import authReducer from './authReducer';
import optionsReducer from './optionsReducer';
import chartsReducer from './getChartsReducer';
import downloadReducer from './downloadReducer';
import getFilterLayoutsReducer from './getFilterLayoutsReducer';
import getShipmentDetailsReducer from './getShipmentDetailsReducer';
import feedbackReducer from './feedbackReducer';
import tabledataReducer from './tabledataReducer';
import analyticsReducer from './analyticsReducer';
import guardrailsReducer from './guardrailsReducer';
import stockConstraintReportReducer from './stockConstraintReportReducer';
import {connectRouter} from "connected-react-router"

export default (history) => combineReducers({
    router: connectRouter(history),
    sites: getLocationsReducer,
    favorites: getFavUnfavReducer,
    items: selectionsReducer,
    auth: authReducer,
    options: optionsReducer,
    charts: chartsReducer,
    downloads: downloadReducer,
    user: getFilterLayoutsReducer,
    shipment:getShipmentDetailsReducer,
    feedbackform: feedbackReducer,
    tabledata: tabledataReducer,
    analytics: analyticsReducer,
    guardrails:guardrailsReducer,
    stockConstraintReport: stockConstraintReportReducer
});