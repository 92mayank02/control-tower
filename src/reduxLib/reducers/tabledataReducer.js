import { tabledataConstants } from '../constants';

const tabledataReducer = (state = { loading: true, data: {} }, action) => {

    const { variables } = action;

    switch (action.type) {

        case tabledataConstants.TABLE_DATA_FETCH_SUCCESS:
        case tabledataConstants.TABLE_DATA_FETCH_FAILURE:
            return { ...state, data: action.payload, ...variables }

        case tabledataConstants.LOADING:
            return { ...state, loading: action.payload, data: action.payload ? {} : state.data }

        default:
            return state
    }
}

export default tabledataReducer;
