export const businessUnits = ["CONSUMER", "PROFESSIONAL"]

export const salesOrgsbyBU = [
    {
        bu:businessUnits[1],
        salesOrgs:[{value:"2821", region:"Canada"},{value:"2811", region:"US"}],
    },
    {
        bu:businessUnits[0],
        salesOrgs:[{value:"2820", region:"Canada"},{value:"2810", region:"US"}],        
    }
]
export const viewByConstants = {
    locations: {
        name: "shortName",
        id: "siteNum",
        type: "type"
    },
    customer: {
        name: "salesOffice",
        id: "salesOffice",
        type: "selectionType",
    },
    
}

export const distributionChannel = [{value:"80", name:"80 - Domestic"},{value:"90", name:"90 - International"}]