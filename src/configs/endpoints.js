export const endpoints = {
    table : {
        network: "/v2/orders/search",
        orders: "/v2/orders/search",
        transport: "/v2/orders/search",
        distributionInbound: "/v2/orders/search/inbound",
        distributionOutbound: "/v2/orders/search"
    },
    charts:"/v2/summary",
    itemDetails:"/v2/orders/search/items",
    shipmentDetails:"/shipment/search",
    globalFilters:{
        locations:"/sites/region/NA",
        sitePerformance: "/sites/performance/region/NA",
        customerSearch:"/customer/search",
        saleOfficeSearch:"/customer/salesoffice/search"
    },
    favourites: {
        userFavourites: "/users/favourites",
        favSites: "/users/favSites?action=fav",
        unfavSites: "/users/favSites?action=unfav",
        buFavourites: "/users/favBusinessUnits",
        cusoFavourites: "/users/favCustomerSalesOffice"
    },
    analytics: {
        updateCstRanking: "/carrieroptimization/secondarysequence"
    },
    guardrails: {
       siteData:"/site/guardrails",
       allData:"/users/getAllWarehouseGuardrails",
       update:"/users/updateGuardrail"
    },
    stockConstraintReport: {
        shipmentList:"/stockconstraintreport/shipments",
        shipmentMaterials:"/stockconstraintreport/shipments/items",
        shipmentSearch:"/stockconstraintreport/shipments/search",
        shipmentMaterialRecomendation:"/stockconstraintreport/shipments/items/recommendation"
    }
}