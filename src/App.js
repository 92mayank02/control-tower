import React from 'react';
import { ThemeProvider, CssBaseline } from '@material-ui/core';
import { Provider } from 'react-redux'
import configureStore from './store'; //Store Configuration
import Routes from './Routes';
import './assets/scss/index.scss';
import { appTheme } from "../src/theme";
import { ConnectedRouter } from 'connected-react-router' 
import { history } from "store"

const store = configureStore();
window.store = store

const App = () => {
  return (
    <Provider store={store}>
      <ThemeProvider theme={appTheme}>
        <CssBaseline />
        <ConnectedRouter history={history}>
            <Routes />
        </ConnectedRouter>
      </ThemeProvider>
    </Provider>
  );
};

export default App;
